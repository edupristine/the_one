<?phpinclude '../inc/GenericFunctions.php';include '../control/core.php';include '../control/checklogin.php';include '../control/connection.php';
try{
	if( isset($_POST['sku_name']) && isset($_POST['sku_code']) && isset($_POST['id']) && !empty($_POST['sku_name']) && !empty($_POST['sku_name']) && !empty($_POST['id']) )
	{
		$sku_name		 	= get_post_value('sku_name');		$sku_code		 	= get_post_value('sku_code');		$sku_type		 	= get_post_value('sku_type');		$sub_courses		= $_POST['param'];		$sku_desc		 	= get_post_value('sku_desc');		$unit_price		 	= get_post_value('unit_price'); if($unit_price == '') $unit_price = 0;		$reorder_level		= get_post_value('reorder_level'); if($reorder_level == '') $reorder_level = 0;		$o_to_r_days	 	= get_post_value('o_to_r_days'); if($o_to_r_days == '') $o_to_r_days = 0;		$pref_qty		 	= get_post_value('pref_qty'); if($pref_qty == '') $pref_qty = 0;		$min_order_qty		= get_post_value('min_order_qty'); if($min_order_qty == '') $min_order_qty = 0;		$id = get_post_value('id');		if(count($sub_courses) == 0)		{			header('location:../sku_edit.php?sub_course_missing&id='.$id);			exit();		}
		$query_dupcheck = "SELECT `id` FROM `skus` WHERE `sku_name`='$sku_name' AND `id`!='$id'";		$result_dupcheck = Select($query_dupcheck,$conn);		if($result_dupcheck['count']!=0){			header('location:../sku_edit.php?dupname1&id='.$id);			exit();		}		$query_dupcheck = "SELECT `id` FROM `skus` WHERE `sku_code`='$sku_code' AND `id`!='$id'";		$result_dupcheck = Select($query_dupcheck,$conn);		if($result_dupcheck['count']!=0){			header('location:../sku_edit.php?dupname2&id='.$id);			exit();		}

		$query_update = "UPDATE `skus` SET `sku_name` = '".$sku_name."',										   `sku_code` = '".$sku_code."',										   `sku_type`= '".$sku_type."',										   										   `sku_desc`= '".$sku_desc."',										   `unit_price`= '".$unit_price."',										   `reorder_level`= '".$reorder_level."',										   `o_to_r_days`= '".$o_to_r_days."',										   `pref_qty`= '".$pref_qty."',										   `min_order_qty`= '".$min_order_qty."' WHERE id = ".$id;
		$result_update = Update($query_update,$conn,'skus');				$del_qry = "DELETE from subcourses_skus where sku_id = ".$id;		$result_delete = Del($del_qry,$conn);				foreach($sub_courses as $sub_course)		{			$query_insert = "INSERT INTO `subcourses_skus`(`sku_id`, `subcourse_id`) VALUES ('".$id."','".$sub_course."')";			$result_insert = Insert($query_insert,$conn,'subcourses_skus');		}		
		header('location:../sku_edit.php?success&id='.$id);
	}
	else	{
		header('location:../sku_edit.php?paramsmissing&id='.$id);
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);	//print_r($ex);
	header('location:../sku_edit.php?dberror&id='.$id);	exit();
}
?>