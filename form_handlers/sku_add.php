<?phpinclude '../inc/GenericFunctions.php';include '../control/core.php';include '../control/checklogin.php';include '../control/connection.php';
try{
	if( isset($_POST['sku_name']) && isset($_POST['sku_code']) && !empty($_POST['sku_name']) && !empty($_POST['sku_name']) )
	{
		$sku_name		 	= get_post_value('sku_name');		$sku_code		 	= get_post_value('sku_code');		$sku_type		 	= get_post_value('sku_type');		$sub_courses		 	= $_POST['param'];		$sku_desc		 	= get_post_value('sku_desc');		$unit_price		 	= get_post_value('unit_price'); if($unit_price == '') $unit_price = 0;		$reorder_level		= get_post_value('reorder_level'); if($reorder_level == '') $reorder_level = 0;		$o_to_r_days	 	= get_post_value('o_to_r_days'); if($o_to_r_days == '') $o_to_r_days = 0;		$pref_qty		 	= get_post_value('pref_qty'); if($pref_qty == '') $pref_qty = 0;		$min_order_qty		= get_post_value('min_order_qty'); if($min_order_qty == '') $min_order_qty = 0;						if(count($sub_courses) == 0)		{			header('location:../sku_add.php?sub_course_missing');			exit();		}
		$query_dupcheck = "SELECT `id` FROM `skus` WHERE `sku_name`='$sku_name'";		$result_dupcheck = Select($query_dupcheck,$conn);		if($result_dupcheck['count']!=0){			header('location:../sku_add.php?dupname1');			exit();		}		$query_dupcheck = "SELECT `id` FROM `skus` WHERE `sku_code`='$sku_code'";		$result_dupcheck = Select($query_dupcheck,$conn);		if($result_dupcheck['count']!=0){			header('location:../sku_add.php?dupname2');			exit();		}
		$query_insert = "INSERT INTO `skus` (`sku_name`, `sku_code`, `sku_type`,`sku_desc`, `unit_price`, `reorder_level`, `o_to_r_days`, `pref_qty`, `min_order_qty`)
		VALUES('".$sku_name."','".$sku_code."','".$sku_type."','".$sku_desc."','".$unit_price."','".$reorder_level."','".$o_to_r_days."','".$pref_qty."','".$min_order_qty."')";
		$result_insert = Insert($query_insert,$conn,'skus');		$sku_id  = $result_insert['id'];				foreach($sub_courses as $sub_course)		{			$query_insert = "INSERT INTO `subcourses_skus`(`sku_id`, `subcourse_id`) VALUES ('".$sku_id."','".$sub_course."')";			$result_insert = Insert($query_insert,$conn,'subcourses_skus');					}				$select_locations = "SELECT id from locations where is_active = 1";		$result_loc = Select($select_locations,$conn);				foreach($result_loc['rows'] as $location)		{			$query_insert = "INSERT INTO `skus_locations`(`sku_id`, `loc_id`, `sku_qty_avl`, `sku_pre_qty`, `sku_qty_min`, `sku_qty_req`, `sku_qty_tot_in`, `sku_qty_tot_out`, `sku_qty_pref`) VALUES ('".$sku_id."','".$location['id']."','0','0','0','0','0','0','0')";			$result_insert = Insert($query_insert,$conn,'skus_locations');		}		
		header('location:../sku_add.php?success');
	}
	else{
		header('location:../sku_add.php?paramsmissing');
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);	//print_r($ex);
	header('location:../sku_add.php?dberror');
	//exit();
}
?>