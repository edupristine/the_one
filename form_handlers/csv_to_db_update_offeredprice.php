<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
if(isset($_POST['importSubmit'])){
    
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $crm_id   = $line[0];
                $offeredprice = $line[1];
				$paid_as_revenue = $line[2];
                
                
                
                $qry = "INSERT INTO `bulk_offeredprice_update` (`crm_id`, `offeredprice`,`paid_as_revenue`)
                VALUES ('".$crm_id."','".$offeredprice."','".$paid_as_revenue."')";
                mysqli_query($conn,$qry);
                
            }
            
            // Close opened CSV file
            fclose($csvFile);
            
            $qstring = '?status=succ&'.$qry;
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

// Redirect to the listing page
header("Location: http://one.edupristine.com/update_bulk_offeredprice.php");