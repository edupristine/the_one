<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
if(isset($_POST['importSubmit'])){
    
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $lead_reason   = $line[0];
                $lead_id = $line[1];
                $lead_no  = $line[2];
                $lead_owner = $line[3];
                $lead_status = $line[4];
                $new_owner = $line[5];
                $new_owner_id = $line[6];
                $new_lead_status = $line[7];
                $new_lead_status_reason = $line[9];
                
                
                $qry = "INSERT INTO `sf_lead_upsert` (`LeadReason`, `LeadID`, `LeadNo`, `LeadOwner`, `LeadStatus`, `NewOwner`, `NewOwnerID`, `NewLeadStatus`, `NewLeadStatusReason`)
                VALUES ('".$lead_reason."','".$lead_id."','".$lead_no."','".$lead_owner."','".$lead_status."','".$new_owner."','".$new_owner_id."','".$new_lead_status."','".$new_lead_status_reason."')";
                mysqli_query($conn,$qry);
                
            }
            
            // Close opened CSV file
            fclose($csvFile);
            
            $qstring = '?status=succ&'.$qry;
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

// Redirect to the listing page
//header("Location: http://one.edupristine.com/upsert_bulk_leads.php".$qstring);
header("Location: http://one.edupristine.com/upsert_bulk_leads.php");