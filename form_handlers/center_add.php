<?phpinclude '../inc/GenericFunctions.php';include '../control/core.php';include '../control/checklogin.php';include '../control/connection.php';
try{
	if( isset($_POST['center_name']) && isset($_POST['location_id']) && !empty($_POST['center_name']) && !empty($_POST['location_id']) )
	{
		$center_name		 	= get_post_value('center_name');		$center_manager		 	= get_post_value('center_manager');		$location_id		 	= get_post_value('location_id');										if(count($center_name) == 0)		{			header('location:../center_add.php?center_name_missing');			exit();		}
		$query_dupcheck = "SELECT `id` FROM `centers` WHERE `center_name`='$center_name'";		$result_dupcheck = Select($query_dupcheck,$conn);		if($result_dupcheck['count']!=0){			header('location:../center_add.php?dupcenter');			exit();		}
		$query_insert = "INSERT INTO `centers` (`center_name`, `center_manager`, `loc_id`)
		VALUES('$center_name','$center_manager','$location_id')";
		$result_insert = Insert($query_insert,$conn,'centers');				
		header('location:../center_add.php?success');
	}
	else{
		header('location:../center_add.php?paramsmissing');
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);	//print_r($ex);	//exit;
	header('location:../center_add.php?dberror');
	//exit();
}
?>