<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
if(isset($_POST['importSubmit'])){
    
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            $header = fgetcsv($csvFile);
            $update_field = $header[1];
            
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $external_id = $line[0];
                $update_value = $line[1];
                
                $qry = "INSERT INTO `sf_lead_update` (`external_id`, `update_value`, `update_field`)
                        VALUES ('".$external_id."','".$update_value."','".$update_field."');";
                mysqli_query($conn,$qry);
                
            }
            
            // Close opened CSV file
            fclose($csvFile);
            
            $qstring = '?status=succ';
        }else{
            $qstring = '?status=err';
        }
    }else{
        $qstring = '?status=invalid_file';
    }
}

// Redirect to the listing page
header("Location: http://one.edupristine.com/update_lead_data.php".$qstring);