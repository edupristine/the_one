<?php 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';

// Details verified file

if (isset($_POST['main_report_id'])&& !empty($_POST['main_report_id'])) {
	
	$selected_records=$_POST['selected_records'];
	$reject_all=$_POST['reject_all'];
	
	
	if($_POST['comments']!='')
	{
	$comments=$_POST['comments'];
	}
	else
	{
	$comments="NA";	
	}
    
	
	$verified_by=$_POST['verified_by'];
	$record_id=$_POST['main_report_id'];
	$total_approved_amount_val=$_POST['total_approved_amount_val'];
	$select_rec = "SELECT * FROM expense_report WHERE id=".$record_id; 
	$result_report = Select($select_rec, $conn_exp);
	$main_id=$result_report['rows'][0]['id'];
	$exp_report_display_id=$result_report['rows'][0]['exp_report_id'];
	$report_name=$result_report['rows'][0]['report_name'];
	$report_desc=$result_report['rows'][0]['rep_desc'];
	$report_wallettype=$result_report['rows'][0]['wallettype'];
	$created_on=$result_report['rows'][0]['created_on'];
	$submitted_on=$result_report['rows'][0]['submitted_on'];
	$created_by=$result_report['rows'][0]['created_by'];
	
	$select_user = "SELECT * FROM users WHERE id=".$created_by; 
	$result_user = Select($select_user, $conn_exp);
	$user_name=$result_user['rows'][0]['user_name'];
	$logged_in_user=$result_user['rows'][0]['user_email'];
	$uname=$result_user['rows'][0]['user_name'];
	
	$select_verifier = "SELECT * FROM users WHERE id=".$verified_by; 
	$result_verifier = Select($select_verifier, $conn_exp);
	$verifier_name=$result_verifier['rows'][0]['user_name'];
	
	$select_user_loaction = "SELECT u.`id`, `user_name`, `user_email`, `user_contact`, `password`, `loc_id`,u.`is_active`,l.loc_type,l.loc_name,`user_type` FROM `users` u, locations l WHERE l.id = u.loc_id and u.id=".$created_by; 
	$result_location = Select($select_user_loaction, $conn_exp);
	$loc_name=$result_location['rows'][0]['loc_name'];
	
	

	
	if($reject_all=="")
	{
	$query_counter_2 = "UPDATE expense_report
	SET is_verified=1,verified_on=now(),verified_by=".$verified_by.",verifier_comments='".$comments."'
	where id='".$record_id."' 
	"; 
	
	}
	else if($reject_all=="on")
	{
	$query_counter_2 = "UPDATE expense_report
	SET is_verifier_rejected=1,verifier_rejected_on=now(),verifier_rejected_by=".$verified_by.",verifier_comments='".$comments."'
	where id='".$record_id."'";	
	}
	
	//echo $query_counter_2."<br>";
	$result_counter_final_2 = Select($query_counter_2,$conn_exp);

	
	if($reject_all=="")
	{
	$query_counter4 = "SELECT exp_id  as `exp_id` FROM `expense_report_mapping` where exp_report_id='".$record_id."' and
	exp_id in (".$selected_records.")";
	//echo  $query_counter4."<br>";
	$result_counter4 = Select($query_counter4,$conn_exp);
	foreach($result_counter4['rows'] as $rec_expense)
	{
	$select_rec_amount = "SELECT approved_amount FROM expense_records WHERE id='".$rec_expense['exp_id']."'"; 
	$result_amount = Select($select_rec_amount, $conn_exp);
	$main_amount=$result_amount['rows'][0]['approved_amount'];

	$query_counter_final = "UPDATE expense_records
	SET is_verified=1,verified_on=now(),verified_amount='".$main_amount."',verified_by='".$verified_by."' where id='".$rec_expense['exp_id']."'";
	
	//echo $query_counter_final."<br>";
	$result_counter_final = Select($query_counter_final,$conn_exp);
	}
	
	
	$query_counter_rej = "SELECT exp_id  as `exp_id` FROM `expense_report_mapping` where exp_report_id='".$record_id."' and
	exp_id not in (".$selected_records.")";
	//echo  $query_counter_rej."<br>";
	$result_counter_rej = Select($query_counter_rej,$conn_exp);
	foreach($result_counter_rej['rows'] as $rec_rej)
	{

	$query_rej_final = "UPDATE expense_records
	SET is_verifier_rejected=1,verified_by='".$verified_by."'
	where id='".$rec_rej['exp_id']."'";
	
	//echo $query_rej_final."<br>";
	$result_rej_final = Select($query_rej_final,$conn_exp);
	}
	}
	
	
	if($reject_all=="on")
	{
	$query_counter4 = "SELECT exp_id  as `exp_id` FROM `expense_report_mapping` where exp_report_id='".$record_id."'";
	//echo  $query_counter4."<br>";
	$result_counter4 = Select($query_counter4,$conn_exp);
	foreach($result_counter4['rows'] as $rec_expense)
	{


	$query_counter_final = "UPDATE expense_records
	SET is_verifier_rejected=1,verifier_rejected_by=".$verified_by.",verifier_rejected_on=now() where id='".$rec_expense['exp_id']."'";
	
	//echo $query_counter_final."<br>";
	$result_counter_final = Select($query_counter_final,$conn_exp);
	}
	
	}
	
     if($reject_all=="")
	{
	$response = "record_created";
	$response = json_encode($response);
	echo $response;
	//Mail Final Sending to Requester and Accounts Team
		$html="";
		$date = date('Y-m-d');
		$html .= "
        
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; background-color: #337AB7; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/logo.png'  height='60px;'/></b></td>
		</tr>
		<tr>
		<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 12px 12px; background-color: #ffffff; text-align:left; font-size: 14px;width:100%;'>
		<p><center><strong>VERIFIED REIMBURSEMENT #".$exp_report_display_id."</strong></center></p></td></tr>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: green; text-align:right; font-size: 14px;width:30%;'><b>
        Requester Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: green; text-align:left; font-size: 14px;width:70%;'><b>".$user_name."</b></td>
		</tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reporting Location:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$loc_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Report Name:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Report Description:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_desc."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Wallet type:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_wallettype."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Total Verified Amount:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>INR 
		".$total_approved_amount_val."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Submitted On:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$submitted_on."</td></tr>";
		
        $html .= "<tr>
		<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 12px 12px; background-color: #ffffff; text-align:left; font-size: 14px;width:100%;'>
		Please find the below Breakup of the expenses verified</td></tr>";

		$html .= "<tr>
		<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; font-size: 14px;width:100%;'>";
		
		$html .= "<table width='100%' style='border: 1px solid #000000; color:#000000;'>";
		
		$html .= "<tr>
		<td style='background-color: #f0f0f0;color:#000000;'><strong>Spent At</strong></td>
	    <td style='background-color: #f0f0f0;color:#000000;text-align:right;'><strong>Amount Requested</strong></td>
		<td style='background-color: #f0f0f0;color:#000000;text-align:right;'><strong>Approved Amount</strong></td>
		<td style='background-color: #f0f0f0;color:#000000;'><strong>City</strong></td>
		<td style='background-color: #f0f0f0;color:#000000;'><strong>Expense Category</strong></td>
		<td style='background-color: #f0f0f0;color:#000000;style='text-align:center;'><strong>Occurance Date</strong></td>
		</tr>";
		
		$query_counter5 = "SELECT * FROM `expense_records` where id in (".$selected_records.")";
        //echo  $query_counter5."<br>";
		$result_counter5 = Select($query_counter5,$conn_exp);
		
		foreach($result_counter5['rows'] as $res_expense_rec)
		{
		$html .= "<tr>
		<td>".$res_expense_rec['spentat']."</td>
	    <td style='text-align:right;'>".$res_expense_rec['amount']." INR</td>
		<td style='text-align:right;'>".$res_expense_rec['approved_amount']." INR</td>
		<td>".$res_expense_rec['city']."</td>
		<td>".$res_expense_rec['exp_category']."</td>
		<td style='text-align:center;'>".$res_expense_rec['occurance_date']."</td>
		</tr>";
		}
		
		
		$html .= "</table>";
		$html .= "</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Verified By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$verifier_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Verified On:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".date('d-m-Y')."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Verifier Comments:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comments."</td></tr>";
		
		$html .= "
        </table>";
		
		$html .="
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		Note: Attached Documents for your references.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		$subject = "Verified reimbursement Details - ".$exp_report_display_id."";
		
		$query_counter6 = "SELECT * FROM `files` where exp_id in (".$selected_records.")";
        //echo  $query_counter6."<br>";
		$result_counter6 = Select($query_counter6,$conn_exp);
		$files_array=array();
		foreach($result_counter6['rows'] as $res_files)
		{
			$files_array[] = $res_files['file_name'];
		}
		
		send_smt_mail($subject,$html,$files_array,$logged_in_user,$uname);
	}
	if($reject_all=="on")
	{

	//Mail Final Sending to Rajesh Sir and Accounts Team
		$html="";
		$date = date('Y-m-d');
		$html .= "
        
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; background-color: #337AB7; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/logo.png'  height='60px;'/></b></td>
		</tr>
		<tr>
		<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 12px 12px; background-color: #ffffff; text-align:left; font-size: 14px;width:100%;'>
		<p><center><strong>VERIFIER REJECTED REIMBURSEMENT #".$exp_report_display_id."</strong></center></p></td></tr>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: red; text-align:right; font-size: 14px;width:30%;'><b>
        Requester Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: red; text-align:left; font-size: 14px;width:70%;'><b>".$user_name."</b></td>
		</tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reporting Location:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$loc_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Report Name:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Report Description:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_desc."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Wallet type:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$report_wallettype."</td></tr>";
		
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Submitted On:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$submitted_on."</td></tr>";
		
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$verifier_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected On:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".date('d-m-Y')."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Verifier Comments:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comments."</td></tr>";
		
		$html .= "
        </table>";
		
		$html .="

		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		$subject = "Verifier Rejected reimbursement Details - ".$exp_report_display_id."";
		
		
		send_smt_mail_rejected($subject,$html,$logged_in_user,$uname);
		
	}
	
	
		
	    header("Location: http://one.edupristine.com/expenses/dashboard.php");

	exit;
}
else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}

function send_smt_mail($subject,$email_msg,$files_array,$logged_in_user,$uname){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Reimbursement Notification');
		
		$mail->addAddress('accountspayable@edupristine.com', 'Accounts Payable');
		$mail->addAddress($logged_in_user, $uname);	
		$mail->addBCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		foreach($files_array as $files)
		{
		$case_name=explode("_",$files);
        $mail->AddAttachment("/home/ubuntu/webapps/the_one/expenses/uploads/EXP_CASE_".$case_name[0]."/".$files."");
		}
		
        	
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}


function send_smt_mail_rejected($subject,$email_msg,$logged_in_user,$uname){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Reimbursement Verifier Approval Notification');

		$mail->addAddress($logged_in_user, $uname);	
		$mail->addBCC('hitesh.patil@edupristine.com', 'Hitesh Patil');

        	
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}

function dateName($date) {

	$result = "";

	$convert_date = strtotime($date);
	$month = date('F',$convert_date);
	$year = date('Y',$convert_date);
	$name_day = date('l',$convert_date);
	$day = date('j',$convert_date);


	$result = $month . " " . $day . ", " . $year;

	return $result;
}

?>