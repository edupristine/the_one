<?php 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';
// Details save file

if (isset($_POST['student_name'])&& !empty($_POST['student_name'])) {
	
	$timestamp = time();
	$student_name=$_POST['student_name'];
	$course=$_POST['course'];
	$email_id=$_POST['email_id'];
	$contact_no=$_POST['contact_no'];
	$amount=$_POST['amount'];
	$receiver_name=$_POST['receiver_name'];
	$payment_reason=$_POST['payment_reason'];
	$payment_mode=$_POST['payment_mode'];
	$rep_desc=$_POST['rep_desc'];
	
	
	$uq_recpt_no="EDU-".strtoupper($payment_mode)."-".$_SESSION['U_LOCATION_ABBR']."-".$timestamp;
	
	
	$insert_rec = "INSERT INTO cash_acknowledgement (uq_recpt_no,student_name, student_course, email_id, contact_no, amount, receiver_name, payment_reason, payment_mode, description,created_on , created_by,location,is_mail_sent ) VALUES ('".$uq_recpt_no."','".$student_name."','".$course."','".$email_id."','".$contact_no."','".$amount."','".$receiver_name."','".$payment_reason."','".$payment_mode."','".$rep_desc."', NOW(), '".$_SESSION['USER_ID']."','".$_SESSION['U_LOCATION_ID']."',1)"; 
	

	$result_counter = Select($insert_rec,$conn_exp);
	$rec_id  = $conn_exp->lastInsertId();
	
	$response = "record_created";
	$response = json_encode($response);
	echo $response;
	//Mail Sending for Approval to Rajesh Sir
		$html="";
		$date = date('Y-m-d');
		$html .= "
        <p>
		
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; background-color: #337AB7; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/logo.png'  height='60px;'/></b></td>
		</tr>
		<tr>
		<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 12px 12px; background-color: #ffffff; text-align:left; font-size: 14px;width:100%;'>
		<p><center><strong>PROVISIONAL ".strtoupper($payment_mode)." ACKNOWLEDGEMENT DETAILS</strong></center></p></td></tr>
		<tr>
		<td style='border: 1px solid #008080; color:white; padding: 8px 8px; background-color: #008080; text-align:right; font-size: 14px;width:30%;'><b>
        Created by:</b></td>
		<td style='border: 1px solid #008080; color:white; padding: 8px 8px; background-color: #008080; text-align:left; font-size: 14px;width:70%;'><b>".$_SESSION['U_NAME']."</b></td>
		</tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Recepient Location:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$_SESSION['U_LOCATION_NAME']."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Student Name:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$student_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount Received:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>INR 
		".$amount."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Course:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$course."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Email Id:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$email_id."</td></tr>";
		
	    $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Contact No:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'> 
		".$contact_no."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Receiver Name:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$receiver_name."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Payment Reason:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$payment_reason."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Received On:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".date('d-M-Y')."</td></tr>";
		
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Unique record Id:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
		".$uq_recpt_no."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Center Address:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$_SESSION['U_LOCATION_ADD']."</td></tr>";
	
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Comments:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$rep_desc."</td></tr>";
		
		
		


		
		
		$html .= "
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = date("d-M-Y")." Received ".strtoupper($payment_mode)." Acknowledgement Details - ".$uq_recpt_no;

		send_smt_mail($subject,$html,$email_id,$student_name);
	
	    header("Location: http://one.edupristine.com/expenses/receive_cash.php?status=succ&capture_id=".$rec_id."");

	exit;
}
else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}

function send_smt_mail($subject,$email_msg,$email_id,$student_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Received Cash/Cheque Acknowledgement Notification');
		$mail->addAddress($email_id, $student_name);
        $mail->addBCC('accountspayable@edupristine.com', 'Accounts Payable');		
        $mail->addBCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
	
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}

function dateName($date) {

	$result = "";

	$convert_date = strtotime($date);
	$month = date('F',$convert_date);
	$year = date('Y',$convert_date);
	$name_day = date('l',$convert_date);
	$day = date('j',$convert_date);


	$result = $month . " " . $day . ", " . $year;

	return $result;
}

?>