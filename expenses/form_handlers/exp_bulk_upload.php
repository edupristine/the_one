<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn_exp = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'expenses');
if(isset($_POST['importSubmit'])){
$timestamp = time();	
$usertype=$_POST['usertype'];	
$bulk_rec_array="";
$total_amount=0;
    // Allowed mime types
    $csvMimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain');
    
    // Validate whether selected file is a CSV file
    if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'], $csvMimes)){
        
        // If the file is uploaded
        if(is_uploaded_file($_FILES['file']['tmp_name'])){
            
            // Open uploaded CSV file with read-only mode
            $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
            
            // Skip the first line
            fgetcsv($csvFile);
            
			//Cash
			if($_POST['exp_type']==1)
			{
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $description   = $line[0];
                $spentat = $line[1];
                $exp_category  = $line[2];
                $currency = $line[3];
				$city = $line[4];
                $amount = $line[5];
                $wallettype = $line[6];
                $occurance_date = $line[7];
                $from_date = $line[8];
                $to_date = $line[9]; 
				$qry = "INSERT INTO `expense_records` (`exp_type`,`description`, `spentat`, `exp_category`, `currency`,`city`, `amount`, `wallettype`, `occurance_date`, `from_date`, `to_date`,`created_on`,`created_by`,`user_type`,`is_saved`,`saved_on`)
				VALUES (1,'".$description."','".$spentat."','".$exp_category."','".$currency."','".$city."','".$amount."','".$wallettype."','".$occurance_date."','".$from_date."','".$to_date."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())";
              
				mysqli_query($conn_exp,$qry);
				$ins_val=$conn_exp->insert_id; 
                $bulk_rec_array .=$bulk_rec_array + $ins_val .",";
				$total_amount += round($amount,2);
				
				$insert_rec_map = "INSERT INTO expense_report_mapping (exp_id, created_on, created_by, user_type,is_saved,saved_on ) VALUES ('".$ins_val."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())"; 
				$result_id_mapped = mysqli_query($conn_exp,$insert_rec_map);
            }
			}
			
			//Vehicle allowance
			if($_POST['exp_type']==2)
			{
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $description   = $line[0];
                $spentat = $line[1];
                $exp_category  = $line[2];
                $currency = $line[3];
				$city = $line[4];
                $amount = $line[5];
                $wallettype = $line[6];
                $occurance_date = $line[7];
                $type_of_vehicle = $line[8];
                $distance = $line[9]; 
				$rate_per_km = $line[10]; 
				$qry = "INSERT INTO `expense_records` (`exp_type`,`description`, `spentat`, `exp_category`, `currency`,`city`, `amount`, `wallettype`, `occurance_date`, `type_of_vehicle`, `distance`,`rate_per_km`,`created_on`,`created_by`,`user_type`,`is_saved`,`saved_on`)
				VALUES (2,'".$description."','".$spentat."','".$exp_category."','".$currency."','".$city."','".$amount."','".$wallettype."','".$occurance_date."','".$type_of_vehicle."','".$distance."','".$rate_per_km."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())";
                mysqli_query($conn_exp,$qry);
				$ins_val=$conn_exp->insert_id; 
				$bulk_rec_array .=$bulk_rec_array + $ins_val .",";
				$total_amount += round($amount,2);
				
				$insert_rec_map = "INSERT INTO expense_report_mapping (exp_id, created_on, created_by, user_type,is_saved,saved_on ) VALUES ('".$ins_val."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())"; 
				$result_id_mapped = mysqli_query($conn_exp,$insert_rec_map);
				
            }
			}
			
			//PettyCash
			if($_POST['exp_type']==3)
			{
            // Parse data from CSV file line by line
            while(($line = fgetcsv($csvFile)) !== FALSE){
                // Get row data
                $description   = $line[0];
                $spentat = $line[1];
                $exp_category  = $line[2];
                $currency = $line[3];
				$city = $line[4];
                $amount = $line[5];
                $wallettype = $line[6];
                $occurance_date = $line[7];
                $from_date = $line[8];
                $to_date = $line[9]; 
				$conversion_amount = $line[10]; 
				$qry = "INSERT INTO `expense_records` (`exp_type`,`description`, `spentat`, `exp_category`, `currency`,`city`, `amount`, `wallettype`, `occurance_date`, `from_date`, `to_date`,`conversion_amount`,`created_on`,`created_by`,`user_type`,`is_saved`,`saved_on`)
				VALUES (3,'".$description."','".$spentat."','".$exp_category."','".$currency."','".$city."','".$amount."','".$wallettype."','".$occurance_date."','".$from_date."','".$to_date."','".$conversion_amount."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())";
                mysqli_query($conn_exp,$qry);
				$ins_val=$conn_exp->insert_id; 
				$bulk_rec_array .=$bulk_rec_array + $ins_val .",";
				$total_amount += round($amount,2);
				
				
				$insert_rec_map = "INSERT INTO expense_report_mapping (exp_id, created_on, created_by, user_type,is_saved,saved_on ) VALUES ('".$ins_val."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())"; 
				$result_id_mapped = mysqli_query($conn_exp,$insert_rec_map);
            }
			}
			
			//Batch Imprint
			$qry = "INSERT INTO `expense_records_batch` (`exp_id_array`,`exp_type`, `total_amount`,`created_on`,`created_by`,`user_type`,`is_saved`,`saved_on`)
			VALUES ('".rtrim($bulk_rec_array,",")."','".$_POST['exp_type']."','".round($total_amount,2)."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())";

			mysqli_query($conn_exp,$qry);
            
            // Close opened CSV file
            fclose($csvFile);
            
            $qstring = 'status=succ';
        }else{
            $qstring = 'status=err';
        }
    }else{
        $qstring = 'status=invalid_file';
    }
}

// Redirect to the listing page
header("Location: http://one.edupristine.com/expenses/exp_bulk_upload.php?".$qstring);