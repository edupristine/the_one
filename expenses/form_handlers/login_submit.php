<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';


if (isset($_POST['email']) && isset($_POST['password']) && !empty($_POST['email']) && !empty($_POST['password'])) {
	
    $uname  = $_POST['email'];
    $pwd    = $_POST['password'];
	
    $query  = "SELECT u.`id`, `user_name`, `user_email`, `user_contact`, `password`, `loc_id`,u.`is_active`,l.loc_type,l.loc_name,`user_type`,l.address,l.abbr FROM `users` u, locations l WHERE l.id = u.loc_id and user_email='$uname' and password='$pwd'";

   $result = Select($query, $conn_exp);
    if ($result['count'] == 1) {
        $is_active     = $result['rows'][0]['is_active'];
        $user_id       = $result['rows'][0]['id'];
        $loc_id       = $result['rows'][0]['loc_id'];
		$loc_type       = $result['rows'][0]['loc_type'];
		$loc_name       = $result['rows'][0]['loc_name'];
		$user_name       = $result['rows'][0]['user_name'];
		$user_type       = $result['rows'][0]['user_type'];
		$loc_abbr       = $result['rows'][0]['abbr'];
		$loc_add       = $result['rows'][0]['address'];
		$user_email       = $result['rows'][0]['user_email'];
		
        if ($is_active != '1') {
			$response = "inactive_user";
			$response = json_encode($response);
			echo $response;
			exit;
        }
        $_SESSION['USER_NAME']          = $uname;
		$_SESSION['U_NAME']          = $user_name;
        $_SESSION['USER_ID']            = $user_id;
        $_SESSION['U_LOCATION_ID']        = $loc_id;
		$_SESSION['U_LOCATION_NAME']        = $loc_name;
        $_SESSION['U_LOCATION_TYPE']      = $loc_type;
		$_SESSION['USER_TYPE']      = $user_type;
		$_SESSION['U_LOCATION_ABBR']      = $loc_abbr;
		$_SESSION['U_LOCATION_ADD']      = $loc_add;
		$_SESSION['U_EMAIL']      = $user_email;
		$_SESSION['WH_ID']      =  1;
		$_SESSION['COURIER_ID']      =  2;
        $id=session_id();
		
        $response = "valid_go";
		$response = json_encode($response);
		echo $response;
		//For maintaining login Log
		$query_insert = "INSERT INTO `login_audit`(`user_id`,`session_id`,`ip`, `login_at`) VALUES ('".$user_id."','".$id."','".get_client_ip()."',now())";
		$result_insert = InsertCreMod($query_insert,$conn_exp,"login_audit");	
		exit;
       
    } else {
			$response = "invalid_creds";
			$response = json_encode($response);
			echo $response;
			exit;
    }
} else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}



?>