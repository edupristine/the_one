<?php 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';
// Details save file

if (isset($_POST['exp_type']) && isset($_POST['amount']) && !empty($_POST['exp_type']) && !empty($_POST['amount'])) {
	
	$usertype  = $_POST['usertype'];
	$exp_type  = $_POST['exp_type'];
	$amount    = $_POST['amount'];
	$currency  = $_POST['currency'];
	$wallettype    = $_POST['wallettype'];
	$spentat  = $_POST['spentat'];
	$description    = $_POST['description'];
	$city  = $_POST['city'];
	$distance  = $_POST['distance'];
	$rate_per_km  = $_POST['rate_per_km'];
	
	
	$type_of_vehicle='';
	if($_POST['type_of_vehicle']!='')
	{
	$type_of_vehicle  = $_POST['type_of_vehicle'];
	}
	
	$exp_report_id  = $_POST['exp_report_id'];
	$exp_gen_report_id  = $_POST['exp_gen_report_id'];	
		

	$exp_category    = $_POST['exp_category'];
	$raw_date_of_expense    = explode("/",$_POST['date_of_expense']);
	$date_of_expense    = $raw_date_of_expense[2]."-".$raw_date_of_expense[0]."-".$raw_date_of_expense[1];
	
	$time_of_expense    = $_POST['time_of_expense'];
	$multi_day    = $_POST['multi_day'];
	
	$raw_start    = explode("/",$_POST['start']);
	$start    = $raw_start[2]."-".$raw_start[0]."-".$raw_start[1];
	
	$raw_end    = explode("/",$_POST['end']);
	$end    = $raw_end[2]."-".$raw_end[0]."-".$raw_end[1];


	$query_counter = "SELECT id  as `id` FROM `expense_records` where exp_type='".$exp_type."' and amount='".$amount."' and exp_category='".$exp_category."' and created_by='".$_SESSION['USER_ID']."'";
	$result_counter = Select($query_counter,$conn_exp);
	$counter=$result_counter['rows'][0]['id'];
	
	
	if(($counter=="")||($counter=="0"))
		{
			$insert_rec = "INSERT INTO expense_records (report_id,exp_type, amount, currency, wallettype, spentat, description, city, exp_category, created_on, created_by, user_type, occurance_date, occurance_time, from_date, to_date, is_multiday,type_of_vehicle,distance,rate_per_km,is_saved,saved_on,location ) VALUES ('".$exp_report_id."','".$exp_type."','".$amount."','".$currency."','".$wallettype."','".$spentat."','".$description."','".$city."','".$exp_category."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."','".$date_of_expense."','".$time_of_expense."','".$start."','".$end."','".$multi_day."', '".$type_of_vehicle."', '".$distance."', '".$rate_per_km."',1,NOW(),'".$_SESSION['U_LOCATION_ID']."')"; 
			$result_id = Select($insert_rec, $conn_exp);
			$rec_id  = $conn_exp->lastInsertId();
			
			$insert_rec_map = "INSERT INTO expense_report_mapping (exp_id,exp_report_id, created_on, created_by, user_type,is_saved,saved_on ) VALUES ('".$rec_id."','".$exp_gen_report_id."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW())"; 
			$result_id_mapped = Select($insert_rec_map, $conn_exp);
			
		}
		else
		{
		    $rec_id  = $counter;
		}
	

	
if(!empty($_FILES)){ 

    // File path configuration
	$timestamp = time();
	$folder_gen="EXP_CASE_".$timestamp;
    $uploadDir = $_SERVER['DOCUMENT_ROOT'] ."/expenses/uploads/".$folder_gen."/"; 
 
	if (!file_exists($uploadDir)) {
	mkdir($uploadDir, 0777, true);
	}

    $fileName = $timestamp."_".basename($_FILES['file']['name']); 
    $uploadFilePath = $uploadDir.$fileName; 

    // Upload file to server 
    if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath)){ 
	    chmod($uploadFilePath, 0777);
        // Insert file information in the database 
        $insert_file = "INSERT INTO files (exp_id,file_name,folder_name, uploaded_on) VALUES ('".$rec_id."','".$fileName."','".$folder_gen."', NOW())"; 
		$result = Select($insert_file, $conn_exp);
    } 
	
}

	$response = "record_created";
	$response = json_encode($response);
	echo $response;
	exit;
}
else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}


?>