<?php 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';
// Details save file

if (isset($_POST['exp_type']) && isset($_POST['amount']) && !empty($_POST['exp_type']) && !empty($_POST['amount'])) {
	$timestamp = time();
	$generic_report_id="EDRP"."-".date('Ymd')."-".$timestamp;
	$usertype  = $_POST['usertype'];
	$exp_type  = $_POST['exp_type'];
	$amount    = $_POST['amount'];
	$currency  = $_POST['currency'];
	$wallettype    = $_POST['wallettype'];
	$spentat  = $_POST['spentat'];
	$description    = $_POST['description'];
	$city  = $_POST['city'];
	$distance  = $_POST['distance'];
	$rate_per_km  = $_POST['rate_per_km'];
	$type_of_vehicle  = $_POST['type_of_vehicle'];
	$just_save  = $_POST['just_save'];
	

	$exp_category    = $_POST['exp_category'];
	$raw_date_of_expense    = explode("/",$_POST['date_of_expense']);
	$date_of_expense    = $raw_date_of_expense[2]."-".$raw_date_of_expense[0]."-".$raw_date_of_expense[1];
	
	$time_of_expense    = $_POST['time_of_expense'];
	$multi_day    = $_POST['multi_day'];
	
	$raw_start    = explode("/",$_POST['start']);
	$start    = $raw_start[2]."-".$raw_start[0]."-".$raw_start[1];
	
	$raw_end    = explode("/",$_POST['end']);
	$end    = $raw_end[2]."-".$raw_end[0]."-".$raw_end[1];


	$query_counter = "SELECT id  as `id` FROM `expense_records` where exp_type='".$exp_type."' and amount='".$amount."' and exp_category='".$exp_category."' and created_by='".$_SESSION['USER_ID']."'";
	$result_counter = Select($query_counter,$conn_exp);
	$counter=$result_counter['rows'][0]['id'];
	
	
	if(($counter=="")||($counter=="0"))
		{
			if($just_save==1)
			{
			$insert_rec = "INSERT INTO expense_records (report_id,exp_type, amount, currency, wallettype, spentat, description, city, exp_category, created_on, created_by, user_type, occurance_date, occurance_time, from_date, to_date, is_multiday,type_of_vehicle,distance,rate_per_km,is_saved,saved_on,is_submitted,submitted_on ) VALUES ('".$generic_report_id."','".$exp_type."','".$amount."','".$currency."','".$wallettype."','".$spentat."','".$description."','".$city."','".$exp_category."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."','".$date_of_expense."','".$time_of_expense."','".$start."','".$end."','".$multi_day."', '".$type_of_vehicle."', '".$distance."', '".$rate_per_km."',1,NOW(),1,NOW())"; 
			}
			if($just_save==0)
			{
			$insert_rec = "INSERT INTO expense_records (report_id,exp_type, amount, currency, wallettype, spentat, description, city, exp_category, created_on, created_by, user_type, occurance_date, occurance_time, from_date, to_date, is_multiday,type_of_vehicle,distance,rate_per_km,is_saved,saved_on ) VALUES ('".$generic_report_id."','".$exp_type."','".$amount."','".$currency."','".$wallettype."','".$spentat."','".$description."','".$city."','".$exp_category."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."','".$date_of_expense."','".$time_of_expense."','".$start."','".$end."','".$multi_day."', '".$type_of_vehicle."', '".$distance."', '".$rate_per_km."',1,NOW())"; 
			}
			$result_id = Select($insert_rec, $conn_exp);
			$rec_id  = $conn_exp->lastInsertId();
		}
		else
		{
		    $rec_id  = $counter;
		}
	

	
if(!empty($_FILES)){ 

    // File path configuration
	
	$folder_gen="EXP_CASE_".$timestamp;
    $uploadDir = $_SERVER['DOCUMENT_ROOT'] ."/expenses/uploads/".$folder_gen."/"; 
 
	if (!file_exists($uploadDir)) {
	mkdir($uploadDir, 0777, true);
	}

    $fileName = $timestamp."_".basename($_FILES['file']['name']); 
    $uploadFilePath = $uploadDir.$fileName; 

    // Upload file to server 
    if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath)){ 
	    chmod($uploadFilePath, 0777);
        // Insert file information in the database 
        $insert_file = "INSERT INTO files (exp_id,file_name, uploaded_on) VALUES ('".$rec_id."','".$fileName."', NOW())"; 
		$result = Select($insert_file, $conn_exp);
    } 
	
}

	$response = "record_created";
	$response = json_encode($response);
	echo $response;
	//Mail Sending for Approval to Rajesh Sir
		$html="";
		$date = date('Y-m-d');
		$html .= "
        <p>
		
		<center>REQUEST REIMBURSEMENT #".$generic_report_id."</center></p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #00AAFF; text-align:right; font-size: 14px;width:30%;'><b>
        Requester Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #00AAFF; text-align:left; font-size: 14px;width:70%;'><b>".$_SESSION['U_NAME']."</b></td>
		</tr>";
		
		
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$amount."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Expense Category:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$exp_category."</td></tr>";
		
	    $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Spent At:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$spentat."</td></tr>";
	
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Description:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$description."</td></tr>";

		
		
		$html .= "
		<tr >
		<td colspan=2>
		<center>
		<table cellspacing='0' cellpadding='0'>
		<tr>
		<td style='border-radius: 2px; padding: 8px 8px;' bgcolor='#ffffff'>
		<a href='http://one.edupristine.com/process_modtrans.php?id=&type=approve' target='_blank' style='padding: 8px 12px; border: 1px solid #000000;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #000000;text-decoration: none;font-weight:bold;display: inline-block;'>
		Approve             
		</a>
		</td>
		<td style='width:10px;'></td>
		<td style='border-radius: 2px; padding: 8px 8px;' bgcolor='#ffffff'>
		<a href='http://one.edupristine.com/process_modtrans.php?id=&type=reject' target='_blank' style='padding: 8px 12px; border: 1px solid #000000;border-radius: 2px;font-family: Helvetica, Arial, sans-serif;font-size: 14px; color: #000000;text-decoration: none;font-weight:bold;display: inline-block;'>
		Reject             
		</a>
		</td>
		</tr>
		</table>
		</center>
		</td>
		</tr>
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Request for reimbursement Details";
		if($just_save==1)
			{
		send_smt_mail($subject,$html);
			}
		
	
	exit;
}
else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}

function send_smt_mail($subject,$email_msg){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Reimbersement Notification');
		$mail->addAddress('hitesh.patil@edupristine.com', 'Hitesh Patil');

	
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}

function dateName($date) {

	$result = "";

	$convert_date = strtotime($date);
	$month = date('F',$convert_date);
	$year = date('Y',$convert_date);
	$name_day = date('l',$convert_date);
	$day = date('j',$convert_date);


	$result = $month . " " . $day . ", " . $year;

	return $result;
}

?>