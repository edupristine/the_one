<?php 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/connection.php';
// Details save file

if (isset($_POST['expense_id']) && !empty($_POST['expense_id'])) {
	
	$usertype  = $_POST['usertype'];
	$exp_type  = $_POST['exp_type'];
	$amount    = $_POST['amount'];
	$currency  = $_POST['currency'];
	$wallettype    = $_POST['wallettype'];
	$spentat  = $_POST['spentat'];
	$description    = $_POST['description'];
	$city  = $_POST['city'];
	$distance  = $_POST['distance'];
	$rate_per_km  = $_POST['rate_per_km'];
	$type_of_vehicle  = $_POST['type_of_vehicle'];
	$expense_id  = $_POST['expense_id'];
	$case_folder_name  = $_POST['case_folder_name'];
	
		

	$exp_category    = $_POST['exp_category'];
	$raw_date_of_expense    = explode("/",$_POST['date_of_expense']);
	$date_of_expense    = $raw_date_of_expense[2]."-".$raw_date_of_expense[0]."-".$raw_date_of_expense[1];
	
	$time_of_expense    = $_POST['time_of_expense'];
	$multi_day    = $_POST['multi_day'];
	
	$raw_start    = explode("/",$_POST['start']);
	$start    = $raw_start[2]."-".$raw_start[0]."-".$raw_start[1];
	
	$raw_end    = explode("/",$_POST['end']);
	$end    = $raw_end[2]."-".$raw_end[0]."-".$raw_end[1];



	$update_rec = "UPDATE expense_records SET 
	exp_type='".$exp_type."', 
	amount='".$amount."', 
	currency='".$currency."', 
	wallettype='".$wallettype."', 
	spentat='".$spentat."', 
	description='".$description."', 
	city='".$city."', 
	exp_category='".$exp_category."', 
	occurance_date='".$date_of_expense."', 
	occurance_time='".$time_of_expense."', 
	from_date='".$start."', 
	to_date='".$end."', 
	is_multiday='".$multi_day."',
	type_of_vehicle='".$type_of_vehicle."',
	distance='".$distance."',
	rate_per_km='".$rate_per_km."' 
	WHERE ID=".$expense_id; 
	
	
	//echo $update_rec; 
	$result_id = Select($update_rec, $conn_exp);
	$rec_id  = $expense_id;
			
			
	

	
if(!empty($_FILES)){
if($case_folder_name!='')
	{	
    $case_folder_name_raw=explode("_",$case_folder_name);
	// File path configuration
	$timestamp = $case_folder_name_raw[2];
	$folder_gen=$case_folder_name;
	}
	else
	{
	 // File path configuration
	$timestamp = time();
	$folder_gen="EXP_CASE_".$timestamp;
	}
   
	
	
    $uploadDir = $_SERVER['DOCUMENT_ROOT'] ."/expenses/uploads/".$folder_gen."/"; 
 
	if (!file_exists($uploadDir)) {
	mkdir($uploadDir, 0777, true);
	}

    $fileName = $timestamp."_".basename($_FILES['file']['name']); 
    $uploadFilePath = $uploadDir.$fileName; 

    // Upload file to server 
    if(move_uploaded_file($_FILES['file']['tmp_name'], $uploadFilePath)){ 
	    chmod($uploadFilePath, 0777);
        // Insert file information in the database 
        $insert_file = "INSERT INTO files (exp_id,file_name,folder_name, uploaded_on) VALUES ('".$rec_id."','".$fileName."','".$folder_gen."', NOW())"; 
		$result = Select($insert_file, $conn_exp);
    } 
	
}

	$response = "record_created?id=".$_POST['expense_id'];
	$response = json_encode($response);
	echo $response;
	exit;
}
else {
	$response = "blank";
	$response = json_encode($response);
	echo $response;
	exit;
}


?>