<?php

?>
	<!-- begin:: Footer -->
					<div class="kt-footer kt-grid__item" id="kt_footer">
						<div class="kt-container">
							<div class="kt-footer__bottom">
								<div class="kt-footer__copyright">
									<?php echo date('Y');?>&nbsp;&copy;&nbsp;<a href="https://www.edupristine.com" target="_blank" class="kt-link">EduPristine</a>
								</div>
								<div class="kt-footer__menu">
									<!--<a href="#" target="_blank" class="kt-link">About</a>
									<a href="#" target="_blank" class="kt-link">Team</a>
									<a href="#" target="_blank" class="kt-link">Contact</a>-->
								</div>
							</div>
						</div>
					</div>

    <!-- end:: Footer -->
<?php

?>