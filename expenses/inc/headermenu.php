<?php
$display_head='Welcome User!';
if($_SESSION['USER_TYPE']=="Emp")
{
	$display_head='Employee';
}
else if($_SESSION['USER_TYPE']=="Pre")
{
	$display_head='Privileged User';	
}

?>
					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
						<div class="kt-header__top">
							<div class="kt-container">

								<!-- begin:: Brand -->
								<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
									<div class="kt-header__brand-logo">
										<a href="expenses/dashboard.php">
											<img alt="Logo" src="/assets/media/logos/logo-light.png" class="kt-header__brand-logo-default" />
										</a>
									</div>
									<div class="kt-header__brand-nav">
										<div class="dropdown">
											<button type="button" class="btn btn-pill btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												What You like to do?
											</button>
											<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md">
												<ul class="kt-nav kt-nav--bold kt-nav--md-space kt-margin-t-20 kt-margin-b-20">
												    <?php
                                                    if($_SESSION['USER_TYPE']=='Emp')
													{														
												    ?>
													<li class="kt-nav__item">
													    <a class="kt-nav__link active" href="expenses/add_expense.php">
															<span class="kt-nav__link-icon"><i class="flaticon-business"></i></span>
															<span class="kt-nav__link-text">Add New Expenses</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a class="kt-nav__link" href="expenses/create_report.php" >
															<span class="kt-nav__link-icon"><i class="flaticon-folder-1"></i></span>
															<span class="kt-nav__link-text">Add New Report</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a class="kt-nav__link" href="expenses/receive_cash.php" >
															<span class="kt-nav__link-icon"><i class="flaticon-notepad"></i></span>
															<span class="kt-nav__link-text">Cash/Cheque Acknowledgement</span>
														</a>
													</li>
													<li class="kt-nav__separator"></li>
													<?php
						                            }
													?>
				
													
													
													<li class="kt-nav__item">
														<a class="kt-nav__link" href="expenses/emp_report.php">
															<span class="kt-nav__link-icon"><i class="flaticon-coins"></i></span>
															<span class="kt-nav__link-text">Expenses Records</span>
														</a>
													</li>
													
													<li class="kt-nav__item">
														<a class="kt-nav__link" href="expenses/emp_report_records.php">
															<span class="kt-nav__link-icon"><i class="flaticon-interface-7"></i></span>
															<span class="kt-nav__link-text">Reports Records</span>
														</a>
													</li>
												
													
													
												</ul>
											</div>
										</div>
									</div>
									<div style="width:800px;"> <marquee>Welcome to EduPristine Expenses & Cash Management Portal!.<!-- <font style="color:#ff0000"><b>Due to Maintenance, Kindly do not access any Module until further instructions</b></font>-->Kindly clear the cache of your browser once before using this application. For clearing cache, use command keys (CTRL+SHIFT+R). </marquee></div>
									
								</div>

								<!-- end:: Brand -->

								<!-- begin:: Header Topbar -->
								<div class="kt-header__topbar kt-grid__item kt-grid__item--fluid">
                                   
									<!--begin: Search -->
									<!--<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
										<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
											<span class="kt-header__topbar-icon kt-header__topbar-icon--brand"><i class="flaticon2-search-1"></i></span>
										</div>
										<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
											<div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
												<form method="get" class="kt-quick-search__form">
													<div class="input-group">
														<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
														<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
														<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
													</div>
												</form>
												<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
												</div>
											</div>
										</div>
									</div>-->

									<!--end: Search -->

									<!--begin: Notifications -->
									<!--<div class="kt-header__topbar-item dropdown">
										<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
											<span class="kt-header__topbar-icon kt-header__topbar-icon--success"><i class="flaticon2-bell-alarm-symbol"></i></span>
											<span class="kt-hidden kt-badge kt-badge--danger"></span>
										</div>
										<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
											<form>

												
												<div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
													<h3 class="kt-head__title">
														User Notifications
														&nbsp;
														<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 new</span>
													</h3>
													<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
														<li class="nav-item">
															<a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#topbar_notifications_events" role="tab" aria-selected="false">Events</a>
														</li>
														<li class="nav-item">
															<a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Logs</a>
														</li>
													</ul>
												</div>

												
												<div class="tab-content">
													<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
														<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-line-chart kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been received
																	</div>
																	<div class="kt-notification__item-time">
																		2 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-box-1 kt-font-brand"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-chart2 kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Application has been approved
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-image-file kt-font-warning"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New file has been uploaded
																	</div>
																	<div class="kt-notification__item-time">
																		5 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-bar-chart kt-font-info"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New user feedback received
																	</div>
																	<div class="kt-notification__item-time">
																		8 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-pie-chart-2 kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		System reboot has been successfully completed
																	</div>
																	<div class="kt-notification__item-time">
																		12 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-favourite kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been placed
																	</div>
																	<div class="kt-notification__item-time">
																		15 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item kt-notification__item--read">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-safe kt-font-primary"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Company meeting canceled
																	</div>
																	<div class="kt-notification__item-time">
																		19 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-psd kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New report has been received
																	</div>
																	<div class="kt-notification__item-time">
																		23 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon-download-1 kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Finance report has been generated
																	</div>
																	<div class="kt-notification__item-time">
																		25 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon-security kt-font-warning"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer comment recieved
																	</div>
																	<div class="kt-notification__item-time">
																		2 days ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-pie-chart kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 days ago
																	</div>
																</div>
															</a>
														</div>
													</div>
													<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
														<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-psd kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New report has been received
																	</div>
																	<div class="kt-notification__item-time">
																		23 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon-download-1 kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Finance report has been generated
																	</div>
																	<div class="kt-notification__item-time">
																		25 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-line-chart kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been received
																	</div>
																	<div class="kt-notification__item-time">
																		2 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-box-1 kt-font-brand"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-chart2 kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Application has been approved
																	</div>
																	<div class="kt-notification__item-time">
																		3 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-image-file kt-font-warning"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New file has been uploaded
																	</div>
																	<div class="kt-notification__item-time">
																		5 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-bar-chart kt-font-info"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New user feedback received
																	</div>
																	<div class="kt-notification__item-time">
																		8 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-pie-chart-2 kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		System reboot has been successfully completed
																	</div>
																	<div class="kt-notification__item-time">
																		12 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-favourite kt-font-brand"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New order has been placed
																	</div>
																	<div class="kt-notification__item-time">
																		15 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item kt-notification__item--read">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-safe kt-font-primary"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Company meeting canceled
																	</div>
																	<div class="kt-notification__item-time">
																		19 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-psd kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New report has been received
																	</div>
																	<div class="kt-notification__item-time">
																		23 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon-download-1 kt-font-danger"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		Finance report has been generated
																	</div>
																	<div class="kt-notification__item-time">
																		25 hrs ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon-security kt-font-warning"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer comment recieved
																	</div>
																	<div class="kt-notification__item-time">
																		2 days ago
																	</div>
																</div>
															</a>
															<a href="#" class="kt-notification__item">
																<div class="kt-notification__item-icon">
																	<i class="flaticon2-pie-chart kt-font-success"></i>
																</div>
																<div class="kt-notification__item-details">
																	<div class="kt-notification__item-title">
																		New customer is registered
																	</div>
																	<div class="kt-notification__item-time">
																		3 days ago
																	</div>
																</div>
															</a>
														</div>
													</div>
													<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
														<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
															<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
																<div class="kt-grid__item kt-grid__item--middle kt-align-center">
																	All caught up!
																	<br>No new notifications.
																</div>
															</div>
														</div>
													</div>
												</div>
											</form>
										</div>
									</div>-->

									<!--end: Notifications -->




									<!--begin: Quick panel toggler -->
									<!--<div class="kt-header__topbar-item" data-toggle="kt-tooltip" title="Quick panel" data-placement="top">
										<div class="kt-header__topbar-wrapper">
											<span class="kt-header__topbar-icon kt-header__topbar-icon--warning" id="kt_quick_panel_toggler_btn"><i class="flaticon2-gear"></i></span>
										</div>
									</div>-->

									<!--end: Quick panel toggler -->



									<!--begin: User bar -->
									<div class="kt-header__topbar-item kt-header__topbar-item--user">
										<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
											<span class="kt-hidden kt-header__topbar-welcome">Hi,</span>
											<span class="kt-hidden kt-header__topbar-username"><?php echo $_SESSION['U_NAME']; ?><br>
											
											</span>
											<img class="kt-hidden-" alt="Pic" src="/assets/media/users/default.jpg" />
											<span class="kt-header__topbar-icon kt-header__topbar-icon--brand kt-hidden"><b>S</b></span>
										</div>
										<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

											<!--begin: Head -->
											<div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
												<div class="kt-user-card__avatar">
													<img class="kt-hidden-" alt="Pic" src="/assets/media/users/default.jpg" />

													<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
													<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden"><?php echo substr($_SESSION['U_NAME'],0,1); ?></span>
												</div>
												<div class="kt-user-card__name">
													<?php echo $_SESSION['U_NAME']; ?>
														<?php
															     if($_SESSION['USER_TYPE']=='Emp')
																{														
																?>
																(Employee)
																<?php
																}
																if($_SESSION['USER_TYPE']=='Pre')
																{														
																?>
																(Privileged User)
																<?php
																}?>
												</div>
												<div class="kt-user-card__badge">
													<span class="btn btn-label-primary btn-sm btn-bold btn-font-md"><?php echo $_SESSION['U_LOCATION_NAME']; ?></span>
												</div>
											</div>

											<!--end: Head -->

											<!--begin: Navigation -->
											<div class="kt-notification">
												<div class="kt-notification__custom kt-space-between">
													<a href="expenses/logout.php" class="btn btn-label btn-label-brand btn-sm btn-bold">Sign Out</a>
													<a href="expenses/update_password.php"  class="btn btn-clean btn-sm btn-bold">Change Password</a>
												</div>
											</div>

											<!--end: Navigation -->
										</div>
									</div>

									<!--end: User bar -->
								</div>

								<!-- end:: Header Topbar -->
							</div>
						</div>
						
				

					<!-- end:: Header -->
<?php
?>		
