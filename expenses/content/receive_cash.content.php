<?php
//Categories
$exp_cities = "SELECT * from cities order by city_name";
$result_exp_city = Select($exp_cities,$conn_exp);

$base_id  = $_GET['capture_id'];
if($base_id!='')
{
$select_rec = "SELECT * FROM cash_acknowledgement WHERE id=".$base_id; 
$result_report = Select($select_rec, $conn_exp);
$main_id=$result_report['rows'][0]['id'];
$uq_recpt_no=$result_report['rows'][0]['uq_recpt_no'];
$payment_mode=$result_report['rows'][0]['payment_mode'];
$student_name=$result_report['rows'][0]['student_name'];
$amount=$result_report['rows'][0]['amount'];
$course=$result_report['rows'][0]['course'];
$email_id=$result_report['rows'][0]['email_id'];
$contact_no=$result_report['rows'][0]['contact_no'];
$payment_reason=$result_report['rows'][0]['payment_reason'];
if($result_report['rows'][0]['bank_slip']!="")
{
$bank_slip=$result_report['rows'][0]['bank_slip'];
}
else
{
$bank_slip="NA";	
}
$rep_desc=$result_report['rows'][0]['description'];
$receiver_name=$result_report['rows'][0]['receiver_name'];
$course=$result_report['rows'][0]['student_course'];
}

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
<center>
<style type="text/css" media="screen"></style>

<style type="text/css" media="print">
@media print {
@page {
    /* dimensions for the whole page */
    size: A4 potrait;
    
    margin: 0;
}

html {
    /* off-white, so body edge is visible in browser */
    background: #eee;
}

body {
    /* A5 dimensions */
    height: 297mm;
    width: 210mm;

    margin: 0;
}


}
</style>

                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Receive Cash / Cheque From Student</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										
										<a href="expenses/dashboard.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Go Back To Dashboard
										</a>
									</div>
									
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->
										<?php
										// Get status message
										if(!empty($_GET['status'])){
										switch($_GET['status']){
										case 'succ':
										$statusType = 'alert-success';
										$statusMsg = 'Cash Acknowledgement created successfully.';
										$altype = "success";
										break;
										case 'err':
										$statusType = 'alert-danger';
										$statusMsg = 'Some problem occurred, please try again.';
										$altype = "danger";
										break;
										case 'invalid_file':
										$statusType = 'alert-danger';
										$statusMsg = 'Please upload a valid CSV file.';
										$altype = "danger";
										break;
										default:
										$statusType = '';
										$statusMsg = '';
										}
										?>
										<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
										<div class="alert-icon"><i class="<?php echo $statusType; ?>"></i></div>
										<div class="alert-text"><?php echo $statusMsg; ?></div>
										<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
										</div>
										</div>
										<?php } ?>

									<!--Begin::Section-->
									<div class="row">
										<div class="col-xl-8">
											<!--begin::Portlet-->
											<div class="kt-portlet">
												<div class="kt-portlet__head">
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
															Create New Cash / Cheque Acknowledgement
														</h3>
													</div>
												</div>

												<!--begin::Form-->
												<form class="kt-form kt-form--label-right" enctype="multipart/form-data" action="expenses/form_handlers/receive_cash.php" method="post" >
													<div class="kt-portlet__body">
													     
														 
												<div class="form-group row">
															
															<div class="col-lg-6">
															<label>Student Name:</label>
															<input type="text" id="student_name" name="student_name" class="form-control" placeholder="Student Name" required>
															<span class="form-text text-muted">Please enter proper student name</span>
															</div>
															
															<div class="col-lg-6">
																<label>Select Course opted:</label>
																<select id="course" name="course" class="form-control">
																<option value="ACCA">ACCA</option>
																<option value="BAT">BAT</option>
																<option value="CPA">CPA</option>
																<option value="CMA">CMA</option>
																<option value="CFA">CFA</option>
																<option value="DM">DM</option>
																<option value="FM">FM</option>
																<option value="FRM">FRM</option>
																</select>
																<span class="form-text text-muted">Please select course opted by student</span>
															</div>
												</div>
												
												
												<div class="form-group row">
															
															<div class="col-lg-6">
															<label>Email Id:</label>
															<input type="text" id="email_id" name="email_id" class="form-control" placeholder="Student Email Id" required>
															<span class="form-text text-muted">Please enter student valid email id</span>
															</div>
															
															<div class="col-lg-6">
															<label>Contact No:</label>
															<input type="number" id="contact_no" name="contact_no" class="form-control" placeholder="Student contact no." maxlength=10  required>
															<span class="form-text text-muted">Please enter student valid Contact No.</span>	
															</div>
												</div>
												
												<div class="form-group row">
															
															<div class="col-lg-6">
															<label>Amount Received:</label>
															<input type="number" id="amount" name="amount" class="form-control" placeholder="Amount Recieved" required>
															<span class="form-text text-muted">Please enter amount received from student</span>
															</div>
															
															<div class="col-lg-6">
															<label>Received By:</label>
															<input type="text" id="receiver_name" name="receiver_name" class="form-control" placeholder="Enter recieved by" value="<?php echo $_SESSION['U_NAME'];?>" required>
															<span class="form-text text-muted">Please enter who recieved the cash.</span>	
															</div>
												</div>
												
												<div class="form-group row">
															
															<div class="col-lg-6">
																<label>Select Payement Reason:</label>
																<select id="payment_reason" name="payment_reason" class="form-control">
																<option value="New Registration">New Registration</option>
																<option value="Balance Payment">Balance Payment</option>
																<option value="Material">Material</option>
																<option value="Upgradation of Course">Upgradation of Course</option>
																<option value="Others">Others</option>
															    </select>
																<span class="form-text text-muted">Specify payment reason</span>
															</div>
															
															<div class="col-lg-6">
																<label>Select Payement Mode:</label>
																<select id="payment_mode" name="payment_mode" class="form-control">
																<option value="Cash">Cash</option>
																<option value="Cheque">Cheque</option>
																</select>
																<span class="form-text text-muted">Specify payment mode</span>
															</div>
												</div>
														
														<div class="form-group row">
															
															
															<div class="col-lg-12">
																<label class="">Description:</label>
																<textarea id="rep_desc" name="rep_desc" class="form-control"  placeholder="Comments if Any"></textarea>
																<span class="form-text text-muted">Tell us something more about the Cash taken</span>
															</div>
															
														</div>
													
													
													     
													
														</div>
														</div>
														
														
														
													
													<div class="kt-portlet__foot">
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-lg-12 kt-align-center">
																	<button type="submit" id="next" name="next" class="btn btn-primary">Save</button>
																	<button type="reset" class="btn btn-secondary">Cancel</button>
																</div>
																
															</div>
														</div>
													</div>
												</form>
                                                </div>
												<!--end::Form-->

									    </div>

									<!--end::Portlet-->
								<?php if($base_id=="Notapp")
								{
								?>
								<div class="form-group row"style="padding-top:50px;">
								
								
									<div class="col-lg-12">
									<center><button onclick="printElem('print_area2')">Print</button></center>
									</div>
									
									<section id="print_area2" class="sheet padding-10mm ">

									<!-- Write HTML just like a web page -->

									<table style="width:100%;border:1px;">
									<tr>
									<td style="font-size:14px;text-align:left;width:100%;height:160px;vertical-align:top;">
									<?php

									$html .= "
									<p>

									<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 20px;'>
									<tr>
									<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; background-color: #337AB7; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/logo.png'  height='60px;'/></b></td>
									</tr>
									<tr>
									<td colspan=2 style='border: 1px solid #000000; color:#000000; padding: 12px 12px; background-color: #ffffff; text-align:left; font-size: 20px;width:100%;'>
									<p><center><strong>PROVISIONAL ".strtoupper($payment_mode)." ACKNOWLEDGEMENT</strong></center></p></td></tr>
									<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'><b>
									Created by:</b></td>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:left; font-size: 20px;width:70%;'><b>".$_SESSION['U_NAME']."</b></td>
									</tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Recepient Location:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$_SESSION['U_LOCATION_NAME']."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Student Name:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$student_name."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Amount Received:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>INR 
									".$amount."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Course:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$course."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Email Id:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$email_id."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Contact No:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'> 
									".$contact_no."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Receiver Name:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$receiver_name."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Payment Reason:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$payment_reason."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Received On:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".date('d-M-Y')."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Unique record Id:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$uq_recpt_no."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Center Address:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$_SESSION['U_LOCATION_ADD']."</td></tr>";

									$html .= "<tr>
									<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 20px;width:30%;'>Comments:</td>
									<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 20px;width:70%;'>
									".$rep_desc."</td></tr>";







									$html .= "
									</table>";
									echo $html; 
									?>
									</td>
									</tr>
									<tr>
									<td>
									<center>This is computer generated receipt. Signature not required.</center>
									</td>
									</tr>
									</table>


									</section>
							
								</div>
								<?php
								}
								?>
								<!--
								<div class="row">
								<div class="col-xl-8">	
								<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											 Bulk Uploaded Expenses
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-download"></i> Export
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="kt-nav">
															<li class="kt-nav__section kt-nav__section--first">
																<span class="kt-nav__section-text">Choose an option</span>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-print"></i>
																	<span class="kt-nav__link-text">Print</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-copy"></i>
																	<span class="kt-nav__link-text">Copy</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-excel-o"></i>
																	<span class="kt-nav__link-text">Excel</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-text-o"></i>
																	<span class="kt-nav__link-text">CSV</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																	<span class="kt-nav__link-text">PDF</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
												&nbsp;
												<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													New Record
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

								
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>Record ID</th>
												<th>Order ID</th>
												<th>Country</th>
												<th>Ship City</th>
												<th>Ship Address</th>
												<th>Company Agent</th>
												<th>Company Name</th>
												<th>Ship Date</th>
												<th>Status</th>
												<th>Type</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>61715-075</td>
												<td>China</td>
												<td>Tieba</td>
												<td>746 Pine View Junction</td>
												<td>Nixie Sailor</td>
												<td>Gleichner, Ziemann and Gutkowski</td>
												<td>2/12/2018</td>
												<td>3</td>
												<td>2</td>
												<td nowrap></td>
											</tr>
										
										</tbody>
									</table>

								
								</div>
							</div>  
							    </div></div>-->
								</div>

									</div>

									<!--End::Section-->



									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
							
							
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->

  
  
<script>
function printElem(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
document.body.innerHTML = originalContents;
return true;
}
</script>