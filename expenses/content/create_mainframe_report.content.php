<?php
$usertype = $_SESSION['USER_TYPE'];
$report_name  = $_POST['report_name'];
$wallettype  = $_POST['wallettype'];
$rep_desc    = $_POST['rep_desc'];
$timestamp = time();
$generic_report_id="EDRP"."-".date('Ymd')."-".$timestamp;

$insert_rec = "INSERT INTO expense_report (report_name,wallettype, rep_desc, exp_report_id, created_on, created_by, user_type,is_saved,saved_on,location ) VALUES ('".$report_name."','".$wallettype."','".$rep_desc."','".$generic_report_id."', NOW(), '".$_SESSION['USER_ID']."','".$usertype."',1,NOW(),'".$_SESSION['U_LOCATION_ID']."')"; 
$result_id = Select($insert_rec, $conn_exp);
$rec_id  = $conn_exp->lastInsertId();

//echo $insert_rec;

?>


                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Report</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										
										<a href="expenses/dashboard.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Go Back To Dashboard
										</a>
										
										<a href="expenses/add_expense.php?id=<?php echo $rec_id;?>&wt=<?php echo $wallettype;?>" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Add New Expense
										</a>
										
										<a href="expenses/existing_emp_report.php?id=<?php echo $rec_id;?>&wt=<?php echo $wallettype;?>" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Add Existing Expenses
										</a>
									</div>
									
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->
										<?php
										// Get status message
										if(!empty($_GET['status'])){
										switch($_GET['status']){
										case 'succ':
										$statusType = 'alert-success';
										$statusMsg = 'Expenses data has been imported successfully.';
										$altype = "success";
										break;
										case 'err':
										$statusType = 'alert-danger';
										$statusMsg = 'Some problem occurred, please try again.';
										$altype = "danger";
										break;
										case 'invalid_file':
										$statusType = 'alert-danger';
										$statusMsg = 'Please upload a valid CSV file.';
										$altype = "danger";
										break;
										default:
										$statusType = '';
										$statusMsg = '';
										}
										?>
										<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
										<div class="alert-icon"><i class="<?php echo $statusType; ?>"></i></div>
										<div class="alert-text"><?php echo $statusMsg; ?></div>
										<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
										</div>
										</div>
										<?php } ?>

									<!--Begin::Section-->
									<div class="row">
										<div class="col-xl-8">
											<!--begin::Portlet-->
											<div class="kt-portlet">
												<div class="kt-portlet__head">
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
															EDIT REPORT (ID: <?php echo $generic_report_id;?>)
														</h3>
													</div>
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
														
														</h3>
													</div>
												</div>

												<!--begin::Form-->
												<form class="kt-form kt-form--label-right" enctype="multipart/form-data" action="expenses/edit_mainframe_report.php?id=<?php echo $rec_id;?>" method="post" >
													<div class="kt-portlet__body">
													     
														 
													<div class="form-group row" style='background-color:#f0f0f0;'>

													<div class="col-lg-11">
													<label><strong><?php echo $report_name;?></strong></label></br>
													<label>Description: <strong><?php echo $rep_desc ;?></strong></label></br>
													<label>Wallet: <strong><?php echo $wallettype;?></strong></label></br>
													</div>

													<div class="col-lg-1" style="align:right;margin-top:20px;">
                                                    <a href="expenses/edit_created_report.php?id=<?php echo $rec_id;?>" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
														EDIT
													</a>
													</div>
													</div>
														
													
													
													     
													
														</div>
														</div>
														
														
														
													
													<div class="kt-portlet__foot">
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-lg-12 kt-align-center">
																	<button type="submit" id="next" name="next" class="btn btn-primary">Next</button>
																	<a href="expenses/dashboard.php" class="btn btn-secondary">
																	Cancel</a>
																</div>
																
															</div>
														</div>
													</div>
												</form>
                                                </div>
												<!--end::Form-->
									    </div>

									<!--end::Portlet-->
								<div class="row" style="padding-top:50px;">
								<div class="col-xl-8">
								</div>
								</div>
								<!--
								<div class="row">
								<div class="col-xl-8">	
								<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											 Bulk Uploaded Expenses
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-download"></i> Export
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="kt-nav">
															<li class="kt-nav__section kt-nav__section--first">
																<span class="kt-nav__section-text">Choose an option</span>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-print"></i>
																	<span class="kt-nav__link-text">Print</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-copy"></i>
																	<span class="kt-nav__link-text">Copy</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-excel-o"></i>
																	<span class="kt-nav__link-text">Excel</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-text-o"></i>
																	<span class="kt-nav__link-text">CSV</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																	<span class="kt-nav__link-text">PDF</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
												&nbsp;
												<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													New Record
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

								
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>Record ID</th>
												<th>Order ID</th>
												<th>Country</th>
												<th>Ship City</th>
												<th>Ship Address</th>
												<th>Company Agent</th>
												<th>Company Name</th>
												<th>Ship Date</th>
												<th>Status</th>
												<th>Type</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>61715-075</td>
												<td>China</td>
												<td>Tieba</td>
												<td>746 Pine View Junction</td>
												<td>Nixie Sailor</td>
												<td>Gleichner, Ziemann and Gutkowski</td>
												<td>2/12/2018</td>
												<td>3</td>
												<td>2</td>
												<td nowrap></td>
											</tr>
										
										</tbody>
									</table>

								
								</div>
							</div>  
							    </div></div>-->
								</div>

									</div>

									<!--End::Section-->



									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
							
							
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->
