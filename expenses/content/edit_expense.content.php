<?php
$expense_record_id="";

if (isset($_GET['id'])) {
	$get_exp_id = "SELECT * from expense_records where id=".$_GET['id']."";
	$result_get_exp_id = Select($get_exp_id,$conn_exp);
    $exp_type=$result_get_exp_id['rows'][0]['exp_type'];
	$report_id=$result_get_exp_id['rows'][0]['report_id'];
	$amount=$result_get_exp_id['rows'][0]['amount'];
	$currency=$result_get_exp_id['rows'][0]['currency'];
	$wallettype=$result_get_exp_id['rows'][0]['wallettype'];
	$spentat=$result_get_exp_id['rows'][0]['spentat'];
	$description=$result_get_exp_id['rows'][0]['description'];
	$city=$result_get_exp_id['rows'][0]['city'];
	$exp_category_sel=$result_get_exp_id['rows'][0]['exp_category'];
	$occurance_date_raw=explode("-",$result_get_exp_id['rows'][0]['occurance_date']);
	$occurance_date=$occurance_date_raw[1]."/".$occurance_date_raw[2]."/".$occurance_date_raw[0];
}


//Categories
$exp_cities = "SELECT * from cities order by city_name";
$result_exp_city = Select($exp_cities,$conn_exp);

//Categories
$exp_category = "SELECT * from expense_categories";
$result_exp_category = Select($exp_category,$conn_exp);

?>


                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Expense</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										
										<a href="expenses/dashboard.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Go Back To Dashboard
										</a>
									</div>
									<div class="kt-subheader__toolbar">
										<div class="kt-subheader__wrapper">
											<a href="expenses/exp_bulk_upload.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-12">
											<i class="flaticon-paper-plane"></i> Bulk Upload 
										   </a>
										</div>
									</div>
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->
									<?php
									if(isset($_GET['dberror']))
									{
										$msg = "Expense Record Creation Failed. Unknown Error Contact Administrator.";
										$altype = "danger";
										$icontype = "flaticon2-cross";
									}
									elseif(isset($_GET['success']))
									{
										$msg = "Expense Details Updated Successfully.";
										$altype = "success";
										$icontype = "flaticon2-flaticon-suitcase";
									}
									else
									{
										$msg = "";
									}
									if($msg != '')
									{
									?>
									<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
										<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
										<div class="alert-text"><?php echo $msg; ?></div>
										<div class="alert-close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
											</button>
										</div>
									</div>
									<?php } ?>

									<!--Begin::Section-->
									<div class="row">
										<div class="col-xl-8">
											<!--begin::Portlet-->
											<div class="kt-portlet">
												<div class="kt-portlet__head">
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
															Edit Expense details
														</h3>
													</div>
												</div>

												<!--begin::Form-->
												<form class="kt-form kt-form--label-right" action="expenses/form_handlers/update_upload.php" class="dropzone">
													<div class="kt-portlet__body">
													     
														 
														<div class="form-group row">
														    <input type="hidden" name="usertype" id="usertype" value="<?php echo $_SESSION['USER_TYPE'];?>">
															<input type="hidden" name="expense_id" id="expense_id" value="<?php echo $_GET['id'];?>">
                                                          
															
															<div class="col-lg-12">
																<label>Select Expense Type:</label>
																<div class="kt-radio-inline">
																<?php 
																if($exp_type==1)
																{
																?>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="1" Checked> Cash
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="2"> Own Vehicle - Local Conveyance
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="3" > Petty Cash
																		<span></span>
																	</label>
																	<?php
																}else if($exp_type==2)
																{
																?>
																<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="1" > Cash
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="2" Checked> Own Vehicle - Local Conveyance
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="3" > Petty Cash
																		<span></span>
																	</label>
																<?php
																}else if($exp_type==3)
																{
																?>
																<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="1" > Cash
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="2" > Own Vehicle - Local Conveyance
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" class="main_exp" name="exp_type" id="exp_type" value="3" Checked> Petty Cash
																		<span></span>
																	</label>																
																
																<?php
																}
																?>
																</div>
																<span class="form-text text-muted">Please select your expense type</span>
															</div>
														</div> 
														<div id="vehicle_details" name="vehicle_details" style="display: none;">
														<div class="form-group row">
														
															<div class="col-lg-4">
															    <label class="">Type Of Vehicle:</label>
																<select id="type_of_vehicle" name="type_of_vehicle" class="form-control">
                                                                <option value="">Select</option>
																<option value="2 Wheeler">2 Wheeler</option>
																<option value="4 Wheeler">4 Wheeler</option>
																</select>
																<span class="form-text text-muted">Select Wheeler Type</span>
																
															</div>
															<div class="col-lg-4">
															    <label class="">Rate per Kilometre:</label>
																<input type="number" id="rate_per_km" name="rate_per_km" class="form-control" placeholder="Eg - 9" >
																<span class="form-text text-muted">Current rate per Kilometre</span>
																
															</div>
															<div class="col-lg-4">
															    <label class="">Distance:</label>
																<input type="number" id="distance" name="distance" class="form-control" placeholder="Eg - 140" >
																<span class="form-text text-muted">Distance covered in Kilometre </span>
																
															</div>
														</div>
														</div>
													
													     
														<div class="form-group row">
															
															<div class="col-lg-3">
															<label>Amount*:</label>
															<input type="number" id="amount" name="amount" class="form-control" placeholder="Eg - 100" value="<?php echo $amount;?>" required>
															<span class="form-text text-muted">Please enter amount</span>
															</div>
															<div class="col-lg-3">
																<label class="">Currency:</label>
																<input type="text" id="currency" name="currency" class="form-control" value="<?php echo $currency;?>" placeholder="INR - Indian rupee">
																<span class="form-text text-muted">Selected Currency</span>
															</div>
															<div class="col-lg-6">
																<label>Wallet:</label>
																<select id="wallettype" name="wallettype" class="form-control">
																<?php if($wallettype=="Imprest")
																{	
																?>
																<option value="Imprest" selected>Imprest</option>
																<option value="Reimbursement">Reimbursement</option>
																<?php
																}
																else if($wallettype=="Reimbursement")
																{
																?>
																<option value="Imprest" >Imprest</option>
																<option value="Reimbursement" selected>Reimbursement</option>
																<?php																
																}
																
																?>
																</select>
																<span class="form-text text-muted">Please select wallet type</span>
															</div>
														</div>
														
														
														
														<div class="form-group row">
														    <div class="col-lg-6">
																<label class="">Spent At*:</label>
																<input type="text" id="spentat" name="spentat" class="form-control" value="<?php echo $spentat;?>" placeholder="Eg - Starbucks" required>
																<span class="form-text text-muted">Enter where you spent</span>
															</div>
															<div class="col-lg-6">
																<label>Description:</label>
																<div class="kt-input-icon">
																	<input type="text" class="form-control" id="description" name="description" value="<?php echo $description;?>" placeholder="Tell us more">
																	
																</div>
																<span class="form-text text-muted">Please enter some description about your expense</span>
															</div>
														</div>
														
													
														<div class="form-group row">
														    <div class="col-lg-6">
																<label class="">City:</label>
																<div class="kt-input-icon">
																    <select id="city" name="city" class="form-control">
																	<?php
																	foreach($result_exp_city['rows'] as $exp_city)
																	{
																		
																		
                                                                    if(trim($city)==trim($exp_city['city_name']))	
																	{																		
																	?>
																	<option value="<?php echo trim($exp_city['city_name']);?>" selected><?php echo trim($exp_city['city_name']); ?></option>
																	
																	<?php
																	}
																	else
																	{
																	?>
																	<option value="<?php echo $exp_city['city_name']; ?>" ><?php echo $exp_city['city_name']; ?></option>
																	<?php	
																	}
																	}
																	?>
																	</select>
																
																	<span class="kt-input-icon__icon kt-input-icon__icon--right"><span><i class="la la-map-marker"></i></span></span>
																</div>
																<span class="form-text text-muted">Please enter city</span>
															</div>
															
															<div class="col-lg-6">
																<label class="">Category:</label>
																<div class="kt-input-icon">
																	<select id="exp_category" name="exp_category" class="form-control">
																	<?php
																	foreach($result_exp_category['rows'] as $exp_cat)
																	{
                                                                    if(trim($exp_category_sel)==trim($exp_cat['categories']))
																	{																		
																	?>
																	<option value="<?php echo trim($exp_cat['categories']);?>" selected><?php echo trim($exp_cat['categories']);?></option>
																	<?php
																	}else
																	{
																	?>
                                                                    <option value="<?php echo $exp_cat['categories']; ?>"><?php echo $exp_cat['categories']; ?></option>
																	<?php
																	}
																	}
																	?>
																	</select>
																	
																</div>
																<span class="form-text text-muted">Select Category</span>
															</div>
														</div>
														
														<div class="form-group row">
														    <div class="col-lg-2">
																<label class="">Date of Expense:</label>
												                <input type="text" class="form-control" id="kt_datepicker_1" name="kt_datepicker_1" readonly placeholder="Select date" value="<?php echo $occurance_date;?>" />
										                        <span class="form-text text-muted">Please select date of expense</span>
															</div>
															 <div class="col-lg-2">
																<label class="">Time of Expense:</label>
												               <input class="form-control" id="kt_timepicker_1" name="kt_timepicker_1" readonly="readonly" placeholder="Select time" type="text">
										                        <span class="form-text text-muted">Please select Time of expense</span>
															</div>
															
															<div class="col-lg-2">
																<label class="">Is it a MultiDay Expense?:</label>
																
																<div class="kt-radio-inline">
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" name="multi_day" id="multi_day" value="1" class="astro" > Yes
																		<span></span>
																	</label>
																	<label class="kt-radio kt-radio--solid">
																		<input type="radio" name="multi_day" id="multi_day" value="2" class="astro" checked> No
																		<span></span>
																	</label>
																</div>
															</div>	
															<div class="col-lg-6">
															
																<div id="multidate" name="multidate" style="display: none;">
																<label class="">From & To Date of Expense:</label>
																<div class="input-daterange input-group" id="kt_datepicker_5">
																<input type="text" class="form-control" id="start"   name="start" />
																<div class="input-group-append">
																<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
																</div>
																<input type="text" class="form-control" id="end" name="end" />
																</div>
																<span class="form-text text-muted">Select Date Range</span>
																
																</div>
															</div>
														</div>
														
														
															<div class="form-group row">
														    <div class="col-lg-12">
																<label class="">Attachments uploaded:</label>
																<table style="border:1px;">
																<tr style="background-color:#c0c0c0;color:#ffffff;">
																<td style="width:300px;border:1px;">File Name</td>
																<td style="width:100px;border:1px;">Remove</td>
																</tr>
																
																<?php
																$expense_records_files = "SELECT *  FROM files WHERE exp_id = '".$_GET['id']."'";
																$result_expense_files = Select($expense_records_files,$conn_exp);
																$case_folder_name="";
																foreach($result_expense_files['rows'] as $files_val)
																{
																$case_name=explode("_",$files_val['file_name']);
																$case_folder_name="EXP_CASE_".$case_name[0];
																?>
																<tr style="background-color:#f0f0f0;color:#000000;">
																<td><a href='http://one.edupristine.com/expenses/uploads/<?php echo "EXP_CASE_".$case_name[0]."/".$files_val['file_name'];?>' target=_blank><?php echo $files_val['file_name']?></a> </td>
																<td>
																<button type="button"  onclick="removefile(<?php echo $files_val['id'];?>);" class="btn btn-warning btn-elevate btn-pill btn-elevate-air btn-sm">Remove</button>
																</td>
																</tr>
																<?php
																}
																?>
																<input type="hidden" id="case_folder_name" name="case_folder_name" value="<?php echo $case_folder_name; ?>"/>
																</table>
															</div>
														</div>
														
														<div class="form-group row">
															<label class="col-form-label col-lg-12 col-sm-12" style="text-align:left;">Select File/s to Upload</label>
															<div class="col-lg-12 col-md-9 col-sm-12">
																<div class="kt-dropzone dropzone m-dropzone--primary" action="expenses/form_handlers/upload.php" id="m-dropzone-two">
																	<div class="kt-dropzone__msg dz-message needsclick">
																		<h3 class="kt-dropzone__msg-title">Drop files here or click to upload.</h3>
																		<span class="kt-dropzone__msg-desc">Upload up to 25 files Max</span>
																	</div>
																</div>
															</div>
														</div>
														
														
														
														
													</div>
													<div class="kt-portlet__foot">
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-lg-12 kt-align-center">
																	<button type="button" id="startUpload" class="btn btn-primary">Update Details</button>
																	<a href="expenses/emp_report.php" class="btn btn-secondary">
																	Cancel</a>
																</div>
																
															</div>
														</div>
													</div>
												</form>

												<!--end::Form-->
											</div>

									<!--end::Portlet-->
										</div>

									</div>

									<!--End::Section-->



									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->
<script src="expenses/js/jquery.min.js"></script>		
<script type="text/javascript" src="expenses/js/dropzone/dropzone.js"></script>
<script src="expenses/js/jquery.validate.min.js"></script>
<script>
//Disabling autoDiscover
Dropzone.autoDiscover = false;

$(function() {
    //Dropzone class
    var myDropzone = new Dropzone(".dropzone", {
        url: "expenses/form_handlers/update_upload.php",
        paramName: "file",
        maxFilesize: 50,
        maxFiles: 25,
        acceptedFiles: "image/*,application/pdf",
        autoProcessQueue: false
    });
	
	
    
    $('#startUpload').click(function(){  
		myDropzone.on("sending", function(file, xhr, formData) { 
		formData.append("usertype", $('#usertype').val());
        formData.append("exp_type", $('input[name=exp_type]:checked').val()); 
        formData.append("amount", $('#amount').val());  
        formData.append("currency", $('#currency').val());
        formData.append("wallettype", $('#wallettype').val());
        formData.append("spentat", $('#spentat').val()); 
        formData.append("description", $('#description').val()); 
		formData.append("date_of_expense", $('#kt_datepicker_1').val()); 
		formData.append("time_of_expense", $('#kt_timepicker_1').val()); 
		formData.append("start", $('#start').val()); 
		formData.append("end", $('#end').val()); 
        formData.append("city", $('#city').val());  
        formData.append("exp_category", $('#exp_category').val());
        formData.append("multi_day", $('input[name=multi_day]:checked').val());
        formData.append("type_of_vehicle", $('#type_of_vehicle').val());
		formData.append("rate_per_km", $('#rate_per_km').val());
		formData.append("distance", $('#distance').val());
		formData.append("expense_id", $('#expense_id').val());	
        formData.append("case_folder_name", $('#case_folder_name').val());
		
        });
        myDropzone.processQueue();
		
		// alert($('#kt_datepicker_1').val());
		// alert($('#end').val());
		// alert($('#start').val());
		
		myDropzone.on('success', function(file, response){
                $("#loading").hide();
                 localStorage.setItem("message", response['message']);
                 window.location = 'expenses/edit_expense.php?success';
        });
		
		
    });
	
	
  $(".astro").change(function(){
        var val = $(".astro:checked").val();
        if(val==1)
		{
			$('#multidate').show();
			$('#end').val('');
		    $('#start').val('');
			
		}
		else if(val==2)
		{
			$('#multidate').hide();
			$('#end').val('');
		    $('#start').val('');
		}
		else
		{
			$('#multidate').hide();
			$('#end').val('');
		    $('#start').val('');
		}
    });
	
	
	  $(".main_exp").change(function(){
        var val = $(".main_exp:checked").val();
        if(val==1)
		{
			$('#vehicle_details').hide();
			$('#type_of_vehicle').val('');
		    $('#rate_per_km').val('');
			$('#distance').val('');
			
		}
		else if(val==2)
		{
			$('#vehicle_details').show();
			$('#type_of_vehicle').val('');
		    $('#rate_per_km').val('');
			$('#distance').val('');
		}
		else if(val==3)
		{
			$('#vehicle_details').hide();
			$('#type_of_vehicle').val('');
		    $('#rate_per_km').val('');
		    $('#distance').val('');
		}
    });
	
});

function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

function removefile(file_id)
{
  if (confirm("Are you sure you want to remove this file from current expense?") == true) {
    
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("File removed Successfully.");
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Expense related file removal Failed. Contact Administrator");
				
			}
			
			else
			{
				alert("Expense related file removal Failed. Contact Administrator2");
			
			}
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/remove_file.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('file_id='+fixEscape(file_id));
	
  } else {
   
  }
	
}
</script>	
