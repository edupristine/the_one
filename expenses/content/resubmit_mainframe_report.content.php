<?php
$usertype = $_SESSION['USER_TYPE'];
$base_id  = $_GET['id'];

$expense_records = "SELECT *,er.id as recid FROM expense_records er,expense_report ep, expense_report_mapping erp WHERE erp.exp_id=er.id AND erp.exp_report_id=ep.id AND ep.id=".$base_id." and ep.created_by = '".$_SESSION['USER_ID']."' and (er.is_approver_rejected=1 or er.is_verifier_rejected=1) AND erp.is_submitted=1  ";
$result_expense = Select($expense_records,$conn_exp); 



$select_rec = "SELECT * FROM expense_report WHERE id=".$base_id; 
$result_report = Select($select_rec, $conn_exp);
$main_id=$result_report['rows'][0]['id'];
$report_name=$result_report['rows'][0]['report_name'];
$rep_desc=$result_report['rows'][0]['rep_desc'];
$wallettype=$result_report['rows'][0]['wallettype'];
$created_on=$result_report['rows'][0]['created_on'];
$submitted_on=$result_report['rows'][0]['submitted_on'];
//echo $insert_rec;

?>
<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 300px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 10px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
 /* font-size: 17px;*/
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
  border-left: none;
  height: 300px;
}
</style>

                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Report</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										
										<a href="expenses/dashboard.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											Go Back To Dashboard
										</a>
										
									
									</div>
									
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->
										<?php
										// Get status message
										if(!empty($_GET['status'])){
										switch($_GET['status']){
										case 'succ':
										$statusType = 'alert-success';
										$statusMsg = 'Expenses data has been imported successfully.';
										$altype = "success";
										break;
										case 'err':
										$statusType = 'alert-danger';
										$statusMsg = 'Some problem occurred, please try again.';
										$altype = "danger";
										break;
										case 'invalid_file':
										$statusType = 'alert-danger';
										$statusMsg = 'Please upload a valid CSV file.';
										$altype = "danger";
										break;
										default:
										$statusType = '';
										$statusMsg = '';
										}
										?>
										<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
										<div class="alert-icon"><i class="<?php echo $statusType; ?>"></i></div>
										<div class="alert-text"><?php echo $statusMsg; ?></div>
										<div class="alert-close">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true"><i class="la la-close"></i></span>
										</button>
										</div>
										</div>
										<?php } ?>

									<!--Begin::Section-->
									<div class="row">
										<div class="col-xl-8">
											<!--begin::Portlet-->
											<div class="kt-portlet">
												<div class="kt-portlet__head">
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
															RESUBMIT REPORT (ID: <?php echo $report_name;?>)
														</h3>
													</div>
													<div class="kt-portlet__head-label">
														<h3 class="kt-portlet__head-title">
														
														</h3>
													</div>
												</div>

												<!--begin::Form-->
												<form class="kt-form kt-form--label-right" enctype="multipart/form-data" action="expenses/form_handlers/resubmit_mainframe_report.php" method="post" >
													<div class="kt-portlet__body">
													     
														 
													<div class="form-group row" style='background-color:#f0f0f0;'>
                                                    <input type="hidden" id="main_report_id" name="main_report_id" value="<?php echo $main_id;?>"/>
													<div class="col-lg-11">
													<label><strong><?php echo $report_name;?></strong></label><input type="hidden" id="report_name"  name="report_name" value="<?php echo $report_name;?>"/></br>
													<label>Description: <strong><?php echo $rep_desc ;?></strong></label><input type="hidden" id="report_desc"  name="report_desc" value="<?php echo $rep_desc ;?>"/></br>
													<label>Wallet: <strong><?php echo $wallettype;?></strong></label></br><input type="hidden" id="report_wallettype"  name="report_wallettype" value="<?php echo $wallettype;?>"/>
													<label>Report Created On: <strong><?php echo $created_on;?></strong></label></br>
													<label>Report Submitted On: <strong><?php echo $submitted_on;?></strong></label></br>
													</div>

													
													</div>
													
													<div class="form-group row" >

													<div class="col-lg-9">
													    <div class="kt-portlet kt-portlet--mobile">
														<div class="kt-portlet__head kt-portlet__head--lg">
														<div class="kt-portlet__head-label">
														<span class="kt-portlet__head-icon">
														<i class="kt-font-brand flaticon2-line-chart"></i>
														</span>
														<h3 class="kt-portlet__head-title">
														Rejected Expenses Records
														</h3>
														</div>
														
														</div>
														<div class="kt-portlet__body">

														<!--begin: Datatable -->
														<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
														<thead>
														<tr>
														<th hidden></th>
														<th>Status</th>
														<th>Date-<br>Time</th>
														<th>Merchant-<br>Description</th>
														
														<th>ExpenseId-<br>Txn Type</th>
														
														<th >Requested Amount</th>
														<th>Approved Amount</th>
														<th hidden></th>
														<th>Category-<br>Wallet Type</th>
                                                        <th>Actions</th>
														
														</tr>
														</thead>
														<tbody>
														<?php 
														$total_expenses=0;
														$a=1;
														foreach($result_expense['rows'] as $rec_expense)
														{
														$display_val="";													
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==0))
														{
														$display_val="<span class='kt-badge kt-badge--brand kt-badge--inline kt-badge--pill'>Saved</span>";
														}
														else if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1))
														{
														$display_val="<span class='kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill'>Submitted</span>";
														}
														$display_exp_type="";
														if($rec_expense['exp_type']==1)
														{
															$display_exp_type="Cash";
														}
														else if($rec_expense['exp_type']==2)
														{
														    $display_exp_type="Own Vehicle - Local Conveyance";	
														}
														else if($rec_expense['exp_type']==3)
														{
															$display_exp_type="Petty Cash";
														}
														
														$total_expenses=$total_expenses + $rec_expense['amount'];
														?>
														<tr>
														
														<td><?php echo $display_val;?></td>
														<td><?php echo $rec_expense['created_on']?></td>
														<td><?php echo $rec_expense['spentat']."<br>".$rec_expense['description'];?></td>
														<td><?php echo $rec_expense['report_id']."<br>".$display_exp_type;?></td>
														<td><?php echo $rec_expense['amount'];?></td>
														<td><?php echo $rec_expense['approved_amount'];?></td>
														<td hidden></td>
														<td><span class="kt-font-bold kt-font-primary"><?php echo $rec_expense['exp_category']."-".$rec_expense['wallettype'];?></span></td>

														<td >
														<center>
														<span class="dropdown show">
														<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
														<i class="la la-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right " x-placement="top-end" style="position: absolute; transform: translate3d(-214px, -145px, 0px); top: 0px; left: 0px; will-change: transform;">
														<a class="dropdown-item" href="expenses/edit_expense.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-edit"></i> Edit Details</a>
														<a  class="dropdown-item" href="#" data-toggle="modal" data-target="#kt_modal_<?php echo $rec_expense['recid'];?>" onclick="callme(<?php echo $a;?>)">
														<i class="la la-paperclip"></i>Bills</i>
														</a>
														<a  class="dropdown-item" href="#" data-toggle="modal" data-target="#kt_modal_view_<?php echo $rec_expense['recid'];?>" >
														<i class="la la-eye"></i>View</i>
														</a>
														
														</div>
														</span>
														</center>
														</center>
														<button type="button"  onclick="remove_expense(<?php echo $main_id;?>,<?php echo $rec_expense['recid'];?>);" class="btn btn-warning btn-elevate btn-pill btn-elevate-air btn-sm">Remove</button>
														
														
														
														
														</td>
														<td nowrap="" hidden></td>
														</tr>
														<?php	
														$a++;
														}
														?>

														</tbody>
														</table>

														<!--end: Datatable -->
														</div>
														</div> 
													</div>
													<div class="col-lg-3">
													<label><strong>Report Summary</strong></label></br>
													<label><strong>Description: </strong></label><br><?php echo $rep_desc ;?></br>
													<label><strong>Wallet: </strong></label><br><?php echo $wallettype;?></br>
													<label>Total Expenses: <strong><?php echo $total_expenses;?> INR</strong></label><input type="hidden" id="total_exp"  name="total_exp" value="<?php echo $total_expenses;?>"/></br>
													<label>Report: <strong><?php echo $total_expenses;?> INR</strong></label></br>
													</div>
                                                    </div>
														
													
													
													     
													
													</div>
											        </div>
														
														
														
													
													<div class="kt-portlet__foot">
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-lg-12 kt-align-center">
																	<button type="submit" id="next" name="next" class="btn btn-primary">Resubmit</button>
																	<a href="expenses/dashboard.php" class="btn btn-secondary">
																	Cancel</a>
																</div>
																
															</div>
														</div>
													</div>
												</form>
                                        </div>
												<!--end::Form-->
									    </div>

									<!--end::Portlet-->
								<div class="row" style="padding-top:50px;">
								<div class="col-xl-8">
								</div>
								</div>
								<!--
								<div class="row">
								<div class="col-xl-8">	
								<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											 Bulk Uploaded Expenses
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="la la-download"></i> Export
													</button>
													<div class="dropdown-menu dropdown-menu-right">
														<ul class="kt-nav">
															<li class="kt-nav__section kt-nav__section--first">
																<span class="kt-nav__section-text">Choose an option</span>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-print"></i>
																	<span class="kt-nav__link-text">Print</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-copy"></i>
																	<span class="kt-nav__link-text">Copy</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-excel-o"></i>
																	<span class="kt-nav__link-text">Excel</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-text-o"></i>
																	<span class="kt-nav__link-text">CSV</span>
																</a>
															</li>
															<li class="kt-nav__item">
																<a href="#" class="kt-nav__link">
																	<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																	<span class="kt-nav__link-text">PDF</span>
																</a>
															</li>
														</ul>
													</div>
												</div>
												&nbsp;
												<a href="#" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													New Record
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

								
									<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
										<thead>
											<tr>
												<th>Record ID</th>
												<th>Order ID</th>
												<th>Country</th>
												<th>Ship City</th>
												<th>Ship Address</th>
												<th>Company Agent</th>
												<th>Company Name</th>
												<th>Ship Date</th>
												<th>Status</th>
												<th>Type</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td>61715-075</td>
												<td>China</td>
												<td>Tieba</td>
												<td>746 Pine View Junction</td>
												<td>Nixie Sailor</td>
												<td>Gleichner, Ziemann and Gutkowski</td>
												<td>2/12/2018</td>
												<td>3</td>
												<td>2</td>
												<td nowrap></td>
											</tr>
										
										</tbody>
									</table>

								
								</div>
							</div>  
							    </div></div>-->
								</div>

									</div>

									<!--End::Section-->



									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
							
							
						</div>
					</div>

				
				</div>
			</div>
		</div>
<?php 
$jx=1;
$b=1;
foreach($result_expense['rows'] as $rec_expense)
{
$expense_records_files = "SELECT *  FROM files WHERE exp_id = '".$rec_expense['recid']."'";
$result_expense_files = Select($expense_records_files,$conn_exp);
?>
<!--begin::Modal-->
							<div class="modal fade" id="kt_modal_<?php echo $rec_expense['recid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="max-width:1200px;">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Bills Attached</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="kt-scroll" data-scroll="true" data-height="400">
				
											<!--begin::Accordion-->
											<div class="accordion accordion-light  accordion-toggle-arrow" id="accordionExample5">
											<input type="hidden" id="last_selected_<?php echo $b;?>" name="last_selected_<?php echo $b;?>" value="<?php echo $jx;?>" />
											<div class="tab">
											<?php
												
												foreach($result_expense_files['rows'] as $files_val)
												{
											?> 
												
												<button class="tablinks" onclick="openFile(event, 'file_<?php echo $files_val['id'];?>')" id="defaultOpen_<?php echo $jx;?>"><?php echo $files_val['file_name'];?></button>
												<?php
												$jx++;
												}
												?>
												<button >
												<center><strong><a  href="expenses/ajax/downloadfiles.php?exp_id=<?php echo $rec_expense['recid'];?>" >Download All</a></strong></center>
												</button>
												
												</div>
												
												
												
												<?php
											
												foreach($result_expense_files['rows'] as $files_val)
												{
												$case_name=explode("_",$files_val['file_name']);

												?> 
												<div id="file_<?php echo $files_val['id'];?>" class="tabcontent">
												<center>
											    <object  data=
												"http://one.edupristine.com/expenses/uploads/<?php echo "EXP_CASE_".$case_name[0]."/".$files_val['file_name'];?> "
												width="800" 
												height="500"> 
												</object>
												</center>
												</div>
												<?php
												}
												?>
	
												
												
											
											</div>

											<!--end::Accordion-->
									
									

											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="modal fade" id="kt_modal_view_<?php echo $rec_expense['recid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="max-width:800px;">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Expense Record Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="kt-scroll" data-scroll="true" data-height="200">
											<?php
														$display_exp_type="";
														if($rec_expense['exp_type']==1)
														{
															$display_exp_type="Cash";
														}
														else if($rec_expense['exp_type']==2)
														{
														    $display_exp_type="Own Vehicle - Local Conveyance";	
														}
														else if($rec_expense['exp_type']==3)
														{
															$display_exp_type="Petty Cash";
														}
											?>		
									                  
														
													
														<table style="width:100%;border:1px #000000;font-size:16px;" class="form-control" >
                                                    <tr>
                                                    <td><strong>Report Id:</strong></td>
                                                    <td><?php echo $rec_expense['report_id'];?></td>
													</tr>
													<tr>
													<td><strong>Created On:</strong></td>
                                                    <td><?php echo $rec_expense['created_on']?></td>
													</tr> 
													<tr>
                                                    <td><strong>Spent At:</strong></td>
                                                    <td><?php echo $rec_expense['spentat'];?></td>
													</tr>
													<tr>
													<td><strong>Description:</strong></td>
                                                    <td><?php echo $rec_expense['description']?></td>
													</tr> 
                                                    <tr>
                                                    <td><strong>City:</strong></td>
                                                    <td><?php echo $rec_expense['city'];?></td>
													</tr>
													<tr>
													<td><strong>Expense category:</strong></td>
                                                    <td><?php echo $rec_expense['exp_category']?></td>
													</tr> 
													<tr>
                                                    <td><strong>Requested Amount:</strong></td>
                                                    <td>INR <?php echo $rec_expense['amount'];?></td>
													</tr>
													<tr>
													<td><strong>Occurance Date:</strong></td>
                                                    <td><?php echo $rec_expense['occurance_date']?></td>
													</tr>
                                                    <tr>
													<td><strong>Occurance Time:</strong></td>
                                                    <td><?php echo $rec_expense['occurance_time']?></td>
													</tr>
                                                    <tr>
													<td><strong>Submitted On:</strong></td>
                                                    <td><?php echo $rec_expense['submitted_on']?></td>
													</tr>													
													</table>
									

											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
<?php
$b++;
}?>
							<!--end::Modal-->

		<!-- end:: Page -->
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

function remove_expense(record_id,exp_id)
{
  if (confirm("Are you sure you want to remove this expense from current report?") == true) {
    
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				///alert("Student Courier Request Raised Successfully.");
				submit_button_clicked = '';
				$('#kt_modal_'+exp_id).modal('toggle');
				
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Expense Record removal Failed. Contact Administrator");
				
			}
			
			else
			{
				alert("Expense Record removal Failed. Contact Administrator2");
			
			}
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/remove_expense_record.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('record_id='+fixEscape(record_id)+'&exp_id='+fixEscape(exp_id));
	
  } else {
   
  }
	
}


function openFile(evt, fileName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(fileName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen_1").click();

function callme(id)
{
	var mainid=document.getElementById("last_selected_"+id).value;
	document.getElementById("defaultOpen_"+mainid).click();
}

function downloadall_files(val)
{
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/downloadfiles.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('exp_id='+fixEscape(val));
}
</script>
