<?php
setlocale(LC_MONETARY, 'en_IN');
$location_id = get_get_value('lid');
$status = get_get_value('st');
$w_type = get_get_value('wtp');
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date("Y-m-d",strtotime("first day of last month"));
    $edate = date("Y-m-d",strtotime("+1 day"));
    $date_range = $sdate." / ".$edate;
}
$startdate=$sdate;
$enddate=$edate;

$qrys_range="";
if($date_range!='')
{
$qrys_range="and created_on >= '".$startdate."' AND created_on <= '".$enddate."' ";
}


$qrys="";
if($location_id!='')
{
if($location_id=='All')
{	
$qrys="and location in (1,3,4,5,6,7,9)";
}
else if($location_id!='All')
{	
$qrys="and location in (".$location_id.")";
}
}

$qrys_status="";
if($status!='')
{
if($status=='Saved')
{	
$qrys_status="and is_saved=1 and is_submitted=0 and is_verified=0";
}
else if($status=='Submitted')
{	
$qrys_status="and is_saved=1 and is_submitted=1 and is_approved=0 and is_verified=0 and is_rejected=0";
}
else if($status=='Approved')
{	
$qrys_status="and is_saved=1 and is_submitted=1 and is_approved=1 and is_verified=0";
}
else if($status=='Verified')
{	
$qrys_status="and is_saved=1 and is_submitted=1 and is_approved=1 and is_verified=1";
}
else if($status=='Rejected')
{	
$qrys_status="and is_saved=1 and is_submitted=1 and (is_verifier_rejected=1 OR is_approver_rejected=1)";
}
else if($status!='All')
{	
$qrys_status="";
}
}

$qrys_wtp="";
if($w_type!='')
{
if($w_type=='All')
{	
$qrys_wtp="";
}
else
{	
$qrys_wtp="and wallettype = '".$w_type."'";
}
}

$user_type=$_SESSION['USER_TYPE'];
if($user_type=="Emp")
{
$expense_report = "SELECT *,id as recid from expense_report where created_by = '".$_SESSION['USER_ID']."' ".$qrys_status." ".$qrys_wtp." ".$qrys_range." order by id desc";
$result_expense = Select($expense_report,$conn_exp);

$select_user_det = "SELECT u.`id`, `user_name`, `user_email`, `user_contact`, `password`, `loc_id`,u.`is_active`,l.loc_type,l.loc_name,`user_type` FROM `users` u, locations l WHERE l.id = u.loc_id and u.id=".$_SESSION['USER_ID']; 
$result_user_det = Select($select_user_det, $conn_exp);
$user_name=$result_user_det['rows'][0]['user_name'];
$user_location=$result_user_det['rows'][0]['loc_name'];
$user_location_ids=$result_user_det['rows'][0]['loc_id'];
$center_locations = "SELECT id,loc_name from locations where id=".$user_location_ids;
}
else if($user_type=="Pre")
{
$expense_report = "SELECT *,id as recid from expense_report where id!=0 ".$qrys." ".$qrys_status." ".$qrys_wtp." ".$qrys_range." order by id desc";
$result_expense = Select($expense_report,$conn_exp);

$select_user_det = "SELECT u.`id`, `user_name`, `user_email`, `user_contact`, `password`, `loc_id`,u.`is_active`,l.loc_type,l.loc_name,`user_type` FROM `users` u, locations l WHERE l.id = u.loc_id and u.id=".$_SESSION['USER_ID']; 
$result_user_det = Select($select_user_det, $conn_exp);
$user_name=$result_user_det['rows'][0]['user_name'];
$user_location=$result_user_det['rows'][0]['loc_name'];	

$center_locations = "SELECT id,loc_name from locations ";
	
}
$result_loc_centers = Select($center_locations,$conn_exp);
?>

<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 30%;
  height: 300px;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 10px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
 /* font-size: 17px;*/
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  float: left;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 70%;
  border-left: none;
  height: 300px;
}
</style>
                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										<span class="kt-subheader__desc">REPORTS</span>
										<?php
										if($user_type=="Emp")
										{
										?>
										
										<a href="expenses/add_expense.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ ADD EXPENSE
										</a>
										<a href="expenses/create_report.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ NEW REPORT
										</a>
										<?php
										}
										?>
										
									</div>
									<div class="kt-subheader__toolbar">
										<div class="kt-subheader__wrapper">
										<div class="input-group" id="kt_daterangepicker_2">
										<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
										<div class="input-group-append">
										<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
										</div>
										</div>
									    </div>
										<div class="kt-subheader__wrapper">
								        <button onClick="get_batches();" type="button" class="btn btn-primary form-control">Get Report</button>	
										</div>
										
									</div>
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->


									<!--Begin::Section-->
										<div class="row">
										<div class="col-xl-8">	
										<div class="kt-portlet kt-portlet--mobile">
										<div class="kt-portlet__head kt-portlet__head--lg">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon">
													<i class="kt-font-brand flaticon-interface-7"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													Report Records
												</h3>
											</div>
											<div class="kt-portlet__head-toolbar">
												<div class="kt-portlet__head-wrapper">
													<div class="kt-portlet__head-actions">
													     <div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="la la-hourglass"></i> Status
															</button>
															
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																    <li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=Saved&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Saved</span></a>
																	</li>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=Submitted&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Submitted</span></a>
																	</li>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=Verified&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Verified</span></a>
																	</li>
																	
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=Approved&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Approved</span></a>
																	</li>
																	
																   <li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=Rejected&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Rejected</span></a>
																	</li>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=All&lid=<?php echo $location_id;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">All</span></a>
																	</li>
																</ul>
															</div>
														</div>
														
														
													    <?php
														if($user_type=="Pre")
														{
														?>
													    <div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="la la-map-marker"></i> Locations
															</button>
															
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																
																	<?php 
																	foreach($result_loc_centers['rows'] as $loc_center)
																	{	
																	?>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?lid=<?php echo $loc_center['id'];?>&st=<?php echo $status;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text"><?php echo $loc_center['loc_name']; ?></span></a>
																	</li>
																	<?php	
																	}
																	?>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?lid=All&st=<?php echo $status;?>&wtp=<?php echo $w_type;?>&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">All</span></a>
																	</li>
																</ul>
															</div>
														</div>
														<?php
														}
														?>
														 <div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="la la-folder-open"></i> Wallet
															</button>
															
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																    <li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=<?php echo $status;?>&lid=<?php echo $location_id;?>&wtp=Imprest&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Imprest</span></a>
																	</li>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=<?php echo $status;?>&lid=<?php echo $location_id;?>&wtp=Reimbursement&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">Reimbursement</span></a>
																	</li>
																	<li class="kt-nav__item">
																	<a class="kt-nav__link" href="expenses/emp_report_records.php?st=<?php echo $status;?>&lid=<?php echo $location_id;?>&wtp=All&date_range=<?php echo $date_range;?>"><span class="kt-nav__link-text">All</span></a>
																	</li>
																</ul>
															</div>
														</div>
														
														
														<div class="dropdown dropdown-inline">
															<button type="button"  onClick="get_export();"class="btn btn-default btn-icon-sm " aria-haspopup="true" aria-expanded="false">
																<i class="la la-download"></i> Export
															</button>
															
															
														</div>
														<input type="hidden" id="selected_location" name="selected_location" value="<?php echo $location_id;?>"/>
														<input type="hidden" id="selected_status" name="selected_status" value="<?php echo $status;?>"/>
														<input type="hidden" id="selected_wtype" name="selected_wtype" value="<?php echo $w_type;?>"/>
														<input type="hidden" id="sel_created_by" name="sel_created_by" value="<?php echo $_SESSION['USER_ID'];?>"/>
														<input type="hidden" id="sel_creater_type" name="sel_creater_type" value="<?php echo $_SESSION['USER_TYPE'];?>"/>
													</div>
												</div>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin: Datatable -->
											<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
												<thead>
													<tr>
													    <th hidden></th>
														<th>Status</th>
														<th>Report ID</th>
														<th>Report Name</th>
														<th>Name</th>
														<th>Location</th>
														<th>Wallet</th>
														<th>Created On</th>
														<th>Submission Date</th>
														<th>Approved Date</th>
														<th>Verified Date</th>
														<th>Actions</th>
														<th hidden></th>
													</tr>
												</thead>
												<tbody>
												<?php 
												$a=1;
												foreach($result_expense['rows'] as $rec_expense)
												{
												
                                                $partial_link=0;												
													
												$expense_records_creater = "SELECT u.`id`, `user_name`, `user_email`, `user_contact`, `password`, `loc_id`,u.`is_active`,l.loc_type,l.loc_name,`user_type` FROM `users` u, locations l WHERE l.id = u.loc_id and u.id=".$rec_expense['created_by']."";
												$result_expense_creater = Select($expense_records_creater,$conn_exp);
												$user_created_by=$result_expense_creater['rows'][0]['user_name'];
												$user_created_by_loc=$result_expense_creater['rows'][0]['loc_name'];
												
												//Total Expenses in record
												$select_expenses="select count(er.id) as `totalexp` from expense_records er,expense_report_mapping erm where erm.exp_id=er.id and erm.exp_report_id=".$rec_expense['recid'];
											    $result_select_expenses = Select($select_expenses,$conn_exp);
												$totalexp=$result_select_expenses['rows'][0]['totalexp'];
												
												//Total Expenses in record verified
												$select_expenses_ver="select count(er.id) as `totalexp_ver` from expense_records er,expense_report_mapping erm where erm.exp_id=er.id and er.is_verified=1 and er.is_verifier_rejected=0 and erm.exp_report_id=".$rec_expense['recid'];
												$result_select_expenses_ver = Select($select_expenses_ver,$conn_exp);
												$totalexp_ver=$result_select_expenses_ver['rows'][0]['totalexp_ver'];
												
												
												//Total Expenses in record approved
												$select_expenses_app="select count(er.id) as `totalexp_rej` from expense_records er,expense_report_mapping erm where erm.exp_id=er.id and er.is_approved=1 and er.is_verified=0 and er.is_approver_rejected=0 and erm.exp_report_id=".$rec_expense['recid'];
												$result_select_expenses_app = Select($select_expenses_app,$conn_exp);
												$totalexp_app=$result_select_expenses_app['rows'][0]['totalexp_rej'];
												
												
												$total_display_calc = "Total Expense:".$totalexp."--Verified:".$totalexp_ver."--Rejected:".$totalexp_rej;
												
													
                                                $display_val="";													
												if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==0))
												{
													$display_val="<span class='kt-badge kt-badge--brand kt-badge--inline kt-badge--pill'>Saved</span>";
												}
												if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==0))
												{
													$display_val="<span class='kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill'>Submitted</span>";
												}
												
												if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==1))
												{
													if($totalexp==$totalexp_app)
													{
													$display_val="<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill' style='background:#FFFF00;color:#000000' >Approved</span>";
													}
													else if($totalexp_app<$totalexp)
													{
													$display_val="<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill' style='background:#FFFFD9;color:#000000;width:120px;' ><strong>Partially Approved</strong></span>";	
													}
													
												}
												if($rec_expense['is_verified']==1)
												{
													if($totalexp==$totalexp_ver)
													{
													$display_val="<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill' style='background:#008040;color:#ffffff' >Verified</span>";
													}
													else if($totalexp_ver<$totalexp)
													{
													$display_val="<span class='kt-badge  kt-badge--success kt-badge--inline kt-badge--pill' style='background:#C6FFE2;color:#000000;width:120px;' ><strong>Partially Verified</strong></span>";	
													
													$partial_link=1;
													
													}
												}
												if(($rec_expense['is_approver_rejected']==1)&&($rec_expense['is_resubmitted']==0))
												{
													$display_val="<span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill' style='background:#ff0000;width:120px;'><strong>Approver Rejected</strong></span>";
												}
												if(($rec_expense['is_verifier_rejected']==1)&&($rec_expense['is_resubmitted']==0)&&($rec_expense['is_reapproved']==0))
												{
													$display_val="<span class='kt-badge  kt-badge--danger kt-badge--inline kt-badge--pill' style='background:#ff0000;width:120px;'><strong>Verifier Rejected</strong></span>";
												}
												if(($rec_expense['is_resubmitted']==1)&&($rec_expense['is_reapproved']==0))
												{
													$display_val="<span class='kt-badge  kt-badge--warning kt-badge--inline kt-badge--pill'>Re-Submitted</span>";
												}
												?>
													<tr>
														<td hidden>
														
														</td>
														<td><?php echo $display_val;?></td>
														<td><?php echo $rec_expense['exp_report_id'];?></td>
														<td><?php echo $rec_expense['report_name'];?></td>
														<td><?php echo $user_created_by;?></td>
														<td><?php echo $user_created_by_loc; ?></td>
														<td><span class="kt-font-bold kt-font-primary"><?php echo $rec_expense['wallettype'];?></span></td>
														<td><?php echo $rec_expense['created_on'];?></td>
														<td><?php echo $rec_expense['submitted_on'];?></td>
														<td><?php echo $rec_expense['approved_on'];?></td>
														<td><?php echo $rec_expense['verified_on'];?></td>
														<td >
														<center>
														<span class="dropdown show">
														<a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
														<i class="la la-ellipsis-h"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right " x-placement="top-end" style="position: absolute; transform: translate3d(-214px, -145px, 0px); top: 0px; left: 0px; will-change: transform;">
														<?php 
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==0)&&($user_type=="Emp"))
														{
														?>
														<a class="dropdown-item" href="expenses/edit_mainframe_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-edit"></i> Edit Report</a>
														<?php
														}
														?>
														<?php 
														if((($rec_expense['is_approver_rejected']==1)||($rec_expense['is_verifier_rejected']==1))&&($rec_expense['is_resubmitted']==0)&&($user_type=="Emp"))
														{
													
														?>
														<a class="dropdown-item" href="expenses/resubmit_mainframe_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-retweet"></i> Resubmit Report</a>
														<?php
														}
														?>
														
														<?php
														if($partial_link==1)
														{
														?>
														<a class="dropdown-item" href="expenses/resubmit_mainframe_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-retweet"></i> Resubmit Report</a>
														<?php	
														}
														?>
														
														<?php 
														if((($rec_expense['is_approver_rejected']==1)||($rec_expense['is_verifier_rejected']==1))&&($rec_expense['is_resubmitted']==1)&&($rec_expense['is_reapproved']==0)&&($user_type=="Pre")&&($_SESSION['USER_ID']==1))
														{
													
														?>
														<a class="dropdown-item" href="expenses/reapprove_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-retweet"></i> Reapprove Report</a>
														<?php
														}
														?>
														
														<?php 
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==0)&&($rec_expense['is_approver_rejected']==0)&&($user_type=="Pre")&&($_SESSION['USER_ID']==1))
														{
														?>
														<a class="dropdown-item" href="expenses/approve_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-check-square"></i> Approve Report</a>
														<?php
														}
														?>
														<?php 
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==1)&&($rec_expense['is_verified']==0)&&($rec_expense['is_verifier_rejected']==0)&&($rec_expense['is_resubmitted']==0)&&($user_type=="Pre")&&($_SESSION['USER_ID']!=1))
														{
														?>
														<a class="dropdown-item" href="expenses/verify_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-check-circle"></i> Verify Report</a>
														<?php
														}
														?>
														
														
														<?php 
														if(($rec_expense['is_reapproved']==1)&&($rec_expense['is_reverified']==0)&&($rec_expense['is_verifier_rerejected']==0)&&($user_type=="Pre")&&($_SESSION['USER_ID']!=1))
														{
														?>
														<a class="dropdown-item" href="expenses/reverify_report.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-retweet"></i>Re-Verify Report</a>
														<?php
														}
														?>
														
														<?php 
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==1)&&($rec_expense['is_verified']==1)&&($user_type=="Pre"))
														{
														?>
														<a class="dropdown-item" href="expenses/view_report_details.php?id=<?php echo $rec_expense['recid'];?>"><i class="la la-binoculars"></i> View Summary </a>
														<?php
														}
														?>
														
														
														<a  class="dropdown-item" href="#" data-toggle="modal" data-target="#kt_modal_<?php echo $rec_expense['recid'];?>" onclick="callme(<?php echo $a;?>)">
														<i class="la la-paperclip"></i>Bills</i>
														</a>
														<a  class="dropdown-item" href="#" data-toggle="modal" data-target="#kt_modal_view_<?php echo $rec_expense['recid'];?>" >
														<i class="la la-eye"></i>View</i>
														</a>
														<?php 
														if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==0)&&($user_type=="Emp"))
														{
														?>
														<a  class="dropdown-item" href="#" data-toggle="modal" onclick="remove_report(<?php echo $rec_expense['recid'];?>);">
														<i class="la la-trash-o"></i>Remove</i>
														</a>
														<?php
														}
														?>
														
														</div>
														</span>
														</center>
														</center>
														
														
														
														
														
														</td>
														<td nowrap hidden></td>
													</tr>
													<?php
                                                    $a++;													
													}
													?>
												
												</tbody>
											</table>

											<!--end: Datatable -->
										</div>
										</div>  
										</div>
										</div>

									<!--End::Section-->

									


									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->
<?php 
$jx=1;
$b=1;
foreach($result_expense['rows'] as $rec_expense)
{
$expense_records_files = "SELECT *,f.id as files_sid  FROM files f,expense_report_mapping erm WHERE f.exp_id=erm.exp_id AND erm.exp_report_id = '".$rec_expense['recid']."'";



$result_expense_files = Select($expense_records_files,$conn_exp);
?>
<!--begin::Modal-->
							<div class="modal fade" id="kt_modal_<?php echo $rec_expense['recid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="max-width:1200px;">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Bills Attached</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="kt-scroll" data-scroll="true" data-height="400">
				
											<!--begin::Accordion-->
											<div class="accordion accordion-light  accordion-toggle-arrow" id="accordionExample5">
											<input type="hidden" id="last_selected_<?php echo $b;?>" name="last_selected_<?php echo $b;?>" value="<?php echo $jx;?>" />
											<div class="tab">
											<?php
												
												foreach($result_expense_files['rows'] as $files_val)
												{
											?> 
												
												<button class="tablinks" onclick="openFile(event, 'file_<?php echo $files_val['files_sid'];?>')" id="defaultOpen_<?php echo $jx;?>"><?php echo $files_val['file_name'];?></button>
												<?php
												$jx++;
												}
												?>
												<button >
												<center><strong><a  href="expenses/ajax/downloadfiles_reports.php?report_id=<?php echo $rec_expense['recid'];?>" >Download All</a></strong></center>
												</button>
												
												</div>
												
												
												
												<?php
											
												foreach($result_expense_files['rows'] as $files_val)
												{
												$case_name=explode("_",$files_val['file_name']);

												?> 
												<div id="file_<?php echo $files_val['files_sid'];?>" class="tabcontent">
												<center>
											    <object  data=
												"http://one.edupristine.com/expenses/uploads/<?php echo "EXP_CASE_".$case_name[0]."/".$files_val['file_name'];?> "
												width="800" 
												height="500"> 
												</object>
												</center>
												</div>
												<?php
												}
												?>
	
												
												
											
											</div>

											<!--end::Accordion-->
									
									

											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
							
							
							
							<div class="modal fade" id="kt_modal_view_<?php echo $rec_expense['recid'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								<div class="modal-dialog" role="document" style="max-width:800px;">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title" id="exampleModalLabel">Report Details</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											</button>
										</div>
										<div class="modal-body">
											<div class="kt-scroll" data-scroll="true" data-height="200">
											<?php
														$display_exp_type="";
														if($rec_expense['exp_type']==1)
														{
															$display_exp_type="Cash";
														}
														else if($rec_expense['exp_type']==2)
														{
														    $display_exp_type="Own Vehicle - Local Conveyance";	
														}
														else if($rec_expense['exp_type']==3)
														{
															$display_exp_type="Petty Cash";
														}
											?>		
									                  
														
													
														<table style="width:100%;border:1px #000000;font-size:16px;" class="form-control" >
                                                    <tr>
                                                    <td><strong>Report Id:</strong></td>
                                                    <td><?php echo $rec_expense['exp_report_id'];?></td>
													</tr>
													
													<tr>
                                                    <td><strong>Report Name:</strong></td>
                                                    <td><?php echo $rec_expense['report_name'];?></td>
													</tr>
													
													<tr>
                                                    <td><strong>Wallet Type:</strong></td>
                                                    <td><?php echo $rec_expense['wallettype'];?></td>
													</tr>
													
													
													<tr>
                                                    <td><strong>Report Description:</strong></td>
                                                    <td><?php echo $rec_expense['rep_desc'];?></td>
													</tr>
													
													
													<tr>
													<td><strong>Created On:</strong></td>
                                                    <td><?php echo $rec_expense['created_on']?></td>
													</tr> 
													
													<td><strong>Submitted On:</strong></td>
                                                    <td><?php echo $rec_expense['submitted_on']?></td>
													</tr>
													 <tr>
													<td><strong>Approved On:</strong></td>
                                                    <td><?php echo $rec_expense['approved_on']?></td>
													</tr>
													 <tr>
                                                    <td><strong>Approver Comments:</strong></td>
                                                    <td><?php echo $rec_expense['approver_comments'];?></td>
													</tr>		
                                                    <tr>
													<td><strong>Verified On:</strong></td>
                                                    <td><?php echo $rec_expense['verified_on']?></td>
													</tr>	

                                                    <tr>
                                                    <td><strong>Verifier Comments:</strong></td>
                                                    <td><?php echo $rec_expense['verifier_comments'];?></td>
													</tr>													
													</table>
									

											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										</div>
									</div>
								</div>
							</div>
<?php
$b++;
}?>
							<!--end::Modal-->
	
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

function remove_report(record_id)
{
  if (confirm("Are you sure you want to remove this Report. Any Expenses linked with this Report will be affected. Kindly check before intiating this process as this action cannot be reversed?") == true) {
    
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Selected Report and all Linked Expenses has been removed Successfully.");
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Expense Record removal Failed. Contact Administrator");
				
			}
			
			else
			{
				alert("Expense Record removal Failed. Contact Administrator2");
			
			}
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/remove_report_expenses.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('record_id='+fixEscape(record_id));
	
  } else {
   
  }
	
}


function openFile(evt, fileName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(fileName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen_1").click();

function callme(id)
{
	var mainid=document.getElementById("last_selected_"+id).value;
	document.getElementById("defaultOpen_"+mainid).click();
}

function downloadall_files(val)
{
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/downloadfiles.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('exp_id='+fixEscape(val));
}


function get_batches()
{
    var date_range = document.getElementById('date_range').value;
	var selected_location = document.getElementById('selected_location').value;
	var selected_status = document.getElementById('selected_status').value;
	var selected_wtype = document.getElementById('selected_wtype').value;
	
	window.location = 'expenses/emp_report_records.php?date_range='+date_range+'&lid='+selected_location+'&st='+selected_status+'&wtp='+selected_wtype;
}
	
function get_export()
	{
		

		var sel_created_by = document.getElementById('sel_created_by').value;
		var sel_creater_type = document.getElementById('sel_creater_type').value;
		var date_range = document.getElementById('date_range').value;
		var selected_location = document.getElementById('selected_location').value;
		var selected_status = document.getElementById('selected_status').value;
		var selected_wtype = document.getElementById('selected_wtype').value;
		window.location = 'expenses/content/report_exportdata.php?date_range='+date_range+'&lid='+selected_location+'&st='+selected_status+'&wtp='+selected_wtype+'&sel_created_by='+sel_created_by+'&sel_creater_type='+sel_creater_type;
	}
</script>

														