<?php
$user_type=$_SESSION['USER_TYPE'];
?>


                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										<span class="kt-subheader__desc">EXPENSES</span>
										<?php
										if(($user_type=="Emp")||($_SESSION['USER_ID']==1))
										{
										?>
										
										<a href="expenses/add_expense.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ ADD EXPENSE
										</a>
										<a href="expenses/create_report.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ NEW REPORT
										</a>
										<?php
										}
										?>
									</div>
									<div class="kt-subheader__toolbar">
										<div class="kt-subheader__wrapper">
											
											
										</div>
									</div>
								</div>

								<!-- end:: Content Head -->
                                
								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">
								<div class="row">
								<div class="col-xl-8">	
                                    <div class="kt-portlet ">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Pending Reports
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin::Accordion-->
											<div class="accordion  accordion-toggle-arrow" id="accordionExample4">
												<?php 
												$jx=1;
												for ($i = -1; $i <= 0; $i++){
												if($user_type=="Emp")
												{	
												$expense_records = "SELECT * from expense_report where created_by = '".$_SESSION['USER_ID']."' and is_submitted=0 and DATE_FORMAT(created_on,'%m')=".date('m', strtotime("$i month"))." AND DATE_FORMAT(created_on,'%Y')=".date('Y', strtotime("$i month"))."";
												$result_expense = Select($expense_records,$conn_exp);	
												}
												else
												{
												
												if($_SESSION['USER_ID']==1)
												{
												$expense_records = "SELECT * from expense_report where  is_submitted=1 and is_approved=0 and is_approver_rejected=0 and DATE_FORMAT(created_on,'%m')=".date('m', strtotime("$i month"))." AND DATE_FORMAT(created_on,'%Y')=".date('Y', strtotime("$i month"))."";
												$result_expense = Select($expense_records,$conn_exp);		
												}
												else
												{
												$expense_records = "SELECT * from expense_report where  is_submitted=1 and  is_approved=1 and is_verified=0 and is_verifier_rejected=0  and DATE_FORMAT(created_on,'%m')=".date('m', strtotime("$i month"))." AND DATE_FORMAT(created_on,'%Y')=".date('Y', strtotime("$i month"))."";
												$result_expense = Select($expense_records,$conn_exp);		
												}
												
												
												}
												?>
												<div class="card">
													<div class="card-header" id="headingOne4">
														<div class="card-title" data-toggle="collapse" data-target="#collapseOne<?php echo $jx;?>" aria-expanded="true" aria-controls="collapseOne<?php echo $jx;?>">
															<i class="flaticon2-layers-1"></i> <?php echo date('F Y', strtotime("$i month"));?>
														</div>
													</div>
													<div id="collapseOne<?php echo $jx;?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample4">
														<div class="card-body">
															<div class="row">
															<div class="col-xl-8">	
															<div class="kt-portlet kt-portlet--mobile">
														
															<div class="kt-portlet__body">
                                                            <table   id="table_<?php echo $jx;?>">
															<thead>
															<tr>
															<th>Report Name</th>
															<th>Wallet Type</th>
															<th>Description</th>
															<th>Created On</th>
															<th>Amount</th>
															<th>Actions</th>
															</tr>
															</thead>
															<tbody>
															<?php 
															foreach($result_expense['rows'] as $rec_expense)
															{
															if($user_type=="Emp")
															{	
															$expense_records_disc = "SELECT sum(amount) as `total_amount` from expense_report_mapping exm , expense_records  er where er.id=exm.exp_id and  exm.exp_report_id = '".$rec_expense["id"]."'";
															$result_expense_disc = Select($expense_records_disc,$conn_exp);
															$amount=$result_expense_disc['rows'][0]['total_amount'];
															}
															else
															{
																
															if($_SESSION['USER_ID']!=1)
															{
															$expense_records_disc = "SELECT sum(approved_amount) as `total_amount` from expense_report_mapping exm , expense_records  er where er.id=exm.exp_id and  exm.exp_report_id = '".$rec_expense["id"]."'";
															$result_expense_disc = Select($expense_records_disc,$conn_exp);
															$amount=$result_expense_disc['rows'][0]['total_amount'];	
															}
															else
															{
															$expense_records_disc = "SELECT sum(amount) as `total_amount` from expense_report_mapping exm , expense_records  er where er.id=exm.exp_id and  exm.exp_report_id = '".$rec_expense["id"]."'";
															$result_expense_disc = Select($expense_records_disc,$conn_exp);
															$amount=$result_expense_disc['rows'][0]['total_amount'];	
																
															}
																
															}
															?>
															<tr>
														
															<td><?php echo $rec_expense['report_name'];?></td>
															<td><?php echo $rec_expense['wallettype'];?></td>
															<td><?php echo $rec_expense['rep_desc'];?></td>
															<td><?php echo $rec_expense['created_on'];?></td>
															<td>INR <?php echo $amount;?></td>
													        <td nowrap>
															
															<?php 
															if($user_type=="Emp")
															{
															?>
															<a href="expenses/edit_mainframe_report.php?id=<?php echo $rec_expense["id"]; ?>" >Edit</a> |  <button type="button"  onclick="remove_record(<?php echo $rec_expense["id"]; ?>);" class="btn btn-warning btn-elevate btn-pill btn-elevate-air btn-sm">Remove</button> | <a href="expenses/edit_mainframe_report.php?id=<?php echo $rec_expense["id"]; ?>" >Submit</a>
														    <?php
															}
															?>
															<?php 
															if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_approved']==1)&&($rec_expense['is_verified']==0)&&($user_type=="Pre")&&($rec_expense['is_verifier_rejected']==0)&&($_SESSION['USER_ID']!=1))
															{
															?>
															<a  href="expenses/verify_report.php?id=<?php echo $rec_expense['id'];?>"><i class="la la-check-circle"></i> Verify Report</a>
															<?php
															}
															?>
															
															<?php 
															if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_verified']==0)&&($rec_expense['is_approved']==0)&&($rec_expense['is_approver_rejected']==0)&&($user_type=="Pre")&&($_SESSION['USER_ID']==1))
															{
															?>
															<a  href="expenses/approve_report.php?id=<?php echo $rec_expense['id'];?>"><i class="la la-check-circle"></i> Approve Report</a>
															<?php
															}
															?>
															
														
															<?php 
															if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1)&&($rec_expense['is_verified']==1)&&($user_type=="Pre"))
															{
															?>
															<a  href="expenses/view_report_details.php?id=<?php echo $rec_expense['id'];?>"><i class="la la-binoculars"></i> View Summary </a>
															<?php
															}
															?>



															
															
															
															
															
															
															
															
															
															
															
															
															
															
															
															</td>
															</tr>
															<?php	
															}
															?>

															</tbody>
															</table>


															</div>
															</div>  
															</div>
															</div>
														</div>
													</div>
												</div>
												<?php
												$jx++;
												}
												?>
											</div>

											<!--end::Accordion-->
										</div>
									</div>
									</div>
									</div>
									
									
									<?php 
									if(($user_type=="Pre")&&($_SESSION['USER_ID']!=1))
									{
									?>
									
									<div class="row">
									<?php
									$center_locations = "SELECT id,loc_name from locations where id!=1 ";
									$result_loc_centers = Select($center_locations,$conn_exp);
									foreach($result_loc_centers['rows'] as $loc_center)
									{
										
									$center_reports = "SELECT count(id) as `total_reports` from expense_report where location=".$loc_center['id']." ";
									$result_center_reports = Select($center_reports,$conn_exp);	
									$total_reports_generated=$result_center_reports['rows'][0]['total_reports'];

									$center_reports_sub = "SELECT count(id) as `total_reports` from expense_report where location=".$loc_center['id']." and is_submitted=1";
									$result_center_reports_sub = Select($center_reports_sub,$conn_exp);	
									$total_reports_generated_sub=$result_center_reports_sub['rows'][0]['total_reports'];
                                    
									$center_reports_ver = "SELECT count(id) as `total_reports` from expense_report where location=".$loc_center['id']." and is_verified=1";
									$result_center_reports_ver = Select($center_reports_ver,$conn_exp);	
									$total_reports_generated_ver=$result_center_reports_ver['rows'][0]['total_reports'];
									
									
									$center_exp = "SELECT count(id) as `total_expense` from expense_records where location=".$loc_center['id']." ";
									$result_center_exp = Select($center_exp,$conn_exp);	
									$total_reports_exp=$result_center_exp['rows'][0]['total_expense'];
									
									$center_exp_sub = "SELECT count(id) as `total_expense` from expense_records where location=".$loc_center['id']." and is_submitted=1";
									$result_center_exp_sub = Select($center_exp_sub,$conn_exp);	
									$total_reports_exp_sub=$result_center_exp_sub['rows'][0]['total_expense'];
									
									$center_exp_ver = "SELECT count(id) as `total_expense` from expense_records where location=".$loc_center['id']." and is_verified=1";
									$result_center_exp_ver = Select($center_exp_ver,$conn_exp);	
									$total_reports_exp_ver=$result_center_exp_ver['rows'][0]['total_expense'];
									
									$center_exp_amount = "SELECT sum(amount) as `total_expense` from expense_records where location=".$loc_center['id']." ";
									$result_center_exp_amt = Select($center_exp_amount,$conn_exp);	
									$total_reports_exp_amt=$result_center_exp_amt['rows'][0]['total_expense'];
									
									
									$center_exp_approved_amount = "SELECT sum(approved_amount) as `total_expense` from expense_records where location=".$loc_center['id']." ";
									$result_center_exp_amt_ap = Select($center_exp_approved_amount,$conn_exp);	
									$total_reports_exp_amt_ap=$result_center_exp_amt_ap['rows'][0]['total_expense'];
									
									?>
									<div class="col-lg-4">	
									<div class="kt-portlet kt-portlet--skin-solid kt-portlet-- kt-bg-brand">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon">
													<i class="flaticon-placeholder"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													Summary : <?php echo $loc_center['loc_name'];?>
												</h3>
											</div>
											
										</div>
										<div class="kt-portlet__body">
										<table style=" padding-top: 50px;padding-right: 30px;padding-bottom: 50px;padding-left: 80px;">
										<tr>
										<td colspan=3>
										<span class="kt-nav__link-icon"><i class="flaticon-interface-7"></i></span> <strong>Reports</strong>
										</td>
										</tr>
										<tr style="padding:20px;">
										<td>
										Generated : <?php echo $total_reports_generated; ?>
										</td>
										<td>
										Sumbitted : <?php echo $total_reports_generated_sub; ?>
										</td>
										<td>
										Verified : <?php echo $total_reports_generated_ver; ?>
										</td>
										</tr>
										
										<tr>
										<td colspan=3>
										<span class="kt-nav__link-icon"><i class="flaticon-coins"></i></span> <strong>Expenses</strong>
										</td>
										</tr>
										<tr style="padding:20px;">
										<td>
										Generated : <?php echo $total_reports_exp; ?>
										</td>
										<td>
										Sumbitted : <?php echo $total_reports_exp_sub; ?>
										</td>
										<td>
										Verified : <?php echo $total_reports_exp_ver; ?>
										</td>
										</tr>
										
										<tr>
										<td colspan=3>
										<span class="kt-nav__link-icon"><i class="flaticon-layers"></i></span> <strong>Expense Amount</strong>
										</td>
										</tr>
										<tr style="padding:20px;">
										<td>
										<strong>Incurred :INR <?php echo $total_reports_exp_amt; ?></strong>
										</td>
										<td>
										<strong>Approved :INR <?php echo $total_reports_exp_amt_ap; ?></strong>
										</td>
										<td>
										
										</td>
										</tr>
										</table>
										
										</div>
									</div>
									</div>
									<?php
									}
									?>
									
									</div>
									<?php
									}
									?>


									

									


									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->

<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

function remove_record(record_id)
{
  if (confirm("Are you sure you want to remove this Report?") == true) {
    
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				///alert("Student Courier Request Raised Successfully.");
				submit_button_clicked = '';
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Report removal Failed. Contact Administrator");
				
			}
			
			else
			{
				alert("Report removal Failed. Contact Administrator2");
			
			}
		}
	}

	xmlhttp.open('POST', 'expenses/ajax/remove_report.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('record_id='+fixEscape(record_id));
	
  } else {
   
  }
	
}
</script>	
