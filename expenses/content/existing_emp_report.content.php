<?php

$base_id  = $_GET['id'];
$wallettype  = $_GET['wt'];
$expense_records = "SELECT *,er.id as recid FROM expense_records er, expense_report_mapping erp WHERE erp.exp_id=er.id  AND (erp.exp_report_id IS NUll OR erp.exp_report_id='') AND (er.report_id IS NULL OR er.report_id='')  and er.created_by = '".$_SESSION['USER_ID']."' and wallettype='".$wallettype."'";

//echo $expense_records;

$result_expense = Select($expense_records,$conn_exp);



$select_rec = "SELECT * FROM expense_report WHERE id=".$base_id; 
$result_report = Select($select_rec, $conn_exp);
$main_id=$result_report['rows'][0]['id'];
$report_name=$result_report['rows'][0]['report_name'];
$rep_desc=$result_report['rows'][0]['rep_desc'];
$wallettype=$result_report['rows'][0]['wallettype'];
$created_on=$result_report['rows'][0]['created_on'];
$submitted_on=$result_report['rows'][0]['submitted_on'];
?>


                    <div class="kt-header__bottom">
							<div class="kt-container">

								<!-- begin: Header Menu -->
								<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
								<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
									<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
										<ul class="kt-menu__nav ">
											<li class="kt-menu__item  kt-menu__item--active " aria-haspopup="true"><a href="expenses/dashboard.php" class="kt-menu__link "><span class="kt-menu__link-text"><?php echo $display_head; ?></span></a></li>
										</ul>
									</div>
								</div>

								<!-- end: Header Menu -->
							</div>
					</div>
					</div>
					<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-grid--stretch">
						<div class="kt-container kt-body  kt-grid kt-grid--ver" id="kt_body">
							<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">

								<!-- begin:: Content Head -->
								<div class="kt-subheader   kt-grid__item" id="kt_subheader">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
										<span class="kt-subheader__separator kt-subheader__separator--v"></span>
										<span class="kt-subheader__desc">EXPENSES</span>
										<a href="expenses/add_expense.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ ADD EXPENSE
										</a>
										<a href="expenses/create_report.php" class="btn btn-label-primary btn-bold btn-icon-h kt-margin-l-10">
											+ NEW REPORT
										</a>
									</div>
									<div class="kt-subheader__toolbar">
									<!--
										<div class="kt-subheader__wrapper">
											<a href="expenses/dashboard.php" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="Select dashboard daterange" data-placement="left">
												<span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
												<span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>
												<i class="flaticon2-calendar-1"></i>
											</a>
											
										</div>
									-->	
									</div>
								</div>

								<!-- end:: Content Head -->

								<!-- begin:: Content -->
								<div class="kt-content kt-grid__item kt-grid__item--fluid" id="kt_content">

									<!--Begin::Dashboard 2-->
                                    <?php
									if(isset($_GET['dberror']))
									{
										$msg = "Expense Record update Failed. Unknown Error Contact Administrator.";
										$altype = "danger";
										$icontype = "flaticon2-cross";
									}
									elseif(isset($_GET['record_created']))
									{
										$msg = "Expense record/s Added to report Successfully.";
										$altype = "success";
										$icontype = "flaticon2-flaticon-suitcase";
									}
									else
									{
										$msg = "";
									}
									if($msg != '')
									{
									?>
									<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
										<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
										<div class="alert-text"><?php echo $msg; ?></div>
										<div class="alert-close">
											<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true"><i class="la la-close"></i></span>
											</button>
										</div>
									</div>
									<?php } ?>

									<!--Begin::Section-->
									    <form  id="exp_rec_sub" name="exp_rec_sub" method="post" action="expenses/form_handlers/existing_emp_report.php" >
										<input type="hidden" id="selected_records" name="selected_records" />
										<input type="hidden" id="record_id" name="record_id"  value="<?php echo $base_id;?>" />
										<div class="row">
										<div class="col-xl-8">	
										<div class="kt-portlet kt-portlet--mobile">
										<div class="kt-portlet__head kt-portlet__head--lg">
											<div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon">
													<i class="kt-font-brand flaticon2-line-chart"></i>
												</span>
												<h3 class="kt-portlet__head-title">
													Add Existing Expenses Records to Report - <?php echo $report_name;?>
												</h3>
											</div>
											<div class="kt-portlet__head-toolbar">
											<!--
												<div class="kt-portlet__head-wrapper">
													<div class="kt-portlet__head-actions">
														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-default btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="la la-download"></i> Export
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Choose an option</span>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-print"></i>
																			<span class="kt-nav__link-text">Print</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-copy"></i>
																			<span class="kt-nav__link-text">Copy</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-excel-o"></i>
																			<span class="kt-nav__link-text">Excel</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-text-o"></i>
																			<span class="kt-nav__link-text">CSV</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																			<span class="kt-nav__link-text">PDF</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
														&nbsp;
														
													</div>
												</div>
												-->
												
											</div>
										</div>
										<div class="kt-portlet__body">

											<!--begin: Datatable -->
											<table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
												<thead>
													<tr>
													    <th></th>
														<th>Status</th>
														<th>Report ID</th>
														<th>Date Time</th>
														<th>Submission Date</th>
														<th>Name</th>
														<th>Requested Amount</th>
														<th>Approved Amount</th>
														<th>Wallet</th>
														
														<th hidden>Actions</th>
													</tr>
												</thead>
												<tbody>
												<?php 
												foreach($result_expense['rows'] as $rec_expense)
												{
                                                $display_val="";													
												if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==0))
												{
													$display_val="<span class='kt-badge kt-badge--brand kt-badge--inline kt-badge--pill'>Saved</span>";
												}
												else if(($rec_expense['is_saved']==1)&&($rec_expense['is_submitted']==1))
												{
													$display_val="<span class='kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill'>Submitted</span>";
												}
												?>
													<tr>
														<td>
														<span style="width: 20px;"><label class="kt-checkbox kt-checkbox--single kt-checkbox--solid"><input type="checkbox" class="rec_checkme" id="rec_check" name="rec_check" value="<?php echo $rec_expense['recid'];?>" >&nbsp;<span></span></label></span>
														</td>
														<td><?php echo $display_val;?></td>
														<td><?php echo $rec_expense['report_id'];?></td>
														<td><?php echo $rec_expense['occurance_date']."<br>".$rec_expense['occurance_time'];?></td>
														<td><?php echo $rec_expense['created_on'];?></td>
														<td><?php echo $_SESSION['U_NAME']; ?></td>
														<td><?php echo $rec_expense['amount'];?></td>
														<td><?php echo $rec_expense['approved_amount'];?></td>
														<td><span class="kt-font-bold kt-font-primary"><?php echo $rec_expense['wallettype'];?></span></td>
														
														<td hidden nowrap></td>
													</tr>
													<?php	
													}
													?>
												
												</tbody>
											</table>
											<div class="kt-portlet__foot">
														<div class="kt-form__actions">
															<div class="row">
																<div class="col-lg-12 kt-align-center">
																	<button type="button" id="startprocess" name="startprocess" class="btn btn-primary">Save Details</button>
																	<button type="reset" class="btn btn-secondary">Cancel</button>
																</div>
																
															</div>
														</div>
											</div>

											<!--end: Datatable -->
										</div>
										</div>  
										</div>
										</div>
                                        </form>
									<!--End::Section-->

									


									<!--End::Dashboard 2-->
								</div>

								<!-- end:: Content -->
							</div>
						</div>
					</div>

				
				</div>
			</div>
		</div>

		<!-- end:: Page -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script>
    $(document).ready(function() {
        $(".rec_checkme").click(function(){
            var favorite = [];
            $.each($("input[name='rec_check']:checked"), function(){            
                favorite.push($(this).val());
            });
           $("#selected_records").val(favorite.join(","));
        });
		
		
		 $("#startprocess").click(function(){
			var legchecked = $('input[name=rec_check]:checked').length;
			if (legchecked==0){ 
			alert("You have not selected any Expense records! Select any record before submitting.")
			return false;	
			}
			else
			{
			 $( "#exp_rec_sub" ).submit();	
			};
        });
    });
</script>