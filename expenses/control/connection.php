<?php
	require $env;
	//echo "ABCC";

	try
	{
		//connect as appropriate as above
		$conn_exp = new PDO("mysql:host=52.77.5.117;dbname=expenses","liveuser","p3t3r5bu4g");
		$conn_exp->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$conn_exp->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $ex)
	{
		$file	=	fopen('logs/connectionerror.log.txt','a');
		fwrite($file,'------------------------------------'.PHP_EOL);
		fwrite($file,'----------Connection Error----------'.PHP_EOL);
		fwrite($file,'Database:'.$mydb_main.PHP_EOL);
		fwrite($file,'Error Message:'.$ex->getMessage().PHP_EOL);
		fwrite($file,'Error File:'.$ex->getFile().PHP_EOL);
		fwrite($file,'Error Line:'.$ex->getLine().PHP_EOL);
		fwrite($file,'Error Code:'.$ex->getCode().PHP_EOL);
		fwrite($file,'Error Description:'.$ex->getTraceAsString().PHP_EOL);
		fwrite($file,'Date and Time:'.date("Y-m-d H:i:s").PHP_EOL);
		fclose($file);
		//DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString());
	}
?>