<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'issue_books.php?filter_param='+filter_param;
	}
</script>
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($location_id == "")
	{
		$location_id = $_SESSION['U_LOCATION_ID'];
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = $_SESSION['U_LOCATION_ID'];
	}
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		$query_allstudents = "SELECT s.*,sb.subcourse_name FROM `students` s, sub_courses sb WHERE sb.id = s.subcourse_id and loc_id = ".$location_id." LIMIT 100";
	}
	else
	{
		$query_allstudents = "SELECT s.*,sb.subcourse_name FROM `students` s, sub_courses sb WHERE sb.id = s.subcourse_id and loc_id = ".$location_id." and (student_name like '%".$filter_param."%' OR crm_id like '%".$filter_param."%' OR student_email like '%".$filter_param."%' OR student_contact like '%".$filter_param."%' OR subcourse_name like '%".$filter_param."%')";
	}
	
	$result_allstudents = Select($query_allstudents,$conn);	
	$i = 1;
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-form kt-form--label-right">
<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											ISSUE BOOKS
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name</th>
												<th>CRM #</th>
												<th>Sub Course</th>
												<th>Email</th>
												<th>Phone</th>												
												
												<th>Action</th>												
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td><?php echo $student['crm_id']; ?></td>
													<td><?php echo $student['subcourse_name']; ?></td>
													<td><?php echo $student['student_email']; ?></td>
													<td><?php echo $student['student_contact']; ?></td>
													<td><button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#student_<?php echo $student['id']; ?>">Issue Books</button></td>
												</tr>
												
										<?php	
											$i++;
											}
											?>
											</tbody>
									</table>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,isu.issued_status,isu.id,isu.created_at,isu.created_by,isu.issue_date,isu.issued_by from skus s,issuance isu where s.id = isu.sku_id and isu.student_id = ".$student['id'];
												$result_select_issue = Select($select_issue,$conn);
											?>
											<div class="modal fade" id="student_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Books Status: ".$student['student_name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="200">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['id'];?>">
																		<thead>
																			<tr>
																				<td>#</td>
																				<td>SKU Name</td>
																				<td>Raised On</td>
																				<td>Raised By</td>
																				<td>Issue Status</td>
																				<td>Issued On</td>
																				
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{

																				$raise_int = "SELECT user_name from users where id = ".$each_issue['created_by'];
																				$result_raise_int = Select($raise_int,$conn);	
																				
																					

																																								
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td><?php echo $each_issue['created_at']; ?></td>
																						<td><?php echo $result_raise_int['rows'][0]['user_name']; ?></td>
																						<td>
																							<?php if( $each_issue['issued_status']) echo "Issued"; 
																							else
																							{	
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$each_issue['id']."|";
																							?>
																								<input type="checkbox" id="cb_<?php echo $student['id']."_".$each_issue['id']; ?>" name="cb_<?php echo $student['id']."_".$each_issue['id']; ?>" value="1">
																							<?php }
																							?>
																						</td>
																						<td><?php echo $each_issue['issue_date']; ?></td>
																						
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																		</tbody>
																	</table>
																	<input hidden id="hidden_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="IssueBooks(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Issue</button>
															</div>
														</div>
													</div>
												</div>
												<?php	
											
											}
										?>
											
										
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function IssueBooks(student_id)
{
	
	var tb = "hidden_"+student_id;
	var cbs = document.getElementById(tb).value;
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);
	//alert(final_cbs_json);
	if(submit_button_clicked=='1')
	{
		//alert('Sale saving is in progress...');
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Books Issued Successfully.");
				submit_button_clicked = '';
				$('#student_'+student_id).modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'inventoryunavailable')
			{
				alert("Atleast one SKU not available at the Location");
				submit_button_clicked = '';
			}
			else if(output.status == 'db_error')
			{
				alert("Issuance FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Issuance FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/issue_books.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id));
}
</script>