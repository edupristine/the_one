<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
    $msg="";
    if(isset($_GET['success']))
	{
		$msg = "Emails Sent to Students Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else if(isset($GET['failed']))
	{
		$msg = "Email Sending Failed";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Send  Mails to  students
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/send_crm_mail.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-12">

<table class="table table-striped- table-bordered table-hover table-checkable" id="" >
<thead>
	<tr>
		<th>Mail Template</th>
	</tr>
</thead>
	<tbody>
	
	<tr>
	<td>
	Subject: <input class="form-control" type="text" name="subjectcontent" id="subjectcontent"  />
    </td>
    </tr>
	
	<tr>
	<td>
	Body:
	<textarea class="form-control"  type="text" name="bodycontent" id="bodycontent" cols=100 rows=20 ></textarea>
    </td>
    </tr>
</tbody>
</table>
<!--end: Datatable -->

</div>
</div>
<div class="form-group row">
<div class="col-lg-12">
<label class="">Enter Student/s Email ID/s (Comma Seperated)</label>
<div class="">
<textarea class="form-control" id="emails" name="emails"  ></textarea>
</div>
</div>
</div>

</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			<button type="submit" name="submit" id="kt_blockui_3_5" class="btn btn-success" >Send Emails</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>

<script>
$('body').on('click', '#submit', function() { 
     alert('test'); 
     App.startPageLoading({animate: true});	 
	 
});

</script>

<script type="text/javascript">
$(document).ready(function () {
	$('#bodycontent').jqxEditor({
		height: "400px",
		width: '100%'
	});
	
	


});
</script>