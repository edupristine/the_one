<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$id = get_get_value('id');	
$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$qry1 = "SELECT * FROM job_locations where id=".$id;
$res = mysqli_query($edu,$qry1);
$r = mysqli_fetch_assoc($res);
	
	if(isset($_GET['dupname1']))
	{
		$msg = "Location Update Failed. Location Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Location Update Failed. Location already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Location Update Failed. Location Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Location Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Location Updated Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Edit Job Location
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/location_edit.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-12">
	<label>Location Name</label>
	<input type="hidden" class="form-control" id="location_id" name="location_id" value="<?php echo $r['id'];?>">
	<input type="text" class="form-control" id="location_name" name="location_name" value="<?php echo $r['location_name'];?>">
	
</div>
</div>
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
			<center><button type="submit" class="btn btn-success">Update</button>
			<button type="reset" class="btn btn-secondary">Cancel</button></center>
			<a href="location_master.php" class="btn btn-warning">Back To List</a></center>
		</div>
	</div>
</div>
</div>
</form>

</div>
</div>
</div>

</div>

<?php

?>