<!-- end:: Subheader -->
<script>
	function filter_records()
	{
		var date_range = document.getElementById('date_range').value;
		var status = document.getElementById('status').value;
		window.location = 'wp_leads.php?date_range='+date_range+'&status='+status;
	}
	
	function get_export()
	{
		var date_range = document.getElementById('date_range').value;
		var status = document.getElementById('status').value;
		window.location = 'content/lead_exportdata.php?date_range='+date_range+'&status='+status;
	}
	
</script>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$date_range = get_get_value('date_range');
$status = get_get_value('status');
	
	$dtquery="";
	$stquery="";
	if($date_range != ""){
	$drange = explode(" / ",$date_range);
	$sdate = date('Y-m-d',strtotime($drange[0]));
	$edate = date('Y-m-d',strtotime($drange[1]));
	
	$dtquery = " and CONVERT(createdon, DATE) between '".$sdate."' and '".$edate."' ";
	
	}
	else
	{
		$sdate = date('Y-m-d');
	    $edate = date('Y-m-d');
		
		$dtquery = " and CONVERT(createdon, DATE) between '".$sdate."' and '".$edate."' ";
	}
	
	if($status!="All")
	{
		$stquery = " and sf_response like '%".$status."%' ";
	}
	else
	{
		$stquery="";
	}
	
	
$mysql_host='13.126.164.124';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'wp_tetris');

$result = mysqli_query($conn,"SELECT * FROM wp_leads WHERE leadid!='' $dtquery $stquery ORDER BY leadid ASC");
$num_rows = mysqli_num_rows($result);
?>



<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Pushed Leads 
</h3>
</div>

<div class="kt-portlet__head-toolbar">
	<div class="kt-portlet__head-wrapper">
		<div class="kt-portlet__head-actions">
			<div class="col">
					<table><tr>
					<td>
					<div class="dropdown">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Push Status
							</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="wp_leads.php?status=Success">Success</a>
							<a class="dropdown-item" href="wp_leads.php?status=Failed">Failed</a>
							<a class="dropdown-item" href="wp_leads.php?status=All">All</a>
							</div>
							
						</div>
						</td></tr></table>
					</div>
		</div>
	</div>
</div>
</div>
<form class="kt-form kt-form--label-right" id="batch" name="batch" method='post' enctype="multipart/form-data" action='form_handlers/csv_to_db.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<input type="hidden" id="status" name="status" value="<?php echo $status;?>" />
<div class="col-lg-4">
<label>Date Range</label>
<div class="input-group" id="kt_daterangepicker_2">
<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
<div class="input-group-append">
<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
</div>
</div>
</div>

<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_records();" type="button" class="btn btn-primary form-control">Submit</button>
	
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_export();" type="button" class="btn btn-primary form-control">Export</button>
</div>

</div>
</div>
</form>
<div class="kt-portlet__body">



                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="">
                            <thead>
                                <tr style="text-align: center;">
								   <th rowspan=2>#</th>
                                    <th rowspan=2>Name</th>
                                    <!--<th rowspan=2>Email</th>
                                    <th rowspan=2>Phone</th>-->
									<th rowspan=2>Unique identifier</th>
                                    <th rowspan=2>Course 2</th>
                                    <th rowspan=2>Type</th>
									<th rowspan=2>Created On</th>
									<th rowspan=2>Status</th>
									<th rowspan=2>Message /Failure Message</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php
                      $status="";
					  $msg="";
					  $id=1;
                        if($num_rows > 0){
                        while($r = mysqli_fetch_assoc($result)) {
                            $rows[] = $r;
                        }
                        foreach($rows as $row){
									
						$string = json_decode(trim($row['sf_response'], '"'));
						$status =  $string[0]->responseType;
						$flmsg =  $string[0]->failureMessage;
						$msg=$string[0]->message;
						
						$finalmsg="";
						if($msg!='')
						{
							$finalmsg=$msg;
						}
						else
						{
							$finalmsg=$flmsg;
						}
						
                        						
                        ?>
                            <tr><td><?php echo $id; ?></td>
                                <td><?php echo $row['name']; ?></td>
                               <!-- <td><?php// echo $row['email']; ?></td>
                                <td><?php //echo $row['contact']; ?></td>-->
								<td><?php echo $row['unique_identifier']; ?></td>
                                <td><?php echo $row['course']; ?></td>
                                <td><?php echo $row['type']; ?></td>
								<td><?php echo $row['createdon']; ?></td>
								<td><?php echo $status; ?></td>
								<td><?php echo $finalmsg; ?></td>
                            </tr>
                        <?php 
						$id++;
						} ?>
                   
                        <?php
                        }?>
                        </tbody>
                    </table>
</div>
</div>
</div>

</div>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>