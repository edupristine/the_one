<script>
	function get_results()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'placement_reg_list.php?filter_param='+filter_param;
	}
</script>
<?php
$filter_param = get_get_value('filter_param');

if($filter_param != "")
{
$query = "SELECT * FROM student_placements where batch_code!='' and (batch_code like '%".$filter_param."%' or student_name like '%".$filter_param."' or email like '%".$filter_param."%') ORDER BY id desc";
}
elseif($filter_param == "")
{
$query = "SELECT * FROM student_placements ORDER BY id desc";	
}

$placements = Select($query,$conn);

?>

<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
				<div class="kt-portlet__body">
				<div class="form-group row">
				<div class="col-lg-4">
					<label>Search</label>
					<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
				</div>
				<div class="col-lg-2">
					<label class="">&nbsp;</label>
					<button onClick="get_results();" type="button" class="btn btn-primary form-control">Submit</button>
					
				</div>
				</div>
				</div>
                <div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Placement Registration Records
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
                                                <th rowspan=2></th>
												<th rowspan=2>Batch Code</th>
                                                <th rowspan=2>Student Name</th>
												<th rowspan=2>Phone</th>
												<th rowspan=2>Email</th>
												<th rowspan=2>Fresher / Experienced</th>
												<th rowspan=2>Intrested</th>
												<th rowspan=2>Download CV</th>
                                            </tr>
                                        </thead>
										<tbody>
										<?php
											foreach($placements['rows'] as $placement){
												$id=$placement['id'];
												$batch_code = $placement['batch_code'];
                                                $student_name = $placement['student_name'];
                                                $email = $placement['phone'];
                                                $phone = $placement['email'];
												$address = $placement['address'];
												$qualification1 = $placement['qualification1'];
												$qualification2 = $placement['qualification2'];
												$student_type = $placement['student_type'];
												$years_of_exp = $placement['years_of_exp'];
												$current_company = $placement['current_company'];
												$current_ctc = $placement['current_ctc'];
												$intrested = $placement['intrested'];
												$cv_filename = $placement['cv_filename'];
                                         ?>
                                                
												<tr>
                                                    <td><a href="#" onclick="expandRow('<?php echo $placement['id']; ?>');"><i class="fa fa-caret-right" id="<?php echo $placement['id']; ?>"></i></a></td>
													<td><?php echo $batch_code; ?></td>
                                                    <td><?php echo $student_name; ?></td>
													<td ><?php echo $email; ?></td>
													<td><?php echo $phone; ?></td>
                                                    <td><?php echo $student_type=='Exp'?'Experienced':'Fresher';?></td>
													<td><?php echo $intrested=='Y'?'Yes':'No'; ?></td>
													<?php
													if($cv_filename!="")
													{
													?>
													<td><center><a href="http://one.edupristine.com/uploads/<?php echo $cv_filename;?>" class="flaticon2-download-2" target=_blank /></td>
                                                   <?php
													}else
													{
													?>
													<td><center>NA</center></td>
													<?php
													}
													?>
												</tr>
                                               
                                                <tr id="<?php echo  $placement['id']."_1"; ?>" style="display:none;">
                                                <th></th>
												<th style="font-weight:500;">Qualification(1)</th>
                                                <th  style="font-weight:500;">Qualification(2)</th>    
                                                <th colspan=2  style="font-weight:500;">Address</th>
                                                <th  style="font-weight:500;">Years of Exp</th>
                                                <th style="font-weight:500;">Current Company</th>
                                                <th style="font-weight:500;">Current CTC</th>
                                                 </tr>
												 <tr id="<?php echo  $placement['id']."_2"; ?>" style="display:none;">
                                                <th></th>
												<td ><?php echo $qualification1;?></td>
                                                <td ><?php echo $qualification2;?></td>    
                                                <td colspan=2><?php echo $address; ?></td>
                                                <td><?php echo $years_of_exp!=''? $years_of_exp : 0;?></td>
                                                <td><?php echo $current_company;?></td>
                                                <td><?php echo $current_ctc;?></td>
                                                 </tr>
                                               
										<?php	
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
                            <script>
                            function expandRow(pid){
								if(document.getElementById(pid).classList.contains('fa-caret-right')){
								document.getElementById(pid+"_1").style.display="table-row";
								document.getElementById(pid+"_2").style.display="table-row";
                                document.getElementById(pid).classList.remove('fa-caret-right');
                                document.getElementById(pid).classList.add('fa-caret-down');
                                }else{
                                document.getElementById(pid+"_1").style.display="none";
								document.getElementById(pid+"_2").style.display="none";
                                document.getElementById(pid).classList.remove('fa-caret-down');
                                document.getElementById(pid).classList.add('fa-caret-right');
                                }
                                
                            }
                          
                            </script>

