
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}

	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id  and  sl.id = s.loc_id  ORDER BY s.student_name";
	
	}
	else
	{
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and  sl.id = s.loc_id and (student_name = '".$filter_param."' OR crm_id = '".$filter_param."' OR student_email = '".$filter_param."' OR student_contact = '".$filter_param."' OR subcourse_name = '".$filter_param."') ORDER BY s.student_name";
	}
	$result_allstudents = Select($query_allstudents,$conn);	
	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">


	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Update Students Location 
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">

											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name</th>
												<th >Account No</th>
									            <th>Student Location</th>
												<th>Student Course</th>
												<th>Student Subcourse</th>
												<th colspan=2><center>Action</center></th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										        $locs = "SELECT * from locations where id = ".$student['loc_id'];
												$result_locs = Select($locs,$conn);
												
												
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td ><?php echo $student['crm_id']; ?></td>
										            <td><?php echo $result_locs['rows'][0]['loc_name']; ?></td>
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_subcourses['rows'][0]['subcourse_name']; ?></td>
													<td>
														<select id="new_loc_<?php echo $student['id'];?>" name="new_loc_<?php echo $student['id'];?>">
														<option value="0">Select</option>
														<?php 
														
														
														$location_c = "SELECT id,loc_name from locations where id not in (1,2)";
														$result_loc_c = Select($location_c,$conn);

														foreach($result_loc_c['rows'] as $loc_c)
														{
                                                        if($loc_c['id']==$student['loc_id'])
														{
														?>
														<option value="<?php echo $loc_c['id'];?>" hidden><?php echo $loc_c['loc_name'];?></option>
														<?php
														
														}
														else
														{
														?>
														<option value="<?php echo $loc_c['id'];?>"><?php echo $loc_c['loc_name'];?></option>
														<?php
														}
														}
														?>															

														</select>
													</td>
													
													
													<td>
													<button onClick="UpdateStudentLoc(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Update</button>
													
												   
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
										
										
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function UpdateStudentLoc(student_id)
{

	var er = document.getElementById('new_loc_'+student_id);
	var new_loc = er.options[er.selectedIndex].value;
	if(new_loc==0)
	{
		alert("Please select a location to update");
		return false;
		submit_button_clicked = '';
	}
	
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Student Location Updated Successfully.");
				submit_button_clicked = '';
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Student Location Update Failed. Contact Administrator");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Student Location Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_student_location.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('student_id='+fixEscape(student_id)+'&new_location='+fixEscape(new_loc));
	
}

</script>