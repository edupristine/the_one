<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['dupno']))
{
$msg = "FeedBack Submission Failed. Student already Exists.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['paramsmissing']))
{
$msg = "FeedBack Submission Failed. Student Name and Batch Code are Mandatory Fields.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "FeedBack Submission Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "Exam Details Submission Successfull.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<!--begin::Portlet-->

<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Exam Details
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-4">
<label>Account No:</label>
<input  class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text"  value=""/>
</div>
<div class="col-lg-4" style="padding-top:25px;">
<label></label>
<button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="RECSelect();">Get Details</button>
</div>
<div class="col-lg-4" >
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" id="stu_exm_det" name="stu_exm_det" method='POST' action='form_handlers/student_exam_details.php'>
	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Details
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
							Organization Details
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
							Exam Details
						</a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
							Records
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_portlet_tab_1_1">
				<div class="form-group row">
				<div class="col-lg-3">
				<label>Student Name :</label>
				<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
				<input    class="form-control" name="acc_id" id="acc_id" style="width:100%;" type="text" hidden />
				</div>
				<div class="col-lg-3">
				<label>Registered Email ID :</label>
				<input    class="form-control" name="emailid_1" id="emailid_1" style="width:100%;" type="text"  value=""   readonly />
				</div>
				<div class="col-lg-3">
				<label class="">Registered Mobile Number :</label>
				<input   class="form-control" name="mobileno_1" id="mobileno_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-3">
				<label class="">Counselor Name :</label>
				<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				</div>
				
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label>Course :</label>
				<input    class="form-control" name="course_1" id="course_1" style="width:100%;" type="text"  value="" readonly />
			    </div>
				<div class="col-lg-3">
				<label class="">Sub Course :</label>
				<input   class="form-control" name="subcourse_1" id="subcourse_1"  style="width:100%;" type="text" readonly />
				</div>
				<div class="col-lg-3">
				<label>Type of training mode :</label>
				<select class="form-control" id="training_mode" name="training_mode" >
				<option value="">Select</option>
				<option value="Self Study">Self Study</option>
				<option value="Classroom">Classroom</option>
				<option value="Online(LVC)">Online(LVC)</option>
				</select>
			    </div>
				<div class="col-lg-3">
				<label>Location :</label>
				<select class="form-control" id="location" name="location" >
				<option value="">Select</option>
				<option value="Mumbai">Mumbai</option>
				<option value="Delhi">Delhi</option>
				<option value="Bangalore">Bangalore</option>
				<option value="Pune">Pune</option>
				<option value="Hyderabad">Hyderabad</option>
				<option value="Chennai">Chennai</option>
				<option value="Other">Other</option>
				</select>
			    </div>
				</div>
				
			    <div class="form-group row">
				<div class="col-lg-6">
				<label>Modules Selected :</label>
				<textarea class="form-control" name="modules_1" id="modules_1" style="width:100%;" type="text"  value="" readonly /></textarea>
			    </div>
				<div class="col-lg-6">
				<label>Course you have enrolled with us :</label>
				<select class="form-control" id="course_enrolled" name="course_enrolled" >
				<option value="">Select</option>
				<option value="CPA">CPA</option>
				<option value="CMA">CMA</option>
				<option value="ACCA">ACCA</option>
				<option value="CFA">CFA</option>
				<option value="FRM">FRM</option>
				<option value="FM">FM</option>
				<option value="BAT">BAT</option>
				<option value="DM">DM</option>
				</select>
				</div>
				</div>
				
				
				</div>
				<div class="tab-pane" id="kt_portlet_tab_1_2">
				
				
				<div id="cma_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
General Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you registered with IMA ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg" name="ima_reg" value="1" onclick="displaycma(this.value);">Yes
<input type="radio" id="ima_reg" name="ima_reg" value="0" onclick="displaycma(this.value);">No
</div>
<div class="col-lg-4" id='im_display' name='im_display'  style="display:none;">
<label class="">If yes Please provide the IMA Member ID / Customer ID :</label><br>
<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"  maxlength="12"   />
</div>
</div>

</div>
</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CMA Part I Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CMA Part I ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cma_1" name="ima_reg_cma_1" value="1" onclick="displaycmaat1(this.value);">Yes
<input type="radio" id="ima_reg_cma_1" name="ima_reg_cma_1" value="0" onclick="displaycmaat1(this.value);">No
</div>
<div class="col-lg-8" id='im_cma_1_display_apr' name='im_cma_1_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cma_exam_day_1' name='cma_exam_day_1'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cma_exam_month_1' name='cma_exam_month_1'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cma_exam_year_1' name='cma_exam_year_1'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cma_exam_result_1' name='cma_exam_result_1'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cma_1_display_atp' name='im_cma_1_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cma_apr_day_1' name='cma_apr_day_1'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cma_apr_month_1' name='cma_apr_month_1'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cma_apr_year_1' name='cma_apr_year_1'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CMA Part II Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CMA Part II ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cma_2" name="ima_reg_cma_2" value="1" onclick="displaycmaat2(this.value);">Yes
<input type="radio" id="ima_reg_cma_2" name="ima_reg_cma_2" value="0" onclick="displaycmaat2(this.value);">No
</div>
<div class="col-lg-8" id='im_cma_2_display_apr' name='im_cma_2_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cma_exam_day_2' name='cma_exam_day_2'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cma_exam_month_2' name='cma_exam_month_2'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cma_exam_year_2' name='cma_exam_year_2'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cma_exam_result_2' name='cma_exam_result_2'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cma_2_display_atp' name='im_cma_2_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cma_apr_day_2' name='cma_apr_day_2'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cma_apr_month_2' name='cma_apr_month_2'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cma_apr_year_2' name='cma_apr_year_2'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='cma_main_comments' name='cma_main_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>

</div>
<!--CMA EXAM PATCH END-->

<!--CPA EXAM PATCH-->
<div id="cpa_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CPA AUD Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CPA AUD ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cpa_aud" name="ima_reg_cpa_aud" value="1" onclick="displaycpaaud(this.value);">Yes
<input type="radio" id="ima_reg_cpa_aud" name="ima_reg_cpa_aud" value="0" onclick="displaycpaaud(this.value);">No
</div>
<div class="col-lg-8" id='im_cpa_aud_display_apr' name='im_cpa_aud_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cpa_aud_exam_day' name='cpa_aud_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cpa_aud_exam_month' name='cpa_aud_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cpa_aud_exam_year' name='cpa_aud_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cpa_aud_exam_result' name='cpa_aud_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cpa_aud_display_atp' name='im_cpa_aud_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cpa_aud_apr_day' name='cpa_aud_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cpa_aud_apr_month' name='cpa_aud_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cpa_aud_apr_year' name='cpa_aud_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CPA FAR Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CPA FAR ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cpa_far" name="ima_reg_cpa_far" value="1" onclick="displaycpafar(this.value);">Yes
<input type="radio" id="ima_reg_cpa_far" name="ima_reg_cpa_far" value="0" onclick="displaycpafar(this.value);">No
</div>
<div class="col-lg-8" id='im_cpa_far_display_apr' name='im_cpa_far_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cpa_far_exam_day' name='cpa_far_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cpa_far_exam_month' name='cpa_far_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cpa_far_exam_year' name='cpa_far_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cpa_far_exam_result' name='cpa_far_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cpa_far_display_atp' name='im_cpa_far_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cpa_far_apr_day' name='cpa_far_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cpa_far_apr_month' name='cpa_far_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cpa_far_apr_year' name='cpa_far_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CPA BEC Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CPA BEC ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cpa_bec" name="ima_reg_cpa_bec" value="1" onclick="displaycpabec(this.value);">Yes
<input type="radio" id="ima_reg_cpa_bec" name="ima_reg_cpa_bec" value="0" onclick="displaycpabec(this.value);">No
</div>
<div class="col-lg-8" id='im_cpa_bec_display_apr' name='im_cpa_bec_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cpa_bec_exam_day' name='cpa_bec_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cpa_bec_exam_month' name='cpa_bec_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cpa_bec_exam_year' name='cpa_bec_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cpa_bec_exam_result' name='cpa_bec_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cpa_bec_display_atp' name='im_cpa_bec_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cpa_bec_apr_day' name='cpa_bec_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cpa_bec_apr_month' name='cpa_bec_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cpa_bec_apr_year' name='cpa_bec_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CPA REG Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CPA REG ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_cpa_reg" name="ima_reg_cpa_reg" value="1" onclick="displaycpareg(this.value);">Yes
<input type="radio" id="ima_reg_cpa_reg" name="ima_reg_cpa_reg" value="0" onclick="displaycpareg(this.value);">No
</div>
<div class="col-lg-8" id='im_cpa_reg_display_apr' name='im_cpa_reg_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cpa_reg_exam_day' name='cpa_reg_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cpa_reg_exam_month' name='cpa_reg_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cpa_reg_exam_year' name='cpa_reg_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cpa_reg_exam_result' name='cpa_reg_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cpa_reg_display_atp' name='im_cpa_reg_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cpa_reg_apr_day' name='cpa_reg_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cpa_reg_apr_month' name='cpa_reg_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cpa_reg_apr_year' name='cpa_reg_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='cpa_comments' name='cpa_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>

</div>
<!--CPA EXAM PATCH END-->
				
				
<!--FM EXAM PATCH-->
<div id="fm_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
FM Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted FM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_fm" name="ima_reg_fm" value="1" onclick="displayfm(this.value);">Yes
<input type="radio" id="ima_reg_fm" name="ima_reg_fm" value="0" onclick="displayfm(this.value);">No
</div>
<div class="col-lg-8" id='im_fm_display_apr' name='im_fm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='fm_exam_day' name='fm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='fm_exam_month' name='fm_exam_month'  class="form-control">

<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>


</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='fm_exam_year' name='fm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='fm_exam_result' name='fm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_fm_display_atp' name='im_fm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='fm_apr_day' name='fm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='fm_apr_month' name='fm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='fm_apr_year' name='fm_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='fm_comments' name='fm_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
<!--FM END-->
				
				
				
			<!--CFA EXAM PATCH-->
<div id="cfa_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
General Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you registered with any CFA Institute ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="cfa_insta" name="cfa_insta" value="1" onclick="displaycfainsta(this.value);">Yes
<input type="radio" id="cfa_insta" name="cfa_insta" value="0" onclick="displaycfainsta(this.value);">No
</div>
<div class="col-lg-4" id='cfainsta_display' name='cfainsta_display'  style="display:none;">
<label class="">If yes Please provide the CFA institute ID :</label><br>
<input   class="form-control"  name="cfa_insta_id" id="cfa_insta_id"  style="width:100%;" type="text"    />
</div>
</div>

</div>
</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CFA Level I Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CFA Level I ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_cfa_1" name="ima_cfa_1" value="1" onclick="displaycfaat1(this.value);">Yes
<input type="radio" id="ima_cfa_1" name="ima_cfa_1" value="0" onclick="displaycfaat1(this.value);">No
</div>
<div class="col-lg-8" id='im_cfa_1_display_apr' name='im_cfa_1_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cfa1_exam_day' name='cfa1_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cfa1_exam_month' name='cfa1_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cfa1_exam_year' name='cfa1_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cfa1_exam_result' name='cma_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cfa_1_display_atp' name='im_cfa_1_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cfa1_apr_day' name='cfa1_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cfa1_apr_month' name='cfa1_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cfa1_apr_year' name='cfa1_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
CFA Level II Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted CFA Level II ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_cfa_2" name="ima_cfa_2" value="1" onclick="displaycfaat2(this.value);">Yes
<input type="radio" id="ima_cfa_2" name="ima_cfa_2" value="0" onclick="displaycfaat2(this.value);">No
</div>
<div class="col-lg-8" id='im_cfa_2_display_apr' name='im_cfa_2_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='cfa2_exam_day' name='cfa2_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='cfa2_exam_month' name='cfa2_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='cfa2_exam_year' name='cfa2_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='cfa2_exam_result' name='cfa2_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_cfa_2_display_atp' name='im_cfa_2_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='cfa2_apr_day' name='cfa2_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='cfa2_apr_month' name='cfa2_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='cfa2_apr_year' name='cfa2_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='cfa_comments' name='cfa_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>

</div>
<!--CMA EXAM PATCH END-->







<!--FRM EXAM PATCH-->
<div id="frm_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
General Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you have FRM GARP ID yet ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="frm_insta" name="frm_insta" value="1" onclick="displayfrminsta(this.value);">Yes
<input type="radio" id="frm_insta" name="frm_insta" value="0" onclick="displayfrminsta(this.value);">No
</div>
<div class="col-lg-4" id='frminsta_display' name='frminsta_display'  style="display:none;">
<label class="">If yes Please provide the FRM GARP ID :</label><br>
<input   class="form-control"  name="frm_insta_id" id="frm_insta_id"  style="width:100%;" type="text"    />
</div>
</div>

</div>
</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
FRM Information
</h3>
</div>
</div>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted FRM Exam?:*</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_frm" name="ima_reg_frm" value="1" onclick="displayfrm(this.value);" >Yes
<input type="radio" id="ima_reg_frm" name="ima_reg_frm" value="0" onclick="displayfrm(this.value);" >No
</div>
<div class="col-lg-8" id='im_frm_display_apr' name='im_frm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='frm_exam_day' name='frm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='frm_exam_month' name='frm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='frm_exam_year' name='frm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='frm_exam_result' name='frm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_frm_display_atp' name='im_frm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='frm_apr_day' name='frm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='frm_apr_month' name='frm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='frm_apr_year' name='frm_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='frm_comments' name='frm_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
<!--FRM END-->



<!--BAT EXAM PATCH-->
<div id="bat_patch" style="display:none;">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
BAT Information
</h3>
</div>
</div>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted BAT ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_bat" name="ima_reg_bat" value="1" onclick="displaybat(this.value);">Yes
<input type="radio" id="ima_reg_bat" name="ima_reg_bat" value="0" onclick="displaybat(this.value);">No
</div>
<div class="col-lg-8" id='im_bat_display_apr' name='im_bat_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='bat_exam_day' name='bat_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='bat_exam_month' name='bat_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='bat_exam_year' name='bat_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='bat_exam_result' name='bat_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_bat_display_atp' name='im_bat_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='bat_apr_day' name='bat_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='bat_apr_month' name='bat_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='bat_apr_year' name='bat_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='bat_comments' name='bat_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
<!--BAT END-->



<!--DM EXAM PATCH-->
<div id="dm_patch" style="display:none;">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
DM Information
</h3>
</div>
</div>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted DM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_dm" name="ima_reg_dm" value="1" onclick="displaydm(this.value);">Yes
<input type="radio" id="ima_reg_dm" name="ima_reg_dm" value="0" onclick="displaydm(this.value);">No
</div>
<div class="col-lg-8" id='im_dm_display_apr' name='im_dm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='dm_exam_day' name='dm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='dm_exam_month' name='dm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='dm_exam_year' name='dm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='dm_exam_result' name='dm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_dm_display_atp' name='im_dm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='dm_apr_day' name='dm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='dm_apr_month' name='dm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='dm_apr_year' name='dm_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='dm_comments' name='dm_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
<!--DM END-->



<!--ACCA EXAM PATCH-->
<div id="acca_patch" style="display:none;">

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
General Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you registered with ACCA ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_acca" name="ima_acca" value="1" onclick="displayacca(this.value);">Yes
<input type="radio" id="ima_acca" name="ima_acca" value="0" onclick="displayacca(this.value);">No
</div>
<div class="col-lg-3" id='im_acca_display' name='im_acca_display'  style="display:none;">
<label class="">Please provide ACCA Registration id ( 7 digit) :</label><br>
<input   class="form-control"  name="acca_reg_id" id="acca_reg_id" maxlength="7" style="width:100%;" type="text"     />
</div>
<div class="col-lg-3">
<label class="">Please mark the consent to share the data with ACCA? :</label>
<br>
</div>
<div class="col-lg-2">
<input  type="radio" id="acca_consent" name="acca_consent" value="1">Yes
<input type="radio" id="acca_consent" name="acca_consent" value="0">No
</div>
</div>

<div class="form-group row">
<div class="col-lg-1">
<label class="">Surname: </label>
<br>
</div>
<div class="col-lg-3">
<input   class="form-control"  name="acca_reg_surname" id="acca_reg_surname"  style="width:100%;" type="text"     />
</div>
<div class="col-lg-1">
<label class="">Firstname: </label>
<br>
</div>
<div class="col-lg-3" >
<input   class="form-control"  name="acca_reg_firstname" id="acca_reg_firstname"  style="width:100%;" type="text"     />
</div>
<div class="col-lg-1">
<label class="">DOB: </label>
<br>
</div>
<div class="col-lg-3" >
<input   class="form-control"  name="acca_reg_dob" id="acca_reg_dob"  style="width:100%;" type="date"     />
</div>
</div>

</div>



</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Financial Reporting (FR) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA FR ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_fr" name="ima_reg_acca_fr" value="1" onclick="displayaccafr(this.value);">Yes
<input type="radio" id="ima_reg_acca_fr" name="ima_reg_acca_fr" value="0" onclick="displayaccafr(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_fr_display_apr' name='im_acca_fr_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_fr_exam_day' name='acca_fr_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_fr_exam_month' name='acca_fr_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_fr_exam_year' name='acca_fr_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_fr_exam_result' name='acca_fr_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_fr_display_atp' name='im_acca_fr_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_fr_apr_day' name='acca_fr_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_fr_apr_month' name='acca_fr_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_fr_apr_year' name='acca_fr_apr_year'  class="form-control">
echo "<option value='0'>Select</option>";
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>



<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Financial Management (FM) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA FM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_fm" name="ima_reg_acca_fm" value="1" onclick="displayaccafm(this.value);">Yes
<input type="radio" id="ima_reg_acca_fm" name="ima_reg_acca_fm" value="0" onclick="displayaccafm(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_fm_display_apr' name='im_acca_fm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_fm_exam_day' name='acca_fm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_fm_exam_month' name='acca_fm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_fm_exam_year' name='acca_fm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_fm_exam_result' name='acca_fm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_fm_display_atp' name='im_acca_fm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_fm_apr_day' name='acca_fm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_fm_apr_month' name='acca_fm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_fm_apr_year' name='acca_fm_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Strategic Business Leader (SBL) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA SBL ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_sbl" name="ima_reg_acca_sbl" value="1" onclick="displayaccasbl(this.value);">Yes
<input type="radio" id="ima_reg_acca_sbl" name="ima_reg_acca_sbl" value="0" onclick="displayaccasbl(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_sbl_display_apr' name='im_acca_sbl_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_sbl_exam_day' name='acca_sbl_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_sbl_exam_month' name='acca_sbl_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_sbl_exam_year' name='acca_sbl_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_sbl_exam_result' name='acca_sbl_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_sbl_display_atp' name='im_acca_sbl_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_sbl_apr_day' name='acca_sbl_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_sbl_apr_month' name='acca_sbl_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_sbl_apr_year' name='acca_sbl_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Strategic Business Reporting (SBR) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA SBR ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_sbr" name="ima_reg_acca_sbr" value="1" onclick="displayaccasbr(this.value);">Yes
<input type="radio" id="ima_reg_acca_sbr" name="ima_reg_acca_sbr" value="0" onclick="displayaccasbr(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_sbr_display_apr' name='im_acca_sbr_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_sbr_exam_day' name='acca_sbr_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_sbr_exam_month' name='acca_sbr_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_sbr_exam_year' name='acca_sbr_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_sbr_exam_result' name='acca_sbr_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_sbr_display_atp' name='im_acca_sbr_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_sbr_apr_day' name='acca_sbr_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_sbr_apr_month' name='acca_sbr_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_sbr_apr_year' name='acca_sbr_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Management Accounting (MA) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA MA ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_ma" name="ima_reg_acca_ma" value="1" onclick="displayaccama(this.value);">Yes
<input type="radio" id="ima_reg_acca_ma" name="ima_reg_acca_ma" value="0" onclick="displayaccama(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_ma_display_apr' name='im_acca_ma_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_ma_exam_day' name='acca_ma_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_ma_exam_month' name='acca_ma_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_ma_exam_year' name='acca_ma_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_ma_exam_result' name='acca_ma_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_ma_display_atp' name='im_acca_ma_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_ma_apr_day' name='acca_ma_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_ma_apr_month' name='acca_ma_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_ma_apr_year' name='acca_ma_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Business and Technology (BT) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA BT ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_bt" name="ima_reg_acca_bt" value="1" onclick="displayaccabt(this.value);">Yes
<input type="radio" id="ima_reg_acca_bt" name="ima_reg_acca_bt" value="0" onclick="displayaccabt(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_bt_display_apr' name='im_acca_bt_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_bt_exam_day' name='acca_bt_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_bt_exam_month' name='acca_bt_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_bt_exam_year' name='acca_bt_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_bt_exam_result' name='acca_bt_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_bt_display_atp' name='im_acca_bt_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_bt_apr_day' name='acca_bt_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_bt_apr_month' name='acca_bt_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_bt_apr_year' name='acca_bt_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Corporate and Business Law (LW) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA LW ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_lw" name="ima_reg_acca_lw" value="1" onclick="displayaccalw(this.value);">Yes
<input type="radio" id="ima_reg_acca_lw" name="ima_reg_acca_lw" value="0" onclick="displayaccalw(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_lw_display_apr' name='im_acca_lw_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_lw_exam_day' name='acca_lw_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_lw_exam_month' name='acca_lw_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_lw_exam_year' name='acca_lw_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_lw_exam_result' name='acca_lw_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_lw_display_atp' name='im_acca_lw_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_lw_apr_day' name='acca_lw_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_lw_apr_month' name='acca_lw_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>

<select id='acca_lw_apr_year' name='acca_lw_apr_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Taxation (TX) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA TX ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_tx" name="ima_reg_acca_tx" value="1" onclick="displayaccatx(this.value);">Yes
<input type="radio" id="ima_reg_acca_tx" name="ima_reg_acca_tx" value="0" onclick="displayaccatx(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_tx_display_apr' name='im_acca_tx_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_tx_exam_day' name='acca_tx_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_tx_exam_month' name='acca_tx_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_tx_exam_year' name='acca_tx_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_tx_exam_result' name='acca_tx_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_tx_display_atp' name='im_acca_tx_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_tx_apr_day' name='acca_tx_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_tx_apr_month' name='acca_tx_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_tx_apr_year' name='acca_tx_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Financial Accounting (FA) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA FA ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_fa" name="ima_reg_acca_fa" value="1" onclick="displayaccafa(this.value);">Yes
<input type="radio" id="ima_reg_acca_fa" name="ima_reg_acca_fa" value="0" onclick="displayaccafa(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_fa_display_apr' name='im_acca_fa_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_fa_exam_day' name='acca_fa_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_fa_exam_month' name='acca_fa_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_fa_exam_year' name='acca_fa_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_fa_exam_result' name='acca_fa_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_fa_display_atp' name='im_acca_fa_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_fa_apr_day' name='acca_fa_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_fa_apr_month' name='acca_fa_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_fa_apr_year' name='acca_fa_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Performance Management (PM) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA PM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_pm" name="ima_reg_acca_pm" value="1" onclick="displayaccapm(this.value);">Yes
<input type="radio" id="ima_reg_acca_pm" name="ima_reg_acca_pm" value="0" onclick="displayaccapm(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_pm_display_apr' name='im_acca_pm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_pm_exam_day' name='acca_pm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_pm_exam_month' name='acca_pm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_pm_exam_year' name='acca_pm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_pm_exam_result' name='acca_pm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_pm_display_atp' name='im_acca_pm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_pm_apr_day' name='acca_pm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_pm_apr_month' name='acca_pm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_pm_apr_year' name='acca_pm_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Audit and Assurance (AA) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA AA ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_aa" name="ima_reg_acca_aa" value="1" onclick="displayaccaaa(this.value);">Yes
<input type="radio" id="ima_reg_acca_aa" name="ima_reg_acca_aa" value="0" onclick="displayaccaaa(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_aa_display_apr' name='im_acca_aa_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_aa_exam_day' name='acca_aa_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_aa_exam_month' name='acca_aa_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_aa_exam_year' name='acca_aa_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_aa_exam_result' name='acca_aa_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_aa_display_atp' name='im_acca_aa_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_aa_apr_day' name='acca_aa_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_aa_apr_month' name='acca_aa_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_aa_apr_year' name='acca_aa_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Advanced Financial Management (AFM) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA AFM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_afm" name="ima_reg_acca_afm" value="1" onclick="displayaccaafm(this.value);">Yes
<input type="radio" id="ima_reg_acca_afm" name="ima_reg_acca_afm" value="0" onclick="displayaccaafm(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_afm_display_apr' name='im_acca_afm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_afm_exam_day' name='acca_afm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_afm_exam_month' name='acca_afm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_afm_exam_year' name='acca_afm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_afm_exam_result' name='acca_afm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_afm_display_atp' name='im_acca_afm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_afm_apr_day' name='acca_afm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_afm_apr_month' name='acca_afm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_afm_apr_year' name='acca_afm_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
ACCA Advanced Performance Management (APM) Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Have you Attempted ACCA APM ?:</label>
<br>
</div>
<div class="col-lg-1">
<input  type="radio" id="ima_reg_acca_apm" name="ima_reg_acca_apm" value="1" onclick="displayaccaapm(this.value);">Yes
<input type="radio" id="ima_reg_acca_apm" name="ima_reg_acca_apm" value="0" onclick="displayaccaapm(this.value);">No
</div>
<div class="col-lg-8" id='im_acca_apm_display_apr' name='im_acca_apm_display_apr'  style="display:none;">

<label class="">Exam Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class="">Exam Day:</label>
<select id='acca_apm_exam_day' name='acca_apm_exam_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Exam Month:</label>
<select id='acca_apm_exam_month' name='acca_apm_exam_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class="">Exam Year:</label>

<select id='acca_apm_exam_year' name='acca_apm_exam_year'  class="form-control">
<option value='0'>Select</option>
<?php
for($k=2019;$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>
<label class="">Result:</label>

<select id='acca_apm_exam_result' name='acca_apm_exam_result'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Pass</option>
<option value='2'>Fail</option>
</select>
</td>
</tr>
</table>
</div>



<div class="col-lg-8" id='im_acca_apm_display_atp' name='im_acca_apm_display_atp'  style="display:none;">

<label class="">Appearing Date:</label>
<br>
<table style="width:100%;">
<tr>
<td>
<label class=""> Day:</label>
<select id='acca_apm_apr_day' name='acca_apm_apr_day'  class="form-control">
<?php
echo "<option value='0'>Select</option>";
for($i=1;$i<=31;$i++)
{
echo "<option value='".$i."'>".$i."</option>"	;
}
?>
</select>
</td>
<td>
<label class=""> Month:</label>
<select id='acca_apm_apr_month' name='acca_apm_apr_month'  class="form-control">
<option value='0'>Select</option>
<option value='1'>Jan</option>
<option value='2'>Feb</option>
<option value='3'>Mar</option>
<option value='4'>April</option>
<option value='5'>May</option>
<option value='6'>Jun</option>
<option value='7'>Jul</option>
<option value='8'>Aug</option>
<option value='9'>Sep</option>
<option value='10'>Oct</option>
<option value='11'>Nov</option>
<option value='12'>Dec</option>
</select>
</td>
<td>
<label class=""> Year:</label>
<select id='acca_apm_apr_year' name='acca_apm_apr_year'  class="form-control">
<option value='0'>Select</option>";
<?php
for($k=date('Y');$k<=2030;$k++)
{
echo "<option value='".$k."'>".$k."</option>"	;
}
?>
</select>
</td>
<td>

</td>
</tr>
</table>
</div>
</div>
</div>

</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="">Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='acca_comments' name='acca_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>

</div>
<!--ACCA EXAM PATCH END-->
				
				

				
				
				
			</div>
		</div>
	</div>

	<!--end::Portlet-->
	<div class="kt-portlet__foot kt-portlet__foot--fit-x">
	<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="approve" name="approve" type="button" class="btn btn-success" onclick="checkvalues();">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
	</div>
	</div>
</form>
</div>

<?php
?>
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function RECSelect()
{
	var val = document.getElementById("accountno_1").value;
	if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById("stu_exm_det").reset();
			document.getElementById('recs_1').value = output["account_no"];
			document.getElementById('acc_id').value = output["accountid"];
			var str = output["modules"];
            var res = str.split("|##|");
			document.getElementById('modules_1').value = res;
			document.getElementById('studentname_1').value = output["studentname"];
			document.getElementById('acca_reg_surname').value = output["last_name"];
			document.getElementById('acca_reg_firstname').value = output["first_name"];
			document.getElementById('assignedto_1').value = output["assigned_user"];
			document.getElementById('emailid_1').value = output["email"];
			document.getElementById('mobileno_1').value = output["phone"];
			document.getElementById('course_1').value = output["course"];
			document.getElementById('subcourse_1').value = output["subcourse"];
			document.getElementById('course_enrolled').value = output["course"];
			document.getElementById('location').value = output["batch_location"];
			
			if(output["course"]=='ACCA')
			{
				document.getElementById("acca_patch").style.display="block";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CPA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="block";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CMA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="block";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CFA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="block";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='FM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="block";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='FRM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="block";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='DM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="block";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='BAT')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="block";
			}
		
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_studentdetails.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}


function displaycma(val)
{

if(val==1)
{
document.getElementById('im_display').style.display='block';
document.getElementById('ima_mem_id').value='';

}
else if (val==0)
{
document.getElementById('im_display').style.display='none';
document.getElementById('ima_mem_id').value='';
}

}


function displaycmaat1(val)
{

if(val==1)
{
document.getElementById('im_cma_1_display_apr').style.display='block';
document.getElementById('im_cma_1_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cma_1_display_apr').style.display='none';
document.getElementById('im_cma_1_display_atp').style.display='block';
}

}


function displaycmaat2(val)
{

if(val==1)
{
document.getElementById('im_cma_2_display_apr').style.display='block';
document.getElementById('im_cma_2_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cma_2_display_apr').style.display='none';
document.getElementById('im_cma_2_display_atp').style.display='block';
}

}

function displaycpaaud(val)
{

if(val==1)
{
document.getElementById('im_cpa_aud_display_apr').style.display='block';
document.getElementById('im_cpa_aud_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cpa_aud_display_apr').style.display='none';
document.getElementById('im_cpa_aud_display_atp').style.display='block';
}

}


function displaycpafar(val)
{

if(val==1)
{
document.getElementById('im_cpa_far_display_apr').style.display='block';
document.getElementById('im_cpa_far_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cpa_far_display_apr').style.display='none';
document.getElementById('im_cpa_far_display_atp').style.display='block';
}

}

function displaycpabec(val)
{

if(val==1)
{
document.getElementById('im_cpa_bec_display_apr').style.display='block';
document.getElementById('im_cpa_bec_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cpa_bec_display_apr').style.display='none';
document.getElementById('im_cpa_bec_display_atp').style.display='block';
}

}

function displaycpareg(val)
{

if(val==1)
{
document.getElementById('im_cpa_reg_display_apr').style.display='block';
document.getElementById('im_cpa_reg_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cpa_reg_display_apr').style.display='none';
document.getElementById('im_cpa_reg_display_atp').style.display='block';
}

}


function displayfm(val)
{

if(val==1)
{
document.getElementById('im_fm_display_apr').style.display='block';
document.getElementById('im_fm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_fm_display_apr').style.display='none';
document.getElementById('im_fm_display_atp').style.display='block';
}

}



function displaycfaat1(val)
{

if(val==1)
{
document.getElementById('im_cfa_1_display_apr').style.display='block';
document.getElementById('im_cfa_1_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cfa_1_display_apr').style.display='none';
document.getElementById('im_cfa_1_display_atp').style.display='block';
}

}


function displaycfaat2(val)
{

if(val==1)
{
document.getElementById('im_cfa_2_display_apr').style.display='block';
document.getElementById('im_cfa_2_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_cfa_2_display_apr').style.display='none';
document.getElementById('im_cfa_2_display_atp').style.display='block';
}

}


function displaycma(val)
{

if(val==1)
{
document.getElementById('im_display').style.display='block';
document.getElementById('ima_mem_id').value='';

}
else if (val==0)
{
document.getElementById('im_display').style.display='none';
document.getElementById('ima_mem_id').value='';
}

}

function displaycfainsta(val)
{

if(val==1)
{
document.getElementById('cfainsta_display').style.display='block';
document.getElementById('cfa_insta_id').value='';

}
else if (val==0)
{
document.getElementById('cfainsta_display').style.display='none';
document.getElementById('cfa_insta_id').value='';
}

}


function displayfrminsta(val)
{

if(val==1)
{
document.getElementById('frminsta_display').style.display='block';
document.getElementById('frm_insta_id').value='';

}
else if (val==0)
{
document.getElementById('frminsta_display').style.display='none';
document.getElementById('frm_insta_id').value='';
}

}


function displayfrm(val)
{

if(val==1)
{
document.getElementById('im_frm_display_apr').style.display='block';
document.getElementById('im_frm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_frm_display_apr').style.display='none';
document.getElementById('im_frm_display_atp').style.display='block';
}

}

function displaybat(val)
{

if(val==1)
{
document.getElementById('im_bat_display_apr').style.display='block';
document.getElementById('im_bat_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_bat_display_apr').style.display='none';
document.getElementById('im_bat_display_atp').style.display='block';
}

}



function displaydm(val)
{

if(val==1)
{
document.getElementById('im_dm_display_apr').style.display='block';
document.getElementById('im_dm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_dm_display_apr').style.display='none';
document.getElementById('im_dm_display_atp').style.display='block';
}

}



function displayaccafr(val)
{
if(val==1)
{
document.getElementById('im_acca_fr_display_apr').style.display='block';
document.getElementById('im_acca_fr_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_fr_display_apr').style.display='none';
document.getElementById('im_acca_fr_display_atp').style.display='block';
}
}

function displayaccafm(val)
{
if(val==1)
{
document.getElementById('im_acca_fm_display_apr').style.display='block';
document.getElementById('im_acca_fm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_fm_display_apr').style.display='none';
document.getElementById('im_acca_fm_display_atp').style.display='block';
}
}

function displayaccasbl(val)
{
if(val==1)
{
document.getElementById('im_acca_sbl_display_apr').style.display='block';
document.getElementById('im_acca_sbl_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_sbl_display_apr').style.display='none';
document.getElementById('im_acca_sbl_display_atp').style.display='block';
}
}


function displayaccasbr(val)
{
if(val==1)
{
document.getElementById('im_acca_sbr_display_apr').style.display='block';
document.getElementById('im_acca_sbr_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_sbr_display_apr').style.display='none';
document.getElementById('im_acca_sbr_display_atp').style.display='block';
}
}

function displayaccama(val)
{
if(val==1)
{
document.getElementById('im_acca_ma_display_apr').style.display='block';
document.getElementById('im_acca_ma_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_ma_display_apr').style.display='none';
document.getElementById('im_acca_ma_display_atp').style.display='block';
}
}

function displayaccabt(val)
{
if(val==1)
{
document.getElementById('im_acca_bt_display_apr').style.display='block';
document.getElementById('im_acca_bt_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_bt_display_apr').style.display='none';
document.getElementById('im_acca_bt_display_atp').style.display='block';
}
}


function displayaccalw(val)
{
if(val==1)
{
document.getElementById('im_acca_lw_display_apr').style.display='block';
document.getElementById('im_acca_lw_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_lw_display_apr').style.display='none';
document.getElementById('im_acca_lw_display_atp').style.display='block';
}
}

function displayaccatx(val)
{
if(val==1)
{
document.getElementById('im_acca_tx_display_apr').style.display='block';
document.getElementById('im_acca_tx_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_tx_display_apr').style.display='none';
document.getElementById('im_acca_tx_display_atp').style.display='block';
}
}




function displayaccafa(val)
{
if(val==1)
{
document.getElementById('im_acca_fa_display_apr').style.display='block';
document.getElementById('im_acca_fa_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_fa_display_apr').style.display='none';
document.getElementById('im_acca_fa_display_atp').style.display='block';
}
}


function displayaccapm(val)
{
if(val==1)
{
document.getElementById('im_acca_pm_display_apr').style.display='block';
document.getElementById('im_acca_pm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_pm_display_apr').style.display='none';
document.getElementById('im_acca_pm_display_atp').style.display='block';
}
}


function displayaccaaa(val)
{
if(val==1)
{
document.getElementById('im_acca_aa_display_apr').style.display='block';
document.getElementById('im_acca_aa_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_aa_display_apr').style.display='none';
document.getElementById('im_acca_aa_display_atp').style.display='block';
}
}

function displayaccaafm(val)
{
if(val==1)
{
document.getElementById('im_acca_afm_display_apr').style.display='block';
document.getElementById('im_acca_afm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_afm_display_apr').style.display='none';
document.getElementById('im_acca_afm_display_atp').style.display='block';
}
}

function displayaccaapm(val)
{
if(val==1)
{
document.getElementById('im_acca_apm_display_apr').style.display='block';
document.getElementById('im_acca_apm_display_atp').style.display='none';

}
else if (val==0)
{
document.getElementById('im_acca_apm_display_apr').style.display='none';
document.getElementById('im_acca_apm_display_atp').style.display='block';
}
}


function displayacca(val)
{

if(val==1)
{
document.getElementById('im_acca_display').style.display='block';
document.getElementById('acca_reg_id').value='';

}
else if (val==0)
{
document.getElementById('im_acca_display').style.display='none';
document.getElementById('acca_reg_id').value='';
}

}


function checker(val) {            
        var radio_check_val = "";
        for (i = 0; i < document.getElementsByName(val).length; i++) {
            if (document.getElementsByName(val)[i].checked) {
               // alert("this radio button was clicked: " + document.getElementsByName(val)[i].value);
                radio_check_val = document.getElementsByName(val)[i].value;        
            }        
        }
        if (radio_check_val === "")
        {
            alert("Please select relevant options or answers");
        }
		else
		{
			document.getElementById("stu_exm_det").submit();
		}
        
    }

function checkvalues()
{
	var course=document.getElementById("course_1").value;
	if(course=='CMA')
	{
		checker('ima_reg');
	}
	if(course=='ACCA')
	{
		checker('ima_acca');
	}
	if(course=='CFA')
	{
		checker('cfa_insta');
	}
	if(course=='BAT')
	{
		checker('ima_reg_bat');
	}
	if(course=='CPA')
	{
		checker('ima_reg_cpa_aud');
	}
    if(course=='DM')
	{
		checker('ima_reg_dm');
	}
	if(course=='FM')
	{
		checker('ima_reg_fm');
	}
	if(course=='FRM')
	{
		checker('frm_insta');
	}
	
	
	
}



function displaycmamarks1(checked)
{

if(checked==true)
{
document.getElementById('cma1_marks').style.display='block';
document.getElementById('is_cma_part1_marks').value='';
}
else if(checked==false)
{
document.getElementById('cma1_marks').style.display='none';
document.getElementById('is_cma_part1_marks').value='';
}
}


function displaycmamarks2(checked)
{

if(checked==true)
{
document.getElementById('cma2_marks').style.display='block';
document.getElementById('is_cma_part2_marks').value='';
}
else if(checked==false)
{
document.getElementById('cma2_marks').style.display='none';
document.getElementById('is_cma_part2_marks').value='';
}
}

function displaycmamarks3(checked)
{

if(checked==true)
{

document.getElementById('cma1_marks').style.display='none';
document.getElementById('is_cma_part1_marks').value='';
document.getElementById('cma2_marks').style.display='none';
document.getElementById('is_cma_part2_marks').value='';
}
else if(checked==false)
{

}
}
//cma chunk end

</script>