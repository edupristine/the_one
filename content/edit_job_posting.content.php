<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$id = get_get_value('id');	
$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$con_one=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');


$course="SELECT distinct(course_name) as `course` FROM courses where course_name in ('Corporate','ACCA','CPA','CMA','USMLE','FRM','FM','DM','CFA','BAT','USMLE CMTC','Analytics','Ecom-CFA')ORDER BY course_name";
$resultc = mysqli_query($con_one,$course);
while($rc = mysqli_fetch_assoc($resultc)) {
$rowsc[] = $rc;
}

$qry1 = "SELECT * FROM job_locations order by location_name";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}



$qry1l = "SELECT *,DATE_FORMAT(end_date,'%Y-%m-%d') as enddt,DATE_FORMAT(start_date,'%Y-%m-%d') as sdt FROM job_postings where id=".$id;
$resls = mysqli_query($edu,$qry1l);
$rjp = mysqli_fetch_assoc($resls);

	
	if(isset($_GET['dupname1']))
	{
		$msg = "Job Posting Update Failed. Similiar Posting  already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Job Posting Update Failed. Similiar Posting already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Job Posting Update Failed.Mandatory Fields missing.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Job Posting Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Job Posting Update Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Edit Job Posting
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/edit_job_posting.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
<label>Select Course</label>
<select id="course" class="form-control" name="course" required>
<option value="0">--Select--</option>
<?php
foreach($rowsc as $courses)
{
if($rjp['course']==$courses['course'])	
{
?>
<option value="<?php echo $courses['course'];?>" selected><?php echo $courses['course'];?></option>
<?php
}else
{
?>	
<option value="<?php echo $courses['course'];?>" ><?php echo $courses['course'];?></option>	
<?php
}
}
?>
</select >
</div>

<div class="col-lg-4">
<label>Select Vertical</label>
<select id="vertical"  class="form-control" name="vertical"  >
<option value="0">--Select--</option>
<?php

if($rjp['vertical']==1)
{
echo 
"<option value='1' selected>Accounts</option>
<option value='2'>Finance</option>
<option value='3'>Both Accounts & Finance</option>";
}else if($rjp['vertical']==2)
{
echo 
"<option value='1' >Accounts</option>
<option value='2' selected>Finance</option>
<option value='3'>Both Accounts & Finance</option>";
}else if($rjp['vertical']==3)
{
echo 
"<option value='1' >Accounts</option>
<option value='2' >Finance</option>
<option value='3' selected>Both Accounts & Finance</option>";
}else{
echo "
<option value='1' >Accounts</option>
<option value='2'>Finance</option>
<option value='3'>Both Accounts & Finance</option>";
}


?>
</select >
</div>
<div class="col-lg-4">
<label>Select Job Type</label>
<select id="job_type" class="form-control" name="job_type" >
<?php

if($rjp['job_type']==1)
{
echo 
"<option value='1' selected>Internal</option>
<option value='2'>External</option>";
}else if($rjp['job_type']==2)
{
echo 
"<option value='1' >Internal</option>
<option value='2' selected>External</option>";
}else{
echo "<option value='1' >Internal</option>
<option value='2' >External</option>";
}
?>
</select >

</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
	<label>Job Title 1</label>
	<input type="text" class="form-control" id="job_title_1" name="job_title_1" value="<?php echo $rjp['job_title_1']?>"required >
	<input type="hidden" class="form-control" id="jpid" name="jpid" value="<?php echo $rjp['id']?>" >
</div>
<div class="col-lg-6">
	<label>Job Title 2</label>
	<input type="text" class="form-control" id="job_title_2" name="job_title_2" value="<?php echo $rjp['job_title_2']?>">
	
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label>Company Logo:</label>
<input type="file" class="form-control" name="company_logo" id="company_logo" >
	
</div>
<div class="col-lg-6">
	<label>Company Name</label>
	<input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo $rjp['company_name']?>" required>
</div>
</div>

<div class="form-group row">
<div class="col-lg-4">
<label>Qualification:</label>
<input type="text" class="form-control" name="qualification" id="qualification"  value="<?php echo $rjp['qualification']?>"required >
	
</div>
<div class="col-lg-4">
<label>Salary:</label>
<input type="number" class="form-control" name="salary" id="salary"value="<?php echo $rjp['salary']?>" required>
	
</div>
<div class="col-lg-4">
<label>Job Location</label>
<select class="form-control kt-select2" id="kt_select2_3" name="param[]" multiple="multiple" >
<option value="0">--Select--</option>
<?php
foreach($rows as $location)
{
$final_locations = array();	
$qry_loc = "SELECT id FROM job_locations where id in (".$rjp['job_locations'].") order by id desc";
$res = mysqli_query($edu,$qry_loc);
while($rl = mysqli_fetch_assoc($res)) {
    $final_locations[] = $rl['id'];
}
print_r($final_locations);

if (in_array($location['id'], $final_locations))
{
?>
<option value="<?php echo $location['id'];?>" selected><?php echo $location['location_name'];?></option>
<?php
}
else
{
?>
<option value="<?php echo $location['id'];?>" ><?php echo $location['location_name'];?></option>
<?php	
}
}
?>
</select >

</div>
</div>


<div class="form-group row">
<div class="col-lg-4">
<label>Experience:</label>
<input type="text" class="form-control" name="experience" id="experience"value="<?php echo $rjp['experience']?>">
	
</div>
<div class="col-lg-4">
<label>Date Posted:</label>
<input type="date" class="form-control" id="start_date"  name="start_date" value = "<?php echo $rjp['sdt']; ?>" readonly>
	
</div>
<div class="col-lg-4">
<label>Job Expires On</label>
<input type="date" class="form-control" id="end_date"  name="end_date" value = "<?php echo $rjp['enddt']; ?>" required>

</div>
</div>


<div class="form-group row">
<div class="col-lg-12">
<label>About Company:</label>
<textarea class="form-control" id="about_company" name="about_company"><?php echo $rjp['about_company']?></textarea>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label>Job Description:</label>
<textarea class="form-control" id="job_description" name="job_description" required><?php echo $rjp['job_description']?></textarea></div>
</div>


<div class="form-group row">
<div class="col-lg-12">
<label>Skills and Requirement:</label>
<textarea class="form-control" id="skillset" name="skillset"><?php echo $rjp['skillset']?></textarea>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label>Education:</label>
<textarea class="form-control" id="education" name="education"><?php echo $rjp['education']?></textarea></div>
</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
			<center><button type="submit" class="btn btn-success">Update</button>
			<button type="reset" class="btn btn-secondary">Cancel</button></center>
		</div>
	</div>
</div>
</div>
</form>


<?php /*
<div class="kt-portlet__body">
<input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="">
<thead>
<tr>
<th >#</th>
<th >Location Name</th>
<th ><center>Action</center></th>
</tr>
</thead>
<tbody>
<?php
$i=1;
foreach($rows as $locs){
	
?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $locs['location_name'];?></td>
<td><center><a href="#" class="kt-nav__link-icon flaticon2-writing" data-toggle="modal" data-target="#approverefund_<?php echo $locs['id']; ?>" ></a></center></td>
</tr>
<?php
$i++;
}
?>

</tbody>
</table>
</div>
<?php
*/
?>

</div>
</div>
</div>

</div>

<?php
?>

<script type="text/javascript">
$(document).ready(function () {
	$('#about_company').jqxEditor({
		height: "400px",
		width: '100%'
	});
	
	$('#job_description').jqxEditor({
	height: "400px",
	width: '100%'
	});
	
    $('#skillset').jqxEditor({
	height: "400px",
	width: '100%'
	});
	
	$('#education').jqxEditor({
	height: "400px",
	width: '100%'
	});


});
</script>