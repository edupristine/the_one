<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupbatch']))
	{
		$msg = "Batch Creation Failed. Batch Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}

	elseif(isset($_GET['batch_name_missing']))
	{
		$msg = "Batch Creation Failed. Please enter Batch name.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Batch Creation Failed. Batch Name and Start Date are Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Batch Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Batch Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Create Batch
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" id="batch" name="batch" method='post' action='form_handlers/create_batch_one.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
	<label>Batch Name</label>
	<input type="text" class="form-control" id="batch_name" name="batch_name">
	
</div>
<div class="col-lg-4">
<?php 
	$query_batchtype = "SELECT id,param_type,param_value from help where param_type='batch_type'";
	$result_batchtype = Select($query_batchtype,$conn);
?>
	<label class="">Batch Type:</label>
			<select class="form-control kt-select2" id="batch_type" name="batch_type">
			<option value="0">Select</option>
				<?php 
					foreach($result_batchtype['rows'] as $batchtype)
					{

							echo '<option value="'.$batchtype['id'].'">'.$batchtype['param_value'].'</option>';
		
					}
				?>
			</select>
</div>
<?php 
	$query_locations = "SELECT id,loc_name from locations  where id not in (1,2)";
	$result_locations = Select($query_locations,$conn);
?>
<div class="col-lg-4">
	<label class="">Batch Center</label>

			<select class="form-control kt-select2" id="batch_center" name="batch_center">
			<option value="0">Select</option>
				<?php 
					foreach($result_locations['rows'] as $location)
					{

							echo '<option value="'.$location['id'].'">'.$location['loc_name'].'</option>';
		
					}
				?>
			</select>

	</div>
</div>

<div class="form-group row">
<div class="col-lg-4">
<?php 
	$query_courses = "SELECT id,course_name from courses";
	$result_courses = Select($query_courses,$conn);
?>
	<label class="">Course Name:</label>
			<select class="form-control kt-select2" id="course_name" name="course_name">
			<option value="0">Select</option>
				<?php 
					foreach($result_courses['rows'] as $course)
					{

							echo '<option value="'.$course['id'].'">'.$course['course_name'].'</option>';
		
					}
				?>
			</select>
	
</div>

<div class="col-lg-4">
<?php 
	$query_cm = "SELECT id,course_module from course_modules where is_active=1";
	$result_cm = Select($query_cm,$conn);
?>
	<label class="">Course Module</label>

			<select class="form-control kt-select2" id="course_module" name="course_module">
			<option value="0">Select</option>
				<?php 
					foreach($result_cm['rows'] as $cm)
					{

							echo '<option value="'.$cm['id'].'">'.$cm['course_module'].'</option>';
		
					}
				?>
			</select>

	</div>

<div class="col-lg-4">

	<label class="">Start Date</label>


    <input type="date" class="form-control" id="start_date"  name="start_date" value = "<?php echo date('Y-m-d'); ?>">
	</div>

</div>

<div class="form-group row">
<div class="col-lg-2">


	<label class="">Time slot(Start Time)</label>
    <input class="form-control" id="kt_timepicker_1" readonly placeholder="Select time" name="time_slot_start" type="text" />
	</div>
	<div class="col-lg-2">

	<label class="">(End Time)</label>
    <input class="form-control" id="kt_timepicker_1" readonly placeholder="Select time" name="time_slot_end" type="text" />
	</div>
	
	
	<div class="col-lg-4">

	<label class="">Mode</label>

			<select class="form-control kt-select2" id="mode" name="mode" >
			<option value="0">Select</option>
		    <option value="Classroom">Classroom</option>
            <option value="LVC_Mat">LVC_Mat</option>
            <option value="LVC">LVC</option>	
            <option value="Seminar">Seminar</option>
            <option value="Webinar">Webinar</option>			
            <option value="Blended" >Blended</option>				
			</select>
    
	</div>

</div>

</div>



<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-3"></div>
		<div class="col-lg-9">
		    <button type="submit" id="one" name="one" class="btn btn-success">Create Batch in One</button>
			<button type="button" id="harry" name="harry" class="btn btn-warning"  onClick="confSubmit(this.form);" hidden>Create Batch in Harry</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>

<script>
function confSubmit(form) {
if (confirm("Are you sure you want to create this Batch across all systems?")) {
form.submit();
}

else {
submit_button_clicked = '';
return false;


}
}


</script>