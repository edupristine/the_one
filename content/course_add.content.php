<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "Course Creation Failed. Course Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Course Creation Failed. Course is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Course Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Course Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add Course
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/course_add.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Course Name</label>
	<input type="text" class="form-control" id="course_name" name="course_name">
	
</div>
<?php 
	$query_users1 = "SELECT id,user_name from users";
	$result_users1 = Select($query_users1,$conn);
?>
<div class="col-lg-6">
	<label class="">Course Owner:</label>
		<select class="form-control kt-select2" id="kt_select2_2" name="course_owner">
		<option value="0">Select Course owner</option>
		<?php
			foreach($result_users1['rows'] as $user)
			{
				echo '<option value="'.$user['id'].'">'.$user['user_name'].'</option>';
			}
		?>
	</select>
	
</div>
</div>
<?php 
	$query_users = "SELECT id,user_name from users";
	$result_users = Select($query_users,$conn);
?>
<div class="form-group row">

<div class="col-lg-4">

	<label for="course_manager">Course Manager</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="course_manager">
		<option value="0">Select Course Manager</option>
		<?php
			foreach($result_users['rows'] as $user)
			{
				echo '<option value="'.$user['id'].'">'.$user['user_name'].'</option>';
			}
		?>
	</select>
	</div>
<div class="col-lg-4">
	<label class="">Course Start Date</label>
	<input type="date" class="form-control" id="start_date"  name="start_date" value = "<?php echo date('Y-m-d'); ?>">	
</div>
<div class="col-lg-4">
	<label class="">Course End Date</label>
	<input type="date" class="form-control" id="end_date"  name="end_date" value = "<?php echo date('Y-m-d'); ?>">	
	
</div>

</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>