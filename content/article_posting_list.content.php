<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$qry1 = "SELECT * FROM articles order by id desc";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}


	
	if(isset($_GET['dupname1']))
	{
		$msg = "Article Update Failed. Location Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Article Update Failed. Location already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Article Update Failed. Location Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Article Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Article Updated Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<i class="kt-font-brand flaticon2-line-chart"></i> Articles List  | <a href="add_website_article.php">Add New Article</a>
</h3>
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Date Range</label>
	<table>
	<tr>
	<td>
	<input type='text'  id='search_fromdate' class="datepicker" style="width:230px;"placeholder='From date' value=<?php echo date('m/01/Y');?> >
	</td>
	<td>
	<input type='text'  id='search_todate' class="datepicker" style="width:230px;" placeholder='To date' value=<?php echo date('m/d/Y');?>>
	</td>
	
	<td>
	<select  id="publish_type" name="publish_type" style="margin:5px;height:42px;" >
	<option value=''>Select Posting Type</option>
    <option value='1'>Published</option>
	<option value='0'>Unpublsihed</option>
	</select>
	</td>

	<td>
	<td>
	<input type='button' id="btn_search" value="Search">
	</td>
	</tr>
	</table>
</div>

</div>
<div class="kt-portlet">
<div style="padding-top:20px;padding-left:20px;padding-right:20px;">
  <table id='empTable' class='display dataTable'>
                <thead>
                <tr>
				<!--<th >#</th>-->
				<th >Url</th>
				<th >Synopsis</th>
				<th >Published Date</th>
				<th >Created At</th>
				<th >Is Published</th>
				<th ><center>Action</center></th>
                </tr>
                </thead>  
		
    </table>
</div>
</div>
</div>
</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

$(document).ready(function(){

   // Datapicker 
   $( ".datepicker" ).datepicker({
      "dateFormat": "yy-mm-dd"
   });

   // DataTable
   var dataTable = $('#empTable').DataTable({
     'processing': true,
     'serverSide': true,
     'serverMethod': 'post',
     'searching': true, // Set false to Remove default Search Control
     'ajax': {
       'url':'ajax/article_ajaxfile.php',
       'data': function(data){
          // Read values
          var from_date = $('#search_fromdate').val();
          var to_date = $('#search_todate').val();
		  var publish_type = $('#publish_type').val();
          // Append to data
          data.searchByFromdate = from_date;
          data.searchByTodate = to_date;
		  if(publish_type!='')
		  {
			data.publish_type = publish_type;  
		  }
		  
       }
     },	 
	'columns': [
	{ data: 'arct_url' },
	{ data: 'synopsis' },
	{ data: 'published_on' },
	{ data: 'created_at' },
	{ data: 'is_published' },
	{ data: 'id', name: 'Action',
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a target=_blank href='/edit_article_posting.php?id="+oData.id+"' ><i class='la la-edit'></i></a>");
        }
    },
	]
  });

  // Search button
  $('#btn_search').click(function(){
     dataTable.draw();
  });

});
</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
	<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery UI CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- jQuery UI JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>