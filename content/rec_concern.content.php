<!-- end:: Subheader -->
<!-- begin:: Content -->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$account_no = get_get_value('account_no');
$course = get_get_value('course');
$city = get_get_value('city');
$name = get_get_value('name');
$email = get_get_value('email');
$phone  = get_get_value('phone');


if(isset($_GET['paramsmissing']))
{
$msg = "Concern regarding Payments Submission Failed. Student Name is Mandatory Field.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "Concern regarding Payments Submission Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "We acknowledge your concern. Relevant Department will get back to you .";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<div class="row" >
<div class="col-lg-12" >
<!--begin::Portlet-->
<div class="kt-portlet" style="background-color:#088FD7;">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<strong style="color:#fff;">EDUPRISTINE</strong>
</h3>
</div>
</div>
</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Raise concern regarding pending payment
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/rec_concern.php'>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-6">
<label>Student ID:</label>
<input type="text" class="form-control" id="account_no" name="account_no" value="<?php echo $account_no; ?>"  >
</div>
<div class="col-lg-6">
<label class="">Name:</label>
<input type="text" class="form-control" id="student_name"  name="student_name" value="<?php echo $name; ?>">
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label>Email:</label>
<input type="text" class="form-control" id="email" name="email" value="<?php echo $email; ?>">
</div>
<div class="col-lg-6">
<label class="">Phone No.:</label>
<input type="text" class="form-control" id="phone"  name="phone" value="<?php echo $phone; ?>">
</div>
</div>



<div class="form-group row">
<div class="col-lg-6">
<label class="">Course:</label>
<input type="text" class="form-control" id="course"  name="course" value="<?php echo $course;  ?>" >
</div>
<div class="col-lg-6">
<label class="">City:</label>
<input type="text" class="form-control" id="city"  name="city" value="<?php echo $city;  ?>" >
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Your Concern:</label>
<textarea type="text" class="form-control" id="concerns"  name="concerns" rows="4"></textarea>
</div>
</div>
</div>

<div class="kt-portlet__foot kt-portlet__foot--fit-x" style="padding-top:0px;border-top:0px Solid #fff;">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button type="submit" id="kt_blockui_3_5" class="btn btn-success"  onclick="return Validate();">Raise Concern</button>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<script>
	function Validate()
	{
		
		var account_no=document.getElementById('account_no').value;
		var student_name=document.getElementById('student_name').value;
		var email=document.getElementById('email').value;
		var phone=document.getElementById('phone').value;
		var concerns=document.getElementById('concerns').value;
		
		if(account_no=='')
		{
			alert("Account no is mandatory!");
			account_no.focus();
		}
		
		
		if(student_name=='')
		{
			alert("Name is mandatory!");
			student_name.focus();
		}
		
		if(email=='')
		{
			alert("Email is mandatory!");
			email.focus();
		}else
		{
			 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) 
            {
                alert('Please enter a valid Email!');
				email.focus();
				submit_button_clicked = '';
                return (false);
            }
		}
		
		if(phone=='')
		{
			alert("Mobile No. is mandatory!");
			phone.focus();
			submit_button_clicked = '';
		}
		
		if(concerns=='')
		{
			alert("Concerns Details are mandatory!");
			submit_button_clicked = '';
			return (false);
		}
		
	}
</script>
