<?php
	$query_allclassroom = "SELECT c.id,c.classroom_name,c.center_id,available_seats FROM `classrooms` c ORDER BY classroom_name";
	$result_allclassroom = Select($query_allclassroom,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Classrooms List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="classroom_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Classrooms
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Classroom Name</th>
												<th>Center</th>
												<th>Available Seats</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allclassroom['rows'] as $class) 
											{	
											$query_cen = "SELECT id,center_name FROM `centers` where id=".$class['center_id']."";
											$result_cen = Select($query_cen,$conn);
											
	
											?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $class['classroom_name']; ?></td>
													<td><?php echo $result_cen['rows'][0]['center_name']; ?></td>
													<td><?php echo $class['available_seats']; ?></td>
													
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="classroom_edit.php?id=<?php echo $class['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>

														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>