
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "Requirement already raised for this student!.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Please enter Student ID";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['insufficient_qty']))
	{
		$msg = "Raise Requirement Failed. Insufficient qty for ".$_GET['insf'];
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
    elseif(isset($_GET['dberror']))
	{
		$msg = "Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Requirement raised successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Raise Requirement of books to Student prior to 1st June 2019 
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/prev_student_rr.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Student ID</label>
	<input type="text" class="form-control" id="stu_id" name="stu_id">
	
</div>

<?php 
	$query_skus = "SELECT id,sku_name from skus order by sku_name";
	$result_skus = Select($query_skus,$conn);
?>
<div class="col-lg-6">
	<label class="">Select Sku's</label>
	<div class="">
			<select class="form-control kt-select2" id="kt_select2_3" name="param[]" multiple="multiple">
				<?php 
					foreach($result_skus['rows'] as $sku)
					{
					
							echo '<option value="'.$sku['id'].'">'.$sku['sku_name'].'</option>';
						
						
						echo '</optgroup>';
					}
				?>
			</select>
		</div>
	</div>
</div>	
<div class="form-group row">	
<?php 
	$query_loc = "SELECT id,loc_name from locations where id not in (2) order by  loc_name ";
	$result_loc = Select($query_loc,$conn);
?>
<div class="col-lg-6">
	<label class="">Select Location</label>
	<div class="">
			<select class="form-control" id="new_location" name="new_location" onchange="returnvalue(this.value);">
				<?php 
					foreach($result_loc['rows'] as $Location)
					{
					
							echo '<option value="'.$Location['id'].'">'.$Location['loc_name'].'</option>';
					}
				?>
			</select>
		</div>
	</div>	
	
<div class="col-lg-6" id="sendcourier_display" style="display:none;">
<table>
<tr>

<td>
Sending Center : <select id="courier_loc" name="courier_loc" >
<?php 
$location_courier = "SELECT id,loc_name from locations  where id not in (2,8)";
$result_loc_courier = Select($location_courier,$conn);



foreach($result_loc_courier['rows'] as $loc_courier)
{

$sent_courier = "SELECT sending_center from student_couriers_request  ";
$result_sent_courier = Select($sent_courier,$conn);
$sendingcenter = $result_sent_courier['rows'][0]['sending_center'];
if($loc_courier['id']== $sendingcenter)
{


?>
<option value="<?php echo $loc_courier['id'];?>" selected><?php echo $loc_courier['loc_name'];?></option>
<?php
}
else
{
?>
<option value="<?php echo $loc_courier['id'];?>"><?php echo $loc_courier['loc_name'];?></option>
<?php
}
}
?>																		

</select>
<td>
</tr>
<tr>
<td>
Student Address:
<textarea id="courier_address" name="courier_address" cols="63" rows="9"  ></textarea>
<td>
</tr>
</table>
</div>	
</div>



<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>
<script>
function returnvalue(val)
{
	if(val==8)
	{
		document.getElementById('sendcourier_display').style.display='block';
	}else
	{
		document.getElementById('sendcourier_display').style.display='none';
	}
}
</script>