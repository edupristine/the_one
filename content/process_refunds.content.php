<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
function send_smt_mail3($subject,$email_msg){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Refunds Notification');
		$mail->addAddress('TeamEduPristineSupport@edupristine.com', 'Team Support');
		$mail->addBCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		$mail->addBCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}


$id=get_get_value('id');
$type=get_get_value('type');

$query_details = "SELECT `account_no`,`amount_to_refund`,`course_enrolled`,`student_name` FROM `student_refund_requests` WHERE `id`=".$id." ";
$result_details = Select($query_details,$conn);
$amtref=$result_details['rows'][0]['amount_to_refund'];
$account_no=$result_details['rows'][0]['account_no'];
$course_enrolled=$result_details['rows'][0]['course_enrolled'];
$student_name=$result_details['rows'][0]['student_name'];

if($type=="approve")
{
$query_dupcheck = "SELECT `id` FROM `refund_details` WHERE `approver_id`='4' and `refund_req_id`=".$id." ";
	$result_dupcheck = Select($query_dupcheck,$conn);
	if($result_dupcheck['count']!=0){
	$query_update = "UPDATE `refund_details` SET approval_status=1,`approval_email_sent`=1 where `approver_id`='4' and `refund_req_id`=".$id."";
	$result_update = Insert($query_update,$conn,'refund_details');
	}
    else 
	{
	$query_insert = "INSERT INTO `refund_details` (`refund_req_id`, `approver_id`, `approval_email_sent`, `approval_status`, `comments`)
	VALUES('".$id."','4','1','1','".$refundreason."')";
	$result_insert = Insert($query_insert,$conn,'refund_details');	
	}
	echo  "<center><h1>Thank you for your Approval for this refund request.</h1></center>";
	
	//For Audit logs
	
	
	$ip_address = get_client_ip();

	$query_insert = "INSERT INTO `refund_logs` (`ip`, `creating_user`, `status`, `reason_of_update`, `amount`,`org_no`,`course`,`log_type`)

	VALUES('".$ip_address."','4','1','".$refundreason."','".$amtref."','".$account_no."','".$course_enrolled."','CEO_APPROVAL')";

	$result_insert = Insert($query_insert,$conn,'refund_logs');
	
		$refundreason		 	= get_post_value('comment_1');
	//Approval  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the refund request raised which is Approved by CEO</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #53AC00; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #53AC00; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$account_no."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$amtref."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Approval:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$refundreason."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Approved By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		CEO</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Approved  by CEO";
		send_smt_mail3($subject,$html);


}
elseif($type=="reject")
{
$query_dupcheck = "SELECT `id` FROM `refund_details` WHERE `approver_id`='4' and `refund_req_id`=".$id." ";
	$result_dupcheck = Select($query_dupcheck,$conn);
	if($result_dupcheck['count']!=0){
	$query_update = "UPDATE `refund_details` SET approval_status=0  , `approval_email_sent` =0 where `approver_id`='4' and `refund_req_id`=".$id."";
	$result_update = Insert($query_update,$conn,'refund_details');
	}
    else 
	{
	$query_insert = "INSERT INTO `refund_details` (`refund_req_id`, `approver_id`, `approval_email_sent`, `approval_status`, `comments`)
	VALUES('".$id."','4','0','0','".$refundreason."')";
	$result_insert = Insert($query_insert,$conn,'refund_details');	
	}
	echo "<center><h1>Thank you for your Feedback for this refund request.</h1></center>";
	
    //For Audit logs
	
	
	$ip_address = get_client_ip();

	$query_insert = "INSERT INTO `refund_logs` (`ip`, `creating_user`, `status`, `reason_of_update`, `amount`,`org_no`,`course`,`log_type`)

	VALUES('".$ip_address."','4','0','".$refundreason."','".$amtref."','".$account_no."','".$course_enrolled."','CEO_APPROVAL')";

	$result_insert = Insert($query_insert,$conn,'refund_logs');
	
	$refundreason		 	= get_post_value('comment_1');
	//Reject  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the refund request raised which is Rejected by CEO</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$account_no."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$amtref."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$refundreason."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		CEO</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Rejected  by CEO";
		send_smt_mail3($subject,$html);
	
	
}


if (isset($_POST['submit']))
{
	
	$refundreason		 	= get_post_value('comment_1');
    $query_dupcheck = "SELECT `id` FROM `refund_details` WHERE `approver_id`='4' and `refund_req_id`=".$id." ";
	$result_dupcheck = Select($query_dupcheck,$conn);
	if($result_dupcheck['count']!=0){
	$query_update = "UPDATE `refund_details` SET  `comments` ='".$refundreason."' where `approver_id`='4' and `refund_req_id`=".$id."";
	$result_update = Insert($query_update,$conn,'refund_details');
	
	
		$refundreason		 	= get_post_value('comment_1');
	//Reject  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the refund request raised which is Rejected by CEO</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$account_no."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$amtref."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$refundreason."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		CEO</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Rejected  by CEO";
		send_smt_mail3($subject,$html);
	}
    else 
	{
	$query_insert = "INSERT INTO `refund_details` (`refund_req_id`, `approver_id`, `approval_email_sent`, `approval_status`, `comments`)
	VALUES('".$id."','4','0','0','".$refundreason."')";
	$result_insert = Insert($query_insert,$conn,'refund_details');	
	
		$refundreason		 	= get_post_value('comment_1');
	//Reject  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the refund request raised which is Rejected by CEO</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$account_no."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$amtref."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$refundreason."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		CEO</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Rejected  by CEO";
		send_smt_mail3($subject,$html);
	}

//For Audit logs
	
	
	$ip_address = get_client_ip();

	$query_insert = "INSERT INTO `refund_logs` (`ip`, `creating_user`, `status`, `reason_of_update`, `amount`,`org_no`,`course`,`log_type`)

	VALUES('".$ip_address."','4','2','".$refundreason."','".$amtref."','".$account_no."','".$course_enrolled."','CEO_APPROVAL')";

	$result_insert = Insert($query_insert,$conn,'refund_logs');	
	
$msg = "Thank you for your Comments for this refund request.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<?php
if($type=="comment")
{
?>
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Process Refunds
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-12">
<label class="">Comments:</label>
<textarea type="text" class="form-control"  name="comment_1" id="comment_1" rows="4"></textarea>
</div>
</div>


</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button type="submit" id="submit" name="submit" class="btn btn-success" >Submit</button>
<button type="reset" class="btn btn-secondary">Cancel</button>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<?php
}
?>
