<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['dupno']))
{
$msg = "Placement Registeration Failed. Student already Exists.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['paramsmissing']))
{
$msg = "Placement Registeration Failed. Student Name and Batch Code are Mandatory Fields.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "Placement Registeration Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "Placement Registeration Successfull.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Register for Placements
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/student_add.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
<label>Batch Code:</label>
<input type="text" class="form-control" id="batch_code" name="batch_code" required>
</div>
<div class="col-lg-4">
<label class="">Student Name:</label>
<input type="text" class="form-control" id="student_name"  name="student_name"required>
</div>
<div class="col-lg-4">
<label>Email:</label>
<input type="text" class="form-control" id="email" name="email"required>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
<label class="">Phone:</label>
<input type="text" class="form-control" id="phone"  name="phone"required>
</div>
<div class="col-lg-8">
<label>Residential Location:</label>
<input type="text" class="form-control" id="address" name="address" required>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
<label class="">Qualification 1:</label>
<input type="text" class="form-control" id="qualification1"  name="qualification1"required>
</div>
<div class="col-lg-4">
<label class="">Qualification 2:</label>
<input type="text" class="form-control" id="qualification2"  name="qualification2" >
</div>
<div class="col-lg-4">
<label class="">Experienced / Fresher:</label>
<div class="kt-radio-inline">
	<label class="kt-radio kt-radio--solid">
		<input type="radio" name="student_type" id="student_type" checked="" value="Exp"> Experienced
		<span></span>
	</label>
	<label class="kt-radio kt-radio--solid">
		<input type="radio" name="student_type" id="student_type" value="Fresher"> Fresher
		<span></span>
	</label>
</div>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
<label class="">Current Company:</label>
<input type="text" class="form-control" id="current_company"  name="current_company">
</div>
<div class="col-lg-4">
<label class="">Current CTC:</label>
<input type="text" class="form-control" id="current_ctc"  name="current_ctc" >
</div>
<div class="col-lg-4">
<label>Interested in Placement : Y/N</label>
<div class="kt-radio-inline">
	<label class="kt-radio kt-radio--solid">
		<input type="radio" name="intrested" id="intrested" checked="" value="Y"> Yes
		<span></span>
	</label>
	<label class="kt-radio kt-radio--solid">
		<input type="radio" name="intrested" id="intrested" value="N"> No
		<span></span>
	</label>
</div>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
<div class="form-group">
<label>Upload CV:</label>

<input type="file" name="uploaded_file" id="uploaded_file"></input><br />


</div>
</div>
</div>
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">

<div class="col-lg-12">
<center><button type="submit" class="btn btn-success" onclick="return Validate();">Submit</button>
<button type="reset" class="btn btn-secondary">Cancel</button></center>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<script>
	function Validate()
	{
		var student_name=document.getElementById('student_name').value;
		var batch_code=document.getElementById('batch_code').value;
		var email=document.getElementById('email').value;
		var phone=document.getElementById('phone').value;
		var address=document.getElementById('address').value;
		var qualification1=document.getElementById('qualification1').value;
		var qualification2=document.getElementById('qualification2').value;
		var intrested=document.getElementById('intrested').value;
		var uploaded_file=document.getElementById('uploaded_file').value;
		var student_type=document.getElementById('student_type').value;
		var current_company=document.getElementById('current_company').value;
		var current_ctc=document.getElementById('current_ctc').value;
		
		if(batch_code=='')
		{
			alert("Batch Code cannot be blank");
			batch_code.focus();
		}
		if(student_name=='')
		{
			alert("Name cannot be blank");
			student_name.focus();
		}
		
		if(email=='')
		{
			alert("Email cannot be blank");
			email.focus();
		}else
		{
			 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) 
            {
                alert('Please enter a valid email id!');
				email.focus();
                return (false);
            }
		}
		
		if(phone=='')
		{
			alert("Phone No. cannot be blank");
			phone.focus();
		}
		
		if(address=='')
		{
			alert("Please enter your residential location!");
			address.focus();
		}
		if(qualification1=='')
		{
			alert("Please enter your qualification1!");
			qualification1.focus();
		}
		
		
		
		
		if((intrested=='Y')&&(uploaded_file==''))
		{
		  alert("Please upload your CV!");	
		  uploaded_file.focus();
		  return false;
		}
		
		
		if((student_type=='Exp')&&(current_company==''))
		{
		  alert("Please Enter your Current Company!");	
		  current_company.focus();
		}
		
		if((student_type=='Exp')&&(current_ctc==''))
		{
		  alert("Please Enter your Current CTC!");	
		  current_ctc.focus();
		}
		
		
		
	}
</script>
