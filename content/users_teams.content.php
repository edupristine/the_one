<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'users_teams.php?filter_param='+filter_param;
	}
</script>
<?php
	$location_id=$_SESSION['U_LOCATION_ID'];
    $team_id = get_get_value('team_id');
	$filter_param="";	
	
	
	if($team_id!="")
	{	
	$query_allusers = "SELECT u.id,u.user_name,DATE_FORMAT(ut.start_date, '%d/%m/%Y') as start_date,DATE_FORMAT(ut.end_date, '%d/%m/%Y') as end_date,t.team_name,ut.id as utid,t.id as team_id,ut.del_flag FROM users u,users_teams ut,teams t where ut.user_id=u.id and ut.team_id = ".$team_id." and t.id=ut.team_id and u.is_active=1 ORDER BY ut.created_at desc";
	}
	elseif($team_id=="")
	{
	$query_allusers = "SELECT u.id,u.user_name,DATE_FORMAT(ut.start_date, '%d/%m/%Y') as start_date,DATE_FORMAT(ut.end_date, '%d/%m/%Y') as end_date,t.team_name,ut.id as utid,t.id as team_id,ut.del_flag FROM users u,users_teams ut,teams t where ut.user_id=u.id and t.id=ut.team_id and u.is_active=1 ORDER BY ut.created_at desc";	
	}
	
	$result_allusers = Select($query_allusers,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	
	if($team_id!="")
	{
	$query_team_name = "SELECT team_name from teams where id = ".$team_id ."";
	$result_team_name = Select($query_team_name,$conn);
	$team_name_report = $result_team_name['rows'][0]['team_name'];
	}else
	{
	$team_name_report="";	
	}

	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Users for <?php echo $team_name_report;?> Team
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
											
											<div class="col">
														<table><tr>
														<td>
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Select Team
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																	<?php
																	$query_teams = "SELECT id,team_name FROM teams";
																	$result_allteams = Select($query_teams,$conn);
																	foreach($result_allteams['rows'] as $team)
																	{
																	?>
																   <a class="dropdown-item" href="users_teams.php?team_id=<?php echo $team['id'];?>"><?php echo $team['team_name'];?></a>
																	<?php
																	}
																	?>
																	</div>
																
															</div>
															</td></tr></table>
														</div>
																						
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Team Name</th>
												<th>User Name</th>
												<th>Start Date</th>
												<th>End Date</th>
											    <th><center>Action</center></th>
												
												
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allusers['rows'] as $user) 
											{	
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $user['team_name']; ?></td>
													<td ><?php echo $user['user_name']; ?></td>
													<td ><?php echo $user['start_date']; ?></td>
													<td ><?php echo $user['end_date']; ?></td>
													<td><center>
													<?php
													if($user['del_flag']==0)
													{
													?>
													<button onClick="Removemember(<?php echo $user['utid']; ?>);" type="button" class="btn-sm btn-success btn-elevate btn-pill">Remove</button><center></td>
													<?php
													}
													else
													{
													?>
													<button  type="button" class="btn-sm btn-error btn-elevate btn-pill" disabled >Removed</button><center></td>
													<?php
													}?>
								
												</tr>
										<?php	
											$i++;
											}
										?>
										<tr>
										<td colspan=2>
										</td>
										
										<td colspan=4>
										<?php if($team_id!="")
										{?>
										<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#addmember_<?php echo $team_id; ?>">Add Member</button>
										</td>
										<?php
										}
										?>
                                        </tr>										
										</tbody>
									</table>
										<?php 
											
												if($team_id!="")
												{
												$team_det = "SELECT id,team_name from teams where id=".$team_id;
												$result_team_det = Select($team_det,$conn);
												}
												else
												{
                                                $result_team_det['rows'][0]['team_name']="";												
												}												
												
												
												
												$user_int = "SELECT id,user_name from users where is_active=1";
												$result_user_int = Select($user_int,$conn);
											?>
											<div class="modal fade" id="addmember_<?php echo $team_id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Add member to: ".$result_team_det['rows'][0]['team_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="200"  style="height: 200px; overflow: hidden;">
																	
																	
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $team_id; ?>"  >
                                                                        
																		<tbody>
																		 <div class="form-group row">

																		<div class="col-lg-12">
																		<label class="">Select 	Users</label>
																		    <div class="">
																				<select class="form-control kt-select2" id="kt_select2_3_<?php echo $team_id; ?>" name="param[]" multiple="multiple">
																					<?php 
																						foreach($result_user_int['rows'] as $usersint)
																						{
																							
																								echo '<option value="'.$usersint['id'].'">'.$usersint['user_name'].'</option>';
																							
																							
																							
																						}
																					?>
																				</select>
																			</div>
																			


																		</div>
																		</div>
																			
																	    </tbody>
																	<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																	</table>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Addnewmember(<?php echo $team_id; ?>);" type="button" class="btn btn-primary">Add Member/s</button>
															</div>
														</div>
													</div>
												</div>
												
											
										
										
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function Addnewmember(team_id)
{
   
	var teamusers = document.getElementById('kt_select2_3_'+team_id);
	
	var str=[];
	for (i=0;i<teamusers.length;i++) { 
	if(teamusers[i].selected){
	str +=teamusers[i].value + ","; 
	}
	}
	var nameArr = str.split(',');
	
	
    if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("New Team Member/s added Successfully.");
				submit_button_clicked = '';
				$('#addmember_'+team_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Member addition Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Member addition Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/add_team_users.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('team_id='+fixEscape(team_id)+'&teamusers='+fixEscape(nameArr));
	

}


function Removemember(member_id)
{
	
	if (confirm('Are you sure you want to remove this Member?')) {
		if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Member Removed Successfully.");
				submit_button_clicked = '';
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Member Removal Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Member Removal Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/remove_member.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('member_id='+fixEscape(member_id));	
	return true;
	} else {
	submit_button_clicked = '';
	return false;
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
}


</script>