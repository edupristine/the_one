<?php
setlocale(LC_MONETARY, 'en_IN');
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date('Y-m-d');
    $edate = date('Y-m-d');
    $date_range = $sdate." / ".$edate;
}
$query2 = "SELECT * FROM workshops where start_date >= '".$sdate."' AND start_date <='".$edate."' AND
location != 'Other' AND course IN ('CFA','DM','FRM','FM','PBA','BD') ORDER BY code ASC";
$result2 = mysqli_query($conn2,$query2);
while($r2 = mysqli_fetch_assoc($result2)) {
    $batch_code[] = $r2;
}
?>
<script>
	function get_batches()
	{
		var date_range = document.getElementById('date_range').value;
		window.location = 'batch_threshold.php?date_range='+date_range;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_batches();" type="button" class="btn btn-primary form-control">Get Threshold</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Batch Threshold
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
                                                <th rowspan=2></th>
												<th rowspan=2>Batchcode</th>
                                                <th rowspan=2>Batch Start Date</th>
												<th rowspan=2>Days to Batch</th>
												<th rowspan=2>Course</th>
												<th rowspan=2>City</th>
                                                <th colspan=4 style="text-align: center;vertical-align: middle;">Count</th>
                                                <!-- <th colspan=4 style="text-align: center;vertical-align: middle;">Revenue</th>
                                                <th colspan=2 style="text-align: center;vertical-align: middle;">Gross Margin</th> -->
												
											</tr>
                                            <tr>
                                            <th>Threshold</th>
                                            <th>Enrolled</th>
                                            <th>%</th>
                                            <th>Difference</th>
                                            <!-- <th>Threshold</th>
                                            <th>Enrolled</th>
                                            <th>%</th>
                                            <th>Difference</th>
                                            <th>Exp.</th>
                                            <th>Actual</th> -->
                                            </tr>
										</thead>
										<tbody>
										<?php
											foreach($batch_code as $batch){
                                                $batch_location = $batch['location'];
                                                $batch_course = $batch['course_version'];
                                                $batch_category = $batch['course'];
                                            $query_master = "SELECT * FROM batch_threshold WHERE location='".$batch_location."' AND course='".$batch_course."'
                                                            AND category='".$batch_category."'";
                                                            $result_master = mysqli_query($connone,$query_master);
                                            $query = "SELECT count(ac.accountid) as `students_count`,sc.cf_1495 as `batch_code`,
                                                        sc.cf_1601 as `start_date`,sc.cf_918 as `course`,
                                                        sc.cf_1513 as `sub_course`, ab.bill_city, SUM(sc.cf_936) as `revenue`
                                                        FROM vtiger_account ac
                                                        LEFT JOIN vtiger_accountscf sc ON sc.accountid = ac.accountid
                                                        LEFT JOIN vtiger_crmentity ct ON ct.crmid = ac.accountid
                                                        LEFT JOIN vtiger_accountbillads ab ON ab.`accountaddressid` = ac.accountid
                                                        where ct.smownerid != 3920 AND sc.cf_1495='".$batch['code']."' AND ct.deleted != 1";
                                                        
                                                    $result = mysqli_query($conn1,$query);
                                                    $rows = mysqli_fetch_assoc($result_master);
                                                    $rows2 = mysqli_fetch_assoc($result);
                                                    $threshold_revenue = $rows['threshold_revenue'];
                                                    $threshold_brc = $rows['total_batch_cost'];
                                                    $exp_gm = round((($threshold_revenue - $threshold_brc)/$threshold_revenue)*100,0);
                                                    //$actual_revenue = $rows2['revenue'];
                                                    $actual_revenue = $rows['avg_basic_fee']*$rows2['students_count'];
                                                    if($actual_revenue == 0)
                                                    $actual_revenue = 1;
                                                    $student_variable = $rows2['students_count'] * $rows['total_variable_per_student'];
                                                    $class_variable = ($rows['course_classes']+$rows['non_course_classes']) * $rows['total_variable_per_class'];
                                                    $fixed_cost = $rows['total_fixed_cost'] + ($rows['faculty_fees_per_class']*$rows['online_classes']);
                                                    $actual_brc = $student_variable + $class_variable + $fixed_cost;
                                                    if($actual_revenue != 1)
                                                    $actual_gm = round((($actual_revenue - $actual_brc)/$actual_revenue)*100,0);
                                                    else
                                                    $actual_gm = "NA";
                                                    
                                                    $actual_revenue1 = $rows['avg_basic_fee']*($rows2['students_count']+1);
                                                    $student_variable1 = ($rows2['students_count']+1) * $rows['total_variable_per_student'];
                                                    $class_variable1 = ($rows['course_classes']+$rows['non_course_classes']) * $rows['total_variable_per_class'];
                                                    $fixed_cost1 = $rows['total_fixed_cost']+ ($rows['faculty_fees_per_class']*$rows['online_classes']);
                                                    $actual_brc1 = $student_variable1 + $class_variable1 + $fixed_cost1;
                                                    $actual_gm1 = round((($actual_revenue1 - $actual_brc1)/$actual_revenue1)*100,0);
                                            
                                                    $actual_revenue2 = $rows['avg_basic_fee']*($rows2['students_count']+2);
                                                    $student_variable2 = ($rows2['students_count']+2) * $rows['total_variable_per_student'];
                                                    $class_variable2 = ($rows['course_classes']+$rows['non_course_classes']) * $rows['total_variable_per_class'];
                                                    $fixed_cost2 = $rows['total_fixed_cost'] + ($rows['faculty_fees_per_class']*$rows['online_classes']);
                                                    $actual_brc2 = $student_variable2 + $class_variable2 + $fixed_cost2;
                                                    $actual_gm2 = round((($actual_revenue2 - $actual_brc2)/$actual_revenue2)*100,0);
                                            
                                                    $actual_revenue3 = $rows['avg_basic_fee']*($rows2['students_count']+3);
                                                    $student_variable3 = ($rows2['students_count']+3) * $rows['total_variable_per_student'];
                                                    $class_variable3 = ($rows['course_classes']+$rows['non_course_classes']) * $rows['total_variable_per_class'];
                                                    $fixed_cost3 = $rows['total_fixed_cost'] + ($rows['faculty_fees_per_class']*$rows['online_classes']);
                                                    $actual_brc3 = $student_variable3 + $class_variable3 + $fixed_cost3;
                                                    $actual_gm3 = round((($actual_revenue3 - $actual_brc3)/$actual_revenue3)*100,0);
                                            
                                                    $actual_revenue4 = $rows['avg_basic_fee']*($rows2['students_count']+4);
                                                    $student_variable4 = ($rows2['students_count']+4) * $rows['total_variable_per_student'];
                                                    $class_variable4 = ($rows['course_classes']+$rows['non_course_classes']) * $rows['total_variable_per_class'];
                                                    $fixed_cost4 = $rows['total_fixed_cost'] + ($rows['faculty_fees_per_class']*$rows['online_classes']);
                                                    $actual_brc4 = $student_variable4 + $class_variable4 + $fixed_cost4;
                                                    $actual_gm4 = round((($actual_revenue4 - $actual_brc4)/$actual_revenue4)*100,0);
                                                    $today = new DateTime();
                                                    $batchDate = date('Y-m-d',strtotime($batch['start_date']));
                                                    $bDate = new DateTime($batchDate);
                                                    $days_remain = date_diff($bDate,$today);
                                                    $revenue_pre_tax = round($rows2['revenue']/1.18,0);
                                                    $surplus_count = $rows['threshold_no'] - $rows2['students_count'];
                                                    $surplus_revenue = $rows['threshold_revenue'] - $revenue_pre_tax;
                                            
                                           
												?>
                                                
												<tr>
                                                    <td><a href="#" onclick="expandRow('<?php echo $batch['code']; ?>');"><i class="fa fa-caret-right" id="<?php echo $batch['code']; ?>"></i></a></td>
													<td><?php echo $batch['code']; ?></td>
                                                    <td><?php echo $batch['start_date']; ?></td>
													<td style="text-align: center;vertical-align: middle;"><?php echo $days_remain->d; ?></td>
													<td><?php echo $batch_course; ?></td>
                                                    <td><?php echo $batch_location; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $rows['threshold_no']; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $rows2['students_count']; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo round(($rows2['students_count']/$rows['threshold_no'])*100,0)."%"; ?></td>
                                                    <td class="kt-font-bold kt-font-<?php if($surplus_count < 0){echo "success";}else{echo "danger";} ?>" style="text-align: center;vertical-align: middle;"><?php echo $surplus_count; ?></td>
												</tr>
                                                <tr id="<?php echo $batch['code']."_1"; ?>" style="display:none;">
                                                <th colspan=6 style="text-align: right;vertical-align: middle;font-weight:500;">Revenue Details</th>
                                                <td style="text-align: center;vertical-align: middle;"><?php echo money_format('%!.0i',$rows['threshold_revenue']); ?></td>
                                                <td style="text-align: center;vertical-align: middle;"><?php echo money_format('%!.0i',$revenue_pre_tax); ?></td>
                                                <td style="text-align: center;vertical-align: middle;"><?php echo round(($revenue_pre_tax/$rows['threshold_revenue'])*100,0)."%"; ?></td>
                                                <td class="kt-font-bold kt-font-<?php if($surplus_revenue < 0){echo "success";}else{echo "danger";} ?>" style="text-align: center;vertical-align: middle;"><?php echo money_format('%!.0i',$surplus_revenue); ?></td>
                                                    
                                                </tr>
                                                <tr id="<?php echo $batch['code']."_2"; ?>" style="display:none;">
                                                <th colspan=2 style="text-align: right;vertical-align: middle;font-weight:500;">Gross Margin</th>
                                                <td class="kt-font-bold kt-font-primary" style="text-align: center;vertical-align: middle;">(Exp.) <?php echo $exp_gm."%"; ?></td>    
                                                <td class="kt-font-bold kt-font-<?php if($actual_gm < 35 || $actual_gm == "NA"){echo "danger";}elseif($actual_gm < 50){echo "warning";}else{echo "success";} ?>" style="text-align: center;vertical-align: middle;">(Cur.) <?php echo $actual_gm."%"; ?></td>
                                                <td>(+1)  <?php echo $actual_gm1."%"; ?></td>
                                                <td>(+2)  <?php echo $actual_gm2."%"; ?></td>
                                                <td>(+3)  <?php echo $actual_gm3."%"; ?></td>
                                                <td>(+4)  <?php echo $actual_gm4."%"; ?></td>
                                                <td id="<?php echo $batch['code']."_gmResult"; ?>" style="text-align: center;vertical-align: middle;"></td>
                                                <td style="text-align: center;vertical-align: middle;"><input type="text" style="width:50%" id="<?php echo $batch['code']."_count";?>"></input><button class="kt-nav__link-icon flaticon2-writing" style="margin-left:5%;" onclick="getGM('<?php echo $batch['code']; ?>',<?php echo $rows['avg_basic_fee'].','.$rows2['students_count'].','.$rows['total_variable_per_student'].','.$class_variable.','.$fixed_cost; ?>);"></button></td>
                                                </tr>
                                                <!-- <tr>
                                                <td colspan=4 style="text-align: right;vertical-align: middle;">Extra Enrollment(s)</td>
                                                <td colspan=2>(+1)  <?php echo $actual_gm1."%"; ?></td>
                                                <td colspan=2>(+2)  <?php echo $actual_gm2."%"; ?></td>
                                                <td colspan=2>(+3)  <?php echo $actual_gm3."%"; ?></td>
                                                <td colspan=2>(+4)  <?php echo $actual_gm4."%"; ?></td>
                                                </tr> -->
										<?php	
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>

                            <script>
                            function expandRow(batch){
                                if(document.getElementById(batch).classList.contains('fa-caret-right')){
                                document.getElementById(batch+"_1").style.display="table-row";
                                document.getElementById(batch+"_2").style.display="table-row";
                                document.getElementById(batch).classList.remove('fa-caret-right');
                                document.getElementById(batch).classList.add('fa-caret-down');
                                }else{
                                document.getElementById(batch+"_1").style.display="none";
                                document.getElementById(batch+"_2").style.display="none";
                                document.getElementById(batch).classList.remove('fa-caret-down');
                                document.getElementById(batch).classList.add('fa-caret-right');
                                }
                                
                            }
                            function getGM(batch,avgBasicFee,studentCount,varPerStudent,classVariable,fixedCost){
                                var n = parseInt(document.getElementById(batch+"_count").value);
                                var newStudentCount = studentCount + n;
                                    var actual_revenue = avgBasicFee*(newStudentCount);
                                    var student_variable = (newStudentCount)*varPerStudent;
                                    var actual_brc = student_variable+classVariable+fixedCost;
                                    var actual_gm = Math.round(((actual_revenue-actual_brc)/actual_revenue)*100);
                                    var s = "(+";
                                    document.getElementById(batch+"_gmResult").innerHTML=s+n+')'+actual_gm+'%';
                                    document.getElementById(batch+"_count").innerHTML = "";
                            }
                            </script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
</div>