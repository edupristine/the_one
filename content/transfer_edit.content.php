<?php
if (!isset($_GET['id']) || empty($_GET['id']))
{
	header('location:transfer_list.php');
	exit();
}

$trans_id = $_GET['id'];
$query_skus_selected = "SELECT * FROM `transfers_skus` WHERE `trans_id`='". $trans_id . "'";
$result_skus_selected = Select($query_skus_selected, $conn);
$num_of_skus = $result_skus_selected['count'];

$query_transfer = "SELECT * FROM transfers WHERE `id`='" . $trans_id . "'";
$result_transfer = Select($query_transfer, $conn);
if ($result_transfer['count'] == '0')
{
	header('location:transfer_list.php');
	exit();
}
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Edit Transfers
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">
<div class="col-lg-2" hidden>
	<label>Transfer ID</label>
	<input type="text" class="form-control" id="trans_id" name="trans_id" value="<?php echo $result_transfer['rows'][0]['id']; ?>" readonly>
</div>

<div class="form-group row">
<div class="col-lg-4">
	<label>Transfer Number</label>
	<input type="text" class="form-control" id="transfer_no" name="transfer_no" value="<?php echo $result_transfer['rows'][0]['transfer_no']; ?>" readonly>
</div>
<?php
$query_loc = "SELECT id,loc_name from locations where loc_type='CENTER'";
$result_loc = Select($query_loc,$conn);

$query_skus = "select id,sku_name,sku_code from skus";
$result_skus = Select($query_skus,$conn);

?>
<div class="col-lg-4">
	<label>Transfer To</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="transfer_to">
		<option value="0">Select Center Location</option>
		<?php
			foreach($result_loc['rows'] as $loc)
			{
				if($result_transfer['rows'][0]['to_loc_id'] == $loc['id'])
				{
					echo '<option value="'.$loc['id'].'" selected>'.$loc['loc_name'].'</option>';
				}
				else
				{
					echo '<option value="'.$loc['id'].'">'.$loc['loc_name'].'</option>';
				}
				
				echo '<option value="'.$loc['id'].'">'.$loc['loc_name'].'</option>';
			}
		?>
	</select>
</div>

<div class="col-lg-4">
	<label class="">Transfer Date</label>
	<input type="date" class="form-control" id="transfer_date"  name="transfer_date" value="<?php echo $result_transfer['rows'][0]['transfer_date']; ?>">	
</div>
</div>

<div class="form-group row">
<div class="col-lg-4">
	<label class="">Transfer Courier</label>
    <input type="text" class="form-control" id="transfer_courier" name="transfer_courier" value="<?php echo $result_transfer['rows'][0]['transfer_courier']; ?>" >
</div>
<div class="col-lg-4">
	<label>Courier Track No.</label>
	<input type="text" class="form-control" id="courier_track_no" name="courier_track_no" value="<?php echo $result_transfer['rows'][0]['courier_track_no']; ?>" >
</div>
<div class="col-lg-4">
	<label>Courier Charges</label>
	<input type="text" class="form-control" id="transfer_charges" name="transfer_charges" value="<?php echo $result_transfer['rows'][0]['transfer_charges']; ?>" >
</div>

<input type="hidden" class="form-control" id="transfer_status" name="transfer_status" value="INITIATED" readonly>
</div>

</div>
	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:10%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:15%;">Quantity</th>
				<th style="width:10%;">Action</th>
			</tr>
		</thead>
		
		<tbody hidden id="samplerow_div">
			<tr id="row_id_0">	
				<td class="text-center"><input readonly name="sr_0" id="sr_0" style="width:100%;padding:2px;" type="text" value="1"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_0');" id="skuname_0" name="skuname_0" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="qty_0" id="qty_0" placeholder="Qty" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td id="prodel_0" name="prodel_0" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_0');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
		
		
		<tbody id="rows_div">
		<?php 
			$query_skus = "Select s.sku_name,ts.sku_id,ts.trans_qty from transfers_skus ts, skus s where trans_id = ".$trans_id." AND s.id = ts.sku_id";
			$result_skus_selected = Select($query_skus,$conn);
			$i=0;
			foreach($result_skus_selected['rows'] as $sku_selected)
			{
				$i++;?>
			<tr id="row_id_<?php echo $i; ?>">
				<td class="text-center"><input readonly name="sr_<?php echo $i; ?>" id="sr_<?php echo $i; ?>" style="width:100%;padding:2px;" type="text" value="<?php echo $i; ?>"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_<?php echo $i; ?>');" id="skuname_<?php echo $i; ?>" name="skuname_<?php echo $i; ?>" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							if($sku_selected['sku_id'] == $sku['id'])
								echo "<option value='".$sku['id']."' selected>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
							else
								echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="qty_<?php echo $i; ?>" id="qty_<?php echo $i; ?>" placeholder="Qty" style="width:100%;padding:2px;" type="text" value="<?php echo $sku_selected['trans_qty']; ?>"/></td>
				<td id="prodel_<?php echo $i; ?>" name="prodel_<?php echo $i; ?>" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_<?php echo $i; ?>');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<a onClick="SaveOrder();" class="btn btn-success">Save</a>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

<?php
?>