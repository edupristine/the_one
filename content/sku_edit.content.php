<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['id']) && !empty($_GET['id']))
{
	

	if(isset($_GET['dupname1']))
	{
		$msg = "SKU Creation Failed. SKU Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "SKU Creation Failed. SKU Code already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['sub_course_missing']))
	{
		$msg = "SKU Creation Failed. Please select a Sub Course.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "SKU Creation Failed. SKU Name and SKU Code are Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "SKU Creation Failed. SKU Name and SKU Code are Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "SKU Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<?php
	$id = get_get_value('id');
	$query_sku = "SELECT * from skus where id = ".$id; 
	$result_sku = Select($query_sku,$conn);
	$sku_name = $result_sku['rows'][0]['sku_name'];
	$sku_code = $result_sku['rows'][0]['sku_code'];
	$sku_type = $result_sku['rows'][0]['sku_type'];
	
	$sku_desc = $result_sku['rows'][0]['sku_desc'];
	$unit_price = $result_sku['rows'][0]['unit_price'];
	$pref_qty = $result_sku['rows'][0]['pref_qty'];
	$o_to_r_days = $result_sku['rows'][0]['o_to_r_days'];
	$reorder_level = $result_sku['rows'][0]['reorder_level'];
	$min_order_qty = $result_sku['rows'][0]['min_order_qty'];
?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update SKUs
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/sku_edit.php'>
<div class="kt-portlet__body">
<div class="col-lg-6" hidden>
	<label>ID</label>
	<input type="text" class="form-control" id="id" name="id" value = "<?php echo $id; ?>">
	
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>SKU Name</label>
	<input type="text" class="form-control" id="sku_name" name="sku_name" value = "<?php echo $sku_name; ?>">
	
</div>
<div class="col-lg-6">
	<label class="">SKU Code:</label>
	<input type="text" class="form-control" id="sku_code"  name="sku_code" value = "<?php echo $sku_code; ?>">
	
</div>
</div>
<?php 
	$query_courses = "SELECT id,course_name from courses";
	$result_courses = Select($query_courses,$conn);
	
	$sub_course_arr = array();
	$query_sub_courses_sku = "SELECT subcourse_id from subcourses_skus where sku_id = ".$id;
	$result_sub_courses_sku = Select($query_sub_courses_sku,$conn);
	$i = 0;
	foreach($result_sub_courses_sku['rows'] as $subcourse)
	{
		$sub_course_arr[$i] =  $subcourse['subcourse_id'];
		$i++;
	}
	
?>
<div class="form-group row">

<div class="col-lg-6">
	<label class="">Sub Course</label>
	<div class="">
			<select class="form-control kt-select2" id="kt_select2_3" name="param[]" multiple="multiple">
				<?php 
					foreach($result_courses['rows'] as $course)
					{
						echo '<optgroup label="'.$course['course_name'].'">';
						
						$query_subcourses = "SELECT id,subcourse_name from sub_courses where course_id = ".$course['id'];
						$result_subcourses = Select($query_subcourses,$conn);
						
						foreach($result_subcourses['rows'] as $subcourse)
						{
							if (in_array($subcourse['id'], $sub_course_arr))
							{
								echo '<option value="'.$subcourse['id'].'" selected>'.$subcourse['subcourse_name'].'</option>';
							}
							else
							{
								echo '<option value="'.$subcourse['id'].'">'.$subcourse['subcourse_name'].'</option>';
							}
							
						}
						
						echo '</optgroup>';
					}
				?>
			</select>
		</div>
	<!--<label for="sub_course">Sub Course</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="sub_course">
		<option value="0">Select Sub Course</option>
		<?php
			foreach($result_subcourses['rows'] as $subcourse)
			{
				echo '<option value="'.$subcourse['id'].'">'.$subcourse['subcourse_name'].'</option>';
			}
		?>
	</select>-->
	</div>
	<div class="col-lg-6">
	<label for="sku_type">SKU Type</label>
	<select class="form-control" id="sku_type" name="sku_type">
		<option value="Book" <?php if($sku_type == "Book") echo "selected"; ?>>Book</option>
		<option value="Brochure" <?php if($sku_type == "Brochure") echo "selected"; ?>>Brochure</option>
	</select>
	
</div>

</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>SKU Description:</label>
	<input type="text" class="form-control" id="sku_desc" name="sku_desc" value = "<?php echo $sku_desc; ?>">
	
</div>
<div class="col-lg-6">
	<label class="">SKU Unit Price:</label>
	<input type="text" class="form-control" id="unit_price"  name="unit_price" value = "<?php echo $unit_price; ?>">
	
</div>

</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Re-Order Level:</label>
	 <input type="text" class="form-control" id="reorder_level"  name="reorder_level" value = "<?php echo $reorder_level; ?>">
	
</div>
<div class="col-lg-6">
	<label class="">Order to Receiving Lag (In Days):</label>
	<input type="text" class="form-control" id="o_to_r_days" name="o_to_r_days" value = "<?php echo $o_to_r_days; ?>">
	
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>SKU Prefered Ordering Quantity:</label>
	 <input type="text" class="form-control" id="pref_qty"  name="pref_qty" value = "<?php echo $pref_qty; ?>">
	
</div>
<div class="col-lg-6">
	<label class="">Min. Order Quantity:</label>
	<input type="text" class="form-control" id="min_order_qty"  name="min_order_qty" value = "<?php echo $min_order_qty; ?>">
	
</div>
</div>


</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
}
else
{
	header('location:sku_list.php');
	exit();
}
?>