<?php

$post_mail_display = 0;
$mailsent = 0;
$workshop_id = "";
$ws_date = "";
if(isset($_GET['workshop_id']))
{
	$post_mail_display = 1;
	$workshop_id = get_get_value('workshop_id');
	
}
$wdateqry="";
$workshop_date = get_get_value('ws_date');
if($workshop_date!='')
{
$wdateArray = explode(',', $workshop_date);

if(count($wdateArray)==1)
{
$wdateqry="AND wd.ws_date='".$wdateArray[0]."'";	
}
else
{
$querystring ="";	
foreach($wdateArray as $wdate)
{
$querystring .="'".$wdate."',";	
}

$wdateqry="AND wd.ws_date in (".rtrim($querystring,',').")";	
}

		
}	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
    $msg="";
    if(isset($_GET['success']))
	{
		$msg = "Emails Sent to Students Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else if(isset($GET['failed']))
	{
		$msg = "Email Sending Failed";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Send Schedule Mails to students
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/send_schedule_mail.php'>
<div class="kt-portlet__body">

<?php 
	$query_harry_codes = "SELECT id,code FROM workshops WHERE STATUS IN ('Conducted','Upcoming','Confirmed') AND code not like '%SEM-%' ORDER BY id desc ";
	$result_harry_code = Select($query_harry_codes,$harry);
?>
<div class="form-group row">
 <div class="col-lg-4">
	<label class="">Select</label>
	<div class="">
			<select class="form-control kt-select2" id="kt_select2_3" name="kt_select2_3" onchange="fillwdates(this.value);">
				<option value="0"> Batch Code</option>
				<?php 
					foreach($result_harry_code['rows'] as $code)
					{
						if($code['id']==$workshop_id)
						{
						echo '<option value="'.$code['id'].'" selected>'.$code['code'].'</option>';
						}
						else
						{
						echo '<option value="'.$code['id'].'" >'.$code['code'].'</option>';	
						}
						
					}
				?>
			</select>
		</div>
 </div>
 
  <div class="col-lg-4">
	<label class="">Select</label>
	<?php
	if($post_mail_display)
	{
		$select_wdates = "SELECT ws_date FROM workshops_dates WHERE STATUS IN ('Conducted','Upcoming','Confirmed') AND workshop_id=".$workshop_id."  AND ws_date >=now() order by ws_date";
	    $result_wdates = Select($select_wdates,$harry,"workshops_dates");
	}
   	
	?>

		<div class=""id="ws_date">
	     <select class="form-control kt-selectpicker"  id="w_date"   name="param[]" multiple="multiple" onblur="getMultipleSelectedValue();" >
				<option value="0"> Workshop Date</option>
				<?php 
					if($result_wdates['count'] > 0)
					{
						foreach($result_wdates['rows'] as $dates)
						{
							
							    $wdateArray = explode(',', $workshop_date);
								
								if (in_array($dates['ws_date'],$wdateArray))
								{
								 echo "<option value='".$dates['ws_date']."' selected>".$dates['ws_date']."</option>";
								}
								else
								{
                                echo "<option value='".$dates['ws_date']."'>".$dates['ws_date']."</option>";
								}
								
							
							
							
						}
					}
				
				?>
			</select>
			
    </div>
 </div>
 <div class="col-lg-4">
 <label class=""></label>
 <div class="" style="padding-top:4px;">
<button type="button" name="generatepreview" id="generatepreview" class="btn btn-success" onclick="getMultipleSelectedValue();">Generate Preview</button>
</div>
 </div>
</div>
<?php
$subject = '';
$body = '';	

if(isset($_POST['wid']))
{
	$wid=$_POST['wid'];
	$ws_date=$_POST['wsdate'];
}
else
{
	$wid = $workshop_id;
	$ws_date=$workshop_date;
}

	$query_mailpreview = "SELECT
					wd.id, 
					wd.batch_code as `batch_code`, 
					wd.venue_details as `venue_details`,
					wd.location as `location`, 
					wd.start_time `start_time`, 
					wd.end_time as `end_time`,
					wd.actual_start_time as `actual_start_time`,
					wd.actual_end_time as `actual_end_time`,
					wd.classroom_rent as `classroom_rent`,
					wd.slot as slot,
					wd.student_count_by_admin as student_count_by_admin,
					wd.topic as `topic`, 
					wd.fac_average_rating as `faculty_rating`,
					wd.content_quality_rating,
					wd.infra_rating, 
					wd.cs_rating,
					wd.newFaculty,
					wd.ws_comment as `ws_comment`, 
					wd.`status` as `status`,
					wd.mail_sent_flag as `mail_sent_flag`,
					wd.sms_sent_flag as `sms_sent_flag`,
					wd.course as `course`,
					DATE_FORMAT(wd.ws_date, '%e-%b-%Y') as `ws_date`,
					wd.ws_date as `og_ws_date`,
					wd.content_link,
					wd.venue_coordinator_name,
					wd.venue_coordinator_email,
					wd.venue_coordinator_contact,
					wd.support_team_email,
					wd.sales_manager_email,
					wd.extra_instructions_faculty,
					wd.cancel_point,
					wd.changes_point, 
					wd.repeat_session,
					wd.re_entry_point,
					wd.tentitve_remarks,
					wd.feedback_status as `feedback_status`,
					wd.lvc_url as `lvc_url`,
					w.id as `workshop_id`,
					DAYNAME(wd.ws_date) as `day`,
					DATE_FORMAT(w.start_date, '%e-%b-%Y') as `start_date`,
					wd.faculty_incharge as `faculty_incharge`,
					wd.faculty_incharge_email as `faculty_incharge_email`, 
					fc.fid,
					fc.name as  `name`, 
					fc.email as `email`,
					fc.phone as `phone`,
					(select count(*) from workshops_feedback wf
					where wf.batch_code = wd.batch_code and wf.ws_feedback_date = wd.ws_date group by wf.batch_code, wf.ws_feedback_date) as `no_of_student`
					FROM workshops_dates wd
					LEFT JOIN workshops w ON w.id = wd.workshop_id
					LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
        			WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'  and wd.`status`!= 'Error'
        			AND wd.workshop_id='".$workshop_id."' $wdateqry ";
					
					//echo $query_mailpreview;
	
	$result_mailpreview = Select($query_mailpreview,$harry);	
				


?>
	

<div class="form-group row">
<div class="col-lg-12">

<table class="table table-striped- table-bordered table-hover table-checkable" id="" >
<thead>
	<tr>
		<th>Mail Preview</th>
	</tr>
</thead>
	<tbody>
	    
			<tr>
				<td>
				<?php
				$body = '';
				$subject= '';
				
				if($workshop_id!="")
				{	
					
				$body .= "<p>Hello ";
				
				$body .= 'Student';
				

				$body .=",<br/>
				Greetings from Edupristine!<br/>This is to inform you that your class has been <span style='font-size:18px;color:#006400;font-weight:bold;'>Confirmed</span>. Details of the same are as below:	
				</p>";
				
				foreach($result_mailpreview['rows'] as $info)
				{
				$subject = "Class Confirmed: ";
				if(isset($info['day'])){
				$subject .= " ".$info['day'];
				}else{
				$subject .= 'N/A';
				}
				if(isset($info['ws_date'])){
				$subject .= " ".$info['ws_date'];
				}else{
				$subject .= 'N/A';
				}
				if(isset($info['location'])){
				$subject .= " ".$info['location'];
				}else{
				$subject .= 'N/A';
				}
				if(isset($info['course'])){
				$subject .= " ".$info['course'];
				}else{
				$subject .= 'N/A';
				}	
					
				$body .="<table border='1' cellspacing='0px'><tr><th style='background-color:#16365C;color:#fff;'>Class Date</th><td width='300px'>";
				if(isset($info['ws_date'])){
				$body .= $info['ws_date'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Day</th><td width='300px'>";
				if(isset($info['day'])){
				$body .= $info['day'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Batch Code</th><td width='300px'>";
				if(isset($info['batch_code'])){
				$body .= $info['batch_code'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Start Time</th><td width='300px'>";
				if(isset($info['start_time'])){
				$body .= $info['start_time'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>End Time</th><td width='300px'>";
				if(isset($info['end_time'])){
				$body .= $info['end_time'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Location</th><td width='300px'>";
				if(isset($info['location'])){
				$body .= $info['location'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Venue</th><td width='400px' style='color:#d35400;font-weight:bold;font-size:16px;'>";
				if(isset($info['venue_details']) && $info['venue_details']!=''){
				$body .= $info['venue_details'];
				}else if(isset($info['venue_details']) && ($info['location'] == 'LVC' || $info['location'] == 'Online')){
				$body .= 'N/A';
				}else{
				$body .= 'Venue Detail will be Shared on Friday via Email';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Course</th><td width='300px'>";
				if(isset($info['course'])){
				$body .= $info['course'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Batch Start Date</th><td width='300px'>";
				if(isset($info['start_date'])){
				$body .= $info['start_date'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Topic</th><td width='300px'>";
				if(isset($info['topic'])){
				$body .= $info['topic'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Faculty Name</th><td width='300px'>";
				if(isset($info['name'])){
				$body .= $info['name'];
				}else{
				$body .= 'N/A';
				}
				$body .="</td></tr></table><br/>";
				}
				$body .="<p>Please feel free to contact the following persons in case of any problem.<table border='1' cellspacing='0px'><tr><th style='background-color:#16365C;color:#fff;'>For Schedule</th><td>";
				
				$body .= 'Customer Support';
				
				$body .="</td><td>";
				
				$body .= 'care@edupristine.com';
				
				$body .="</td><td>";
				
				$body .= '1800 200 5835';
				
				$body .="</td></tr>";
				$body .="</table></p>";
				
				$body .="		
			    <p>Regards,<br/><strong>";
				
				$body .= 'Edupristine';
				
				$body .="</strong></p>";
				echo $body;
				
				
				echo "<input hidden type='text' name='batchcode' id='batchcode' value='".$info['batch_code']."' />";
				}
			    echo "<input hidden  type='text' name='wsdate' id='wsdate' value='".$workshop_date."' />";
				
                
				?>
				<input hidden type="text" name="bodycontent" id="bodycontent" value="<?php echo $body;?>" />
				<input hidden type="text" name="wid" id="wid" value="<?php echo $workshop_id;?>" />
				<input hidden type="text" name="subjectcontent" id="subjectcontent" value="<?php echo $subject;?>" />
				
				</td>

</tr>
</tbody>
</table>
<!--end: Datatable -->

</div>
</div>
<div class="form-group row">
<div class="col-lg-12">
<label class="">Enter Student/s Email ID/s (Comma Seperated)</label>
<div class="">
<textarea class="form-control" id="emails" name="emails"  ></textarea>
</div>
</div>
</div>

</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			<button type="submit" name="submit" id="kt_blockui_3_5" class="btn btn-success" >Send Emails</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>

<script>
$('body').on('click', '#submit', function() { 
     alert('test'); 
     App.startPageLoading({animate: true});	 
	 
});



function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function fillwdates(workshopid)
{
	
	var workshop_id = workshopid;
	
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById('ws_date').innerHTML=xmlhttp.responseText;
		}
	}

	xmlhttp.open('POST', 'ajax/fillwdates.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('workshop_id='+fixEscape(workshop_id));
	

	
}


function getMultipleSelectedValue()
    {
	 
      var x=document.getElementById("w_date");
	  var wid=document.getElementById("kt_select2_3").value;
	 
	  var datearray=[];
      for (var i = 0; i < x.options.length; i++) {
         if(x.options[i].selected ==true){
			  datearray.push(x.options[i].value);
          }
      }
	  var url = "send_schedule_mail.php?workshop_id="+wid+"&ws_date="+datearray+"";
	  window.location = url;
	  
    }
</script>