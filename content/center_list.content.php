<?php
	$query_allcenters = "SELECT c.id,center_name,center_manager,loc_id FROM `centers` c ORDER BY center_name";
	$result_allcenters = Select($query_allcenters,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Centers List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="center_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Center
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Center Name</th>
												<th>Center Manager</th>
												<th>Center Location</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allcenters['rows'] as $center) 
											{	
											$query_cenmanager = "SELECT id,user_name FROM `users` where id=".$center['center_manager']."";
											$result_cenmanager = Select($query_cenmanager,$conn);
											
											$query_cenloc = "SELECT id,loc_name FROM `locations` where id=".$center['loc_id']."";
											$result_cenloc = Select($query_cenloc,$conn);
											?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $center['center_name']; ?></td>
													<td><?php echo $result_cenmanager['rows'][0]['user_name']; ?></td>
													<td><?php echo $result_cenloc['rows'][0]['loc_name']; ?></td>
													
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="center_edit.php?id=<?php echo $center['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>

														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>