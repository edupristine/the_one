<!-- end:: Subheader -->
<?php
$mysql_host='13.126.164.124';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$connsf=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'wp_tetris');

$query_leads = "Select * from site_chat where details_updated=0 and del_flag=0 order by id desc";
$resultlead = mysqli_query($connsf,$query_leads);
$num_rows1 = mysqli_num_rows($resultlead);

if($num_rows1 != 0) 
{
while($rlead = mysqli_fetch_assoc($resultlead)) {
    $newleads[] = $rlead;
}
}
?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update Site Chat Details
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">



	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:5%;">#</th>
				<th style="width:12%;">Full Name</th>
				<th style="width:15%;">Email</th>
				<th style="width:12%;">City</th>
				<th style="width:12%;">Phone</th>
				<th style="width:12%;">Course 2</th>
				<th style="width:12%;">Location</th>
				<th style="width:10%;" ><center>Action</center></th>
			</tr>
		</thead>
		<tbody id="rows_div">
			<?php
			if($num_rows1 != 0) 
            {
			echo "<input type='text' id='tocount' name='totcountg' value='".$num_rows1."' hidden />";
			$i=1;
			foreach($newleads as $leads)
			{
			if($leads['del_flag']==0)
			{				
				
			?>
			<tr id="row_id_1">
				<td class="text-center"><input readonly name="sr_<?php echo $i;?>" id="sr_<?php echo $i;?>" style="width:100%;padding:2px;border:0;" type="text" value="<?php echo $leads['id'];?>"/></td>
				<td class="text-center"><input class="form-control ktselect2" name="fullname_<?php echo $i;?>" id="fullname_<?php echo $i;?>" style="width:100%;padding:2px;" type="text"  value="<?php echo $leads['gaUser'];?>"  /></td>
				<td class="text-center"><input class="form-control" name="email_<?php echo $i;?>" id="email_<?php echo $i;?>" style="width:100%;padding:2px;" type="text"  value="<?php echo $leads['gaEmail'];?>"  /></td>
				<td class="text-center"><input class="form-control" name="city_<?php echo $i;?>" id="city_<?php echo $i;?>" style="width:100%;padding:2px;" type="text"  value="<?php echo $leads['gaCity'];?>"  /></td>
				<td class="text-center"><input class="form-control" name="phone_<?php echo $i;?>" id="phone_<?php echo $i;?>" style="width:100%;padding:2px;" type="text"  value="<?php echo $leads['gaPhone'];?>"  /></td>
				<td  class="text-left">
					<select class="form-control ktselect2" id="course2_<?php echo $i;?>"  name="course2_<?php echo $i;?>" style="width:100%;padding:2px;">
					<option value="0">--None--</option>
					<option value="ACCA">ACCA</option>
					<option value="Accounts">Accounts</option>
					<option value="ACFM">ACFM</option>
					<option value="Advance Excel">Advance Excel</option>
					<option value="AIFS">AIFS</option>
					<option value="Analytics">Analytics</option>
					<option value="Big Data & Hadoop">Big Data & Hadoop</option>
					<option value="Business Accounting & Taxation">Business Accounting & Taxation</option>
					<option value="CFA">CFA</option>
					<option value="CFA - Level I">CFA - Level I</option>
					<option value="CFA - Level II">CFA - Level II</option>
					<option value="CFA - Level III">CFA - Level III</option>
					<option value="CFA - Level I PT">CFA - Level I PT</option>
					<option value="Charting Techniques">Charting Techniques</option>
					<option value="CMA">CMA</option>
					<option value="CPA">CPA</option>
					<option value="Data Analysis">Data Analysis</option>
					<option value="Data Science">Data Science</option>
					<option value="Demand & Supply Analysis">Demand & Supply Analysis</option>
					<option value="Digital Marketing">Digital Marketing</option>
					<option value="Finance">Finance</option>
					<option value="Financial Modeling">Financial Modeling</option>		
					<option value="FRM">FRM</option>
					<option value="Google Analytics & Affiliate Marketing">Google Analytics & Affiliate Marketing</option>
					<option value="GST">GST</option>
					<option value="Healthcare">Healthcare</option>
					<option value="HR Analytics">HR Analytics</option>
					<option value="Human Resources">Human Resources</option>
					<option value="IFRS">IFRS</option>
					<option value="Investment Banking">Investment Banking</option>
					<option value="Machine Learning">Machine Learning</option>
					<option value="Marketing">Marketing</option>
					<option value="Mobile, E-Mail & Affiliate Marketing">Mobile, E-Mail & Affiliate Marketing</option>
					<option value="Other">Other</option>
					<option value="Other Accounts">Other Accounts</option>
					<option value="Other Analytics">Other Analytics</option>
					<option value="Other Finance">Other Finance</option>
					<option value="Other Marketing">Other Marketing</option>
					<option value="PGCFR">PGCFR</option>
					<option value="Predictive Business Analytics">Predictive Business Analytics</option>
					<option value="Search Engine Optimization (SEO)">Search Engine Optimization (SEO)</option>
					<option value="Social Media Marketing">Social Media Marketing</option>
					<option value="USMLE">USMLE</option>
					<option value="USMLE-Clinical Rotations">USMLE-Clinical Rotations</option>
					<option value="USMLE-Step 1">USMLE-Step 1</option>
					<option value="USMLE-Step 2 CK">USMLE-Step 2 CK</option>
					<option value="USMLE-Step 2 CS">USMLE-Step 2 CS</option>
					<option value="VBA & MACROS">VBA & MACROS</option>		
					</select>
				</td>
				<td>
				<select class="form-control ktselect2" id="location_<?php echo $i;?>" name="location_<?php echo $i;?>">
				<option value="0">--None--</option>
				<option value="Bangalore">Bangalore</option>
				<option value="Chennai">Chennai</option>
				<option value="Delhi">Delhi</option>
				<option value="Hyderabad">Hyderabad</option>
				<option value="Mumbai">Mumbai</option>
				<option value="Pune">Pune</option>
				<option value="Other">Other</option>

				</select>
				</td>
                <td style="text-align:center;">
				<button onClick="deletelead(<?php echo $leads['id']; ?>);" type="button" class="btn-sm btn-danger btn-elevate btn-pill">Delete</button>
				</td>
			</tr>
			<?php
			$i++;
			}
			}
			}
			else
			{
				echo "No Records to update";
			}
			?>
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-8">
			<a onClick="updatecourse2();" class="btn btn-success">Update Details</a>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>