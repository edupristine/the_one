<!-- end:: Subheader -->
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['dupno']))
{
$msg = "Feedback Submission Failed. Student already Exists.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['paramsmissing']))
{
$msg = "Feedback Submission Failed. Student Name is Mandatory Field.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "Feedback Submission Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "Feedback Submitted Successfully.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Atharva Seminar Feedback
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/atharva.php'>
<div class="kt-portlet__body">
<div class="form-group row"hidden>
<div class="col-lg-12">
<label>Batch Code:</label>
<input type="text" class="form-control" id="batch_code" name="batch_code" value="ATH-SEM">
</div>
</div>
<div class="form-group row">
<div class="col-lg-12">
<label class="">Name:</label>
<input type="text" class="form-control" id="student_name"  name="student_name"required>
</div>
</div>
<div class="form-group row">
<div class="col-lg-12">
<label>Email:</label>
<input type="text" class="form-control" id="email" name="email"required>
</div>
</div>
<div class="form-group row">
<div class="col-lg-12">
<label class="">Mobile No.:</label>
<input type="number" class="form-control" id="phone"  name="phone"required>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Feedback about the Seminar:</label>
<textarea type="text" class="form-control" id="comments"  name="comments" ></textarea>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Are you planning to join the Financial Modeling course?:</label>
<select id="coun_req"  name="coun_req" class="form-control" required >
<option value="">Select</option>
<option value="1">Yes</option>
<option value="0">No</option>
</select>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Any other course you would be interested in?:</label>
<textarea type="text" class="form-control" id="interestedcourses"  name="interestedcourses" ></textarea>
</div>
</div>




</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button type="submit" class="btn btn-success" onclick="return Validate();">Send Feedback</button>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<script>
	function Validate()
	{
		var student_name=document.getElementById('student_name').value;
		var email=document.getElementById('email').value;
		var phone=document.getElementById('phone').value;
		var coun_req=document.getElementById('coun_req').value;
		var comments=document.getElementById('comments').value;
		
		
		if(student_name=='')
		{
			alert("Name is mandatory!");
			student_name.focus();
		}
		
		if(email=='')
		{
			alert("Email is mandatory!");
			email.focus();
		}else
		{
			 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) 
            {
                alert('Please enter a valid Email!');
				email.focus();
				submit_button_clicked = '';
                return (false);
            }
		}
		
		if(phone=='')
		{
			alert("Mobile No. is mandatory!");
			phone.focus();
			submit_button_clicked = '';
		}
		
		if(comments=='')
		{
			alert("Seminar Feedback is mandatory!");
			submit_button_clicked = '';
			return (false);
		}
		
		if((coun_req==''))
		{
		  alert("Please select Yes/No to join the Financial Modeling course!");
            submit_button_clicked = '';		  
		    return (false);
		}
		
		
		
		
	
		
	}
</script>
