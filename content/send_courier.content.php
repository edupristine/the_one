<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'send_courier.php?filter_param='+filter_param;
	}
</script>
<?php
	$location_id=1;
	$course_id = get_get_value('cid');
	$course_name = get_get_value('course_name');
	
	$qrys="";
	if($course_id!='')
	{
		$qrys="and s.course_id = ".$course_id."";
	}
	elseif($course_id=='')
	{
		$qrys="and s.course_id in (SELECT course_id from sub_courses where final_live=1) ";
	}
	

	$status = get_get_value('status');
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		if(($status!="")&&($status!="All"))
		{
		
		if($status=="Pending")
		{
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid,sc.created_at FROM `students` s,student_couriers_request sc, sub_courses sb WHERE sb.id=s.subcourse_id  and  sc.student_id = s.id and sc.courier_status in ( 'Requested','Partially Sent' )and sc.sending_center = '".$location_id."' ".$qrys." ORDER BY sc.courier_status";
		}
		elseif($status=="Sent")
		{
		$query_allstudents = "SELECT s.id,s.student_name,s.crm_id,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid,sc.created_at FROM `students` s,student_couriers_request sc, sub_courses sb WHERE sb.id=s.subcourse_id  and  sc.student_id = s.id AND s.created_at BETWEEN CURDATE() - INTERVAL 100 DAY AND CURDATE() and sc.courier_status = 'Sent' and sc.sending_center = '".$location_id."' ".$qrys." ORDER BY sc.courier_status";
		}
		
		}else
		{
		$query_allstudents = "SELECT s.id,s.student_name,s.crm_id,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid,sc.created_at FROM `students` s,student_couriers_request sc, sub_courses sb WHERE sb.id=s.subcourse_id  and  sc.student_id = s.id and sc.sending_center = '".$location_id."' ".$qrys." ORDER BY sc.courier_status";
		}
	}
	else
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.crm_id,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid,sc.created_at FROM `students` s,student_couriers_request sc, sub_courses sb WHERE sb.id=s.subcourse_id  and  sc.student_id = s.id and (student_name = '".$filter_param."' OR crm_id = '".$filter_param."' OR student_email = '".$filter_param."' OR student_contact = '".$filter_param."' OR subcourse_name = '".$filter_param."') ORDER BY sc.courier_status";
	}
	

	$result_allstudents = Select($query_allstudents,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$course = "SELECT id,course_name from courses where final_live=1 order by course_name ";
	$result_course = Select($course,$conn);
	
	if($course_name!="")
	{
	 $course_name_report = $course_name;
	}

	
	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Send Courier <?php 
											
												echo "(".$location_name_report."  ".$course_name_report.")"; 
	                                       
											?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="col">
														<table><tr>
														<td>
														
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Courses
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_course['rows'] as $course)
																			{	
																				
																				?>
																				<a class="dropdown-item" href="send_courier.php?uid=<?php echo $user_id;?>&cid=<?php echo $course['id'];?>&lid=<?php echo $location_id;?>&status=Pending&course_name=<?php echo $course['course_name'];?>"><?php echo $course['course_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															
														
															</td>
														<td>
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Status
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																	<a class="dropdown-item" href="send_courier.php?status=Pending">Pending</a>
																	<a class="dropdown-item" href="send_courier.php?status=Sent">Sent</a>
																	<a class="dropdown-item" href="send_courier.php?status=All">All</a>
																	</div>
																
															</div>
															</td></tr></table>
														</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Org No.</th>
												<th>Student Name</th>
												<th >Courier Status</th>
												
												<th>Student contact</th>
												<th>Student Course</th>
												<th>Student Subcourse</th>
												<th>Raised On</th>
												<th colspan=2 ><center>Action</center></th>
												
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										       
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['crm_id']; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td ><?php echo $student['courier_status']; ?></td>
													<td><?php echo $student['student_contact']; ?></td>
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_subcourses['rows'][0]['subcourse_name']; ?></td>
													<td><?php echo $student['created_at']; ?></td>
													<td style="text-align:center;vertical-align:middle;">
													<div class="btn-group" role="group" aria-label="First group">
															<a href="#" class="kt-nav__link-icon flaticon-eye" data-toggle="modal" data-target="#courierview_<?php echo $student['courierid']; ?>" ></a>
														</div>
													</td>
													<td><center>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#student_<?php echo $student['courierid']; ?>">Send Courier</button>
													
													</center></td>
												    <!--<td style="text-align:center;vertical-align:middle;">
													<a href="courier_print.php?id=<?php echo $student['id']; ?>" class="kt-nav__link-icon flaticon-technology"  target=_blank ></a>
													</td>-->
								
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$result_select_issue="";
												$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and  isu.student_id = ".$student['id']." and isu.courier_req_id='".$student['courierid']."' ";
											
										        $result_select_issue = Select($select_issue,$conn);
												
												
												$courier_int = "SELECT user_name from users where id = ".$student['courier_intiated_by'];
												$result_courier_int = Select($courier_int,$conn);
											?>
											<div class="modal fade" id="student_<?php echo $student['courierid']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Send Courier For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $student['courierid'];?>"  >
                                                                        <thead>
																		<tr>
																				<th >#</th>
																				<th>SKU Name</th>
																				<th>Select</th>
																		</tr>
																		</thead>
																		<tbody>
																		 
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['courierid']."_".$i; ?>>
																						<td ><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																							<?php if($each_issue['issued_status']) echo "Issued"; 
																							else
																							{
																							$cbconcat = $cbconcat."cb_".$student['courierid']."_".$each_issue['skuid']."|";?>
																							<input type="checkbox" id="cb_<?php echo $student['courierid']."_".$each_issue['skuid']; ?>" name="cb_<?php echo $student['courierid']."_".$each_issue['skuid']; ?>" value="1">
																							<?php
																							}
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																			<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																		</tbody>
																		
																		
																	</table>
																	
																	<table class="table table-striped- table-bordered table-hover table-checkable"  id= "<?php echo $student['courierid'];?>">
																	<tr>
																		<td>Courier Provider<input type="text" id="courier_provider_<?php echo $student['courierid'];?>" name="courier_provider_<?php echo $student['courierid'];?>" value="India Post" />
																		</td>
																		<td colspan=2>
																		Courier Track No.
																		<input type="text" id="courier_track_no_<?php echo $student['courierid'];?>" name="courier_track_no_<?php echo $student['courierid'];?>" />
																		</td>
																		</tr>
																		<tr>
																		<td>
																		<input type="hidden" id="courier_charges_<?php echo $student['courierid'];?>" name="courier_charges_<?php echo $student['courierid'];?>" />
																		</td>
																		<td colspan=2>
																		Intiated By: <?php echo $result_courier_int['rows'][0]['user_name']."<br>";?>
																		<?php
																		$display_cour_rec= "SELECT count(id) as count from student_couriers_records where courier_req_id='".$student['courierid']."'  ";
																		$result_cour_rec=Select($display_cour_rec,$conn);
																		echo "Total No. of Courier's Sent:".$result_cour_rec['rows'][0]['count'];
																		?>
																		</td>
																		</tr>
																		<tr>
																		<td colspan=3>
																		Student Address
																		<textarea id="courier_address_<?php echo $student['courierid']; ?>" name="student_address" cols="60" rows="5" readonly style="border:none;"><?php echo $student['student_address'];?></textarea>
																   	   </div>
																
																		</td>
																		</tr>
																		
																	</table>
																	<input hidden id="hidden_<?php echo $student['courierid']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="SendCourier(<?php echo $student['id']; ?>,<?php echo $location_id; ?>,<?php echo $student['courierid']; ?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												
												
												$courier_int = "SELECT user_name from users where id = ".$student['courier_intiated_by'];
												$result_courier_int = Select($courier_int,$conn);
												
																	
											?>
											<div class="modal fade" id="courierview_<?php echo $student['courierid']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Send Courier Summary For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
																
																
																<?php
																	  
																	$query_req="select * from student_couriers_request where student_id=".$student['id']." and id=".$student['courierid'];
                                                                    $result_req=Select($query_req,$conn);	
																	foreach($result_req['rows'] as $reqs)
																	{
																	$query_rec="select * from student_couriers_records where courier_req_id=".$reqs['id'];
                                                                    $result_rec=Select($query_rec,$conn);	
																	foreach($result_rec['rows'] as $recs)
																	{
																	$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and isu.courier_record_id=".$recs['id']."   and  isu.student_id = ".$student['id'];
																	$result_select_issue = Select($select_issue,$conn);
																		
																		echo "<strong>Courier Provider:-".$recs['courier_provider']."<br>";
																		echo "Tracking No:-".$recs['courier_track_no']."<br>";
																		echo "Sent On:-".$recs['created_at']."<br></strong>";
																	?>
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $student['id'];?>"  >
                                                                        
																		
																	
																		<thead>
																		<tr>
																				<th >#</th>
																				<th>SKU Name</th>
																				<th>Status</th>
																		</tr>
																		</thead>
																		<tbody>
																	
																	
																		 
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td ><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																							<?php if($each_issue['issued_status']) echo "Issued"; 
																							else
																							{
																							echo "Not Issued";
																							}
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																				
																				
																				
																			?>
																			<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																		</tbody>
																		
																		
																	</table>
																	<hr></hr>
																	<?php
																	}
																	
																	}
																	?>
																	
																	
																	<input hidden id="hidden_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
										
																			
										
									<!--end: Datatable -->

								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function SendCourier(student_id,loc_id,courierid)
{
    
	var courier_prov = document.getElementById('courier_provider_'+courierid).value;
	var courier_track_no = document.getElementById('courier_track_no_'+courierid).value;
	var courier_charges = document.getElementById('courier_charges_'+courierid).value;
	
	var tb = "hidden_"+courierid;
	var cbs = document.getElementById(tb).value;
	console.log(cbs);
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);
	//alert(final_cbs_json);
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Courier Record Created Successfully.");
				submit_button_clicked = '';
				$('#student_'+courierid).modal('toggle');
				window.location.reload();
			}
		    else if(output.status == 'insufficient_qty')
			{
				alert(output.message+" for "+output.sku_name);
				submit_button_clicked = '';
			}
			else if(output.status == 'db_error')
			{
				alert("Courier Record Creation Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Courier Record Creation Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/send_courier.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id)+'&courier_prov='+fixEscape(courier_prov)+'&courier_track_no='+fixEscape(courier_track_no)+'&courier_charges='+fixEscape(courier_charges)+'&courierid='+fixEscape(courierid));
}



</script>