<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		var date_range = document.getElementById('date_range').value;
		window.location = 'refund_process_list.php?filter_param='+filter_param+'&date_range='+date_range;
	}
	
	
</script>
<?php
    $team="";
	$user="";

	$query_userteams = "SELECT team_id,user_id FROM users_teams  where user_id ='".$_SESSION['USER_ID']."' and team_id in (1,7,8,10,11)";
	$result_team = Select($query_userteams,$conn);

    $team=$result_team['rows'][0]['team_id'];
	$user=$result_team['rows'][0]['user_id'];

	$status = get_get_value('status');
	$date_range = get_get_value('date_range');
	
	$dtquery="";
	if($date_range != ""){
	$drange = explode(" / ",$date_range);
	$sdate = date('Y-m-d',strtotime($drange[0]));
	$edate = date('Y-m-d',strtotime($drange[1]));
	
	$dtquery = " and CONVERT(raised_on, DATE) between '".$sdate."' and '".$edate."' ";
	
	}

	
	$qrys="";
	if($status!='')
	{
		if($status=="3")
		{
		if(($team=="1")||($team=="7"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) )";	
		}
		if(($team=="8")||($team=="10")||($team=="11"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE (approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10,11)) or (approver_id=4 and approval_status = '1') ) )";		
		}
		}
		else
		{
		
		if(($team=="1")||($team=="7"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) and approval_status = ".$status.")";	
		}
		if(($team=="8")||($team=="10")||($team=="11"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10,11) or (approver_id=4 and approval_status = '1'))  and approval_status = ".$status.")";		
		}
		}
	}
	elseif($status=='')
	{
		if(($team=="1")||($team=="7"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) )";	
		}
		if(($team=="8")||($team=="10")||($team=="11"))
		{
		$qrys="and id in (SELECT refund_req_id FROM refund_details WHERE (approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10,11)) or (approver_id=4 and approval_status = '1') ) )";		
		}
	}

	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		
		$query_allrefunds = "SELECT * FROM student_refund_requests where id!=0  $dtquery $qrys order by id desc";
	    
	}
	else
	{
		
		$query_allrefunds = "SELECT * FROM student_refund_requests WHERE (student_name like '%".$filter_param."%' OR account_no like '%".$filter_param."%' OR email_id like '%".$filter_param."%' OR mobile_no like '%".$filter_param."%') $dtquery $qrys ORDER BY id desc";
	}
	
   
	$result_allrefunds = Select($query_allrefunds,$conn);	
	
	
	
	//echo $query_allrefunds;

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<input type='hidden' id='team' name='team' value='<?php echo $team;?>'>
<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-4">
<label>Date Range</label>
<div class="input-group" id="kt_daterangepicker_2">
<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
<div class="input-group-append">
<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
</div>
</div>
</div>
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Submit</button>
	
</div>

<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_export();" type="button" class="btn btn-primary form-control">Export</button>
</div>


</div>


</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Refund Request List
											<?php if(($team=="1")||($team=="7")) 
											{
                                             echo "(Support)"; 
											}
                                           else
										   {
                                             echo "(Accounts & Finance)";
										   }											   
											?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="col">
														<table><tr>
														<td>
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Refund Status
																</button>
																<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																<a class="dropdown-item" href="refund_process_list.php?status=1">Approved</a>
																<a class="dropdown-item" href="refund_process_list.php?status=0">Rejected</a>
																<a class="dropdown-item" href="refund_process_list.php?status=3">All</a>
																</div>
																
															</div>
															</td></tr></table>
														</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">
                              <input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th >#</th>
												<th >Student Name</th>
												<th >Org No.</th>
									            <th >Contact</th>
											    <th >FeesPaid</th>
												<th >Refund Type</th>
												<th >Total Val. Amount</th>
												<th >Amount to refund</th>
												<th >RaisedOn</th>
												<th >No.Days</th>
												<th colspan ="4" ><center> Approval Levels</center></th>
											    <th ><center>Action</center></th>
												
												
											</tr>
											
											<tr>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
									            <th rowspan="2"></th>
											    <th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"><center> Support</center></th>
												<th rowspan="2"><center> CEO</center></th>
												<th rowspan="2"><center> Accounts Level 1</center></th>
												<th rowspan="2"><center> Accounts Level 2</center></th>
											    <th rowspan="2"></th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											$days = 0;
											foreach($result_allrefunds['rows'] as $student) 
											{	
												
												$now = time(); 
												$your_date = strtotime($student['created_at']);
												$datediff = $now - $your_date;
                                                $days = round($datediff / (60 * 60 * 24));
												$displayday="";
												if($days>90)
												{
												$displayday =  "<font color='red'><strong>$days</strong></font>";	
												}
												else
												{
                                                $displayday = "<font color='green'><strong>$days</strong></font>";
												}													
												
												
												
												$query_supapproval = "SELECT approval_status,created_at,approver_id FROM refund_details  WHERE refund_req_id=".$student['id']." AND approver_id in (SELECT user_id FROM users_teams  where team_id in (1)) ";
												$result_sup = Select($query_supapproval,$conn);	
												
												
												$query_supapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) ";
												$result_sup = Select($query_supapproval,$conn);	
												
												$query_ceoapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	

                                                $query_accapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10)) ";
												$result_acc = Select($query_accapproval,$conn);
												
												
												
												$query_accapproval2 = "SELECT *,us.user_name FROM student_refund_requests rd, users us WHERE rd.actual_refund_processed_by=us.id and rd.actual_refund_processed_by in (SELECT user_id FROM users_teams  where  team_id in (11)) AND rd.id=".$student['id']."";
												$result_acc2 = Select($query_accapproval2,$conn);	

												$supdate = date('d-m-Y',strtotime($result_sup['rows'][0]['created_at']));
												if($supdate=='01-01-1970')
												{
												$supdate =  "NA";
												}
												else
												{
												$supdate =  $supdate;	
												 
												}
												
												
												$ceodate = date('d-m-Y',strtotime($result_ceo['rows'][0]['created_at']));
												if($ceodate=='01-01-1970')
												{
												$ceodate = "NA";
												}
												else
												{
												$ceodate = $ceodate;	
												}
												
												$accdate = date('d-m-Y',strtotime($result_acc['rows'][0]['created_at']));
												if($accdate=='01-01-1970')
												{
												$accdate = "NA";
												}
												else
												{
												$accdate = $accdate;	
												}
												
												
												$accdate2 = date('d-m-Y',strtotime($result_acc2['rows'][0]['created_at']));
												if($accdate2=='01-01-1970')
												{
												$accdate2 = "NA";
												}
												else
												{
												$accdate2 = $accdate2;	
												}

												$supstatus="";
												$supuser=$result_sup['rows'][0]['user_name'];
												$ceouser=$result_ceo['rows'][0]['user_name'];
												$accuser=$result_acc['rows'][0]['user_name'];
												$accuser2=$result_acc2['rows'][0]['user_name'];
												if($result_sup['rows'][0]['approval_status']=="1")
												{
												$supstatus="<font color='green'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $supdate  Processed By : $supuser' >
                         <strong> Approved</strong>
                        </div></font>";	
												}
												else if($result_sup['rows'][0]['approval_status']=="0")
												{
												$supstatus="<font color='red'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $supdate Processed By : $supuser'><strong>Rejected</strong></div></font>";		
												}
												else
												{
												$supstatus="<font color='black'>NA</font>";	
												}

												$query_ceoapproval = "SELECT approval_status,created_at,approver_id FROM refund_details  WHERE refund_req_id=".$student['id']." AND approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	


												$ceostatus="";
												if($result_ceo['rows'][0]['approval_status']=="1")
												{
												$ceostatus="<font color='green'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $ceodate Processed By : $ceouser' ><strong>Approved</strong></div></font>";	
												}
												else if($result_ceo['rows'][0]['approval_status']=="0")
												{
												$ceostatus="<font color='red'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $ceodate Processed By : $ceouser'><strong>Rejected</strong></div></font>";		
												}
												else
												{
												$ceostatus="<font color='black'>NA</font>";	
												}

												$query_accapproval = "SELECT approval_status,created_at,approver_id FROM refund_details  WHERE refund_req_id=".$student['id']." AND  approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10))  ";
												$result_acc = Select($query_accapproval,$conn);	


												$accstatus="";
												if($result_acc['rows'][0]['approval_status']=="1")
												{
												$accstatus="<font color='green'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $accdate Processed By : $accuser'><strong>Approved</strong></div></font>";	
												}
												else if($result_acc['rows'][0]['approval_status']=="0")
												{
												$accstatus="<font color='red'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $accdate Processed By : $accuser'><strong>Rejected</strong></div></font>";		
												}
												else
												{
												$accstatus="<font color='black'>NA</font>";	
												}
												
												
											
										    	$query_accapproval2 = "SELECT *,us.user_name FROM student_refund_requests rd, users us WHERE rd.actual_refund_processed_by=us.id and rd.actual_refund_processed_by in (SELECT user_id FROM users_teams  where  team_id in (11)) AND rd.id=".$student['id']."";
												$result_acc2 = Select($query_accapproval2,$conn);												

                                                $tran_id= $result_acc2['rows'][0]['actual_refund_transaction_id'];
												$tran_amt= $result_acc2['rows'][0]['actual_refund_amount'];
												$tran_cat= $result_acc2['rows'][0]['refund_category'];
												$accstatus2="";
												if($result_acc2['rows'][0]['actual_refund_transaction_id']!="")
												{
												$accstatus2="<font color='orange'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $accdate2 Processed By : $accuser2 Transaction ID: $tran_id Transaction Amt: $tran_amt Transaction Category: $tran_cat'><strong>Refund Completed</strong></div></font>";	
												}
												
												else
												{
												$accstatus2="<font color='black'>NA</font>";	
												}


											
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td><?php echo $student['account_no']; ?></td>
													<td><?php echo $student['mobile_no']; ?></td>
													<td><?php echo $student['fees_paid']; ?></td>
													<td><?php echo $student['refund_category']; ?></td>
													<td><?php echo $student['total_validated_amt']; ?></td>
													<td><?php echo $student['amount_to_refund']; ?></td>
													<td><?php echo date('d-m-Y',strtotime($student['created_at'])); ?></td>
													<td><center><?php echo $displayday; ?></center></td>
													<td><center><?php echo $supstatus; ?></center></td>
													<td><center><?php echo $ceostatus; ?></center></td>
													<td><center><?php echo $accstatus; ?></center></td>
													<td><center><?php echo $accstatus2; ?></center></td>
													<td style="text-align:center;vertical-align:middle;">
													<div class="btn-group" role="group" aria-label="First group">
															<a href="#" class="kt-nav__link-icon flaticon-eye" data-toggle="modal" data-target="#refundview_<?php echo $student['id']; ?>" ></a>
															
														</div>
														<?php
														if(($team=="10"))
		                                                {
														?>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="#" class="kt-nav__link-icon flaticon2-writing" data-toggle="modal" data-target="#approverefund_<?php echo $student['id']; ?>" ></a>
														</div>
														<?php
														}
														?>
													</td>
													

													
													
								
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

										
										<?php 
											
											foreach($result_allrefunds['rows'] as $student) 
											{	
											
												
												$query_supapproval = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) ";
												$result_sup = Select($query_supapproval,$conn);	
												
												$query_ceoapproval = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	

                                                $query_accapproval = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (8,10)) ";
												$result_acc = Select($query_accapproval,$conn);	
												
												//$query_accapproval2 = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (11)) ";
												
												$query_accapproval2 = "SELECT *,us.user_name FROM student_refund_requests rd, users us WHERE rd.actual_refund_processed_by=us.id and rd.actual_refund_processed_by in (SELECT user_id FROM users_teams  where  team_id in (11)) AND rd.id=".$student['id']."";
												$result_acc2 = Select($query_accapproval2,$conn);	


												
																	
											?>
											<div class="modal fade" id="refundview_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Refund Process Summary For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
																<table width="100%">
																<tr>
																<td style="padding: 6px 6px; "><strong>Org No.:</strong><?php echo $student['account_no'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Email.:</strong><?php echo $student['email_id'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Vertical Enrolled.:</strong><?php echo $student['vertical_enrolled'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Course Enrolled.:</strong><?php echo $student['course_enrolled'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Batch Date.:</strong><?php echo $student['batch_date'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Batch Location.:</strong><?php echo $student['batch_location'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Date of Payment .:</strong><?php echo $student['payment_date'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Payment Mode.:</strong><?php echo $student['payment_mode'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Beneficiary Name.:</strong><?php echo $student['beneficiary_name'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Bank Name.:</strong><?php echo $student['bank_name'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Bank Account No.:</strong><?php echo $student['bank_account_no'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>IFSC Code.:</strong><?php echo $student['ifsc_code'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Branch Location:</strong><?php echo $student['branch_location'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Course Fee:</strong><?php echo $student['course_fee'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Total Classes:</strong><?php echo $student['total_classes'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Cost per classes.:</strong><?php echo $student['cost_per_class'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Class conducted:</strong><?php echo $student['class_conducted'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Administration charges:</strong><?php echo $student['administration_charges'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Third Party access cost:</strong><?php echo $student['third_party_cost'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Councelor name:</strong><?php echo $student['counselor_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>Reason for Refund:</strong><?php echo $student['refund_reason'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td colspan=2><hr></hr>
																</td>
																
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Support Processed. on :</strong><?php
																$supdate = date('d-m-Y',strtotime($result_sup['rows'][0]['created_at']));
																if($supdate=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $supdate;	
																}
																
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>Support Processed. by:</strong><?php echo $result_sup['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; "><strong>CEO Processed. on :</strong><?php 
																$ceodate = date('d-m-Y',strtotime($result_ceo['rows'][0]['created_at']));
																if($ceodate=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $ceodate;	
																}
																
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>CEO Processed. by:</strong><?php echo $result_ceo['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>CEO Remarks:</strong><?php echo $result_ceo['rows'][0]['comments'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Accounts Level 1 Processed. on :</strong><?php 
																$accdate = date('d-m-Y',strtotime($result_acc['rows'][0]['created_at']));
																if($accdate=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $accdate;	
																}
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>Accounts Level 1 Processed. by:</strong><?php echo $result_acc['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>Acc. Level 1 Remarks:</strong><?php echo $result_acc['rows'][0]['comments'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Accounts Level 2 Processed. on :</strong><?php 
																$accdate2 = date('d-m-Y',strtotime($result_acc2['rows'][0]['actual_refund_date']));
																if($accdate2=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $accdate2;	
																}
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>Accounts Level 2 Processed. by:</strong><?php echo $result_acc2['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																<?php if($result_acc2['rows'][0]['actual_refund_transaction_id']!=''){ ?>
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>Acc. Level 2 Details:</strong>Transaction ID:<?php echo $result_acc2['rows'][0]['actual_refund_transaction_id'];?> <br>Actual Refund Category: <?php echo $result_acc2['rows'][0]['refund_category'];?> <br> Actual Refund Amount: <?php echo $result_acc2['rows'][0]['actual_refund_amount'];?>
																</td>
																<td>
																</td>
																</tr>
																<?php
																}
																?>
																
																
																
																</table>
																
																	
																	
																	
																	
																
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
									<!--end: Datatable -->
									
										<?php 
											
											foreach($result_allrefunds['rows'] as $student) 
											{	
												$courier_int = "SELECT user_name from users where id = ".$student['id'];
												$result_courier_int = Select($courier_int,$conn);
												
												$query_supapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where  team_id in (1)) ";
												$result_sup = Select($query_supapproval,$conn);	
												
												$query_ceoapproval = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	

                                                $query_accapproval = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where user_id ='".$_SESSION['USER_ID']."' and team_id in (8,10)) ";
												$result_acc = Select($query_accapproval,$conn);	
												
												 $query_accapproval2 = "SELECT rd.approval_status,rd.created_at,us.user_name,rd.comments FROM refund_details rd, users us  WHERE rd.approver_id=us.id and rd.refund_req_id=".$student['id']." AND rd.approver_id in (SELECT user_id FROM users_teams  where user_id ='".$_SESSION['USER_ID']."' and team_id in (11)) ";
												$result_acc2 = Select($query_accapproval2,$conn);	


												
																	
											?>

											<div class="modal fade" id="approverefund_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Refund Process Summary For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
																<table width="100%">
																<tr>
																<td style="padding: 6px 6px; "><strong>Org No.:</strong><?php echo $student['account_no'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Email.:</strong><?php echo $student['email_id'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Beneficiary Name.:</strong><?php echo $student['beneficiary_name'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Bank Name.:</strong><?php echo $student['bank_name'];?>
																</td>
																<td>
																</td>
																</tr>
																<tr>
																<td style="padding: 6px 6px; "><strong>Bank Account No.:</strong><?php echo $student['bank_account_no'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>IFSC Code.:</strong><?php echo $student['ifsc_code'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Branch Location:</strong><?php echo $student['branch_location'];?>
																</td>
																<td style="padding: 6px 6px; "><strong>Amout To Refund:</strong><?php echo $student['amount_to_refund'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>Reason for Refund:</strong><?php echo $student['refund_reason'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td colspan=2><hr></hr>
																</td>
																
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; "><strong>Support Processed. on :</strong><?php
																$supdate = date('d-m-Y',strtotime($result_sup['rows'][0]['created_at']));
																if($supdate=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $supdate;	
																}
																
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>Support Processed. by:</strong><?php echo $result_sup['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																<tr>
																<td style="padding: 6px 6px; "><strong>CEO Processed. on :</strong><?php 
																$ceodate = date('d-m-Y',strtotime($result_ceo['rows'][0]['created_at']));
																if($ceodate=='01-01-1970')
																{
																echo "NA";
																}
																else
																{
																echo $ceodate;	
																}
																
																?>
																</td>
																<td style="padding: 6px 6px; "><strong>CEO Processed. by:</strong><?php echo $result_ceo['rows'][0]['user_name'];?>
																</td>
																<td>
																</td>
																</tr>
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>CEO Remarks:</strong><?php echo $result_ceo['rows'][0]['comments'];?>
																</td>
																<td>
																</td>
																</tr>
																
																
																
																<tr>
																<td style="padding: 6px 6px; " colspan=2><strong>Reason for Process:</strong>
																<textarea type="text" class="form-control"  name="comment_<?php echo $student['id']; ?>" id="comment_<?php echo $student['id']; ?>" rows="4" ></textarea>
																</td>
																<td>
																</td>
																</tr>
																
																
																
																</table>
																
																	
																	
																	
																	
																
																</div>
															</div>
															<div class="modal-footer">
															<button type="button" class="btn btn-info" onClick="refundapproval(<?php echo $student['id']?>,1);" id="approve" name="approve"  >Approve </button>
															<button type="button" class="btn btn-warning" onClick="refundapproval(<?php echo $student['id']?>,0);" id="reject" name="reject"  >Reject</button>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															

															
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>

								</div>
							</div>
</div>
<script>
function get_export()
	{
		var date_range = document.getElementById('date_range').value;
		var filter_param = document.getElementById('search_param').value;
		var selstatus = document.getElementById('selstatus').value;
		var team = document.getElementById('team').value;
		window.location = 'content/refundprocess_exportdata.php?date_range='+date_range+'&param='+filter_param+'&status='+selstatus+'&team='+team;
	}
	

submit_button_clicked = '';
function refundapproval(student_id,val)
{
	var comment = document.getElementById('comment_'+student_id).value;
	var status = val;
	
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Refund Request processed Successfully.");
				submit_button_clicked = '';
				$('#approverefund_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Refund Request process Failed. Contact Administrator");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Refund Request process Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_accountsref_status.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('student_id='+student_id+'&status='+status+'&comment='+comment);
	
}	

</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>