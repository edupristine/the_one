<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add Transfers
</h3>
</div>
</div>
<?php
$query_trans = "SELECT max(transfer_no) as last_transfer,count(id) as count from transfers";
$result_trans = Select($query_trans,$conn);
$max_transfer = $result_trans['rows'][0]['last_transfer'];
if($max_transfer == "" || $max_transfer == NULL )
{
	$max_transfer = 1;
}
else
{
	$max_transfer = $max_transfer + 1;
}
?>
<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
	<label>Transfer Number</label>
	<input type="text" class="form-control" id="transfer_no" name="transfer_no" value="<?php echo $max_transfer; ?>" readonly>
</div>
<?php
$query_loc = "SELECT id,loc_name from locations where loc_type='CENTER'";
$result_loc = Select($query_loc,$conn);

?>
<div class="col-lg-4">
	<label>Transfer To</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="transfer_to">
		<option value="0">Select Center Location</option>
		<?php
			foreach($result_loc['rows'] as $loc)
			{
				echo '<option value="'.$loc['id'].'">'.$loc['loc_name'].'</option>';
			}
		?>
	</select>
</div>

<div class="col-lg-4">
	<label class="">Transfer Date</label>
	<input type="date" class="form-control" id="transfer_date"  name="transfer_date" value = "<?php echo date('Y-m-d'); ?>">	
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
	<label class="">Transfer Courier</label>
    <input type="text" class="form-control" id="transfer_courier" name="transfer_courier" >
</div>
<div class="col-lg-4">
	<label>Courier Track No.</label>
	<input type="text" class="form-control" id="courier_track_no" name="courier_track_no" value="">
</div>
<div class="col-lg-4">
	<label>Courier Charges</label>
	<input type="text" class="form-control" id="transfer_charges" name="transfer_charges" value="">
</div>

<input type="hidden" class="form-control" id="transfer_status" name="transfer_status" value="INITIATED" readonly>
</div>
<?php
$query_skus = "select id,sku_name,sku_code from skus";
$result_skus = Select($query_skus,$conn);
?>

	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:10%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:15%;">Quantity</th>
				<th style="width:10%;">Action</th>
			</tr>
		</thead>
		<tbody id="rows_div">
			<tr id="row_id_1">
				<td class="text-center"><input readonly name="sr_1" id="sr_1" style="width:100%;padding:2px;" type="text" value="1"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_1');" id="skuname_1" name="skuname_1" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="qty_1" id="qty_1" placeholder="Qty" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td id="prodel_1" name="prodel_1" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_1');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<a onClick="SaveTransfer();" class="btn btn-success">Save</a>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>