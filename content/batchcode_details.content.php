<?php
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date('Y-m-d');
    $edate = date('Y-m-d');
    $date_range = $sdate." / ".$edate;
}
$query2 = "SELECT * FROM workshops where start_date >= '".$sdate."' AND start_date <='".$edate."' ORDER BY code ASC";
$query3 = "SELECT DISTINCT(location) FROM batch_threshold";
$query4 = "SELECT DISTINCT(course) FROM batch_threshold";
$query5 = "SELECT DISTINCT(category) FROM batch_threshold";
$result2 = mysqli_query($conn2,$query2);
$result3 = mysqli_query($connone,$query3);
$result4 = mysqli_query($connone,$query4);
$result5 = mysqli_query($connone,$query5);
while($r2 = mysqli_fetch_assoc($result2)) {
    $batch_codes[] = $r2;
}
while($r3 = mysqli_fetch_assoc($result3)) {
    $locations[] = $r3;
}
while($r4 = mysqli_fetch_assoc($result4)) {
    $versions[] = $r4;
}
while($r5 = mysqli_fetch_assoc($result5)) {
    $courses[] = $r5;
}
?>
<script>
	function get_batches()
	{
		var date_range = document.getElementById('date_range').value;
		window.location = 'batchcode_details.php?date_range='+date_range;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_batches();" type="button" class="btn btn-primary form-control">Get Batch Details</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Batch Details
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th rowspan=2>Batchcode</th>
                                                <th rowspan=2>Batch Start Date</th>
												<th rowspan=2>Course</th>
                                                <th rowspan=2>New Course</th>
												<th rowspan=2>Version</th>
                                                <th rowspan=2>New Version</th>
												<th rowspan=2>Location</th>
                                                <th rowspan=2>New Location</th>
                                                <th rowspan=2>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
											foreach($batch_codes as $batch){
                                                $batch_location = $batch['location'];
                                                $batch_course = $batch['course_version'];
                                                $batch_category = $batch['course'];
												?>
                                                
												<tr>
													<td style="text-align: center;vertical-align: middle;"><?php echo $batch['code']; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $batch['start_date']; ?></td>
													<td style="text-align: center;vertical-align: middle;"><?php echo $batch['course'] ?></td>
                                                    <td>
                                                    <select class="form-control bootstrap-select" id="<?php echo $batch['code']."_c";?>">
                                                    <option value="<?php echo $batch['course'] ?>">Select Course</option>
                                                        <?php
                                                        foreach($courses as $course){
                                                            ?>
                                                        <option value="<?php echo $course['category']; ?>"><?php echo $course['category']; ?></option>
                                                    <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    </td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $batch['course_version']; ?></td>
                                                    <td>
                                                    <select class="form-control bootstrap-select" id="<?php echo $batch['code']."_v";?>">
                                                        <option value="<?php echo $batch['course_version']; ?>">Select Version</option>
                                                        <?php
                                                        foreach($versions as $version){
                                                            ?>
                                                        <option value="<?php echo $version['course']; ?>"><?php echo $version['course']; ?></option>
                                                    <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    </td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $batch['location']; ?></td>
                                                    <td>
                                                    <select class="form-control bootstrap-select" id="<?php echo $batch['code']."_l";?>">
                                                    <option value="<?php echo $batch['location']; ?>">Select Location</option>
                                                        <?php
                                                        foreach($locations as $location){
                                                            ?>
                                                        <option value="<?php echo $location['location']; ?>"><?php echo $location['location']; ?></option>
                                                    <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    </td>
                                                    <td><button type="button" class="btn btn-brand btn-icon-xsm" onclick="updateBatch('<?php echo $batch['code'];?>')"><i class="flaticon2-plus"></i>Update</button></td>
												</tr>
										<?php	
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>

                            <script>
                            function updateBatch(batch){
                                var location = document.getElementById(batch+"_l").value;
                                var version = document.getElementById(batch+"_v").value;
                                var course = document.getElementById(batch+"_c").value;
                                var preparedData = {'batch':batch,'location':location,'version':version,'course':course};
                                $.ajax({
                                        url:"http://one.edupristine.com/updateBatchDetails.php",
                                        type:"POST",
                                        data:preparedData,
                                        success:function(e){
                                            alert(e);
                                            window.location.reload();
                                        },
                                        error:function(er){
                                            console.log(er);
                                        }
                                    });
                            }
                            </script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
</div>