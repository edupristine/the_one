<script>
	function ConfirmReceived()
	{
		var action = confirm("Are you sure, you have received this Transfer? \n  Pressing OK will mark it as Received \n  Pressing Cancel will NOT mark it as Received");
		return action;
	}
</script>
<?php
	
	
	if($_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$query_alltransfers = "SELECT t.id,transfer_no,transfer_date,L.loc_name,transfer_status,transfer_courier,courier_track_no,transfer_charges FROM `transfers` t, locations L WHERE t.to_loc_id = L.id and to_loc_id = '".$_SESSION['U_LOCATION_ID']."' ORDER BY transfer_no desc";
		$receivedflg = true;
		$editflg = false;
	}
	else
	{
		$query_alltransfers = "SELECT t.id,transfer_no,transfer_date,L.loc_name,transfer_status,transfer_courier,courier_track_no,transfer_charges FROM `transfers` t, locations L WHERE t.to_loc_id = L.id ORDER BY transfer_no desc";
		$receivedflg = false;
		$editflg = true;
	}
	$result_alltransfers = Select($query_alltransfers,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<?php
	if(isset($_GET['tfrrecparammiss']))
	{
		$msg = "Transfer Updation Failed. Missing Fields";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['tfrupdfailed']))
	{
		$msg = "Transfer Updation Failed. Missing Fields";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['tfrstatusupd']))
	{
		$msg = "Transfer marked as Received.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Transfers List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
												if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
												{
												?>
												&nbsp;
												<a href="transfer_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Intiate New Transfer
												</a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
											
												<th>Transfer No.</th>
												<th>Transfer Date</th>
												<th>To Location</th>
												<th>Transfer Status</th>
												<th>Transfer Courier </th>
												<th>Courier Track No.</th>
												<th colspan=3><center>Actions</center></th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_alltransfers['rows'] as $transfer) 
											{	
												
												?>
												<tr>
													
													<td><?php echo $transfer['transfer_no']; ?></td>
													<td><?php echo $transfer['transfer_date']; ?></td>
													<td><?php echo $transfer['loc_name']; ?></td>
													<td><?php echo $transfer['transfer_status']; ?></td>
													<td><?php echo $transfer['transfer_courier']; ?></td>
													<td><?php echo $transfer['courier_track_no']; ?></td>
													<?php
													if(($_SESSION['U_LOCATION_TYPE'] == "CENTER")||($_SESSION['U_LOCATION_TYPE'] != "CENTER"))
	                                                 {
													?>
													<td style="text-align:center;vertical-align:middle;">
														
														<div class="btn-group" role="group" aria-label="First group">
														
															<?php 
															if($transfer['transfer_status'] == "INITIATED" && $editflg)
															{
															?>
															<a href="transfer_edit.php?id=<?php echo $transfer['id']; ?>"  class="kt-nav__link-icon flaticon2-writing"></a>
															<?php } 
															if($transfer['transfer_status'] == "INITIATED" && $receivedflg)
															{
															?>
															<a onclick="return ConfirmReceived();" href="ajax/transfer_received.php?id=<?php echo $transfer['id']; ?>" class="btn-sm btn-success btn-elevate btn-pill"><i class="la la-angle-double-left"></i>Received</a>
															<?php } ?>
														
														</div>
														
													</td>
													<td style="text-align:center;vertical-align:middle;"><a href="#" class="kt-nav__link-icon flaticon-eye" data-toggle="modal" data-target="#transferview_<?php echo $transfer['id']; ?>" ></a></td>
													<?php
													 }
													 ?>
													
													<?php
													if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
	                                                 {
													?>
													<td style="text-align:center;vertical-align:middle;"><a href="#" class="kt-nav__link-icon flaticon2-delivery-truck"  data-toggle="modal" data-target="#transfercourdet_<?php echo $transfer['id']; ?>" ></a></td>	
												    <?php
													 }
													 ?>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
							<?php 

							foreach($result_alltransfers['rows'] as $transfer) 
							{	

							?>
							<div class="modal fade" id="transferview_<?php echo $transfer['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel"><?php echo "Transfered SKU's List"; ?></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<div class="kt-scroll" data-scroll="true" data-height="230">
										<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
										<tr>
										<th style="width:10%;">#</th>
										<th style="width:35%;">SKU Name</th>
										<th style="width:15%;"><center>Quantity</center></th>
										
										</tr>
										</thead>
										<tbody id="rows_div">
										<?php 
										$query_skus = "Select s.sku_name,ts.sku_id,ts.trans_qty from transfers_skus ts, skus s where trans_id = ".$transfer['id']." AND s.id = ts.sku_id";
										$result_skus_selected = Select($query_skus,$conn);
										$i=0;
										foreach($result_skus_selected['rows'] as $sku_selected)
										{
										$i++;?>
										<tr id="row_id_<?php echo $i; ?>">
										<td class="text-center"><?php echo $i; ?></td>
										<td  class="text-left">
										<?php
										echo $sku_selected['sku_name'];
										?>
										</td>
										<td class="text-center"><?php echo $sku_selected['trans_qty']; ?></td>
										</tr>
										<?php } ?>
										</tbody>
										</table>
										</div>
									</div>
					
								</div>
							</div>
						</div>
						
						<?php	
					
					}
				?>
				
				<?php 

							foreach($result_alltransfers['rows'] as $transfer) 
							{	

							?>
							<div class="modal fade" id="transfercourdet_<?php echo $transfer['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel"><?php echo "Transfered SKU's Courier Details"; ?></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<div class="kt-scroll" data-scroll="true" data-height="230">
											<div class="form-group row">
											<div class="col-lg-12">
											<label class="">Transfer Courier</label>
											<input type="text" class="form-control" id="transfer_courier_<?php echo $transfer['id']; ?>" name="transfer_courier_<?php echo $transfer['id']; ?>" value="<?php echo $transfer['transfer_courier']; ?>" >
											</div>
											<br>
											<div class="col-lg-12">
											<label>Courier Track No.</label>
											<input type="text" class="form-control" id="courier_track_n_<?php echo $transfer['id']; ?>" name="courier_track_n_<?php echo $transfer['id']; ?>" value="<?php echo $transfer['courier_track_no']; ?>" >
											</div>
											<br>
											<div class="col-lg-12">
											<label>Courier Charges</label>
											<input type="text" class="form-control" id="transfer_charges_<?php echo $transfer['id']; ?>" name="transfer_charges_<?php echo $transfer['id']; ?>" value="<?php echo $transfer['transfer_charges']; ?>" >
											</div>

											
											</div>
										</div>
									</div>
									<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									<button onClick="Updatecourierdetails(<?php echo $transfer['id'];?>);" type="button" class="btn btn-primary">Update</button>
									</div>
								</div>
							</div>
						</div>
						
						<?php	
					
					}
				?>
				
										
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}



function Updatecourierdetails(transfer_id)
{
	
	var transfer_courier = document.getElementById('transfer_courier_'+transfer_id).value;
	var courier_track_no = document.getElementById('courier_track_n_'+transfer_id).value;
	var transfer_charges = document.getElementById('transfer_charges_'+transfer_id).value;
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Transfered Courier Details Updated  Successfully.");
				submit_button_clicked = '';
				$('#transfercourdet_'+transfer_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Courier Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Courier Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_trans_courier.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('transfer_id='+fixEscape(transfer_id)+'&courier_track_no='+fixEscape(courier_track_no)+'&transfer_courier='+fixEscape(transfer_courier)+'&transfer_charges='+fixEscape(transfer_charges));
	
}


</script>					