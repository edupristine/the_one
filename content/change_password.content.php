<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['duppass1']))
	{
		$msg = "Password Update Failed. Please Enter a new password.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['duppass2']))
	{
		$msg = "Your Old password did not match. ";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Please enter new password to change the existing one.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Password Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "New Password updated Sucessfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Change Password
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/change_password.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>User Name</label>
	<input type="text" class="form-control" id="user_name" name="user_name" value="<?php echo $_SESSION['U_NAME']; ?>" readonly >
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label class="">Old Password:</label>
	<input type="password" class="form-control" id="old_pass"  name="old_pass">
	
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label class="">New Password:</label>
	<input type="password" class="form-control" id="new_pass"  name="new_pass">
	
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label class="">Re-Type Password:</label>
	<input type="password" class="form-control" id="re_new_pass"  name="re_new_pass" onchange="Validate()";>
</div>
</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success" onclick="Validate()">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>