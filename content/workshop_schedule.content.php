<style>
.loader {
    margin-left:42%;
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid blue;
  border-bottom: 16px solid blue;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite;
  animation: spin 2s linear infinite;
}
#loader {
    display:none;
}
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>
<?php
    $batch = get_get_value('batch');
    $location = get_get_value('location');
    $batch_type = get_get_value('batch_type');

    $temp = get_get_value('template');
    // $faculty_incharge = json_decode(stripslashes(get_get_value('faculty')));
    // $support_person = json_decode(stripslashes(get_get_value('support')));
    
    $topics = array();
    $class_date = array();
    $holidays = array();
	if($batch != "")
	{
        $qryFac = "SELECT be.faculty_emails,be.support_emails FROM batch_email_master be,batches b
        WHERE be.`course_mod_id` = b.`course_mod_id` AND be.`location_id` = b.`loc_id` AND b.batch_name = '".$batch."'";
        $resFac = mysqli_query($connone,$qryFac);
        $facData = mysqli_fetch_assoc($resFac);
        $faculty_incharge = explode(',',$facData['faculty_emails']);
        $support_person = explode(',',$facData['support_emails']);
    }else{
        $faculty_incharge = array();
        $support_person = array();
    }
    $query = "SELECT code FROM workshops WHERE start_date >= '2019-08-01' AND code NOT LIKE '%SEM%'";
    $query2 = "SELECT param_value FROM `help` where param_type = 'batch_type'";
    $query3 = "SELECT DISTINCT(template_name) FROM schedule_templates";
    $query4 = "SELECT loc_name FROM locations WHERE loc_type = 'CENTER'";
    $query5 = "SELECT us.* FROM users us
                LEFT JOIN users_teams ut ON ut.user_id = us.id
                WHERE ut.team_id = 2";
    $query6 = "SELECT us.* FROM users us
                LEFT JOIN users_teams ut ON ut.user_id = us.id
                WHERE ut.team_id = 1";
    $query7 = "SELECT param_value FROM help WHERE param_type = 'push_reason' ORDER BY param_imp ASC";
    $result = mysqli_query($conn2,$query);
    $result2 = mysqli_query($connone,$query2);
    $result3 = mysqli_query($connone,$query3);
    $result4 = mysqli_query($connone,$query4);
    $result5 = mysqli_query($connone,$query5);
    $result6 = mysqli_query($connone,$query6);
    $result7 = mysqli_query($connone,$query7);
    while($r1 = mysqli_fetch_assoc($result)) {
        $batchCodes[] = $r1;
    }
    while($r2 = mysqli_fetch_assoc($result2)) {
        $batchTypes[] = $r2;
    }
    while($r3 = mysqli_fetch_assoc($result3)) {
        $batch_template[] = $r3;
    }
    while($r4 = mysqli_fetch_assoc($result4)) {
        $locations[] = $r4;
    }
    while($r5 = mysqli_fetch_assoc($result5)) {
        $faculty[] = $r5;
    }
    while($r6 = mysqli_fetch_assoc($result6)) {
        $support[] = $r6;
    }
    while($r7 = mysqli_fetch_assoc($result7)) {
        $push_reason[] = $r7;
    }
?>
<script>
	function get_schedule()
	{
		var batch = document.getElementById('batch_code').value;
        var loc = document.getElementById('location').value;
        var batch_type = document.getElementById('batchType').value;
        var template = document.getElementById('template').value;
        // var faculty_tmp = $("#faculty").val();
        // var faculty = JSON.stringify(faculty_tmp);
        // var support_tmp = $("#support").val();
        // var support = JSON.stringify(support_tmp);
		window.location = 'workshop_schedule.php?batch='+batch+'&location='+loc+'&batch_type='+batch_type+'&template='+template;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label><b>Update Workshop Schedule</b></label>
</div>
</div>
<div class="form-group row">
				<div class="col-lg-3">
					<select class="form-control select-batchcode" id="batch_code" name="param" tabindex="-1" aria-hidden="true">
					    <option value="">Select Batchcode</option>
                        <?php foreach($batchCodes as $code){ ?>
                        <option value="<?php echo $code['code']; ?>"><?php echo $code['code']; ?></option>
                        <?php } ?>
					</select>
				</div>
                <div class="col-lg-3">
					<select class="form-control select-location" id="location" name="param" tabindex="-1" aria-hidden="true">
					    <option value="">Select Location</option>
                        <?php foreach($locations as $center){ ?>
                        <option value="<?php echo $center['loc_name']; ?>"><?php echo $center['loc_name']; ?></option>
                        <?php } ?>
					</select>
				</div>
                <div class="col-lg-3">
					<select class="form-control select-batchtype" id="batchType"  name="param" tabindex="-1" aria-hidden="true">
					    <option value="">Select Batchtype</option>
                        <?php foreach($batchTypes as $type){ ?>
                        <option value="<?php echo $type['param_value']; ?>"><?php echo $type['param_value']; ?></option>
                        <?php } ?>
					</select>
				</div>
                <div class="col-lg-3">
					<select class="form-control select-template" id="template" name="param" tabindex="-1" aria-hidden="true">
					    <option value="">Select Template</option>
                        <?php foreach($batch_template as $template){ ?>
                        <option value="<?php echo $template['template_name']; ?>"><?php echo $template['template_name']; ?></option>
                        <?php } ?>
					</select>
				</div>
			</div>
        <div class="form-group row">
                <div class="col-lg-3">
                    <select class="form-control select-faculty" id="faculty" name="param" tabindex="-1" aria-hidden="true" multiple>
                        <option value="">Select Faculty Incharge</option>
                        <?php foreach($faculty as $fac){ ?>
                        <option <?php if(in_array($fac['user_email'],$faculty_incharge)){echo "selected";} ?> value="<?php echo $fac['user_email']; ?>"><?php echo $fac['user_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                    <select class="form-control select-support" id="support" name="param" tabindex="-1" aria-hidden="true" multiple>
                        <option value="">Select Support Team</option>
                        <?php foreach($support as $sup){ ?>
                        <option <?php if(in_array($sup['user_email'],$support_person)){echo "selected";} ?>  value="<?php echo $sup['user_email']; ?>"><?php echo $sup['user_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-2" id="review_schedule">
                    <button onClick="get_schedule()" type="button" class="btn btn-primary form-control">Review</button>
                </div>
                <?php
            if($batch != ''){ ?>
                <div class="col-lg-2" id="push_to_harry">
                <button onClick="update_schedule()" type="button" class="btn btn-primary form-control" id="push_to_harry_btn">Push To Harry</button>
                </div>
            <?php } ?>
        </div>
        <div class="form-group row">
                <div class="col-lg-2">
                    <p><b>Start Date:</b> <span id="startDate"></span></p>
                </div>
        </div>

</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Workshop Details
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="dataTable">
										<thead>
											<tr style="text-align: center;">
												<th rowspan=2>Batchcode</th>
                                                <th rowspan=2>Course</th>
												<th rowspan=2>Topic</th>
                                                <th rowspan=2>Batch Date</th>
											</tr>
										</thead>
										<tbody>
                                        <tr>
                                            <td colspan=6 id="loader"><div class="loader"></div></td>
                                        </tr>
										<?php
                                            if($batch != ''){
                                                $sql = "SELECT start_date,course FROM workshops WHERE code='".$batch."'";
                                                $sql1 = "SELECT COUNT(*) as `no_of_topics` FROM sc_temp_topics WHERE sc_id = 
                                                (SELECT id FROM `schedule_templates` WHERE batch_type = '".$batch_type."' AND template_name ='".$temp."')";
                                                $r = mysqli_query($conn2,$sql);
                                                $r1 = mysqli_query($connone,$sql1);
                                                $batch_details = mysqli_fetch_assoc($r);
                                                $no_of_topics = mysqli_fetch_assoc($r1)['no_of_topics'];
                                                $qry = "SELECT holiday_date FROM holidays_list WHERE 
                                                holiday_date >= '".$batch_details['start_date']."' 
                                                AND loc_id = (SELECT id FROM locations WHERE loc_name = '".$location."')";
                                                $res = mysqli_query($connone,$qry);
                                                while($hl = mysqli_fetch_assoc($res)) {
                                                    $holidays[] = $hl['holiday_date'];
                                                }
                                                
                                                $sql2 = "SELECT topic_name FROM sc_temp_topics WHERE sc_id = 
                                                (SELECT id FROM `schedule_templates` WHERE batch_type = '".$batch_type."' AND template_name ='".$temp."') ORDER BY topic_seq ASC";
                                                
												
												
                                                $r2 = mysqli_query($connone,$sql2);
                                                while($tp = mysqli_fetch_assoc($r2)) {
                                                    $topics[] = $tp['topic_name'];
                                                }
                                                
                                                $class_date = get_class_dates($batch_details['start_date'],$batch_type,$no_of_topics,$holidays);
                                                $cnt = 0;
                                                ?>
                                                <script>
                                                $("#dataTable > tbody").html("");
                                                document.getElementById("startDate").textContent="<?php echo $batch_details['start_date'];?>";
                                                </script>
                                                <?php
                                                foreach($topics as $topic){
												?>
												<tr>
													<td style="text-align: center;vertical-align: middle;"><?php echo $batch; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $batch_details['course']; ?></td>
													<td style="text-align: center;vertical-align: middle;"><?php echo $topic; ?></td>
                                                    <td style="text-align: center;vertical-align: middle;"><?php echo $class_date[$cnt]; ?></td>
												</tr>
										<?php	
                                                $cnt += 1;
                                            }
										?>
                                        </br>
                                        <?php 
                                        }
                                     ?>
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>

                            <script>
                            function update_schedule(){
                                <?php global $topics; global $class_date; ?>
                                $("#push_to_harry_btn").attr('disabled',true);
                                var topics = <?php echo '["'.implode('","',  $topics ).'"]'; ?>;
                                var dates = <?php echo '["'.implode('","',  $class_date ).'"]'; ?>;
                                var batch = $("#batch_code").val();
                                var faculty = $('#faculty').val();
                                var support = $('#support').val();
                                var template = $('#template').val();
                                var batch_type = $('#batchType').val();
                                var location = $("#location").val();
                                if(batch_type == ""){
                                    alert("Enter Batch Type");
                                    return false;
                                }else if(template == ""){
                                    alert("Enter Template");
                                    return false;
                                }
                                var preparedData = {'batch':batch,'topics':topics,'dates':dates,'support':support,'faculty':faculty,'batch_type':batch_type,'batch_template':template,'location':location};
                                
                                $.ajax({
                                        url:"http://one.edupristine.com/updateWorkshopSchedule.php",
                                        type:"POST",
                                        data:preparedData,
                                        success:function(e){
                                            alert(e);
                                            window.location="//one.edupristine.com/workshop_schedule.php";
                                        },
                                        error:function(er){
                                            console.log(er);
                                        }
                                    });
                            }
                            function push(batch,topic,date){
                                $('#reason_modal').modal('show');
                                $('#ws_code').val(batch);
                                $('#ws_topic').val(topic);
                                $('#ws_date').val(date);
                            }
                            function pull(batch,topic,date){
                                $('#pull_reason_modal').modal('show');
                                $('#pws_code').val(batch);
                                $('#pws_topic').val(topic);
                                $('#pws_date').val(date);
                            }
                            function push_submit(){
                                var batch = $('#ws_code').val();
                                var topic = $('#ws_topic').val();
                                var date = $('#ws_date').val();
                                var reason = $('#reason').val();
                                var comment = $('#others').val();
                                if(comment == ""){
                                    alert("Enter a description");
                                }else{
                                var preparedData = {'batch':batch,'topic':topic,'date':date,'reason':reason,'comment':comment};
                                
                                $.ajax({
                                        url:"http://one.edupristine.com/pushWorkshopSchedule.php",
                                        type:"POST",
                                        data:preparedData,
                                        success:function(e){
                                            alert(e);
                                            $('#reason_modal').modal('hide');
                                            window.location.reload();
                                        },
                                        error:function(er){
                                            console.log(er);
                                        }
                                    });
                            }
                            }
                            function pull_submit(){
                                var batch = $('#pws_code').val();
                                var topic = $('#pws_topic').val();
                                var date = $('#pws_date').val();
                                var reason = $('#pull_reason').val();
                                var preparedData = {'batch':batch,'topic':topic,'date':date,'reason':reason};
                                
                                $.ajax({
                                        url:"http://one.edupristine.com/pullWorkshopSchedule.php",
                                        type:"POST",
                                        data:preparedData,
                                        success:function(e){
                                            alert(e);
                                            $('#pull_reason_modal').modal('hide');
                                            window.location.reload();
                                        },
                                        error:function(er){
                                            console.log(er);
                                        }
                                    });
                            }
                            </script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    <?php if($batch != "") {?>
    
    $("#batch_code").val(<?php echo "'".$batch."'"; ?>).trigger('change');
    $("#location").val(<?php echo "'".$location."'"; ?>).trigger('change');
    $("#batchType").val(<?php echo "'".$batch_type."'"; ?>).trigger('change');
    
    $("#template").val(<?php echo "'".$temp."'"; ?>).trigger('change');
    <?php } ?>
    $('#batch_code').change(function() {
    $("#review_schedule").css('display','block');
    $("#push_to_harry").css('display','none');
    var result;
    $('#dataTable').find('tbody').append('<tr><td colspan=6 id="loader"><div class="loader"></div></td></tr>');
    $("#loader").css("display", "table-cell");
    var data = $("#batch_code").val();
    var preparedData = {'batchCode':data};
    $.ajax({
        url:"http://one.edupristine.com/getWorkshopSchedule.php",
        type:"GET",
        data:preparedData,
        success : function(response){
            if(response != ""){
            var result = JSON.parse(response);
            var html = "";
            $("#dataTable > tbody").html("");
            if(result.length > 1){
            for(var i = 0; i < result.length; i++){
                html += '<tr>';
                html += '<td style="text-align: center;vertical-align: middle;">'+result[i]['batch_code']+'</td>';
                html += '<td style="text-align: center;vertical-align: middle;">'+result[i]['course']+'</td>'
                html += '<td style="text-align: center;vertical-align: middle;">'+result[i]['topic']+'</td>'
                html += '<td style="text-align: center;vertical-align: middle;">'+result[i]['estimated_start_date']+'</td>'
                html += '<td style="text-align: center;vertical-align: middle;"><button onClick="push(\''+result[i]['batch_code']+'\',\''+result[i]['topic']+'\',\''+result[i]['estimated_start_date']+'\')" type="button" class="btn-sm btn-danger form-control" style="margin-bottom:1px;">Push</button><button onClick="pull(\''+result[i]['batch_code']+'\',\''+result[i]['topic']+'\',\''+result[i]['estimated_start_date']+'\')" type="button" class="btn-sm btn-primary form-control">Pull</button></td>';
                html += '</tr>';
            }
            $("#review_schedule").css('display','none');
            if(result[0]['loc_name'] == 'Andheri'){
                $("#location").val('Mumbai').trigger('change');    
            }else{
            $("#location").val(result[0]['loc_name']).trigger('change');
            }
            $("#batchType").val(result[0]['param_value']).trigger('change');
            var fvals = result[0]['faculty_emails'].split(",");
            $("#faculty").val(fvals).change();
            var svals = result[0]['support_emails'].split(",");
            $("#support").val(svals).change();
            document.getElementById("startDate").textContent=result[0]['estimated_start_date'];
            $("#template").val(result[0]['batch_template']).trigger('change');
            $('#dataTable').find('tbody').append(html);
        }else{
            if(result[0]['loc_name'] == 'Andheri'){
                $("#location").val('Mumbai').trigger('change');    
            }else{
            $("#location").val(result[0]['loc_name']).trigger('change');
            }
            $("#batchType").val(result[0]['param_value']).trigger('change');
            var fvals = result[0]['faculty_emails'].split(",");
            $("#faculty").val(fvals).change();
            var svals = result[0]['support_emails'].split(",");
            $("#support").val(svals).change();
            document.getElementById("startDate").textContent=result[0]['estimated_start_date'];
            $("#template").val("").trigger('change');
            $("#dataTable > tbody").html("");
            $("#loader").css("display", "none");
        }
            }else{
                $("#dataTable > tbody").html("");
                $("#loader").css("display", "none");
            }
        },
        error: function(er) {
            alert(er);
        }
    });
    });
    $('.select-batchcode').select2();
    $('.select-template').select2();
    $('.select-batchtype').select2();
    $('.select-location').select2();
    $('.select-faculty').select2({placeholder: 'Select Faculty Incharge'});
    $('.select-support').select2({placeholder: 'Select Support Team'});

});

</script>
</div>
<div class="modal fade show" id="reason_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reason For Pushing Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                <input type="hidden" id="ws_code">
                <input type="hidden" id="ws_date">
                <input type="hidden" id="ws_topic">
                    <div class="form-group">
                        <label for="recipient-name" class="form-control-label">Reason:</label>
                        <select class="form-control" id="reason">
                            <?php foreach($push_reason as $reason){ ?>
                            <option value="<?php echo $reason['param_value'];?>"><?php echo $reason['param_value'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Description:</label>
                        <input type="text" class="form-control" id="others">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="push_submit()">Submit</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade show" id="pull_reason_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reason For Pulling Schedule</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <form>
                <input type="hidden" id="pws_code">
                <input type="hidden" id="pws_date">
                <input type="hidden" id="pws_topic">
                    <div class="form-group">
                        <label for="message-text" class="form-control-label">Reason:</label>
                        <input type="text" class="form-control" id="pull_reason">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="pull_submit()">Submit</button>
            </div>
        </div>
    </div>
</div>
<?php
function get_class_dates($start_date,$type,$no_of_topics,$holidays){
    global $topics;
    $cnt =0;
    $tmp = 'No Class';
    $class_dates = array();
    $date = $start_date;
    if($type == 'Weekdays'){
        while($cnt < $no_of_topics){
        $day = date('D', strtotime($date));
        if($day != 'Sun' && $day != 'Sat'){
            array_push($class_dates,$date);
            if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
        }
        $date = date('Y-m-d', strtotime($date . ' +1 day'));
    }
    }else if($type == 'Weekend'){
        while($cnt < $no_of_topics){
            $day = date('D', strtotime($date));
            if(($day == 'Sun' || $day == 'Sat')){
                array_push($class_dates,$date);
                if((in_array($date,$holidays))){
                    array_splice($topics, $cnt, 0, $tmp);
                    $no_of_topics +=1;
                }
                $cnt +=1;
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
    }else if($type == 'Saturday Only'){
        while($cnt < $no_of_topics){
            $day = date('D', strtotime($date));
            if($day == 'Sat'){
                array_push($class_dates,$date);
                if((in_array($date,$holidays))){
                    array_splice($topics, $cnt, 0, $tmp);
                    $no_of_topics +=1;
                }
                $cnt +=1;
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
    }else if($type == 'Sunday Only'){
        while($cnt < $no_of_topics){
            $day = date('D', strtotime($date));
            if($day == 'Sun'){
                array_push($class_dates,$date);
                if((in_array($date,$holidays))){
                    array_splice($topics, $cnt, 0, $tmp);
                    $no_of_topics +=1;
                }
                $cnt +=1;
            }
            $date = date('Y-m-d', strtotime($date . ' +1 day'));
        }
    }else if($type == 'M-W-F'){
        while($cnt < $no_of_topics){
        $day = date('D', strtotime($date));
        if($day != 'Sun' && $day != 'Sat' && $day != 'Tue' && $day != 'Thu'){
            array_push($class_dates,$date);
            if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
        }
        $date = date('Y-m-d', strtotime($date . ' +1 day'));
    }
    }
	else if($type == 'Alt Sat and All Sun'){
        while($cnt < $no_of_topics){
		$day = date('D', strtotime($date));
		$satday = date('d', strtotime($date));
		
		$month = date('M', strtotime($date));
		$year = date('Y', strtotime($date));
		$secsat= date('d', strtotime(''.$month.' '.$year.' second saturday')); 
		$forthsat= date('d', strtotime(''.$month.' '.$year.' fourth saturday')); 
		$leapsecsat= date('d', strtotime(''.$month.' '.$year.' first saturday')); 
		$leapforthsat= date('d', strtotime(''.$month.' '.$year.' third saturday')); 

		 if(($day == 'Sat')){
			
			 //Leap Year Bug Handled 2 Sat was considered 3 Sat here
			if(($month=="Feb")&&($year=="2020"))
			{
			if($satday==$leapsecsat)
			{
            array_push($class_dates,$date);
			if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
			}
			
			if($satday==$leapforthsat)
			{
            array_push($class_dates,$date);
			if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
			}	
			}
			else
			{
			if($satday==$secsat)
			{
            array_push($class_dates,$date);
			if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
			}
			
			if($satday==$forthsat)
			{
            array_push($class_dates,$date);
			if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
			}
			}
			
            
        }
		
        if(($day == 'Sun')){
            array_push($class_dates,$date);
            if((in_array($date,$holidays))){
                array_splice($topics, $cnt, 0, $tmp);
                $no_of_topics +=1;
            }
            $cnt +=1;
        }

        $date = date('Y-m-d', strtotime($date . ' +1 day'));
		
    }
    }
	
	return $class_dates;

}
?>