<?php

setlocale(LC_MONETARY, 'en_IN');
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date("Y-m-d",strtotime("-1 days"));
    $edate = date("Y-m-d",strtotime("-1 days"));
    $date_range = $sdate." / ".$edate;
}

$startdate=$sdate;
$enddate=$edate;

	
?>
<script>
	function get_export()
	{
		var date_range = document.getElementById('date_range').value;
		window.location = 'content/monthlytransaction_exportdata.php?date_range='+date_range;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
										
										<h3 class="kt-portlet__head-title">
											<i class="kt-font-brand flaticon2-line-chart"></i> Monthly Transaction Report (Download)
										</h3>
									</div>
</div>									
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
</div>

<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_export();" type="button" class="btn btn-primary form-control">Export</button>
</div>

</div>
</div>
</div>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>