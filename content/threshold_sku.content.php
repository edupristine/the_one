
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$location_id = $_SESSION['U_LOCATION_ID'];
$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
$result_loc_type = Select($location_type,$conn);
$location_type_report = $result_loc_type['rows'][0]['loc_type'];
$location_name_report = $result_loc_type['rows'][0]['loc_name'];

if(isset($_GET['dberror']))
	{
		$msg = "Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Sku's Discarded Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Threshold Assignment for Books
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='form_handlers/threshold_sku.php'>
<div class="kt-portlet__body">
<div class="form-group">

<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Date</label>
		<td class="text-center"><input data-toggle="tooltip" name="update_date" id="update_date"  class="form-control" type="date" value = "<?php echo date('Y-m-d'); ?>"  />
	
</div>
</div>
</div>
		
	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:2%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:10%;"><center>Current Threshold</center></th>
				<th style="width:15%;">New Threshold Value</th>
			</tr>
		</thead>
		
		
		
		<tbody id="rows_div">
		<?php 
			$query_skus = "SELECT sl.id,s.sku_name,l.loc_name,(sl.sku_pre_qty+sl.sku_qty_avl) as sku_available,sl.sku_qty_avl as sku_og,sl.sku_pre_qty as sku_pre_og,sl.sku_id,sl.loc_id,sl.sku_qty_min as sku_min FROM `skus_locations` sl,`skus` s, `locations` l WHERE sl.sku_id=s.id and l.id=1
and sl.loc_id=l.id  order by l.id,s.sku_name";
			$result_skus = Select($query_skus,$conn);
			$i=0;
			foreach($result_skus['rows'] as $sku)
			{
				$i++;?>
			<tr id="row_id_<?php echo $i; ?>">
				<td class="text-left"><input class="form-control" name="skl_<?php echo $sku['id']; ?>" id="skl_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['id']; ?>"/><?php echo $i; ?></td>
				<td class="text-left"><input class="form-control" name="sku_<?php echo $sku['id']; ?>" id="sku_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['sku_id']; ?>"/><?php echo $sku['sku_name']; ?></td>
				<td class="text-center"><input class="form-control" name="threshold_pre_<?php echo $sku['id']; ?>" id="threshold_pre_<?php echo $sku['id']; ?>"  type="text" value="<?php echo $sku['sku_min']; ?>" hidden />
				<?php echo $sku['sku_min']; ?></td>
				<td class="text-center"><input class="form-control" name="new_threshold_<?php echo $sku['id']; ?>" id="new_threshold_<?php echo $sku['id']; ?>"  type="text" value=""  /></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<!--end: Datatable -->
</div>	
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="submit" name="submit" type="submit" class="btn btn-success">Update</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>