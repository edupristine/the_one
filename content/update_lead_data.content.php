<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$display = "block";
// Get status message
if(!empty($_GET['status'])){
    switch($_GET['status']){
        case 'succ':
            $statusType = 'alert-success';
            $statusMsg = 'Lead data has been imported successfully.';
            $altype = "success";
            break;
        case 'err':
            $statusType = 'alert-danger';
            $statusMsg = 'Some problem occurred, please try again.';
            $altype = "danger";
            break;
        case 'invalid_file':
            $statusType = 'alert-danger';
            $statusMsg = 'Please upload a valid CSV file.';
            $altype = "danger";
            break;
        default:
            $statusType = '';
            $statusMsg = '';
    }
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $statusType; ?>"></i></div>
		<div class="alert-text"><?php echo $statusMsg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update Leads In SalesForce
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" id="batch" name="batch" method='post' enctype="multipart/form-data" action='form_handlers/csv_to_db_update.php'>
<div class="kt-portlet__body">
<div class="form-group row">

<div class = "col-lg-3">
<label class="">Select Update Lead File(CSV):</label>
<input type="file" name="file" />
</div>

</div>
</div>



<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-3" style="display:<?php echo $display; ?>;">
		    <button type="submit" id="submit" name="importSubmit" class="btn btn-success" onClick="this.form.submit(); this.disabled=true; this.value='Sending…';">Import</button>
		</div>
	</div>
</div>
</div>
</form>
<div class="kt-portlet__body">

                        <!--begin: Datatable -->
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="">
                            <thead>
                                <tr style="text-align: center;">
                                    <th rowspan=2>External ID</th>
                                    <th rowspan=2>Update Value</th>
                                    <th rowspan=2>Update Field</th>
                                </tr>
                            </thead>
                            <tbody>
                    <?php
                        $mysql_host='52.77.5.117';
                        $mysql_user='liveuser';/*liveuser*/
                        $mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
                        $conn = mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
                    
                        $result = mysqli_query($conn,"SELECT * FROM sf_lead_update WHERE lead_updated = 0 ORDER BY id ASC LIMIT 100");
                        $num_rows = mysqli_num_rows($result);
                        if($num_rows > 0){
                        while($r = mysqli_fetch_assoc($result)) {
                            $rows[] = $r;
                        }
                        foreach($rows as $row){
                        ?>
                            <tr>
                                <td><?php echo $row['external_id']; ?></td>
                                <td><?php echo $row['update_value']; ?></td>
                                <td><?php echo $row['update_field']; ?></td>
                            </tr>
                        <?php } ?>
                        <div class="col-lg-3">
                        <button type="button" id="update_leads" name="update_lead" class="btn btn-success">Update Leads</button>
                        </div>
                        </br>
                        <?php
                        }?>
                        </tbody>
                    </table>
</div>
</div>
</div>

</div>

<?php
?>
<script>
$('#update_leads').click(function() {
	//alert("Push Triggered.");
    $(this).attr('disabled');
  $.ajax({
    type: "POST",
    url: "form_handlers/update_lead.php",
    data: { command : "push"  }
  }).done(function( msg ) {
    alert( "Message : " + msg );
    window.location.href = "http://one.edupristine.com/update_lead_data.php";
  });
});

</script>