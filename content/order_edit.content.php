<?php
if (!isset($_GET['id']) || empty($_GET['id']))
{
	header('location:order_list.php');
	exit();
}

$order_id = $_GET['id'];
$query_skus_selected = "SELECT * FROM `orders_skus` WHERE `ord_id`='". $order_id . "'";
$result_skus_selected = Select($query_skus_selected, $conn);
$num_of_skus = $result_skus_selected['count'];

$query_order = "SELECT * FROM orders WHERE `id`='" . $order_id . "'";
$result_order = Select($query_order, $conn);
if ($result_order['count'] == '0')
{
	header('location:order_list.php');
	exit();
}
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Edit Order
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">
<div class="col-lg-2" hidden>
	<label>Order ID</label>
	<input type="text" class="form-control" id="ord_id" name="ord_id" value="<?php echo $result_order['rows'][0]['id']; ?>" readonly>
</div>
<div class="form-group row">
<div class="col-lg-2">
	<label>Order Number</label>
	<input type="text" class="form-control" id="ord_num" name="ord_num" value="<?php echo $result_order['rows'][0]['ord_num']; ?>" readonly>
</div>
<div class="col-lg-2">
	<label>Ref. Number</label>
	<input type="text" class="form-control" id="ref_num" name="ref_num" value="<?php echo $result_order['rows'][0]['pi_num']; ?>">
</div>
<div class="col-lg-3">
	<label class="">Order Date</label>
	<input type="date" class="form-control" id="ord_date"  name="ord_date" value="<?php echo $result_order['rows'][0]['ord_date']; ?>">	
</div>
<div class="col-lg-3">
	<label class="">Exp. Rec. Date</label>
	<input type="date" class="form-control" id="exp_rec_date"  name="exp_rec_date" value="<?php echo $result_order['rows'][0]['exp_rec_date']; ?>">	
</div>
<div class="col-lg-2">
	<label>Order Amount</label>
	<input type="text" class="form-control" id="ord_amount" name="ord_amount" value="<?php echo $result_order['rows'][0]['ord_amt']; ?>">
</div>
</div>
<?php
$query_vendor = "SELECT id,vendor_name from vendors";
$result_vendor = Select($query_vendor,$conn);

$query_skus = "select id,sku_name,sku_code from skus";
$result_skus = Select($query_skus,$conn);
?>
<div class="form-group row">
<div class="col-lg-3">
	<label>Vendor</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="vendor_name">
		<option value="0">Select Vendor</option>
		<?php
			foreach($result_vendor['rows'] as $vendor)
			{
				if($result_order['rows'][0]['vendor_id'] == $vendor['id'])
				{
					echo '<option value="'.$vendor['id'].'" selected>'.$vendor['vendor_name'].'</option>';
				}
				else
				{
					echo '<option value="'.$vendor['id'].'">'.$vendor['vendor_name'].'</option>';
				}
			}
		?>
	</select>
</div>
<div class="col-lg-9">
	<label>Comments</label>
	<input type="text" class="form-control" id="comments" name="comments" value="<?php echo $result_order['rows'][0]['comments']; ?>">
</div>

</div>
	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:10%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:15%;">Quantity</th>
				<th style="width:15%;">Rate</th>
				<th style="width:15%;">Amount</th>
				<th style="width:10%;">Action</th>
			</tr>
		</thead>
		
		<tbody hidden id="samplerow_div">
			<tr id="row_id_0">	
				<td class="text-center"><input readonly name="sr_0" id="sr_0" style="width:100%;padding:2px;" type="text" value="1"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_0');" id="skuname_0" name="skuname_0" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="RefreshSKURow('row_id_0','qty');" name="qty_0" id="qty_0" placeholder="Qty" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_0','rate');" name="rate_0" id="rate_0" placeholder="Rate" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_0','amt');" name="amt_0" id="amt_0" placeholder="Amt" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td id="prodel_0" name="prodel_0" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_0');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
		
		
		<tbody id="rows_div">
		<?php 
			$query_skus = "Select s.sku_name,os.sku_id,os.qty,os.rate,os.amount from orders_skus os, skus s where ord_id = ".$order_id." AND s.id = os.sku_id";
			$result_skus_selected = Select($query_skus,$conn);
			$i=0;
			foreach($result_skus_selected['rows'] as $sku_selected)
			{
				$i++;?>
			<tr id="row_id_<?php echo $i; ?>">
				<td class="text-center"><input readonly name="sr_<?php echo $i; ?>" id="sr_<?php echo $i; ?>" style="width:100%;padding:2px;" type="text" value="<?php echo $i; ?>"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_<?php echo $i; ?>');" id="skuname_<?php echo $i; ?>" name="skuname_<?php echo $i; ?>" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							if($sku_selected['sku_id'] == $sku['id'])
								echo "<option value='".$sku['id']."' selected>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
							else
								echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="RefreshSKURow('row_id_<?php echo $i; ?>','qty');" name="qty_<?php echo $i; ?>" id="qty_<?php echo $i; ?>" placeholder="Qty" style="width:100%;padding:2px;" type="text" value="<?php echo $sku_selected['qty']; ?>"/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_<?php echo $i; ?>','rate');" name="rate_<?php echo $i; ?>" id="rate_<?php echo $i; ?>" placeholder="Rate" style="width:100%;padding:2px;" type="text" value="<?php echo $sku_selected['rate']; ?>"/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_<?php echo $i; ?>','amt');" name="amt_<?php echo $i; ?>" id="amt_<?php echo $i; ?>" placeholder="Amt" style="width:100%;padding:2px;" type="text" value="<?php echo $sku_selected['amount']; ?>"/></td>
				<td id="prodel_<?php echo $i; ?>" name="prodel_<?php echo $i; ?>" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_<?php echo $i; ?>');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<a onClick="SaveOrder();" class="btn btn-success">Save</a>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>