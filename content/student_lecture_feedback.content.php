<!-- end:: Subheader -->
<!-- begin:: Content -->
<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'ecademe_harry_new');
$conn=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');



$id  = $_GET['id'];
$unique_id = $id;
//Open at the Last before making Live
$unique_id = base64_decode($id);
$sql = "SELECT * FROM lectures_feedback where unique_id='".$unique_id."'";
$result = mysqli_query($conn,$sql);
$stu_details = mysqli_fetch_assoc($result);
$main_id=$stu_details['id'];
$account_no=$stu_details['organization_no'];
$workshop_id=$stu_details['date_id'];
$feedback_given=$stu_details['feedback_given'];
$url=$stu_details['link'];
$ws_id=$stu_details['workshop_id'];
$ws_date_id=$stu_details['date_id'];
$faculty_id=$stu_details['faculty_id'];

if($feedback_given==0)
{

$output="";			

$sel_workshop = "SELECT
					wd.id, 
					wd.batch_code as `batch_code`,
					wd.ws_date,
					wd.venue_details as `venue_details`,
					wd.location as `location`, 
					wd.start_time `start_time`, 
					wd.end_time as `end_time`,
					wd.actual_start_time as `actual_start_time`,
					wd.actual_end_time as `actual_end_time`,
					wd.classroom_rent as `classroom_rent`,
					wd.slot as slot,
					wd.student_count_by_admin as student_count_by_admin,
					wd.topic as `topic`, 
					wd.fac_average_rating as `faculty_rating`,
					wd.content_quality_rating,
					wd.infra_rating, 
					wd.cs_rating,
					wd.newFaculty,
					wd.ws_comment as `ws_comment`, 
					wd.`status` as `status`,
					wd.mail_sent_flag as `mail_sent_flag`,
					wd.sms_sent_flag as `sms_sent_flag`,
					wd.course as `course`,
					wd.ws_date as `ws_date`,
					wd.content_link,
					wd.venue_coordinator_name,
					wd.venue_coordinator_email,
					wd.venue_coordinator_contact,
					wd.support_team_email,
					wd.sales_manager_email,
					wd.extra_instructions_faculty,
					wd.cancel_point,
					wd.changes_point, 
					wd.repeat_session,
					wd.re_entry_point,
					wd.tentitve_remarks,
					wd.feedback_status as `feedback_status`,
					wd.lvc_url as `lvc_url`,
					w.id as `workshop_id`,
					DAYNAME(wd.ws_date) as `day`,
					w.start_date as `start_date`,
					wd.faculty_incharge as `faculty_incharge`,
					wd.faculty_incharge_email as `faculty_incharge_email`, 
					fc.fid,
					fc.name as  `name`, 
					fc.email as `email`,
					fc.phone as `phone`,
					fc1.name as `new_fac_name`,
					w.course_version AS `course_version`
					FROM workshops_dates wd
					LEFT JOIN workshops w ON w.id = wd.workshop_id
					LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
					LEFT JOIN fac_facultydetails fc1 ON fc1.fid = wd.new_faculty_id
        			WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'  and wd.`status`!= 'Error'  AND wd.id='".$workshop_id."'";

$result1 = mysqli_query($conn1,$sel_workshop);
$res_workshop_det = mysqli_fetch_assoc($result1);
$output = json_encode($finalarr);
//echo $output;

mysqli_close($conn1);



?>
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<style>


div.stars{
  width: 270px;
  display: inline-block;
}
input.star{
  display: none;
}
label.star {
  float: right;
  padding: 10px;
  font-size: 36px;
  color: #444;
  transition: all .2s;
}
input.star:checked ~ label.star:before {
  content:'\f005';
  color: #FD4;
  transition: all .25s;
}
input.star-5a:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1a:checked ~ label.star:before {
  color: #F62;
}


input.star-5b:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1b:checked ~ label.star:before {
  color: #F62;
}

input.star-5c:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1c:checked ~ label.star:before {
  color: #F62;
}

input.star-5d:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1d:checked ~ label.star:before {
  color: #F62;
}


input.star-5e:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1e:checked ~ label.star:before {
  color: #F62;
}

input.star-5f:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1f:checked ~ label.star:before {
  color: #F62;
}


input.star-5g:checked ~ label.star:before {
  color:#FE7;
  text-shadow: 0 0 20px #952;
}
input.star-1g:checked ~ label.star:before {
  color: #F62;
}



label.star:hover{
  transform: rotate(-15deg) scale(1.3);
}
label.star:before{
  content:'\f006';
  font-family: FontAwesome;
}
</style>


<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['dupno']))
{
$msg = "FeedBack Submission Failed. Student already Exists.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['paramsmissing']))
{
$msg = "FeedBack Submission Failed. Please provide ratings / answers to all listed questions.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "FeedBack Submission Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "FeedBack Details Submission Successfull.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Lecture FeedBack
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" id="stu_exm_det" name="stu_exm_det" method='post' action='form_handlers/student_lecture_feedback.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-12">
<h3>Student Lecture FeedBack</h3>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label style="font-size:14px;" >Participant's Name :</label>
<input   class="form-control" name="participant_name" id="participant_name"  style="width:100%;" type="text" value="<?php echo $stu_details['student_name'] ?>" readonly />
<input    class="form-control" name="main_id" id="main_id" style="width:100%;" value="<?php echo $main_id; ?>" type="text" hidden />
<input    class="form-control" name="url" id="url" style="width:100%;" value="<?php echo $url; ?>" type="text" hidden />
<input    class="form-control" name="ws_id" id="ws_id" style="width:100%;" value="<?php echo $ws_id; ?>" type="text" hidden />
<input    class="form-control" name="wdate_id" id="wdate_id" style="width:100%;" value="<?php echo $ws_date_id; ?>" type="text" hidden />
<input    class="form-control" name="faculty_id" id="faculty_id" style="width:100%;" value="<?php echo $faculty_id; ?>" type="text" hidden />
</div>

<div class="col-lg-3">
<label style="font-size:14px;" >Participant's Email Id :</label>
<input   class="form-control" name="email_id" id="email_id"  style="width:100%;" type="text" value="<?php echo $stu_details['email_id'] ?>" readonly />
</div>
<div class="col-lg-3">
<label style="font-size:14px;" >Participant's Contact No :</label>
<input    class="form-control" name="contact_no" id="contact_no" style="width:100%;" type="text"  value="<?php echo $stu_details['contact_no'] ?>"   readonly />
</div>

</div>

<div class="form-group row">
<div class="col-lg-6">
<label style="font-size:14px;" >Instructor's Name:</label>
<input    class="form-control" name="instructor_name" id="instructor_name" style="width:100%;" type="text"  value="<?php echo $res_workshop_det['name'] ?>"   readonly />
</div>
<div class="col-lg-3">
<label style="font-size:14px;"  class="">Workshop Date :</label>
<input   class="form-control" name="w_date" id="w_date"  style="width:100%;" type="text" value="<?php echo $res_workshop_det['ws_date'] ?>"  readonly  />
</div>
<div class="col-lg-3">
<label class="" style="font-size:14px;" >Workshop Location :</label>
<input    class="form-control" name="w_location" id="w_location" style="width:100%;" type="text"  value="<?php echo $res_workshop_det['location'] ?>"  readonly />
</div>
</div>


<div class="form-group row">
<div class="col-lg-3">
<label style="font-size:14px;" >Batch Code :</label>
<input    class="form-control" name="batch_code" id="batch_code" style="width:100%;" type="text"  value="<?php echo $res_workshop_det['batch_code'] ?>" readonly />
</div>


<div class="col-lg-9">
<label style="font-size:14px;" >Topic :</label>
<input    class="form-control" name="topic" id="topic" style="width:100%;" type="text"  value="<?php echo $res_workshop_det['topic'] ?>" readonly />
</div>
</div>

</div>


</div>
</div>
</div>


<div id="patch" style="font-size:14px;" >

<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<strong>Please rate the following aspects of today's lecture</strong>
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >1. Instructor's knowledge / Understanding of the subject ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5a" id="star-5a" type="radio" name="star-5a" value="5"/>
  <label class="star star-5a" for="star-5a"></label>
  <input class="star star-4a" id="star-4a" type="radio" name="star-5a" value="4"/>
  <label class="star star-4a" for="star-4a"></label>
  <input class="star star-3a" id="star-3a" type="radio" name="star-5a" value="3"/>
  <label class="star star-3a" for="star-3a"></label>
  <input class="star star-2a" id="star-2a" type="radio" name="star-5a" value="2"/>
  <label class="star star-2a" for="star-2a"></label>
  <input class="star star-1a" id="star-1a" type="radio" name="star-5a" value="1"/>
  <label class="star star-1a" for="star-1a"></label>

</div>
</div>
</div>


<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >2. Instructor's Ability to explain key concepts ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5b" id="star-5b" type="radio" name="star-5b" value="5"/>
  <label class="star star-5b" for="star-5b"></label>
  <input class="star star-4b" id="star-4b" type="radio" name="star-5b" value="4"/>
  <label class="star star-4b" for="star-4b"></label>
  <input class="star star-3b" id="star-3b" type="radio" name="star-5b" value="3"/>
  <label class="star star-3b" for="star-3b"></label>
  <input class="star star-2b" id="star-2b" type="radio" name="star-5b" value="2"/>
  <label class="star star-2b" for="star-2b"></label>
  <input class="star star-1b" id="star-1b" type="radio" name="star-5b" value="1"/>
  <label class="star star-1b" for="star-1b"></label>

</div>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >3. Solving of relevant questions by the instructor ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5c" id="star-5c" type="radio" name="star-5c" value="5"/>
  <label class="star star-5c" for="star-5c"></label>
  <input class="star star-4c" id="star-4c" type="radio" name="star-5c" value="4"/>
  <label class="star star-4c" for="star-4c"></label>
  <input class="star star-3c" id="star-3c" type="radio" name="star-5c" value="3"/>
  <label class="star star-3c" for="star-3c"></label>
  <input class="star star-2c" id="star-2c" type="radio" name="star-5c" value="2"/>
  <label class="star star-2c" for="star-2c"></label>
  <input class="star star-1c" id="star-1c" type="radio" name="star-5c" value="1"/>
  <label class="star star-1c" for="star-1c"></label>

</div>
</div>
</div>


<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >4. Coverage of key concepts by the instructor ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5d" id="star-5d" type="radio" name="star-5d" value="5"/>
  <label class="star star-5d" for="star-5d"></label>
  <input class="star star-4d" id="star-4d" type="radio" name="star-5d" value="4"/>
  <label class="star star-4d" for="star-4d"></label>
  <input class="star star-3d" id="star-3d" type="radio" name="star-5d" value="3"/>
  <label class="star star-3d" for="star-3d"></label>
  <input class="star star-2d" id="star-2d" type="radio" name="star-5d" value="2"/>
  <label class="star star-2d" for="star-2d"></label>
  <input class="star star-1d" id="star-1d" type="radio" name="star-5d" value="1" />
  <label class="star star-1d" for="star-1d"></label>

</div>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >5. Content quality ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5e" id="star-5e" type="radio" name="star-5e" value="5"/>
  <label class="star star-5e" for="star-5e"></label>
  <input class="star star-4e" id="star-4e" type="radio" name="star-5e" value="4"/>
  <label class="star star-4e" for="star-4e"></label>
  <input class="star star-3e" id="star-3e" type="radio" name="star-5e" value="3"/>
  <label class="star star-3e" for="star-3e"></label>
  <input class="star star-2e" id="star-2e" type="radio" name="star-5e" value="2"/>
  <label class="star star-2e" for="star-2e"></label>
  <input class="star star-1e" id="star-1e" type="radio" name="star-5e" value="1"/>
  <label class="star star-1e" for="star-1e"></label>

</div>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >6. Infrastructure rating ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5f" id="star-5f" type="radio" name="star-5f" value="5"/>
  <label class="star star-5f" for="star-5f"></label>
  <input class="star star-4f" id="star-4f" type="radio" name="star-5f" value="4"/>
  <label class="star star-4f" for="star-4f"></label>
  <input class="star star-3f" id="star-3f" type="radio" name="star-5f" value="3"/>
  <label class="star star-3f" for="star-3f"></label>
  <input class="star star-2f" id="star-2f" type="radio" name="star-5f" value="2" />
  <label class="star star-2f" for="star-2f"></label>
  <input class="star star-1f" id="star-1f" type="radio" name="star-5f" value="1"/>
  <label class="star star-1f" for="star-1f"></label>

</div>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >7. Customer Service rating ?:</label>
<br>
</div>
<div class="col-lg-6">
<div class="stars">

  <input class="star star-5g" id="star-5g" type="radio" name="star-5g" value="5"/>
  <label class="star star-5g" for="star-5g"></label>
  <input class="star star-4g" id="star-4g" type="radio" name="star-5g" value="4"/>
  <label class="star star-4g" for="star-4g"></label>
  <input class="star star-3g" id="star-3g" type="radio" name="star-5g" value="3"/>
  <label class="star star-3g" for="star-3g"></label>
  <input class="star star-2g" id="star-2g" type="radio" name="star-5g" value="2"/>
  <label class="star star-2g" for="star-2g"></label>
  <input class="star star-1g" id="star-1g" type="radio" name="star-5g" value="1"/>
  <label class="star star-1g" for="star-1g"></label>

</div>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >8. Faculty Reporting Time:</label>
<br>
</div>
<div class="col-lg-6">
<table>
<tr>
<td>
<div style="vertical-align:center;padding:10px;"><input type="radio" id="faculty_reporting" name="faculty_reporting" value="1" >&nbsp;<strong>Late</strong></div>
</td>
<td>
<div style="vertical-align:center;padding:10px;"><input  type="radio" id="faculty_reporting" name="faculty_reporting" value="2" >&nbsp;<strong>On Time</strong></div>
</td>
</tr>
</table>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="" style="font-size:14px;" >9. Class End Time:</label>
<br>
</div>
<div class="col-lg-6">
<table>
<tr>
<td>
<div style="vertical-align:center;padding:10px;"><input type="radio" id="faculty_reporting_end" name="faculty_reporting_end" value="1" >&nbsp;<strong>Late</strong></div>
</td>
<td>
<div style="vertical-align:center;padding:10px;"><input  type="radio" id="faculty_reporting_end" name="faculty_reporting_end" value="2" >&nbsp;<strong>On Time</strong></div>
</td>
<td>
<div style="vertical-align:center;padding:10px;"><input  type="radio" id="faculty_reporting_end" name="faculty_reporting_end" value="3" >&nbsp;<strong>Early</strong></div>
</td>
</tr>
</table>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Additional Information
</h3>
</div>
</div>

<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-3">
<label class="" style="font-size:14px;" >Comments(If Any) :</label>
<br>
</div>
<div class="col-lg-9">
<textarea id='main_comments' name='main_comments' class='form-control' ></textarea>
</div>
</div>
</div>

</div>
</div>
</div>

</div>
<!--CMA EXAM PATCH END-->

<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button id="approve" name="approve" type="submit" class="btn btn-success" >Submit</button>
<button type="reset" class="btn btn-secondary">Cancel</button></center>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<p>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" ><strong>Legends :</strong></label>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" >* Very Poor</label>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" >** Poor</label>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" >*** Avearge</label>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" >**** Good</label>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<label class="" style="font-size:14px;" >***** Excellent</label>
</div>
</div>
</div>
</div>

</form>

<?php
}
else
{
	echo "<p><p><p><p><p><p><p><p><center><h3>Thank you for the feedback for this lecture.</h3></center><p>";
}
?>


