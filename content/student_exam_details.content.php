<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "Refund Requirement already raised.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Refund Requirement cannot be processed. Mandatory Fields missing";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['excessamt']))
	{
		$msg = "Refund Requirement cannot be processed. Excess Refund amount";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Refund Requirement cannot be processed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Refund Requirement  processed Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<!--begin::Portlet-->

<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Exam Details
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-4">
<label>Account No:</label>
<input  class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text"  value=""/>
</div>
<div class="col-lg-4" style="padding-top:25px;">
<label></label>
<button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="RECSelect();">Get Details</button>
</div>
<div class="col-lg-4" >
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" id="stu_exm_det" name="stu_exm_det" method='POST' action='form_handlers/student_exam_details.php'>
	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Details
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
							Organization Details
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
							Exam Details
						</a>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
							Records
						</a>
					</li>-->
				</ul>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_portlet_tab_1_1">
				<div class="form-group row">
				<div class="col-lg-3">
				<label>Student Name :</label>
				<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
				</div>
				<div class="col-lg-3">
				<label>Registered Email ID :</label>
				<input    class="form-control" name="emailid_1" id="emailid_1" style="width:100%;" type="text"  value=""   readonly />
				</div>
				<div class="col-lg-3">
				<label class="">Registered Mobile Number :</label>
				<input   class="form-control" name="mobileno_1" id="mobileno_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-3">
				<label class="">Counselor Name :</label>
				<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				</div>
				
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label>Course :</label>
				<input    class="form-control" name="course_1" id="course_1" style="width:100%;" type="text"  value="" readonly />
			    </div>
				<div class="col-lg-3">
				<label class="">Sub Course :</label>
				<input   class="form-control" name="subcourse_1" id="subcourse_1"  style="width:100%;" type="text" readonly />
				</div>
				<div class="col-lg-3">
				<label>Type of training mode :</label>
				<select class="form-control" id="training_mode" name="training_mode" required>
				<option value="">Select</option>
				<option value="Self Study">Self Study</option>
				<option value="Classroom">Classroom</option>
				<option value="Online(LVC)">Online(LVC)</option>
				</select>
			    </div>
				<div class="col-lg-3">
				<label>Location :</label>
				<select class="form-control" id="location" name="location" required>
				<option value="">Select</option>
				<option value="Mumbai">Mumbai</option>
				<option value="Delhi">Delhi</option>
				<option value="Bangalore">Bangalore</option>
				<option value="Pune">Pune</option>
				<option value="Hyderabad">Hyderabad</option>
				<option value="Chennai">Chennai</option>
				<option value="Other">Other</option>
				</select>
			    </div>
				</div>
				
			    <div class="form-group row">
				<div class="col-lg-6">
				<label>Modules Selected :</label>
				<textarea class="form-control" name="modules_1" id="modules_1" style="width:100%;" type="text"  value="" readonly /></textarea>
			    </div>
				<div class="col-lg-6">
				<label>Course you have enrolled with us :</label>
				<select class="form-control" id="course_enrolled" name="course_enrolled" >
				<option value="">Select</option>
				<option value="CPA">CPA</option>
				<option value="CMA">CMA</option>
				<option value="ACCA">ACCA</option>
				<option value="CFA">CFA</option>
				<option value="FRM">FRM</option>
				<option value="FM">FM</option>
				<option value="BAT">BAT</option>
				<option value="DM">DM</option>
				</select>
				</div>
				</div>
				
				
				</div>
				<div class="tab-pane" id="kt_portlet_tab_1_2">
				
				
				<!--CMA EXAM PATCH-->
				<div id="cma_patch" style="display:none;">
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you registered with IMA ?:</label>
				<br>
				</div>
				<div class="col-lg-1">
				<input  type="radio" id="ima_reg" name="ima_reg" value="1">Yes
				<input type="radio" id="ima_reg" name="ima_reg" value="0">No
				</div>
				<div class="col-lg-8">
				<label class="">If yes Please provide the IMA Member ID / Customer ID :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
			    <div class="form-group row">
				<div class="col-lg-3">
				<label class="">Which CMA Part you have cleared ?:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> PART I</label>
				<input type="checkbox" id="is_cma_part1" name="is_cma_part1" value="1" />
				<input type="text" class="form-control" id="is_cma_part1_marks" name="is_cma_part1_marks" value="" placeholder='Enter Marks Obtained' />
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> PART II</label>
			    <input type="checkbox" id="is_cma_part2" name="is_cma_part2" value="1" />
			    <input type="text" class="form-control" id="is_cma_part2_marks" name="is_cma_part2_marks" value="" placeholder='Enter Marks Obtained' />
				</div>
				<div class="col-lg-3">
				<label for="vehicle3"> NONE</label>
			    <input type="checkbox" id="is_cma_none" name="is_cma_none" value="1" />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">When you are planning for Part 1 ? :</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1">Select Date</label>
				<input   class="form-control" name="is_cma_plan_1_sq" id="is_cma_plan_1_sq"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">When you are planning for Part 2 ? :</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1">Select Date</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Comments(If Any) :</label>
				<br>
				</div>
				<div class="col-lg-9">
				<textarea id='cma_comments' name='cma_comments' class='form-control' ></textarea>
				</div>
				</div>
				</div>
				<!--CMA EXAM PATCH END-->

				<!--CPA EXAM PATCH-->
				<div id="cpa_patch" style="display:none;">
                <div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Subject Cleared or planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
				<label for="vehicle1"> AUD</label>
				<input type="checkbox" id="is_cpa_aud_appear" name="is_cpa_aud_appear" value="1" />
				<input   class="form-control" name="is_cpa_aud_mon" id="is_cpa_aud_mon"  style="width:100%;" type="date"  /><br>
				<input type="text" class="form-control" id="is_cpa_aud_marks" name="is_cpa_aud_marks" value="" placeholder='Enter Score Obtained' />
				</div>
				<div class="col-lg-2">
				<label for="vehicle1"> BEC</label>
			    <input type="checkbox" id="is_cpa_bec_appear" name="is_cpa_bec_appear" value="1" />
				<input   class="form-control" name="is_cpa_bec_mon" id="is_cpa_bec_mon"  style="width:100%;" type="date"  /><br>
				 <input type="text" class="form-control" id="is_cpa_bec_marks" name="is_cpa_bec_marks" value="" placeholder='Enter Score Obtained' />
				
				</div>
				<div class="col-lg-2">
				<label for="vehicle1"> FAR</label>
			    <input type="checkbox" id="is_cpa_far_appear" name="is_cpa_far_appear" value="1" />
			    <input   class="form-control" name="is_cpa_far_mon" id="is_cpa_far_mon"  style="width:100%;" type="date"  /><br>
				<input type="text" class="form-control" id="is_cpa_far_marks" name="is_cpa_far_marks" value="" placeholder='Enter Score Obtained' />
				</div>
				<div class="col-lg-2">
				<label for="vehicle1"> REG</label>
			    <input type="checkbox" id="is_cpa_reg_appear" name="is_cpa_reg_appear" value="1" />
			    <input   class="form-control" name="is_cpa_reg_mon" id="is_cpa_reg_mon"  style="width:100%;" type="date"  /><br>
				  <input type="text" class="form-control" id="is_cpa_reg_marks" name="is_cpa_reg_marks" value="" placeholder='Enter Score Obtained' />
				</div>
				</div>
				
				
				</div>
				<!--CPA EXAM PATCH END-->
				
				
				<!--FM EXAM PATCH-->
				<div id="fm_patch" style="display:none;" >
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you appeared for the exams? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="fm_ex_appear" name="fm_ex_appear" value="1">Yes
				<input type="radio" id="fm_ex_appear" name="fm_ex_appear" value="0">No
				</div>
				<div class="col-lg-2">
				<label class="">Exam Date :</label><br>
				<input   class="form-control" name="is_fm_attempt" id="is_fm_attempt"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">If No When are you planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				</div>
				<!--FM EXAM PATCH END-->
				
				
				
				<!--ACCA EXAM PATCH-->
				<div id="acca_patch" style="display:none;" >
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">ACCA Registration id ( 7 digit)</label>
				<br>
				</div>
				<div class="col-lg-3">
				<input type="text" id="acca_reg_id" name="acca_reg_id" class="form-control" />
			    </div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Please mark the consent to share the data with ACCA? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="acca_consent" name="acca_consent" value="1">Yes
				<input type="radio" id="acca_consent" name="acca_consent" value="0">No
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Enter Student Details :</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label>Student First Name :</label>
				<input   class="form-control" name="acca_first_name" id="acca_first_name"  style="width:100%;" type="text"  />
				</div>
				<div class="col-lg-3">
				<label>Student Last Name  :</label>
				<input   class="form-control" name="acca_last_name" id="acca_last_name"  style="width:100%;" type="text"  />
				</div>
				<div class="col-lg-3">
				<label>Student DOB  :</label>
				<input   class="form-control" name="acca_student_dob" id="acca_student_dob"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Attempting :</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> FR</label>
				<input type="checkbox" id="is_fr" name="is_fr" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> FM</label>
			    <input type="checkbox" id="is_fm" name="is_fm" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> SBL</label>
			    <input type="checkbox" id="is_sbl" name="is_sbl" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> SBR</label>
			    <input type="checkbox" id="is_sbr" name="is_sbr" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> MA</label>
			    <input type="checkbox" id="is_ma" name="is_ma" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> BT</label>
			    <input type="checkbox" id="is_bt" name="is_bt" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> LW</label>
			    <input type="checkbox" id="is_lw" name="is_lw" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> TX</label>
			    <input type="checkbox" id="is_tx" name="is_tx" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class=""></label>
				<br>
				</div>
				<div class="col-lg-3">
				<label for="vehicle1"> AA</label>
			    <input type="checkbox" id="is_aa" name="is_aa" value="1" />
				<input   class="form-control" name="is_fm_appr" id="is_fm_appr"  style="width:100%;" type="date"  />
				</div>
				</div>
				<!--ACCA EXAM PATCH END-->
				
				
				</div>
				
				
				<!--CFA EXAM PATCH-->
				<div id="cfa_patch"  style="display:none;">
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Sub course :</label>
				<br>
				</div>
				
				<div class="col-lg-3">
				<select class="form-control" id="cfa_subcourse" name="cfa_subcourse" required>
				<option value="">Select</option>
				<option value="CFA Level 1">CFA Level 1</option>
				<option value="CFA LEVEL 2">CFA LEVEL 2</option>
				</select>
			    </div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you appeared for the exams? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="cfa_appear" name="cfa_appear" value="1">Yes
				<input type="radio" id="cfa_appear" name="cfa_appear" value="0">No
				</div>
				<div class="col-lg-2">
				<label class="">Exam Date :</label><br>
				<input   class="form-control" name="is_cfa_attempt" id="is_cfa_attempt"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-2">
				<label class="">Result :</label><br>
				<select  class="form-control" id="is_cfa_result" name="is_cfa_result"   >
				<option value="Pass">Pass</option>
				<option value="Fail">Fail</option>
				</select>
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">If No When are you planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
				<input   class="form-control" name="is_cfa_appr" id="is_cfa_appr"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				</div>
				<!--CFA EXAM PATCH END-->
				
				<!--FRM EXAM PATCH-->
				<div id="frm_patch" style="display:none;" >
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
                <div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you appeared for the exams? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="frm_appear" name="frm_appear" value="1">Yes
				<input type="radio" id="frm_appear" name="frm_appear" value="0">No
				</div>
				<div class="col-lg-2">
				<label class="">Exam Date :</label><br>
                <input   class="form-control" name="is_frm_attempt" id="is_frm_attempt"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-2">
				<label class="">Result :</label><br>
				<select  class="form-control" id="is_frm_result" name="is_frm_result"   >
				<option value="Pass">Pass</option>
				<option value="Fail">Fail</option>
				</select>
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">If No When are you planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
				 <input   class="form-control" name="is_frm_appr" id="is_frm_appr"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				</div>
				<!--FRM EXAM PATCH END-->
				
				<!--BAT EXAM PATCH-->
				<div id="bat_patch"  style="display:none;">
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
                <div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you appeared for the exams? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="bat_appear" name="bat_appear" value="1">Yes
				<input type="radio" id="bat_appear" name="bat_appear" value="0">No
				</div>
				<div class="col-lg-2">
				<label class="">Exam Date :</label><br>
				<input   class="form-control" name="is_bat_attempt" id="is_bat_attempt"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">If No When are you planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
				<input   class="form-control" name="is_bat_appr" id="is_bat_appr"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				</div>
				<!--BAT EXAM PATCH END-->
				
				
				<!--DM EXAM PATCH-->
				<div id="dm_patch"  style="display:none;">
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">Exam Details:</label>
				<br>
				</div>
				<div class="col-lg-3">
				<label class="">Exam Date:</label>
				<input   class="form-control" name="is_cma_plan_2_sq" id="is_cma_plan_2_sq"  style="width:100%;" type="date"  />
				</div>
				<div class="col-lg-6">
				<label class="">Final Score :</label><br>
				<input   class="form-control"  name="ima_mem_id" id="ima_mem_id"  style="width:100%;" type="text"    />
				</div>
				</div>
				
                <div class="form-group row">
				<div class="col-lg-3">
				<label class="">Have you appeared for the exams? :</label>
				<br>
				</div>
				<div class="col-lg-2">
				<input  type="radio" id="dm_appear" name="dm_appear" value="1">Yes
				<input type="radio" id="dm_appear" name="dm_appear" value="0">No
				</div>
				<div class="col-lg-2">
				<label class="">Exam Date :</label><br>
				<input   class="form-control" name="is_dm_attempt" id="is_dm_attempt"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<label class="">If No When are you planning to appear? :</label>
				<br>
				</div>
                <div class="col-lg-2">
			    <input   class="form-control" name="is_dm_appr" id="is_dm_appr"  style="width:100%;" type="date"  />
				</div>
				
				</div>
				</div>
				<!--DM EXAM PATCH END-->
				
				
				</div>
				
				<div class="tab-pane" id="kt_portlet_tab_1_3">
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Beneficiary Name in Bank:</label>
				<input  class="form-control" name="benf_1" id="benf_1"  style="width:100%;" type="text"   />
				</div>
				<div class="col-lg-6">
				<label class="">Bank Name:</label>
				<input   class="form-control"  name="bankname_1" id="bankname_1"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Bank A/c Number:</label>
				<input  class="form-control" name="accno_1" id="accno_1"  style="width:100%;" type="text"   />
				</div>
				<div class="col-lg-6">
				<label class="">IFSC Code:</label>
				<input   class="form-control"  name="ifsc_1" id="ifsc_1"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Branch Location:</label>
				<input  class="form-control" name="banklocation_1" id="banklocation_1"  style="width:100%;" type="text"   />
				</div>
				
				<div class="col-lg-6">
				<label class="">Refund Category:</label>
				<select  class="form-control" id="refund_category"  name="refund_category" onchange="displaybankdetails(this.value)">
				<option value="0">Select</option>
				<option value="Direct Refund">Direct Refund</option>
				<option value="Neev Finance">Neev Finance - Refund</option>
				<option value="Liqui Loan">Liqui Loan - Refund</option>
				<option value="ABFL">ABFL - Refund</option>
				<option value="Avanse">Avanse - Refund</option>
				<option value="Refund As Exception">Refund As Exception</option>
				</select >
				</div>
				
				</div>
				
				<div class="form-group row">
				<div class="col-lg-12">
				<label class="">Reason for Refund:</label>
				<textarea type="text" class="form-control"  name="comment_1" id="comment_1" rows="4" required></textarea>
				</div>
				</div>
				</div>
				
				

				
				
				
			</div>
		</div>
	</div>

	<!--end::Portlet-->
	<div class="kt-portlet__foot kt-portlet__foot--fit-x">
	<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="approve" name="approve" type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
	</div>
	</div>
</form>
</div>

<?php
?>
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function RECSelect()
{
	var val = document.getElementById("accountno_1").value;
	if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById("stu_exm_det").reset();
			document.getElementById('recs_1').value = output["account_no"];
			
			var str = output["modules"];
            var res = str.split("|##|");
			document.getElementById('modules_1').value = res;
			document.getElementById('studentname_1').value = output["studentname"];
			document.getElementById('assignedto_1').value = output["assigned_user"];
			document.getElementById('emailid_1').value = output["email"];
			document.getElementById('mobileno_1').value = output["phone"];
			document.getElementById('course_1').value = output["course"];
			document.getElementById('subcourse_1').value = output["subcourse"];
			document.getElementById('course_enrolled').value = output["course"];
			document.getElementById('location').value = output["batch_location"];
			
			if(output["course"]=='ACCA')
			{
				document.getElementById("acca_patch").style.display="block";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CPA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="block";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CMA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="block";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='CFA')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="block";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='FM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="block";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='FRM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="block";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='DM')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="block";
				document.getElementById("bat_patch").style.display="none";
			}
			if(output["course"]=='BAT')
			{
				document.getElementById("acca_patch").style.display="none";
				document.getElementById("cpa_patch").style.display="none";
				document.getElementById("cma_patch").style.display="none";
				document.getElementById("cfa_patch").style.display="none";
				document.getElementById("fm_patch").style.display="none";
				document.getElementById("frm_patch").style.display="none";
				document.getElementById("dm_patch").style.display="none";
				document.getElementById("bat_patch").style.display="block";
			}
		
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_studentdetails.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}


function costperclass(val)
{
	var cost=document.getElementById('course_fee').value;
	document.getElementById('cost_per_classes').value = (cost/val).toFixed(2);
	
}


function calculateamtref()
{
var amtpd = document.getElementById('amount_paid').value;
var adchrg=document.getElementById('admin_charges').value;
var cpc=document.getElementById('cost_per_classes').value;
var counducted_classes=document.getElementById('no_of_classes').value;
var thirdparty=document.getElementById('third_party_cost').value;

document.getElementById('amtref_1').value = Math.round(((amtpd-adchrg)-(counducted_classes*cpc)-thirdparty).toFixed(2));
}

function displaybankdetails(val)
{
if(val=='Neev Finance')
{
document.getElementById('benf_1').value='NEEV CREDIT PVT LTD';
document.getElementById('bankname_1').value='YES BANK';
document.getElementById('accno_1').value='019063700001121';
document.getElementById('ifsc_1').value='YESB0000190';
document.getElementById('banklocation_1').value='KOLKATA';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;
}else if (val=='Liqui Loan')
{
document.getElementById('benf_1').value='NDX P2P Pvt Ltd Borrowers Repayment Account';
document.getElementById('bankname_1').value='IDBI Bank';
document.getElementById('accno_1').value='0004102000039899';
document.getElementById('ifsc_1').value='IBKL0000004';
document.getElementById('banklocation_1').value='Nariman Point';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else if (val=='ABFL')
{
document.getElementById('benf_1').value='Aditya Birla Finance Limited';
document.getElementById('bankname_1').value='HDFC Bank';
document.getElementById('accno_1').value='00600350111290';
document.getElementById('ifsc_1').value='HDFC0000060';
document.getElementById('banklocation_1').value='HDFC Bank Limited, Fort Branch, Mumbai ';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else if (val=='Avanse')
{
document.getElementById('benf_1').value='BLUEBEAR TECHNOLOGY PRIVATE LIMITED';
document.getElementById('bankname_1').value='ICICI Bank';
document.getElementById('accno_1').value='662905600314';
document.getElementById('ifsc_1').value='ICIC0006629';
document.getElementById('banklocation_1').value='Janakuri, Delhi';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else
{
document.getElementById('benf_1').value='';
document.getElementById('bankname_1').value='';
document.getElementById('accno_1').value='';
document.getElementById('ifsc_1').value='';
document.getElementById('banklocation_1').value='';

document.getElementById('benf_1').readOnly = false;
document.getElementById('bankname_1').readOnly = false;
document.getElementById('accno_1').readOnly = false;
document.getElementById('ifsc_1').readOnly = false;
document.getElementById('banklocation_1').readOnly = false;	
}
}
</script>