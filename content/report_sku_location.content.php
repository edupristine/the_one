<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$location_id = 1;
	
	
	// if($user_id == "")
	// {
		// $user_id = $_SESSION['USER_ID'];
	// }
	// if($location_id == "")
	// {
		// $location_id = $_SESSION['U_LOCATION_ID'];
	// }
	
	// if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	// {
		// $location_id = $_SESSION['U_LOCATION_ID'];
	// }
	
	$query_allskus = "SELECT s.id,s.sku_name,s.sku_code,s.sku_type,(sl.sku_pre_qty+sl.sku_qty_avl) as sku_available,sl.sku_qty_min,sl.sku_qty_req FROM `skus` s,skus_locations sl WHERE sl.sku_id = s.id and sl.loc_id = ".$location_id." ORDER BY sku_name";
	$result_allskus = Select($query_allskus,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id;
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$center_locations = "SELECT id,loc_name from locations where loc_type = 'CENTER'";
	$result_loc_centers = Select($center_locations,$conn);
	
	if($_SESSION['U_LOCATION_TYPE'] != "CENTER" && $location_type_report == "CENTER")
	{
		$newrecord = array('id'=>$_SESSION['U_LOCATION_ID'],'loc_name'=>$_SESSION['U_LOCATION_NAME']);
		array_push($result_loc_centers['rows'],$newrecord);
	}
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											SKU Report (<?php echo $location_name_report; ?>)
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
																if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
																{
																?>
														<div class="col">
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Other Locations
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_loc_centers['rows'] as $loc_center)
																			{	
																				if($loc_center['id'] != $location_id)
																				{
																				?>
																				<a class="dropdown-item" href="report_sku_location.php?uid=<?php echo $user_id;?>&lid=<?php echo $loc_center['id'];?>"><?php echo $loc_center['loc_name']; ?></a>
																		<?php	}
																				}
																		?>
																		
																	</div>
																
															</div>
														</div>
													<?php
																}
																?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>SKU Name</th>
												<!--<th>SKU Type</th>-->
												
												<th><CENTER>Available</CENTER></th>
												<th><CENTER><?php if($location_type_report == "CENTER") echo "Requirement"; else echo "Threshold";?></CENTER> </th>
												<!--<th><?php if($location_type_report == "CENTER") echo "In Courier"; else echo "Order Placed";?></th>-->
												<th><CENTER>Shortfall</CENTER></th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allskus['rows'] as $sku) 
											{	
												$incour = 0;
												if($location_type_report == "CENTER")
												{
													$incourier_query = "SELECT sum(trans_qty) as incour from transfers_skus where sku_id = ".$sku['id']." AND trans_id in (SELECT id from transfers where transfer_status = 'INITIATED' and to_loc_id = ".$location_id.");";
													$result_incourier = Select($incourier_query,$conn);
													if($result_incourier['rows'][0]['incour'] != NULL)
													{
														$incour = $result_incourier['rows'][0]['incour'];
													}
												}
												else
												{
													$incourier_query = "SELECT sum(qty) as incour from orders_skus where sku_id = ".$sku['id']." AND ord_id in (SELECT id from orders where ord_status = 'Placed');";
													$result_incourier = Select($incourier_query,$conn);
													if($result_incourier['rows'][0]['incour'] != NULL)
													{
														$incour = $result_incourier['rows'][0]['incour'];
													}
												}
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $sku['sku_name']; ?></td>
													<!--<td><?php// echo $sku['sku_type']; ?></td>-->
													
													<td><CENTER><?php echo $sku['sku_available']; ?></CENTER></td>
													<td><CENTER><?php if($location_type_report == "CENTER") echo $sku['sku_qty_req']; else echo $sku['sku_qty_min'];?> </CENTER></td>
													<!--<td><?php// echo $incour; ?></td>-->
													<?php
													$HOshortfall="0";
													$HOshortfallcal=$sku['sku_qty_min'] - $sku['sku_available'];
													$style="style='background-color:#ffffff;color:#000000;'";
													if($HOshortfallcal<=0)
													{
														$HOshortfall=0;
													}else
													{
						                                $HOshortfall=$HOshortfallcal;
														$style="style='background-color:#FFD9B3;color:#000000;'";
													}
													
													
													$CEshortfall="0";
													$CEshortfallcal=$sku['sku_qty_req'] - $sku['sku_available'];
													if($CEshortfallcal<=0)
													{
														$CEshortfall=0;
													}else
													{
						                                $CEshortfall=$CEshortfallcal;
													}
													?>
													<td <?php echo $style;?>><b><CENTER><?php if($location_type_report == "CENTER") echo  $CEshortfall; else echo $HOshortfall; ?></b></CENTER></td>
													
												 
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
      setTimeout(function () {
        location.reload(true);
      }, 15000);
    });
  </script>