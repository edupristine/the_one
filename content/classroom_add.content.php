<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupclassroom']))
	{
		$msg = "Classroom Creation Failed. Classroom Name already Exists at selected center";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}

	elseif(isset($_GET['Classroom_name_missing']))
	{
		$msg = "Classroom Creation Failed. Please enter Classroom name.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Classroom Creation Failed. Classroom Name and Center are Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Classroom Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Classroom Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add Classrooms
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/classroom_add.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Classroom Name</label>
	<input type="text" class="form-control" id="classroom_name" name="classroom_name">
	
</div>
<div class="col-lg-6">
<?php 
	$query_users = "SELECT id,center_name from centers";
	$result_users = Select($query_users,$conn);
?>
	<label class="">Center Name:</label>
			<select class="form-control kt-select2" id="center_name" name="center_name">
			<option value="0">Select</option>
				<?php 
					foreach($result_users['rows'] as $user)
					{

							echo '<option value="'.$user['id'].'">'.$user['center_name'].'</option>';
		
					}
				?>
			</select>
	
</div>
</div>

<div class="form-group row">

<div class="col-lg-6">
<label>Available Seats</label>
	<input type="text" class="form-control" id="avl_seats" name="avl_seats">
	</div>
	<div class="col-lg-6">

	
</div>

</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>