<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$detailscheck		= get_post_value('batchcodes');
$detailscheck = stripslashes($detailscheck);
$detailscheck = json_decode($detailscheck);
$detailscheck = json_decode(json_encode($detailscheck), true);

// $query_skus = "SELECT s.sku_name FROM issuance isu,skus s  WHERE s.id=isu.sku_id and isu.student_id='".$result_students['rows'][0]['id']."' and isu.courier_record_id is not Null ";
// $result_skus = Select($query_skus,$conn);

?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="background-color:#fff;">

<div class="kt-portlet__body">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

<!-- Load paper.css for happy printing -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">
<center>
<style type="text/css" media="screen"></style>

<style type="text/css" media="print">
@media print {
@page {
    /* dimensions for the whole page */
    size: A4 potrait;
    
    margin: 0;
}

html {
    /* off-white, so body edge is visible in browser */
    background: #eee;
}

body {
    /* A5 dimensions */
    height: 297mm;
    width: 210mm;

    margin: 0;
}


}
</style>

<div class="form-group row" >
<div class="col-lg-12">
<center><button onclick="printElem('print_area2')">Print</button></center>
</div>
</div>
 <section id="print_area2" class="sheet padding-10mm ">

    <!-- Write HTML just like a web page -->
	
	<table style="width:100%;border:1px;">
	<?php
	$total_books=array();
	foreach($detailscheck as $details)
	{
	$query_students = "SELECT id,student_name,student_contact,student_email,student_address,course_id,subcourse_id FROM `students`  WHERE crm_id='". $details['accountno']."' ";
	$result_students = Select($query_students,$conn);
	
	
	$query_course = "SELECT course_name FROM `courses`  WHERE id='". $result_students['rows'][0]['course_id']."' ";
	$result_course = Select($query_course,$conn);
	
	$query_subcourse = "SELECT subcourse_name FROM `sub_courses`  WHERE id='". $result_students['rows'][0]['subcourse_id']."' ";
	$result_subcourse = Select($query_subcourse,$conn);
    ?>
	<tr>
	<td style="font-size:14px;text-align:left;width:100%;height:160px;vertical-align:top;">
	<?php
	echo "Name: <b>".strtoupper($result_students['rows'][0]['student_name'])."</b><br>";
	echo "Organization No: ".$details['accountno']."<br>";
	echo "Course: ".$result_course['rows'][0]['course_name']."<br>";
	echo "Phone No: ".$result_students['rows'][0]['student_contact']."<br>";
	//echo "Email: ".$result_students['rows'][0]['student_email']."<br>";
	echo "Address: ".$result_students['rows'][0]['student_address']."<br>";
    echo "<b>In Courier:</b><br>";
	$books="";
	foreach($details['books'] as $mybooks)
	{
	$total_books[] =$mybooks;	
	$books .= trim($mybooks).",";
	}
	echo  rtrim($books, ", ");
	echo "<p>";
	?>
	</td>
	</tr>
	<?php


	}
	
	$countedarray = array_count_values($total_books);

	?>
	
	<tr>
	
	<td>
	<b>BOOKS IN COURIER:</b></br>
	<?php
	$total_bk=0;
	foreach($countedarray as $x => $x_value) {
	echo "<b>". $x . "</b>=" . $x_value."<br>";
	$total_bk=$total_bk+$x_value;
	}
	?>
	<b>Total Books Count :</b> <?php echo "<b>".$total_bk."</b>";?>
	</td>
	<td>
	</td>
	</tr>
	</table>


  </section>



</div>


</center>

</div>
</div>

<script>
function printElem(divId) {
var printContents = document.getElementById(divId).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
document.body.innerHTML = originalContents;
return true;
}
</script>

