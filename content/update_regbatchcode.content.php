<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update Registered Batch Code
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">


<?php
$query_harry_codes = "SELECT id,code FROM workshops WHERE code not like '%SEM%' AND deleteflag=0 ORDER BY id desc";
$result_harry_code = Select($query_harry_codes,$harry);	
?>

	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:5%;">#</th>
				<th style="width:15%;">Account No.</th>
				<th style="width:12%;">Old Registered batch Code</th>
				<th style="width:12%;">New Registered Batch Code</th>
				<th style="width:10%;"><center>Action</center></th>
			</tr>
		</thead>
		<tbody id="rows_div">
			<tr id="row_id_1">
				<td class="text-center"><input readonly name="sr_1" id="sr_1" style="width:100%;padding:2px;" type="text" value="1"/></td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="accountno_1" id="accountno_1" style="width:100%;padding:2px;" type="text" onBlur="RECSelect(this.value,'row_1');" value=""/></td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="oldregcode_1" id="oldregcode_1"  style="width:100%;padding:2px;" type="text" value="" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;padding:2px;" type="text" hidden /></td>
				<td  class="text-left">
					<select class="chosen-select"  id="newregcode_1" name="newregcode_1" style="width:100%;padding:2px;">
						<option value="">Select </option>
						<?php
						foreach($result_harry_code['rows'] as $codes)
						{
							echo "<option value='".$codes['code']."'>".$codes['code']."</option>";
							
						}
						?>
					</select>
				</td>
				
				<td id="prodel_1" name="prodel_1" class="text-center">
					<div class="btn-group">
						<a onClick="DelREC('row_id_1');" data-toggle="tooltip" title="Delete Record" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddREC();" data-toggle="tooltip" title="Add Record" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-8">
			<a onClick="SaveBatchcodes();" class="btn btn-success">Update Registered Batch Code/s</a>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>