<!-- end:: Subheader -->

<script>
	function filter_students()
	{
		var filter_param = document.getElementById('date_range').value;
		window.location = 'book_couriers.php?date_range='+filter_param;
	}
</script>
<?php
setlocale(LC_MONETARY, 'en_IN');
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date("Y-m-d");
    $edate = date("Y-m-d");
    $date_range = $sdate." / ".$edate;
}

$startdate=$sdate;
$enddate=$edate;


if($date_range=="")
{
$sel_regcode = "SELECT s.id,s.student_name,s.crm_id,scr.id as `req_id`,s.course_id  FROM students s,student_couriers_request scr WHERE s.id=scr.student_id AND scr.courier_status!='Sent' AND scr.created_at=CURRENT_DATE()";
}
else
{
$sel_regcode = "SELECT s.id,s.student_name,s.crm_id,scr.id as `req_id`,s.course_id  FROM students s,student_couriers_request scr WHERE s.id=scr.student_id AND scr.courier_status!='Sent' AND DATE(scr.created_at) BETWEEN '".$startdate."' AND '".$enddate."'";	
}
$outputs = Select($sel_regcode,$conn);	


$query_sku_codes = "SELECT id,sku_name FROM skus order by sku_name ";
$result_sku_code = Select($query_sku_codes,$conn);	
?>
<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Print Courier Labels
</h3>
</div>
</div>

<div class="form-group row">
<div class="kt-portlet">
<div class="form-group row"style="padding-top:2%;padding-left:2%;">

<div class="col-lg-4">
	<label>Date Range</label>
	<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range"  name="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Get Records</button>
	
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='courier_print.php'>
<div class="kt-portlet__body">

	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:5%;text-align:center;"> <input type="checkbox" id='checkall' checked /></th>
				<th style="width:5%;text-align:center;">Account No.</th>
				<th style="width:5%;text-align:center;">Student Name</th>
				<th style="width:5%;text-align:center;">Course</th>
				<th style="width:30%;">Books To Courier</th>
				
			</tr>
		</thead>
		<tbody id="rows_div">
		
		     <?php
			 $i=1;
		     foreach($outputs['rows'] as $recods)
			 {
					$query_course = "SELECT * FROM courses where id='".$recods['course_id']."'";
					$result_course = Select($query_course,$conn);	 
				 
				 
			 ?>
			<tr id="row_id_<?php echo  $i;?>">
				<td class="text-center"><input type='checkbox'  class='checkbox'  name="checked_<?php echo  $i;?>" id="checked_<?php echo  $i;?>" checked /><input hidden name="sr_<?php echo  $i;?>"  id="sr_<?php echo  $i;?>" style="width:100%;padding:2px;" type="text" value="<?php echo  $i;?>"/></td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="" name="accountno_<?php echo  $i;?>" id="accountno_<?php echo  $i;?>" style="width:100%;padding:2px;" type="text"  value="<?php echo $recods['crm_id'];?>" hidden/><?php echo $recods['crm_id'];?></td>
				<input    class="form-control" name="recs_<?php echo  $i;?>" id="recs_<?php echo  $i;?>" style="width:100%;padding:2px;" type="text" hidden /></td>
				<td><?php echo $recods['student_name'];?>
				</td>
				<td><center><?php echo $result_course['rows'][0]['course_name'];?></center>
				</td>
				<td  class="text-left">
				<?php 
				$final_books="";
				?>
				<div id="newdiv_<?php echo  $i;?>" name="newdiv_<?php echo  $i;?>" hidden>
					<select  multiple  id="books_<?php echo  $i;?>" name="books_<?php echo  $i;?>" style="width:100%;padding:2px;">
						<option value="">Select </option>
						<?php
						$query_sku_rec = "SELECT * FROM issuance where student_id='".$recods['id']."' and courier_req_id='".$recods['req_id']."' ";
						$result_sku_rec = Select($query_sku_rec,$conn);	
						
				

						foreach($result_sku_code['rows'] as $skus)
						{
						$selected="";
						foreach($result_sku_rec['rows'] as $skusot)
						{
						if($skusot['sku_id']==$skus['id'])
						{
						$selected="selected";
						echo $selected."<br>";
						echo "<option value='".$skus['sku_name']."' ".$selected.">".$skus['sku_name']."</option>";
						$final_books .=$skus['sku_name'].",";
						}
                        }
                        }
						?>
                       
					</select>
				</div>
				<?php echo rtrim($final_books, ',');?>
				</td>
				
				
			</tr>
			<?php
			$i++;
			 }
			 ?>
			
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-8">
			<a onClick="PrintLabels();" class="btn btn-success">Print Courier Label/s/s</a>
			  <!-- <button class="btn btn-success"> Print Courier Label</button>-->
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<script type='text/javascript'>
 $(document).ready(function(){
   // Check or Uncheck All checkboxes
   $("#checkall").change(function(){
     var checked = $(this).is(':checked');
     if(checked){
       $(".checkbox").each(function(){
         $(this).prop("checked",true);
       });
     }else{
       $(".checkbox").each(function(){
         $(this).prop("checked",false);
       });
     }
   });
 
  // Changing state of CheckAll checkbox 
  $(".checkbox").click(function(){
 
    if($(".checkbox").length == $(".checkbox:checked").length) {
      $("#checkall").prop("checked", true);
    } else {
      $("#checkall").prop("checked", false);
    }

  });
});
</script>