<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
//Ex
	if(isset($_GET['paramsmissing']))
	{
		$msg = "Offline Lead Creation  Failed. Enter Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Offline Lead Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Offline Lead Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
 Create  Lead<!--<marquee>This Module is Under Maintenance. Please work on this module after sometime.</marquee>-->
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/offline_sitechat_details.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Full Name</label>
	<input type="text" class="form-control" id="full_name" name="full_name" required>
	
</div>
<div class="col-lg-6">
	<label class="">Email:</label>
	<input type="text" class="form-control" id="email"  name="email" >
	
</div>
</div>


<div class="form-group row">
<div class="col-lg-4">
	<label>City:</label>
	<input type="text" class="form-control" id="city" name="city">
	
</div>
<div class="col-lg-4">
	<label class="">Phone:</label>
	<input type="text" class="form-control" id="Phone"  name="Phone" required>
	
</div>
<div class="col-lg-4">
	<label class="">Origin:</label>
	<select class="form-control ktselect2" id="lead_origin" name="lead_origin">
				<option value="1">Site Chat</option>
				<option value="2">Sugar CRM</option>
				<option value="3">Whatsapp</option>
				<option value="4">Facebook Ads</option>
				<option value="5">Intagram Ads</option>
				<option value="6">LINKEDIN Ads</option>
				<option value="7">Youtube Ads</option>
				<option value="8">Google Business</option>
				<option value="9">Outbound</option>
				<option value="10">Walk-in</option>
				<option value="11">reference</option>
				<option value="12">Google Reference Form</option>
				
    </select>
</div>

</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Course 2:</label>
	 <select class="form-control ktselect2" id="course2"  name="course2" >
					<option value="0">--None--</option>
					<option value="ACCA">ACCA</option>
					<option value="Accounts">Accounts</option>
					<option value="ACFM">ACFM</option>
					<option value="Advance Excel">Advance Excel</option>
					<option value="AIFS">AIFS</option>
					<option value="Analytics">Analytics</option>
					<option value="Big Data & Hadoop">Big Data & Hadoop</option>
					<option value="Business Accounting & Taxation">Business Accounting & Taxation</option>
					<option value="CFA">CFA</option>
					<option value="CFA - Level I">CFA - Level I</option>
					<option value="CFA - Level II">CFA - Level II</option>
					<option value="CFA - Level III">CFA - Level III</option>
					<option value="CFA - Level I PT">CFA - Level I PT</option>
					<option value="Charting Techniques">Charting Techniques</option>
					<option value="CMA">CMA</option>
					<option value="CPA">CPA</option>
					<option value="Data Analysis">Data Analysis</option>
					<option value="Data Science">Data Science</option>
					<option value="Demand & Supply Analysis">Demand & Supply Analysis</option>
					<option value="Digital Marketing">Digital Marketing</option>
					<option value="EA">EA</option>		
					<option value="Finance">Finance</option>
					<option value="Financial Modeling">Financial Modeling</option>		
					<option value="FRM">FRM</option>
					<option value="Google Analytics & Affiliate Marketing">Google Analytics & Affiliate Marketing</option>
					<option value="GST">GST</option>
					<option value="Healthcare">Healthcare</option>
					<option value="HR Analytics">HR Analytics</option>
					<option value="Human Resources">Human Resources</option>
					<option value="IFRS">IFRS</option>
					<option value="Investment Banking">Investment Banking</option>
					<option value="Machine Learning">Machine Learning</option>
					<option value="Marketing">Marketing</option>
					<option value="Mobile, E-Mail & Affiliate Marketing">Mobile, E-Mail & Affiliate Marketing</option>
					<option value="Other">Other</option>
					<option value="Other Accounts">Other Accounts</option>
					<option value="Other Analytics">Other Analytics</option>
					<option value="Other Finance">Other Finance</option>
					<option value="Other Marketing">Other Marketing</option>
					<option value="PGCFR">PGCFR</option>
					<option value="Predictive Business Analytics">Predictive Business Analytics</option>
					<option value="Search Engine Optimization (SEO)">Search Engine Optimization (SEO)</option>
					<option value="Social Media Marketing">Social Media Marketing</option>
					<option value="USMLE">USMLE</option>
					<option value="USMLE-Clinical Rotations">USMLE-Clinical Rotations</option>
					<option value="USMLE-Step 1">USMLE-Step 1</option>
					<option value="USMLE-Step 2 CK">USMLE-Step 2 CK</option>
					<option value="USMLE-Step 2 CS">USMLE-Step 2 CS</option>
					<option value="VBA & MACROS">VBA & MACROS</option>		
					</select>
	
</div>
<div class="col-lg-6">
<label>Location:</label>
	<select class="form-control ktselect2" id="location" name="location">
				<option value="0">--None--</option>
				<option value="Ahmedabad">Ahmedabad</option>
				<option value="Bangalore">Bangalore</option>
				<option value="Chennai">Chennai</option>
				<option value="Delhi">Delhi</option>
				<option value="Hyderabad">Hyderabad</option>
				<option value="Mumbai">Mumbai</option>
				<option value="Pune">Pune</option>
				<option value="Other">Other</option>

				</select>
	
</div>
</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-8">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>