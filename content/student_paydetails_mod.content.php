<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
     if(isset($_GET['mesg2']))
	{
		$msg = $_GET['mesg2'];
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['mesg']))
	{
		$msg = $_GET['mesg'];
		$altype = "warning";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['alreadyverified']))
	{
		$msg = "Already Verfied Transaction Details cannot be Modified .";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['duptransdate']))
	{
		$msg = "Same Transaction Date cannot be changed More than Once.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['duppaymode']))
	{
		$msg = "Same Payment Mode cannot be changed More than Once.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['duptransamt']))
	{
		$msg = "Same Transaction Amount cannot be changed More than Once.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Transaction Details change request cannot be processed. Mandatory Fields missing";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['excessamt']))
	{
		$msg = "Transaction Details change request cannot be processed. Excess Refund amount";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Transaction Details change request cannot be processed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Transaction Details change request  processed Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<!--begin::Portlet-->

<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Update Student Transaction Details Request
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-4">
<label>Account No:</label>
<input  class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text"  value=""/>
</div>
<div class="col-lg-4" style="padding-top:25px;">
<label></label>
<button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="RECSelect();">Get Details</button>
</div>
<div class="col-lg-4" >
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='form_handlers/student_paydetails_mod.php'>
	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Organization No. Transaction Details
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
							
						</a>
					</li>

				</ul>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_portlet_tab_1_1">
				<div class="form-group row">
				<div class="col-lg-6">
				<label>Student Name:</label>
				<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
				<input    class="form-control" name="course" id="course" style="width:100%;" type="text" hidden />
				<input    class="form-control" name="subcourse" id="subcourse" style="width:100%;" type="text" hidden />
				</div>
				<div class="col-lg-3">
				<label class="">Created On:</label>
				<input    class="form-control" name="createdon_1" id="createdon_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				<div class="col-lg-3">
				<label class="">Counselor Name:</label>
				<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Fees Paid.:</label>
				<input  class="form-control" name="paid_1" id="paid_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-6">
				<label class="">Balance Amount:</label>
				<input   class="form-control"  name="balance_1" id="balance_1"  style="width:100%;" type="text" readonly   />
				</div>
				</div>
		
				<div class="form-group row">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="">
				<thead>
				<tr >
	
				<th >Transactions</th>
				<th >Amount</th>
				<th >Date</th>
				<th >Mode</th>
				<th >Validated</th>
				<th >Transaction Id /Details</th>
				</tr>
                </thead>
				<tbody>
				<tr>
			
				<td>Trans. 1</td>
				<td ><input   class="form-control"  name="t_amt_1" id="t_amt_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_1" id="t_dat_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_1" id="t_mod_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_1" id="t_valid_1"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_1" id="t_tra_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Trans. 2</td>
				<td ><input   class="form-control"  name="t_amt_2" id="t_amt_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_2" id="t_dat_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_2" id="t_mod_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_2" id="t_valid_2"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_2" id="t_tra_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Trans. 3</td>
				<td ><input   class="form-control"  name="t_amt_3" id="t_amt_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_3" id="t_dat_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_3" id="t_mod_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_3" id="t_valid_3"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_3" id="t_tra_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Trans. 4</td>
				<td ><input   class="form-control"  name="t_amt_4" id="t_amt_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_4" id="t_dat_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_4" id="t_mod_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_4" id="t_valid_4"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_4" id="t_tra_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Trans. 5</td>
				<td ><input   class="form-control"  name="t_amt_5" id="t_amt_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_5" id="t_dat_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_5" id="t_mod_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_5" id="t_valid_5"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_5" id="t_tra_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Trans. 6</td>
				<td ><input   class="form-control"  name="t_amt_6" id="t_amt_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_6" id="t_dat_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_6" id="t_mod_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_6" id="t_valid_6"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_6" id="t_tra_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				</tbody>
				</table>
				</div>
				
				
				<div class="form-group row">
				<div class="col-lg-3">
				<div class="kt-checkbox-inline">
				<label class="kt-checkbox" style="padding-left:0px">Transaction no.:</label>
				<span class="form-text text-muted"><select class="form-control" id="field_no" name="field_no">
				<option value='0'>Select</option>
				<option value='1'>Transaction 1</option>
				<option value='2'>Transaction 2</option>
				<option value='3'>Transaction 3</option>
				<option value='4'>Transaction 4</option>
				<option value='5'>Transaction 5</option>
				<option value='6'>Transaction 6</option>
				</select></span>
				</div>
				</div>
				
				<div class="col-lg-3">
				<div class="kt-checkbox-inline">
				<label class="kt-checkbox">
				<input  class="form-control" name="field_type[]" id="field_type"   type="checkbox" value="Transaction_date"   />Transaction Date
				<span></span>
				</label>
				<span class="form-text text-muted"><input  class="form-control" name="trans_date" id="trans_date" style="width:100%;" type="date"    /></span>
				</div>
	            </div>
				<div class="col-lg-3">
				<div class="kt-checkbox-inline">
				<label class="kt-checkbox">
				<input  class="form-control" name="field_type[]" id="field_type"   type="checkbox" value="Mode_of_payment"   /> Payment Mode
				<span></span>
				</label>
				<span class="form-text text-muted">
				<select class="form-control" id="mode_of_payment" name="mode_of_payment" style="width:100%;" >
				<option value="">Select</option>
				<option value="Online">Online</option>
				<option value="Cash">Cash</option>
				<option value="Wire Transfer">Wire Transfer</option>
				<option value="Cheque">Cheque</option>
				<option value="Card">Card</option>
				<option value="Purchase Order">Purchase Order</option>
				<option value="Tata Capital">Tata Capital</option>
				<option value="Paytm">Paytm</option>
				<option value="Neev Finance">Neev Finance</option>
				<option value="Liqui Loans">Liqui Loans</option>
				</select></span>
				</div>
	            </div>
				<div class="col-lg-3">
				<div class="kt-checkbox-inline">
				<label class="kt-checkbox">
				<input  class="form-control" name="field_type[]" id="field_type"   type="checkbox" value="transaction_amount"  />Transaction Amount
				<span></span>
				</label>
				<span class="form-text text-muted">
				<input  class="form-control" name="trans_amt" id="trans_amt"   type="text"  style="width:100%;"   /></span>
				</div>
	            </div>
				</div>
				
				
				
				
				<div class="form-group row">
				<div class="col-lg-12">
				<label class="">Reason for Transaction Details update <font color="red">(Max characters 500)</font>:</label>
				<textarea type="text" class="form-control"  name="comment_1" id="comment_1" rows="4" required maxlength="500" ></textarea>
				</div>
				</div>
				
				
				</div>
					
				
			</div>
		</div>
	</div>

	<!--end::Portlet-->
	<div class="kt-portlet__foot kt-portlet__foot--fit-x">
	<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="approve" name="approve" type="button" class="btn btn-success" onClick="checkvalues(this.form);">Submit Request</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
	</div>
	</div>
</form>
</div>

<?php
?>
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function RECSelect()
{
	var val = document.getElementById("accountno_1").value;
	if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			
			document.getElementById('recs_1').value = output["account_no"];
			document.getElementById('paid_1').value = output["Paid"];
			document.getElementById('balance_1').value = output["Balance"];

			document.getElementById('studentname_1').value = output["studentname"];
			document.getElementById('assignedto_1').value = output["assigned_user"];
			document.getElementById('createdon_1').value = output["createdat"];
			document.getElementById('course').value = output["course"];
            document.getElementById('subcourse').value = output["subcourse"];



			document.getElementById('t_amt_1').value = output["t_amt_1"];
			document.getElementById('t_amt_2').value = output["t_amt_2"];
			document.getElementById('t_amt_3').value = output["t_amt_3"];
			document.getElementById('t_amt_4').value = output["t_amt_4"];
			document.getElementById('t_amt_5').value = output["t_amt_5"];
			document.getElementById('t_amt_6').value = output["t_amt_6"];
			document.getElementById('t_dat_1').value = output["t_dat_1"];
			document.getElementById('t_dat_2').value = output["t_dat_2"];
			document.getElementById('t_dat_3').value = output["t_dat_3"];
			document.getElementById('t_dat_4').value = output["t_dat_4"];
			document.getElementById('t_dat_5').value = output["t_dat_5"];
			document.getElementById('t_dat_6').value = output["t_dat_6"];
			document.getElementById('t_mod_1').value = output["t_mod_1"];
			document.getElementById('t_mod_2').value = output["t_mod_2"];
			document.getElementById('t_mod_3').value = output["t_mod_3"];
			document.getElementById('t_mod_4').value = output["t_mod_4"];
			document.getElementById('t_mod_5').value = output["t_mod_5"];
			document.getElementById('t_mod_6').value = output["t_mod_6"];
			document.getElementById('t_tra_1').value = output["t_tra_1"];
			document.getElementById('t_tra_2').value = output["t_tra_2"];
			document.getElementById('t_tra_3').value = output["t_tra_3"];	
			document.getElementById('t_tra_4').value = output["t_tra_4"];
			document.getElementById('t_tra_5').value = output["t_tra_5"];
			document.getElementById('t_tra_6').value = output["t_tra_6"];
			
			var valid1="";
			var valid2="";
			var valid3="";
			var valid4="";
			var valid5="";
			var valid6="";
			if(output["t_valid_1"]==1)
			{
			valid1="Yes";	
			}
			else
			{
			valid1="NA";		
			}
			
			if(output["t_valid_2"]==1)
			{
			valid2="Yes";	
			}
			else
			{
			valid2="NA";		
			}
			
			if(output["t_valid_3"]==1)
			{
			valid3="Yes";	
			}
			else
			{
			valid3="NA";		
			}
			
			if(output["t_valid_4"]==1)
			{
			valid4="Yes";	
			}
			else
			{
			valid4="NA";		
			}
			if(output["t_valid_5"]==1)
			{
			valid5="Yes";	
			}
			else
			{
			valid5="NA";		
			}
			if(output["t_valid_6"]==1)
			{
			valid6="Yes";	
			}
			else
			{
			valid6="NA";		
			}
			
			document.getElementById('t_valid_1').value = valid1;
			document.getElementById('t_valid_2').value = valid2;
			document.getElementById('t_valid_3').value = valid3;
			document.getElementById('t_valid_4').value = valid4;
			document.getElementById('t_valid_5').value = valid5;
			document.getElementById('t_valid_6').value = valid6;

			
			}
	}

	xmlhttp.open('POST', 'ajax/fetch_refunddetails.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}



function checkvalues(form)
{
	var trans_date=document.getElementById('trans_date').value;
	var paymode=document.getElementById('mode_of_payment').value;
	var trans_amount=document.getElementById('trans_amt').value;
	var field_no=document.getElementById('field_no').value;
	var inpfields = document.getElementsByTagName('input');
	
	var r = confirm("Confirmation: Are you sure to Change the Transaction Details?");
    if (r == true) {
	
	if((field_no!="0")&&((trans_date=="")&&(paymode=="")&&(trans_amount=="")))
	{
	    alert("Please Select to Modify!");
		submit_button_clicked = '';
        return false;
	}
		
	for(var i=0;i<inpfields.length;i++){	
	if(inpfields[i].type =='checkbox' && inpfields[i].checked == true) 
	{
	if(inpfields[i].value=='Transaction_date')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
        document.getElementById('field_no').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(trans_date=="")
		{
		alert("You Have Selected Transaction Date Option To Modify. Please Enter A New valid Transaction Date!");
        document.getElementById('trans_date').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(trans_date!="")
		{
        var check=document.getElementById('t_dat_'+ field_no).value; 

        if(trans_date==check)
		{			
		alert("You cannot select an existing Transaction Date.Please Enter A New valid Transaction Date!");
        document.getElementById('trans_date').focus;
		submit_button_clicked = '';
        return false;
		}
		
		}
	}
	else if(inpfields[i].value=='Mode_of_payment')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
        document.getElementById('field_no').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(paymode=="")
		{
		alert("You Have Selected Paymode Option To Modify. Please Select a New Payment Mode!");
        document.getElementById('mode_of_payment').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(paymode!="")
		{

        var check=document.getElementById('t_mod_'+ field_no).value; 

        if(paymode==check)
		{			
		alert("You cannot select an existing Paymode.Please Enter A New valid Paymode!");
        document.getElementById('mode_of_payment').focus;
		submit_button_clicked = '';
        return false;
		}
		
		}
	}
    else if(inpfields[i].value=='transaction_amount')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
        document.getElementById('field_no').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(trans_amount=="")
		{
		alert("You Have Selected Transaction Amount Option To Modify. Please Enter New Transaction Amount!");
        document.getElementById('trans_amt').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(trans_amount!="")
		{
		
        var check=document.getElementById('t_tra_'+ field_no).value; 

        if(trans_amount==check)
		{			
		alert("You cannot enter an existing Transaction amount.Please Enter A New valid Transaction Amount!");
        document.getElementById('trans_amt').focus;
		submit_button_clicked = '';
        return false;
		}
		
		}
	}
	}
	else if(inpfields[i].type =='checkbox' && inpfields[i].checked == false) 
	{
	if(inpfields[i].value=='Transaction_date')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
        document.getElementById('field_no').focus;
		submit_button_clicked = '';
        return false;		
		}
		else if(trans_date!="")
		{
		alert("You Have Entered A New valid Transaction Date.Please Select Transaction Date Option To Modify!");
		submit_button_clicked = '';
        return false;		
		}
	}
	else if(inpfields[i].value=='Mode_of_payment')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
		submit_button_clicked = '';
        document.getElementById('field_no').focus;
        return false;		
		}
		else if(paymode!="")
		{
		alert("You Have Selected a New Paymode.Please Select Paymode Option To Modify!");
		submit_button_clicked = '';
        return false;		
		}
	}
    else if(inpfields[i].value=='transaction_amount')
	{
		if(field_no=="0")
		{
		alert("Please a Select a Transaction No. to Modify!");
		submit_button_clicked = '';
        document.getElementById('field_no').focus;
        return false;		
		}
		else if(trans_amount!="")
		{
		alert("You Have Entered New Transaction Amount .Please Select Transaction Amount Option To Modify!");
		submit_button_clicked = '';
        return false;		
		}
	}
	}
	}
	
	form.submit();
	}
}
</script>