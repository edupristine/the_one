<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update Batch Start Date
</h3>
</div>
</div>
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h5 class="kt-portlet__head-title">
	Send Mail:-
</h5>
<div class="kt-form__actions">
<a onClick="Nochangebatch();" class="btn btn-warning" style="color:#ffffff;">No Change Batch Schedule</a>
<a onClick="Tentativebatchplan();" class="btn btn-info" style="color:#ffffff;">Tentative Batch Plan</a>
</div>

<div class="kt-form__actions">
<table>
<tr>
<td style="padding-left:50px;">
<select class="form-control"  id="month" name="month" >
<option value="1">January </option>
<option value="2">February </option>
<option value="3">March </option>
<option value="4">April </option>
<option value="5">May </option>
<option value="6">June </option>
<option value="7">July </option>
<option value="8">August </option>
<option value="9">September </option>
<option value="10">October </option>
<option value="11">November </option>
<option value="12">December </option>
</select>
</td>
<td>
<select class="form-control"  id="year" name="year" >
<option value="2019">2019 </option>
<option value="2020">2020 </option>
<option value="2021">2021 </option>
<option value="2022">2022 </option>
<option value="2023">2023 </option>
<option value="2024">2024 </option>
<option value="2025">2025 </option>
</select>
</td>
<td>
<a onClick="Existingbatches();" class="btn btn-info" style="color:#ffffff;">Monthly Batch Plan</a>
</td>
</tr>
</table>
</div>
</div>
</div>



<form class="kt-form kt-form--label-right" method='POST' action=''>

<div class="kt-portlet__body">


<?php
$query_harry_codes = "SELECT id,code FROM workshops WHERE code not like '%SEM%' AND deleteflag=0 ORDER BY id desc";
$result_harry_code = Select($query_harry_codes,$harry);	
?>

	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:2%;">#</th>
				<th style="width:16%;">Registered Batch Code</th>
				<th style="width:12%;">Status</th>
				<th style="width:12%;">Old Start Date</th>
				<th style="width:12%;">New Start Date</th>
				<th style="width:24%;">Comment</th>
				<th style="width:10%;"><center>Action</center></th>
			</tr>
		</thead>
		<tbody id="rows_div">
			<tr id="row_id_1">
				<td class="text-center"><input readonly name="sr_1" id="sr_1" style="width:100%;padding:2px;border:0;" type="text" value="1"/></td>
				
				<td  class="text-left">
					<select class="chosen-select"  id="newregcode_1" name="newregcode_1" style="width:100%;padding:2px;" onChange="RECSelect(this.value,'row_1');">
						<option value="">Select </option>
						<?php
						foreach($result_harry_code['rows'] as $codes)
						{
							echo "<option value='".$codes['code']."'>".$codes['code']."</option>";
							
						}
						?>
					</select>
				</td>
				<td  class="text-left">
					<select class="form-control"  id="status_1" name="status_1" style="width:100%;padding:2px;">
						<option value="1">Postponed </option>
						<option value="2">Cancelled </option>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" name="oldstartdate_1" id="oldstartdate_1" class="form-control" type="text"  value=""/></td>
				<td class="text-center"><input data-toggle="tooltip" name="newstartdate_1" id="newstartdate_1"  class="form-control" type="date" value = "<?php echo date('Y-m-d'); ?>"  />

     			<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;padding:2px;" type="text" hidden /></td>
				
				<td class="text-center"><input data-toggle="tooltip" name="comment_1" id="comment_1" class="form-control" type="text"  value=""/></td>
				<td id="prodel_1" name="prodel_1" class="text-center">
					<div class="btn-group">
						<a onClick="DelREC('row_id_1');" data-toggle="tooltip" title="Delete Record" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddREC();" data-toggle="tooltip" title="Add Record" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-4"></div>
		<div class="col-lg-3">
			<a onClick="SaveBatchstartdate();" class="btn btn-success" style="color:#ffffff;" >Update Batch Start Date</a>
			
		</div>
	
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>