<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "Student Validity already raised.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Student Validity cannot be processed. Mandatory Fields missing";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}

	elseif(isset($_GET['dberror']))
	{
		$msg = "Student Validity cannot be processed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Student Validity  extended Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<!--begin::Portlet-->

<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Validity Checker
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-4">
<label>Account No:</label>
<input  class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text"  value=""/>
</div>
<div class="col-lg-4" style="padding-top:25px;">
<label></label>
<button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="RECSelect();">Get Details</button>
</div>
<div class="col-lg-4" >
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' >
	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Details
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
							Organization Info
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_portlet_tab_1_1">
				<div class="form-group row">
				<div class="col-lg-3">
				<label>Student Name:</label>
				<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
				<input    class="form-control" name="accountid" id="accountid" style="width:100%;" type="text" hidden />
				
				</div>
				<div class="col-lg-3">
				<label class="">Counselor Name:</label>
				<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				<div class="col-lg-3">
				<label>Course :</label>
				<input    class="form-control" name="course" id="course" style="width:100%;" type="text"  value=""  />
				</div>
				<div class="col-lg-3">
				<label class="">Mobile Number:</label>
				<input   class="form-control" name="mobileno_1" id="mobileno_1"  style="width:100%;" type="text"  />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-2">
				<label class="">Org Created On:</label>
				<input   class="form-control" name="createdon_1" id="createdon_1"  style="width:100%;" type="text" readonly />
				</div>
				
				<div class="col-lg-2">
				<label>Batch Date :</label>
				<input    class="form-control" name="batchdate_1" id="batchdate_1" style="width:100%;" type="text"  value="" readonly />
			    </div>
				<div class="col-lg-2">
				<label class="">Batch Location:</label>
				<input   class="form-control" name="batchlocation_1" id="batchlocation_1"  style="width:100%;" type="text" readonly />
				</div>
	
	
	            <div class="col-lg-2">
				<label class="">Offered Price:</label>
				<input  class="form-control" name="offeredprice_1" id="offeredprice_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-2">
				<label class="">Fees Paid.:</label>
				<input  class="form-control" name="paid_1" id="paid_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-2">
				<label class="">Balance Amount:</label>
				<input   class="form-control"  name="balance_1" id="balance_1"  style="width:100%;" type="text" readonly   />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-12">
				<center>
				<div id="validity_display" name="validity_display">
				
				</div></center>
				</div>
				</div>
				
				
				</div>
				

			
				
			</div>
		</div>
	</div>

	<!--end::Portlet-->
	
</form>
</div>

<?php
?>
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function RECSelect()
{
	var val = document.getElementById("accountno_1").value;
	if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById('recs_1').value = output["account_no"];
			document.getElementById('accountid').value = output["accountid"];
			document.getElementById('offeredprice_1').value = output["offered_price"];
			document.getElementById('paid_1').value = output["Paid"];
			document.getElementById('balance_1').value = output["Balance"];
			document.getElementById('studentname_1').value = output["studentname"];
			document.getElementById('assignedto_1').value = output["assigned_user"];
			document.getElementById('course').value = output["course"];
			document.getElementById('mobileno_1').value = output["phone"];
			document.getElementById('batchdate_1').value = output["batch_date"];
			document.getElementById('createdon_1').value = output["createdat"];
			document.getElementById('batchlocation_1').value = output["batch_location"];
            document.getElementById("validity_display").innerHTML="";
			if(output["access_block"]==1)
			{
			document.getElementById("validity_display").innerHTML = "<html><img src='failed.jpg' height='100px'/><br><h3>Access Blocked for this student.<br>Incase of any discrepancies/ Issues, Kindly consult IT/Accounts/Support Team.</h3></html>";
			}
			else if(output["access_block"]==0)
			{
			document.getElementById("validity_display").innerHTML = "<html><img src='success.gif' height='100px' /><br><h3>Access granted</h3></html>";	
			}
         
			
		
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_student_orgdetails_validity.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}




</script>