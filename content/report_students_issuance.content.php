<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'report_students_issuance.php?reporttype=livestudents&filter_param='+filter_param;
	}
</script>
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$reporttype = get_get_value('reporttype');
	$filter_param = get_get_value('filter_param');
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($location_id == "")
	{
		$location_id = $_SESSION['U_LOCATION_ID'];
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = $_SESSION['U_LOCATION_ID'];
	}
	
	if($reporttype=="livestudents")
	{
	
	if($_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
	$query_prestudent = "SELECT distinct(si.student_id) FROM `issuance`si, `students` s  WHERE si.student_id=s.id  and si.issued_by IN (SELECT id FROM users WHERE loc_id = ".$location_id.") and  (s.student_name like '%".$filter_param."%' OR s.crm_id like '%".$filter_param."%' OR s.student_email like '%".$filter_param."%' OR s.student_contact like '%".$filter_param."%') ";
	}
	else
	{
	$query_prestudent = "SELECT distinct(si.student_id) FROM `issuance`si, `students` s where si.student_id=s.id and (s.student_name like '%".$filter_param."%' OR s.crm_id like '%".$filter_param."%' OR s.student_email like '%".$filter_param."%' OR s.student_contact like '%".$filter_param."%')   ";
	}
	
	$result_prestudent = Select($query_prestudent,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id;
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$center_locations = "SELECT id,loc_name from locations where loc_type = 'CENTER'";
	$result_loc_centers = Select($center_locations,$conn);
	
	if($_SESSION['U_LOCATION_TYPE'] != "CENTER" && $location_type_report == "CENTER")
	{
		$newrecord = array('id'=>$_SESSION['U_LOCATION_ID'],'loc_name'=>$_SESSION['U_LOCATION_NAME']);
		array_push($result_loc_centers['rows'],$newrecord);
	}
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											<?php echo $location_name_report; ?> SKU Issuance Report 
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
													<div class="col">
															<div class="dropdown" HIDDEN>
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Select Report
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																	
																				<a class="dropdown-item" href="report_students_issuance.php?reporttype=prestudents">Pre Ist June 2019</a>
															                    <!--<a class="dropdown-item" href="report_students_issuance.php?reporttype=livestudents" selected>Students Issuance</a>-->
																		
																	</div>
																
															</div>
														</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th width="20px">#</th>
												<th >Org No.</th>
												<th >Student Name</th>
												<th >Course</th>
												<th >Sub Course</th>
												<th>SKU's Issued</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_prestudent['rows'] as $student) 
											{	
												$incour = 0;
										           $studentname_query = "SELECT s.crm_id,s.student_name,c.course_name,sc.subcourse_name from students s,courses c,sub_courses sc where s.id = '".$student['student_id']."'  and s.course_id=c.id and s.subcourse_id=sc.id";
												   $result_studentname = Select($studentname_query,$conn);
										
												  ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $result_studentname['rows'][0]['crm_id']; ?></td>
													<td><?php echo $result_studentname['rows'][0]['student_name']; ?></td>
													<td><?php echo $result_studentname['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_studentname['rows'][0]['subcourse_name']; ?></td>
													<td>
													<table style="font-size:12px;">
													<tr>
													<td><b>Books</b></td><td><b>Raised On</b></td><td><b>Issued On</b><td><b>Couriered By</b><td><b>Tracking ID</b></td>
													</tr>
													<?php
													 $skudisplay_query = "SELECT s.sku_name,si.issue_date,si.created_at,cr.courier_provider,cr.courier_track_no from skus s,issuance si,student_couriers_records cr where s.id=si.sku_id and  si.courier_record_id=cr.id AND student_id = '".$student['student_id']."' and issued_by is not Null";
												   $result_skudisplay = Select($skudisplay_query,$conn);
									               $issued="";
												   foreach($result_skudisplay['rows'] as $skudisplay) 
											        {
													echo "<tr><td>".$skudisplay['sku_name']."</td><td>".$skudisplay['created_at']."</td><td>".$skudisplay['issue_date']."</td><td>".$skudisplay['courier_provider']."</td><td>".$skudisplay['courier_track_no']."</td></tr>";	
													}
													?>
												
													
													</table>
													</td>
													
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
<?php
	}
?>	