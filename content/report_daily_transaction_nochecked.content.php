<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'report_daily_transaction_nochecked_nochecked.php?param=0&reporttype=NOT_CHECKED&filter_param='+filter_param;
	}
</script>
<?php

setlocale(LC_MONETARY, 'en_IN');
$date_range = get_get_value('date_range');
if($date_range != ""){
    $drange = explode(" / ",$date_range);
    $sdate = date('Y-m-d',strtotime($drange[0]));
    $edate = date('Y-m-d',strtotime($drange[1]));
}else{
    $sdate = date("Y-m-d",strtotime("-1 days"));
    $edate = date("Y-m-d",strtotime("-1 days"));
    $date_range = $sdate." / ".$edate;
}
$reporttype = get_get_value('reporttype');
$modes = get_get_value('param');
$startdate=$sdate;
$enddate=$edate;

$reasons = array( 
    "Expected in 1-2 days" =>  "Expected in 1-2 days", 
    "On hold" =>  "On hold", 
    "Awaiting Confirmation" =>  "Awaiting Confirmation",
	"Wrong Entry" =>  "Wrong Entry", 
    "Pedencies" =>  "Pedencies", 
    "Cheque Bounce" =>  "Cheque Bounce",
	"Unknown" =>  "Unknown", 
    "Reject" =>  "Reject",
	"Payment Mode Mismatch" =>  "Payment Mode Mismatch"
	);

$modesdisplay = array( 
    "Cash" =>  "Cash", 
    "Card" =>  "Card",
	"Cheque" =>  "Cheque", 
    "Wire Transfer" =>  "Wire Transfer", 
    "Neev Finance" =>  "Neev Finance",
	"Liqui Loans" =>  "Liqui Loans", 
    "Online" =>  "Online",
	"Paytm" =>  "Paytm"
	);
		
$filter_param = get_get_value('filter_param');
if($filter_param == "")
{
if(($modes=='')||($modes=='0'))
{
$pay_query="select paiddate as `paiddated`,mode,trans_id,org_no,name,Assigned_to,course,city,trans_column,acc_id,asgn_councel  FROM (
SELECT DATE(sc.cf_1363) as paiddate,sc.cf_1305 as `mode`,cf_1307 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1307' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1363) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1365) as paiddate ,sc.cf_1315 as `mode` ,cf_1317 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1317' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1365) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1367) as paiddate,sc.cf_1325 as `mode` ,cf_1327 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1327' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1367) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1369) as paiddate, sc.cf_1335 as `mode`,cf_1337 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1337' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1369) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1371) as paiddate, sc.cf_1345 as `mode` ,cf_1347 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1347' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1371) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1373) as paiddate, sc.cf_1355 as `mode` ,cf_1357 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1357' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1373) BETWEEN '".$startdate."' AND '".$enddate."'
AND ce.smownerid != 3920 ) x";
}
else
{
$pay_query="select paiddate as `paiddated`,mode,trans_id,org_no,name,Assigned_to,course,city,trans_column,acc_id,asgn_councel  FROM (
SELECT DATE(sc.cf_1363) as paiddate,sc.cf_1305 as `mode`,cf_1307 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1307' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1363) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1305 = '".$modes."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1365) as paiddate ,sc.cf_1315 as `mode` ,cf_1317 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1317' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1365) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1315 = '".$modes."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1367) as paiddate,sc.cf_1325 as `mode` ,cf_1327 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1327' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1367) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1325 = '".$modes."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1369) as paiddate, sc.cf_1335 as `mode`,cf_1337 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1337' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1369) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1335 = '".$modes."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1371) as paiddate, sc.cf_1345 as `mode` ,cf_1347 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1347' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1371) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1345 = '".$modes."'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1373) as paiddate, sc.cf_1355 as `mode` ,cf_1357 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to' ,'cf_1357' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
DATE(sc.cf_1373) BETWEEN '".$startdate."' AND '".$enddate."'
AND sc.cf_1355 = '".$modes."'
AND ce.smownerid != 3920 ) x";	
	
}
}
else
{
$pay_query="select paiddate as `paiddated`,mode,trans_id,org_no,name,Assigned_to,course,city,trans_column,acc_id,asgn_councel  FROM (
SELECT DATE(sc.cf_1363) as paiddate,sc.cf_1305 as `mode`,cf_1307 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1307' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1307 like '%".$filter_param."%') 
)
AND cf_1303!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1365) as paiddate ,sc.cf_1315 as `mode` ,cf_1317 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1317' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1317 like '%".$filter_param."%') 
)
AND cf_1313!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1367) as paiddate,sc.cf_1325 as `mode` ,cf_1327 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1327' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1327 like '%".$filter_param."%') 
)
AND cf_1323!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1369) as paiddate, sc.cf_1335 as `mode`,cf_1337 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1337' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1337 like '%".$filter_param."%') 
)
AND cf_1333!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1371) as paiddate, sc.cf_1345 as `mode` ,cf_1347 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1347' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1347 like '%".$filter_param."%') 
)
AND cf_1343!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 
UNION
SELECT DATE(sc.cf_1373) as paiddate, sc.cf_1355 as `mode` ,cf_1357 as `trans_id`,ac.account_no  as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1357' as 'trans_column',ac.accountid as `acc_id`,
ce.smownerid as `asgn_councel`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
INNER JOIN vtiger_users vu ON vu.id = ce.smownerid
WHERE
((ac.accountname like '%".$filter_param."%') or
(ac.account_no like '%".$filter_param."%') or 
(sc.cf_918 like '%".$filter_param."%') or
(cf_1357 like '%".$filter_param."%') 
)
AND cf_1353!=''
AND ce.createdtime >='2019-01-01'
AND ce.smownerid != 3920 ) x";
}

$result_pay = mysqli_query($conn1,$pay_query);
while($resultpayment = mysqli_fetch_assoc($result_pay)) {
    $payments[] = $resultpayment;
}




$modez="SELECT cf_1305 FROM vtiger_cf_1305";
$result_mode = mysqli_query($conn1,$modez);
while($modess = mysqli_fetch_assoc($result_mode)) {
    $modecrm[] = $modess;
}
?>
<script>
	function get_batches()
	{
		var date_range = document.getElementById('date_range').value;
		var param = document.getElementById('kt_select2_3').value;
		var reporttype = document.getElementById('amountpaidcheck').value;
		
		window.location = 'report_daily_transaction_nochecked.php?date_range='+date_range+'&param='+param+'&reporttype='+reporttype;
	}
	
	function get_export()
	{
		var date_range = document.getElementById('date_range').value;
		var param = document.getElementById('kt_select2_3').value;
		var reporttype = document.getElementById('amountpaidcheck').value;
		window.location = 'content/dailytransaction_exportdata.php?date_range='+date_range+'&param='+param+'&reporttype='+reporttype;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet">
<div class="form-group row"style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
</div>
<div class="col-lg-2">
<div class="">
	<label class="">Mode</label>
				<select class="form-control kt-select2" id="kt_select2_3" name="kt_select2_3">
				
				<option value="0" >All</option>
				
				<?php

				foreach($modesdisplay as $modesdisp)
				{
				if($modesdisp==$modes)	
				{
				?>
				<option value="<?php echo $modesdisp;?>" Selected><?php echo $modesdisp;?></option>
				<?php
				}
				else
				{
				?>
				<option value="<?php echo $modesdisp;?>" ><?php echo $modesdisp;?></option>
				<?php
				}															
				}
				?>
			</select>
</div>			
	</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_batches();" type="button" class="btn btn-primary form-control">Get Report</button>
	</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_export();" type="button" class="btn btn-primary form-control">Export</button>
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button type="button" class="btn btn-primary form-control" data-toggle="modal" data-target="#dailyreceived">Received Amount</button>
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Daily Transaction Report(Not Checked)
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
													<div class="col">
															
															
															<select class="form-control" id="amountpaidcheck" name="amountpaidcheck">
															
															
															
															
															<?php
															if($reporttype=='ALL')
															{
															?>
														   <option value="NOT_CHECKED" >Not Checked</option>
															<option value="ALL" Selected>All</option>
															<?php
															}
															?>
															<?php
															if($reporttype=='YES')
															{
															?>
															<option value="NOT_CHECKED" >Not Checked</option>
															<option value="ALL" >All</option>
															<?php
															}
															?>
															<?php
															if($reporttype=='NO')
															{
															?>
															<option value="NOT_CHECKED" >Not Checked</option>
															<option value="ALL" >All</option>
															<?php
															}
															?>
															<?php
															if($reporttype=='NOT_CHECKED')
															{
															?>
															<option value="NOT_CHECKED" Selected>Not Checked</option>
															<?php
															}
															?>

														    </select>
														</div>
												
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr >
												<th >#</th>
												<th >Name</th>
												<th >Org Name</th>
												<th >Course</th>
												<th >City</th>
												<th >Assigned To</th>
												<th >Amount</th>
												<th >Transaction Date </th>
												<th >Mode</th>
												<th >Transaction Id</th>
												<th >Amount Received</th>
												<th ><center>Action</center></th>
							                </tr>
											
										</thead>
										<tbody>
										<?php
											$i = 1;
											$totalpaid=0;
											foreach($payments as $pay) 
											{
												
												$uniq=$pay['org_no'].'_'.$i;
												$querypay="select paid as `paid` FROM (
												SELECT sc.cf_1303 as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1307='".$pay['trans_id']."'
										        AND ce.smownerid != 3920 
												UNION
												SELECT sc.cf_1313 as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1317='".$pay['trans_id']."'
												AND ce.smownerid != 3920 
												UNION
												SELECT sc.cf_1323 as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1327='".$pay['trans_id']."'
												AND ce.smownerid != 3920 
												UNION
												SELECT sc.cf_1333 as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1337='".$pay['trans_id']."'
												AND ce.smownerid != 3920 
												UNION
												SELECT sc.cf_1343 as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1347='".$pay['trans_id']."'
												AND ce.smownerid != 3920 
												UNION
												SELECT sc.cf_1353 as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$pay['paiddated']."'
												AND ac.account_no='".$pay['org_no']."'
												AND sc.cf_1357='".$pay['trans_id']."'
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqp = mysqli_query($conn1,$querypay);
												$qrpay = mysqli_fetch_assoc($resultqp);
												
											
												
												$reporttype = get_get_value('reporttype');
												if ($reporttype=='ALL')
												{
												$trans_id = $pay['trans_id'];
												$newtrans = $pay['trans_column'];
												//$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'  AND  `paiddated`='".$pay['paiddated']."' AND  `transaction_id`='".$newtrans."' ";
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											  
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $pay['name']; ?><input hidden id="name_<?php echo $uniq; ?>" name="name_<?php echo $uniq; ?>" value="<?php echo $pay['name']; ?>"></td>
												    <td><?php echo $pay['org_no']; ?><input hidden id="org_no_<?php echo $uniq; ?>" name="org_no_<?php echo $uniq; ?>" value="<?php echo $pay['org_no']; ?>"></td>
													<td><?php echo $pay['course']; ?><input hidden id="course_<?php echo $uniq; ?>" name="course_<?php echo $uniq; ?>" value="<?php echo $pay['course']; ?>"></td>
													<td><?php echo $pay['city']; ?><input hidden id="city_<?php echo $uniq; ?>" name="city_<?php echo $uniq; ?>" value="<?php echo $pay['city']; ?>"></td>
													<td><?php echo $pay['Assigned_to']; ?><input hidden id="assigned_to_<?php echo $uniq; ?>" name="assigned_to_<?php echo $uniq; ?>" value="<?php echo $pay['Assigned_to']; ?>">
													<input hidden id="assigned_<?php echo $uniq; ?>" name="assigned_<?php echo $uniq; ?>" value="<?php echo $pay['asgn_councel']; ?>"></td>
													<td><?php echo $qrpay['paid']; ?><input hidden id="paid_<?php echo $uniq; ?>" name="paid_<?php echo $uniq; ?>" value="<?php echo $qrpay['paid']; ?>"></td>
													<td><input data-toggle="tooltip"name="paiddated_<?php echo $uniq; ?>" id="paiddated_<?php echo $uniq; ?>"  class="form-control" type="date" style="border:0px ;" value = "<?php 
													if($pay['paiddated']!='')
													{
													echo $pay['paiddated'];	
													}
													else
													{
													echo date('Y-m-d'); 
													}
													?>" onchange="updatetrans('<?php echo $uniq; ?>',1);"  /></td>
													<td><select  id="mode_<?php echo $uniq; ?>" name="mode_<?php echo $uniq; ?>" onchange="updatetrans('<?php echo $uniq; ?>',2);">
													<option value=''>Select</option>
													<?php
													foreach($modecrm as $modec)
													{
													if ($modec['cf_1305']==$pay['mode'])
													{
													
													echo "<option value='".$modec['cf_1305']."' Selected>".$modec['cf_1305']."</option>";
                                                    
													}
                                                    else
													{
													echo "<option value='".$modec['cf_1305']."' >".$modec['cf_1305']."</option>";
                                                   
													}														
													}
													?>
													</select>
													</td>
													<td><?php echo $pay['trans_id']; ?><input hidden id="trans_id_<?php echo $uniq; ?>" name="trans_id_<?php echo $uniq; ?>" value="<?php echo $trans_id; ?>"><input hidden id="trans_column_<?php echo $uniq; ?>" name="trans_column_<?php echo $uniq; ?>" value="<?php echo $pay['trans_column']; ?>">
													<input hidden id="acc_id_<?php echo $uniq; ?>" name="acc_id_<?php echo $uniq; ?>" value="<?php echo $pay['acc_id']; ?>">
													</td>
													<td>
													<?php
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                     echo "Not Checked";
													}
													else
													{
														$trans_id = $pay['trans_id'];
														$newtrans = $pay['trans_column'];
														$select_recy="SELECT if(amountpaid = 1,'YES','NO') as `paidcheck` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'  AND  `trans_column`='".$newtrans."' ";
												        $result_recy = Select($select_recy,$conn,"student_paydetails");
														echo $result_recy['rows'][0]['paidcheck'];
													}
													?>
													</td>
													<td><center>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#studentpay_<?php echo $uniq; ?>">Update Comments</button>
													
													</center></td>
												</tr>
										<?php
                                           	$totalpaid=$totalpaid+$qrpay['paid'];												
											$i++;
												}
												
												if ($reporttype=='YES')
												{
												$trans_id = $pay['trans_id'];	
												$newtrans = $pay['trans_column'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'  AND    `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                   
													}
													else
													{
														$trans_id = $pay['trans_id'];
														$newtrans = $pay['trans_column'];
														$select_recy="SELECT if(amountpaid = 1,'YES','NO') as `paidcheck` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' and amountpaid=1 ";
												        $result_recy = Select($select_recy,$conn,"student_paydetails");
														$var_check= $result_recy['rows'][0]['paidcheck'];
													}
													
											     if($var_check=='YES')
												 {
													  $trans_id = $pay['trans_id'];
													  $newtrans = $pay['trans_column'];
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $pay['name']; ?><input hidden id="name_<?php echo $uniq; ?>" name="name_<?php echo $uniq; ?>" value="<?php echo $pay['name']; ?>"></td>
												    <td><?php echo $uniq; ?><input hidden id="org_no_<?php echo $uniq; ?>" name="org_no_<?php echo $uniq; ?>" value="<?php echo $pay['org_no']; ?>"></td>
													<td><?php echo $pay['course']; ?><input hidden id="course_<?php echo $uniq; ?>" name="course_<?php echo $uniq; ?>" value="<?php echo $pay['course']; ?>"></td>
													<td><?php echo $pay['city']; ?><input hidden id="city_<?php echo $uniq; ?>" name="city_<?php echo $uniq; ?>" value="<?php echo $pay['city']; ?>"></td>
													<td><?php echo $pay['Assigned_to']; ?><input hidden id="assigned_to_<?php echo $uniq; ?>" name="assigned_to_<?php echo $uniq; ?>" value="<?php echo $pay['Assigned_to']; ?>">
													<input hidden id="assigned_<?php echo $uniq; ?>" name="assigned_<?php echo $uniq; ?>" value="<?php echo $pay['asgn_councel']; ?>">
													</td>
													<td><?php echo $qrpay['paid']; ?><input hidden id="paid_<?php echo $uniq; ?>" name="paid_<?php echo $uniq; ?>" value="<?php echo $qrpay['paid']; ?>"></td>
													<td><input data-toggle="tooltip"name="paiddated_<?php echo $uniq; ?>" id="paiddated_<?php echo $uniq; ?>"  class="form-control" type="date" style="border:0px ;" value = "<?php 
													if($pay['paiddated']!='')
													{
													echo $pay['paiddated'];	
													}
													else
													{
													echo date('Y-m-d'); 
													}
													?>" onchange="updatetrans('<?php echo $uniq; ?>',1);"  /></td>
													<td><select  id="mode_<?php echo $uniq; ?>" name="mode_<?php echo $uniq; ?>" onchange="updatetrans('<?php echo $uniq; ?>',2);">
													<option value=''>Select</option>
													<?php
													foreach($modecrm as $modec)
													{
													if ($modec['cf_1305']==$pay['mode'])
													{
													
													echo "<option value='".$modec['cf_1305']."' Selected>".$modec['cf_1305']."</option>";
                                                    
													}
                                                    else
													{
													echo "<option value='".$modec['cf_1305']."' >".$modec['cf_1305']."</option>";
                                                   
													}														
													}
													?>
													</select></td>
													<td><?php echo $pay['trans_id']; ?><input hidden id="trans_id_<?php echo $uniq; ?>" name="trans_id_<?php echo $uniq; ?>" value="<?php echo $trans_id; ?>">
													<input hidden id="trans_column_<?php echo $uniq; ?>" name="trans_column_<?php echo $uniq; ?>" value="<?php echo $pay['trans_column']; ?>">
													<input hidden id="acc_id_<?php echo $uniq; ?>" name="acc_id_<?php echo $uniq; ?>" value="<?php echo $pay['acc_id']; ?>">
													</td>
													<td>
													<?php echo "YES"; ?>
													</td>
													<td><center>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#studentpay_<?php echo $uniq; ?>">Update Comments</button>
													
													</center></td>
												</tr>
										<?php
                                           	$totalpaid=$totalpaid+$qrpay['paid'];												
											$i++;
												 }
												}
												
												if ($reporttype=='NO')
												{
												$trans_id = $pay['trans_id'];	
												$newtrans = $pay['trans_column'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                   
													}
													else
													{
														$trans_id = $pay['trans_id'];
														$newtrans = $pay['trans_column'];
														$select_recy="SELECT if(amountpaid = 1,'YES','NO') as `paidcheck` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' and amountpaid=0 ";
												        $result_recy = Select($select_recy,$conn,"student_paydetails");
														$var_check= $result_recy['rows'][0]['paidcheck'];
													}
													
											     if($var_check=='NO')
												 {    $trans_id = $pay['trans_id'];
													  $newtrans = $pay['trans_column'];
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $pay['name']; ?><input hidden id="name_<?php echo $uniq; ?>" name="name_<?php echo $uniq; ?>" value="<?php echo $pay['name']; ?>"></td>
												    <td><?php echo $pay['org_no']; ?><input hidden id="org_no_<?php echo $uniq; ?>" name="org_no_<?php echo $uniq; ?>" value="<?php echo $pay['org_no']; ?>"></td>
													<td><?php echo $pay['course']; ?><input hidden id="course_<?php echo $uniq; ?>" name="course_<?php echo $uniq; ?>" value="<?php echo $pay['course']; ?>"></td>
													<td><?php echo $pay['city']; ?><input hidden id="city_<?php echo $uniq; ?>" name="city_<?php echo $uniq; ?>" value="<?php echo $pay['city']; ?>"></td>
													<td><?php echo $pay['Assigned_to']; ?><input hidden id="assigned_to_<?php echo $uniq; ?>" name="assigned_to_<?php echo $uniq; ?>" value="<?php echo $pay['Assigned_to']; ?>">
													<input hidden id="assigned_<?php echo $uniq; ?>" name="assigned_<?php echo $uniq; ?>" value="<?php echo $pay['asgn_councel']; ?>">
													</td>
													<td><?php echo $qrpay['paid']; ?><input hidden id="paid_<?php echo $uniq; ?>" name="paid_<?php echo $uniq; ?>" value="<?php echo $qrpay['paid']; ?>"></td>
													<td><input data-toggle="tooltip"name="paiddated_<?php echo $uniq; ?>" id="paiddated_<?php echo $uniq; ?>"  class="form-control" type="date" style="border:0px ;" value = "<?php 
													if($pay['paiddated']!='')
													{
													echo $pay['paiddated'];	
													}
													else
													{
													echo date('Y-m-d'); 
													}
													?>" onchange="updatetrans('<?php echo $uniq; ?>',1);"  /></td>
													<td><select  id="mode_<?php echo $uniq; ?>" name="mode_<?php echo $uniq; ?>" onchange="updatetrans('<?php echo $uniq; ?>',2);">
													<option value=''>Select</option>
													<?php
													foreach($modecrm as $modec)
													{
													if ($modec['cf_1305']==$pay['mode'])
													{
													
													echo "<option value='".$modec['cf_1305']."' Selected>".$modec['cf_1305']."</option>";
                                                    
													}
                                                    else
													{
													echo "<option value='".$modec['cf_1305']."' >".$modec['cf_1305']."</option>";
                                                   
													}														
													}
													?>
													</select></td>
													<td><?php echo $pay['trans_id']; ?><input hidden id="trans_id_<?php echo $uniq; ?>" name="trans_id_<?php echo $uniq; ?>" value="<?php echo $trans_id; ?>">
													<input hidden id="trans_column_<?php echo $uniq; ?>" name="trans_column_<?php echo $uniq; ?>" value="<?php echo $pay['trans_column']; ?>">
													<input hidden id="acc_id_<?php echo $uniq; ?>" name="acc_id_<?php echo $uniq; ?>" value="<?php echo $pay['acc_id']; ?>">
													</td>
													<td>
													<?php echo "NO"; ?>
													</td>
													<td><center>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#studentpay_<?php echo $uniq; ?>">Update Comments</button>
													
													</center></td>
												</tr>
										<?php
                                           	$totalpaid=$totalpaid+$qrpay['paid'];												
											$i++;
												 }
												}
												
												if ($reporttype=='NOT_CHECKED')
												{
												$trans_id = $pay['trans_id'];	
												$newtrans = $pay['trans_column'];
												
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                    $var_check='NOT_CHECKED';
													}
													
													
											     if($var_check=='NOT_CHECKED')
												 {
													$trans_id = $pay['trans_id'];
													$newtrans = $pay['trans_column'];
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $pay['name']; ?><input hidden id="name_<?php echo $uniq; ?>" name="name_<?php echo $uniq; ?>" value="<?php echo $pay['name']; ?>"></td>
												    <td><?php echo $pay['org_no']; ?><input hidden id="org_no_<?php echo $uniq; ?>" name="org_no_<?php echo $uniq; ?>" value="<?php echo $pay['org_no']; ?>"></td>
													<td><?php echo $pay['course']; ?><input hidden id="course_<?php echo $uniq; ?>" name="course_<?php echo $uniq; ?>" value="<?php echo $pay['course']; ?>"></td>
													<td><?php echo $pay['city']; ?><input hidden id="city_<?php echo $uniq; ?>" name="city_<?php echo $uniq; ?>" value="<?php echo $pay['city']; ?>"></td>
													<td><?php echo $pay['Assigned_to']; ?><input hidden id="assigned_to_<?php echo $uniq; ?>" name="assigned_to_<?php echo $uniq; ?>" value="<?php echo $pay['Assigned_to']; ?>">
													<input hidden id="assigned_<?php echo $uniq; ?>" name="assigned_<?php echo $uniq; ?>" value="<?php echo $pay['asgn_councel']; ?>">
													</td>
													<td><?php echo $qrpay['paid']; ?><input hidden id="paid_<?php echo $uniq; ?>" name="paid_<?php echo $uniq; ?>" value="<?php echo $qrpay['paid']; ?>"></td>
													<td><input data-toggle="tooltip"name="paiddated_<?php echo $uniq; ?>" id="paiddated_<?php echo $uniq; ?>"  class="form-control" type="date" style="border:0px ;" value = "<?php 
													if($pay['paiddated']!='')
													{
													echo $pay['paiddated'];	
													}
													else
													{
													echo date('Y-m-d'); 
													}
													?>" onchange="updatetrans('<?php echo $uniq; ?>',1);"  /></td>
													<td><select  id="mode_<?php echo $uniq; ?>" name="mode_<?php echo $uniq; ?>" onchange="updatetrans('<?php echo $uniq; ?>',2);">
													<option value=''>Select</option>
													<?php
													foreach($modecrm as $modec)
													{
													if ($modec['cf_1305']==$pay['mode'])
													{
													
													echo "<option value='".$modec['cf_1305']."' Selected>".$modec['cf_1305']."</option>";
                                                    
													}
                                                    else
													{
													echo "<option value='".$modec['cf_1305']."' >".$modec['cf_1305']."</option>";
                                                   
													}														
													}
													?>
													</select></td>
													<td><?php echo $pay['trans_id']; ?><input hidden id="trans_id_<?php echo $uniq; ?>" name="trans_id_<?php echo $uniq; ?>" value="<?php echo $trans_id; ?>">
													<input hidden id="trans_column_<?php echo $uniq; ?>" name="trans_column_<?php echo $uniq; ?>" value="<?php echo $pay['trans_column']; ?>">
													<input hidden id="acc_id_<?php echo $uniq; ?>" name="acc_id_<?php echo $uniq; ?>" value="<?php echo $pay['acc_id']; ?>">
													</td>
													<td>
													<?php echo "Not Checked"; ?>
													</td>
													<td><center>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#studentpay_<?php echo $uniq; ?>">Update Comments</button>
													
													</center></td>
												</tr>
										<?php
                                           	$totalpaid=$totalpaid+$qrpay['paid'];												
											$i++;
												 }
												}
												
											}
										?>
										<tr>
													<td colspan=11 style="text-align:right;"><strong>Total Amount Received : </strong></td>
													<td><center>
													<strong><?php echo $totalpaid;?></strong>
													</center></td>
												</tr>									
										</tbody>
									</table>
									
									<?php 
											$i=1;
											foreach($payments as $pay) 
											{	
											
											
											    $uniq=$pay['org_no'].'_'.$i;
											    $trans_id = $pay['trans_id'];
												$newtrans = $pay['trans_column'];
												$select_rec="SELECT comments,amountpaid,expecteddate,reasons FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_rec = Select($select_rec,$conn,"student_paydetails");
											   
											    $reporttype = get_get_value('reporttype');
												if ($reporttype=='ALL')
												{
												$trans_id = $pay['trans_id'];	
												$newtrans = $pay['trans_column'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
												?>
												
												<div class="modal fade" id="studentpay_<?php echo $uniq; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Update Payment related Comments: ".$pay['name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
															<table class="table table-striped- table-bordered table-hover table-checkable"  id= "<?php echo $uniq;?>">
															<tr>
															<td>Amount Received
															</td>
															<td>
															<select class="form-control" id="amountpaid_<?php echo $uniq;?>" name="amountpaid_<?php echo $uniq;?>">
																<?php
															if($result_rec['rows'][0]['amountpaid']==1)
															{
															?>
															<option value="1" Selected>Yes</option>
															<option value="0" >No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==0)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" Selected>No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==2)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" >No</option>
															<option value="2" Selected >Rejected</option>
															<?php
															}
															?>
															
															
															</select>
															</td>
                                                            </tr>
															
															<tr>
															<td>Expected Date
															</td>
															<td>
															<input data-toggle="tooltip" name="expecteddate_<?php echo $uniq;?>" id="expecteddate_<?php echo $uniq;?>"  class="form-control" type="date" value = "<?php 
															if($result_rec['rows'][0]['expecteddate']!='')
															{
															echo $result_rec['rows'][0]['expecteddate'];	
															}
															else
															{
															echo date('Y-m-d'); 
															}
															?>" />
															</td>
                                                            </tr>
															
															<tr>
															<td>If Not Reasons
															</td>
															<td>
															<select class="form-control" id="reasons_<?php echo $uniq;?>" name="reasons_<?php echo $uniq;?>">
															<option value="0" >Select</option>
                                                            
															<?php
															
															foreach($reasons as $reason)
															{
															if($reason==$result_rec['rows'][0]['reasons'])	
															{
															?>
															<option value="<?php echo $reason;?>" Selected><?php echo $reason;?></option>
															<?php
															}
															else
															{
                                                            ?>
															<option value="<?php echo $reason;?>" ><?php echo $reason;?></option>
															<?php
                                                            }															
															}
															?>
															
														
                                                            </select>
															</td>
                                                            </tr>
															
															<tr>
															<td colspan=2>
															Comments
															<textarea id="comments_<?php echo $uniq;?>" name="comments_<?php echo $uniq;?>" cols="60" rows="5"  style="border:none;"><?php echo $result_rec['rows'][0]['comments'];?></textarea>
															</div>

															</td>
															</tr>

															</table>		

																	
																	
															</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="UpdateDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-primary">Update Details</button>
																<button onClick="NoRecDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-warning">Send No Transaction Mail</button>
															</div>
														</div>
													</div>
											   </div>
												<?php
												$i++;
												}
												
												if ($reporttype=='YES')
												{
												$newtrans = $pay['trans_column'];
												$trans_id = $pay['trans_id'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                   
													}
													else
													{
														$newtrans = $pay['trans_column'];
														$trans_id = $pay['trans_id'];
														$select_recy="SELECT if(amountpaid = 1,'YES','NO') as `paidcheck` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' and amountpaid=1 ";
												        $result_recy = Select($select_recy,$conn,"student_paydetails");
														$var_check= $result_recy['rows'][0]['paidcheck'];
													}
													
											     if($var_check=='YES')
												 {
													  $newtrans = $pay['trans_column'];
													  $trans_id = $pay['trans_id'];
												
											    
												
												?>
                                                <div class="modal fade" id="studentpay_<?php echo $uniq; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Update Payment related Comments: ".$pay['name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
															<table class="table table-striped- table-bordered table-hover table-checkable"  id= "<?php echo $uniq;?>">
															<tr>
															<td>Amount Received
															</td>
															<td>
															<select class="form-control" id="amountpaid_<?php echo $uniq;?>" name="amountpaid_<?php echo $uniq;?>">
															<?php
															if($result_rec['rows'][0]['amountpaid']==1)
															{
															?>
															<option value="1" Selected>Yes</option>
															<option value="0" >No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==0)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" Selected>No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==2)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" >No</option>
															<option value="2" Selected >Rejected</option>
															<?php
															}
															?>
															
															</select>
															</td>
                                                            </tr>
															
															<tr>
															<td>Expected Date
															</td>
															<td>
															<input data-toggle="tooltip" name="expecteddate_<?php echo $uniq;?>" id="expecteddate_<?php echo $uniq;?>"  class="form-control" type="date" value = "<?php 
															if($result_rec['rows'][0]['expecteddate']!='')
															{
															echo $result_rec['rows'][0]['expecteddate'];	
															}
															else
															{
															echo date('Y-m-d'); 
															}
															?>"  />
															</td>
                                                            </tr>
															
															<tr>
															<td>If Not Reasons
															</td>
															<td>
															<select class="form-control" id="reasons_<?php echo $uniq;?>" name="reasons_<?php echo $uniq;?>">
															<option value="0" >Select</option>
                                                            
															<?php
															
															foreach($reasons as $reason)
															{
															if($reason==$result_rec['rows'][0]['reasons'])	
															{
															?>
															<option value="<?php echo $reason;?>" Selected><?php echo $reason;?></option>
															<?php
															}
															else
															{
                                                            ?>
															<option value="<?php echo $reason;?>" ><?php echo $reason;?></option>
															<?php
                                                            }															
															}
															?>
															
														
                                                            </select>
															</td>
                                                            </tr>
															
															<tr>
															<td colspan=2>
															Comments
															<textarea id="comments_<?php echo $uniq;?>" name="comments_<?php echo $uniq;?>" cols="60" rows="5"  style="border:none;"><?php echo $result_rec['rows'][0]['comments'];?></textarea>
															</div>

															</td>
															</tr>

															</table>		

																	
																	
															</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="UpdateDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-primary">Update Details</button>
																<button onClick="NoRecDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-warning">Send No Transaction Mail</button>
															</div>
														</div>
													</div>
											   
												</div>
												<?php
												$i++;
												 }
												}
												
												if ($reporttype=='NO')
												{
												$newtrans = $pay['trans_column'];	
												$trans_id = $pay['trans_id'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'  AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                   
													}
													else
													{
														$newtrans = $pay['trans_column'];
														$trans_id = $pay['trans_id'];
														$select_recy="SELECT if(amountpaid = 1,'YES','NO') as `paidcheck` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'  AND  `trans_column`='".$newtrans."' and amountpaid=0 ";
												        $result_recy = Select($select_recy,$conn,"student_paydetails");
														$var_check= $result_recy['rows'][0]['paidcheck'];
													}
													
											     if($var_check=='NO')
												 {
													  $newtrans = $pay['trans_column'];
													  $trans_id = $pay['trans_id'];
												 
												 ?>
												 
												 <div class="modal fade" id="studentpay_<?php echo $uniq; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Update Payment related Comments: ".$pay['name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
															<table class="table table-striped- table-bordered table-hover table-checkable"  id= "<?php echo $uniq;?>">
															<tr>
															<td>Amount Received
															</td>
															<td>
															<select class="form-control" id="amountpaid_<?php echo $uniq;?>" name="amountpaid_<?php echo $uniq;?>">
																<?php
															if($result_rec['rows'][0]['amountpaid']==1)
															{
															?>
															<option value="1" Selected>Yes</option>
															<option value="0" >No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==0)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" Selected>No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==2)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" >No</option>
															<option value="2" Selected >Rejected</option>
															<?php
															}
															?>
															
															</select>
															</td>
                                                            </tr>
															
															<tr>
															<td>Expected Date
															</td>
															<td>
															<input data-toggle="tooltip" name="expecteddate_<?php echo $uniq;?>" id="expecteddate_<?php echo $uniq;?>"  class="form-control" type="date" value = "<?php 
															if($result_rec['rows'][0]['expecteddate']!='')
															{
															echo $result_rec['rows'][0]['expecteddate'];	
															}
															else
															{
															echo date('Y-m-d'); 
															}
															?>"  />
															</td>
                                                            </tr>
															
															<tr>
															<td>If Not Reasons
															</td>
															<td>
															<select class="form-control" id="reasons_<?php echo $uniq;?>" name="reasons_<?php echo $uniq;?>">
															<option value="0" >Select</option>
                                                            
															<?php
															
															foreach($reasons as $reason)
															{
															if($reason==$result_rec['rows'][0]['reasons'])	
															{
															?>
															<option value="<?php echo $reason;?>" Selected><?php echo $reason;?></option>
															<?php
															}
															else
															{
                                                            ?>
															<option value="<?php echo $reason;?>" ><?php echo $reason;?></option>
															<?php
                                                            }															
															}
															?>
															
														
                                                            </select>
															</td>
                                                            </tr>
															
															<tr>
															<td colspan=2>
															Comments
															<textarea id="comments_<?php echo $uniq;?>" name="comments_<?php echo $uniq;?>" cols="60" rows="5"  style="border:none;"><?php echo $result_rec['rows'][0]['comments'];?></textarea>
															</div>

															</td>
															</tr>

															</table>		

																	
																	
															</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="NoRecDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-warning">Send No Transaction Mail</button>
																<button onClick="UpdateDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-primary">Update Details</button>
															</div>
														</div>
													</div>
													</div>
											     <?php
												 $i++;
												 }
												}
												
												
												if ($reporttype=='NOT_CHECKED')
												{
												$newtrans = $pay['trans_column'];
												$trans_id = $pay['trans_id'];
												$select_recx="SELECT count(org_no) as `org_count` FROM student_paydetails  WHERE `org_no`='".$pay['org_no']."'   AND  `trans_column`='".$newtrans."' ";
												$result_recx = Select($select_recx,$conn,"student_paydetails");
											   
											        $var_check='';
													if($result_recx['rows'][0]['org_count']==0)
													{
                                                    $var_check='NOT_CHECKED';
													}
													
													
											     if($var_check=='NOT_CHECKED')
												 {
													$newtrans = $pay['trans_column'];
													$trans_id = $pay['trans_id'];
													 ?>
													 <div class="modal fade" id="studentpay_<?php echo $uniq; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Update Payment related Comments: ".$pay['name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
															<table class="table table-striped- table-bordered table-hover table-checkable"  id= "<?php echo $uniq;?>">
															<tr>
															<td>Amount Received
															</td>
															<td>
															<select class="form-control" id="amountpaid_<?php echo $uniq;?>" name="amountpaid_<?php echo $uniq;?>">
																<?php
															if($result_rec['rows'][0]['amountpaid']==1)
															{
															?>
															<option value="1" Selected>Yes</option>
															<option value="0" >No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==0)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" Selected>No</option>
															<option value="2" >Rejected</option>
															<?php
															}
															?>
															<?php
															if($result_rec['rows'][0]['amountpaid']==2)
															{
															?>
															<option value="1" >Yes</option>
															<option value="0" >No</option>
															<option value="2" Selected >Rejected</option>
															<?php
															}
															?>
															
															
															</select>
															</td>
                                                            </tr>
															
															<tr>
															<td>Expected Date
															</td>
															<td>
															<input data-toggle="tooltip" name="expecteddate_<?php echo $uniq;?>" id="expecteddate_<?php echo $uniq;?>"  class="form-control" type="date" value = "<?php 
															if($result_rec['rows'][0]['expecteddate']!='')
															{
															echo $result_rec['rows'][0]['expecteddate'];	
															}
															else
															{
															echo date('Y-m-d'); 
															}
															?>"  />
															</td>
                                                            </tr>
															
															<tr>
															<td>If Not Reasons
															</td>
															<td>
															<select class="form-control" id="reasons_<?php echo $uniq;?>" name="reasons_<?php echo $uniq;?>">
															<option value="0" >Select</option>
                                                            
															<?php
															
															foreach($reasons as $reason)
															{
															if($reason==$result_rec['rows'][0]['reasons'])	
															{
															?>
															<option value="<?php echo $reason;?>" Selected><?php echo $reason;?></option>
															<?php
															}
															else
															{
                                                            ?>
															<option value="<?php echo $reason;?>" ><?php echo $reason;?></option>
															<?php
                                                            }															
															}
															?>
															
														
                                                            </select>
															</td>
                                                            </tr>
															
															<tr>
															<td colspan=2>
															Comments
															<textarea id="comments_<?php echo $uniq;?>" name="comments_<?php echo $uniq;?>" cols="60" rows="5"  style="border:none;"><?php echo $result_rec['rows'][0]['comments'];?></textarea>
															</div>

															</td>
															</tr>

															</table>		

																	
																	
															</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																
																<button onClick="UpdateDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-primary">Update Details</button>
																<button onClick="NoRecDetails('<?php echo $uniq; ?>');" type="button" class="btn btn-warning">Send No Transaction Mail</button>
															</div>
														</div>
													</div>
												</div>	
												<?php	 
												$i++;
												 }
												}												 
											}
											?>
											
											
											<div class="modal fade" id="dailyreceived" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Update Payments Date Wise"; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll " data-scroll="true" data-height="200"  style="height: 200px; overflow: hidden;">
															<table class="table table-striped- table-bordered table-hover table-checkable"  id= "pd">
															<tr>
															<td>Received Date
															</td>
															<td>
															<input data-toggle="tooltip" name="recvdate" id="recvdate"  class="form-control" type="date" value = ""  />
															</td>
                                                            </tr>
															
															<tr>
															<td>Amount Received
															</td>
															<td>
                                                            <input type="text" class="form-control" id="amtpaid" name="amtpaid" value = "">
															</td>
                                                            </tr>
															
															
														    </table>		

																	
																	
															</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="UpdateRecDetails();" type="button" class="btn btn-primary">Update Details</button>
															</div>
														</div>
													</div>
												
									<!--end: Datatable -->
								</div>
							</div>

</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function UpdateDetails(org_no)
{
    
	var orgn_no=document.getElementById("org_no_" + org_no).value;
	var amtpaid=document.getElementById("amountpaid_" + org_no).value;
	var expecteddate=document.getElementById("expecteddate_" + org_no).value;
	var reasons=document.getElementById("reasons_" + org_no).value;
	var comments=document.getElementById("comments_" + org_no).value;
	
	var course=document.getElementById("course_" + org_no).value;
	var city=document.getElementById("city_" + org_no).value;
	var Assigned_to=document.getElementById("assigned_to_" + org_no).value;
	var paid=document.getElementById("paid_" + org_no).value;
	
	var paiddated=document.getElementById("paiddated_" + org_no).value;
	var mode=document.getElementById("mode_" + org_no).value;
	var trans_id=document.getElementById("trans_id_" + org_no).value;
	var student_name=document.getElementById("name_" + org_no).value;
	var trans_column=document.getElementById("trans_column_" + org_no).value;
	var acc_id=document.getElementById("acc_id_" + org_no).value;
	
	
   	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Payment Details updated  Successfully.");
				submit_button_clicked = '';
				$('#studentpay_'+org_no).modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Pay Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Pay Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/update_pay_details.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");	
	xmlhttp.send('org_no='+fixEscape(orgn_no)+'&amtpaid='+fixEscape(amtpaid)+'&expecteddate='+fixEscape(expecteddate)+'&reasons='+fixEscape(reasons)+'&comments='+fixEscape(comments)+'&course='+fixEscape(course)+'&city='+fixEscape(city)+'&Assigned_to='+fixEscape(Assigned_to)+'&paid='+fixEscape(paid)+'&paiddated='+fixEscape(paiddated)+'&mode='+fixEscape(mode)+'&trans_id='+trans_id+'&student_name='+fixEscape(student_name)+'&trans_column='+fixEscape(trans_column)+'&acc_id='+fixEscape(acc_id));
	


}



function NoRecDetails(org_no)
{
    
	var orgn_no=document.getElementById("org_no_" + org_no).value;
	var amtpaid=document.getElementById("amountpaid_" + org_no).value;
	var expecteddate=document.getElementById("expecteddate_" + org_no).value;
	var reasons=document.getElementById("reasons_" + org_no).value;
	var comments=document.getElementById("comments_" + org_no).value;
	
	var course=document.getElementById("course_" + org_no).value;
	var city=document.getElementById("city_" + org_no).value;
	var Assigned_to=document.getElementById("assigned_to_" + org_no).value;
	var paid=document.getElementById("paid_" + org_no).value;
	
	var paiddated=document.getElementById("paiddated_" + org_no).value;
	var mode=document.getElementById("mode_" + org_no).value;
	var trans_id=document.getElementById("trans_id_" + org_no).value;
	var student_name=document.getElementById("name_" + org_no).value;
	var trans_column=document.getElementById("trans_column_" + org_no).value;
	var acc_id=document.getElementById("acc_id_" + org_no).value;
	var Assigned=document.getElementById("assigned_" + org_no).value;
	
	
	
   	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Payment Details updated  Successfully.");
				submit_button_clicked = '';
				$('#studentpay_'+org_no).modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Pay Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Pay Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	

	xmlhttp.open('POST', 'ajax/update_nopay_details.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");	
	
	
	
	xmlhttp.send('org_no='+fixEscape(orgn_no)+'&amtpaid='+fixEscape(amtpaid)+'&expecteddate='+fixEscape(expecteddate)+'&reasons='+fixEscape(reasons)+'&comments='+fixEscape(comments)+'&course='+fixEscape(course)+'&city='+fixEscape(city)+'&Assigned_to='+fixEscape(Assigned_to)+'&paid='+fixEscape(paid)+'&paiddated='+fixEscape(paiddated)+'&mode='+fixEscape(mode)+'&trans_id='+trans_id+'&student_name='+fixEscape(student_name)+'&trans_column='+fixEscape(trans_column)+'&acc_id='+fixEscape(acc_id)+'&assigned='+fixEscape(Assigned));
	


}


function UpdateRecDetails()
{
    
	var recvdate=document.getElementById("recvdate").value;
	var amtpaid=document.getElementById("amtpaid").value;
	

   	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Received Payment Details updated  Successfully.");
				submit_button_clicked = '';
				$('#dailyreceived').modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert(" Received Pay Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Received Pay Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/update_received_pay_details.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");	
	xmlhttp.send('amtpaid='+fixEscape(amtpaid)+'&recvdate='+fixEscape(recvdate));
	


}


function updatetrans(org_no,field)
{

	var orgn_no=document.getElementById("org_no_" + org_no).value;
	var amtpaid=document.getElementById("amountpaid_" + org_no).value;
	var expecteddate=document.getElementById("expecteddate_" + org_no).value;
	var reasons=document.getElementById("reasons_" + org_no).value;
	var comments=document.getElementById("comments_" + org_no).value;
	
	var course=document.getElementById("course_" + org_no).value;
	var city=document.getElementById("city_" + org_no).value;
	var Assigned_to=document.getElementById("assigned_to_" + org_no).value;
	var paid=document.getElementById("paid_" + org_no).value;
	
	var paiddated=document.getElementById("paiddated_" + org_no).value;
	var mode=document.getElementById("mode_" + org_no).value;
	var trans_id=document.getElementById("trans_id_" + org_no).value;
	var student_name=document.getElementById("name_" + org_no).value;
	var trans_column=document.getElementById("trans_column_" + org_no).value;
	var acc_id=document.getElementById("acc_id_" + org_no).value;
	
	
   	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Details updated  Successfully.");
				submit_button_clicked = '';
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert(" Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/updatetrans.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");	
	
	xmlhttp.send('org_no='+fixEscape(orgn_no)+'&field='+fixEscape(field)+'&expecteddate='+fixEscape(expecteddate)+'&reasons='+fixEscape(reasons)+'&comments='+fixEscape(comments)+'&course='+fixEscape(course)+'&city='+fixEscape(city)+'&Assigned_to='+fixEscape(Assigned_to)+'&paid='+fixEscape(paid)+'&paiddated='+fixEscape(paiddated)+'&mode='+fixEscape(mode)+'&trans_id='+trans_id+'&student_name='+fixEscape(student_name)+'&trans_column='+fixEscape(trans_column)+'&acc_id='+fixEscape(acc_id));
	
}

</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>