<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = '','?filter_param='+filter_param;
	}
</script>
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($course_id == "")
	{
		$course_id = "1";
	}
	if($location_id == "")
	{
		$location_id = "3";
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = "3";
	}
	
	$qrys="";
	if($course_id!='')
	{
		$qrys="and s.course_id = ".$course_id."";
	}
	elseif($course_id=='')
	{
		$qrys="and s.course_id in (SELECT course_id from sub_courses where rrf = 0)  or s.course_id in (3,5,12)";
	}
	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and (sb.rrf=0 or s.course_id = 5 or s.course_id = 3 or s.course_id = 12 ) and  sl.id = s.loc_id and s.loc_id = '".$location_id."' ".$qrys." ORDER BY s.student_name";
	
	}
	else
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and (sb.rrf=0 or s.course_id = 5 or s.course_id = 3 or s.course_id = 12 ) and  sl.id = s.loc_id and (student_name = '".$filter_param."' OR crm_id = '".$filter_param."' OR student_email = '".$filter_param."' OR student_contact = '".$filter_param."' OR subcourse_name = '".$filter_param."') ORDER BY s.student_name";
	}
	

	$result_allstudents = Select($query_allstudents,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$course_name = "SELECT course_name from courses where id = ".$course_id ."";
	$result_course_name = Select($course_name,$conn);
	$course_name_report = $result_course_name['rows'][0]['course_name'];
	
	$center_locations = "SELECT id,loc_name from locations where loc_type = 'CENTER'";
	$result_loc_centers = Select($center_locations,$conn);
	
	$course = "SELECT id,course_name from courses where id in (select course_id from sub_courses where rrf = 0) or id in (3,5,12) order by course_name ";
	$result_course = Select($course,$conn);
	
	/*if($_SESSION['U_LOCATION_TYPE'] != "CENTER" && $location_type_report == "CENTER")
	{
		$newrecord = array('id'=>$_SESSION['U_LOCATION_ID'],'loc_name'=>$_SESSION['U_LOCATION_NAME']);
		array_push($result_loc_centers['rows'],$newrecord);
	}*/
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet">
<div class="form-group row"style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Raise Requirement List <?php 
											
											echo "(".$location_name_report." - ".$course_name_report.")"; 
	                                       
											?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
																if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
																{
																?>
											
														<div class="col">
														<table><tr>
														<td>
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Courses
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_course['rows'] as $course)
																			{	
																				
																				?>
																				<a class="dropdown-item" href="raise_requirement_list.php?uid=<?php echo $user_id;?>&cid=<?php echo $course['id'];?>&lid=<?php echo $location_id;?>"><?php echo $course['course_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td><td>
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Centers
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_loc_centers['rows'] as $loc_center)
																			{	
																		?>
																				<a class="dropdown-item" href="raise_requirement_list.php?uid=<?php echo $user_id;?>&cid=<?php echo $course_id;?>&lid=<?php echo $loc_center['id'];?>"><?php echo $loc_center['loc_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td></tr></table>
														</div>
													<?php
																}
																?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name</th>
												<th >Student Contact</th>
												<th>Student Course</th>
												<th>Student Subcourse</th>
												<th>Requirement Raised</th>
												<th>Books Issued</th>
												<th>Couriered</th>
												<th>Courier on</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										        $locs = "SELECT * from locations where id = ".$student['loc_id'];
												$result_locs = Select($locs,$conn);
												
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td ><?php echo $student['student_contact']; ?></td>
													
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_subcourses['rows'][0]['subcourse_name']; ?></td>
													
													<td>
													<?php
													foreach($result_select_issue['rows'] as $each_issue)
																				{
																					$select_req="SELECT count(id) as counts FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_req,$conn,"issuance");
																							if($result_sel['rows'][0]['counts']!=0){
																								
																								
																								echo $each_issue['sku_name']."<br>";
																								
																							}
																					
																					
																				}
													?>
													</td>
													<td>
													<?php
													foreach($result_select_issue['rows'] as $each_issue)
																				{
																					$select_req="SELECT count(id) as counts FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_req,$conn,"issuance");
																					
																					
																					$select_status="SELECT issued_status  FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_status = Select($select_status,$conn,"issuance");
																							
																							
																							if($result_sel['rows'][0]['counts']!=0){
																								
																								if($result_status['rows'][0]['issued_status']==1)
																								{
																								echo $each_issue['sku_name']."<br>";
																								}
																								
																								
																							}
																					
																					
																				}
													?>
													</td>
											
													<td>
													<?php
													foreach($result_select_issue['rows'] as $each_issue)
													{
													$select_req="SELECT count(id) as counts FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
													$result_sel = Select($select_req,$conn,"issuance");


													$select_status="SELECT issued_status,courier_record_id  FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."' ";
													$result_status = Select($select_status,$conn,"issuance");


													if($result_sel['rows'][0]['counts']!=0){

													if($result_status['rows'][0]['courier_record_id']!='')
													{
													echo $each_issue['sku_name']."<br>";
													}


													}


													}
													?>
													</td>
												
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
											?>
											<div class="modal fade" id="student_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Books List For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="400">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['id'];?>">

																		<thead>
																																				<tr>
																		<td>Select Location
																		</td>
																		<td colspan=2>
																		<select id="raise_loc_<?php echo $student['id'];?>" name="raise_loc_<?php echo $student['id'];?>">
																		<?php 
																		if($student['loc_id']!=8)
																		{
																		?>
																		<option value="<?php echo $student['loc_id'];?>" selected><?php echo $student['loc_name'];?></option>
																		<?php
																		}
								                                        if($student['loc_id']==8)
																		{
																		?>
																			
																		<option value="8" >Others</option>
																	    <?php
																		
																		}
																		?>
																		</select>
																		</td>
																		</tr>
																			<tr>
																				<td>#</td>
																				<td>SKU Name</td>
																				<td>Add Requirement</td>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																							<?php 
																							$select_req="SELECT count(id) as counts FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_req,$conn,"issuance");
																							
																							$select_status="SELECT issued_status  FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_status = Select($select_status,$conn,"issuance");
																							
																							
																							if($result_sel['rows'][0]['counts']!=0){
																								
																								if($result_status['rows'][0]['issued_status']==1)
																								{
																								echo "Issued";
																								}
																								else{
																								echo "Requirement Raised";
																								}
																							}
										                                                     else{
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$each_issue['skuid']."|";
																							?>
																								<input type="checkbox" id="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" name="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" value="1">
																							<?php
																							 }
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																		</tbody>
																	</table>
																	<input hidden id="hidden_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Raisereq(<?php echo $student['id']; ?>,<?php echo $student['loc_id']; ?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												<?php	
											
											}
										?>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
											?>
													<div class="modal fade" id="address_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Enter Address For: ".$student['student_name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="200">
																	<textarea id="student_address_<?php echo $student['id']; ?>" name="student_address" cols="63" rows="10" ><?php echo $student['student_address'];?></textarea>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Updateadd(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Update Address</button>
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
										<!--For the courier to various locations-->
										
										
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
											?>
													<div class="modal fade" id="courier_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Request Courier for: ".$student['student_name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="230">
																    <table>
																	<tr>
																	
																	<td>
																		Select Sending Center : <select id="courier_loc_<?php echo $student['id'];?>" name="courier_loc_<?php echo $student['id'];?>">
																		<?php 
																		$location_courier = "SELECT id,loc_name from locations where id not in (2,8)";
																		$result_loc_courier = Select($location_courier,$conn);
																		
																		foreach($result_loc_courier['rows'] as $loc_courier)
																		{
																		if($loc_courier['id']==$student['loc_id'])
																		{
																		?>
																		<option value="<?php echo $loc_courier['id'];?>" selected><?php echo $loc_courier['loc_name'];?></option>
																		<?php
																		}
																		else
																		{
																		?>
																		<option value="<?php echo $loc_courier['id'];?>"><?php echo $loc_courier['loc_name'];?></option>
																		<?php
																		}
																		}
                                                                         ?>																		
																		
																		</select>
																    <td>
																	</tr>
																	<tr>
																	<td>
																	Student Address:
																	<textarea id="courier_address_<?php echo $student['id']; ?>" name="student_address" cols="63" rows="9" ><?php echo $student['student_address'];?></textarea>
																    <td>
																	</tr>
																	</table>
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="SendCourierRequest(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Send Courier Request</button>
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
										
										
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
											?>
													<div class="modal fade" id="couriersent_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Courier Request sent for: ".$student['student_name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="230">
																    <table>
																	<tr>
																	
																	<td>
																		Sending Center : <select id="courier_loc_<?php echo $student['id'];?>" name="courier_loc_<?php echo $student['id'];?>">
																		<?php 
																		$location_courier = "SELECT id,loc_name from locations  where id not in (2,8)";
																		$result_loc_courier = Select($location_courier,$conn);
																		
																		
																		
																		foreach($result_loc_courier['rows'] as $loc_courier)
																		{
																			
																		$sent_courier = "SELECT sending_center from student_couriers_request  where student_id=".$student['id']." ";
																		$result_sent_courier = Select($sent_courier,$conn);
																		$sendingcenter = $result_sent_courier['rows'][0]['sending_center'];
																		if($loc_courier['id']== $sendingcenter)
																		{
																		
																			
																		?>
																		<option value="<?php echo $loc_courier['id'];?>" selected><?php echo $loc_courier['loc_name'];?></option>
																		<?php
																		}
																		else
																		{
																		?>
																		<option value="<?php echo $loc_courier['id'];?>"><?php echo $loc_courier['loc_name'];?></option>
																		<?php
																		}
																		}
                                                                         ?>																		
																		
																		</select>
																    <td>
																	</tr>
																	<tr>
																	<td>
																	Student Address:
																	<textarea id="courier_address_<?php echo $student['id']; ?>" name="student_address" cols="63" rows="9" readonly ><?php echo $student['student_address'];?></textarea>
																    <td>
																	</tr>
																	</table>
																</div>
															</div>
															<div class="modal-footer">
															<?php
															$total_issuance = "SELECT count(id)as tot_iss from issuance  where student_id='".$student['id']."'";
															$result_total_iss = Select($total_issuance,$conn);
															$total_iss=$result_total_iss['rows'][0]['tot_iss'];
															
															$total_cour_req = "SELECT sum(total_records)as tot_req from student_couriers_request  where student_id='".$student['id']."'";
															$result_total_req = Select($total_cour_req,$conn);
															$total_req=$result_total_req['rows'][0]['tot_req'];
															
															if($total_iss>$total_req)
															{
															?>
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															<button onClick="SendCourierRequest(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Send Courier Request</button>
															<?php
															}
															
											                 
															?>
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_course = "SELECT id,param_type,param_value from help where param_type='".$result_course_name['rows'][0]['course_name']."' order by param_value";
										
												$result_select_course = Select($select_course,$conn);
											?>
											<div class="modal fade" id="studentcourses_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Batch Courses  Selected List For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="400">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['id'];?>">

																		<thead>
																																				
																			<tr>
																				<td>#</td>
																				<td>Batch Course Name</td>
																				<td>Select</td>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				foreach($result_select_course['rows'] as $course)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td><?php echo $i; ?></td>
																						<td><?php echo $course['param_value']; ?></td>
																						<td>
																							<?php 
																							$select_sel="SELECT count(id) as counts FROM students_batchcourses  WHERE `batchcourse_id`='".$course['id']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_sel,$conn,"students_batchcourses");
																							
																							if($result_sel['rows'][0]['counts']!=0){
																								echo "Selected";
																							}
										                                                     else{
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$course['id']."|";
																							?>
																								<input type="checkbox" id="cb_<?php echo $student['id']."_".$course['id']; ?>" name="cb_<?php echo $student['id']."_".$course['id']; ?>" value="1">
																							<?php
																							 }
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																		</tbody>
																	</table>
																	<input hidden id="hiddens_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Selectcourse(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												<?php	
											
											}
										?>
										
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function Raisereq(student_id,loc_id)
{
   
	var er = document.getElementById('raise_loc_'+student_id);
	var raise_loc = er.options[er.selectedIndex].value;
	var tb = "hidden_"+student_id;
	var cbs = document.getElementById(tb).value;
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);
	//alert(final_cbs_json);
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Books Requirement placed Successfully.");
				submit_button_clicked = '';
				$('#student_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Raise requirement FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Raise requirement FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/raise_requirement_list.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id)+'&raise_loc='+fixEscape(raise_loc));
}

function Updateadd(student_id)
{
	var upadd = document.getElementById('student_address_'+student_id).value;
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Student Addresss Updated  Successfully.");
				submit_button_clicked = '';
				$('#address_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Address Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Address Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_address.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('student_id='+fixEscape(student_id)+'&student_add='+fixEscape(upadd));
	
}





function SendCourierRequest(student_id)
{
	var upadd = document.getElementById('courier_address_'+student_id).value;
	var er = document.getElementById('courier_loc_'+student_id);
	var courierloc = er.options[er.selectedIndex].value;
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Student Courier Request Raised Successfully.");
				submit_button_clicked = '';
				$('#address_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Courier Request Creation Failed. Contact Administrator");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Courier Request Creation Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_courier_center.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('student_id='+fixEscape(student_id)+'&courier_add='+fixEscape(upadd)+'&courier_center='+fixEscape(courierloc));
	
}

function Selectcourse(student_id)
{
   
	var tb = "hiddens_"+student_id;
	var cbs = document.getElementById(tb).value;
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one Batch Course needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);
	//alert(final_cbs_json);
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Selected Courses List Updated Successfully.");
				submit_button_clicked = '';
				$('#studentcourses_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Batch Course List Update FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Batch Course List Update FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
    xmlhttp.open('POST', 'ajax/selected_batchcourses.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id));
}

</script>