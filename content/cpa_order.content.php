<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add CPA Order with Locations
</h3>
</div>
</div>
<?php
$query_ord = "SELECT max(ord_num) as last_ord,count(id) as count from orders";
$result_ord = Select($query_ord,$conn);
$max_ord = $result_ord['rows'][0]['last_ord'];
if($max_ord == "" || $max_ord == NULL )
{
	$max_ord = 1;
}
else
{
	$max_ord = $max_ord + 1;
}
?>
<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-2">
	<label>Order Number</label>
	<input type="text" class="form-control" id="ord_num" name="ord_num" value="<?php echo $max_ord; ?>" readonly>
</div>
<div class="col-lg-2">
	<label>Ref. Number</label>
	<input type="text" class="form-control" id="ref_num" name="ref_num" value="">
</div>
<div class="col-lg-3">
	<label class="">Order Date</label>
	<input type="date" class="form-control" id="ord_date"  name="ord_date" value = "<?php echo date('Y-m-d'); ?>">	
</div>
<div class="col-lg-3">
	<label class="">Exp. Rec. Date</label>
	<input type="date" class="form-control" id="exp_rec_date"  name="exp_rec_date" value = "">	
</div>
<div class="col-lg-2">
	<label>Order Amount</label>
	<input type="text" class="form-control" id="ord_amount" name="ord_amount" value="">
</div>
</div>
<?php
$query_vendor = "SELECT id,vendor_name from vendors";
$result_vendor = Select($query_vendor,$conn);

$query_skus = "select id,sku_name,sku_code from skus where sku_name like '%CPA%'";
$result_skus = Select($query_skus,$conn);
?>
<div class="form-group row">
<div class="col-lg-3">
	<label>Vendor</label>
	<select class="form-control kt-select2" id="kt_select2_1" name="vendor_name">
		<option value="0">Select Vendor</option>
		<?php
			foreach($result_vendor['rows'] as $vendor)
			{
				echo '<option value="'.$vendor['id'].'">'.$vendor['vendor_name'].'</option>';
			}
		?>
	</select>
</div>

<?php 
	$query_loc = "SELECT id,loc_name from locations where id not in (1,2,8) order by  loc_name ";
	$result_loc = Select($query_loc,$conn);
?>
<div class="col-lg-3">
	<label class="">Select Location</label>
	<div class="">
			<select class="form-control" id="location" name="location" >
				<?php 
					foreach($result_loc['rows'] as $Location)
					{
					
							echo '<option value="'.$Location['id'].'">'.$Location['loc_name'].'</option>';
					}
				?>
			</select>
		</div>
	</div>
<div class="col-lg-6">
	<label>Comments</label>
	<input type="text" class="form-control" id="comments" name="comments" value="">
</div>

</div>
	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:10%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:15%;">Quantity</th>
				<th style="width:15%;">Rate</th>
				<th style="width:15%;">Amount</th>
				<th style="width:10%;">Action</th>
			</tr>
		</thead>
		<tbody id="rows_div">
			<tr id="row_id_1">
				<td class="text-center"><input readonly name="sr_1" id="sr_1" style="width:100%;padding:2px;" type="text" value="1"/></td>
				
				<td  class="text-left">
					<select class="chosen-select" onChange="SKUSelect(this.value,'row_1');" id="skuname_1" name="skuname_1" style="width:100%;padding:2px;">
						<option value="">SKU Name</option>
						<?php
						foreach($result_skus['rows'] as $sku)
						{
							echo "<option value='".$sku['id']."'>".$sku['sku_name']." - ".$sku['sku_code']."</option>";
						}
						?>
					</select>
				</td>
				<td class="text-center"><input data-toggle="tooltip" title oninput="RefreshSKURow('row_id_1','qty');" name="qty_1" id="qty_1" placeholder="Qty" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_1','rate');" name="rate_1" id="rate_1" placeholder="Rate" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td class="text-center"><input oninput="RefreshSKURow('row_id_1','amt');" name="amt_1" id="amt_1" placeholder="Amt" style="width:100%;padding:2px;" type="text" value=""/></td>
				<td id="prodel_1" name="prodel_1" class="text-center">
					<div class="btn-group">
						<a onClick="DelSKU('row_id_1');" data-toggle="tooltip" title="Delete SKU" class="btn-sm btn-mini btn-danger"><i class="fa fa-times"></i></a>
						<a onClick="AddSKU();" data-toggle="tooltip" title="Add SKU" style="margin-left:5px;" class="btn-sm btn-mini btn-success"><i class="fa fa-plus"></i></a>
					</div>
				</td>
			</tr>
			
		</tbody>
	</table>

	<!--end: Datatable -->
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<a onClick="SaveOrder();" class="btn btn-success">Save</a>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>