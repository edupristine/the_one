<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php


	if(isset($_GET['paramsmissing']))
	{
		$msg = "Lead Upsert Failed. Fields are Mandatory.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Lead Upsert Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Upsert Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Upsert Lead
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" id="batch" name="batch" method='post' action='form_handlers/upsert_lead.php'>
<div class="kt-portlet__body">
<div class="form-group row">

<div class="col-lg-6">
    <label class="">External ID:</label>
	<input type="text" class="form-control" id="external_id"  name="external_id" value = "">
</div>

<div class="col-lg-6">
    <label class="">Lead Count:</label>
	<input type="text" class="form-control" id="lead_count"  name="lead_count" value = "">
	</div>
</div>
</div>



<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
		    <button type="submit" id="submit" name="submit" class="btn btn-success" >Upsert Lead</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>

