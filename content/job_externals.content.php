<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$qry1 = "SELECT * FROM student_applications order by id desc";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}

$master_loc = "SELECT id,location_name FROM job_locations order by location_name";
$res_locat = mysqli_query($edu,$master_loc);

	
	if(isset($_GET['dupname1']))
	{
		$msg = "Location Creation Failed. Location Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Location Creation Failed. Location already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Location Creation Failed. Location Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Location Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Location Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">


<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<i class="kt-font-brand flaticon2-line-chart"></i>Student Vistins to External Job Application List 
</h3>
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Date Range</label>
	<table>
	<tr>
	<td>
	<input type='text'  id='search_fromdate' class="datepicker" style="width:230px;"placeholder='From date' value=<?php echo date('m/01/Y');?> >
	</td>
	<td>
	<input type='text'  id='search_todate' class="datepicker" style="width:230px;" placeholder='To date' value=<?php echo date('m/d/Y');?>>
	</td>
	<td ><select  id="location" name="location" style="margin:5px;height:42px;" >
	<option value=''  >Select Location</option>
	<?php
	while($rl = mysqli_fetch_assoc($res_locat)) {
	echo "<option value=".$rl['id']."  >".$rl['location_name']."</option>";
	}
	?>
	</select>
	</td>
	<td>
	<select  id="job_type" name="job_type" style="margin:5px;height:42px;" >
	<option value=''>Select Job Type</option>
    <option value='1'>Internal</option>
	<option value='2'>External</option>
	</select>
	</td>
	<td>
	<select  id="vertical" name="vertical" style="margin:5px;height:42px;" >
	<option value=''>Select Vertical</option>
    <option value='1'>Accounts</option>
	<option value='2'>Finance</option>
	<option value='3'>Both Accounts & Finance</option>
	</select>
	</td>
	<td>
	<td>
	<input type='button' id="btn_search" value="Search">
	</td>
	</tr>
	</table>
</div>

</div>
<div class="kt-portlet">
<div style="padding-top:20px;padding-left:20px;padding-right:20px;">
  <table id='empTable' class='display dataTable'>
                <thead>
                <tr>		
				<!--<th >#</th>-->
				<th >Name</th>
				<th >Email</th>
				<th >Phone</th>
				<th >Course</th>
				<th >Job Location</th>
				<th >Company Name</th>
				<th >Crm No.</th>
				<th >Visited On</th>
				<th >Job Profile</th>
                </tr>
                </thead>  
		
    </table>
</div>
</div>
</div>

</div>

</div>
</div>
</div>

</div>
<?php 
foreach($rows as $locs){	
?>
<div class="modal fade" id="approveapp_<?php echo $locs['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
<div class="modal-dialog" role="">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLabel"><?php echo "Application Summary For: ".$locs['name']; ?></h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
</button>
</div>
<div class="modal-body">
<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
<table width="100%">
<tr>
<td style="padding: 6px 6px; "><strong>Org No.:</strong><?php echo $locs['org_no'];?>
</td>
<td style="padding: 6px 6px; "><strong>Email.:</strong><?php echo $locs['email'];?>
</td>
<td>
</td>
</tr>
<tr>
<td style="padding: 6px 6px; "><strong>Phone.:</strong><?php echo $locs['phone'];?>
</td>
<td style="padding: 6px 6px; "><strong>Course.:</strong><?php echo $locs['course'];?>
</td>
<td>
</td>
</tr>
<tr>
<td colspan=2><hr></hr>
</td>
<td>
</td>
</tr>
<tr>
<td style="padding: 6px 6px; " colspan=2><strong>Comments:</strong>
<textarea type="text" class="form-control"  name="comment_<?php echo $locs['id']; ?>" id="comment_<?php echo $locs['id']; ?>" rows="4" ></textarea>
</td>
<td>
</td>
</tr>
</table>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-info" onClick="appl_approval(<?php echo $locs['id']?>,1);" id="approve" name="approve"  >Approve </button>
<button type="button" class="btn btn-warning" onClick="appl_approval(<?php echo $locs['id']?>,0);" id="reject" name="reject"  >Reject</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<?php	
}
?><?php
?>





<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

$(document).ready(function(){

   // Datapicker 
   $( ".datepicker" ).datepicker({
      "dateFormat": "yy-mm-dd"
   });

   // DataTable
   var dataTable = $('#empTable').DataTable({
     'processing': true,
     'serverSide': true,
     'serverMethod': 'post',
     'searching': true, // Set false to Remove default Search Control
     'ajax': {
       'url':'ajax/externals_ajaxfile.php',
       'data': function(data){
          // Read values
          var from_date = $('#search_fromdate').val();
          var to_date = $('#search_todate').val();
		  var location = $('#location').val();
		  var job_type = $('#job_type').val();
          var vertical = $('#vertical').val();
          // Append to data
          data.searchByFromdate = from_date;
          data.searchByTodate = to_date;
		  if(job_type!='')
		  {
			data.job_type = job_type;  
		  }
		  
		  if(location!='')
		  {
			data.location = location;  
		  }
		  
		  if(vertical!='')
		  {
			data.vertical = vertical;  
		  }
       }
     },	

 
	'columns': [
	{ data: 'name' },
	{ data: 'email' },
    { data: 'phone' },
	{ data: 'course' },
	{ data: 'job_locations' },
	{ data: 'company_name' },
	{ data: 'org_no' },
	{ data: 'visited_on' },
	{ data: 'job_profile' },
	]
  });

  // Search button
  $('#btn_search').click(function(){
     dataTable.draw();
  });

});
</script>


<script>
submit_button_clicked = '';
function appl_approval(id,val)
{
	var comment = document.getElementById('comment_'+id).value;
	var status_x = val;
	
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Job Application Request processed Successfully.");
				submit_button_clicked = '';
				$('#approveapp_'+id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Job Application Request process Failed. Contact Administrator");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Job Application Request process Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_jobapplication_status.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('id='+id+'&status='+status_x+'&comment='+comment);
	
}
</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
	<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery UI CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- jQuery UI JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>