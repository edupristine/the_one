<?php
	$db_server='52.77.5.117';
	$db_username='liveuser';/*liveuser*/
	$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
    $conn1ex = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
	$con = new mysqli($db_server, $db_username, $db_password, 'ecademe_harry_new');
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($course_id == "")
	{
		$course_id = "1";
	}
	if($location_id == "")
	{
		$location_id = "3";
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = "3";
	}
	
	
	
				
$statuses="SELECT distinct(wd.status) as status FROM workshops_dates wd
				LEFT JOIN workshops w ON w.id = wd.workshop_id
				LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
        		WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'  and w.`status` != ' ' ORDER BY wd.status";
$result_status = mysqli_query($con,$statuses);
while($stats = mysqli_fetch_assoc($result_status)) {
$statsus[] = $stats;
}
	
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<i class="kt-font-brand flaticon2-line-chart"></i> Student Batch Scheduler
</h3>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<table>
	<tr>
	<td>
	<input type='text'  id='search_fromdate' class="datepicker" placeholder='From date' value=<?php echo date('m/d/Y');?> >
	</td>
	<td>
	<input type='text'  id='search_todate' class="datepicker" placeholder='To date' value=<?php echo date('m/d/Y');?>>
	</td>
	<td ><select  id="status" name="status" style="margin:5px;height:42px;" >
	<option value=''  >Select Batch Status</option>
	<?php
	foreach($statsus as $statuschk)
	{
    echo "<option value='".$statuschk['status']."' >".$statuschk['status']."</option>";
													
	}
	?>
	</select>
	</td>
	<td>
	<input type='button' id="btn_search" value="Search">
	</td>
	</tr>
	</table>
</div>

</div>
<div class="kt-portlet">
<div style="padding-top:20px;padding-left:20px;padding-right:20px;">
  <table id='empTable' class='display dataTable'>
                <thead>
                <tr>
				    <th>Batch Code</th>
					<th>Sch.Sent</th>
                    <th>Class Date</th>
				    <th>Day</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Faculty Name</th>
					<th>Course</th>
					<th>Start Time</th>
					<th>End Time</th>
					<th>Batch Start Date</th>
                </tr>
                </thead>  
		
    </table>
</div>
</div>
</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

$(document).ready(function(){

   // Datapicker 
   $( ".datepicker" ).datepicker({
      "dateFormat": "yy-mm-dd"
   });

   // DataTable
   var dataTable = $('#empTable').DataTable({
     'processing': true,
     'serverSide': true,
     'serverMethod': 'post',
     'searching': true, // Set false to Remove default Search Control
     'ajax': {
       'url':'ajax/ajaxfile.php',
       'data': function(data){
          // Read values
          var from_date = $('#search_fromdate').val();
          var to_date = $('#search_todate').val();
		  
		  var status = $('#status').val();

          // Append to data
          data.searchByFromdate = from_date;
          data.searchByTodate = to_date;
		  if(status!='')
		  {
			data.status = status;  
		  }
       }
     },
	'columns': [
	{ data: 'batch_code' },
	{ data: 'schedule_sent' },
	{ data: 'ws_date', name: 'ws_date',
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a target=_blank href='/students_registered.php?id="+oData.id+"&batch_code="+oData.batch_code+"&workshop_id="+oData.workshop_id+"&faculty_id="+oData.faculty_id+"'>"+oData.ws_date+"</a>");
        }
    },
	{ data: 'day' },
	{ data: 'location' },
	{ data: 'status' },
	{ data: 'name' },
	{ data: 'course' },
	{ data: 'start_time' },
    { data: 'end_time' },
	{ data: 'start_date' },
	]
  });

  // Search button
  $('#btn_search').click(function(){
     dataTable.draw();
  });

});
</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
	<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery UI CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- jQuery UI JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>