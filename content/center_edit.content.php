<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['id']) && !empty($_GET['id']))
{
	

	if(isset($_GET['dupcenter']))
	{
		$msg = "Center Updation Failed. Center Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}

	elseif(isset($_GET['center_name_missing']))
	{
		$msg = "Center Updation Failed. Please enter center name.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Center Updation Failed. Center Name and Center Location are Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Center updation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Center Updated Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<?php
	$id = get_get_value('id');
	$query_center = "SELECT * from centers where id = ".$id; 
	$result_center = Select($query_center,$conn);
	$center_name = $result_center['rows'][0]['center_name'];
	$center_manager = $result_center['rows'][0]['center_manager'];
	$center_location = $result_center['rows'][0]['loc_id'];
	

?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update Center
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/center_edit.php'>
<div class="kt-portlet__body">
<div class="col-lg-6" hidden>
	<label>ID</label>
	<input type="text" class="form-control" id="id" name="id" value = "<?php echo $id; ?>">
	
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Center Name</label>
	<input type="text" class="form-control" id="center_name" name="center_name" value = "<?php echo $center_name; ?>">
	
</div>
<div class="col-lg-6">
<?php 
	$query_users = "SELECT id,user_name from users";
	$result_users = Select($query_users,$conn);
?>
	<label class="">Center Manager:</label>
			<select class="form-control kt-select2" id="center_manager" name="center_manager">
			<option value="0">Select</option>
				<?php 
					foreach($result_users['rows'] as $user)
					{
                            if($user['id']==$center_manager)
							{
							echo '<option value="'.$user['id'].'" selected>'.$user['user_name'].'</option>';
							}
							else
							{
							echo '<option value="'.$user['id'].'">'.$user['user_name'].'</option>';
							}
		
					}
				?>
			</select>
	
</div>
</div>
<?php 
	$query_locations = "SELECT id,loc_name from locations  where id not in (1,2)";
	$result_locations = Select($query_locations,$conn);
?>
<div class="form-group row">

<div class="col-lg-6">
	<label class="">Center Location</label>

			<select class="form-control kt-select2" id="location_id" name="location_id">
			<option value="0">Select</option>
				<?php 
					foreach($result_locations['rows'] as $location)
					{
                            if($location['id']==$center_location)
							{
							echo '<option value="'.$location['id'].'" selected>'.$location['loc_name'].'</option>';
							}
							else
							{
							echo '<option value="'.$location['id'].'">'.$location['loc_name'].'</option>';
							}
		
					}
				?>
			</select>

	</div>
	<div class="col-lg-6">

	
</div>

</div>


</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
}
else
{
	header('location:center_list.php');
	exit();
}
?>