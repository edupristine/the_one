<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$qry1 = "SELECT * FROM job_postings order by id desc";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}

$master_loc = "SELECT id,location_name FROM job_locations order by location_name";
$res_locat = mysqli_query($edu,$master_loc);

	
	if(isset($_GET['dupname1']))
	{
		$msg = "Location Creation Failed. Location Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Location Creation Failed. Location already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Location Creation Failed. Location Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Location Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Location Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>



<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<i class="kt-font-brand flaticon2-line-chart"></i> Job Posting List  | <a href="add_jobs.php">Add New Job Posting</a>
</h3>
</div>
</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Date Range</label>
	<table>
	<tr>
	<td>
	<input type='text'  id='search_fromdate' class="datepicker" style="width:230px;"placeholder='From date' value=<?php echo date('m/01/Y');?> >
	</td>
	<td>
	<input type='text'  id='search_todate' class="datepicker" style="width:230px;" placeholder='To date' value=<?php echo date('m/d/Y');?>>
	</td>
	<td ><select  id="location" name="location" style="margin:5px;height:42px;" >
	<option value=''  >Select Location</option>
	<?php
	while($rl = mysqli_fetch_assoc($res_locat)) {
	echo "<option value=".$rl['id']."  >".$rl['location_name']."</option>";
	}
	?>
	</select>
	</td>
	<td>
	<select  id="job_type" name="job_type" style="margin:5px;height:42px;" >
	<option value=''>Select Job Type</option>
    <option value='1'>Internal</option>
	<option value='2'>External</option>
	</select>
	</td>
	<td>
	<select  id="vertical" name="vertical" style="margin:5px;height:42px;" >
	<option value=''>Select Vertical</option>
    <option value='1'>Accounts</option>
	<option value='2'>Finance</option>
	<option value='3'>Both Accounts & Finance</option>
	</select>
	</td>
	<td>
	<td>
	<input type='button' id="btn_search" value="Search">
	</td>
	</tr>
	</table>
</div>

</div>
<div class="kt-portlet">
<div style="padding-top:20px;padding-left:20px;padding-right:20px;">
  <table id='empTable' class='display dataTable'>
                <thead>
                <tr>
				<!--<th >#</th>-->
				<th >Course</th>
				<th >Job Title 1</th>
				<th >Job Title 2</th>
				<th >Company Name</th>
				<th >Location</th>
				<th >Qualification</th>
				<th >Salary</th>
				<th >Start Date</th>
				<th >End Date</th>
				<th >Created At</th>
				<th >Is Act</th>
				<th ><center>Action<hr></hr>Applied</center></th>
                </tr>
                </thead>  
		
    </table>
</div>
</div>
</div>
</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

$(document).ready(function(){

   // Datapicker 
   $( ".datepicker" ).datepicker({
      "dateFormat": "yy-mm-dd"
   });

   // DataTable
   var dataTable = $('#empTable').DataTable({
     'processing': true,
     'serverSide': true,
     'serverMethod': 'post',
     'searching': true, // Set false to Remove default Search Control
     'ajax': {
       'url':'ajax/company_ajaxfile.php',
       'data': function(data){
          // Read values
          var from_date = $('#search_fromdate').val();
          var to_date = $('#search_todate').val();
		  var location = $('#location').val();
		  var job_type = $('#job_type').val();
          var vertical = $('#vertical').val();
          // Append to data
          data.searchByFromdate = from_date;
          data.searchByTodate = to_date;
		  if(job_type!='')
		  {
			data.job_type = job_type;  
		  }
		  
		  if(location!='')
		  {
			data.location = location;  
		  }
		  
		  if(vertical!='')
		  {
			data.vertical = vertical;  
		  }
       }
     },	 
	'columns': [
	{ data: 'course' },
	{ data: 'job_title_1' },
    { data: 'job_title_2' },
	{ data: 'company_name' },
	{ data: 'job_locations' },
	{ data: 'qualification' },
	{ data: 'salary' },
	{ data: 'start_date' },
	{ data: 'end_date' },
	{ data: 'created_at' },
    { data: 'is_active' },
	{ data: 'id', name: 'Action',
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<a target=_blank href='/edit_job_posting.php?id="+oData.id+"' ><i class='la la-edit'></i></a> | <a target=_blank href='/mail_job_posting.php?id="+oData.id+"' ><i class='la la-envelope'></i></a> | <a target=_blank href='/view_job_posting.php?id="+oData.id+"' ><i class='la la-eye'></i></a><hr></hr><div style='font-size:20px;'><center><strong><a target=_blank href='/job_posting_scrutiny.php?id="+oData.id+"' >"+oData.totalapplied+"<i class='la la-briefcase'></a></strong></center></div>");
        }
    },
	]
  });

  // Search button
  $('#btn_search').click(function(){
     dataTable.draw();
  });

});
</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
	<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery UI CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- jQuery UI JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>