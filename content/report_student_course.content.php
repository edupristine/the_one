<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'issue_books.php?filter_param='+filter_param;
	}
</script>
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($location_id == "")
	{
		$location_id = "3";
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = "3";
	}
	
	$qrys="";
	if($course_id!='')
	{
		$qrys="and s.course_id = ".$course_id."";
	}
	elseif($course_id=='')
	{
		$qrys="and s.course_id in (select course_id from sub_courses where rrf = 0)";
	}
	
	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and sb.rrf=0 and  sl.id = s.loc_id and s.loc_id = ".$location_id." $qrys ORDER BY s.student_name";
	}
	else
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and sb.rrf=0 and  sl.id = s.loc_id and s.loc_id = ".$location_id." and (student_name = '".$filter_param."' OR crm_id = '".$filter_param."' OR student_email = '".$filter_param."' OR student_contact = '".$filter_param."' OR subcourse_name = '".$filter_param."') $qrys ORDER BY s.student_name";
	}
	//	$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and sb.rrf=0 and  sl.id = s.loc_id and s.loc_id = ".$location_id." ORDER BY s.student_name";
	$result_allstudents = Select($query_allstudents,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$center_locations = "SELECT id,loc_name from locations where loc_type = 'CENTER'";
	$result_loc_centers = Select($center_locations,$conn);
	
	$course = "SELECT id,course_name from courses where id in (select course_id from sub_courses where rrf = 0) order by course_name ";
	$result_course = Select($course,$conn);
	
	/*if($_SESSION['U_LOCATION_TYPE'] != "CENTER" && $location_type_report == "CENTER")
	{
		$newrecord = array('id'=>$_SESSION['U_LOCATION_ID'],'loc_name'=>$_SESSION['U_LOCATION_NAME']);
		array_push($result_loc_centers['rows'],$newrecord);
	}*/
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Student Report <?php 
											
											echo "(".$location_name_report.")"; 
	                                       
											?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
																if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
																{
																?>
											
														<div class="col">
														<table><tr>
														<td>
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Courses
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_course['rows'] as $course)
																			{	
																				
																				?>
																				<a class="dropdown-item" href="report_student_course.php?uid=<?php echo $user_id;?>&cid=<?php echo $course['id'];?>&lid=<?php echo $location_id;?>"><?php echo $course['course_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td><td>
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Centers
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_loc_centers['rows'] as $loc_center)
																			{	
																		?>
																				<a class="dropdown-item" href="report_student_course.php?uid=<?php echo $user_id;?>&lid=<?php echo $loc_center['id'];?>"><?php echo $loc_center['loc_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td></tr></table>
														</div>
													<?php
																}
																?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name</th>
												<th >Student Contact</th>
												<!--<th>Student Email</th>-->
												<th>Student Locations</th>
												<th>Student Course</th>
												<th>Student Subcourse</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										        $locs = "SELECT * from locations where id = ".$student['loc_id'];
												$result_locs = Select($locs,$conn);
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td ><?php echo $student['student_contact']; ?></td>
													<!--<td><?php// echo $student['student_email']; ?></td>-->
													<td><?php echo $result_locs['rows'][0]['loc_name']; ?></td>
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_subcourses['rows'][0]['subcourse_name']; ?></td>
													<td><button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#student_<?php echo $student['id']; ?>">Raise Requirement</button></td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid from skus s,courses c,sub_courses cs,subcourses_skus sk where cs.course_id=c.id AND sk.subcourse_id=cs.id AND s.id = sk.sku_id and c.id=".$student['course_id']." and cs.id=".$student['subcourse_id']."";
										
												$result_select_issue = Select($select_issue,$conn);
											?>
											<div class="modal fade" id="student_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Books List For: ".$student['student_name']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="200">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['id'];?>">
																		<thead>
																			<tr>
																				<td>#</td>
																				<td>SKU Name</td>
																				<td>Add Requirement</td>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																							<?php 
																							$select_req="SELECT count(id) as counts FROM issuance  WHERE `sku_id`='".$each_issue['skuid']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_req,$conn,"issuance");
																							if($result_sel['rows'][0]['counts']!=0){
																								echo "Requirement Raised";
																							}
										                                                     else{
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$each_issue['skuid']."|";
																							?>
																								<input type="checkbox" id="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" name="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" value="1">
																							<?php
																							 }
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																		</tbody>
																	</table>
																	<input hidden id="hidden_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Raisereq(<?php echo $student['id']; ?>,<?php echo $student['loc_id']; ?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												<?php	
											
											}
										?>
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function Raisereq(student_id,loc_id)
{
	

	var tb = "hidden_"+student_id;
	var cbs = document.getElementById(tb).value;
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);
	//alert(final_cbs_json);
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	
			if(output.status == 'success')
			{
				alert("Books Requirement placed Successfully.");
				submit_button_clicked = '';
				$('#student_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Raise requirement FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Raise requirement FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	//alert('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id));
	xmlhttp.open('POST', 'ajax/raise_requirement.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id));
}
</script>