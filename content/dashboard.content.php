<?php

$display=0;
$select_faculty = "SELECT user_id FROM users_teams WHERE team_id = 2 ";
$result_fac = Select($select_faculty,$conn);
foreach($result_fac['rows'] as $faculty)
{
if($faculty['user_id']==$_SESSION['USER_ID'])	
{
$display=1;	
}
}

if($display==0)
{
	$location_id=$_SESSION['U_LOCATION_ID'];
	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	//pending Couriers
	$pending_courier = "SELECT count(sc.id) as total FROM `students` s,student_couriers_request sc, sub_courses sb WHERE sb.id=s.subcourse_id  and  sc.student_id = s.id and sc.courier_status in ( 'Requested','Partially Sent' )ORDER BY sc.courier_status";
	$result_pending_courier = Select($pending_courier,$conn);
	
	
	//In Couriers for Center
	$in_courier = "SELECT count(id) as total from transfers where to_loc_id=2 ";
	$result_in_courier = Select($in_courier,$conn);
	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet__body">




<div class="kt-portlet__body kt-portlet__body--fit">
											<div class="kt-widget17">
												<div class="kt-widget17__visual kt-widget17__visual--chart kt-portlet-fit--top kt-portlet-fit--sides" style="background-color: #fd397a">
													<div class="kt-widget17__chart" style="height:100px;">
														
													</div>
												</div>
												<div class="kt-widget17__stats">
													<div class="kt-widget17__items">
													    <?php
														if($location_id==1)
														{
														?>
														<div class="kt-widget17__item">
														
															<span class="kt-widget17__subtitle" style="font-size:50px;">
																<a href="send_courier.php?status=Pending" id="pendingcount" name="pendingcount"><strong><?php echo $result_pending_courier['rows'][0]['total'];?></strong></a>
															</span>
															<span class="kt-widget17__desc" style="font-size:14px;">
																Pending Couriers Count
															</span>
														</div>
														<?php
														}
														?>
														<?php
														if(($location_id!=1))
														{
														?>
														<div class="kt-widget17__item">
															
															<span class="kt-widget17__subtitle" style="font-size:50px;">
																<strong><?php echo $result_in_courier['rows'][0]['total'];?></strong>
															</span>
															<span class="kt-widget17__desc" style="font-size:14px;">
																In Couriers Count
															</span>
														</div>
														<?php
														}
														?>
													</div>
													<!--<div class="kt-widget17__items">
														<div class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect id="bound" x="0" y="0" width="24" height="24"></rect>
																		<path d="M12.7037037,14 L15.6666667,10 L13.4444444,10 L13.4444444,6 L9,12 L11.2222222,12 L11.2222222,14 L6,14 C5.44771525,14 5,13.5522847 5,13 L5,3 C5,2.44771525 5.44771525,2 6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,13 C19,13.5522847 18.5522847,14 18,14 L12.7037037,14 Z" id="Combined-Shape" fill="#000000" opacity="0.3"></path>
																		<path d="M9.80428954,10.9142091 L9,12 L11.2222222,12 L11.2222222,16 L15.6666667,10 L15.4615385,10 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 L9.80428954,10.9142091 Z" id="Combined-Shape" fill="#000000"></path>
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Reported
															</span>
															<span class="kt-widget17__desc">
																72 Support Cases
															</span>
														</div>
														<div class="kt-widget17__item">
															<span class="kt-widget17__icon">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<rect id="bound" x="0" y="0" width="24" height="24"></rect>
																		<path d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z" id="Combined-Shape" fill="#000000" opacity="0.3"></path>
																		<path d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z" id="Rectangle-102-Copy" fill="#000000"></path>
																	</g>
																</svg> </span>
															<span class="kt-widget17__subtitle">
																Arrived
															</span>
															<span class="kt-widget17__desc">
																34 Upgraded Boxes
															</span>
														</div>
													</div>-->
												</div>
											</div>
										</div>

</div>
	
</div>

<?php
}
?>