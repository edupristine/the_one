<?php
	$query_allorders = "SELECT o.id,ord_num,ord_date,pi_num,vd.vendor_name,ord_status,ord_amt,loc_id,exp_rec_date,comments FROM `orders` o, vendors vd WHERE o.vendor_id = vd.id and loc_id=1 ORDER BY ord_num desc";
	$result_allorders = Select($query_allorders,$conn);	

?>
<script>
	function ConfirmReceived()
	{
		var action = confirm("Are you sure, you have received this Order? \n  Pressing OK will mark it as Received \n  Pressing Cancel will NOT mark it as Received");
		return action;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['ordrecparammiss']))
	{
		$msg = "Order Updation Failed. Missing Fields";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['ordupdfailed']))
	{
		$msg = "Order Updation Failed. Missing Fields";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['ordstatusupd']))
	{
		$msg = "Order marked as Received.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>


	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Order List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
												if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
												{
												?>
												&nbsp;
												<a href="order_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Order
												</a>
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
											
												<th>Order No.</th>
												<th>Order Date</th>
												<th>Vendor</th>
												<th>Ref. No.</th>
												<th>Order Status</th>
												<th>Order Amount</th>
												<th colspan=1><center>Actions</center></th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allorders['rows'] as $order) 
											{	?>
												<tr>
													
													<td><?php echo $order['ord_num']; ?></td>
													<td><?php echo $order['ord_date']; ?></td>
													<td><?php echo $order['vendor_name']; ?></td>
													<td><?php echo $order['pi_num']; ?></td>
													<td><?php echo $order['ord_status']; ?></td>
													<td><?php echo $order['ord_amt']; ?></td>
													<td style="text-align:center;vertical-align:middle;">
													<div class="btn-group" role="group" aria-label="First group">
															<a href="#" class="kt-nav__link-icon flaticon-eye" data-toggle="modal" data-target="#orderview_<?php echo $order['id']; ?>" ></a>
														</div>
													</td>
													<td hidden>
													<?php
														if($order['ord_status'] == "Placed") {
													?>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="order_edit.php?id=<?php echo $order['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
															<a onclick="return ConfirmReceived();" href="ajax/order_received.php?id=<?php echo $order['id']; ?>" class="btn-sm btn-success btn-elevate btn-pill" style="margin-left:5px;"><i class="la la-angle-double-left"></i>Received</a>
														</div>
														<?php } ?>	
													
													</td>
													
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
<?php 

							foreach($result_allorders['rows'] as $order) 
							{	
							$query_location = "SELECT loc_name from locations where id=".$order['loc_id'];
							$result_location = Select($query_location,$conn);
							?>
							<div class="modal fade" id="orderview_<?php echo $order['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel"><?php echo "Order Details for : #Ref ".$order['pi_num']; ?></h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										</button>
									</div>
									<div class="modal-body">
										<div class="kt-scroll" data-scroll="true" data-height="500">
										<div class="form-group row">
										<div class="col-lg-3">
										<label class="">Order Number</label>
										<br>
										<b><?php echo $order['ord_num']; ?></b>
										</div>
										
										<div class="col-lg-3">
										<label>Order Status</label>
										<br>
										<b><?php echo $order['ord_status']; ?></b>
										</div>
										
										<div class="col-lg-3">
										<label>Order Date</label>
										<br>
										<b><?php echo $order['ord_date']; ?></b>
										</div>
										
										<div class="col-lg-3">
										<label class="">Exp. Rec. Date</label>
										<br>
										<b><?php echo $order['exp_rec_date']; ?></b>
										</div>
                                        </div>
										<div class="form-group row">
										
										<div class="col-lg-3">
										<label>Order Amt.</label>
										<br>
										<b><?php echo $order['ord_amt']; ?></b>
										</div>
										
										<div class="col-lg-3">
										<label>Order Location.</label>
										<br>
										<b><?php echo $result_location['rows'][0]['loc_name']; ?></b>
										</div>
										
										<div class="col-lg-6">
										<label>Vendor</label>
										<br>
										<b><?php echo $order['vendor_name']; ?></b>
										</div>
                                        </div>
										
										<div class="form-group row">
										
										<div class="col-lg-12">
										<label>Comments.</label>
										<br>
										<b><?php echo $order['comments']; ?></b>
										</div>
										
                                        </div>
										
										
										<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
										<tr>
										<th style="width:5%;">#</th>
										<th style="width:50%;">SKU Name</th>
										<th style="width:15%;"><center>Quantity</center></th>
										<th style="width:15%;"><center>Rate</center></th>
										<th style="width:15%;"><center>Amt.</center></th>
										</tr>
										</thead>
										<tbody id="rows_div">
										<?php 
										$query_skus = "Select s.sku_name,os.qty,os.rate,os.amount from orders_skus os, skus s where os.ord_id = ".$order['id']." AND s.id = os.sku_id";
										$result_skus_selected = Select($query_skus,$conn);
										$i=0;
										foreach($result_skus_selected['rows'] as $sku_selected)
										{
										$i++;?>
										<tr id="row_id_<?php echo $i; ?>">
										<td class="text-center"><?php echo $i; ?></td>
										<td  class="text-left">
										<?php
										echo $sku_selected['sku_name'];
										?>
										</td>
										<td class="text-center"><?php echo $sku_selected['qty']; ?></td>
										<td class="text-center"><?php echo $sku_selected['rate']; ?></td>
										<td class="text-center"><?php echo $sku_selected['amount']; ?></td>
										</tr>
										<?php } ?>
										</tbody>
										</table>
										</div>
									</div>
					
								</div>
							</div>
						</div>
						
						<?php	
					
					}
				?>