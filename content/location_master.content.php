<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$qry1 = "SELECT * FROM job_locations order by location_name";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}
	
	if(isset($_GET['dupname1']))
	{
		$msg = "Location Creation Failed. Location Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Location Creation Failed. Location already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Location Creation Failed. Location Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Location Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Location Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add Job Location
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/location_add.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-12">
	<label>Location Name</label>
	<input type="text" class="form-control" id="location_name" name="location_name">
	
</div>
</div>
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
			<center><button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button></center>
		</div>
	</div>
</div>
</div>
</form>



<div class="kt-portlet__body">
<input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="">
<thead>
<tr>
<th >#</th>
<th >Location Name</th>
<th ><center>Action</center></th>
</tr>
</thead>
<tbody>
<?php
$i=1;
foreach($rows as $locs){
	
?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $locs['location_name'];?></td>
<td><center>
<a href="edit_location_master.php?id=<?php echo$locs['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
</center></td>
</tr>
<?php
$i++;
}
?>

</tbody>
</table>
</div>
</div>
</div>
</div>

</div>

<?php
?>