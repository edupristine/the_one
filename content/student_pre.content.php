
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "SKU Issue Failed. SKU already Given to the student .";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['insufficient_qty']))
	{
		$msg = "SKU Issue Failed. Insufficient qty for ".$_GET['insf'];
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
    elseif(isset($_GET['dberror']))
	{
		$msg = "Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Sku's Issued Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Issue books to Student prior to 1st June 2019 
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/student_pre.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Student ID</label>
	<input type="text" class="form-control" id="stu_id" name="stu_id">
	
</div>
<?php 
	$query_skus = "SELECT id,sku_name from skus order by sku_name";
	$result_skus = Select($query_skus,$conn);
?>
<div class="col-lg-6">
	<label class="">Select Sku's</label>
	<div class="">
			<select class="form-control kt-select2" id="kt_select2_3" name="param[]" multiple="multiple">
				<?php 
					foreach($result_skus['rows'] as $sku)
					{
					
							echo '<option value="'.$sku['id'].'">'.$sku['sku_name'].'</option>';
						
						
						echo '</optgroup>';
					}
				?>
			</select>
		</div>
	</div>
</div>
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>