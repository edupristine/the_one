<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<!--begin::Portlet-->

<form class="kt-form kt-form--label-right" method='POST' action=''>
<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Update Offered Price
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-6">
<label>Account No:</label>
<input    class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text" onBlur="RECSelect(this.value,'row_1');" value=""/>
<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
</div>
<div class="col-lg-6">
<label class="">Student Name:</label>
<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label>Assigned to:</label>
<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text" readonly  value=""/>
<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
</div>
<div class="col-lg-6">
<label class="">Offered Price:</label>
<input   class="form-control" name="offeredprice_1" id="offeredprice_1"  style="width:100%;" type="text" readonly />
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label class="">Paid.:</label>
<input  class="form-control" name="paid_1" id="paid_1"  style="width:100%;" type="text" readonly  />
</div>
<div class="col-lg-6">
<label class="">Balance:</label>
<input   class="form-control"  name="balance_1" id="balance_1"  style="width:100%;" type="text" readonly   />
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Discounts:</label>
<textarea type="text" class="form-control"  name="discount_1" id="discount_1" rows="2" readonly ></textarea>
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label>New Offered Price:</label>
<input    class="form-control" name="newofferedprice_1" id="newofferedprice_1"  style="width:100%;" type="text" onBlur="OFRSelect(this.value,'row_1');" />
</div>
<div class="col-lg-6">
<label class="">New Balance:</label>
<input   class="form-control" name="newbalance_1" id="newbalance_1"  style="width:100%;" type="text" readonly  />
<input   class="form-control" name="backdiscount_1" id="backdiscount_1"  style="width:100%;" type="text"  hidden  />
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Backend Discounts Comments:</label>
<textarea type="text" class="form-control"  name="backcomment_1" id="backcomment_1" rows="4"></textarea>
</div>
</div>

</div>

<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="Saveofferedprice();">Update Offered Price</button>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
	<!--begin: Datatable -->


	<!--end: Datatable -->
</div>

</form>
</div>

<?php
?>