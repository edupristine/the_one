
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$location_id = $_SESSION['U_LOCATION_ID'];
$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
$result_loc_type = Select($location_type,$conn);
$location_type_report = $result_loc_type['rows'][0]['loc_type'];
$location_name_report = $result_loc_type['rows'][0]['loc_name'];

if(isset($_GET['dberror']))
	{
		$msg = "Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Books Transition Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	 Allocate Books to Faculties
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='form_handlers/faculty_sku.php'>
<div class="kt-portlet__body">
<div class="form-group">

<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Select Faculty</label>
		<td class="text-center">
		<select id="faculty" name="faculty" class="form-control" onchange="showfacultydetails(this.value)">  
		<option value="0">Select</option>
		<?php 
		$query_faculties = "SELECT * FROM faculties order by faculty_name";
		$result_faculties = Select($query_faculties,$conn);
		$i=0;
		foreach($result_faculties['rows'] as $faculty)
		{
		?>
        <option value="<?php echo $faculty['id'];?>">
		<?php echo $faculty['faculty_name'];?></option>
		
		<?php
		}
		?>
		</select>
		</td>
	
</div>
<div class="col-lg-5">
	<label>Allocation Date</label>
		<td class="text-center"><input data-toggle="tooltip" name="allocation_date" id="allocation_date"  class="form-control" type="date" value = "<?php echo date('Y-m-d'); ?>"  /></td>
	
</div>
</div>
</div>
		
	<!--begin: Datatable -->
    <div id="sku_table" name="sku_table"></div>
	<!--end: Datatable -->
</div>	
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="submit" name="submit" type="submit" class="btn btn-success">Update</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function showfacultydetails(fid)
{
	document.getElementById('sku_table').innerHTML = "";
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = xmlhttp.responseText;
			 document.getElementById('sku_table').innerHTML = output;
			
		}
	}

	xmlhttp.open('POST', 'ajax/faculty_skus.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('faculty='+fixEscape(fid));
	
}


function display_transfer(val,sku)
{
	if(val==3)
	{
	document.getElementById("display_me_"+sku).style.display = 'block';
	document.getElementById("transfered_from_"+sku).value = 0;
	
	}
	else
	{
	document.getElementById("display_me_"+sku).style.display = 'none';	
	document.getElementById("transfered_from_"+sku).value = 0;
	}
	
}
</script>

