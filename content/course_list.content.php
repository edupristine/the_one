<?php
	$query_allcourses = "SELECT * FROM courses c ORDER BY course_name";
	$result_allcourses = Select($query_allcourses,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Course List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="course_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Course
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Course Name</th>
												<!--<th>Course Owwner</th>
												<th>Course Manager</th>
												<th>Start Date</th>
												<th>End Date</th>-->
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allcourses['rows'] as $courses) 
											{	?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $courses['course_name']; ?></td>
													<!--<td><?php echo $courses['course_owner']; ?></td>
													<td><?php echo $courses['course_manager']; ?></td>
													<td><?php echo $courses['start_date']; ?></td>
													<td><?php echo $courses['end_date']; ?></td>-->
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="course_edit?id=<?php echo$courses['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
															<!--<button type="button" class="btn btn-brand btn-elevate btn-pill"><i class="la la-bank"></i> Solid</button>
															<a type="button" class="btn btn-success"><i class="la la-paperclip"></i></a>-->
														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>