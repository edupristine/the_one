<?php
date_default_timezone_set('UTC');
setlocale(LC_MONETARY, 'en_IN');
ini_set('max_execution_time', 0);//10 min
ini_set('max_input_time', -1);//10 min
$date_range = get_get_value('date_range');
if($date_range != ""){
$drange = explode(" / ",$date_range);	
// $edate = date("Y-m-d",strtotime($drange));
// $sdate = date('Y-m-d', strtotime('-15 days', strtotime($edate)));
$sdate = date('Y-m-d',strtotime($drange[0]));
$edate = date('Y-m-d', strtotime('+1 days', strtotime($drange[1])));

}else{
    $sdate = date("Y-m-d");
    $edate = date('Y-m-d', strtotime('+1 days', strtotime($sdate)));
    $date_range = $sdate." / ".$edate;
}




$start_date = $sdate;
$end_date = $edate;


$begin = new DateTime($start_date);
$end = new DateTime($end_date);

$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);





?>
<script>
	function get_batches()
	{
		var date_range = document.getElementById('date_range').value;
		window.location = 'summary_daily_transaction.php?date_range='+date_range;
	}
	
	function send_mail()
	{
		var date_range = document.getElementById('date_range').value;
		
		window.location = 'ajax/transaction_summary_mail.php.php?date_range='+date_range;
	}
</script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
	<label>Date </label>

		<div class="input-group" id="kt_daterangepicker_2">
						<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
						</div>
					</div>
					
</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_batches();" type="button" class="btn btn-primary form-control">Get Report</button>
	</div>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
<!--	<button onClick="send_mail();" type="button" class="btn btn-primary form-control">Send Mail</button>-->
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Daily Transaction Summary
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr >
												<th >#</th>
												<th >Transaction Date</th>
												<th >Daily Collection as per BI</th>
												<th >Amount Received</th>
												<th >Actual Amount Received</th>
												<th >Wrong Entry/Rejected Cases</th>
												<th >Amount Pending</th>
												<th >Finance Com</th>
												<th >Cheque </th>
												<th >Others</th>
												
											</tr>
											
										</thead>
										<tbody>
										<?php
											$i = 1;
											$totalpaid=0;
											$amt_recv=0;
											$cumulativesum=0;
											$amtrce=0;
											$penamt=0;
											$financecom=0;
											$cheque=0;
											$ac_amt_rec=0;
											$others=0;
											$wrongentry=0;
											$finaloff=0;
											$blanksvar=0;
                                            foreach ($period as $dt) {
												
											$newdate = $dt->format("Y-m-d");
											$pendingamt=0;
											
											$querypay="select sum(paid) as `paid` FROM (
												SELECT sum(sc.cf_1303) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$newdate."'
									            AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1313) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$newdate."'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1323) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$newdate."'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1333) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$newdate."'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1343) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$newdate."'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1353) as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$newdate."' 
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqp = mysqli_query($conn1,$querypay);
												$qrpay = mysqli_fetch_assoc($resultqp);
												
												
												$querypaymode="select sum(paid) as `paid` FROM (
												SELECT sum(sc.cf_1303) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$newdate."' 
												AND sc.cf_1305 = 'Cheque'
									            AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1313) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$newdate."'
												AND sc.cf_1315 = 'Cheque'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1323) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$newdate."'
												AND sc.cf_1325 = 'Cheque'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1333) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$newdate."'
												AND sc.cf_1335 = 'Cheque'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1343) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$newdate."'
												AND sc.cf_1345 = 'Cheque'
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1353) as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$newdate."'
                                                AND sc.cf_1355 = 'Cheque'												
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqpmode = mysqli_query($conn1,$querypaymode);
												$qrpaymode = mysqli_fetch_assoc($resultqpmode);
												
												
												
												$querypaymodeL="select sum(paid) as `paidL` FROM (
												SELECT sum(sc.cf_1303) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$newdate."' 
												AND (sc.cf_1305 = 'Liqui Loans' OR
												sc.cf_1305 = 'Neev Finance' or sc.cf_1305 = 'ABFL' or sc.cf_1305 = 'Avanse')
									            AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1313) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$newdate."'
												AND (sc.cf_1315 = 'Liqui Loans' OR
												sc.cf_1315 = 'Neev Finance' or sc.cf_1315 = 'ABFL' or sc.cf_1315 = 'Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1323) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$newdate."'
												AND (sc.cf_1325 = 'Liqui Loans' OR
												sc.cf_1325 = 'Neev Finance' or sc.cf_1325 = 'ABFL' or sc.cf_1325 = 'Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1333) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$newdate."'
												AND (sc.cf_1335 = 'Liqui Loans' OR
												sc.cf_1335 = 'Neev Finance' or sc.cf_1335 = 'ABFL' or sc.cf_1335 = 'Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1343) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$newdate."'
												AND (sc.cf_1345 = 'Liqui Loans'
												OR sc.cf_1345 = 'Neev Finance' or sc.cf_1345 = 'ABFL' or sc.cf_1345 = 'Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1353) as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$newdate."'
                                                AND (sc.cf_1355 = 'Liqui Loans' OR
												sc.cf_1355 = 'Neev Finance' or sc.cf_1355 = 'ABFL' or sc.cf_1355 = 'Avanse')
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqpL = mysqli_query($conn1,$querypaymodeL);
												$qrpayL = mysqli_fetch_assoc($resultqpL);
												
												$querypaymodeO="select sum(paid) as `paidO` FROM (
												SELECT sum(sc.cf_1303) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$newdate."' 
												AND (sc.cf_1305 = 'card' OR
												sc.cf_1305 = 'cash' OR
												sc.cf_1305 = 'online' OR
												sc.cf_1305 = 'paytm' OR
												sc.cf_1305 = 'wire transfer' 
												)
									            AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1313) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$newdate."'
												AND (sc.cf_1315 = 'card' OR
												sc.cf_1315 = 'cash' OR
												sc.cf_1315 = 'online' OR
												sc.cf_1315 = 'paytm' OR
												sc.cf_1315 = 'wire transfer' 
												)
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1323) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$newdate."'
												AND 
												(sc.cf_1325 = 'card' OR
												sc.cf_1325 = 'cash' OR
												sc.cf_1325 = 'online' OR
												sc.cf_1325 = 'paytm' OR
												sc.cf_1325 = 'wire transfer' 
												)
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1333) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$newdate."'
												AND 
												(sc.cf_1335 = 'card' OR
												sc.cf_1335 = 'cash' OR
												sc.cf_1335 = 'online' OR
												sc.cf_1335 = 'paytm' OR
												sc.cf_1335 = 'wire transfer' 
												)
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1343) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$newdate."'
												AND 
												(sc.cf_1345 = 'card' OR
												sc.cf_1345 = 'cash' OR
												sc.cf_1345 = 'online' OR
												sc.cf_1345 = 'paytm' OR
												sc.cf_1345 = 'wire transfer' 
												)
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1353) as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$newdate."'
                                                AND 
												(sc.cf_1355 = 'card' OR
												sc.cf_1355 = 'cash' OR
												sc.cf_1355 = 'online' OR
												sc.cf_1355 = 'paytm' OR
												sc.cf_1355 = 'wire transfer' 
												)
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqpO = mysqli_query($conn1,$querypaymodeO);
												$qrpayO = mysqli_fetch_assoc($resultqpO);
												
												
												$querypaymodeOthers="select sum(paid) as `paidOthers` FROM (
												SELECT sum(sc.cf_1303) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1363) = '".$newdate."' 
												AND sc.cf_1305 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
												
									            AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1313) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1365) = '".$newdate."'
												AND sc.cf_1315 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
												
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1323) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1367) = '".$newdate."'
												AND 
												sc.cf_1325 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1333) as paid
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1369) = '".$newdate."'
												AND sc.cf_1335 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1343) as paid 
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1371) = '".$newdate."'
												AND 
												sc.cf_1345 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
												AND ce.smownerid != 3920 
												UNION ALL
												SELECT sum(sc.cf_1353) as paid  
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												WHERE
												DATE(sc.cf_1373) = '".$newdate."'
                                                AND sc.cf_1355 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse') 
												AND ce.smownerid != 3920 ) x";
											 
											
											 
												$resultqpOthers = mysqli_query($conn1,$querypaymodeOthers);
												$qrpayOthers = mysqli_fetch_assoc($resultqpOthers);
												
												
												
												$select_recx="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' ";
												
												$resultrecx = mysqli_query($conn2,$select_recx);
												$qrpayrex = mysqli_fetch_assoc($resultrecx);
												
												$select_recC="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode='Cheque' ";
												
												$resultrecC = mysqli_query($conn2,$select_recC);
												$qrpayreC = mysqli_fetch_assoc($resultrecC);
												
												$select_recL="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode in ('Liqui Loans','Neev Finance','ABFL','Avanse') ";
												
												$resultrecL = mysqli_query($conn2,$select_recL);
												$qrpayreL = mysqli_fetch_assoc($resultrecL);
												
												$select_recO="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode in ('Cash','Card','Online','Wire Transfer','Paytm','',' ') ";
												
												$resultrecO = mysqli_query($conn2,$select_recO);
												$qrpayreO = mysqli_fetch_assoc($resultrecO);
												
												//$select_wrong="SELECT sum(paid) as `amount_rec`,paymode FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='2' and reasons in ('Wrong Entry','Reject') ";
												
												//Query Moved Out Due to Akshata's Report Display Issues
												$select_wrong="SELECT sum(paid) as `amount_rec`,paymode FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid` NOT IN ('1') and reasons in ('Wrong Entry','Reject') ";
												$resultwrong = mysqli_query($conn2,$select_wrong);
												$wrong_entries = mysqli_fetch_assoc($resultwrong);
												
												
												$select_ac_amtrec="SELECT amount as `ac_amount_rec` FROM datewise_payments_recieved  WHERE   `received_date`='".$newdate."' ";
												
												$resultamrec = mysqli_query($conn2,$select_ac_amtrec);
												$acamrec = mysqli_fetch_assoc($resultamrec);
												
												$select_wrong_details="SELECT paid as `amount_rec`,paymode,id,org_no,transaction_id FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='2' ";
												
												$qrpayrewrongdetails = mysqli_query($conn2,$select_wrong_details);
												
												while($qrpayrewrongdets = mysqli_fetch_assoc($qrpayrewrongdetails)) {
												$qrpayrewrong[] =$qrpayrewrongdets;
												}
												
												$cumulativesum += $qrpay['paid'];
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $newdate; ?></strong></td>
												    <td><?php echo $qrpay['paid']; ?></td>
													<td><?php 
													
													if($qrpayrex['amount_rec']!='')
													{
												    $amt_recv=$qrpayrex['amount_rec'];		
                                                    echo $qrpayrex['amount_rec'];
													}
													else
													{
													$amt_recv=0;
                                                    echo "0";
													}	
													
													?></td>
													<td>
													<?php
													$ac_am_rec= $acamrec['ac_amount_rec']; 
													if($ac_am_rec<0)
													{
														echo "0";
													}
													else
													{
														echo $ac_am_rec;
													}
													?>
													</td>
													<td><?php 
													$finalcheq5= $wrong_entries['amount_rec']; 
													if($finalcheq5<0)
													{
														echo "0";
													}
													else
													{
														echo $finalcheq5;
													}
													
													?></td>
													
													<td><?php
                                                    $pendingvalue=$qrpay['paid']-$amt_recv;													
													
													$pendingamt= $pendingvalue-$qrpayrewrong['amount_rec']; 
													
													echo $pendingamt;
													?></td>
													<td><?php 
													$wrongdetailssum=0;
													foreach($qrpayrewrongdetails as $paydet)
													{
														if(($paydet['paymode']=='Liqui Loans')||($paydet['paymode']=='Neev Finance')||($paydet['paymode']=='ABFL')||($paydet['paymode']=='Avanse'))
														{
															$wrongdetailssum += $paydet['amount_rec'];
															
														}
													}
													
													
												    $loanvalue=$qrpayL['paidL']-$qrpayreL['amount_rec'];
													
													if(($loanvalue!=0)&&($wrongdetailssum>0))
													{
													$finalcheq2= $loanvalue-$wrongdetailssum; 
													}
													else
													{
													$finalcheq2= $loanvalue; 	
													}
													if($finalcheq2<0)
													{
														echo "0";
													}
													else
													{
														echo $finalcheq2;
													}
													
														//echo "Og Value:".$finalcheq2."-New Value:".$wrongdetailssum; 
													?></td>
												    <td><?php  
													$wrongdetailssum1=0;
													foreach($qrpayrewrongdetails as $paydet)
													{
														if(($paydet['paymode']=='Cheque'))
														{
															$wrongdetailssum1 += $paydet['amount_rec'];
														    
														}
													}
													
													$chequevalue=$qrpaymode['paid']-$qrpayreC['amount_rec'];
													
													if(($chequevalue!=0)&&($wrongdetailssum1>0))
													{
													$finalcheq= $chequevalue-$wrongdetailssum1; 
													}
													else
													{
													$finalcheq=$chequevalue; 
													}
													
													if($finalcheq<0)
													{
														echo "0";
													}
													else
													{
														echo $finalcheq;
													}
													//echo "Display:".$finalcheq."<br>Sub Wrong Entry value:".$wrongdetailssum1; 
													?></td>
													<td><?php 
													$wrongdetailssum2=0;
													foreach($qrpayrewrongdetails as $paydet)
													{
														
														if(($paydet['paymode']=='Cash')||($paydet['paymode']=='Card')||($paydet['paymode']=='Online')||($paydet['paymode']=='Wire Transfer')||($paydet['paymode']=='Paytm')||($paydet['paymode']==' '))
														{
															$wrongdetailssum2 += $paydet['amount_rec'];
															
														}
														
													}
													$othersvalue=($qrpayO['paidO']+$qrpayOthers['paidOthers'])-$qrpayreO['amount_rec'];
													if($wrongdetailssum2>0)
													{
													$finalcheq3= $othersvalue-$wrongdetailssum2;													
													}
													else
													{
													$finalcheq3= $othersvalue; 
													}
													
													if($finalcheq3<0)
													{
														echo "0";
													}
													else
													{
														echo $finalcheq3;
													} 
													//echo "Og Value:".$finalcheq3."-New Value:".$wrongdetailssum2; 
													
													?></td>
													
													
													
                                                    
													
												</tr>
										<?php
										    $amtrce= $amtrce + $amt_recv;
											$penamt= $penamt + $pendingamt;
											$financecom=$financecom+$finalcheq2;
											$cheque=$cheque+$finalcheq;
											$others=$others+$finalcheq3;
											$wrongentry=$wrongentry+$finalcheq5;
                                           	$totalpaid=$totalpaid+$qrpay['paid'];
                                            $blanksvar=$blanksvar+$finalcheq4;
                                           	$ac_amt_rec=$ac_amt_rec+$ac_am_rec;						
                                            $i++;
											}
										?>
										<tr>
													<td colspan=2 style="text-align:right;"><strong>Total Amount : </strong></td>
													<td>
													<strong><?php echo $totalpaid;?></strong>
													</td>
													<td>
													<strong><?php echo $amtrce;?></strong>
													</td>
													<td>
													<strong><?php echo $ac_amt_rec; ?></strong>
													</td>
													<td>
													<strong><?php echo $wrongentry; ?></strong>
													</td>
													<td>
													<strong><?php echo $penamt;?></strong>
													</td>
													<td>
													<strong><?php echo $financecom;?></strong>
													</td>
													<td>
													<strong><?php echo $cheque;?></strong>
													</td>
													<td>
													<strong><?php echo $others;?></strong>
													</td>
													
													
										</tr>									
										</tbody>
									</table>
									
									

									<!--end: Datatable -->
								</div>
							</div>

</div>

<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>