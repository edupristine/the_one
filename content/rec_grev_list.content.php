<script>
	function get_results()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'rec_grev_list.php?filter_param='+filter_param;
	}

</script>
<?php
$filter_param = get_get_value('filter_param');
$reporttype = get_get_value('reporttype');
$reportval="";
if($reporttype=='Open')
{
	$reportval=" and status='Open'";
}
else if($reporttype=='Closed')
{
	$reportval=" and status='Closed'";
}
else if($reporttype=='All')
{
	$reportval=" and status in ('Open','Closed')";
}
else
{
	$reportval=" and status='Open'";
}


if($filter_param != "")
{

	
$query = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y') as `raised_on`,DATE_FORMAT(resolved_at, '%d/%m/%Y') as `resolved_on` FROM recv_grievances where account_no!='' and (account_no like '%".$filter_param."%' or student_name like '%".$filter_param."' or email like '%".$filter_param."%' or phone like '%".$filter_param."%' or ticket_no like '%".$filter_param."%') $reportval ORDER BY id desc";

}
elseif($filter_param == "")
{

$query = "SELECT *,DATE_FORMAT(created_at, '%d/%m/%Y') as `raised_on`,DATE_FORMAT(resolved_at, '%d/%m/%Y') as `resolved_on` FROM recv_grievances where account_no is not Null $reportval  ORDER BY id desc";		

}

$grievances = Select($query,$conn);

?>

<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content" style="width:100%;">
				<div class="kt-portlet__body">

				
				
				<div class="form-group row">
				<div class="col-lg-4">
					<label>Search</label>
					<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
				</div>
				<div class="col-lg-2">
					<label class="">&nbsp;</label>
					<button onClick="get_results();" type="button" class="btn btn-primary form-control">Search </button> 
			    </div>

				<!--<div class="col-lg-2">
				<label class="">&nbsp;</label>
				  <button onClick="export_data();" type="button" class="btn btn-primary form-control">Export</button>
				</div>-->
				</div>
				</div>
                <div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Receivables Grievances Records
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
														<div class="col">
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Select Report
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																	
																				<a class="dropdown-item" href="rec_grev_list.php?reporttype=All">All</a>
																				<a class="dropdown-item" href="rec_grev_list.php?reporttype=Closed">Closed</a>
																				<a class="dropdown-item" href="rec_grev_list.php?reporttype=Open">Open</a>
															                    
																		
																	</div>
																
															</div>
														</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
                                                <th style="width:5%;" >#</th>
												<th style="width:15%;" >Account No</th>
                                                <th style="width:22%;">Student Name</th>
												<th style="width:5%;">Ticket No</th>
												<th style="width:5%;"><center>Course</center></th>
												<th style="width:5%;"><center>City</center></th>
												<th style="width:5%;"><center>Councelor</center></th>
												<th style="width:5%;"><center>Status</center></th>
												<th style="width:10%;" ><center>Raised On</center></th>
												<th style="width:18%;" ><center>Resolved On</center></th>
												<th style="width:10%;" ><center>Action</center></th>
                                            </tr>
                                        </thead>
										<tbody>
										<?php
										    $i=1;
											foreach($grievances['rows'] as $grieves){
												$id=$grieves['id'];
												$account_no = $grieves['account_no'];
												$student_name = $grieves['student_name'];
												$ticket_no= $grieves['ticket_no'];
												$phone = $grieves['phone'];
												$email = $grieves['email'];
                                                $course= $grieves['course'];
												$city= $grieves['city'];
												$status= $grieves['status'];
												$concerns= $grieves['concerns'];
												$createddate= $grieves['raised_on'];
												$resolvedon= $grieves['resolved_on'];
												$notified_counsellor= $grieves['notified_counsellor'];
												
												
												$sel_assigneduser1 = "SELECT us.user_name as `assigned_user`
												FROM vtiger_account ac
												LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
												INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
												LEFT JOIN `vtiger_users` us ON us.id =ce.smownerid
												WHERE ce.smownerid!='3920' 
												and  ac.account_no='".$account_no."'";
												
												$result1 = mysqli_query($conn1,$sel_assigneduser1);
												$res_assigneduser1 = mysqli_fetch_assoc($result1);

												$assigned_user1= $res_assigneduser1['assigned_user'] ;
										?>
                                                
												<tr>
                                                    <td style="width:5%;"><?php echo $i;?></td>
													<td style="width:15%;"><?php echo $account_no; ?></td>
													<td style="width:22%;"><?php echo $student_name; ?></td>
													<td style="width:10%;"><?php echo $ticket_no; ?></td>
                                                    <td style="width:5%;" ><center><?php echo $course; ?></center></td>
													<td style="width:5%;"><?php echo $city;?></td>
													<td style="width:10%;"><center><?php echo $assigned_user1;?></center></td>
													<td style="width:5%;"><center>
													<?php
													if($status=="Open")
													{
													?>
													<strong style="color:green;"><?php echo $status;?>
													<?php
													}
													else if($status=="Closed")
													{
													?>
													<strong><?php echo $status;?>
													<?php
													}
													?>
													
													</strong></center></td>
												
													<td style="width:10%;"><center><?php echo $createddate;?></center></td>
													<td style="width:15%;"><center><?php echo $resolvedon;?></center></td>
													<td style="width:10%;"><center>
													<table>
													<tr>
													<td>
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#concerns_<?php   echo $id; ?>">View</button>
													</td>
													<td>
													
													<button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#resolve_<?php   echo $id; ?>">
													<?php
													if($status=="Open")
													{
														echo "Resolve";
													}
													else
													{
														echo "Closed";
													}
													?>
													</button>
													</td>
													<?php
													if($status=="Open")
													{
													?>
													<td>
		                                            <?php
													$disabled="";
													if($notified_counsellor==1)
													{
														$disabled="disabled";
													}
													else
													{
														$disabled="";
													}
													?>
													
													
													<button onClick="SIE(<?php echo $id; ?>);" type="button" class="btn btn-bold btn-label-brand btn-sm" <?php echo $disabled;?>>SIE</button>
													</td>
													<td>
													 <button onClick="Duplicate(<?php echo $id; ?>);" type="button" class="btn btn-bold btn-label-brand btn-sm">Duplicate</button>
													</td>
													<?php
											         }
													?>
													</tr>
													</table>
													</center></td>
												</tr>
                                        <?php	
										    $i++;
											}
										?>
										<?php
										foreach($grievances['rows'] as $grieves){
										?>
												<div class="modal fade" id="resolve_<?php echo $grieves['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Ticket No: #".$grieves['ticket_no']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															
																<strong>Resolution Provided:</strong>
																<?php
																$readonly="";
																if($grieves['status']=="Open")
																{
																$readonly="";
																}
																else
																{
																	$readonly="readonly";
																}
																?>
																<div class="kt-scroll" data-scroll="true" data-height="200">
																	<textarea <?php echo $readonly;?> id="resolution_<?php echo $grieves['id']; ?>" name="resolution_<?php echo $grieves['id']; ?>" cols="63" rows="10" ><?php echo $grieves['resolution'];?></textarea>
																</div>
																
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
																<?php
																if($grieves['status']=="Open")
																{
																?>
																<button onClick="Resolve(<?php echo $grieves['id']; ?>);" type="button" class="btn-sm btn-success btn-elevate btn-pill">Close Ticket</button>
																<?php
																}
																?>
																
																
															</div>
														</div>
													</div>
												</div>
											<?php
										}
											?>
											
											
											<?php
										foreach($grievances['rows'] as $grieves){
											
												$account_no  = $grieves['account_no'];
												$phone = $grieves['phone'];
												$email = $grieves['email'];

												$sel_assigneduser = "SELECT us.user_name as `assigned_user`
																FROM vtiger_account ac
																LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
																INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
																LEFT JOIN `vtiger_users` us ON us.id =ce.smownerid
																WHERE ce.smownerid!='3920' 
												                and  ac.account_no='".$account_no."'";

												$result = mysqli_query($conn1,$sel_assigneduser);
												$res_assigneduser = mysqli_fetch_assoc($result);

												$assigned_user= $res_assigneduser['assigned_user'] ;
										?>
												<div class="modal fade" id="concerns_<?php echo $grieves['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Ticket No: #".$grieves['ticket_no']; ?></h5>
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
															<div class="kt-scroll" data-scroll="true" >
															        <strong>Phone:</strong>
																	<?php echo $phone; ?>
																</div>
															<div class="kt-scroll" data-scroll="true" >
															        <strong>Email:</strong>
																	<?php echo $email; ?><p>
																</div>
															<div class="kt-scroll" data-scroll="true" data-height="200">
															        <strong>Concern Raised:</strong>
																	<textarea style="border:0px;" readonly id="concern_<?php echo $grieves['id']; ?>" name="concern_<?php echo $grieves['id']; ?>" cols="63" rows="8" ><?php echo $grieves['concerns'];?></textarea>
																</div>
																
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
											<?php
										}
											?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function Resolve(recordid)
{
	var resolution = document.getElementById('resolution_'+recordid).value;
	if (confirm('Are you sure you want to Close this Ticket?')) {
		if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Ticket Closed Successfully.");
				submit_button_clicked = '';
				$('#resolve_'+recordid).modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Ticket Closure Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Ticket Closure Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/resolve_ticket.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('recordid='+fixEscape(recordid)+'&resolution='+fixEscape(resolution));	
	return true;
	} else {
	submit_button_clicked = '';
	return false;
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}

}


function SIE(recordid)
{
	var resolution = document.getElementById('resolution_'+recordid).value;
	if (confirm('Are you sure you want to send an email to the councellor for this Ticket?')) {
		if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			//alert(xmlhttp.responseText)
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Mail Sent.");
				submit_button_clicked = '';
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Mail Sending  Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Mail Sending Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/send_internal_ticket.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('recordid='+fixEscape(recordid)+'&resolution='+fixEscape(resolution));	
	return true;
	} else {
	submit_button_clicked = '';
	return false;
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}

}

function Duplicate(recordid)
{
	var resolution = document.getElementById('resolution_'+recordid).value;
	if (confirm('Are you sure you want to Close this Ticket directly?')) {
		if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Ticket Closed Successfully.");
				submit_button_clicked = '';
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Ticket Closure Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Ticket Closure Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/resolve_ticket_wout_mail.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('recordid='+fixEscape(recordid)+'&resolution='+fixEscape(resolution));	
	return true;
	} else {
	submit_button_clicked = '';
	return false;
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}

}
</script>