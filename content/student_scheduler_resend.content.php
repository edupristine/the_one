<?php
	$db_server='52.77.5.117';
	$db_username='liveuser';/*liveuser*/
	$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
    $conn1ex = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
	$con = new mysqli($db_server, $db_username, $db_password, 'ecademe_harry_new');
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($course_id == "")
	{
		$course_id = "1";
	}
	if($location_id == "")
	{
		$location_id = "3";
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = "3";
	}
	
    $empQuery = "SELECT * FROM batchcode_schedules where id!=0";
	$result_emaildata = Select($empQuery,$conn);
				

	
?>

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<div class="kt-portlet__body">
<div class="form-group row">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<i class="kt-font-brand flaticon2-line-chart"></i> Resend Batch Schedule
</h3>
</div>
</div>
<div class="form-group row">
<div class="col-lg-4">
	<label>Date Range</label>
	<table>
	<tr>
	<td>
	<input type='text'  id='search_fromdate' class="datepicker" placeholder='From date' value=<?php echo date('m/d/Y');?> >
	</td>
	<td>
	<input type='text'  id='search_todate' class="datepicker" placeholder='To date' value=<?php echo date('m/d/Y');?>>
	</td>
	<td ><select  id="status" name="status" style="margin:5px;height:42px;" >
	<option value=''  >Select Mail Type</option>
	<option value='Confirmed'>Confirmed</option>
	<option value='Cancelled'>Cancelled</option>
	<option value='Blank'>Blank</option>
	
	</select>
	</td>
	<td>
	<input type='button' id="btn_search" value="Search">
	</td>
	</tr>
	</table>
</div>

</div>
<div class="kt-portlet">
<div style="padding-top:20px;padding-left:20px;padding-right:20px;">
  <table id='empTable' class='display dataTable'>
                <thead>
                <tr>
                    <th>Batch Code</th>
                    <th>Class Date</th>
					<th>Student Email</th>
					<th>Mail type</th>
					<th>Action</th>
                </tr>
                </thead>  
		
    </table>
</div>
</div>
</div>
</div>

<?php 

foreach($result_emaildata['rows'] as $students) 
{	
		$statuses="SELECT wd.id, 
		wd.batch_code as `batch_code`, 
		wd.venue_details as `venue_details`,
		wd.location as `location`, 
		wd.start_time `start_time`, 
		wd.end_time as `end_time`,
		wd.actual_start_time as `actual_start_time`,
		wd.actual_end_time as `actual_end_time`,
		wd.classroom_rent as `classroom_rent`,
		wd.slot as slot,
		wd.student_count_by_admin as student_count_by_admin,
		wd.topic as `topic`, 
		wd.fac_average_rating as `faculty_rating`,
		wd.content_quality_rating,
		wd.infra_rating, 
		wd.cs_rating,
		wd.newFaculty,
		wd.ws_comment as `ws_comment`, 
		wd.`status` as `status`,
		wd.mail_sent_flag as `mail_sent_flag`,
		wd.sms_sent_flag as `sms_sent_flag`,
		wd.course as `course`,
		DATE_FORMAT(wd.ws_date, '%e-%b-%Y') as `ws_date`,
		wd.ws_date as `ws_date_display`,
		wd.content_link,
		wd.venue_coordinator_name,
		wd.venue_coordinator_email,
		wd.venue_coordinator_contact,
		wd.support_team_email,
		wd.sales_manager_email,
		wd.extra_instructions_faculty,
		wd.cancel_point,
		wd.changes_point, 
		wd.repeat_session,
		wd.re_entry_point,
		wd.tentitve_remarks,
		wd.feedback_status as `feedback_status`,
		wd.lvc_url as `lvc_url`,
		w.id as `workshop_id`,
		DAYNAME(wd.ws_date) as `day`,
		DATE_FORMAT(w.start_date, '%e-%b-%Y') as `start_date`,
		wd.faculty_incharge as `faculty_incharge`,
		wd.faculty_incharge_email as `faculty_incharge_email`, 
		fc.fid,
		fc.name as  `name`, 
		fc1.name as  `new_fac_name`, 
		fc.email as `email`,
		fc.phone as `phone` FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		LEFT JOIN fac_facultydetails fc1 ON fc1.fid = wd.new_faculty_id
		WHERE w.id='".$students['workshop_id']."' AND wd.ws_date='".$students['workshop_date']."' ORDER BY wd.status";
		$result_status = mysqli_query($con,$statuses);
		$stats = mysqli_fetch_assoc($result_status);
		$workshop_id=$stats['workshop_id'];
		$faculty_name=$stats['new_fac_name'];
		if($faculty_name=="")
		{
		$faculty_name=$stats['name'];	
		}
		
		
		$color="#000000";
        $status=$students['mailtype'];
		if($status=='Confirmed')
		{
		$color="green";
		}else if($status=='Cancelled')
		{
		$color="red";
		}else if($status=='Blank')
		{
		
		}

?>
<div class="modal fade" id="student_<?php echo $students['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="" style="max-width:700px;" >
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"><?php echo "Resend Mail to : ".$students['student_email']; ?></h5>
					
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					</button>
				</div>
				<div class="modal-body">
					<div class="kt-scroll" data-scroll="true" data-height="400">
						<table class="table table-striped- table-bordered table-hover table-checkable" id="<?php echo $students['id'];?>" >
						<thead>
						<tr>
						<th>Mail Preview</th>
						</tr>
						</thead>
						<tbody>
                        <tr>
						<strong>Student Email :</strong><input type="text" id="newemail_<?php echo $students['id'];?>" name="newemail_<?php echo $students['id'];?>" class="form-control" value="<?php echo $students['student_email']; ?>"></input><br>
						</td>
						<tr>
						<td>
						<div id='display_media' name='display_media'>
						<?php
						$body = '';
						$subject= '';
                       echo "<input type='text' id='statusdisplay_".$students['id']."' name='statusdisplay_".$students['id']."' hidden  value='".$status."' ></input>";
						if($workshop_id!="")
						{	
						if(($status=="Confirmed")||($status=="Cancelled"))
						{
						$body .= "<p>Hello ";

						$body .= 'Student';


						$body .=",<br/>
						Greetings from Edupristine!<br/>This is to inform you that your class has been <span style='font-size:18px;font-weight:bold;color:".$color.";'><input type='text' id='status_".$students['id']."' name='status_".$students['id']."' readonly style='border:0px;width:100px;' value='".$status."' readonly></span>. Details of the same are as below:	
						</p>";


						$subject = "Class Details: ";
						if(isset($stats['day'])){
						$subject .= " ".$stats['day'];
						}else{
						$subject .= 'N/A';
						}
						if(isset($stats['ws_date'])){
						$subject .= " ".$stats['ws_date'];
						}else{
						$subject .= 'N/A';
						}
						if(isset($stats['location'])){
						$subject .= " ".$stats['location'];
						}else{
						$subject .= 'N/A';
						}
						if(isset($stats['course'])){
						$subject .= " ".$stats['course'];
						}else{
						$subject .= 'N/A';
						}	

						$body .="<table border='1' cellspacing='0px'><tr><th style='background-color:#16365C;color:#fff;'>Class Date</th><td width='600px'>";
						if(isset($stats['ws_date'])){
						$body .= "<input readonly type='text' id='class_date_".$students['id']."'  style='border:0;width:100%;' name='class_date_".$students['id']."' value='".$stats['ws_date']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Day</th><td width='300px'>";
						if(isset($stats['day'])){
						$body .= "<input readonly type='text' id='day_".$students['id']."'  style='border:0;width:100%;' name='day_".$students['id']."' value='".$stats['day']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Batch Code</th><td width='300px'>";
						if(isset($stats['batch_code'])){
						$body .=  "<input readonly type='text' id='batch_code_".$students['id']."'  style='border:0;width:100%;' name='batch_code_".$students['id']."' value='".$stats['batch_code']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Start Time</th><td width='300px'>";
						if(isset($stats['start_time'])){
						$body .= "<input readonly type='text' id='start_time_".$students['id']."'  style='border:0;width:100%;' name='start_time_".$students['id']."' value='".$stats['start_time']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>End Time</th><td width='300px'>";
						if(isset($stats['end_time'])){
						$body .= "<input readonly type='text' id='end_time_".$students['id']."'  style='border:0;width:100%;' name='end_time_".$students['id']."' value='".$stats['end_time']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Location</th><td width='300px'>";
						if(isset($stats['location'])){
						$body .= "<input readonly type='text' id='location_".$students['id']."'  style='border:0;width:100%;' name='location_".$students['id']."' value='".$stats['location']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Venue</th><td width='400px' style='color:#d35400;font-weight:bold;font-size:16px;'>";
						if(isset($stats['venue_details']) && $stats['venue_details']!=''){
						$body .=  "<input readonly type='text' id='venue_details_".$students['id']."'  style='border:0;width:100%;' name='venue_details_".$students['id']."' value='".$stats['venue_details']."'></input>";
						}else if(isset($stats['venue_details']) && ($stats['location'] == 'LVC' || $stats['location'] == 'Online')){
						$body .= 'N/A';
						}else{
						$body .= 'Venue Detail will be Shared on Friday via Email';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Course</th><td width='300px'>";
						if(isset($stats['course'])){
						$body .= $stats['course'];
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Batch Start Date</th><td width='300px'>";
						if(isset($stats['start_date'])){
						$body .= "<input readonly type='text' id='start_date_".$students['id']."'  style='border:0;width:100%;' name='start_date_".$students['id']."' value='".$stats['start_date']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Topic</th><td width='300px'>";
						if(isset($stats['topic'])){
						$body .= "<input readonly type='text' id='topic_".$students['id']."'  style='border:0;width:100%;' name='topic_".$students['id']."' value='".$stats['topic']."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Faculty Name</th><td width='300px'>";
						if(isset($faculty_name)){
						$body .= "<input readonly type='text' id='name_".$students['id']."'  style='border:0;width:100%;' name='name_".$students['id']."' value='".$faculty_name."'></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr><tr><th style='background-color:#16365C;color:#fff;'>Remarks</th><td width='300px'>";
						if(isset($stats['name'])){
						$body .= "<input type='text' id='remarks_".$students['id']."'  style='border:0;width:100%;' name='remarks_".$students['id']."' value=''></input>";
						}else{
						$body .= 'N/A';
						}
						$body .="</td></tr></table><br/>";

						$body .="<p>Please feel free to contact the following persons in case of any problem.<table border='1' cellspacing='0px'><tr><th style='background-color:#16365C;color:#fff;'>For Schedule</th><td>";

						$body .= 'Customer Support';

						$body .="</td><td>";

						$body .= 'care@edupristine.com';

						$body .="</td><td>";

						$body .= '1800 200 5835';

						$body .="</td></tr>";
						$body .="</table></p>";

						$body .="		
						<p>Regards,<br/><strong>";

						$body .= 'Edupristine';

						$body .="</strong></p>";
						echo $body;


						echo "<input hidden type='text' name='batchcode_".$students['id']."' id='batchcode_".$students['id']."' value='".$stats['batch_code']."' />";
						}
						}
						echo "<input hidden  type='text' name='wsdate_".$students['id']."' id='wsdate_".$students['id']."' value='".$workshop_date."' />";


						?>
						<input hidden type="text" name="bodycontent_<?php echo $students['id'];?>" id="bodycontent_<?php echo $students['id'];?>" value="<?php echo $body;?>" />

						<input hidden type="text" name="subjectcontent_<?php echo $students['id'];?>" id="subjectcontent_<?php echo $students['id'];?>" value="<?php echo $subject;?>" />

						</div>
						<?php
						if($status=="Blank")
						{
						?>
						<input type="text" id="newsubject_<?php echo $students['id'];?>" name="newsubject_<?php echo $students['id'];?>" class="form-control" placeholder="Subject"></input><br>
						<textarea id="mailbody_<?php echo $students['id'];?>" name="mailbody_<?php echo $students['id'];?>" style="width:100%;height:400px;"></textarea>
						<?php
						}
						?>
						
						</td>

						</tr>
						</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					<button onClick="resendmail(<?php echo $students['id']; ?>);" type="button" class="btn btn-primary">Send</button>
				</div>
			</div>
		</div>
	</div>
	<?php	

}
?>


<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

function resendmail(val)
{
  var newemail = document.getElementById('newemail_'+val).value;		
  var statusdisplay = document.getElementById('statusdisplay_'+val).value;	
  if(statusdisplay=='Blank')
  {
	var newsubject = document.getElementById('newsubject_'+val).value;
	var mailbody = document.getElementById('mailbody_'+val).value;  	
  }
  else
  {
	  var remarks = document.getElementById('remarks_'+val).value;
  }
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Schedule Mail resent Successfully.");
				submit_button_clicked = '';
				$('#student_'+val).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Mail Sendind Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Mail Sending Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'form_handlers/resend_schedule_mail.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	
   if(statusdisplay=='Blank')
   {
	xmlhttp.send('sid='+fixEscape(val)+'&newsubject='+fixEscape(newsubject)+'&mailbody='+mailbody+'&newemail='+newemail);
   }
   else
   {
	xmlhttp.send('sid='+fixEscape(val)+'&remarks='+fixEscape(remarks)+'&newemail='+newemail);   
   }
	
}

$(document).ready(function(){

   // Datapicker 
   $( ".datepicker" ).datepicker({
      "dateFormat": "yy-mm-dd"
   });

   // DataTable
   var dataTable = $('#empTable').DataTable({
     'processing': true,
     'serverSide': true,
     'serverMethod': 'post',
     'searching': true, // Set false to Remove default Search Control
     'ajax': {
       'url':'ajax/resendschedule.php',
       'data': function(data){
          // Read values
          var from_date = $('#search_fromdate').val();
          var to_date = $('#search_todate').val();
		  
		  var status = $('#status').val();

          // Append to data
          data.searchByFromdate = from_date;
          data.searchByTodate = to_date;
		  if(status!='')
		  {
			data.status = status;  
		  }
       }
     },
	'columns': [
	{ data: 'batch_code' },
	{ data: 'workshop_date'},
	{ data: 'student_email'},
	{ data: 'mailtype' },
	{ data: 'id', name: 'id',
        fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
            $(nTd).html("<button type='button' class='btn btn-bold btn-label-brand btn-sm' data-toggle='modal' data-target='#student_"+oData.id+"'>Resend</button>");
        }
    },
	]
  });


  // Search button
  $('#btn_search').click(function(){
     dataTable.draw();
  });

});
</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
	<!-- Datatable CSS -->
<link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery UI CSS -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">

<!-- jQuery Library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- jQuery UI JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<!-- Datatable JS -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>