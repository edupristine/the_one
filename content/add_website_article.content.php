<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php

$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$con_one=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');


	
	if(isset($_GET['dupname1']))
	{
		$msg = "Article Posting Failed. Similiar Posting  already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Article Posting Failed. Similiar Posting already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Article Posting Failed.Mandatory Fields missing.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Article Posting Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Article Posted Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add New Website Article
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/article_posting.php'>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-12">
	<label>Article URL Link</label>
	<input type="text" class="form-control" id="arct_url" name="arct_url"required >
	
</div>
</div>


<div class="form-group row">
<div class="col-lg-12">
<label>Synopsis:</label>
<textarea class="form-control" id="synopsis" name="synopsis"></textarea>
</div>
</div>


<div class="form-group row">

<div class="col-lg-6">
<label>Select Job Type</label>
<select id="is_published" class="form-control" name="is_published" >
<option value="1" >Published</option>
<option value="0">Unpublished</option>
</select >
</div>

<div class="col-lg-6">
<label>Published On</label>
<input type="date" class="form-control" id="published_on"  name="published_on" value = "<?php echo date('Y-m-d'); ?>" required>

</div>
</div>


</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
			<center><button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button></center>
		</div>
	</div>
</div>
</div>
</form>


<?php /*
<div class="kt-portlet__body">
<input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="">
<thead>
<tr>
<th >#</th>
<th >Location Name</th>
<th ><center>Action</center></th>
</tr>
</thead>
<tbody>
<?php
$i=1;
foreach($rows as $locs){
	
?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $locs['location_name'];?></td>
<td><center><a href="#" class="kt-nav__link-icon flaticon2-writing" data-toggle="modal" data-target="#approverefund_<?php echo $locs['id']; ?>" ></a></center></td>
</tr>
<?php
$i++;
}
?>

</tbody>
</table>
</div>
<?php
*/
?>

</div>
</div>
</div>

</div>

<?php
?>

<script type="text/javascript">
$(document).ready(function () {
	$('#synopsis').jqxEditor({
		height: "400px",
		width: '100%'
	});
	
});
</script>