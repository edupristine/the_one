<!-- end:: Subheader -->
<!-- begin:: Content -->

<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$account_no = get_get_value('account_no');
$course = get_get_value('course');
$city = get_get_value('city');

	$query_courses = "SELECT id,course_name from courses";
	$result_courses = Select($query_courses,$conn);


if(isset($_GET['paramsmissing']))
{
$msg = "Receivables Grievances Submission Failed. Student Name is Mandatory Field.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['dberror']))
{
$msg = "Receivables Grievances Submission Failed. Unknown Error Contact Administrator.";
$altype = "danger";
$icontype = "flaticon2-cross";
}
elseif(isset($_GET['success']))
{
$msg = "Receivables Grievances Submitted Successfully.";
$altype = "success";
$icontype = "flaticon2-check-mark";
}
else
{
$msg = "";
}
if($msg != '')
{
?>
<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
<div class="alert-text"><?php echo $msg; ?></div>
<div class="alert-close">
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true"><i class="la la-close"></i></span>
</button>
</div>
</div>
<?php } ?>
<div class="row" >
<div class="col-lg-12" >
<!--begin::Portlet-->
<div class="kt-portlet" style="background-color:#088FD7;">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
<strong style="color:#fff;">EDUPRISTINE</strong>
</h3>
</div>
</div>
</div>
</div>
</div>


<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Book Request Portal 
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action='form_handlers/books_request.php'>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-6">
<label>Enter Your Registered Email ID:</label>
<input type="text" class="form-control" id="email_id" name="email_id" value=""  >
</div>
<div class="col-lg-6">
<label class="">Name:</label>
<input type="text" class="form-control" id="student_name"  name="student_name"required>
</div>
</div>


<div class="form-group row">
<div class="col-lg-6">
<label class="">Registered Course/s:</label>
<select class="form-control kt-select2" id="kt_select2_2" name="param[]" multiple="multiple">
	<?php 
		foreach($result_courses['rows'] as $course)
		{
			
				echo '<option value="'.$course['id'].'">'.$course['course_name'].'</option>';

		}
	?>
</select>
</div>
<div class="col-lg-6">
<label class="">Registered Sub Course/s:</label>
<select class="form-control kt-select2" id="kt_select2_3" name="param[]" multiple="multiple">
	<?php 
		foreach($result_courses['rows'] as $course)
		{
			echo '<optgroup label="'.$course['course_name'].'">';
			
			$query_subcourses = "SELECT id,subcourse_name from sub_courses where course_id = ".$course['id'];
			$result_subcourses = Select($query_subcourses,$conn);
			
			foreach($result_subcourses['rows'] as $subcourse)
			{
				echo '<option value="'.$subcourse['id'].'">'.$subcourse['subcourse_name'].'</option>';
			}
			
			echo '</optgroup>';
		}
	?>
</select>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label class="">Any Message:</label>
<textarea type="text" class="form-control" id="concerns"  name="concerns" rows="4"></textarea>
</div>
</div>
</div>

<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
<div class="row">
<div class="col-lg-12">
<center><button type="submit" class="btn btn-success" onclick="return Validate();">Submit</button>
</div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<script>
	function Validate()
	{
		var student_name=document.getElementById('student_name').value;
		var email=document.getElementById('email').value;
		var phone=document.getElementById('phone').value;
		var concerns=document.getElementById('concerns').value;
		
		
		if(student_name=='')
		{
			alert("Name is mandatory!");
			student_name.focus();
		}
		
		if(email=='')
		{
			alert("Email is mandatory!");
			email.focus();
		}else
		{
			 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
            if (reg.test(email) == false) 
            {
                alert('Please enter a valid Email!');
				email.focus();
				submit_button_clicked = '';
                return (false);
            }
		}
		
		if(phone=='')
		{
			alert("Mobile No. is mandatory!");
			phone.focus();
			submit_button_clicked = '';
		}
		
		if(concerns=='')
		{
			alert("Concerns Details are mandatory!");
			submit_button_clicked = '';
			return (false);
		}
		
	}
</script>
