<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['dupname1']))
	{
		$msg = "Refund Requirement already raised.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Refund Requirement cannot be processed. Mandatory Fields missing";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['excessamt']))
	{
		$msg = "Refund Requirement cannot be processed. Excess Refund amount";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Refund Requirement cannot be processed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Refund Requirement  processed Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<!--begin::Portlet-->

<div class="kt-portlet__body">
<div class="row">
<div class="col-lg-12">
<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
Student Refunds
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">

<div class="form-group row">
<div class="col-lg-4">
<label>Account No:</label>
<input  class="form-control" name="accountno_1" id="accountno_1" style="width:100%;" type="text"  value=""/>
</div>
<div class="col-lg-4" style="padding-top:25px;">
<label></label>
<button type="button" id="updateoff"  name="updateoff" class="btn btn-success" onclick="RECSelect();">Get Details</button>
</div>
<div class="col-lg-4" >
</div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='form_handlers/student_refunds.php'>
	<div class="kt-portlet kt-portlet--tabs">
		<div class="kt-portlet__head">
			<div class="kt-portlet__head-label">
				<h3 class="kt-portlet__head-title">
					Details
				</h3>
			</div>
			<div class="kt-portlet__head-toolbar">
				<ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-right nav-tabs-line-brand" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab">
							Organization Info
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab">
							Payment Info
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab">
							Additional Info
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_4" role="tab">
							Refund Calculator
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="kt-portlet__body">
			<div class="tab-content">
				<div class="tab-pane active" id="kt_portlet_tab_1_1">
				<div class="form-group row">
				<div class="col-lg-6">
				<label>Student Name:</label>
				<input   class="form-control" name="studentname_1" id="studentname_1"  style="width:100%;" type="text" readonly />
				<input    class="form-control" name="recs_1" id="recs_1" style="width:100%;" type="text" hidden />
				</div>
				<div class="col-lg-6">
				<label class="">Counselor Name:</label>
				<input    class="form-control" name="assignedto_1" id="assignedto_1" style="width:100%;" type="text"  value="" readonly />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label>Email ID :</label>
				<input    class="form-control" name="emailid_1" id="emailid_1" style="width:100%;" type="text"  value=""  />
				</div>
				<div class="col-lg-6">
				<label class="">Mobile Number:</label>
				<input   class="form-control" name="mobileno_1" id="mobileno_1"  style="width:100%;" type="text"  />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label>Batch Date :</label>
				<input    class="form-control" name="batchdate_1" id="batchdate_1" style="width:100%;" type="text"  value="" readonly />
			    </div>
				<div class="col-lg-6">
				<label class="">Batch Location:</label>
				<input   class="form-control" name="batchlocation_1" id="batchlocation_1"  style="width:100%;" type="text" readonly />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label>Vertical Enrolled :</label>
			    <select  class="form-control" id="ver_enrolled"  name="ver_enrolled" >
				<option value="0">Select</option>
				<option value="Finance">Finance</option>
				<option value="Marketing">Marketing</option>
				<option value="Healthcare">Healthcare</option>
				<option value="Accounting">Accounting</option>
				<option value="Analytics">Analytics</option>
				</select >
				</div>
				<div class="col-lg-6">
				<label class="">Course Enrolled:</label>
				<select  class="form-control" id="course_enrolled"  name="course_enrolled" >
				<option value="0">Select</option>
				<option value="Digital Marketing">Digital Marketing</option>
				<option value="FRM">FRM</option>
				<option value="USMLE - Clinical Rotations">USMLE - Clinical Rotations</option>
				<option value="USMLE - i20">USMLE - i20</option>
				<option value="Financial Modeling">Financial Modeling</option>
				<option value="CPA">CPA</option>
				<option value="ACCA">ACCA</option>
				<option value="CMA">CMA</option>
				<option value="CFA">CFA</option>
				<option value="Business Analytics">Business Analytics</option>
				<option value="Big Data / Hadoop">Big Data / Hadoop</option>
				<option value="Business Accounting & Taxation (BAT)">Business Accounting & Taxation (BAT)</option>
				<option value="USMLE Guide MD">USMLE Guide MD</option>
				<option value="USMLE India LVC">USMLE India LVC</option>
				</select >
				</div>
				</div>
				
				
				</div>
				<div class="tab-pane" id="kt_portlet_tab_1_2">
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Fees Paid.:</label>
				<input  class="form-control" name="paid_1" id="paid_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-6">
				<label class="">Discount:</label>
				<input   class="form-control"  name="discount_1" id="discount_1"  style="width:100%;" type="text" readonly   />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Offered Price:</label>
				<input  class="form-control" name="offeredprice_1" id="offeredprice_1"  style="width:100%;" type="text" readonly  />
				</div>
				<div class="col-lg-6">
				<label class="">Balance Amount:</label>
				<input   class="form-control"  name="balance_1" id="balance_1"  style="width:100%;" type="text" readonly   />
				</div>
				</div>
				
				
				<div class="form-group row">
				<table class="table table-striped- table-bordered table-hover table-checkable" id="">
				<thead>
				<tr >
	
				<th >Transactions</th>
				<th >Amount</th>
				<th >Date</th>
				<th >Mode</th>
				<th >Validated</th>
				<th >Transaction Id /Details</th>
				</tr>
                </thead>
				<tbody>
				<tr>
			
				<td>Transaction 1</td>
				<td ><input   class="form-control"  name="t_amt_1" id="t_amt_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_1" id="t_dat_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_1" id="t_mod_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_1" id="t_valid_1"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_1" id="t_tra_1"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Transaction 2</td>
				<td ><input   class="form-control"  name="t_amt_2" id="t_amt_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_2" id="t_dat_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_2" id="t_mod_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_2" id="t_valid_2"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_2" id="t_tra_2"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Transaction 3</td>
				<td ><input   class="form-control"  name="t_amt_3" id="t_amt_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_3" id="t_dat_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_3" id="t_mod_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_3" id="t_valid_3"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_3" id="t_tra_3"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Transaction 4</td>
				<td ><input   class="form-control"  name="t_amt_4" id="t_amt_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_4" id="t_dat_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_4" id="t_mod_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_4" id="t_valid_4"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_4" id="t_tra_4"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Transaction 5</td>
				<td ><input   class="form-control"  name="t_amt_5" id="t_amt_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_5" id="t_dat_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_5" id="t_mod_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_5" id="t_valid_5"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_5" id="t_tra_5"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				<tr>
			
				<td>Transaction 6</td>
				<td ><input   class="form-control"  name="t_amt_6" id="t_amt_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_dat_6" id="t_dat_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_mod_6" id="t_mod_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_valid_6" id="t_valid_6"  style="width:50%;border:0;" type="text" readonly   /></td>
				<td><input   class="form-control"  name="t_tra_6" id="t_tra_6"  style="width:100%;border:0;" type="text" readonly   /></td>
				</tr>
				</tbody>
				</table>
				</div>
				
				<div class="form-group row" >
				<div class="col-lg-6">
				<label class="">Total Validated Amount:</label>
				<input  class="form-control" name="tot_val_amt" id="tot_val_amt"  style="width:100%;" type="text" readonly  />
				</div>
				
				<div class="col-lg-6">
				<label class="">Fees Paid.:</label>
				<input  class="form-control" name="fpaid_1" id="fpaid_1"  style="width:100%;" type="text" readonly  />
				</div>
				</div>
				
				</div>
				
				<div class="tab-pane" id="kt_portlet_tab_1_3">
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Beneficiary Name in Bank:</label>
				<input  class="form-control" name="benf_1" id="benf_1"  style="width:100%;" type="text"   />
				</div>
				<div class="col-lg-6">
				<label class="">Bank Name:</label>
				<input   class="form-control"  name="bankname_1" id="bankname_1"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Bank A/c Number:</label>
				<input  class="form-control" name="accno_1" id="accno_1"  style="width:100%;" type="text"   />
				</div>
				<div class="col-lg-6">
				<label class="">IFSC Code:</label>
				<input   class="form-control"  name="ifsc_1" id="ifsc_1"  style="width:100%;" type="text"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Branch Location:</label>
				<input  class="form-control" name="banklocation_1" id="banklocation_1"  style="width:100%;" type="text"   />
				</div>
				
				<div class="col-lg-6">
				<label class="">Refund Category:</label>
				<select  class="form-control" id="refund_category"  name="refund_category" onchange="displaybankdetails(this.value)">
				<option value="0">Select</option>
				<option value="Direct Refund">Direct Refund</option>
				<option value="Neev Finance">Neev Finance - Refund</option>
				<option value="Liqui Loan">Liqui Loan - Refund</option>
				<option value="ABFL">ABFL - Refund</option>
				<option value="Avanse">Avanse - Refund</option>
				<option value="Refund As Exception">Refund As Exception</option>
				</select >
				</div>
				
				</div>
				
				<div class="form-group row">
				<div class="col-lg-12">
				<label class="">Reason for Refund:</label>
				<textarea type="text" class="form-control"  name="comment_1" id="comment_1" rows="4" required></textarea>
				</div>
				</div>
				</div>
				
				
				<div class="tab-pane" id="kt_portlet_tab_1_4">
				<div class="form-group row">
				<div class="col-lg-4">
				<label class="">Course Fee:</label>
				<input  class="form-control" name="course_fee" id="course_fee"  style="width:100%;" type="text"  readonly  />
				</div>
				<div class="col-lg-4">
				<label class="">Total Classes:</label>
				<input   class="form-control"  name="total_classes" id="total_classes"  style="width:100%;" type="text"  onblur="costperclass(this.value);"  />
				</div>
				<div class="col-lg-4">
				<label class="">Cost Per Class:</label>
				<input   class="form-control"  name="cost_per_classes" id="cost_per_classes"  style="width:100%;" type="text" readonly   />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-4">
				<label class="">No of Classes conducted:</label>
				<input  class="form-control" name="no_of_classes" id="no_of_classes"  style="width:100%;" type="text"    />
				</div>
				<div class="col-lg-4">
				<label class="">Amount Paid:</label>
				<input   class="form-control"  name="amount_paid" id="amount_paid"  style="width:100%;" type="text" readonly   />
				</div>
				<div class="col-lg-4">
				<label class="">Administration Charges:</label>
				<input   class="form-control"  name="admin_charges" id="admin_charges"  style="width:100%;" type="text" value="6000"    />
				</div>
				</div>
				
				<div class="form-group row">
				<div class="col-lg-6">
				<label class="">Third Party Access Cost:</label>
				<input  class="form-control" name="third_party_cost" id="third_party_cost"  style="width:100%;" type="text"   />
				</div>
                <div class="col-lg-6" style="padding-top:25px;">
				<button id="calculate" name="calculate" type="button" class="btn btn-primary" onclick="calculateamtref();">Calculate</button>
				</div>
				</div>
				
				<div class="form-group row">
                <div class="col-lg-6">
				<label class=""><strong><font color="#ff0000;">Amount to be refunded:</font></strong></label>
				<input   class="form-control"  name="amtref_1" id="amtref_1"  style="width:100%;" type="text" required   />
				</div>
				</div>
				
				
				</div>
				
				
				
			</div>
		</div>
	</div>

	<!--end::Portlet-->
	<div class="kt-portlet__foot kt-portlet__foot--fit-x">
	<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-5"></div>
		<div class="col-lg-7">
			
			<button id="approve" name="approve" type="submit" class="btn btn-success">Approve</button>
			<button id="reject" name="reject" type="submit" class="btn btn-warning">Reject</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
	</div>
	</div>
</form>
</div>

<?php
?>
<script>
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function RECSelect()
{
	var val = document.getElementById("accountno_1").value;
	if (window.XMLHttpRequest) {
	xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById('recs_1').value = output["account_no"];
			document.getElementById('offeredprice_1').value = output["offered_price"];
			document.getElementById('course_fee').value = output["offered_price"];
			document.getElementById('paid_1').value = output["Paid"];
			document.getElementById('fpaid_1').value = output["Paid"];
			document.getElementById('amount_paid').value = output["Paid"];
			document.getElementById('balance_1').value = output["Balance"];
			var str = output["discount"];
            var res = str.split("|##|");
			document.getElementById('discount_1').value = res;
			document.getElementById('studentname_1').value = output["studentname"];
			document.getElementById('assignedto_1').value = output["assigned_user"];
			document.getElementById('emailid_1').value = output["email"];
			document.getElementById('mobileno_1').value = output["phone"];
			document.getElementById('batchdate_1').value = output["batch_date"];
			document.getElementById('batchlocation_1').value = output["batch_location"];
			document.getElementById('t_amt_1').value = output["t_amt_1"];
			document.getElementById('t_amt_2').value = output["t_amt_2"];
			document.getElementById('t_amt_3').value = output["t_amt_3"];
			document.getElementById('t_amt_4').value = output["t_amt_4"];
			document.getElementById('t_amt_5').value = output["t_amt_5"];
			document.getElementById('t_amt_6').value = output["t_amt_6"];
			document.getElementById('t_dat_1').value = output["t_dat_1"];
			document.getElementById('t_dat_2').value = output["t_dat_2"];
			document.getElementById('t_dat_3').value = output["t_dat_3"];
			document.getElementById('t_dat_4').value = output["t_dat_4"];
			document.getElementById('t_dat_5').value = output["t_dat_5"];
			document.getElementById('t_dat_6').value = output["t_dat_6"];
			document.getElementById('t_mod_1').value = output["t_mod_1"];
			document.getElementById('t_mod_2').value = output["t_mod_2"];
			document.getElementById('t_mod_3').value = output["t_mod_3"];
			document.getElementById('t_mod_4').value = output["t_mod_4"];
			document.getElementById('t_mod_5').value = output["t_mod_5"];
			document.getElementById('t_mod_6').value = output["t_mod_6"];
			document.getElementById('t_tra_1').value = output["t_tra_1"];
			document.getElementById('t_tra_2').value = output["t_tra_2"];
			document.getElementById('t_tra_3').value = output["t_tra_3"];	
			document.getElementById('t_tra_4').value = output["t_tra_4"];
			document.getElementById('t_tra_5').value = output["t_tra_5"];
			document.getElementById('t_tra_6').value = output["t_tra_6"];
			document.getElementById('total_classes').value = "";
			document.getElementById('no_of_classes').value = "";
			document.getElementById('amtref_1').value = "";
			
			var course = output["course"];
			var finalpaid = output["offered_price"];
			var third_party="";
			if((course=='CPA')||(course=='CMA'))
			{
				third_party = Math.round(((finalpaid/1.18)*31)/100);
				document.getElementById('third_party_cost').value = third_party;
			}
			
			var totvalidatedamt=0;
			var valid1="";
			var valid2="";
			var valid3="";
			var valid4="";
			var valid5="";
			var valid6="";
			if(output["t_valid_1"]==1)
			{
			valid1="Yes";	
			totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_1"]);
			}
			else
			{
			valid1="NA";		
			}
			
			if(output["t_valid_2"]==1)
			{
			valid2="Yes";	
			totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_2"]);
			}
			else
			{
			valid2="NA";		
			}
			
			if(output["t_valid_3"]==1)
			{
			valid3="Yes";
            totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_3"]);			
			}
			else
			{
			valid3="NA";		
			}
			
			if(output["t_valid_4"]==1)
			{
			valid4="Yes";
            totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_4"]);			
			}
			else
			{
			valid4="NA";		
			}
			if(output["t_valid_5"]==1)
			{
			valid5="Yes";
            totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_5"]);			
			}
			else
			{
			valid5="NA";		
			}
			if(output["t_valid_6"]==1)
			{
			valid6="Yes";
            totvalidatedamt=totvalidatedamt + parseInt(output["t_amt_6"]);			
			}
			else
			{
			valid6="NA";		
			}
			
			document.getElementById('t_valid_1').value = valid1;
			document.getElementById('t_valid_2').value = valid2;
			document.getElementById('t_valid_3').value = valid3;
			document.getElementById('t_valid_4').value = valid4;
			document.getElementById('t_valid_5').value = valid5;
			document.getElementById('t_valid_6').value = valid6;
            document.getElementById('tot_val_amt').value = totvalidatedamt;
			
		
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_refunddetails.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}


function costperclass(val)
{
	var cost=document.getElementById('course_fee').value;
	document.getElementById('cost_per_classes').value = (cost/val).toFixed(2);
	
}


function calculateamtref()
{
var amtpd = document.getElementById('amount_paid').value;
var adchrg=document.getElementById('admin_charges').value;
var cpc=document.getElementById('cost_per_classes').value;
var counducted_classes=document.getElementById('no_of_classes').value;
var thirdparty=document.getElementById('third_party_cost').value;

document.getElementById('amtref_1').value = Math.round(((amtpd-adchrg)-(counducted_classes*cpc)-thirdparty).toFixed(2));
}

function displaybankdetails(val)
{
if(val=='Neev Finance')
{
document.getElementById('benf_1').value='NEEV CREDIT PVT LTD';
document.getElementById('bankname_1').value='YES BANK';
document.getElementById('accno_1').value='019063700001121';
document.getElementById('ifsc_1').value='YESB0000190';
document.getElementById('banklocation_1').value='KOLKATA';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;
}else if (val=='Liqui Loan')
{
document.getElementById('benf_1').value='NDX P2P Pvt Ltd Borrowers Repayment Account';
document.getElementById('bankname_1').value='IDBI Bank';
document.getElementById('accno_1').value='0004102000039899';
document.getElementById('ifsc_1').value='IBKL0000004';
document.getElementById('banklocation_1').value='Nariman Point';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else if (val=='ABFL')
{
document.getElementById('benf_1').value='Aditya Birla Finance Limited';
document.getElementById('bankname_1').value='HDFC Bank';
document.getElementById('accno_1').value='00600350111290';
document.getElementById('ifsc_1').value='HDFC0000060';
document.getElementById('banklocation_1').value='HDFC Bank Limited, Fort Branch, Mumbai ';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else if (val=='Avanse')
{
document.getElementById('benf_1').value='BLUEBEAR TECHNOLOGY PRIVATE LIMITED';
document.getElementById('bankname_1').value='ICICI Bank';
document.getElementById('accno_1').value='662905600314';
document.getElementById('ifsc_1').value='ICIC0006629';
document.getElementById('banklocation_1').value='Janakuri, Delhi';

document.getElementById('benf_1').readOnly = true;
document.getElementById('bankname_1').readOnly = true;
document.getElementById('accno_1').readOnly = true;
document.getElementById('ifsc_1').readOnly = true;
document.getElementById('banklocation_1').readOnly = true;	
}
else
{
document.getElementById('benf_1').value='';
document.getElementById('bankname_1').value='';
document.getElementById('accno_1').value='';
document.getElementById('ifsc_1').value='';
document.getElementById('banklocation_1').value='';

document.getElementById('benf_1').readOnly = false;
document.getElementById('bankname_1').readOnly = false;
document.getElementById('accno_1').readOnly = false;
document.getElementById('ifsc_1').readOnly = false;
document.getElementById('banklocation_1').readOnly = false;	
}
}
</script>