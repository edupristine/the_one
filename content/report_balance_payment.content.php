<?php
$startdate='2019-04-01';
$enddate='2020-02-29';
$date = new DateTime();
$date->modify("last day of previous month");
$enddate= $date->format("Y-m-d");

$col1startdate=date('Y-m-01');
$col1enddate=date('Y-m-07');

$col2startdate=date('Y-m-08');
$col2enddate=date('Y-m-14');

$col3startdate=date('Y-m-15');
$col3enddate=date('Y-m-21');

$col4startdate=date('Y-m-22');
$col4enddate=date('Y-m-t');

$today_date=date('d');

if($today_date=='1')
{
	$col=4;
}
elseif($today_date=='8')
{
	$col=1;
}
elseif($today_date=='15')
{
	$col=2;
}
elseif(($today_date=='18')||($today_date=='19')||($today_date=='20')||($today_date=='21')||($today_date=='22')||($today_date=='23')||($today_date=='24'))
{
	$col=4;
}
else
{
	exit;
}

$rec_period_startdate=$_GET['rps'];
$rec_period_enddate=$_GET['rpe'];

$var1month=explode('-',$startdate);
$startmonth=intval($var1month[1]);

$var2month=explode('-',$enddate);
$endmonth=intval($var2month[1]);


$Months = array( 
    "1" =>  "Jan", 
    "2" =>  "Feb", 
    "3" =>  "Mar",
	"4" =>  "Apr", 
    "5" =>  "May", 
    "6" =>  "Jun",
	"7" =>  "Jul", 
    "8" =>  "Aug", 
    "9" =>  "Sep",
	"10" =>  "Oct", 
    "11" =>  "Nov", 
    "12" =>  "Dec"
	); 
      




$course_query="SELECT distinct(sc.cf_918) as `course`
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
WHERE DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."' AND sc.cf_936 > sc.cf_938 AND ce.smownerid != 3920 
AND sc.cf_918 not in ('CIB','Packages','USMLE Guide MD')
group by sc.cf_918,MONTH(ce.createdtime)";


$result_course = mysqli_query($conn1,$course_query);
while($resultcourse = mysqli_fetch_assoc($result_course)) {
    $courses[] = $resultcourse;
}
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Balance Payment (Course Wise) from 1st Apr - 29 Feb 2020
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr >
												<th >#</th>
												<th  >Course</th>
												<th style='text-align:center;'>Receivables <br> <?php echo date('01-M-y',strtotime($startdate))."-".date('30-M-y',strtotime($enddate)) ?></th>
											
												
												
												<?php
												
												if($col>=1)
												{
													echo "<th style='text-align:right;'>1st-7th ".date('M')."</th>";
												}
												if($col>=2)
												{
													echo "<th style='text-align:right;'>8th-14th ".date('M')."</th>";
												}
												if($col>=3)
												{
													echo "<th style='text-align:right;'>15th-21st ".date('M')."</th>";
												}
												if($col>=4)
												{
													echo "<th style='text-align:right;'>22nd ".date('M')." Onwards</th>";
												}
												
												?>
												<th style='text-align:right;'>Collected this Month</th>
												<th style='text-align:right;'>Current Receivables</th>
												
												
							                </tr>
											
										</thead>
										<tbody>
										<?php
											$i = 1;
											$grandtotal=0;
											$total17=0;
											$total814=0;
											$total1521=0;
											$total22=0;
											foreach($courses as $course) 
											{
												
											    $totthismonth=0;
											    $bal=0;
												$amt=0;
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><strong><?php echo $course['course']; ?></strong></td>
													<?php
													$count ="select
													SUM((sc.cf_936  - sc.cf_938)) as balance
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."' AND sc.cf_936 > sc.cf_938 
													AND  sc.cf_918='".$course['course']."' AND ce.smownerid != 3920 ";
													
													
													$result_count = mysqli_query($conn1,$count);
													$num_rows=mysqli_num_rows($result_count);
													$balcount = mysqli_fetch_assoc($result_count);	
													
													if($num_rows==0)
													{
														$bal=0;
														
													}
													else
													{
														$bal=$balcount['balance'];
													}
													echo "<td style='text-align:right;'>".$bal."</td>";
													$grandtotal=$grandtotal+$bal;
													if($col>=1)
													{
													$countpay ="select sum(paid) as `paid` from (
													
													SELECT sum(sc.cf_1313) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1365) BETWEEN '".$col1startdate."' AND '".$col1enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1323) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1367) BETWEEN '".$col1startdate."' AND '".$col1enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1333) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1369) BETWEEN '".$col1startdate."' AND '".$col1enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
												    AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1343) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1371) BETWEEN '".$col1startdate."' AND '".$col1enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1353) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1373) BETWEEN '".$col1startdate."' AND '".$col1enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 ) x";	

												    $result_countpay = mysqli_query($conn1,$countpay);	
													$balcountpay = mysqli_fetch_assoc($result_countpay);
													
													$numrows = mysqli_num_rows($result_countpay);
													if(($numrows == 0)||($balcountpay['paid']==''))
														$amt=0;
													else
														$amt=$balcountpay['paid'];
														
													echo "<td style='text-align:right;'>".$amt."</td>";    
													$totthismonth=$totthismonth+$amt;
													$total17=$total17+$amt;
													}
																											
													
													if($col>=2)
													{
													$countpay ="select sum(paid) as `paid` from (
													
													SELECT sum(sc.cf_1313) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1365) BETWEEN '".$col2startdate."' AND '".$col2enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1323) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1367) BETWEEN '".$col2startdate."' AND '".$col2enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1333) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1369) BETWEEN '".$col2startdate."' AND '".$col2enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
												    AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1343) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1371) BETWEEN '".$col2startdate."' AND '".$col2enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1353) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1373) BETWEEN '".$col2startdate."' AND '".$col2enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 ) x";	

												    $result_countpay = mysqli_query($conn1,$countpay);	
													$balcountpay = mysqli_fetch_assoc($result_countpay);
													$numrows = mysqli_num_rows($result_countpay);
													if(($numrows == 0)||($balcountpay['paid']==''))
														$amt=0;
													else
														$amt=$balcountpay['paid'];
														
													echo "<td style='text-align:right;'>".$amt."</td>";  
													$totthismonth=$totthismonth+$amt;
													$total814=$total814+$amt;
													}
													if($col>=3)
													{
													$countpay ="select sum(paid) as `paid` from (
													
													SELECT sum(sc.cf_1313) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1365) BETWEEN '".$col3startdate."' AND '".$col3enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1323) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1367) BETWEEN '".$col3startdate."' AND '".$col3enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1333) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1369) BETWEEN '".$col3startdate."' AND '".$col3enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
												    AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1343) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1371) BETWEEN '".$col3startdate."' AND '".$col3enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1353) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1373) BETWEEN '".$col3startdate."' AND '".$col3enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 ) x";	

												    $result_countpay = mysqli_query($conn1,$countpay);	
													$balcountpay = mysqli_fetch_assoc($result_countpay);
													$numrows = mysqli_num_rows($result_countpay);
													if(($numrows == 0)||($balcountpay['paid']==''))
														$amt=0;
													else
														$amt=$balcountpay['paid'];
														
													echo "<td style='text-align:right;'>".$amt."</td>";  
													$totthismonth=$totthismonth+$amt;
													$total1521=$total1521+$amt;
													}
													if($col>=4)
													{
													$countpay ="select sum(paid) as `paid` from (
													
													SELECT sum(sc.cf_1313) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1365) BETWEEN '".$col4startdate."' AND '".$col4enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1323) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1367) BETWEEN '".$col4startdate."' AND '".$col4enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1333) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1369) BETWEEN '".$col4startdate."' AND '".$col4enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
												    AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1343) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1371) BETWEEN '".$col4startdate."' AND '".$col4enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 
													UNION
													SELECT sum(sc.cf_1353) as paid
													FROM vtiger_account ac
													LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
													INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
													WHERE
													DATE(sc.cf_1373) BETWEEN '".$col4startdate."' AND '".$col4enddate."' AND
													DATE(ce.createdtime) BETWEEN '".$startdate."' AND '".$enddate."'
													AND  sc.cf_918='".$course['course']."'
													AND ce.smownerid != 3920 ) x";	

												    $result_countpay = mysqli_query($conn1,$countpay);	
													$balcountpay = mysqli_fetch_assoc($result_countpay);
													$numrows = mysqli_num_rows($result_countpay);
													if(($numrows == 0)||($balcountpay['paid']==''))
														$amt=0;
													else
														$amt=$balcountpay['paid'];
														
													echo "<td style='text-align:right;'>".$amt."</td>";  
													$totthismonth=$totthismonth+$amt;
													$total22=$total22+$amt;
													}
													$totbal=$bal-$totthismonth;
													echo "<td style='text-align:right;'>".$totthismonth."</td>";
													echo "<td style='text-align:right;'>".$totbal."</td>";
													
													
													?>
													
												
												</tr>
										<?php
                                           													
											$i++;
											
											}
										?>
											<tr>
											<td colspan=2>Total</td>
											<td style='text-align:right;'><?php echo $grandtotal; ?></td>
											<?php
												
												if($col>=1)
												{
													echo "<td style='text-align:right;'>".$total17."</td>";
												}
												if($col>=2)
												{
													echo "<td style='text-align:right;'>".$total814."</td>";
												}
												if($col>=3)
												{
													echo "<td style='text-align:right;'>".$total1521."</td>";
												}
												if($col>=4)
												{
													echo "<td style='text-align:right;'>".$total22."</td>";
												}
												$totthismonth=$total17+$total814+$total1521+$total22;
												$currentrecv=$grandtotal-$totthismonth;
												
												?>
												<td style='text-align:right;'><?php echo $totthismonth; ?></td>
												<td style='text-align:right;'><?php echo $currentrecv; ?></td>
											</tr>									
										</tbody>
									</table>
                                    
									<!--end: Datatable -->
								</div>
							</div>
</div>