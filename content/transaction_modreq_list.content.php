<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		var date_range = document.getElementById('date_range').value;
		window.location = 'transaction_modreq_list.php?filter_param='+filter_param+'&date_range='+date_range;
	}
	
	
</script>
<?php
    $team="";
	$user="";

	$query_userteams = "SELECT team_id,user_id FROM users_teams  where user_id ='".$_SESSION['USER_ID']."' and team_id in (1,7,8,10,11)";
	$result_team = Select($query_userteams,$conn);

    $team=$result_team['rows'][0]['team_id'];
	$user=$result_team['rows'][0]['user_id'];

	$status = get_get_value('status');
	$date_range = get_get_value('date_range');
	
	$dtquery="";
	if($date_range != ""){
	$drange = explode(" / ",$date_range);
	$sdate = date('Y-m-d',strtotime($drange[0]));
	$edate = date('Y-m-d',strtotime($drange[1]));
	
	$dtquery = " and CONVERT(raised_on, DATE) between '".$sdate."' and '".$edate."' ";
	
	}

	


	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		
	if($_SESSION['USER_ID']==42)
	{
    $query_alltrans = "SELECT * FROM mod_paydetails_requests where id!=0   $dtquery  order by id desc";
	}else
	{	
    $query_alltrans = "SELECT * FROM mod_paydetails_requests where id!=0  and created_by ='".$_SESSION['USER_ID']."' $dtquery  order by id desc";
	}
	    
	}
	else
	{
		if($_SESSION['USER_ID']==42)
	    {
		$query_alltrans = "SELECT * FROM mod_paydetails_requests WHERE (student_name like '%".$filter_param."%' OR account_no like '%".$filter_param."%' )  $dtquery  ORDER BY id desc";
		}
		else
		{
		$query_alltrans = "SELECT * FROM mod_paydetails_requests WHERE (student_name like '%".$filter_param."%' OR account_no like '%".$filter_param."%' ) and created_by ='".$_SESSION['USER_ID']."' $dtquery  ORDER BY id desc";
		}
	}
	
   
	$result_alltrans = Select($query_alltrans,$conn);	
	
	
	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<input type='hidden' id='team' name='team' value='<?php echo $team;?>'>
<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-4">
<label>Date Range</label>
<div class="input-group" id="kt_daterangepicker_2">
<input type="text" class="form-control" id="date_range" readonly="" placeholder="Select date range" value="<?php echo $date_range;?>">
<div class="input-group-append">
<span class="input-group-text"><i class="la la-calendar-check-o"></i></span>
</div>
</div>
</div>
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Submit</button>
	
</div>
<?php 
if($_SESSION['USER_ID']==42)
{
?>
<div class="col-lg-2">
	<label class="">&nbsp;</label>
	<button onClick="get_export();" type="button" class="btn btn-primary form-control">Export</button>
</div>
<?php
}
?>

</div>


</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Transaction Details Modification Request List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<div class="col">
														<table><tr>
														<td>
														<!--<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Approval Status
																</button>
																<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																<a class="dropdown-item" href="transaction_modreq_list.php?status=1">Approved</a>
																<a class="dropdown-item" href="transaction_modreq_list.php?status=0">Rejected</a>
																<a class="dropdown-item" href="transaction_modreq_list.php?status=3">All</a>
																</div>
																
															</div>-->
															</td></tr></table>
														</div>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">
                              <input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th >#</th>
												<th >Student Name</th>
												<th >Org No.</th>
												<th >Details Type</th>
												<th >Old Value</th>
												<th >New Value</th>
												<th >RaisedOn</th>
												<?php if($_SESSION['USER_ID']==42)
	                                             {?>
												<th >Raised By</th>
												<?php
												 }?>
												<th >Reason</th>
												<th colspan ="2" ><center> Levels</center></th>
											    
												
												
											</tr>
											
											<tr>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
									            <th rowspan="2"></th>
											    <th rowspan="2"></th>
												<th rowspan="2"></th>
												<th rowspan="2"></th>
												<?php 
												if($_SESSION['USER_ID']==42)
	                                            {
			                                    ?>
												<th rowspan="2"></th>
												<?php
												}
												?>
												<th rowspan="2"></th>
												<th rowspan="2"><center> CEO</center></th>
												<th rowspan="2"><center> Accounts</center></th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											$days = 0;
											foreach($result_alltrans['rows'] as $student) 
											{	
												
												$now = time(); 
												$your_date = strtotime($student['created_at']);
												$datediff = $now - $your_date;
                                                $days = round($datediff / (60 * 60 * 24));
												$displayday="";
												if($days>90)
												{
												$displayday =  "<font color='red'><strong>$days</strong></font>";	
												}
												else
												{
                                                $displayday = "<font color='green'><strong>$days</strong></font>";
												}													
												
												
												$query_ceoapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM mod_pay_details rd, users us  WHERE rd.approver_id=us.id and rd.mod_req_id=".$student['id']." AND rd.approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	

                                                $query_accapproval = "SELECT value_updated FROM mod_paydetails_requests rd  WHERE  rd.id=".$student['id']." ";
												$result_acc = Select($query_accapproval,$conn);
												
												
                                                $ceodate = date('d-m-Y',strtotime($result_ceo['rows'][0]['created_at']));
												if($ceodate=='01-01-1970')
												{
												$ceodate = "NA";
												}
												else
												{
												$ceodate = $ceodate;	
												}
												
												/*$accdate = date('d-m-Y',strtotime($result_acc['rows'][0]['created_at']));
												if($accdate=='01-01-1970')
												{
												$accdate = "NA";
												}
												else
												{
												$accdate = $accdate;	
												}*/
												
												
												$supstatus="";
												
												$ceouser=$result_ceo['rows'][0]['user_name'];
												$accuser=$result_acc['rows'][0]['user_name'];
												

												$query_ceoapproval = "SELECT rd.approval_status,rd.created_at,us.user_name FROM mod_pay_details rd, users us  WHERE rd.approver_id=us.id and rd.mod_req_id=".$student['id']." AND rd.approver_id=4 ";
												$result_ceo = Select($query_ceoapproval,$conn);	


												$ceostatus="";
												if($result_ceo['rows'][0]['approval_status']=="1")
												{
												$ceostatus="<font color='green'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $ceodate Processed By : $ceouser' ><strong>Approved</strong></div></font>";	
												}
												else if($result_ceo['rows'][0]['approval_status']=="0")
												{
												$ceostatus="<font color='red'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Processed On : $ceodate Processed By : $ceouser'><strong>Rejected</strong></div></font>";		
												}
												else
												{
												$ceostatus="<font color='black'>NA</font>";	
												}

												$query_accapproval = "SELECT value_updated FROM mod_paydetails_requests rd  WHERE  rd.id=".$student['id']."";
												$result_acc = Select($query_accapproval,$conn);	


												$accstatus="";
												if($result_acc['rows'][0]['value_updated']=="1")
												{
												$accstatus="<font color='green'><strong>Updated</strong></font>";	
												}
												else if($result_acc['rows'][0]['value_updated']=="0")
												{
												$accstatus="<font color='red'><strong>Not Updated</strong></font>";		
												}
												else
												{
												$accstatus="<font color='black'>NA</font>";	
												}
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td><?php echo $student['account_no']; ?></td>
													<td><?php echo $student['trans_field']."->".$student['field_type']; ?></td>
													<td><?php echo $student['old_value']; ?></td>
													<td><?php echo $student['new_value']; ?></td>
													<td><?php echo date('d-m-Y',strtotime($student['created_at'])); ?></td>
													<?php if($_SESSION['USER_ID']==42)
	                                                  {
														  $query_rais = "SELECT user_name FROM users   WHERE  id=".$student['created_by']."";
												          $result_raise = Select($query_rais,$conn);	 
														  echo "<td>".$result_raise['rows'][0]['user_name']."</td>";
		                                              }
			                                         ?>
													<td><center><?php echo "<div class='kt-nav__link-icon flaticon-eye'  data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Reason : ".$student['modify_reason']." '></div>"; ?></center></td>
													<td><center><?php echo $ceostatus; ?></center></td>
													<td><center><?php echo $accstatus; ?></center></td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
                                </div>
							</div>
</div>
<script>
function get_export()
	{
		var date_range = document.getElementById('date_range').value;
		var filter_param = document.getElementById('search_param').value;
		//var selstatus = document.getElementById('selstatus').value;
		//var team = document.getElementById('team').value;
		window.location = 'content/transmoddet_exportdata.php?date_range='+date_range+'&param='+filter_param;
	}
	



</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>