<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'return_courier_list.php?filter_param='+filter_param;
	}
</script>
<?php
	$location_id=1;
	$course_id = get_get_value('cid');
		
	
	
	$qrys="";
	if($course_id!='')
	{
		$qrys="and s.course_id = ".$course_id."";
	}
	elseif($course_id=='')
	{
		$qrys="and s.course_id in (SELECT course_id from sub_courses where final_live=1)";
	}

	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		if($location_id==2)
	    {
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.org_loc_id,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid, sk.sku_name, sr.courier_provider,sr.courier_track_no,sr.courier_charges,sr.id as `record_id`,DATE_FORMAT(sr.created_at, '%d/%m/%Y') as sent,DATE_FORMAT(sc.created_at, '%d/%m/%Y') as raised_on,DATE_FORMAT(sr.returned_on, '%d/%m/%Y') as returned_on,sr.return_comment as `comment` FROM `students` s,student_couriers_request sc,student_couriers_records sr,issuance isu,skus sk WHERE   sc.student_id = s.id and sr.courier_req_id=sc.id and sk.id=isu.sku_id   and sr.re_raised=0 and sr.del_flag=1 ".$qrys." group by sr.id ORDER BY sr.created_at desc ";
	    }
		else
		{
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.org_loc_id,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid, sk.sku_name, sr.courier_provider,sr.courier_track_no,sr.courier_charges,sr.id as `record_id`,DATE_FORMAT(sr.created_at, '%d/%m/%Y') as sent,DATE_FORMAT(sc.created_at, '%d/%m/%Y') as raised_on,DATE_FORMAT(sr.returned_on, '%d/%m/%Y') as returned_on,sr.return_comment as `comment` FROM `students` s,student_couriers_request sc,student_couriers_records sr,issuance isu,skus sk WHERE   sc.student_id = s.id and sr.courier_req_id=sc.id and sk.id=isu.sku_id   and sr.re_raised=0 and sr.del_flag=1 and sc.sending_center = '".$location_id."' ".$qrys." group by sr.id ORDER BY sr.created_at desc";
	    }
	}
	else
	{
		
		$query_allstudents = "SELECT s.id,s.crm_id,s.student_name,s.student_contact,s.org_loc_id,s.student_email,s.course_id,s.subcourse_id,s.student_address,sc.courier_status, sc.courier_intiated_by, sc.id as courierid,sk.sku_name,sr.courier_provider,sr.courier_track_no,sr.courier_charges,sr.id as `record_id`,DATE_FORMAT(sr.created_at, '%d/%m/%Y') as sent,DATE_FORMAT(sc.created_at, '%d/%m/%Y') as raised_on,DATE_FORMAT(sr.returned_on, '%d/%m/%Y') as returned_on,sr.return_comment as `comment` FROM `students` s,student_couriers_request sc,student_couriers_records sr, issuance isu,skus sk WHERE   sc.student_id = s.id and sr.courier_req_id=sc.id  and sk.id=isu.sku_id  and sr.re_raised=0 and sr.del_flag=1 and (student_name like '%".$filter_param."%' OR crm_id like '%".$filter_param."%' OR student_email like '%".$filter_param."%' OR student_contact like '%".$filter_param."%') group by sr.id ORDER BY sr.created_at desc";
	}
	
	//echo $query_allstudents;
   
	$result_allstudents = Select($query_allstudents,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet">
<div class="form-group row" style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Return Courier List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
											
														<div class="col">
														<table><tr>
														<td>
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Courses
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php
																			$course = "SELECT id,course_name from courses where final_live=1  order by course_name ";
																			$result_course = Select($course,$conn);																		
																			foreach($result_course['rows'] as $course)
																			{	
																				
																				?>
																				<a class="dropdown-item" href="send_courier_list.php?cid=<?php echo $course['id'];?> "><?php echo $course['course_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td></tr></table>
														</div>
			
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Org No.</th>
												<th>Student Name</th>
												<!--<th >Sku Name</th>-->
									            <th>contact</th>
												<th>Courier Provider</th>
												<th style="width:100px">Tracking No.</th>
												<th style="width:100px">Raised On</th>
												<th style="width:100px">Sent On</th>
												<th style="width:100px">Returned On</th>
												<th colspan=2><center>Action</center></th>
												
												
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										       $return_comment=$student['comment'];
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['crm_id']; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<!--<td ><?php echo $student['sku_name']; ?></td>-->
													<td><?php echo $student['student_contact']; ?></td>
													<td><?php echo $student['courier_provider']; ?></td>
													<td><?php echo $student['courier_track_no']; ?></td>
													<td><?php echo $student['raised_on']; ?></td>
													<td><?php echo $student['sent']; ?></td>
													<td>
													<font color='red'><div data-toggle='kt-tooltip' data-placement='top' title='' data-original-title='Returning Comment : <?php echo $return_comment;?>'><strong><?php echo $student['returned_on']; ?></strong></div></font>
													
													</td>
													
													<td style="text-align:center;vertical-align:middle;">
													<div class="btn-group" role="group" aria-label="First group">
															<a href="#" class="kt-nav__link-icon flaticon-eye" data-toggle="modal" data-target="#courierview_<?php echo $student['record_id']; ?>" tooltip="View Courier Details"></a>
														</div>
													</td>
													<!--<td style="text-align:center;vertical-align:middle;"><a href="#" class="kt-nav__link-icon flaticon2-fast-back"  data-toggle="modal" data-target="#courdet_<?php echo $student['record_id']; ?>" tooltip="Edit Courier Details" > Edit</a></td>-->
													<td style="text-align:center;vertical-align:middle;"><a href="#" class="kt-nav__link-icon flaticon2-notification" tooltip="Set Notification"  data-toggle="modal" data-target="#re_raise_<?php echo $student['record_id']; ?>" ></a></td>
													<td style="text-align:center;vertical-align:middle;"><a href="#" class="kt-nav__link-icon flaticon2-fast-back" tooltip="Return Courier Package"  data-toggle="modal" data-target="#return_my_courier_<?php echo $student['record_id']; ?>" > Re-Raise Requirement</a></td>
                                                 <!--  <td><center><button onClick="Returncourier(<?php echo $student['id']; ?>,<?php echo $student['record_id']; ?>);" type="button" class="btn-sm btn-success btn-elevate btn-pill">Return Courier</button></center></td>-->
													
													
								
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and  isu.student_id = ".$student['id'];
										        $result_select_issue = Select($select_issue,$conn);
												
												
												$courier_int = "SELECT user_name from users where id = ".$student['courier_intiated_by'];
												$result_courier_int = Select($courier_int,$conn);
											?>
											<div class="modal fade" id="courdet_<?php echo $student['record_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Send Courier For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="200">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['record_id'];?>">

																		<thead>
																		<tr>
																		<td>Courier Provider :
																		</td>
																		<td>
																		<input type="text" id="courier_provider_<?php echo $student['record_id'];?>" name="courier_provider_<?php echo $student['record_id'];?>" value="<?php echo $student['courier_provider']; ?>" />
																		</td>
																		</tr>
																		<tr>
																		<td >
																		Courier Track No. :
																		</td>
																		<td>
																		<input type="text" id="courier_track_no_<?php echo $student['record_id'];?>" name="courier_track_no_<?php echo $student['record_id'];?>" value="<?php echo $student['courier_track_no']; ?>" />
																		</td>
																		</tr>
																		<!--<tr>
																		<td >
																		Courier Sent On. :
																		</td>
																		<td>
																		<?php echo $student['sent']; ?>
																		</td>
																		</tr>-->
																		<tr>
																		<td>
																		
																		</td>
																		<td>
																		<input type="hidden" id="courier_charges_<?php echo $student['record_id'];?>" name="courier_charges_<?php echo $student['record_id'];?>" value="<?php echo $student['courier_charges']; ?>" />
																		</td>
																		</tr>
																		</thead>
																
																	</table>
																
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Updatecourierdetails(<?php echo $student['record_id']; ?>,<?php echo $student['courierid']; ?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												
												
												
												<div class="modal fade" id="return_my_courier_<?php echo $student['record_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content" style="width:600px;">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Reraise Requirement For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" >
																			
																<?php
																	
																	$query_req="select * from student_couriers_request where student_id=".$student['id']." and id=".$student['courierid'] ;
                                                                    $result_req=Select($query_req,$conn);	
																	foreach($result_req['rows'] as $reqs)
																	{
																	$query_rec="select *,DATE_FORMAT(created_at, '%d/%m/%Y') as sent from student_couriers_records where courier_req_id=".$reqs['id']." and id=".$student['record_id']."";
																	
																
                                                                    $result_rec=Select($query_rec,$conn);	
																	foreach($result_rec['rows'] as $recs)
																	{
																	$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and isu.courier_record_id=".$recs['id']."   and  isu.student_id = ".$student['id'];
																	$result_select_issue = Select($select_issue,$conn);
																		
																	
																	?>
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $student['record_id'];?>"  >
                                                                        
																		
																	
																		<thead>
																		<tr>
																				<th >#</th>
																				<th>SKU Name</th>
																				<th>Status</th>
																		</tr>
																		</thead>
																		<tbody>
																	
																	
																		 
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td ><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																						    <?php
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$each_issue['skuid']."|";
																							?>
																							
																							<input type="checkbox" id="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" name="cb_<?php echo $student['id']."_".$each_issue['skuid']; ?>" value="1">
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																				
																				
																				
																			?>
																			<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																		</tbody>
																		
																		
																	</table>
																	<hr></hr>
																	<?php
																	}
																	
																	}
																	?>
															
																<input hidden id="hiddencs_<?php echo $student['id']."_".$student['record_id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Raisereq(<?php echo $student['id']; ?>,<?php echo $student['org_loc_id']; ?>,<?php echo $student['record_id'];?>);" type="button" class="btn btn-primary">Submit</button>
															</div>
														</div>
													</div>
												</div>
												
												
												<div class="modal fade" id="re_raise_<?php echo $student['record_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content" style="width:600px;">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo " Create Notification For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="200">
																			
																<?php
																	
																	$query_req="select * from student_couriers_request where student_id=".$student['id']." and id=".$student['courierid'] ;
                                                                    $result_req=Select($query_req,$conn);	
																	foreach($result_req['rows'] as $reqs)
																	{
																	$query_rec="select *,DATE_FORMAT(created_at, '%d/%m/%Y') as sent from student_couriers_records where courier_req_id=".$reqs['id']." and id=".$student['record_id']."";
																	
																
                                                                    $result_rec=Select($query_rec,$conn);	
																	foreach($result_rec['rows'] as $recs)
																	{
																	$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and isu.courier_record_id=".$recs['id']."   and  isu.student_id = ".$student['id'];
																	$result_select_issue = Select($select_issue,$conn);
																		
																	
																	?>
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $student['record_id'];?>"  >
                                                                        
																		
																	
																		<thead>
																		<tr>
																				<th >#</th>
																				<th>SKU Name</th>
																				<th>Status</th>
																		</tr>
																		</thead>
																		<tbody>
																	
																	
																		 
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td ><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																						    <?php
																							$cbconcatc = $cbconcat."cbc_".$student['id']."_".$each_issue['skuid']."|";
																							?>
																							
																							<input type="checkbox" id="cbc_<?php echo $student['id']."_".$each_issue['skuid']; ?>" name="cbc_<?php echo $student['id']."_".$each_issue['skuid']; ?>" value="1" checked>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																				
																				
																				
																			?>
																			<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																		</tbody>
																		
																		
																	</table>
																	<hr></hr>
																	<?php
																	}
																	
																	}
																	?>
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['record_id'];?>">

																		<thead>
																		<tr>
																		<td>Provider :
																		</td>
																		<td>
																		<?php echo $student['courier_provider']; ?>
																		</td>
																		<td >
																		Track No. :
																		</td>
																		<td>
																		<?php echo $student['courier_track_no']; ?>
																		</td>
																		</tr>
																	
																		<tr>
																		<td >
																		Sent On. :
																		</td>
																		<td>
																		<?php echo $student['sent']; ?>
																		</td>
																		
																		<td >
																		Notify On:
																		</td>
																		<td>
																		<input data-toggle="tooltip" id="recourierattempt_<?php echo $student['record_id'];?>" name="recourierattempt_<?php echo $student['record_id'];?>"   class="form-control" type="date" value = ""  />
																		</td>
																		</tr>
																		<tr>
																		<td >
																		Comments :
																		</td>
																		<td colspan=3>
																		<textarea type="text" id="returncomment_<?php echo $student['record_id'];?>" name="returncomment_<?php echo $student['record_id'];?>" rows=3 cols=60 ></textarea>
																		</td>
																		</tr>
																		<tr>
																		<td>
																		
																		</td>
																		<td>
																		<input type="hidden" id="courier_charges_<?php echo $student['record_id'];?>" name="courier_charges_<?php echo $student['record_id'];?>" value="<?php echo $student['courier_charges']; ?>" />
																		</td>
																		</tr>
																		</thead>
																
																	</table>
																<input hidden id="hiddencs2_<?php echo $student['id']."_".$student['record_id']; ?>" value="<?php echo $cbconcatc; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																<button onClick="Raise_notification(<?php echo $student['id']; ?>,<?php echo $student['org_loc_id']; ?>,<?php echo $student['record_id'];?>);" type="button" class="btn btn-primary">Create</button>
															</div>
														</div>
													</div>
												</div>
												
												
												
												<?php	
											
											}
										?>
										
										
										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courier_int = "SELECT user_name from users where id = ".$student['courier_intiated_by'];
												$result_courier_int = Select($courier_int,$conn);
												
																	
											?>
											<div class="modal fade" id="courierview_<?php echo $student['record_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-modal="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Sent Courier Summary For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll " data-scroll="true" data-height="400"  style="height: 400px; overflow: hidden;">
																
																
																<?php
																	 echo "<strong>Courier Address</strong>:-".$student['student_address']."<br><hr>"; 
																	$query_req="select * from student_couriers_request where student_id=".$student['id']." and id=".$student['courierid'] ;
                                                                    $result_req=Select($query_req,$conn);	
																	foreach($result_req['rows'] as $reqs)
																	{
																	$query_rec="select *,DATE_FORMAT(created_at, '%d/%m/%Y') as sent from student_couriers_records where courier_req_id=".$reqs['id']." and id=".$student['record_id']."";
																	
																
                                                                    $result_rec=Select($query_rec,$conn);	
																	foreach($result_rec['rows'] as $recs)
																	{
																	$select_issue = "SELECT s.sku_name,s.id as skuid,isu.issued_status from skus s,issuance isu where s.id=isu.sku_id and isu.courier_record_id=".$recs['id']."   and  isu.student_id = ".$student['id'];
																	$result_select_issue = Select($select_issue,$conn);
																		
																		echo "<strong>Courier Provider:-".$recs['courier_provider']."<br>";
																		echo "Tracking No:-".$recs['courier_track_no']."<br>";
																		echo "Sent On:-".$recs['sent']."<br></strong>";
																	?>
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable "   id= "<?php echo $student['record_id'];?>"  >
                                                                        
																		
																	
																		<thead>
																		<tr>
																				<th >#</th>
																				<th>SKU Name</th>
																				<th>Status</th>
																		</tr>
																		</thead>
																		<tbody>
																	
																	
																		 
																			<?php
																				$i = 1;
																				$cbconcat = "";
																				
																				
																				foreach($result_select_issue['rows'] as $each_issue)
																				{	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td ><?php echo $i; ?></td>
																						<td><?php echo $each_issue['sku_name']; ?></td>
																						<td>
																							<?php if($each_issue['issued_status']) echo "Issued"; 
																							else
																							{
																							echo "Not Issued";
																							}
																							?>
																						</td>
																					</tr>
																				<?php
																				$i++;
																				}
																				
																				
																				
																				
																			?>
																			<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div>
																		</tbody>
																		
																		
																	</table>
																	<hr></hr>
																	<?php
																	}
																	
																	}
																	?>
																	
																	
																	<input hidden id="hidden_<?php echo $student['record_id']; ?>" value="<?php echo $cbconcat; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
															
															</div>
														</div>
													</div>
												</div>
												
												<?php	
											
											}
										?>
										
									<!--end: Datatable -->
									
									

								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}


function Returncourier(student_id,recordid)
{
	if (confirm('Are you sure you want to return this courier?')) {
		if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Courier Returned Successfully.");
				submit_button_clicked = '';
				$('#student_'+student_id).modal('toggle');
				window.location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert("Courier Return Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Courier Return Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	
	xmlhttp.open('POST', 'ajax/return_courier.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('student_id='+fixEscape(student_id)+'&recordid='+fixEscape(recordid));	
	return true;
	} else {
	submit_button_clicked = '';
	return false;
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}

}


function Updatecourierdetails(record_id,courier_id)
{
	
	var courier_provider = document.getElementById('courier_provider_'+record_id).value;
	var courier_track_no = document.getElementById('courier_track_no_'+record_id).value;
	var courier_charges = document.getElementById('courier_charges_'+record_id).value;


	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert("Courier Details Updated Successfully.");
				submit_button_clicked = '';
				$('#courdet_'+record_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Courier Details Update Failed. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Courier Details Update Failed. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_courier_details.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('record_id='+fixEscape(record_id)+'&courier_track_no='+fixEscape(courier_track_no)+'&courier_provider='+fixEscape(courier_provider)+'&courier_charges='+fixEscape(courier_charges));
	
}

function Raisereq(student_id,loc_id,record_id)
{
   

	var raise_loc = 1;
	var tb = "hiddencs_"+student_id+"_"+record_id;
	var cbs = document.getElementById(tb).value;

	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);

	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Books Requirement placed Successfully.");
				submit_button_clicked = '';
				$('#return_my_courier_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Raise requirement FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Raise requirement FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/reraise_requirement.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id)+'&raise_loc='+fixEscape(raise_loc)+'&prev_courier_req_id='+record_id);
}



function Raise_notification(student_id,loc_id,record_id)
{
   

	var raise_loc = 1;
	var tb = "hiddencs2_"+student_id+"_"+record_id;
	var cbs = document.getElementById(tb).value;
	var reattempt_date = document.getElementById('recourierattempt_'+record_id).value;
	var reattempt_comment = document.getElementById('returncomment_'+record_id).value;
	
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	var i;
	var atleastone = false;
	final_cbs = [];
	
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
			var final_cb = {"cb_name": cbs_array[i]};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
	}
	if(!atleastone)
	{
		alert("Atleast one SKU needs to be selected to proceed.");
		return;
	}
	final_cbs_json = JSON.stringify(final_cbs);

	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Notification created for raise requirement sucessfully.");
				submit_button_clicked = '';
				$('#re_raise_'+record_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Notification Createion FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Notification Creation FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/create_notification.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&student_id='+fixEscape(student_id)+'&student_loc='+fixEscape(loc_id)+'&raise_loc='+fixEscape(raise_loc)+'&prev_courier_req_id='+record_id+'&reattempt_date='+reattempt_date+'&recomment='+fixEscape(reattempt_comment));
}



</script>
<script src="/assets/js/demo1/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>