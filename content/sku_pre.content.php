
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
if(isset($_GET['dberror']))
	{
		$msg = "Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Sku's Previous. Qty. Updated Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>
<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Update SKU Previous Quantity  
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='POST' action='form_handlers/sku_pre.php'>
<div class="kt-portlet__body">
<div class="form-group">



	<!--begin: Datatable -->
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:10%;">#</th>
				<th style="width:35%;">SKU Name</th>
				<th style="width:15%;">Location</th>
				<th style="width:15%;">SKU Pre Qty.</th>
			</tr>
		</thead>
		
		
		
		<tbody id="rows_div">
		<?php 
			$query_skus = "SELECT sl.id,s.sku_name,l.loc_name,sl.sku_pre_qty,sl.sku_id,sl.loc_id FROM `skus_locations` sl,`skus` s, `locations` l WHERE sl.sku_id=s.id and loc_id in (1)
and sl.loc_id=l.id  order by l.id,s.sku_name";
			$result_skus = Select($query_skus,$conn);
			$i=0;
			foreach($result_skus['rows'] as $sku)
			{
				$i++;?>
			<tr id="row_id_<?php echo $i; ?>">
				<td class="text-left"><input class="form-control" name="skl_<?php echo $sku['id']; ?>" id="skl_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['id']; ?>"/><?php echo $i; ?></td>
				<td class="text-left"><input class="form-control" name="sku_<?php echo $sku['id']; ?>" id="sku_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['sku_id']; ?>"/><?php echo $sku['sku_name']; ?></td>
				<td class="text-left"><input class="form-control" name="loc_<?php echo $sku['id']; ?>" id="loc_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['loc_id']; ?>"/><?php echo $sku['loc_name']; ?></td>
				<td class="text-center"><input class="form-control" name="qty_pre_<?php echo $sku['id']; ?>" id="qty_pre_<?php echo $sku['id']; ?>"  type="text" value="<?php echo $sku['sku_pre_qty']; ?>"/></td>
				
			</tr>
			<?php } ?>
		</tbody>
	</table>

	<!--end: Datatable -->
</div>	
</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			
			<button id="submit" name="submit" type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
			
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>