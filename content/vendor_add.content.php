<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
	if(isset($_GET['vendor']))
	{
		$msg = "Vendor Creation Failed. Vendor Name already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Vendor Creation Failed. Vendor Name is Mandatory Fields.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Vendor Creation Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Vendor  Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	Add Vendors
</h3>
</div>
</div>

<form class="kt-form kt-form--label-right" method='post' action='form_handlers/vendor_add.php'>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-6">
	<label>Vendor Name</label>
	<input type="text" class="form-control" id="vendor_name" name="vendor_name">
	
</div>
<div class="col-lg-6">
	<label class="">Vendor Email:</label>
	<input type="text" class="form-control" id="vendor_email"  name="vendor_email">
	
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
	<label>Vendor Address:</label>
	<input type="text" class="form-control" id="vendor_address" name="vendor_address">
	
</div>
<div class="col-lg-6">
	<label class="">Vendor Contact:</label>
	<input type="text" class="form-control" id="vendor_contact"  name="vendor_contact">
	
</div>

</div>
<div class="form-group row">
<div class="col-lg-6">
	<label>Vendor NPP:</label>
	 <input type="text" class="form-control" id="vendor_npp"  name="vendor_npp">
	
</div>
</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-2"></div>
		<div class="col-lg-10">
			<button type="submit" class="btn btn-success">Submit</button>
			<button type="reset" class="btn btn-secondary">Cancel</button>
		</div>
	</div>
</div>
</div>
</form>
</div>
</div>
</div>

</div>

<?php
?>