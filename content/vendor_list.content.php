<?php
	$query_allvendors = "SELECT * FROM `vendors` ORDER BY vendor_name";
	$result_allvendors = Select($query_allvendors,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Vendors List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="vendor_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Vendor
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Vendor Name</th>
												<th>Vendor Email</th>
												<th>Vendor Address</th>
												<th>Vendor Contact</th>
												<th>Vendor NPP</th>
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allvendors['rows'] as $vendor) 
											{	?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $vendor['vendor_name']; ?></td>
													<td><?php echo $vendor['vendor_email']; ?></td>
													<td><?php echo $vendor['vendor_address']; ?></td>
													<td><?php echo $vendor['vendor_contact']; ?></td>
													<td><?php echo $vendor['vendor_npp']; ?></td>
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="vendor_edit?id=<?php echo $vendor['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
															<!--<button type="button" class="btn btn-brand btn-elevate btn-pill"><i class="la la-bank"></i> Solid</button>
															<a type="button" class="btn btn-success"><i class="la la-paperclip"></i></a>-->
														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>