<script>
	function filter_students()
	{
		var filter_param = document.getElementById('search_param').value;
		window.location = 'selected_batchcourses.php?filter_param='+filter_param;
	}
</script>
<?php
	$user_id = get_get_value('uid');
	$location_id = get_get_value('lid');
	$course_id = get_get_value('cid');
	
	if($user_id == "")
	{
		$user_id = $_SESSION['USER_ID'];
	}
	if($course_id == "")
	{
		$course_id = "1";
	}
	if($location_id == "")
	{
		$location_id = "3";
	}
	
	if( $_SESSION['U_LOCATION_TYPE'] == "CENTER")
	{
		$location_id = "3";
	}
	
	$qrys="";
	if($course_id!='')
	{
		$qrys="and s.course_id = ".$course_id."";
	}
	elseif($course_id=='')
	{
		$qrys="and s.course_id in (SELECT course_id from sub_courses where rrf = 0)  or s.course_id not in (0)";
	}
	
	$filter_param = get_get_value('filter_param');
	if($filter_param == "")
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and (sb.rrf=0 or s.course_id not in (8,14,15,16,17)) and  sl.id = s.loc_id and s.loc_id = '".$location_id."' ".$qrys." ORDER BY s.student_name";
	
	}
	else
	{
		$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id,s.student_address,sl.loc_name FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and (sb.rrf=0 or s.course_id not in (8,14,15,16,17) ) and  sl.id = s.loc_id and (student_name = '".$filter_param."' OR crm_id = '".$filter_param."' OR student_email = '".$filter_param."' OR student_contact = '".$filter_param."' OR subcourse_name = '".$filter_param."') ORDER BY s.student_name";
	}
	
	
	//	$query_allstudents = "SELECT s.id,s.student_name,s.student_contact,s.student_email,s.course_id,s.subcourse_id,s.loc_id FROM `students` s,locations sl, sub_courses sb WHERE sb.id=s.subcourse_id and sb.rrf=0 and  sl.id = s.loc_id and s.loc_id = ".$location_id." ORDER BY s.student_name";
	$result_allstudents = Select($query_allstudents,$conn);	
	
	$location_type = "SELECT loc_type,loc_name from locations where id = ".$location_id ."";
	$result_loc_type = Select($location_type,$conn);
	$location_type_report = $result_loc_type['rows'][0]['loc_type'];
	$location_name_report = $result_loc_type['rows'][0]['loc_name'];
	
	$course_name = "SELECT id,course_name from courses where id = ".$course_id ."";
	$result_course_name = Select($course_name,$conn);
	$course_name_report = $result_course_name['rows'][0]['course_name'];
	
	$center_locations = "SELECT id,loc_name from locations where loc_type = 'CENTER'";
	$result_loc_centers = Select($center_locations,$conn);
	
	$course = "SELECT id,course_name from courses where id in (select course_id from sub_courses where rrf = 0) or id not in (8,14,15,16,17) order by course_name ";
	$result_course = Select($course,$conn);
	
	
	
	/*if($_SESSION['U_LOCATION_TYPE'] != "CENTER" && $location_type_report == "CENTER")
	{
		$newrecord = array('id'=>$_SESSION['U_LOCATION_ID'],'loc_name'=>$_SESSION['U_LOCATION_NAME']);
		array_push($result_loc_centers['rows'],$newrecord);
	}*/
	$finallocation="";
	if($location_name_report=='Mumbai')
	{
	$finallocation="AND city in ('Mumbai','Andheri')";	
	}
	else
	{
	$finallocation="AND city like '%".$location_name_report."%'";		
	}
	

																						
?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

<div class="kt-portlet">
<div class="form-group row"style="padding-top:2%;padding-left:2%;">
<div class="col-lg-5">
	<label>Search</label>
	<input type="text" class="form-control" id="search_param" name="search_param" value = "<?php echo $filter_param; ?>">
	
</div>
<div class="col-lg-1">
	<label class="">&nbsp;</label>
	<button onClick="filter_students();" type="button" class="btn btn-primary form-control">Filter</button>
	
</div>
</div>
</div>
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Selected Batchcourses <?php 
											
											echo "(".$location_name_report." - ".$course_name_report.")"; 
	                                       
											?>
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												<?php 
																if($_SESSION['U_LOCATION_TYPE'] != "CENTER")
																{
																?>
											
														<div class="col">
														<table><tr>
														<td>
														
														
														<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Courses
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_course['rows'] as $course)
																			{	
																				
																				?>
																				<a class="dropdown-item" href="selected_batchcourses.php?uid=<?php echo $user_id;?>&cid=<?php echo $course['id'];?>&lid=<?php echo $location_id;?>"><?php echo $course['course_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td><td>
															<div class="dropdown">
																<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	Centers
																</button>
																
																	<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																		<?php 
																			foreach($result_loc_centers['rows'] as $loc_center)
																			{	
																		?>
																				<a class="dropdown-item" href="selected_batchcourses.php?uid=<?php echo $user_id;?>&cid=<?php echo $course_id;?>&lid=<?php echo $loc_center['id'];?>"><?php echo $loc_center['loc_name']; ?></a>
																		<?php	
																				}
																		?>
																		
																	</div>
																
															</div>
															</td></tr></table>
														</div>
													<?php
																}
																?>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>Student Name</th>
												<th >Student Contact</th>
												<!--<th>Student Email</th>-->
												<th>Student Locations</th>
												<th>Student Course</th>
												<th>Student Subcourse</th>
												
												<th colspan=3><center>Action</center></th>
												    
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allstudents['rows'] as $student) 
											{	
												
												$courses = "SELECT * from courses where id = ".$student['course_id'];
												$result_courses = Select($courses,$conn);
											
												
												$subcourses = "SELECT * from sub_courses where id = ".$student['subcourse_id'];
												$result_subcourses = Select($subcourses,$conn);
												
										        $locs = "SELECT * from locations where id = ".$student['loc_id'];
												$result_locs = Select($locs,$conn);
												
												
												
												?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $student['student_name']; ?></td>
													<td ><?php echo $student['student_contact']; ?></td>
													<!--<td><?php// echo $student['student_email']; ?></td>-->
													<td><?php echo $result_locs['rows'][0]['loc_name']; ?></td>
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<td><?php echo $result_subcourses['rows'][0]['subcourse_name']; ?></td>
													
													<td><button type="button" class="btn btn-bold btn-label-brand btn-sm" data-toggle="modal" data-target="#studentcourses_<?php echo $student['id']; ?>">Selected Courses</button></td>
													
											
													
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>
									

										<?php 
											
											foreach($result_allstudents['rows'] as $student) 
											{	
												$select_course = "SELECT id,course,course_module from course_modules where course_id = '".$student['course_id']."' and is_active=1 order by course_module";
										
												$result_select_course = Select($select_course,$conn);
											?>
											<div class="modal fade" id="studentcourses_<?php echo $student['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
													<div class="modal-dialog" role="">
														<div class="modal-content">
															<div class="modal-header">
																<h5 class="modal-title" id="exampleModalLabel"><?php echo "Batch Courses  Selected List For: ".$student['student_name']; ?></h5>
																
																<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																</button>
															</div>
															<div class="modal-body">
																<div class="kt-scroll" data-scroll="true" data-height="400">
																	
																	<table  class="table table-striped- table-bordered table-hover table-checkable" id= "<?php echo $student['id'];?>">

																		<thead>
																																				
																			<tr>
																				<td>#</td>
																				<td>Batch Course Name</td>
																				<td>Select</td>
																				<td>Attended/Attending Batch</td>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																			
																				$i = 1;
																				$cbconcat = "";
																				$cbconcatsc = "";
																				
																				foreach($result_select_course['rows'] as $course)
																				{
																				$select_courseval = "SELECT course_name from courses where id = '".$student['course_id']."'";

																				$result_select_courseval = Select($select_courseval,$conn);
																				$cname= $result_select_courseval['rows'][0]['course_name']; 	
																				$query_harry_codes = "SELECT id,code FROM workshops WHERE course like '%".$cname."%'  AND code not like '%SEM%' AND deleteflag=0 ORDER BY id desc";
																				$result_harry_code = Select($query_harry_codes,$harry);	
																					?>
																					<tr <?php echo "row_".$student['id']."_".$i; ?>>
																						<td><?php echo $i; ?></td>
																						<td><?php echo $course['course_module']; ?></td>
																						<td>
																							<?php 
																							$select_sel="SELECT count(id) as counts FROM students_course_modules  WHERE `course_module_id`='".$course['id']."' AND  `student_id`='".$student['id']."'";
																							$result_sel = Select($select_sel,$conn,"students_course_modules");
																							
																							if($result_sel['rows'][0]['counts']!=0){
																								echo "Selected";
																								$cbconcatsc = $cbconcatsc."cb_".$student['id']."_".$course['id']."|";
																								?>
																								<input  type="checkbox" id="cb_<?php echo $student['id']."_".$course['id']; ?>" name="cb_<?php echo $student['id']."_".$course['id']; ?>" checked hidden>
																							
																							<?php
																							}
										                                                     if($result_sel['rows'][0]['counts']==0){
																							$cbconcat = $cbconcat."cb_".$student['id']."_".$course['id']."|";
																							?>
																								<input type="checkbox" id="cb_<?php echo $student['id']."_".$course['id']; ?>" name="cb_<?php echo $student['id']."_".$course['id']; ?>" value="1" >
																							<?php
																							 }
																							?>
																						</td>
																						<td>
																					    <select class="form-control bootstrap-select" id="batch_code_<?php echo $student['id']."_".$course['id']; ?>"  name="batch_code_<?php echo $student['id']."_".$course['id']; ?>">
																						<option value="0">Select Batch</option>
																						<?php
																						$select_code="SELECT count(batch_attended) as count FROM students_course_modules  WHERE `course_module_id`='".$course['id']."' AND  `student_id`='".$student['id']."' and batch_attended!=0";
																						$result_code = Select($select_code,$conn,"students_course_modules");
																						$countcode= $result_code['rows'][0]['count'];	
																						foreach($result_harry_code['rows'] as $codes){
																						if($countcode!=0)
																						{	
                                                                                        $select_code2="SELECT batch_attended FROM students_course_modules  WHERE `course_module_id`='".$course['id']."' AND  `student_id`='".$student['id']."' and batch_attended!=0";
																						$result_code2 = Select($select_code2,$conn,"students_course_modules");
																						$batch_attended= $result_code2['rows'][0]['batch_attended'];
                                                                                        if($codes['code']==$batch_attended)
																						{																							
																						?>
																						<option value="<?php echo $codes['code']; ?>" selected><?php echo $codes['code']; ?></option>
																						<?php
																						}
																						else
																						{
																						?>
																						<option value="<?php echo $codes['code']; ?>"><?php echo $codes['code']; ?></option>
                                                                                        <?php
																						}
																						}
																						else
																						{
																						?>
																						<option value="<?php echo $codes['code']; ?>"><?php echo $codes['code']; ?></option>
																						<?php
																						}
																						}
																						?>
																						</select>
																						
																						</td>
																						
																					</tr>
																				<?php
																				$i++;
																				}
																				
																			?>
																		</tbody>
																	</table>
																	<input hidden id="hiddens_<?php echo $student['id']; ?>" value="<?php echo $cbconcat; ?>">
																	<input hidden id="hiddensc_<?php echo $student['id']; ?>" value="<?php echo $cbconcatsc; ?>">
																</div>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
																
																<button onClick="Selectcourse(<?php echo $student['id']; ?>);" type="button" class="btn btn-primary">Submit</button>
																
															</div>
														</div>
													</div>
												</div>
												<?php	
											
											}
										?>
										
									<!--end: Datatable -->
								</div>
							</div>
</div>
<script>
submit_button_clicked = '';
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}



function Selectcourse(student_id)
{
  
	var tb = "hiddens_"+student_id;
	var cbs = document.getElementById(tb).value;
	cbs = cbs.substring(0, cbs.length - 1);
	var cbs_array = cbs.split("|");
	
	var tbsc = "hiddensc_"+student_id;
	var cbsc = document.getElementById(tbsc).value;
    cbsc = cbsc.substring(0, cbsc.length - 1);
	var cbsc_array = cbsc.split("|");
	
	
	
	var i;
	var j;
	var atleastone = false;
	if(cbs!='')
	{
	final_cbs = [];
	}
	if(cbsc!='')
	{
	final_codes = [];
    }
	if(cbs!='')
	{
	for (i = 0; i < cbs_array.length; i++) { 
		if (document.getElementById(cbs_array[i]).checked == true)
		{
		    var sel =cbs_array[i].substring(2);
			var finalcode = document.getElementById('batch_code'+sel).value;
			var final_cb = {"cb_name": cbs_array[i]+'_'+finalcode};
			final_cbs.push(final_cb);
			atleastone = true;
		}
		
		
	}
	}
	
	if(cbsc!='')
	{
	for (j = 0; j < cbsc_array.length; j++) { 
	if (document.getElementById(cbsc_array[j]).defaultChecked == true) 
		{
		   
			var sel =cbsc_array[j].substring(2);
			var finalcode = document.getElementById('batch_code'+sel).value;
			var final_cbsc = {"cbsc_name": cbsc_array[j]+'_'+finalcode};
			final_codes.push(final_cbsc);
			atleastone = true;
			
		}
	}
	}
	
	
	if(!atleastone)
	{
		alert("Atleast one Batch Course needs to be selected to proceed.");
		return;
	}
	
	
	if(cbs!='')
	{
	final_cbs_json = JSON.stringify(final_cbs);
	}
	else
	{
		final_cbs_json ="";
	}
	if(cbsc!='')
	{
	final_upcodes_json = JSON.stringify(final_codes);
	}
	else
	{
	final_upcodes_json="";	
	}
	if(submit_button_clicked=='1')
	{

		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
	        
			if(output.status == 'success')
			{
				alert("Selected Courses List Updated Successfully.");
				submit_button_clicked = '';
				$('#studentcourses_'+student_id).modal('toggle');
				window.location.reload();
			}
		
			else if(output.status == 'db_error')
			{
				alert("Batch Course List Update FAILED. Contact Administrator1");
				submit_button_clicked = '';
			}
			
			else
			{
				alert("Batch Course List Update FAILED. Contact Administrator2");
				submit_button_clicked = '';
			}
		}
	}
    xmlhttp.open('POST', 'ajax/selected_batchcourses.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	//alert('final_cbs_json='+fixEscape(final_cbs_json)+'&final_upcodes_json='+fixEscape(final_upcodes_json)+'&student_id='+fixEscape(student_id));
	xmlhttp.send('final_cbs_json='+fixEscape(final_cbs_json)+'&final_upcodes_json='+fixEscape(final_upcodes_json)+'&student_id='+fixEscape(student_id));
}

</script>