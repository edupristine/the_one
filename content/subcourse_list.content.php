<?php
	$query_allsubcourses = "SELECT * from sub_courses  ORDER BY subcourse_name";
	$result_allsubcourses = Select($query_allsubcourses,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											Sub Course List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="subcourse_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New Sub Course
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>SubCourse Name</th>
												<th>Course Name</th>
												<!--<th>Course Owner</th>
												<th>Course Manager</th>
												<th>Start Date</th>
												<th>End Date</th>
												<th>Duration in Hours</th>-->
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allsubcourses['rows'] as $subcourses) 
											{	
											$query_courses = "SELECT course_name from courses  where id ='".$subcourses['course_id']."'";
											$result_courses = Select($query_courses,$conn);	
											
											
											$query_coursesown = "SELECT user_name from users  where id ='".$subcourses['subcourse_owner']."'";
											$result_coursesown = Select($query_coursesown,$conn);	
											
											
											$query_coursesman = "SELECT user_name from users  where id ='".$subcourses['subcourse_manager']."'";
											$result_coursesman = Select($query_coursesman,$conn);	
											
											?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $subcourses['subcourse_name']; ?></td>
													<td><?php echo $result_courses['rows'][0]['course_name']; ?></td>
													<!--<td><?php echo $result_coursesown['rows'][0]['user_name']; ?></td>
													<td><?php echo $result_coursesman['rows'][0]['user_name']; ?></td>
													<td><?php echo $subcourses['start_date']; ?></td>
													<td><?php echo $subcourses['end_date']; ?></td>
													<td><?php echo $subcourses['duration_in_hours']; ?></td>-->
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="subcourse_edit?id=<?php echo$subcourses['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
															<!--<button type="button" class="btn btn-brand btn-elevate btn-pill"><i class="la la-bank"></i> Solid</button>
															<a type="button" class="btn btn-success"><i class="la la-paperclip"></i></a>-->
														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>