<!-- end:: Subheader -->

<!-- begin:: Content -->
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
<?php
$id = get_get_value('id');	
$edu=mysqli_connect('13.126.164.124','liveuser','p3t3r5bu4g','wp_tetris');
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$con_one=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');


$course="SELECT distinct(course_name) as `course` FROM courses where course_name in ('Corporate','ACCA','CPA','CMA','USMLE','FRM','FM','DM','CFA','BAT','USMLE CMTC','Analytics','Ecom-CFA')ORDER BY course_name";
$resultc = mysqli_query($con_one,$course);
while($rc = mysqli_fetch_assoc($resultc)) {
$rowsc[] = $rc;
}

$qry1 = "SELECT * FROM job_locations order by location_name";
$res = mysqli_query($edu,$qry1);
while($r = mysqli_fetch_assoc($res)) {
    $rows[] = $r;
}



$qry1l = "SELECT *,DATE_FORMAT(end_date,'%Y-%m-%d') as enddt,DATE_FORMAT(start_date,'%Y-%m-%d') as sdt FROM job_postings where id=".$id;
$resls = mysqli_query($edu,$qry1l);
$rjp = mysqli_fetch_assoc($resls);

	
	if(isset($_GET['dupname1']))
	{
		$msg = "Job Posting Update Failed. Similiar Posting  already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dupname2']))
	{
		$msg = "Job Posting Update Failed. Similiar Posting already Exists.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	
	elseif(isset($_GET['paramsmissing']))
	{
		$msg = "Job Posting Update Failed.Mandatory Fields missing.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['dberror']))
	{
		$msg = "Job Posting Update Failed. Unknown Error Contact Administrator.";
		$altype = "danger";
		$icontype = "flaticon2-cross";
	}
	elseif(isset($_GET['success']))
	{
		$msg = "Job Posting Update Created Successfully.";
		$altype = "success";
		$icontype = "flaticon2-check-mark";
	}
	else
	{
		$msg = "";
	}
	if($msg != '')
	{
?>
	<div class="alert alert-<?php echo $altype; ?> fade show" role="alert">
		<div class="alert-icon"><i class="<?php echo $icontype; ?>"></i></div>
		<div class="alert-text"><?php echo $msg; ?></div>
		<div class="alert-close">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true"><i class="la la-close"></i></span>
			</button>
		</div>
	</div>
	<?php } ?>

<div class="row">
<div class="col-lg-12">

<!--begin::Portlet-->
<div class="kt-portlet">
<div class="kt-portlet__head">
<div class="kt-portlet__head-label">
<h3 class="kt-portlet__head-title">
	View Job Posting | <a href="job_posting_list.php">Back to Job Posting List</a> | <a href="add_jobs.php">Add New Job Posting</a>
</h3>
</div>
</div>
<form enctype="multipart/form-data" class="kt-form kt-form--label-right" method='post' action=''>
<div class="kt-portlet__body">
<div class="form-group row">
<div class="col-lg-4">
<label>Selected Course</label>
<br>

<?php
foreach($rowsc as $courses)
{
if($rjp['course']==$courses['course'])	
{
?>
<?php echo $courses['course'];?>
<?php
}
}
?>

</div>

<div class="col-lg-4">
<label>Selected Vertical</label><br>
<?php

if($rjp['vertical']==1)
{
echo "Accounts";
}else if($rjp['vertical']==2)
{
echo "Finance";
}else if($rjp['vertical']==3)
{
echo "Both Accounts & Finance";
}


?>
</select >
</div>
<div class="col-lg-4">
<label>Selected Job Type</label><br>

<?php

if($rjp['job_type']==1)
{
echo "Internal";
}else if($rjp['job_type']==2)
{
echo "External";
}
?>
</select >

</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
	<label>Job Title 1</label><br>
	<?php echo $rjp['job_title_1']?>
	<input type="hidden" class="form-control" id="jpid" name="jpid" value="<?php echo $rjp['id']?>" >
</div>
<div class="col-lg-6">
	<label>Job Title 2</label><br>
	<?php echo $rjp['job_title_2']?>
	
</div>
</div>

<div class="form-group row">
<div class="col-lg-6">
<label>Company Logo:</label><br>
<?php 
if($rjp['company_logo']!="")
{
?>
<img src="https://s3-us-west-2.amazonaws.com/content.edupristine.com/edu_job_posting/<?php echo $rjp['company_logo']?>" width="120px;"></img>
<?php
}else
{
?>
<img src="https://s3-us-west-2.amazonaws.com/content.edupristine.com/edu_job_posting/default_company_logo.jpg" width="120px;"></img>
<?php
}?>
</div>
<div class="col-lg-6">
	<label>Company Name</label><br>
	<?php echo $rjp['company_name']?>
</div>
</div>

<div class="form-group row">
<div class="col-lg-4">
<label>Qualification:</label><br>
<?php echo $rjp['qualification']?>
	
</div>
<div class="col-lg-4">
<label>Salary:</label><br>
<?php echo $rjp['salary']?>
	
</div>
<div class="col-lg-4"><br>
<label>Job Location</label>

<?php
foreach($rows as $location)
{
$final_locations = array();	
$qry_loc = "SELECT id FROM job_locations where id in (".$rjp['job_locations'].") order by id desc";
$res = mysqli_query($edu,$qry_loc);
while($rl = mysqli_fetch_assoc($res)) {
    $final_locations[] = $rl['id'];
}
//print_r($final_locations);

if (in_array($location['id'], $final_locations))
{
?>
<?php echo $location['location_name']."<br>";?>
<?php
}

}
?>
</select >

</div>
</div>


<div class="form-group row">
<div class="col-lg-4">
<label>Experience:</label><br>
<?php echo $rjp['experience']?>
	
</div>
<div class="col-lg-4">
<label>Date Posted:</label><br>
<?php echo $rjp['sdt']; ?>
	
</div>
<div class="col-lg-4">
<label>Job Expires On</label><br>
<?php echo $rjp['enddt']; ?>

</div>
</div>


<div class="form-group row">
<div class="col-lg-12">
<label>About Company:</label><br>
<?php echo $rjp['about_company']?>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label>Job Description:</label><br>
<?php echo $rjp['job_description']?></div>
</div>


<div class="form-group row">
<div class="col-lg-12">
<label>Skills and Requirement:</label><br>
<?php echo $rjp['skillset']?>
</div>
</div>

<div class="form-group row">
<div class="col-lg-12">
<label>Education:</label><br>
<?php echo $rjp['education']?></div>
</div>



</div>
<div class="kt-portlet__foot kt-portlet__foot--fit-x">
<div class="kt-form__actions">
	<div class="row">
		<div class="col-lg-12">
			<!--<center><button type="submit" class="btn btn-success">Deactivate</button>
			<button type="reset" class="btn btn-secondary">Cancel</button></center>-->
		</div>
	</div>
</div>
</div>
</form>


<?php /*
<div class="kt-portlet__body">
<input type="hidden" class="form-control" id="selstatus" name="selstatus"  value="<?php echo $status;?>">
<!--begin: Datatable -->
<table class="table table-striped- table-bordered table-hover table-checkable" id="">
<thead>
<tr>
<th >#</th>
<th >Location Name</th>
<th ><center>Action</center></th>
</tr>
</thead>
<tbody>
<?php
$i=1;
foreach($rows as $locs){
	
?>
<tr>
<td><?php echo $i;?></td>
<td><?php echo $locs['location_name'];?></td>
<td><center><a href="#" class="kt-nav__link-icon flaticon2-writing" data-toggle="modal" data-target="#approverefund_<?php echo $locs['id']; ?>" ></a></center></td>
</tr>
<?php
$i++;
}
?>

</tbody>
</table>
</div>
<?php
*/
?>

</div>
</div>
</div>

</div>

<?php
?>

<script type="text/javascript">
$(document).ready(function () {
	$('#about_company').jqxEditor({
		height: "400px",
		width: '100%'
	});
	
	$('#job_description').jqxEditor({
	height: "400px",
	width: '100%'
	});
	
    $('#skillset').jqxEditor({
	height: "400px",
	width: '100%'
	});
	
	$('#education').jqxEditor({
	height: "400px",
	width: '100%'
	});


});
</script>