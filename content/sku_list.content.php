<?php
	$query_allskus = "SELECT s.id,sku_name,sku_code,sku_type FROM `skus` s ORDER BY sku_name";
	$result_allskus = Select($query_allskus,$conn);	

?>
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
	<div class="kt-portlet kt-portlet--mobile">
								<div class="kt-portlet__head kt-portlet__head--lg">
									<div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-line-chart"></i>
										</span>
										<h3 class="kt-portlet__head-title">
											SKU List
										</h3>
									</div>
									<div class="kt-portlet__head-toolbar">
										<div class="kt-portlet__head-wrapper">
											<div class="kt-portlet__head-actions">
												
												&nbsp;
												<a href="sku_add.php" class="btn btn-brand btn-elevate btn-icon-sm">
													<i class="la la-plus"></i>
													Add New SKU
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="kt-portlet__body">

									<!--begin: Datatable -->
									<table class="table table-striped- table-bordered table-hover table-checkable" id="">
										<thead>
											<tr>
												<th>#</th>
												<th>SKU Name</th>
												<th>SKU Code</th>
												<th>SKU Type</th>
												
												<th>Actions</th>
											</tr>
										</thead>
										<tbody>
										<?php
											$i = 1;
											foreach($result_allskus['rows'] as $sku) 
											{	?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $sku['sku_name']; ?></td>
													<td><?php echo $sku['sku_code']; ?></td>
													<td><?php echo $sku['sku_type']; ?></td>
													
													<td>
														<div class="btn-group" role="group" aria-label="First group">
															<a href="sku_edit.php?id=<?php echo$sku['id']; ?>" class="btn-sm btn-brand btn-elevate btn-pill"><i class="la la-edit"></i>Edit</a>
															<!--<button type="button" class="btn btn-brand btn-elevate btn-pill"><i class="la la-bank"></i> Solid</button>
															<a type="button" class="btn btn-success"><i class="la la-paperclip"></i></a>-->
														</div>
													</td>
												</tr>
										<?php	
											$i++;
											}
										?>
											
										</tbody>
									</table>

									<!--end: Datatable -->
								</div>
							</div>
</div>