<?php 
include 'inc/GenericFunctions.php';
include 'control/core.php'; 	
include 'control/checklogin.php';
include 'control/connection.php';

include("inc/head.php");
include("inc/sidebar.php");
include("inc/headermenu.php");

include("content/students_registered.content.php");
//include("js/sku.js.php");

include("inc/footer.php");
include("inc/foot.php");
?>
<script src="./assets/js/demo1/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<link href="./assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<!-- Datatable CSS -->
<link href='DataTables/datatables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<!--<script src="jquery-3.3.1.min.js"></script>-->

<!-- Datatable JS -->
<script src="DataTables/datatables.min.js"></script>

