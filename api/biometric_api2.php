<?php

//include_once("dbconfig.php");
//$conn=mysqli_connect($db_server,$db_username,$db_password,'edupristine_one');


function callAPI($method, $url, $data){
   $curl = curl_init();

   switch ($method){
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data)
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data)
            $url = sprintf("%s?%s", $url, http_build_query($data));
   }

   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'x-apiKey: vTaHJ9jJCVgskBfXPxW1C8hWk',
	  'x-secret: uJYoHJzhlgZwfT3mMyHK9eLXb',
      'Content-Type: application/json',
	  
   ));
   curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
   curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
}



//UPDATE USER DETAILS USING EMPID=CRM ID
function updateuser2($branchName,$dob,$doj,$dept,$econtact,$empid,$name,$gender,$marital,$mob,$designationname,$loe,$reason)
{
$url="https://api.securtime.in/api/employee/update";	

$data_array =  array(
  "affiliateName" => "EduPristine",
  "autoShiftEligibility" => false,
  "branchName" => $branchName,
  "categoryName" => "BA",
  "customMasterName" => "",
  "dateOfBirth" => $dob,
  "dateOfJoining" => $doj,
  "dateOfResignation" => "1980-12-01 00:00:00",
  "departmentName" => $dept,
  "designationName" => $designationname,
  "email" => "",
  "emergencyContactNumber" => $econtact,
  "employeeId" => $empid,
  "employeeName" => $name,
  "employmentStatus" => "TERMINATED",
  "enrollmentId" => substr($empid, 3),
  "gender" => $gender,
  "lastDateOfEmployment" => "2019-12-12",
  "managerEmployeeId" => "",
  "maritalStatus" => $marital,
  "mobile" => $mob,
  "nationalId" => "",
  "otEligibility" => false,
  "passportNumber" => "",
  "personalAddress" =>  "",
  "personalEmailId" => "",
  "pfNumber" => 0,
  "profilePic"         => array(
            "content"         => "",
            "mimeType"        => "",
            "name"         => ""
      ),
  "reasonForLeaving" => $reason,
  "roleName" => "Student",
  "userName" => substr($name,0,14),	  
);
print_r($data_array);

$call = callAPI('PUT',$url,json_encode($data_array));
$response = json_decode($call, true);
print_r($response);
}

//GET RAW PUNCHES
function getrawpunches($empid,$enddate,$startdate)
{
$url="https://api.securtime.in/api/raw-data/punches?empId=".$empid."&endDate=".$enddate."&startDate=".$startdate."";	

$call = callAPI('GET',$url,false);
print_r($call);
}


//For Testing Unit Below
// For creating user 
//Fields : createuser($branchName,$dob(YYYY-MM-DD),$doj(YYYY-MM-DD),$dept,$econtact(Emergency Contact),$crmid,$name,$gender,$marital,$mob)
//echo createuser('Andheri','1986-09-30','2019-09-30','ACCA Knowledge Level','9967286657','PQR200098','HiteshP','MALE','MARRIED','9967286657');

// For Updating user 
//$crm_id=$emp_id
//Fields : updateuser($branchName,$dob(YYYY-MM-DD),$doj(YYYY-MM-DD),$dept,$econtact(Emergency Contact),$crmid,$name,$gender,$marital,$mob)
//echo updateuser('Andheri','1980-12-01','2018-09-30','DM','9619487787','ACC56797','RoshniSrisanth','MALE','SINGLE','9619487787','Digital Marketing Master Program - Weekday','2019-09-16','New Offer');
//Update user will display Connection Failure error as the update doesnt give any json response

//For getting Punches Stamp
//echo getrawpunches('ACC56019','2019-10-17','2019-07-17');



?>
