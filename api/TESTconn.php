<?php
//The URL of the resource that is protected by Basic HTTP Authentication.
$url = 'https://api.securtime.in/api/raw-data/punches?empId=101879&endDate=2022-12-31&startDate=2022-12-01';


//Initiate cURL.
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
//Specify the username and password using the CURLOPT_USERPWD option. 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'x-apiKey: vTaHJ9jJCVgskBfXPxW1C8hWk',
	  'x-secret: uJYoHJzhlgZwfT3mMyHK9eLXb',
      'Content-Type: application/json',
	  
   ));
//Tell cURL to return the output as a string instead
//of dumping it to the browser.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//Execute the cURL request.
$response = curl_exec($ch);
 
//Check for errors.
if(curl_errno($ch)){
    //If an error occured, throw an Exception.
    throw new Exception(curl_error($ch));
}

//Print out the response.
echo $response;
?>