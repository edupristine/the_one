<?php

/* Template Name: Migrate students LMS(CRM Based) to ACEIT
 * Description: Migrating and Enrolling the students from crm to the ACEIT, Yesterday CFA Enrolled Students.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 1st June, 2021
 * Created By: Hitesh P
 *
 **/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

$yesterday = date("Y-m-d",strtotime("-1 days"));
$today = date("Y-m-d");
//Orginal Query		
$sql= "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		,sc.cf_1487 AS `not_access`,sc.cf_1467 AS `course_access_flag`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`,ad.bill_country as `country`,sc.cf_1649 as `modules`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE  sc.cf_918 = 'CFA' 
		AND ce.smownerid!='3920' 
		AND ce.smownerid!='4754'
		AND sc.cf_1489=0
		AND sc.cf_1513 in 
		(
		'CFA Level I',
		'CFA Level I Crash Course',
		'CFA Level I Crash Course LVC',
		'CFA level I LVC',
		'CFA Level I Self study Package',
		'Self-Study Package',
		'CFA FT LVC',
		'CFA FT'
		)
		AND ac.account_no NOT IN ('ACC58948',
		'ACC58193',
		'ACC55344',
		'ACC58929',
		'ACC58687',
		'ACC53289',
		'ACC56718',
		'ACC58680',
		'ACC59001',
		'ACC58123',
		'ACC59047',
		'ACC59092',
		'ACC53505',
		'ACC57939',
		'ACC59306',
		'ACC59325',
		'ACC59440',
		'ACC59504',
		'ACC59491',
		'ACC55333',
		'ACC55335',
		'ACC55334',
		'ACC51700',
		'ACC51701',
		'ACC59368',
		'ACC58455',
		'ACC59186',
		'ACC58102',
		'ACC56218',
		'ACC58306',
		'ACC58169',
		'ACC57338',
		'ACC59450',
		'ACC58794',
		'ACC59442',
		'ACC59539',
		'ACC59736',
		'ACC56723',
		'ACC59742',
		'ACC52928',
		'ACC59743',
		'ACC58727',
		'ACC58497',
		'ACC56174',
		'ACC59924',
		'ACC59844',
		'ACC59589',
		'ACC58189',
		'ACC59586',
		'ACC59033',
		'ACC57969',
		'ACC59784',
		'ACC58373',
		'ACC59926',
		'ACC59629',
		'ACC55719',
		'ACC57884',
		'ACC59641',
		'ACC59772',
		'ACC59950',
		'ACC59324',
		'ACC59112',
		'ACC59597',
		'ACC55864',
		'ACC55865',
		'ACC60115',
		'ACC59028',
		'ACC58010',
		'ACC59389',
		'ACC52727',
		'ACC60258',
		'ACC58363',
		'ACC58492',
		'ACC58932',
		'ACC56264',
		'ACC57296',
		'ACC57349',
		'ACC59993',
		'ACC57586',
		'ACC58424',
		'ACC59175',
		'ACC54107',
		'ACC54219',
		'ACC59089',
		'ACC60455',
		'ACC59951',
		'ACC59257',
		'ACC59088',
		'ACC59129',
		'ACC60682',
		'ACC40163',
		'ACC59733',
		'ACC59026',
		'ACC51459',
		'ACC60914',
		'ACC60612',
		'ACC59123',
		'ACC59404',
		'ACC59181',
		'ACC58756',
		'ACC60108',
		'ACC53878',
		'ACC58559',
		'ACC59501',
		'ACC49958',
		'ACC59730',
		'ACC60724',
		'ACC54886',
		'ACC61104',
		'ACC59367',
		'ACC59027',
		'ACC59477',
		'ACC60641',
		'ACC56596',
		'ACC61001',
		'ACC58313',
		'ACC57938',
		'ACC59713',
		'ACC60932',
		'ACC60590',
		'ACC60636',
		'ACC47920',
		'ACC57650',
		'ACC58579',
		'ACC61375',
		'ACC59417',
		'ACC60925',
		'ACC57761',
		'ACC59036',
		'ACC59585',
		'ACC59817',
		'ACC61011',
		'ACC60215',
		'ACC61467',
		'ACC61466',
		'ACC61180',
		'ACC55562',
		'ACC58353',
		'ACC59018',
		'ACC59017',
		'ACC59091',
		'ACC58577',
		'ACC59338',
		'ACC58026',
		'ACC58395',
		'ACC59652',
		'ACC61157',
		'ACC58846',
		'ACC61140',
		'ACC58582',
		'ACC58893',
		'ACC61691',
		'ACC58857',
		'ACC59318',
		'ACC61703',
		'ACC61704',
		'ACC58134',
		'ACC60503',
		'ACC58356',
		'ACC61731',
		'ACC61681',
		'ACC60954',
		'ACC58357',
		'ACC58394',
		'ACC59767',
		'ACC61269',
		'ACC59355',
		'ACC59116',
		'ACC58376',
		'ACC59676',
		'ACC56130',
		'ACC58296',
		'ACC59867',
		'ACC61382',
		'ACC59846',
		'ACC61083',
		'ACC61919',
		'ACC56380',
		'ACC59945',
		'ACC60930',
		'ACC61881',
		'ACC54440',
		'ACC57176',
		'ACC61532',
		'ACC57950',
		'ACC58203',
		'ACC61225',
		'ACC62691',
		'ACC59914',
		'ACC60894',
		'ACC60401',
		'ACC61767',
		'ACC62769',
		'ACC56313',
		'ACC61771',
		'ACC61514',
		'ACC61848',
		'ACC59627',
		'ACC63206',
		'ACC61722',
		'ACC62140',
		'ACC62950',
		'ACC62065',
		'ACC63479',
		'ACC56231',
		'ACC62725',
		'ACC63595',
		'ACC61516',
		'ACC63326',
		'ACC63719',
		'ACC63720',
		'ACC63721',
		'ACC59333',
		'ACC61295',
		'ACC59345',
		'ACC63153',
		'ACC63779',
		'ACC63043',
		'ACC63044',
		'ACC62566',
		'ACC59162',
		'ACC62003',
		'ACC63653',
		'ACC59408',
		'ACC50647',
		'ACC62147',
		'ACC63127',
		'ACC62281',
		'ACC63864',
		'ACC63495',
		'ACC59577',
		'ACC59448',
		'ACC62550',
		'ACC58737',
		'ACC64281',
		'ACC64280',
		'ACC64223',
		'ACC62207',
		'ACC64013',
		'ACC63812',
		'ACC57337',
		'ACC57244',
		'ACC57262',
		'ACC63923',
		'ACC63466',
		'ACC64397',
		'ACC64405',
		'ACC61744',
		'ACC64438',
		'ACC64161',
		'ACC64226',
		'ACC64441',
		'ACC59298',
		'ACC64646',
		'ACC64688',
		'ACC64684',
		'ACC64717',
		'ACC63525',
		'ACC64793',
		'ACC64837',
		'ACC64049',
		'ACC64652',
		'ACC64653',
		'ACC64218',
		'ACC59080',
		'ACC57221',
		'ACC48878',
		'ACC60325',
		'ACC64526',
		'ACC63803',
		'ACC64708',
		'ACC64944',
		'ACC64187',
		'ACC64079',
		'ACC62183',
		'ACC65067',
		'ACC57762',
		'ACC58079',
		'ACC58344',
		'ACC64426',
		'ACC64592',
		'ACC64083',
		'ACC64847',
		'ACC64951',
		'ACC65011',
		'ACC58032',
		'ACC61445',
		'ACC64501',
		'ACC64716',
		'ACC64680',
		'ACC64985',
		'ACC64960',
		'ACC63827',
		'ACC61953',
		'ACC65428',
		'ACC64123',
		'ACC58820',
		'ACC65716',
		'ACC65618',
		'ACC65799',
		'ACC61139',
		'ACC64356',
		'ACC64569',
		'ACC65738',
		'ACC65949',
		'ACC64263',
		'ACC65224',
		'ACC64305',
		'ACC64294',
		'ACC64298',
		'ACC64151',
		'ACC65985',
		'ACC66102',
		'ACC65978',
		'ACC66145',
		'ACC66146',
		'ACC66231',
		'ACC64672')
		AND DATE(ce.createdtime) >= '2021-11-01'
		 ";		

		
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
     //For  ACEIT ID array 
	 $prodid="";
	 $acesql= "SELECT aceit_id from aceit_course_map WHERE crm_course='".$row['course']."' AND crm_subcourse='".$row['subcourse']."' AND is_active=1";		
     $resultac = $conn2->query($acesql);
	 $rowac = $resultac->fetch_assoc();
	 
	 $toaccess="";
	 if($row['subcourse']=='Self-Study Package')
		{
			$finalarray= "";
			$sel_modules=explode(" |##| ",$row['modules']);
			$total_count=count($sel_modules);
		
			foreach($sel_modules as $mods)
			{		
			$modql= "SELECT MOODLE_ID from MODULE_MULTISELECT WHERE MODULE_NAME='".$mods."'";		
			$resultacmod = $conn1->query($modql);
			$rowacmod = $resultacmod->fetch_assoc();
			$finalarray .=$rowacmod['MOODLE_ID'].",";
			
			}
			
			if($total_count>=5)
			{
			$finalarray .="38,";
			}
			
			$finalarray .="42,44,107,108,109,";
			 
			$toaccess=rtrim($finalarray, ',');
		}else
		{
			$toaccess=$rowac['aceit_id'];
		}
		
	
	    $tofinalaccess=explode(",",$toaccess);
		
		
	    foreach ($tofinalaccess as $aceid)
		{
		//echo $row['first_name'].",".$row['last_name'].",".$row['email'].",".$aceid.","."Mumbai".","."India<br>";	
		echo grantaccesslms($row['first_name'],$row['last_name'],$row['email'],$aceid,"Mumbai","India");	
		}
	//	echo $row['first_name'].",".$row['last_name'].",".$row['email'].",84,"."Mumbai".","."India<br>";	
		echo grantaccesslms($row['first_name'],$row['last_name'],$row['email'],84,"Mumbai","India");	
		echo grantaccesslms($row['first_name'],$row['last_name'],$row['email'],37,"Mumbai","India");	
		//Update CRM Course Access Flag
		$updatecaccesflag="UPDATE `vtiger_accountscf` SET cf_1467=1,cf_1469='".$today."' WHERE accountid=".$row['accid'];
		//echo $updatecaccesflag."<p>";
		$resultup = $conn1->query($updatecaccesflag);
		
	}
	
function grantaccesslms($fname,$lname,$email,$prodid,$location,$country)
{
			
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_create.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_enroll.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_get_userid.php");
 
        $user = get_moodleuser($email); //Check the user with email address
		//print_r($user);
		if($user == 0 || !isset($user)){
			create_moodleuser(strtolower($email),$fname,$lname,$location,$country);
			enroll_moodleuser(strtolower($email), $prodid);
			 $result = "New ACEIT User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
			
		}else{
		     enroll_moodleuser(strtolower($email), $prodid);
			 $result = "Existing User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
		}
		
		return $result;
		

}	

mysqli_close($conn1);



?>
