<?php
/* Template Name: Receivable Cron Revised
 * Description: Reminder mail to students for payment before due date.
 * Version: 1.0
 * Created On: 27th October, 2020
 * Created By: EduPristine
 **/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';


include_once("dbconfig.php");
setlocale(LC_MONETARY, 'en_IN');
$conn1=mysqli_connect($db_server,$db_username,$db_password,'vtigercrm6');
$conn2=mysqli_connect($db_server,$db_username,$db_password,'edupristine_one');
$conn3=mysqli_connect($db_server,$db_username,$db_password,'ecademe_harry_new');

$today = date("Y-m-d");
$other_students = array();

$query = "SELECT code FROM workshops WHERE `batch_status` = 'In Progress'";

$result = mysqli_query($conn3,$query);

while($re = mysqli_fetch_assoc($result)) {
    $codes[] = $re['code'];
}

$sql = "SELECT ac.account_no,ac.accountname,ac.email1 as `email`, ac.phone,sc.cf_918 as `course`,
sc.cf_1495 as `reg_batch`, sc.cf_936 as `offered_price`, sc.cf_938 as `paid`, sc.cf_1497 as `at1`,sc.cf_1499 as `at2`,
sc.cf_1501 as `at3`, sc.cf_1503 as `at4`, sc.cf_1505 as `at5`, ce.smownerid as `salesperson`, sc.cf_1629 as `center`,sc.cf_1571 as `complementary_code`, sc.cf_1575 as `rescheduling_code`, DATE(ce.createdtime) as`created_on`,ac.accountid
FROM vtiger_account ac
LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
WHERE sc.cf_936 > sc.cf_938  AND ce.smownerid != 3920";
$result1 = mysqli_query($conn1,$sql);
$students = array();
while($res = mysqli_fetch_assoc($result1)) {
$students[] = $res;
}

foreach($students as $student)
{
	/*echo "Student-Org:".$student['account_no']."<br>";
	echo "Student-Name:".$student['accountname']."<br>";
	echo "Email:".$student['email']."<br>";
	echo "Course:".$student['course']."<br>";
	echo "Complementry:".$student['complementary_code']."<br>";
	echo "Offered_price:".$student['offered_price']."<br>";
	echo "Paid:".$student['paid']."<br>";*/
	
	$batch_codes = array($student['reg_batch'],$student['at1'],$student['at2'],$student['at3'],$student['at4'],$student['at5']);
	//print_r($batch_codes);
	

	$batch_codes = array_unique($batch_codes);
	$batch_codes = array_filter($batch_codes);

	$locQuery = "SELECT city FROM workshops WHERE code='".$student['reg_batch']."'";
	$locresult = mysqli_query($conn3,$locQuery);
	$locres = mysqli_fetch_assoc($locresult);
	if (($key = array_search("Online", $batch_codes)) !== false) {
	unset($batch_codes[$key]);
	}
	if($student['center'] == ""){
	if($locres['city'] == 'Andheri')
	$student['center'] = 'Mumbai';
	else
	$student['center'] = $locres['city'];
	}
	if($student['center'] == "Online" || $student['center'] == "LVC" || $student['center'] == "LVC / Self Study")
	{
	$student['center'] = "LVC";
	}
	
	$total_sessions = $conducted = 0;
	$org_codes = "'";
	foreach($batch_codes as $codes){
	if($codes != 'Online' && $codes != 'Code Unavailable'){

	$dqry = "SELECT COUNT(*) as `conducted` FROM workshops_dates WHERE batch_code = '".$codes."' AND status = 'Conducted'";
	$dresult = mysqli_query($conn3,$dqry);
	$dres = mysqli_fetch_assoc($dresult);
	$conducted += $dres['conducted'];
    $org_codes .= $codes."', '";
	}
	}
	//Total Sessions available in Harry
	$total_sessions = total_number_of_session($student['account_no'],$student['reg_batch'],$conn2,$conn3);
		
        $org_codes = substr($org_codes,0,-3);
        $student['batch'] = $org_codes;
        if($total_sessions !=0 ){
            $conducted_percent = round(($conducted/$total_sessions)*100,0);
        }else{
            $conducted_percent = 0;
        }
		
	    //Get Paid Percentt  
        $paid_percent = round(($student['paid']/$student['offered_price'])*100,0);
		
		
        $next_session="";
		$orgQry = "SELECT MIN(ws_date) as`ws_date` FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."'";
		$orgResult = mysqli_query($conn3,$orgQry);
		$orgRes = mysqli_fetch_assoc($orgResult);
		
        if($paid_percent < 100){
        $student_data = $student['account_no'].",".$student['email'].",".$total_sessions.",".$paid_percent;
        array_push($students_all,$student_data);
        }
		
        array_push($students_cal,$student['account_no']);
	

        $tDate ="";
        if($total_sessions/4 > $conducted && $paid_percent < 50){
            $remaining_session = ceil($total_sessions/4 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
		    $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
			
			
        }elseif($total_sessions/2 > $conducted && $paid_percent < 75){
            $remaining_session = ceil($total_sessions/2 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
			
            $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
			
			
        }elseif($total_sessions*0.75 > $conducted && $paid_percent < 100){
            $remaining_session = ceil($total_sessions*0.75 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
			
            $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
			
			
        }elseif($conducted_percent > $paid_percent){
            $tDate = date("Y-m-d");
            $student['due_date'] = $tDate;
            $student['session_percentage'] = $conducted_percent;
        }
		
		
        $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
        $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
        $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $next_installment = 0;
		
		$sessions_till_due_date = "select count(id) as UpcomingTillDue from workshops_dates where batch_code = '".$codes."' AND ws_date <= '".$tDate."' and status = 'Upcoming'";
		$result_sessions_till_due_date = mysqli_query($conn3,$sessions_till_due_date);
		$dres_sess_til_due = mysqli_fetch_assoc($result_sessions_till_due_date);
		$upcomingtilldue = $dres_sess_til_due['UpcomingTillDue'];
		if($total_sessions == 0)
			$total_sessions = 1;
		$new_conducted_check = $conducted_percent + ($upcomingtilldue * 100/$total_sessions);
		$new_conducted_check = round($new_conducted_check,0);
		
		
		
        if($new_conducted_check < 25 && $tDate!="" && $paid_percent <50){
            $next_installment = $balance_50;
        }elseif($new_conducted_check >= 25 && $new_conducted_check <50 && $paid_percent <50){
            $next_installment = $balance_50;
            }elseif($new_conducted_check >= 50 && $new_conducted_check <75 && $paid_percent <75){
                $next_installment = $balance_75;
            }elseif($new_conducted_check >= 75 && $new_conducted_check <100){
                $next_installment = $balance;
            }
			
			
        $student['amt_to_reach'] = $next_installment + $student['paid'];
        $student['next_installment'] = $next_installment;
		
		//echo $student['account_no']." - ".$new_conducted_check." - ".$student['session_percentage']." - ".$paid_percent."\n";
		
		array_push($other_students,$student);
		//continue;
		
        if($tDate != ""){
        $t = new DateTime();
        $tD = new DateTime($tDate);
        $day_diff = $tD->diff($t);
        $days_left = ($day_diff->days)+1;
        if($days_left == 10){
            array_push($t_10,$student);
            array_push($finalArray,$student);
        }elseif($days_left == 5){
            array_push($t_5,$student);
            array_push($finalArray,$student);
        }elseif($days_left == 1 && ($tDate != date('Y-m-d'))){
            array_push($t_1,$student);
            array_push($finalArray,$student);
        }elseif($days_left < 11){
            array_push($finalArray,$student);
        }
		if($next_installment != 0 && $days_left < 11)
		{
			//Stopped Stamping
        }
		if(in_array($student['account_no'],$leftStudentAcc)){
            $key = array_search($student['account_no'], array_column($leftStudents, 'crm_id'));
            $student['due_date'] = $leftStudents[$key]['next_inst_due_date'];
            $student['next_installment']= $leftStudents[$key]['next_installment'];
            array_push($leftStudents_9_0,$student);
            array_push($finalArray,$student);
        }
		
    }
        
       /* echo $tDate." ".$orgRes['ws_date']." ".$student['account_no']." ".$total_sessions." ".$conducted." ".$conducted_percent." ".$paid_percent."\n";
        echo $days_left."\n"; 
	    echo "<p>";*/
	
}

//print_r($other_students);


if(count($other_students) > 0){
    $html = "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
	$html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Created On</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
	$html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
	$html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>LMS Access Revoked</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Revoked On</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Batch Codes</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Center</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Salesperson</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Offered price</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Paid</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Balance</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Next Installment</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Due Date</b></td>";
    $html .= "</tr>";
    $students_in_table = array();
foreach($other_students as $student){
    if(!in_array($student['account_no'],$students_in_table)){
    $salesperson = get_sales_person($student['salesperson'], $conn1);
	$lms_access_details = get_lms_access_status($student['accountid'], $conn1);
	$value_check = ($lms_access_details['valueupdate']==1) ? "YES" : "NO";
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
		$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['created_on']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
		$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$value_check."</td>";
		$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$lms_access_details['modified_on']."</td>";
		$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['batch']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$salesperson['name']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['offered_price']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['paid']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        if($student['session_percentage'] < 25){
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_50."</td>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_75."</td>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        }
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$student['due_date']."</td></tr>";
        array_push($students_in_table,$student['account_no']);
    }
}
    $subject = "Receivables - Updated Student List";
    $html .= "</table></br>";
	echo $html;
	//send_smt_mail($subject,$html,$student['email']);
	exit;
}

function total_number_of_session($org,$batch,$conn2,$conn3){
        $bqry = "SELECT batch_type FROM workshops WHERE code ='".$batch."'";
            $bresult = mysqli_query($conn3,$bqry);
            $bres = mysqli_fetch_assoc($bresult);
            if($bres['batch_type'] != NULL && $bres['batch_type'] != ""){
                $batch_type = $bres['batch_type'];
				if($batch_type == '1')
				{
					$batch_type = "Weekdays";
				}
				elseif($batch_type == '2')
				{
					$batch_type = "Weekend";
				}
				elseif($batch_type == '3')
				{
					$batch_type = "Saturday Only";
				}
				elseif($batch_type == '4')
				{
					$batch_type = "Sunday Only";
				}
            }else{
				$ws_dates = array();
                $qrywsd = "SELECT ws_date FROM workshops_dates WHERE batch_code='".$batch."' ORDER BY ws_date ASC LIMIT 3";
                $reswd = mysqli_query($conn3,$bqry);
                while($re = mysqli_fetch_assoc($reswd)) {
                    $ws_dates[] = $re['ws_date'];
                }
                    $d1 = new DateTime($ws_dates[0]);
                    $d2 = new DateTime($ws_dates[1]);
                    $d3 = new DateTime($ws_dates[2]);
                    $day_diff_d2d1 = $d2->diff($d1);
                    $day_diff_d3d2 = $d3->diff($d2);
                    $d2d1 = $day_diff_d2d1->days;
                    $d3d2 = $day_diff_d3d2->days;
                    if($d2d1 == 1 && $d3d2 == 1){
                        $batch_type = "Weekdays";
                    }elseif($d2d1 == $d3d2){
                        $day = date("l",strtotime($d1->date));
                        if($day == 'Saturday'){
                            $batch_type = "Saturday Only";
                        }else{
                            $batch_type = "Sunday Only";
                        }
                    }else{
                        $batch_type = "Weekend";
                    }

    }

        
    $qry = "SELECT count(ws_date) as `no_of_session` FROM workshops_dates WHERE batch_code='".$batch."' ORDER BY ws_date ASC ";
    $result = mysqli_query($conn3,$qry);
    if(mysqli_num_rows($result)){
    $re = mysqli_fetch_assoc($result);
    return $re['no_of_session'];
    }else{
        return 0;
    }
}

function multi_sort($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    return $ret;
}

function get_sales_person($id,$conn){
    $qry = "SELECT CONCAT(first_name,' ',last_name) as `name`,email1 as `email` FROM vtiger_users WHERE id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_lms_access_status($id,$conn){
	$qry = "SELECT vb.whodid AS `modifier`,DATE(vb.changedon) AS `modified_on`,vmd.postvalue AS `valueupdate` FROM vtiger_modtracker_basic vb,vtiger_modtracker_detail vmd WHERE vb.id=vmd.id  AND vmd.fieldname='cf_1489'  AND vb.crmid=".$id." ORDER BY vb.changedon DESC LIMIT 1";
	$result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_sales_person_reporting($id,$conn){
    $qry = "SELECT CONCAT(vu.first_name,' ',vu.last_name) as `name`,vu.email1 as `email`,(SELECT CONCAT(first_name,' ',last_name) as `name` FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager`,(SELECT  email1 FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager_email` FROM vtiger_users vu WHERE  id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_center_team($city, $conn){
	if($city == "Thane")
	{
		$city = "Mumbai";
	}
    $qry = "SELECT user_name,user_email FROM users u
    LEFT JOIN users_teams ut ON ut.user_id = u.id
    LEFT JOIN teams t ON t.id = ut.team_id
    WHERE ut.team_id = 4 AND u.loc_id = (SELECT id FROM locations WHERE loc_name = '".$city."' AND loc_type = 'CENTER')";
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function send_smt_mail($subject,$email_msg,$email,$email_name,$cc = "",$cc_name = "",$outside_receiver = 0){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		if($outside_receiver == 1)
		{
			//$mail->setFrom('notifications@edupristine.com', 'Team EduPristine');
		    $mail->setFrom('notifications@inmail.edupristine.com', 'Team EduPristine');
		}
		else
		{
			//$mail->setFrom('notifications@edupristine.com', 'Receivables Report');
			$mail->setFrom('notifications@inmail.edupristine.com', 'Receivables Report');
		}
        
        $mail->addAddress($email, $email_name);
		if($cc != "")
		{
			$mail->addCC($cc, $cc_name);
		}
		$mail->addCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
        $mail->addCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		$mail->addCC('finance@edupristine.com', 'Aamir Khan');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
