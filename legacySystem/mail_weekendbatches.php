<?php

function send_smt_mail($subject,$email_msg){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		//$mail->setFrom('notifications@edupristine.com', 'Edupristine Reports');
		$mail->setFrom('notifications@inmail.edupristine.com', 'Edupristine Reports');
		
   		$mail->addCC('faculty@edupristine.com', 'Faculty Team');
		$mail->addCC('teamedupristinesupport@edupristine.com', 'Support Team');
		$mail->addCC('delhioperations@edupristine.com', 'Delhi Operations Team');
		$mail->addCC('admin.delhi@edupristine.com', 'Delhi Admin Team');
		$mail->addCC('bangaloreoperations@edupristine.com', 'Bangalore Operations Team');
		$mail->addCC('marcom@edupristine.com', 'Marketing Team');
		$mail->addCC('sales.managers@edupristine.com', 'Sales Managers');
		$mail->addCC('teamleaders@edupristine.com', 'Team Leaders');
		$mail->addBCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		$mail->addCC('mumbaicareers@edupristine.com', 'Mumbai Careers');
		$mail->addCC('dixit.chouhan@edupristine.com', 'Dixit Chouhan');
        $mail->addCC('rohini.toke@edupristine.com', 'Rohini Toke');
		$mail->addCC('chennaioperation@edupristine.com', 'Chennai Operations Team');
		$mail->addCC('raghuvardhan.g@edupristine.com', 'Raghuvardhan G');

		
		
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
?>