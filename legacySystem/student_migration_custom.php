<?php
/* Template Name: Migrate students to The One
 * Description: Migrating the students from crm to the one, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 8th July, 2019
 * Created By: Ankur
 *
 **/

// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/checklogin.php';
// include '../control/connection.php';
include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
include '/home/ubuntu/webapps/the_one/api/biometric_api.php';
$yesterday = date("Y-m-d",strtotime("-1 days"));

/*$sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		,sc.cf_1487 AS `not_access`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE DATE(ce.createdtime) >= '2020-03-03'";*/

$sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE ac.account_no in (
'ACC58349',
'ACC58348',
'ACC53344',
'ACC53201',
'ACC59415',
'ACC57918',
'ACC52836'
)";
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
    $query = "SELECT ac.email1,sc.cf_918,sc.cf_1513 FROM vtiger_account ac
    LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
    LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
    INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
    WHERE ac.email1 = '".$row['email']."' AND sc.cf_918 = '".$row['course']."' 
    AND sc.cf_1513 = '".$row['subcourse']."'";
    $locQuery = "SELECT id FROM locations where loc_name = '".$row['location']."'";
    $rloc = $conn2->query($locQuery);
    $locCount = mysqli_num_rows($rloc);
    if($locCount < 1){
        $row['location'] = 'Others';
    }
    $result1 = $conn1->query($query);
    $count = mysqli_num_rows($result1);
    if($count > 1){
        $checkQuery = "SELECT * FROM students where course_id =(SELECT id FROM courses where course_name = '".$row['course']."') AND
        subcourse_id = (SELECT id FROM sub_courses where subcourse_name = '".$row['subcourse']."') AND student_email = '".$row['email']."'";
        $checkResult = $conn2->query($checkQuery);
        $resultCount = mysqli_num_rows($checkResult);
        if($resultCount == 0){
            $insertQuery = "INSERT INTO students (crm_id,batch_id,course_id,subcourse_id,loc_id,reg_batch,
            student_name,student_contact,student_email,is_active,created_at,created_by) VALUES ('".$row['account_no']."',
            0,(SELECT id FROM courses where course_name = '".$row['course']."'),
            (SELECT id FROM sub_courses where subcourse_name = '".addslashes($row['subcourse'])."' AND course_id = 
            (SELECT id FROM courses where course_name = '".$row['course']."')),
            (SELECT id FROM locations where loc_name = '".$row['location']."'),'".$row['reg_batch']."','".addslashes($row['accountname'])."','".$row['phone']."',
            '".$row['email']."',1,'".$row['created_at']."',1)";
            $r1 = InsertWo($insertQuery,$conn,"students");
            $insertId = $r1['id'];
            auto_req($insertId);
			course_module($insertId);
			
			//For Biometric Creation
			if(($row['location']!='LVC / Self Study')||($row['location']!='Others')){
			$branchName=$row['location'];
			$dob='1980-12-01 00:00:00';
			$doj=$row['created_at'];
			$dept=$row['course'];
			$econtact=$row['phone'];
			$crmid=$row['account_no'];
			$newname=$row['accountname'].$crmid;
			$newname2=$crmid.$row['accountname'];
			$name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
			$gender="MALE";
			$marital="SINGLE";
			$mob=$row['phone'];
			$designationname=$row['subcourse'];

			createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
			}
			
        }else{
            $insertQuery = "INSERT INTO parked (crm_id,batch_id,course_id,subcourse_id,loc_id,reg_batch,
            student_name,student_contact,student_email,is_active,created_at,created_by) VALUES ('".$row['account_no']."',
            0,(SELECT id FROM courses where course_name = '".$row['course']."'),
            (SELECT id FROM sub_courses where subcourse_name = '".addslashes($row['subcourse'])."' AND course_id = 
            (SELECT id FROM courses where course_name = '".$row['course']."')),
            (SELECT id FROM locations where loc_name = '".$row['location']."'),'".$row['reg_batch']."','".addslashes($row['accountname'])."','".$row['phone']."',
            '".$row['email']."',1,'".$row['created_at']."',1)";
            $r2 = InsertWo($insertQuery,$conn,"parked");
        }
    }
    else{
        $insertQuery = "INSERT INTO students (crm_id,batch_id,course_id,subcourse_id,loc_id,reg_batch,
        student_name,student_contact,student_email,is_active,created_at,created_by) VALUES ('".$row['account_no']."',
        0,(SELECT id FROM courses where course_name = '".$row['course']."'),
        (SELECT id FROM sub_courses where subcourse_name = '".addslashes($row['subcourse'])."' AND course_id = 
        (SELECT id FROM courses where course_name = '".$row['course']."')),
        (SELECT id FROM locations where loc_name = '".$row['location']."'),'".$row['reg_batch']."','".addslashes($row['accountname'])."','".$row['phone']."',
        '".$row['email']."',1,'".$row['created_at']."',1)";
        $r3 = InsertWo($insertQuery,$conn,"students");
        $insertId = $r3['id'];
        auto_req($insertId);
		course_module($insertId);
		
		//For Biometric Creation
		if(($row['location']!='LVC / Self Study')||($row['location']!='Others')){
		$branchName=$row['location'];
		$dob='1980-12-01 00:00:00';
		$doj=$row['created_at'];
		$dept=$row['course'];
		$econtact=$row['phone'];
		$crmid=$row['account_no'];
		$newname=$row['accountname'].$crmid;
		$newname2=$crmid.$row['accountname'];
		$name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
		$gender="MALE";
		$marital="SINGLE";
		$mob=$row['phone'];
		$designationname=$row['subcourse'];
		
		createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
		}
		
		
    }
}
mysqli_close($conn1);
mysqli_close($conn2);

/*
To direct update the req. of the sku against the subcourses of the students when the RRF flag is true at the respective subcourse 
Tables affected 
Issuance Table
Sku_location Table
Student Table
*/
function auto_req($student_id)
{
global $conn;		
// Fetch student details
$select_rrf="SELECT sc.rrf as rrf,sc.id as subcourse,s.loc_id as location  FROM students s, sub_courses sc 
            WHERE s.subcourse_id=sc.id AND sc.rrf=1  and s.id='".$student_id."'";
$result_rrf = Select($select_rrf,$conn,"students");	 
$subcourse_rrf=$result_rrf['rows'][0]['rrf'];		
$student_location=$result_rrf['rows'][0]['location'];
$student_subcourse=$result_rrf['rows'][0]['subcourse'];

if($subcourse_rrf==1)
{
// Select sub courses skus	
$select_scrrf="SELECT sku_id  FROM subcourses_skus WHERE subcourse_id='".$student_subcourse."'";
$result_scrrf = Select($select_scrrf,$conn,"students");		

foreach($result_scrrf['rows'] as $cb)
{
$insert_req="INSERT INTO `issuance`(`sku_id`, `student_id`) VALUES ('".$cb['sku_id']."','".$student_id."')";
$result_req = Insert($insert_req,$conn,"issuance");
//echo $insert_req."<br>";

$update_stock = "UPDATE skus_locations set sku_qty_req = sku_qty_req + 1 WHERE sku_id = '".$cb['sku_id']."'   and loc_id = ".$student_location."";
$result_stock = Update($update_stock,$conn,"skus_locations");
//echo $update_stock."<br>";
}

//echo $update_student."<br><br><br><br><p>";
}
}

/*
Course Module Association of the Students on the creation of the student in the one
tables affected
students_course_modules
*/
function course_module($student_id)
{
global $conn;	

$select_students="SELECT course_id from students where id='".$student_id."'";
$result_students = Select($select_students,$conn,"students");	
foreach($result_students['rows'] as $student)
{ 	
$student_course=$student['course_id'];

if(($student_course!='')&&($student_course!=Null)){
$select_cm="SELECT cm.id,cs.course_name,cm.course_module from courses cs, course_modules cm where cs.id=cm.course_id and cs.cm_flag=1 and cs.id=".$student_course."  and cm.is_active=1";

$result_cm = Select($select_cm,$conn,"course_modules");	

foreach($result_cm['rows'] as $cm)
{   
	$cm_id=$cm['id'];
	$course_name=$cm['course_name'];
	$course_bat=$cm['course_module'];
	
	$insert_cm="INSERT INTO `students_course_modules`(`student_id`,`course_module_id`) VALUES (".$student_id.",".$cm_id.")";
	$result_cm = Insert($insert_cm,$conn,"students_course_modules");
    //echo $insert_cm."<br>";
	
	
}
}
}

}
?>
