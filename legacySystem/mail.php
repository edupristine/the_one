<?php

function send_smt_mail($subject,$email_msg,$email,$email_name,$cc = "",$cc_name = "",$outside_receiver = 0){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@edupristine.com', 'Finance EduPristine');
		
        
        $mail->addAddress($email, $email_name);
		if($cc != "")
		{
			$mail->addCC($cc, $cc_name);
		}
		$mail->addCC('rajesh.puri@edupristine.com', 'Rajesh Puri');
		$mail->addCC('aamirt.khan@edupristine.com', 'Aamir Khan');
		$mail->addCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}




function send_smt_mail_survey($subject,$email_msg,$email,$email_name,$cc = "",$cc_name = "",$outside_receiver = 0){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@edupristine.com', 'Exam Survey EduPristine');
		
        
        $mail->addAddress($email, $email_name);
		if($cc != "")
		{
			$mail->addCC($cc, $cc_name);
		}
		$mail->addCC('hitesh.patil@edupristine.com', 'Hitesh Patil');

		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
?>