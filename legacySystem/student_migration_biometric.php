<?php
/* Template Name: Migrate students to The One
 * Description: Migrating the students from crm to the one, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 8th July, 2019
 * Created By: Ankur
 *
 **/

// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/checklogin.php';
// include '../control/connection.php';
include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
include '/home/ubuntu/webapps/the_one/api/biometric_api.php';
$yesterday = date("Y-m-d",strtotime("-1 days"));
$sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE DATE(ce.createdtime) = '2020-01-11'";
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
    $query = "SELECT ac.email1,sc.cf_918,sc.cf_1513 FROM vtiger_account ac
    LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
    LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
    INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
    WHERE ac.email1 = '".$row['email']."' AND sc.cf_918 = '".$row['course']."' 
    AND sc.cf_1513 = '".$row['subcourse']."'";
    $locQuery = "SELECT id FROM locations where loc_name = '".$row['location']."'";
    $rloc = $conn2->query($locQuery);
    $locCount = mysqli_num_rows($rloc);
    if($locCount < 1){
        $row['location'] = 'Others';
    }
    $result1 = $conn1->query($query);
    $count = mysqli_num_rows($result1);
    if($count > 1){
        $checkQuery = "SELECT * FROM students where course_id =(SELECT id FROM courses where course_name = '".$row['course']."') AND
        subcourse_id = (SELECT id FROM sub_courses where subcourse_name = '".$row['subcourse']."') AND student_email = '".$row['email']."'";
        $checkResult = $conn2->query($checkQuery);
        $resultCount = mysqli_num_rows($checkResult);
        if($resultCount != 0){

			
			//For Biometric Creation
			if(($row['location']!='LVC / Self Study')||($row['location']!='Others')){
			$branchName=$row['location'];
			$dob='1980-12-01 00:00:00';
			$doj=$row['created_at'];
			$dept=$row['course'];
			$econtact=$row['phone'];
			$crmid=$row['account_no'];
			$newname=$row['accountname'].$crmid;
			$newname2=$crmid.$row['accountname'];
			$name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
			$gender="MALE";
			$marital="SINGLE";
			$mob=$row['phone'];
			$designationname=$row['subcourse'];

			createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
			}
			
        }
    }
    else{
        
		
		//For Biometric Creation
		if(($row['location']!='LVC / Self Study')||($row['location']!='Others')){
		$branchName=$row['location'];
		$dob='1980-12-01 00:00:00';
		$doj=$row['created_at'];
		$dept=$row['course'];
		$econtact=$row['phone'];
		$crmid=$row['account_no'];
		$newname=$row['accountname'].$crmid;
		$newname2=$crmid.$row['accountname'];
		$name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
		$gender="MALE";
		$marital="SINGLE";
		$mob=$row['phone'];
		$designationname=$row['subcourse'];
		
		createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
		}
		
		
    }
}
mysqli_close($conn1);
mysqli_close($conn2);


?>
