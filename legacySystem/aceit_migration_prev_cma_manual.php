<?php

/* Template Name: Migrate students LMS(CRM Based) to ACEIT
 * Description: Migrating and Enrolling the students from crm to the ACEIT, PREV CFA Enrolled Students from 01-09-2020 till 01-09-2021.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 1st June, 2021
 * Created By: Hitesh P
 *
 **/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

$yesterday = date("Y-m-d",strtotime("-1 days"));
$today = date("Y-m-d");
//Orginal Query		
// $sql= "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        // sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		// ,sc.cf_1487 AS `not_access`,sc.cf_1467 AS `course_access_flag`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`,ad.bill_country as `country`,sc.cf_1649 as `modules`
        // FROM vtiger_account ac
        // LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        // LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        // INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        // WHERE  sc.cf_918 = 'CFA' 
		// AND ce.smownerid!='3920' 
		// AND ce.smownerid!='4754'
		// AND sc.cf_1513 in 
		// (
		// 'CFA Level I',
		// 'CFA Level I Crash Course',
		// 'CFA Level I Crash Course LVC',
		// 'CFA level I LVC',
		// 'CFA Level I Self study Package',
		// 'Self-Study Package'
		// 'CFA L1-Ethics',
		// 'CFA FT LVC',
		// 'CFA FT'
		// )
		// AND DATE(ce.createdtime) >= '2020-09-06' AND  DATE(ce.createdtime) <= '2021-09-21'";		

$sql= "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		,sc.cf_1487 AS `not_access`,sc.cf_1467 AS `course_access_flag`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`,ad.bill_country as `country`,sc.cf_1649 as `modules`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE  sc.cf_918 = 'CMA' 
		AND ce.smownerid!='3920' 
		AND ce.smownerid!='4754'
		AND ac.account_no = 'ACC65316'";			
		
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
     //For  ACEIT ID array 
		$aceid="56";
	
		echo grantaccesslms($row['first_name'],$row['last_name'],$row['email'],$aceid,$row['location'],$row['country']);	
		
		
		//Update CRM Course Access Flag
		$updatecaccesflag="UPDATE `vtiger_accountscf` SET cf_1467=1,cf_1469='".$today."' WHERE accountid=".$row['accid'];
		//echo $updatecaccesflag."<p>";
		$resultup = $conn1->query($updatecaccesflag);
		
	}
	
function grantaccesslms($fname,$lname,$email,$prodid,$location,$country)
{
			
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_create.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_enroll.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_get_userid.php");
 
        $user = get_moodleuser($email); //Check the user with email address
		//print_r($user);
		if($user == 0 || !isset($user)){
			create_moodleuser(strtolower($email),$fname,$lname,$location,$country);
			enroll_moodleuser(strtolower($email), $prodid);
			 $result = "New ACEIT User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
			
		}else{
		     enroll_moodleuser(strtolower($email), $prodid);
			 $result = "Existing User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
		}
		
		return $result;
		

}	

mysqli_close($conn1);



?>
