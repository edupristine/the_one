<?php
/* Template Name: Workshops Progress Status
 * Description: Changed the workshop status from Not Started to In progress
 * Version: 1.0
 * Created On: 26th September, 2019
 * Created By: Ankur
 **/
// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/connection.php';

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/connection.php';

include_once("dbconfig.php");
$conn=mysqli_connect($db_server,$db_username,$db_password,'ecademe_harry_new');
$conn1=mysqli_connect($db_server,$db_username,$db_password,'edupristine_one');

$yesterday = date("Y-m-d", strtotime("-1 day"));

$query = "SELECT batch_code FROM workshops_dates WHERE ws_date = '".$yesterday."' AND status = 'Conducted' ORDER BY ws_date ASC";
        $result = mysqli_query($conn, $query);
        while($r1 = mysqli_fetch_assoc($result)) {
            $batchCodes[] = $r1;
        }
        foreach($batchCodes as $code){
            $qry = "SELECT MIN(ws_date) FROM workshops_dates WHERE batch_code='".$code."'";
            $res = mysqli_query($conn,$qry);
            $d = mysqli_fetch_assoc($result);
            if($d == $yesterday){
                $qryUpdate = "UPDATE workshops SET status = 'Running' AND batch_status= 'In Progress' WHERE code='".$code."'";
                mysqli_query($conn, $qryUpdate);
                $qryUpdateOne = "UPDATE batches SET batch_status = 'In Progress' WHERE batch_name = '".$code."'";
                mysqli_query($conn1, $qryUpdateOne);
            }
        }