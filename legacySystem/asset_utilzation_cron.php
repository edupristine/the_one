<?php
//error_reporting(0);
/* Template Name: Asset Utilization  Cron
 * Description: Asset Utilization for last week Saturday and Sunday across centers
 * Version: 1.0
 * Created On: 3st March, 2020
 * Created By: Hitesh
 **/
/*include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail_batches.php';*/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/mail_asset.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
//$mysql_user='localuser';
//$mysql_pass='R@5gull@';
setlocale(LC_MONETARY, 'en_IN');
$conn2=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$connone=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'ecademe_harry_new');


//$startdate='2019-04-01';
//$enddate='2019-12-30';
$date = new DateTime();
$date->modify("last day of previous month");
$enddate= $date->format("Y-m-d");

/*$rec_period_startdate=$_GET['rps'];
$rec_period_enddate=$_GET['rpe'];

$var1month=explode('-',$startdate);
$startmonth=intval($var1month[1]);

$var2month=explode('-',$enddate);
$endmonth=intval($var2month[1]);*/

$location_query="SELECT * FROM locations where id not in (1,2,8,9)";
$result1oc = mysqli_query($connone,$location_query);

while($r2 = mysqli_fetch_assoc($result1oc)) {
	$result_locations[] = $r2;
}

$start = date('Y-m-d', strtotime('last saturday'));
$end = date('Y-m-d', strtotime('last sunday'));

function numWeekdays( $start_ts, $end_ts, $day, $include_start_end = false ) {

    $day = strtolower( $day );
    $current_ts = $start_ts;
    // loop next $day until timestamp past $end_ts
    while( $current_ts < $end_ts ) {

        if( ( $current_ts = strtotime( 'next '.$day, $current_ts ) ) < $end_ts) {
            $days++;
        }
    }

    // include start/end days
    if ( $include_start_end ) {
        if ( strtolower( date( 'l', $start_ts ) ) == $day ) {
            $days++;
        }
        if ( strtolower( date( 'l', $end_ts ) ) == $day ) {
            $days++;
        }
    }   

    return (int)$days;

}

//$sat = numWeekDays( $start, $end, 'saturday', true );  
//$sun = numWeekDays( $start, $end, 'sunday', true );

$sat = 1;  
$sun = 1;
$finaldays=$sat+$sun;


$d=strtotime($start);
$disstart=date("j \ F ", $d);

$ed=strtotime($end);
$diend=date("j \ F Y", $ed);

    

        $html="";
		$html .= "
		<p>Please find below the Asset Utilization Report for all centers</p>
		<table border=1px; style='border:1px solid;border-color:black; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<thead>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid;border-color:black;'>
		<th  colspan=4 style='text-align:left;'>Date: $disstart / $diend</th>
		<th colspan=2> No. of classes conducted	</th>
		<th ></th>
		<th colspan=2>Per day Utilization	</th>
		</tr>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid #000000;'>
		<th rowspan=2>City</th>
		<th rowspan=2>Slots Available/Day</th>
		<th rowspan=2>Total Slots Available</th>
		<th rowspan=2>Classes Scheduled</th>
		<th rowspan=2>In situ</th>
		<th rowspan=2>Ex situ</th>
		<th rowspan=2>Percentage Utilization</th>
		<th rowspan=2>Saturday</th>
		<th rowspan=2>Sunday</th>
		</tr>
		</thead>";
				
		$html .= "<tbody>";
		
		
		foreach($result_locations as $locs)
		{
		$locvar="";
		if($locs['loc_name']=='Mumbai')
		{
		$locvar= "and  wd.location IN ('Andheri','Mumbai')";   
		}
		else
		{
		$locvar= "and  wd.location IN ('".$locs['loc_name']."')";  
		}										   
		$conducted="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday','Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.venue_details!='' $locvar
		ORDER BY wd.batch_code ASC";

		$result1 = mysqli_query($conn1,$conducted);
		$re1conducted = mysqli_fetch_assoc($result1);




		$conductedsat="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday') AND wd.`status` LIKE '%Conducted%'
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sat = mysqli_query($conn1,$conductedsat);
		$re1conductedsat = mysqli_fetch_assoc($result1sat);	


		$conductedsun="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sun = mysqli_query($conn1,$conductedsun);
		$re1conductedsun = mysqli_fetch_assoc($result1sun);	



		$conductedsitu="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday','Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";



		$result1situ = mysqli_query($conn1,$conductedsitu);
		$re1conductedsitu = mysqli_fetch_assoc($result1situ);	

		$conductedexsitu="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday','Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.venue_details!='' $locvar
		AND wd.venue_details not like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1exsitu = mysqli_query($conn1,$conductedexsitu);
		$re1conductedexsitu = mysqli_fetch_assoc($result1exsitu);											


		$calfinal=$locs['slots_available']*$finaldays;	

		$finalsat=round(($re1conductedsat['conducted']/($sat*$locs['slots_available']))*100,0);
		$finalsun=round(($re1conductedsun['conducted']/($sun*$locs['slots_available']))*100,0);
		$perutilize=round(($re1conductedsitu['conducted']/$calfinal)*100,0);

		$perstyle='';
		if($perutilize<=70)
		{
		$perstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$perstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}

		$satstyle='';
		if($finalsat<=60)
		{
		$satstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$satstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}

		$sunstyle='';
		if($finalsun<=60)
		{
		$sunstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$sunstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}

		
		$html .= "<tr style='border:1px solid #000000;'>
		<td><strong>".$locs['loc_name']."</strong></td>
		<td><center><strong>".$locs['slots_available']."</strong></center></td>
		<td><center><strong>".$locs['slots_available']*$finaldays."</strong></center></td>
		<td><center>".$re1conducted['conducted']."</center></td>
		<td><center>".$re1conductedsitu['conducted']."</center></td>
		<td><center>".$re1conductedexsitu['conducted']."</center></td>
		<td ".$perstyle."><center>".$perutilize."%"."</td>
		<td ".$satstyle."><center>".$finalsat."%"."</center></td>
		<td ".$sunstyle."><center>".$finalsun."%"."</center></td></tr>";
		}
		$html .= "<tr style='text-align: center;'>

		<th colspan=6 style='border:0;'>	</th>
		<th style='background-color:#8EA9DB;color:#ffffff;'> < 70% rule</th>
		<th colspan=2 style='background-color:#8EA9DB;color:#ffffff;'>< 60% rule </th>
		</tr></tbody>";
		$html .= "</table><p>";
		
		
		$html .= "
		<table border=1px; style='border:1px solid;border-color:black; width: 70%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<thead>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;'>
		<th ></th>
		<th colspan=4 >Slotwise Utilization</th>
		</tr>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;'>
		<th >City</th>
		<th  colspan=2>Saturday</th>
		<th  colspan=2>Sunday</th>
		</tr>
		
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;'>
		<th></th>
		<th rowspan=2 >Morning</th>
		<th rowspan=2 >Evening</th>
		<th rowspan=2 >Morning</th>
		<th rowspan=2 >Evening</th>
		</tr>
		</thead>";
				
		$html .= "<tbody>";
		
		foreach($result_locations as $locs)
		{
		$locvar="";
		if($locs['loc_name']=='Mumbai')
		{
		$locvar= "and  wd.location IN ('Andheri','Mumbai')";   
		}
		else
		{
		$locvar= "and  wd.location IN ('".$locs['loc_name']."')";  
		}										   


		$conductedsat1="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday') AND wd.`status` LIKE '%Conducted%'
		AND wd.start_time IN ( '09:00 AM','09:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 PM','12:30 PM','01:00 PM','01:30 PM')
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sat1 = mysqli_query($conn1,$conductedsat1);
		$re1conductedsat1 = mysqli_fetch_assoc($result1sat1);	


		$conductedsun1="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.start_time IN ( '09:00 AM','09:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 PM','12:30 PM','01:00 PM','01:30 PM')
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sun1 = mysqli_query($conn1,$conductedsun1);
		$re1conductedsun1 = mysqli_fetch_assoc($result1sun1);




		$conductedsat2="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Saturday') AND wd.`status` LIKE '%Conducted%'
		AND wd.start_time IN ('02:00 PM','02:30 PM','03:00 PM','03:30 PM','04:00 PM','04:30 PM','05:00 PM','05:30 PM','06:00 PM','06:30 PM','07:00 PM','07:30 PM')
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sat2 = mysqli_query($conn1,$conductedsat2);
		$re1conductedsat2 = mysqli_fetch_assoc($result1sat2);	


		$conductedsun2="SELECT  COUNT(wd.id) as `conducted`
		FROM workshops_dates wd
		LEFT JOIN workshops w ON w.id = wd.workshop_id
		LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
		WHERE wd.ws_date >= '".$start."' AND wd.ws_date <= '".$end."' AND wd.delete_flag != 1 
		AND fc.name NOT IN ('No Class','Class Combined','Class Cancelled','Batch Cancelled') AND wd.location!='LVC'
		AND  DAYNAME(wd.ws_date) IN ('Sunday') AND wd.`status` LIKE '%Conducted%'
		AND wd.start_time IN ('02:00 PM','02:30 PM','03:00 PM','03:30 PM','04:00 PM','04:30 PM','05:00 PM','05:30 PM','06:00 PM','06:30 PM','07:00 PM','07:30 PM')
		AND wd.venue_details!='' $locvar
		AND wd.venue_details like '%".$locs['address']."%' 
		ORDER BY wd.batch_code ASC";

		$result1sun2 = mysqli_query($conn1,$conductedsun2);
		$re1conductedsun2 = mysqli_fetch_assoc($result1sun2);											

								

				
		$calfinal=$locs['slots_available']*$finaldays;	

		$satmon=round(($re1conductedsat1['conducted']/($sat*($locs['slots_available']/2)))*100,0);
		$sateven=round(($re1conductedsat2['conducted']/($sat*($locs['slots_available']/2)))*100,0);

		$sunmon=round(($re1conductedsun1['conducted']/($sun*($locs['slots_available']/2)))*100,0);
		$suneven=round(($re1conductedsun2['conducted']/($sun*($locs['slots_available']/2)))*100,0);


		$satmonstyle='';
		if($satmon<=70)
		{
		$satmonstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$satmonstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}


		$satevenstyle='';
		if($sateven<=70)
		{
		$satevenstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$satevenstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}


		$sunmonstyle='';
		if($sunmon<=70)
		{
		$sunmonstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$sunmonstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}

		$sunevenstyle='';
		if($suneven<=70)
		{
		$sunevenstyle="style='text-align: center;background-color:#FFC7CE;color:#AD0055;'";
		}
		else
		{
		$sunevenstyle="style='text-align: center;background-color:#FFFFFF;color:#000000;'";
		}

		$html .="<tr>
		<td><strong> ".$locs['loc_name']."</strong></td>
		<td  ".$satmonstyle."><center> ".$satmon."%</center></td>
		<td  ".$satevenstyle."><center> ".$sateven."%</center></td>
		<td  ".$sunmonstyle."><center> ".$sunmon."%</center></td>
		<td  ".$sunevenstyle."><center>".$suneven."%</center></td>
		</tr>";
		}
		$html .= "</tbody></table>";
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Development Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$today = date("Y-m-d");
		$subject = "  Asset Utilization Report for $disstart - $diend";
		send_smt_mail($subject,$html);
		

?>