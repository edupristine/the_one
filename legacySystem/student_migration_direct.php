<?php
/* Template Name: Migrate students to The One
 * Description: Migrating the students from crm to the one, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 8th July, 2019
 * Created By: Ankur
 *
 **/

// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/checklogin.php';
// include '../control/connection.php';
include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

            $row['email']='srinivasankanniah@gmail.com';
			$row['first_name']='Srinivasan';
			$row['last_name']='Kanniah';
			
			//For  LMS Acces Grant 
		    $row['subcourse']='CFA level I LVC';
			
			
			$prodid="";
			if(($row['subcourse']=='CFA level I LVC'))
			{
			$prodid = "cfa_lvc_001";
			}				
			else if(($row['subcourse']=='CFA level II LVC'))
			{
			$prodid = "cfa_lvc_002";
			}
			else if(($row['subcourse']=='CFA Level III LVC'))
			{
			$prodid = "cfa_lvc_003";	
			}
			else if(($row['subcourse']=='CFA FT LVC'))
			{
			$prodid = "cfa_lvc_ft";	
			}
            else if(($row['subcourse']=='FRM Part I LVC'))
			{
			$prodid = "frm_lvc_001";	
			}			
			
			//echo "Course:-".$prodid; 
			
			$prodid="13330";
			
            if($prodid!="")
			{
			
			
			require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_create.php");
			require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_enroll.php");
			require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_get_userid.php");

			$user = get_moodleuser($row['email']); //Check the user with email address
			if($user == 0 || !isset($user)){
				create_moodleuser(strtolower($row['email']),$row['first_name'],$row['last_name']);
				enroll_moodleuser(strtolower($row['email']), $prodid);
			}else{
				enroll_moodleuser(strtolower($row['email']), $prodid);
			}
		    echo "LMS access for ".$prodid." has been sent to ".$row['email']." Email";
		    
			}
			
			
		
?>
