<?php
/* Template Name: T date Status
 * Description: Changed the T date stamping
 * Version: 1.0
 * Created On: 27th September, 2019
 * Created By: Ankur
 **/

include_once("dbconfig.php");
$conn=mysqli_connect($db_server,$db_username,$db_password,'edupristine_one');
$conn1=mysqli_connect($db_server,$db_username,$db_password,'vtigercrm6');

$qry = "SELECT crm_id,amt_to_reach FROM students WHERE next_inst_due_date IS NOT NULL";
$res = mysqli_query($conn, $qry);
    while($r=mysqli_fetch_assoc($res)){
        $students[] = $r; 
    }
    foreach($students as $student){
        $qryCRM = "SELECT sc.cf_938 as `paid`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE ac.account_no = '".$student['crm_id']."'";
        $res1 = mysqli_query($conn1, $qryCRM);
        $resCRM = mysqli_fetch_assoc($res1);
        if($resCRM['paid']>=$student['amt_to_reach']){
            $qryUpdate = "UPDATE students SET amt_to_reach=0,next_inst_due_date=NULL,next_installment=0 WHERE crm_id='".$student['crm_id']."'";
            mysqli_query($conn, $qryUpdate);
        }
    }
