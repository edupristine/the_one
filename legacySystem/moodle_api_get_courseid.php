<?php
// This file is NOT a part of Moodle - http://moodle.org/
//
// This client for Moodle 2 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * REST client for Moodle 2
 * Return JSON or XML format
 *
 * @authorr Jerome Mouneyrac
 */
 
function get_courseid($idnumber){
/// SETUP - NEED TO BE CHANGED
$token = 'c1ecfc2f1ec5f1b5211678ea9f778384'; // 300119ce4df68e7e20565632adbf8256
$domainname = 'http://aceit.edupristine.com';
$functionname = 'core_course_get_courses_by_field';
// REST RETURNED VALUES FORMAT
$restformat = 'json'; //Also possible in Moodle 2.2 and later: 'json'
                     //Setting it to 'json' will fail all calls on earlier Moodle version

//////// moodle_user_create_users ////////

/// REST CALL
//header('Content-Type: text/plain');
$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
require_once('moodle_curl.php');
$curl = new curl;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
$resp = $curl->post($serverurl . $restformat . "&field=idnumber&value=". $idnumber);
$res_arr_course = json_decode($resp, true);
return $res_arr_course['courses'][0]['id'];

}