<?php
/* Template Name: Daily Org Balance Cron
 * Description: Report for daily org created with balance
 * Version: 1.0
 * Created On: 09th October, 2019
 * Created By: Ankur
 **/
// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/checklogin.php';
// include '../control/connection.php';

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';

include_once("dbconfig.php");
setlocale(LC_MONETARY, 'en_IN');
$conn1=mysqli_connect($db_server,$db_username,$db_password,'vtigercrm6');

$yesterday = date("Y-m-d",strtotime("-1 days"));

$sql = "SELECT ac.account_no,ac.accountname,ac.email1 as `email`, ac.phone,sc.cf_918 as `course`,
        sc.cf_1495 as `reg_batch`, sc.cf_936 as `offered_price`, sc.cf_938 as `paid`, ce.smownerid as `salesperson`, sc.cf_1629 as `center`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE DATE(ce.createdtime) = '".$yesterday."' AND sc.cf_936 > sc.cf_938";

$result1 = mysqli_query($conn1,$sql);
    $students = array();
    while($res = mysqli_fetch_assoc($result1)) {
        $students[] = $res;
    }

$finalArray_smownerid = multi_sort($students, "salesperson");

//Preparing array Salesperson Wise
$final_arr_sales = array();
$student_arr = array();
$ct = 1;
$salesTmp="";
foreach($finalArray_smownerid as $data){
    if($ct == 1){
        $salesTmp = $data['salesperson'];
        $ct = 0;
    }
    if($data['salesperson'] == $salesTmp){
        array_push($student_arr, $data);
    }else{
        $temp_arr = array('salesperson'=>$salesTmp,
                        'students'=>$student_arr);
        array_push($final_arr_sales, $temp_arr);
        $student_arr = array();
        $salesTmp = $data['salesperson'];
        array_push($student_arr, $data);
    }
}
if(count($student_arr) != 0){
$temp_arr = array('salesperson'=>$salesTmp,
                'students'=>$student_arr);
array_push($final_arr_sales, $temp_arr);
}


foreach($final_arr_sales as $stud){
    $html = "<p>Below is the list of students created today with due balance amount.</p>";
	$html .= "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Offered price</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Paid</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Balance</b></td>";
    $html .= "</tr>";
    foreach($stud['students'] as $student){
        $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['offered_price']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['paid']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";	
    }
    $emailstosend = get_sales_person_reporting($stud['salesperson'], $conn1);
    $salesperson = get_sales_person($stud['salesperson'], $conn1);
    $html .= "</table></br>";
	
    $html .= "<p>Thanks & Regards</p>";
    $html .= "<p>Edupristine IT Team</p>";
    $subject = "List of Students with Balance";
	
    send_smt_mail($subject,$html,$emailstosend['email'],$emailstosend['name'],$emailstosend['reporting_manager_email'],$emailstosend['reporting_manager']);
}

mysqli_close($conn1);


function multi_sort($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    return $ret;
}

function get_sales_person($id,$conn){
    $qry = "SELECT CONCAT(first_name,' ',last_name) as `name`,email1 as `email` FROM vtiger_users WHERE id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_sales_person_reporting($id,$conn){
    $qry = "SELECT CONCAT(vu.first_name,' ',vu.last_name) as `name`,vu.email1 as `email`,(SELECT CONCAT(first_name,' ',last_name) as `name` FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager`,(SELECT  email1 FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager_email` FROM vtiger_users vu WHERE  id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function send_smt_mail($subject,$email_msg,$email,$email_name,$cc = "",$cc_name = "",$outside_receiver = 0){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('notifications@edupristine.com', 'Daily Receivables Report');
        
        $mail->addAddress($email, $email_name);
		if($cc != "")
		{
			$mail->addCC($cc, $cc_name);
		}
		$mail->addCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
        $mail->addCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
        $mail->addCC('finance@edupristine.com', 'Finance');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}