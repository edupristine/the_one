<?php
/* Template Name: Workshops Date Status
 * Description: Changed the workshop status from upcoming to conducted
 * Version: 1.0
 * Created On: 26th September, 2019
 * Created By: Ankur
 **/
// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/connection.php';

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/connection.php';

include_once("dbconfig.php");
$conn=mysqli_connect($db_server,$db_username,$db_password,'ecademe_harry_new');

$yesterday = date("Y-m-d", strtotime("-1 day"));

$query = "UPDATE workshops_dates SET status = 'Conducted' WHERE status = 'Upcoming' AND ws_date = '".$yesterday."'";
        
    echo mysqli_query($conn,$query);