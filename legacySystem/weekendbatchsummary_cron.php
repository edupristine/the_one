<?php
//error_reporting(0);
/* Template Name: Weekend Batches Summary Cron
 * Description: Weekend Starting Batches for one month ahead with no. of students registered .
 * Version: 1.0
 * Created On: 3st December, 2019
 * Created By: Hitesh
 **/
/*include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail_weekendbatches.php';*/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/mail_weekendbatches.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
//$mysql_user='localuser';
//$mysql_pass='R@5gull@';
setlocale(LC_MONETARY, 'en_IN');
$conn2=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$connone=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'ecademe_harry_new');


$sdate = date( 'Y-m-d', strtotime( 'next Saturday' ));
$edate = date('Y-m-d', strtotime($sdate. ' + 6 days'));


$dispsdate = date('d M (D)', strtotime( 'next Saturday' ));
$dispmdate = date('d M (D)', strtotime($sdate. ' + 1 days'));
$dispedate = date('d M (D)', strtotime($sdate. ' + 2 days'));


	$query1 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.estimated_start_date as `rawdate`,cm.course_module as `course_module`,l.loc_name as `city` ,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c,
	course_modules cm,locations l WHERE b.course_id = c.id  AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  
	AND cm.id=b.course_mod_id AND l.id=b.loc_id
	ORDER BY b.estimated_start_date ASC";

	$result1 = mysqli_query($connone,$query1);
	$num_rows1 = mysqli_num_rows($result1);

	if($num_rows1 != 0) 
	{
	while($r1 = mysqli_fetch_assoc($result1)) {
	$batch_codes[] = $r1;
	}
	}
         //<p>Please find below the tentative batch launch summary for ".$dispsdate.", ".$dispmdate." and ".$dispedate." </p>
		$html="";
		$html .= "
	    <p>Please find below the tentative batch launch summary for Coming weekend + Next Weekdays </p>
		<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:5%;'><b>Sr. No</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Batch Name</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Start Date</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>On Day</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:15%;'><b>Time</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>City</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Batch Type</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Course</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Sub Course</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Registered Students</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Attending Students</b></td>
		</tr>";
				
       
		if($num_rows1 == 0) 
        {
		$html .= "<tr>
		<td colspan=11 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows1 != 0)  
		{
		$i=1;	
		foreach($batch_codes as $batches)
		{
		$querycrm1="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches['batch']."' and deleteFlag = 0"; 	
		
		$resultcrm1 = mysqli_query($conn1,$querycrm1);	
		$re1 = mysqli_fetch_assoc($resultcrm1);	
		
		

		$unixTimestamp = strtotime($batches['rawdate']);
		$day = date('l', $unixTimestamp);
		$dateF1 = $day;
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:5%;'><b>
		".$i."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'><b>
		".$batches['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>
		".$batches['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$dateF1."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches['starttime']."-".$batches['endtime']."</td>
		
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches['city']."	</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches['batch_type']."	</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches['vertical']."	</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches['course_module']."	</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>
		".$re1['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>
		".$re1['attending_count']."</td>
		</tr>";
		$i++;
		}
		}
		
		
		
		$html .= "</table>";
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the Faculty Team.</p>
		<p><strong>Note</strong>: This is a tentative batch plan for coming weekend + next weekdays. You will receive the final batch launch summary by Wednesday EOD or Thursday morning.  
		</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		//<p><strong>Note</strong>: This is a tentative batch plan for coming SAT, SUN & Mon. You will receive the final batch launch summary by Wednesday EOD or Thursday morning.  
		//</p>
		
		
		//$subject = "  Tentative Batch Summary for Planning Purpose -  ".$dispsdate.", ".$dispmdate." and ".$dispedate." ";
		$subject = "  Tentative Batch Summary for Planning Purpose for Coming weekend + Next Weekdays ";
		send_smt_mail($subject,$html);
		
		

?>