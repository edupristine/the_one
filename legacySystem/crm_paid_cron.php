<?php
/* Template Name: CRM Paid Transactions Cron
 * Description: Transactions Entered in CRM.
 * Version: 1.0
 * Created On: 22nd March, 2021
 * Created By: Hitesh
 **/

include_once("dbconfig.php");
setlocale(LC_MONETARY, 'en_IN');
$db_conn=mysqli_connect($db_server,$db_username,$db_password,'vtigercrm6');


$first_day = date('Y-m-01');
$today = date("Y-m-d");


function query_to_csv($db_conn, $query, $filename, $attachment = false, $headers = true) {

    if($attachment) {
        // send response headers to the browser
        header('Content-Type: text/csv; charset=utf-8');
        header( 'Content-Disposition: attachment;filename='.$filename);
        $fp = fopen('php://output', 'w');
    } else {
        $fp = fopen($filename, 'w');
    }

    $result = mysqli_query( $db_conn, $query);

    if($headers) {
        // output header row (if at least one row exists)
        $row = mysqli_fetch_assoc($result);
        if($row) {
            fputcsv($fp, array_keys($row));
            // reset pointer back to beginning
            mysqli_data_seek($result, 0);
        }
    }

    while($row = mysqli_fetch_assoc($result)) {
        fputcsv($fp, $row);
    }

    fclose($fp);
	echo "file generated";
}

function send_csv_file($csv_file,$contents){

    include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP 
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients

		//$mail->setFrom('notifications@edupristine.com', 'Receivables Report');
		$mail->setFrom('notifications@inmail.edupristine.com', 'Transactions in CRM Report');
		
        
        $mail->addAddress('akshata.shenoy@edupristine.com','Akshata Shenoy');
		$mail->addCC('aamirt.khan@edupristine.com','Aamir Khan');
		$mail->addCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
		$mail->addCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		//Content
		// $mail->isHTML(true);			             // Set email format to HTML
		// $mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = 'CRM Transactions from Current Month to '.$today .'';
		$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
		$mail->MsgHTML(file_get_contents($contents));
		$mail->AddAttachment($csv_file);      // attachment
		//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
		$mail->send();
		echo "mail sent";
        exit;
} catch (Exception $e) {
  $mail->ErrorInfo;
}

}

## define your query
$query = "select paiddate as `paiddated`,trans_amt,mode,trans_id,org_no,name,Assigned_to,course,city,trans_column,acc_id,asgn_councel FROM ( SELECT DATE(sc.cf_1363) as paiddate,sc.cf_1303 as `trans_amt`,sc.cf_1305 as `mode`,cf_1307 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1307' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1363) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 UNION SELECT DATE(sc.cf_1365) as paiddate ,sc.cf_1313 as `trans_amt`,sc.cf_1315 as `mode` ,cf_1317 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1317' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1365) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 UNION SELECT DATE(sc.cf_1367) as paiddate,sc.cf_1323 as `trans_amt`,sc.cf_1325 as `mode` ,cf_1327 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1327' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1367) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 UNION SELECT DATE(sc.cf_1369) as paiddate,sc.cf_1333 as `trans_amt`, sc.cf_1335 as `mode`,cf_1337 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1337' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1369) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 UNION SELECT DATE(sc.cf_1371) as paiddate,sc.cf_1343 as `trans_amt`, sc.cf_1345 as `mode` ,cf_1347 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1347' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1371) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 UNION SELECT DATE(sc.cf_1373) as paiddate,sc.cf_1353 as `trans_amt`, sc.cf_1355 as `mode` ,cf_1357 as `trans_id`,ac.account_no as `org_no`,ac.accountname as `name`,sc.cf_918 as `course`,sc.cf_1629 AS `city`,CONCAT(vu.first_name, ' ', vu.last_name) AS 'Assigned_to','cf_1357' as 'trans_column',ac.accountid as `acc_id`, ce.smownerid as `asgn_councel` FROM vtiger_account ac LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid INNER JOIN vtiger_users vu ON vu.id = ce.smownerid WHERE DATE(sc.cf_1373) BETWEEN '$first_day' AND '$today' AND ce.smownerid != 3920 ) x order by paiddated";

#define the file name for the csv file and the html file contents.
$csv_file = 'Transactions_data_'.$first_day.'_to_'.$today.'_.csv';
$contents = 'content.html';

## execute the csv function
query_to_csv($db_conn, $query, $csv_file, false);

## if you want the csv file to be attached to the browser set false to true.
if(file_exists($csv_file) && (file_exists($contents))){

## execute
send_csv_file($csv_file,$contents);

}



