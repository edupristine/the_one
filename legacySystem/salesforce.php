<?php
function salesforce_login(){

    $params = "grant_type=password&client_id=".SF_CLIENT_ID."&client_secret=".SF_CLIENT_SECRET."&username=".SF_USERNAME."&password=".SF_PASSWORD.SF_SECURITY_TOKEN;
    $headers = array("application/x-www-form-urlencoded");

    $ch = curl_init() or die(curl_error()); 
	curl_setopt($ch, CURLOPT_POST,1);
	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
	curl_setopt ( $ch, CURLOPT_ENCODING, 'UTF-8' );
	curl_setopt($ch, CURLOPT_POSTFIELDS,$params); 
	curl_setopt($ch, CURLOPT_URL,SF_LOGIN_URI);// here the request is sent to Salesforce auth url
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	$data1=curl_exec($ch) or die(curl_error());
	curl_close($ch);
    
    $response = json_decode($data1,TRUE);
    file_put_contents(SF_TOKEN_FILE, $data1, LOCK_EX);   
}

function salesforce_getall_lead($date){
    $file_content = file_get_contents(SF_TOKEN_FILE);
    if(!file_exists(SF_TOKEN_FILE) || $file_content == '{"error":"invalid_grant","error_description":"authentication failure"}') {
        salesforce_login();
    }

    $auth_details = json_decode(file_get_contents(SF_TOKEN_FILE), TRUE);
  
    
    $ch = curl_init() or die(curl_error()); 
	curl_setopt($ch, CURLOPT_POST,1);
	// curl_setopt ( $ch, CURLOPT_ENCODING, 'UTF-8' );
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");  
	//curl_setopt($ch, CURLOPT_POSTFIELDS,$param_string); 
	curl_setopt($ch, CURLOPT_URL,$auth_details['instance_url'].SF_LEAD_GET_URI."?fromDate=$date+12:00+AM&toDate=$date+11:59+PM");// here the request is sent to Salesforce lead url
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
        'Accept: application/json',                                                                                
        'Authorization: Bearer '.$auth_details['access_token']  )                                                                   
    );    
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	$data1=curl_exec($ch);
	//print_r($data1);
    $http_status = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
	curl_close($ch);
    // if ($http_status >= 400 && $count < 3) {
        // salesforce_login();
    // } else{
        return $data1;
    //}
    

}

