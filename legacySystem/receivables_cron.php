<?php
/* Template Name: Receivable Cron
 * Description: Reminder mail to students for payment before due date.
 * Version: 1.0
 * Created On: 15th September, 2019
 * Created By: Ankur
 **/
// include '../inc/GenericFunctions.php';
// include '../control/core.php';
// include '../control/checklogin.php';
// include '../control/connection.php';

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';

include_once("dbconfig.php");
setlocale(LC_MONETARY, 'en_IN');
$conn1=mysqli_connect($db_server,$db_username,$db_password,'vtigercrm6');
$conn2=mysqli_connect($db_server,$db_username,$db_password,'edupristine_one');
$conn3=mysqli_connect($db_server,$db_username,$db_password,'ecademe_harry_new');
// $conn4=mysqli_connect('13.126.164.124',$db_username,$db_password,'wp_tetris');

$today = date("Y-m-d");

$query = "SELECT code FROM workshops WHERE `batch_status` = 'In Progress'";

$result = mysqli_query($conn3,$query);

while($re = mysqli_fetch_assoc($result)) {
    $codes[] = $re['code'];
}
$students_cal = array();
$students_all = array();
$leftStudents_9_0 = array();
$other_students = array();
$t_10 = $t_5 = $t_1 = array();
$finalArray = array();
$queryLeftstudents = "SELECT crm_id,`next_installment`,`next_inst_due_date` FROM students WHERE `next_inst_due_date` <= DATE_ADD(CURRENT_DATE(), INTERVAL 10 DAY) AND next_installment != 0";
$resultLeft = mysqli_query($conn2,$queryLeftstudents);
$leftStudentAcc = array();
$leftStudents = array();
while($re = mysqli_fetch_assoc($resultLeft)) {
    $leftStudents[] = $re;
    $leftStudentAcc[] = $re['crm_id'];
}
foreach($codes as $code){
	$student_list_as_per_one = "'";
	$qry_students_in_module = "SELECT s.crm_id from students_course_modules scm,students s where scm.student_id = s.id and scm.batch_attended = '".$code."'";
	$result_in_module = mysqli_query($conn2,$qry_students_in_module);
	while($res_stu_in_mod = mysqli_fetch_assoc($result_in_module)) {
		$student_list_as_per_one = $student_list_as_per_one.$res_stu_in_mod['crm_id']."','";
	}
	if($student_list_as_per_one != "'")
		$student_list_as_per_one = substr($student_list_as_per_one,0,-2);
	else
		$student_list_as_per_one = "'0'";
	
	$sql = "SELECT ac.account_no,ac.accountname,ac.email1 as `email`, ac.phone,sc.cf_918 as `course`,
        sc.cf_1495 as `reg_batch`, sc.cf_936 as `offered_price`, sc.cf_938 as `paid`, sc.cf_1497 as `at1`,sc.cf_1499 as `at2`,
        sc.cf_1501 as `at3`, sc.cf_1503 as `at4`, sc.cf_1505 as `at5`, ce.smownerid as `salesperson`, sc.cf_1629 as `center`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE DATE(ce.createdtime) > '2019-06-01' AND sc.cf_936 > sc.cf_938 AND ce.smownerid != 3920 AND
        ((cf_1495 = '".$code."' OR
	    cf_1497 = '".$code."' OR
	    cf_1499 = '".$code."' OR
	    cf_1501 = '".$code."' OR
	    cf_1503 = '".$code."' OR
        cf_1505 = '".$code."') OR ac.account_no IN (".$student_list_as_per_one."))";
    $result1 = mysqli_query($conn1,$sql);
    $students = array();
    while($res = mysqli_fetch_assoc($result1)) {
        $students[] = $res;
    }
	
    foreach($students as $student){
		
        if(!(in_array($student['account_no'],$students_cal))){
            
			
		
        $batch_codes = array($student['reg_batch'],$student['at1'],$student['at2'],$student['at3'],$student['at4'],$student['at5']);
		
		$query_stu_coursemodules = "select batch_attended from students_course_modules where student_id = (SELECT id from students where crm_id = '".$student['account_no']."')";
		$result_stu_coursemodules = mysqli_query($conn2,$query_stu_coursemodules);
		$i = 6;
		while($res_stu_cm = mysqli_fetch_assoc($result_stu_coursemodules)) {
			if($res_stu_cm['batch_attended'] != 0 && $res_stu_cm['batch_attended'] != '')
			{
				$batch_codes[$i] = $res_stu_cm['batch_attended'];
				$i++;
			}
		}
		
		
        $batch_codes = array_unique($batch_codes);
        $batch_codes = array_filter($batch_codes);
		
        $locQuery = "SELECT city FROM workshops WHERE code='".$student['reg_batch']."'";
        $locresult = mysqli_query($conn3,$locQuery);
        $locres = mysqli_fetch_assoc($locresult);
        if (($key = array_search("Online", $batch_codes)) !== false) {
            unset($batch_codes[$key]);
        }
        if($student['center'] == ""){
            if($locres['city'] == 'Andheri')
            $student['center'] = 'Mumbai';
            else
            $student['center'] = $locres['city'];
        }
		if($student['center'] == "Online" || $student['center'] == "LVC" || $student['center'] == "LVC / Self Study")
		{
			$student['center'] = "LVC";
		}
		
        $total_sessions = $conducted = 0;
        $org_codes = "'";
        foreach($batch_codes as $codes){
            if($codes != 'Online' && $codes != 'Code Unavailable'){
            
            // $cqry = "SELECT no_of_sessions FROM schedule_templates WHERE batch_type = '".$bres['batch_type']."'
            //  AND template_name = '".$bres['course_module']."'";
            //  $cresult = mysqli_query($conn2,$cqry);
            //  $cres = mysqli_fetch_assoc($cresult);
             
            $dqry = "SELECT COUNT(*) as `conducted` FROM workshops_dates WHERE batch_code = '".$codes."' AND status = 'Conducted'";
            $dresult = mysqli_query($conn3,$dqry);
            $dres = mysqli_fetch_assoc($dresult);
            $conducted += $dres['conducted'];
            
            
            $org_codes .= $codes."', '";
			}
		}   
        $total_sessions = total_number_of_session($student['account_no'],$student['reg_batch'],$conn2,$conn3);
		
        $org_codes = substr($org_codes,0,-3);
        $student['batch'] = $org_codes;
        if($total_sessions !=0 ){
            $conducted_percent = round(($conducted/$total_sessions)*100,0);
        }else{
            $conducted_percent = 0;
        }
        $paid_percent = round(($student['paid']/$student['offered_price'])*100,0);
        $next_session="";
            $orgQry = "SELECT MIN(ws_date) as`ws_date` FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."'";
            $orgResult = mysqli_query($conn3,$orgQry);
            $orgRes = mysqli_fetch_assoc($orgResult);
        if($paid_percent < 100){
        $student_data = $student['account_no'].",".$student['email'].",".$total_sessions.",".$paid_percent;
        array_push($students_all,$student_data);
        }
        array_push($students_cal,$student['account_no']);
        $tDate ="";
        if($total_sessions/4 > $conducted && $paid_percent < 50){
            $remaining_session = ceil($total_sessions/4 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
            $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
        }elseif($total_sessions/2 > $conducted && $paid_percent < 75){
            $remaining_session = ceil($total_sessions/2 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
            $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
        }elseif($total_sessions*0.75 > $conducted && $paid_percent < 100){
            $remaining_session = ceil($total_sessions*0.75 - $conducted);
            $remaining_session -= 1;
            $sQry = "SELECT ws_date FROM workshops_dates WHERE batch_code IN (".$org_codes.") AND ws_date >= '".$today."' ORDER BY ws_date LIMIT 1 OFFSET ".$remaining_session;
            $student['session_percentage'] = $conducted_percent;
            $sResult = mysqli_query($conn3,$sQry);
            $sRes = mysqli_fetch_assoc($sResult);
            $tDate = $sRes['ws_date'];
            $student['due_date'] = $tDate;
        }elseif($conducted_percent > $paid_percent){
            $tDate = date("Y-m-d");
            $student['due_date'] = $tDate;
            $student['session_percentage'] = $conducted_percent;
        }
        $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
        $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
        $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $next_installment = 0;
		
		$sessions_till_due_date = "select count(id) as UpcomingTillDue from workshops_dates where batch_code = '".$codes."' AND ws_date <= '".$tDate."' and status = 'Upcoming'";
		$result_sessions_till_due_date = mysqli_query($conn3,$sessions_till_due_date);
		$dres_sess_til_due = mysqli_fetch_assoc($result_sessions_till_due_date);
		$upcomingtilldue = $dres_sess_til_due['UpcomingTillDue'];
		if($total_sessions == 0)
			$total_sessions = 1;
		$new_conducted_check = $conducted_percent + ($upcomingtilldue * 100/$total_sessions);
		$new_conducted_check = round($new_conducted_check,0);
		
		
		
        if($new_conducted_check < 25 && $tDate!="" && $paid_percent <50){
            $next_installment = $balance_50;
        }elseif($new_conducted_check >= 25 && $new_conducted_check <50 && $paid_percent <50){
            $next_installment = $balance_50;
            }elseif($new_conducted_check >= 50 && $new_conducted_check <75 && $paid_percent <75){
                $next_installment = $balance_75;
            }elseif($new_conducted_check >= 75 && $new_conducted_check <100){
                $next_installment = $balance;
            }
			
			
        $student['amt_to_reach'] = $next_installment + $student['paid'];
        $student['next_installment'] = $next_installment;
		
		//echo $student['account_no']." - ".$new_conducted_check." - ".$student['session_percentage']." - ".$paid_percent."\n";
		
		array_push($other_students,$student);
		//continue;
		
        if($tDate != ""){
        $t = new DateTime();
        $tD = new DateTime($tDate);
        $day_diff = $tD->diff($t);
        $days_left = ($day_diff->days)+1;
        if($days_left == 10){
            array_push($t_10,$student);
            array_push($finalArray,$student);
        }elseif($days_left == 5){
            array_push($t_5,$student);
            array_push($finalArray,$student);
        }elseif($days_left == 1 && ($tDate != date('Y-m-d'))){
            array_push($t_1,$student);
            array_push($finalArray,$student);
        }elseif($days_left < 11){
            array_push($finalArray,$student);
        }
		if($next_installment != 0 && $days_left < 11)
		{
			stamp_due_details($student['account_no'],$tDate,$next_installment,$student['amt_to_reach'],$conn2);
        }
		if(in_array($student['account_no'],$leftStudentAcc)){
            $key = array_search($student['account_no'], array_column($leftStudents, 'crm_id'));
            $student['due_date'] = $leftStudents[$key]['next_inst_due_date'];
            $student['next_installment']= $leftStudents[$key]['next_installment'];
            array_push($leftStudents_9_0,$student);
            array_push($finalArray,$student);
        }
		
    }
        
        // echo $tDate." ".$orgRes['ws_date']." ".$student['account_no']." ".$total_sessions." ".$conducted." ".$conducted_percent." ".$paid_percent."\n";
        // echo $days_left."\n"; 
    }
    }
}
/*
echo "1\n";
print_r($t_1);
echo "\n5\n";
print_r($t_5);
echo "\n10\n";
print_r($t_10);
exit;
//print_r($other_students);
//exit;
*/
/*

MAIL ALL Students 

if(count($other_students) > 0){
    $html = "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Batch Codes</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Center</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Salesperson</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Offered price</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Paid</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Balance</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Next Installment</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Due Date</b></td>";
    $html .= "</tr>";
    $students_in_table = array();
foreach($other_students as $student){
    if(!in_array($student['account_no'],$students_in_table)){
    $salesperson = get_sales_person($student['salesperson'], $conn1);
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['batch']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$salesperson['name']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['offered_price']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['paid']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        if($student['session_percentage'] < 25){
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_50."</td>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_75."</td>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
			}
			else
			{
				
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>NA</td>";
			}
		}
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$student['due_date']."</td></tr>";
        array_push($students_in_table,$student['account_no']);
    }
}
    $subject = "Receivables - Updated Student List";
    $html .= "</table></br>";
    $html .= "<p>Thanks & Regards</p>";
    $html .= "<p>Edupristine IT Team</p>";
	send_smt_mail($subject,$html,$student['email']);
	echo "Email Sent \n";
	exit;
}

*/

$finalArray_center = multi_sort($finalArray, "center");
$finalArray_smownerid = multi_sort($finalArray, "salesperson");


//Preparing array Center Wise
$final_arr_center = array();
$student_arr = array();
$ct = 1;
$locationTmp="";
foreach($finalArray_center as $data){
    if($ct == 1){
        $locationTmp = $data['center'];
        $ct = 0;
    }
    if($data['center'] == $locationTmp){
        array_push($student_arr, $data);
    }else{
        $temp_arr = array('city'=>$locationTmp,
                        'students'=>$student_arr);
        array_push($final_arr_center, $temp_arr);
        $student_arr = array();
        $locationTmp = $data['center'];
        array_push($student_arr, $data);
    }
}
if(count($student_arr) != 0){
$temp_arr = array('city'=>$locationTmp,
                'students'=>$student_arr);
array_push($final_arr_center, $temp_arr);
}


//Preparing array Salesperson Wise
$final_arr_sales = array();
$student_arr = array();
$ct = 1;
$salesTmp="";
foreach($finalArray_smownerid as $data){
    if($ct == 1){
        $salesTmp = $data['salesperson'];
        $ct = 0;
    }
    if($data['salesperson'] == $salesTmp){
        array_push($student_arr, $data);
    }else{
        $temp_arr = array('salesperson'=>$salesTmp,
                        'students'=>$student_arr);
        array_push($final_arr_sales, $temp_arr);
        $student_arr = array();
        $salesTmp = $data['salesperson'];
        array_push($student_arr, $data);
    }
}
if(count($student_arr) != 0){
$temp_arr = array('salesperson'=>$salesTmp,
                'students'=>$student_arr);
array_push($final_arr_sales, $temp_arr);
}

$students_support = array();
$students_support = multi_sort($finalArray, "due_date");
if(count($students_support) > 0){
	$html = "<p>Below is the list of students for whom payment has to be collected on or before the corresponding due date.</p>";
	$html .= "<p>Do assist the students in case they call or message of any queries.</p>";
    $html .= "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Batch Codes</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Center</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Salesperson</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Offered price</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Paid</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Balance</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Next Installment</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Due Date</b></td>";
    $html .= "</tr>";
    $students_in_table = array();
foreach($students_support as $student){
    if(!in_array($student['account_no'],$students_in_table)){
    $salesperson = get_sales_person($student['salesperson'], $conn1);
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['batch']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$salesperson['name']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['offered_price']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['paid']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        if($student['session_percentage'] < 25){
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_50."</td>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_75."</td>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
				}
				else
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>NA</td>";
				}
			}
		}
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$student['due_date']."</td></tr>";
        array_push($students_in_table,$student['account_no']);
    }
}
    $subject = "List of all Receivables - For Support Team's Reference";
    $html .= "</table></br>";
    $html .= "<p>Thanks & Regards</p>";
    $html .= "<p>Edupristine IT Team</p>";
	send_smt_mail($subject,$html,"team.support@edupristine.com","Support Team","rahul.karmarkar@edupristine.com","Rahul Karmarkar");
	
}

foreach($t_10 as $student){
    $url = 'http://one.edupristine.com/rec_concern.php?account_no='.$student['account_no'].'&course='.$student['course'].'&city='.$student['center'].'&name='.$student['accountname'].'&email='.$student['email'].'&phone='.$student['phone'];
    $due_date = date("jS M, Y", strtotime($student['due_date']));
    $html = "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
          <td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ".$student['accountname'].",</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>This is to inform you that your next installment is due and last date of payment is ".$due_date.".</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Please find below the details of your registration with EduPristine:</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Student ID:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['course']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>City:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course Price:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['offered_price']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Paid Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['paid']."</td>
    </tr>";
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
    if($student['session_percentage'] < 25){
        $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_50."</td>
    </tr>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_75."</td>
    </tr>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
    </tr>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
				
				$html .= "<tr>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>
				</tr>";
				
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
					</tr>";
				}
				else
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>NA</td>
					</tr>";
				}
			}
		}
    
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Due Date Of Next Installment:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$due_date."</td>
    </tr>";
    /*$html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>To pay this amount <a href=\"https://edupristine.com\">Click Here</a></td>
    </tr>";*/
    $html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In any case, we request you to pay the abovementioned amount by ".$due_date.", in the absence of which we may not be able to allow you to attend the classes for your chosen Program from ".$due_date." and discontinue your access to the Learning Management System.</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In case of any concerns <a href=\"".$url."\">Click Here.</a></td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Thank you,<br>Team EduPristine</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 12px;'>Please ignore if the amount is already paid.</td>
    </tr>
    </table>";
    $subject = "EduPristine - Reminder For Outstanding Payment";
    send_smt_mail($subject,$html,$student['email'],$student['accountname'],"","",1);
    
}

foreach($t_5 as $student){
    $url = 'http://one.edupristine.com/rec_concern.php?account_no='.$student['account_no'].'&course='.$student['course'].'&city='.$student['center'].'&name='.$student['accountname'].'&email='.$student['email'].'&phone='.$student['phone'];
    $due_date = date("jS M, Y", strtotime($student['due_date']));
    $html = "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
          <td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ".$student['accountname'].",</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>This is to inform you that your next installment is due and last date of payment is ".$due_date.".</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Please find below the details of your registration with EduPristine:</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Student ID:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['course']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>City:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course Price:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['offered_price']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Paid Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['paid']."</td>
    </tr>";
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
    if($student['session_percentage'] < 25){
        $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_50."</td>
    </tr>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_75."</td>
    </tr>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
    </tr>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
				
				$html .= "<tr>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>
				</tr>";
				
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
					</tr>";
				}
				else
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>NA</td>
					</tr>";
				}
			}
		}
    
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Due Date Of Next Installment:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$due_date."</td>
    </tr>";
    /*$html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>To pay this amount <a href='#'>Click Here</a></td>
    </tr>";*/
    $html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In any case, we request you to pay the abovementioned amount by ".$due_date.", in the absence of which we may not be able to allow you to attend the classes for your chosen Program from ".$due_date." and discontinue your access to the Learning Management System.</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In case of any concerns <a href=\"".$url."\">Click Here.</a></td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Thank you,<br>Team EduPristine</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 12px;'>Please ignore if the amount is already paid.</td>
    </tr>
    </table>";
    $subject = "EduPristine - Reminder For Outstanding Payment";
    send_smt_mail($subject,$html,$student['email'],$student['accountname'],"","",1);
    
}

foreach($t_1 as $student){
    $url = 'http://one.edupristine.com/rec_concern.php?account_no='.$student['account_no'].'&course='.$student['course'].'&city='.$student['center'].'&name='.$student['accountname'].'&email='.$student['email'].'&phone='.$student['phone'];
    $due_date = date("jS M, Y", strtotime($student['due_date']));
    $html = "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
          <td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ".$student['accountname'].",</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>This is to inform you that your next installment is due and last date of payment is ".$due_date.".</td>
    </tr>
    <tr>
       <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Please find below the details of your registration with EduPristine:</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Student ID:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['course']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>City:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Course Price:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['offered_price']."</td>
    </tr>";
    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Paid Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['paid']."</td>
    </tr>";
    $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
    if($student['session_percentage'] < 25){
        $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_50."</td>
    </tr>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance_75."</td>
    </tr>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
    </tr>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
				
				$html .= "<tr>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
				<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>
				</tr>";
				
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$balance."</td>
					</tr>";
				}
				else
				{
					$html .= "<tr>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Next Installment Amount:</td>
					<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>NA</td>
					</tr>";
				}
			}
		}

    $html .= "<tr>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Due Date Of Next Installment:</td>
    <td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$due_date."</td>
    </tr>";
    /*$html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>To pay this amount <a href='#'>Click Here</a></td>
    </tr>";*/
    $html .= "<tr>
    <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In any case, we request you to pay the abovementioned amount by ".$due_date.", in the absence of which we may not be able to allow you to attend the classes for your chosen Program from ".$due_date." and discontinue your access to the Learning Management System.</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>In case of any concerns <a href=\"".$url."\">Click Here.</a></td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Thank you,<br>Team EduPristine</td>
    </tr>
    <tr>
        <td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 12px;'>Please ignore if the amount is already paid.</td>
    </tr>
    </table>";
    $subject = "EduPristine - Reminder For Outstanding Payment";
    send_smt_mail($subject,$html,$student['email'],$student['accountname'],"","",1);
    
}


foreach($final_arr_center as $stud){
	
    $html = "<p>".$stud['city']." Students to be blocked After 10 Days..</p>";
	$html = "<p>Below is the list of students of ".$stud['city']." for whom payment has to be collected on or before the corresponding due date.</p>";
	$html .= "<p>Please ensure that these students are not allowed to attend post the due date.</p>";
    $html .= "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Batch Codes</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Next Installment</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Due Date</b></td>";
    $html .= "</tr>";
    $students_in_table = array();
    $student_center_array = array();
    $student_center_array = multi_sort($stud['students'], "due_date");
    foreach($student_center_array as $student){
        if(!in_array($student['account_no'],$students_in_table)){
        $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
    $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
    $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['batch']."</td>";
        if($student['session_percentage'] < 25){
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_50."</td>";
        }elseif($student['session_percentage'] < 50){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_75."</td>";
        }elseif($student['session_percentage'] < 75){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        }
		else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
				}
				else
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>NA</td>";
				}
			}
		}
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$student['due_date']."</td></tr>";
    }
    array_push($students_in_table,$student['account_no']);
}
    $center_team = get_center_team($stud['city'],$conn2);
	$html .= "</table></br>";
	$cc = "";
	$cc_name = "";
	if($stud['city'] == "LVC")
	{
		$center_team['user_name'] = "Support Team";
		$center_team['user_email'] = "team.support@edupristine.com";
		$cc = "team.it@edupristine.com";
		$cc_name = "IT Team";
	}
	
	//$html .= "<p>Center : ".$center_team['user_name']."</p><br>";
	//$html .= "<p>Center : ".$center_team['user_email']."</p><br>";
	
    $html .= "<p>Thanks & Regards</p>";
    $html .= "<p>Edupristine IT Team</p>";
    $subject = "List of Receivables For Your Center";
    
    send_smt_mail($subject,$html,$center_team['user_email'],$center_team['user_name'],$cc,$cc_name);
}


foreach($final_arr_sales as $stud){
    $html = "<p>Your following Student(s) will not be allowed to attend sessions 10 days from now.</p>";
    $html = "<p>Below is the list of students assigned to you for whom payment has to be collected on or before the corresponding due date.</p>";
	$html .= "<table style='border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Org. no.</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Name</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Email</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Student Phone</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:left;'><b>Batch Codes</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Offered price</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Paid</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Balance</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Next Installment</b></td>";
    $html .= "<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center;'><b>Due Date</b></td>";
    $html .= "</tr>";
    $students_in_table = array();
    $student_sales_array = array();
    $student_sales_array = multi_sort($stud['students'], "due_date");
    foreach($student_sales_array as $student){
        if(!in_array($student['account_no'],$students_in_table)){
        $balance = number_format(($student['offered_price']-$student['paid']), 2, '.', '');
        $balance_50 = number_format((round($student['offered_price']*0.50,0))-$student['paid'], 2, '.', '');
        $balance_75 = number_format((round($student['offered_price']*0.75,0))-$student['paid'], 2, '.', '');
        $html .= "<tr><td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['email']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['phone']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['batch']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['offered_price']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$student['paid']."</td>";
        $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
        if($student['session_percentage'] < 25){
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_50."</td>";
            }elseif($student['session_percentage'] < 50){
                $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance_75."</td>";
            }elseif($student['session_percentage'] < 75){
                $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
            }
			else
		{
			$query_next_inst_stamp = "select next_installment from students where crm_id = '".$student['account_no']."'";
			$result_next_inst_stamp = mysqli_query($conn2,$query_next_inst_stamp);
			$res_next_inst_stamp = mysqli_fetch_assoc($result_next_inst_stamp);
			
			//echo $query_next_inst_stamp."\n";
			
			if($res_next_inst_stamp['next_installment'] != 0)
			{
				$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$res_next_inst_stamp['next_installment']."</td>";
			}
			else
			{
				if($student['session_percentage'] > 75)
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>Rs. ".$balance."</td>";
				}
				else
				{
					$html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>NA</td>";
				}
			}
		}
            $html .= "<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$student['due_date']."</td></tr>";
    }
    array_push($students_in_table,$student['account_no']);
    }
	$emailstosend = get_sales_person_reporting($stud['salesperson'], $conn1);
    $salesperson = get_sales_person($stud['salesperson'], $conn1);
    $html .= "</table></br>";
    
	//$html .= "<p>Salesperson : ".$emailstosend['name']."</p><br>";
	//$html .= "<p>Salesperson : ".c."</p><br>";
	//$html .= "<p>Salesperson : ".$emailstosend['reporting_manager']."</p><br>";
	//$html .= "<p>Salesperson : ".$emailstosend['reporting_manager_email']."</p><br>";
	
    $html .= "<p>Thanks & Regards</p>";
    $html .= "<p>Edupristine IT Team</p>";
    $subject = "List of Receivables for your Name";
	
    send_smt_mail($subject,$html,$emailstosend['email'],$emailstosend['name'],$emailstosend['reporting_manager_email'],$emailstosend['reporting_manager']);
	
}


function multi_sort($array, $key) {
    $sorter=array();
    $ret=array();
    reset($array);
    foreach ($array as $ii => $va) {
        $sorter[$ii]=$va[$key];
    }
    asort($sorter);
    foreach ($sorter as $ii => $va) {
        $ret[$ii]=$array[$ii];
    }
    return $ret;
}

function get_sales_person($id,$conn){
    $qry = "SELECT CONCAT(first_name,' ',last_name) as `name`,email1 as `email` FROM vtiger_users WHERE id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_sales_person_reporting($id,$conn){
    $qry = "SELECT CONCAT(vu.first_name,' ',vu.last_name) as `name`,vu.email1 as `email`,(SELECT CONCAT(first_name,' ',last_name) as `name` FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager`,(SELECT  email1 FROM vtiger_users  WHERE id=vu.reports_to_id) AS `reporting_manager_email` FROM vtiger_users vu WHERE  id=".$id;
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function get_center_team($city, $conn){
	if($city == "Thane")
	{
		$city = "Mumbai";
	}
    $qry = "SELECT user_name,user_email FROM users u
    LEFT JOIN users_teams ut ON ut.user_id = u.id
    LEFT JOIN teams t ON t.id = ut.team_id
    WHERE ut.team_id = 4 AND u.loc_id = (SELECT id FROM locations WHERE loc_name = '".$city."' AND loc_type = 'CENTER')";
    $result = mysqli_query($conn,$qry);
    $re = mysqli_fetch_assoc($result);
    return $re;
}

function total_number_of_session($org,$batch,$conn2,$conn3){
    $batchFromOne = "SELECT batch_type FROM batches WHERE batch_name='".$batch."'";
    $bOneresult = mysqli_query($conn2,$batchFromOne);
    if(mysqli_num_rows($bOneresult)){
        $b1res = mysqli_fetch_assoc($bOneresult);
        $batch_type = $b1res['batch_type'];
		if($batch_type == '1')
		{
			$batch_type = "Weekdays";
		}
		elseif($batch_type == '2')
		{
			$batch_type = "Weekend";
		}
		elseif($batch_type == '3')
		{
			$batch_type = "Saturday Only";
		}
		elseif($batch_type == '4')
		{
			$batch_type = "Sunday Only";
		}
    }else{
        $bqry = "SELECT batch_type FROM workshops WHERE code ='".$batch."'";
            $bresult = mysqli_query($conn3,$bqry);
            $bres = mysqli_fetch_assoc($bresult);
            if($bres['batch_type'] != NULL && $bres['batch_type'] != ""){
                $batch_type = $bres['batch_type'];
				if($batch_type == '1')
				{
					$batch_type = "Weekdays";
				}
				elseif($batch_type == '2')
				{
					$batch_type = "Weekend";
				}
				elseif($batch_type == '3')
				{
					$batch_type = "Saturday Only";
				}
				elseif($batch_type == '4')
				{
					$batch_type = "Sunday Only";
				}
            }else{
				$ws_dates = array();
                $qrywsd = "SELECT ws_date FROM workshops_dates WHERE batch_code='".$batch."' ORDER BY ws_date ASC LIMIT 3";
                $reswd = mysqli_query($conn3,$bqry);
                while($re = mysqli_fetch_assoc($reswd)) {
                    $ws_dates[] = $re['ws_date'];
                }
                    $d1 = new DateTime($ws_dates[0]);
                    $d2 = new DateTime($ws_dates[1]);
                    $d3 = new DateTime($ws_dates[2]);
                    $day_diff_d2d1 = $d2->diff($d1);
                    $day_diff_d3d2 = $d3->diff($d2);
                    $d2d1 = $day_diff_d2d1->days;
                    $d3d2 = $day_diff_d3d2->days;
                    if($d2d1 == 1 && $d3d2 == 1){
                        $batch_type = "Weekdays";
                    }elseif($d2d1 == $d3d2){
                        $day = date("l",strtotime($d1->date));
                        if($day == 'Saturday'){
                            $batch_type = "Saturday Only";
                        }else{
                            $batch_type = "Sunday Only";
                        }
                    }else{
                        $batch_type = "Weekend";
                    }

    }
}
        
    $qry = "SELECT SUM(st.no_of_sessions) as `no_of_session` FROM schedule_templates st
            LEFT JOIN course_modules cm ON cm.id = st.course_mod_id
            LEFT JOIN `students_course_modules` sc ON sc.course_module_id = st.course_mod_id 
            LEFT JOIN students s ON s.id = sc.`student_id`
            WHERE s.`crm_id` = '".$org."' AND st.batch_type='".$batch_type."'";
    $result = mysqli_query($conn2,$qry);
    if(mysqli_num_rows($result)){
    $re = mysqli_fetch_assoc($result);
    return $re['no_of_session'];
    }else{
        return 0;
    }
}

function stamp_due_details($org,$tDate,$next_installment,$amt_to_reach,$conn){
    $qry = "UPDATE students SET next_inst_due_date='".$tDate."', next_installment='".$next_installment."', amt_to_reach = '".$amt_to_reach."' WHERE crm_id = '".$org."'";
	mysqli_query($conn,$qry);
}

function create_payment_link($conn,$acc_no,$title,$course,$price){
    $query = "INSERT INTO `pg_products` (`title`, `description`, `product_type`, `course`, `price`, `enabled`, `type`, `currency`, `created_by`, `ecomm_flag`, `crm_course`, `crm_subcourse`,`acc_no`,`pending_payment_flag`) 
    VALUES ('".$title."', NULL, 'Classroom', '".$course."', ".$price.", 1, NULL, 'INR', 'admin', 0, '".$course."', NULL, '".$acc_no."', 1)";
    mysqli_query($conn,$query);
    $id = mysqli_insert_id($conn);
    return $id;
}

function send_smt_mail($subject,$email_msg,$email,$email_name,$cc = "",$cc_name = "",$outside_receiver = 0){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'notifications@edupristine.com';                 // SMTP username
		//$mail->Password = '9a213cb82189f48e359574dbdec144db-52b6835e-e7dfaae2'; // SMTP password
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		if($outside_receiver == 1)
		{
			//$mail->setFrom('notifications@edupristine.com', 'Team EduPristine');
		    $mail->setFrom('notifications@inmail.edupristine.com', 'Team EduPristine');
		}
		else
		{
			//$mail->setFrom('notifications@edupristine.com', 'Receivables Report');
			$mail->setFrom('notifications@inmail.edupristine.com', 'Receivables Report');
		}
        
        $mail->addAddress($email, $email_name);
		if($cc != "")
		{
			$mail->addCC($cc, $cc_name);
		}
		$mail->addCC('prajakta.walimbe@edupristine.com', 'Prajakta Walimbe');
        $mail->addCC('hitesh.patil@edupristine.com', 'Hitesh Patil');
		$mail->addCC('finance@edupristine.com', 'Aamir Khan');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
