<?php
/* Template Name: Creation of Student Biometric Automated system
 * Description: Creation of Student Biometric Automated system, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 17th September, 2019
 *
 **/
include '../inc/GenericFunctions.php';
include '../control/core.php'; 	
include '../control/checklogin.php';
include '../control/connection.php';
include_once("dbconfig.php");
include '../api/biometric_api.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
$yesterday = date("Y-m-d",strtotime("-2 days"));
$sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE ce.smownerid!=3920 AND  sc.cf_1495='818ANDBATFTTAL' OR sc.cf_1497='818ANDBATFTTAL' OR sc.cf_1499='818ANDBATFTTAL' OR sc.cf_1501='818ANDBATFTTAL' OR sc.cf_1503='818ANDBATFTTAL' OR sc.cf_1505='818ANDBATFTTAL' and sc.cf_1629 not like '%Thane%'
		
		";
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
    $query = "SELECT ac.email1,sc.cf_918,sc.cf_1513 FROM vtiger_account ac
    LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
    LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
    INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
    WHERE ac.email1 = '".$row['email']."' AND sc.cf_918 = '".$row['course']."' 
    AND sc.cf_1513 = '".$row['subcourse']."'";
    $locQuery = "SELECT id FROM locations where loc_name = '".$row['location']."'";
    $rloc = Select($locQuery,$conn);
    $locCount = $rloc['count'];
    if($locCount < 1){
        $row['location'] = 'Andheri';
    }
    $result1 = $conn1->query($query);
    $count = mysqli_num_rows($result1);
    if($count > 1){
        $checkQuery = "SELECT * FROM students where course_id =(SELECT id FROM courses where course_name = '".$row['course']."') AND
        subcourse_id = (SELECT id FROM sub_courses where subcourse_name = '".$row['subcourse']."') AND student_email = '".$row['email']."'";
        $checkResult = $conn2->query($checkQuery);
        $resultCount = mysqli_num_rows($checkResult);
        if($resultCount != 0){
            
			//As per Yogesh Sir Req.
			if($row['location']=='Mumbai')
			{
			echo $branchName='Andheri';
			}
			else
			{
            echo $branchName=$row['location'];
			}				
			
			
			echo $dob='1980-12-01 00:00:00';
			echo $doj=$row['created_at'];
			echo $dept=$row['course'];
			echo $econtact=$row['phone'];
			echo $crmid=$row['account_no'];
			$newname=$row['accountname'].$crmid;
			$newname2=$crmid.$row['accountname'];
			echo $name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
			echo $gender="MALE";
			echo $marital="SINGLE";
			echo $mob=$row['phone'];
			echo $designationname=$row['subcourse'];
			
			createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
			//echo $crmid=$row['account_no']."<br>";
            
        }
    }
    else{
            
			//As per Yogesh Sir Req.
			if($row['location']=='Mumbai')
			{
			echo $branchName='Andheri';
			}
			else
			{
            echo $branchName=$row['location'];
			}	
			echo $dob='1980-12-01 00:00:00';
			echo $doj=$row['created_at'];
			echo $dept=$row['course'];
			echo $econtact=$row['phone'];
			echo $crmid=$row['account_no'];
			$newname=$row['accountname'].$crmid;
			$newname2=$crmid.$row['accountname'];
			echo $name=preg_replace('/[^A-Za-z0-9\-]/', '', $newname2);
			echo $gender="MALE";
			echo $marital="SINGLE";
			echo $mob=$row['phone'];
			echo $designationname=$row['subcourse'];
			
			createuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname);
          // echo $crmid=$row['account_no']."<br>";
    }
}
mysqli_close($conn1);
mysqli_close($conn2);

?>