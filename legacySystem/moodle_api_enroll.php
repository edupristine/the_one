<?php
// This file is NOT a part of Moodle - http://moodle.org/
//
// This client for Moodle 2 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//

/**
 * REST client for Moodle 2
 * Return JSON or XML format
 *
 * @authorr Jerome Mouneyrac
 */

function enroll_moodleuser($email, $idnumber){
require_once('moodle_api_get_userid.php');
//require_once('moodle_api_get_courseid.php');

$user = get_moodleuser($email);
//$id = get_courseid($idnumber);
$id = $idnumber;
/// SETUP - NEED TO BE CHANGED
$token = 'c1ecfc2f1ec5f1b5211678ea9f778384';
$domainname = 'https://aceit.edupristine.com';
$functionname = 'enrol_manual_enrol_users';

// REST RETURNED VALUES FORMAT
$restformat = 'json'; //Also possible in Moodle 2.2 and later: 'json'
                     //Setting it to 'json' will fail all calls on earlier Moodle version

$date = new DateTime();
$date->add(new DateInterval('P1Y'));

//////// moodle_user_create_users ////////

/// PARAMETERS - NEED TO BE CHANGED IF YOU CALL A DIFFERENT FUNCTION
$enrolment = new stdClass();
$enrolment->roleid = 5;
$enrolment->userid = $user;
$enrolment->courseid = $id;
$enrolment->timeend = $date->getTimestamp();
$enrolments = array($enrolment);
$params = array('enrolments' => $enrolments);

/// REST CALL

$serverurl = $domainname . '/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction='.$functionname;
require_once('moodle_curl.php');
$curl = new curl;
//if rest format == 'xml', then we do not add the param for backward compatibility with Moodle < 2.2
$restformat = ($restformat == 'json')?'&moodlewsrestformat=' . $restformat:'';
$resp = $curl->post($serverurl . $restformat, $params);
print_r($resp);
}