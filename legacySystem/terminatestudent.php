<?php
/* Template Name:  Student Termination from Biometric Automated system
 * Description:  Student Termination from  Biometric Automated system, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 31st December , 2022
 * Created By: Hitesh
 **/

$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$crm=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$one_db=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
include '/home/ubuntu/webapps/the_one/api/biometric_api.php';
$yesterday = date("Y-m-d",strtotime("-1 days"));


$sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
	sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, DATE_FORMAT(ce.createdtime, '%Y-%m-%d' ) as `created_at`, sc.cf_1495 as `reg_batch`
	FROM vtiger_account ac
	LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
	LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
	INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
    WHERE ce.smownerid!='3920' 
	AND ce.smownerid!='4754'
	AND sc.cf_1489=1
	AND sc.cf_1629 !='LVC / Self Study'
	AND DATE(ce.createdtime) >= '2021-01-01' AND DATE(ce.createdtime) <= '".$yesterday."'";
		  
$result_id = mysqli_query($crm,$sql);
while($row = mysqli_fetch_array($result_id)) {


             //For Biometric Creation
			if(($row['location']!='LVC / Self Study')||($row['location']!='Others')){
            $branchName=$row['location'];
			$dob='2019-07-31 00:00:00';
			$doj=$row['created_at'];
			$course=$row['course'];
			$econtact=$row['phone'];
			$crmid=$row['account_no'];
			$newname=$row['accountname'];
			$name=preg_replace('/[^A-Za-z0-9\-]/', '_', $newname);
			$gender="MALE";
			$marital="SINGLE";
			$mob=$row['phone'];
			$loe=date("Y-m-d");
			//$enrollmentId=$new_bioid;
			
			updateuser($branchName,$dob,$doj,$dept,$econtact,$crmid,$name,$gender,$marital,$mob,$designationname,$loe,'TERMINATED');
}
}
?>