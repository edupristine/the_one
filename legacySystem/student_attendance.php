<?php
ini_set('max_execution_time', 320000);  // 120 (seconds) = 2 Minutes
/* Template Name: Creation of Student Attendance Automated system
 * Description: Creation of Student Attendance Automated system, daily.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 4th October, 2019
 *
 **/
include '../inc/GenericFunctions.php';
include '../control/core.php'; 	
include '../control/checklogin.php';
include '../control/connection.php';
include_once("dbconfig.php");
include '../api/biometric_api.php';


$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');
$start_date = "2019-06-01";
$yesterday = date("Y-m-d",strtotime("-1 days"));

		
		/*50 Students Loop for attendance check*/
		$checkQuery = "SELECT crm_id FROM students where attendance_flag = 0 order by crm_id desc limit 50 ";
		$checkResult = $conn2->query($checkQuery);
        while($row = $checkResult->fetch_assoc()) {
        echo $row['crm_id']."<br>";  
			
		$dqry1 = "SELECT id FROM student_attendance where employeeId='".$row['crm_id']."' and punchDate between  '".$yesterday."' and '".$yesterday."'";
        $checkResult1 = $conn2->query($dqry1);
        $resultCount1 = mysqli_num_rows($checkResult1);
		$totalmain =  $resultCount1;
		if($totalmain==0)
		{
			$result = getrawpuncheslive($row['crm_id'],$yesterday,$yesterday);
		}
		
		}
		
		//GET RAW PUNCHES
		function getrawpuncheslive($empid,$enddate,$startdate)
		{
		Global $conn2;	
			
		$url="https://api.securtime.in/api/raw-data/punches?empId=".$empid."&endDate=".$enddate."&startDate=".$startdate."";	

		$call = callAPI('GET',$url,false);
        $response = json_decode($call, true);
		
		//echo count($response['dailyPunches'])."<br>";
		
		foreach($response['dailyPunches'] as $punch)
		{
		 $serialno= $punch['serialNo'];
		 $employeeId= $punch['employeeId'];
		 $affiliateName= $punch['affiliateName'];
		 $branchName= $punch['branchName'];
		 $deviceLocation= $punch['deviceLocation'];
		 $categoryName= $punch['categoryName'];
		 $departmentName= $punch['departmentName'];
		 $enrollmentId= $punch['enrollmentId'];
		 $deviceDirections= $punch['deviceDirections'];
		 $employeeName= $punch['employeeName'];
		 $punchTime= $punch['punchTime'];
		 $workCodeDescription= $punch['workCodeDescription'];
		 $sourceOfPunch= $punch['sourceOfPunch'];
		 $punchDate= $punch['punchDate'];
		 $punchCreationTime= $punch['punchCreationTime'];
		 $timeZone= $punch['timeZone'];
		 $statusType= $punch['statusType'];
        
		
		//For Duplicate Records check	
		/*$dqry = "SELECT id FROM student_attendance where employeeId='".$employeeId."' and punchDate='".$punchDate."' and punchCreationTime='".$punchCreationTime."'";
        
        $checkResult = $conn2->query($dqry);
        $resultCount = mysqli_num_rows($checkResult);
		
	    
		$total =  $resultCount;
		if($total==0)
		{*/
	
	    /*Insert records in Student Attendance*/
		$sql="Insert into student_attendance (`serialno`,`employeeId`,`affiliateName`,`branchName`,`deviceLocation`,`categoryName`,`departmentName`,`enrollmentId`,`deviceDirections`,`employeeName`,`punchTime`,`workCodeDescription`,`sourceOfPunch`,`punchDate`,`punchCreationTime`,`timeZone`,`statusType`,`created_at`) values ('".$serialno."','".$employeeId."','".$affiliateName."','".$branchName."','".$deviceLocation."','".$categoryName."','".$departmentName."','".$enrollmentId."','".$deviceDirections."','".$employeeName."','".$punchTime."','".$workCodeDescription."','".$sourceOfPunch."','".$punchDate."','".$punchCreationTime."','".$timeZone."','".$statusType."',now())";
		$result = $conn2->query($sql);
		/*}*/
		
		/*Update organization for next batch */
		$sqlup="Update students set attendance_flag=1 where crm_id='".$empid."'";
		$result = $conn2->query($sqlup);
		}
		
		}	   

mysqli_close($conn2);


?>