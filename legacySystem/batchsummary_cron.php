<?php
//error_reporting(0);
/* Template Name: Batch Summary Cron
 * Description: Batches for one month ahead with no. of students registered and no. of days left to start.
 * Version: 1.0
 * Created On: 1st November, 2019
 * Created By: Hitesh
 **/
/*include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail_batches.php';*/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/mail_batches.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';

//$mysql_user='localuser';
//$mysql_pass='R@5gull@';

setlocale(LC_MONETARY, 'en_IN');
$conn2=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$connone=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'ecademe_harry_new');


$sdate = date('Y-m-d');
$edate = date("Y-m-d",strtotime("+30 days"));

$query1 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='FRM' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result1 = mysqli_query($connone,$query1);
$num_rows1 = mysqli_num_rows($result1);

if($num_rows1 != 0) 
{
while($r1 = mysqli_fetch_assoc($result1)) {
    $batch_codes[] = $r1;
}
}


$query2 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='FM' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result2 = mysqli_query($connone,$query2);
$num_rows2 = mysqli_num_rows($result2);

if($num_rows2 != 0) 
{
while($r2 = mysqli_fetch_assoc($result2)) {
    $batch_codes2[] = $r2;
}
}

$query3 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='CFA' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result3 = mysqli_query($connone,$query3);
$num_rows3 = mysqli_num_rows($result3);

if($num_rows3 != 0) 
{
while($r3 = mysqli_fetch_assoc($result3)) {
    $batch_codes3[] = $r3;
}
}

$query4 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='CPA' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result4 = mysqli_query($connone,$query4);
$num_rows4 = mysqli_num_rows($result4);

if($num_rows4 != 0) 
{
while($r4 = mysqli_fetch_assoc($result4)) {
    $batch_codes4[] = $r4;
}
}

$query5 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='CMA' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result5 = mysqli_query($connone,$query5);
$num_rows5 = mysqli_num_rows($result5);

if($num_rows5 != 0) 
{
while($r5 = mysqli_fetch_assoc($result5)) {
    $batch_codes5[] = $r5;
}
}


$query6 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='ACCA' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result6 = mysqli_query($connone,$query6);
$num_rows6 = mysqli_num_rows($result6);

if($num_rows6 != 0) 
{
while($r6 = mysqli_fetch_assoc($result6)) {
    $batch_codes6[] = $r6;
}
}

$query7 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='DM' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result7 = mysqli_query($connone,$query7);
$num_rows7 = mysqli_num_rows($result7);

if($num_rows7 != 0) 
{
while($r7 = mysqli_fetch_assoc($result7)) {
    $batch_codes7[] = $r7;
}
}


$query8 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='BAT' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result8 = mysqli_query($connone,$query8);
$num_rows8 = mysqli_num_rows($result8);

if($num_rows8 != 0) 
{
while($r8 = mysqli_fetch_assoc($result8)) {
    $batch_codes8[] = $r8;
}
}


$query9 = "SELECT b.batch_name as `batch`,c.course_name as `vertical`,DATE_FORMAT(b.estimated_start_date, '%Y-%m-%d') as `startdate`,DATE_FORMAT(b.estimated_start_date, '%d-%b-%Y') as `Formatdate`,b.starttime_slot as `starttime`,b.endtime_slot as `endtime`,b.batch_type  FROM batches b,courses c WHERE b.course_id = c.id AND c.course_name='Analytics' AND estimated_start_date >= '".$sdate."' AND estimated_start_date <='".$edate."'  ORDER BY b.estimated_start_date ASC";
$result9 = mysqli_query($connone,$query9);
$num_rows9 = mysqli_num_rows($result9);

if($num_rows9 != 0) 
{
while($r9 = mysqli_fetch_assoc($result9)) {
    $batch_codes9[] = $r9;
}
}
    

        
		
		
		$html="";
		$html .= "
		<p>Please find below the Batch Summary Report for the next 30 Days</p>
		<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Batch Name</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Start Date</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Time</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Vertical</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:10%;'><b>Batch Type</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Registered Students</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Attending Students</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:16%;'><b>Days to start</b></td>
		</tr>";
				
        
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>FRM</b></td>
		</tr>";
		if($num_rows1 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows1 != 0)  
		{
		foreach($batch_codes as $batches)
		{
		$querycrm1="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches['batch']."' and deleteFlag = 0"; 	
		
		$resultcrm1 = mysqli_query($conn1,$querycrm1);	
		$re1 = mysqli_fetch_assoc($resultcrm1);	
		
		

		
		$date1=date_create($sdate);
		$date2=date_create($batches['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF1 = $diff->format('%d');
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches['starttime']."-".$batches['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Finance</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches['batch_type']."	</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re1['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re1['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF1."</td>
		</tr>";
		}
		}
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>FM</b></td>
		</tr>";
		if($num_rows2 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows2 != 0)  
		{
		foreach($batch_codes2 as $batches2)
		{
		$querycrm2="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches2['batch']."' and deleteFlag = 0"; 	
		$resultcrm2 = mysqli_query($conn1,$querycrm2);	
		$re2 = mysqli_fetch_assoc($resultcrm2);	
		
		$date1=date_create($sdate);
		$date2=date_create($batches2['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF2= $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches2['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches2['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches2['starttime']."-".$batches2['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Finance</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches2['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re2['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re2['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF2."</td>
		</tr>";
		}
		}
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>CFA</b></td>
		</tr>";
		if($num_rows3 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows3 != 0)  
		{
		foreach($batch_codes3 as $batches3)
		{
		$querycrm3="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches3['batch']."' and deleteFlag = 0"; 	
		$resultcrm3 = mysqli_query($conn1,$querycrm3);	
		$re3 = mysqli_fetch_assoc($resultcrm3);	
		$date1=date_create($sdate);
		$date2=date_create($batches3['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF3= $diff->format('%d');
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches3['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches3['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches3['starttime']."-".$batches3['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Finance</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches3['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re3['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re3['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF3."</td>
		</tr>";
		}
		}
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>CPA</b></td>
		</tr>";
		if($num_rows4 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows4 != 0)  
		{
		foreach($batch_codes4 as $batches4)
		{
		$querycrm4="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches4['batch']."' and deleteFlag = 0"; 	
		$resultcrm4 = mysqli_query($conn1,$querycrm4);	
		$re4 = mysqli_fetch_assoc($resultcrm4);	
		$date1=date_create($sdate);
		$date2=date_create($batches4['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF4= $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches4['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches4['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches4['starttime']."-".$batches4['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Accounts</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches4['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re4['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re4['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF4."</td>
		</tr>";
		}
		}
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>CMA</b></td>
		</tr>";
		if($num_rows5 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows5 != 0)  
		{
		foreach($batch_codes5 as $batches5)
		{
		$querycrm5="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches5['batch']."' and deleteFlag = 0"; 	
		$resultcrm5 = mysqli_query($conn1,$querycrm5);	
		$re5 = mysqli_fetch_assoc($resultcrm5);	
		$date1=date_create($sdate);
		$date2=date_create($batches5['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF5 = $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches5['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches5['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches5['starttime']."-".$batches5['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Accounts</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches5['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re5['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re5['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF5."</td>
		</tr>";
		}
		}
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>ACCA</b></td>
		</tr>";
		if($num_rows6 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows6 != 0)  
		{
		foreach($batch_codes6 as $batches6)
		{
		$querycrm6="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches6['batch']."' and deleteFlag = 0"; 	
		$resultcrm6 = mysqli_query($conn1,$querycrm6);	
		$re6 = mysqli_fetch_assoc($resultcrm6);	
		$date1=date_create($sdate);
		$date2=date_create($batches6['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF6= $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches6['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches6['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches6['starttime']."-".$batches6['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Accounts</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches6['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re6['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re6['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF6."</td>
		</tr>";
		}
		}
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>Digital Marketing</b></td>
		</tr>";
		if($num_rows7 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows7 != 0)  
		{
		foreach($batch_codes7 as $batches7)
		{
		$querycrm7="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches7['batch']."' and deleteFlag = 0"; 	
		$resultcrm7 = mysqli_query($conn1,$querycrm7);	
		$re7 = mysqli_fetch_assoc($resultcrm7);	
		$date1=date_create($sdate);
		$date2=date_create($batches7['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF7=  $diff->format('%d');
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches7['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches7['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches7['starttime']."-".$batches7['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>DM</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches7['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re7['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re7['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF7."</td>
		</tr>";
		}
		}
		
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>BAT</b></td>
		</tr>";
		if($num_rows8 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows8 != 0)  
		{
		foreach($batch_codes8 as $batches8)
		{
		$querycrm8="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches8['batch']."' and deleteFlag = 0"; 	
		$resultcrm8 = mysqli_query($conn1,$querycrm8);	
		$re8 = mysqli_fetch_assoc($resultcrm8);
		
		$date1=date_create($sdate);
		$date2=date_create($batches8['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF8= $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches8['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches8['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches8['starttime']."-".$batches8['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>BAT</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches8['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re8['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re8['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF8."</td>
		</tr>";
		}
		}
		
		$html .= "<tr>
		<td colspan=8 style='border:1px solid #327bbe;color:black; padding: 8px 5px; background-color: #FFE4AA; text-align:center; font-size: 12px;'><b>Analytics</b></td>
		</tr>";
		if($num_rows9 == 0) 
        {
		$html .= "<tr>
		<td colspan=8 style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		No Records
		</b>
		</td>
		</tr>";
		}else if($num_rows9 != 0)  
		{
		foreach($batch_codes9 as $batches9)
		{
		$querycrm9="SELECT (select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE (sc.cf_1495 = ws.CODE
		) AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ) AS registered_count,
		(select count(*) as `no_of_student` FROM vtigercrm6.vtiger_account ac
		LEFT JOIN vtigercrm6.vtiger_accountscf sc ON sc.accountid = ac.accountid
		LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ((sc.cf_1497 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1499 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1501 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1503 = ws.CODE AND sc.cf_1495 != ws.CODE)
		OR (sc.cf_1505 = ws.CODE AND sc.cf_1495 != ws.CODE))  AND ac.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920) AS attending_count
		FROM workshops ws
		WHERE ws.CODE='".$batches9['batch']."' and deleteFlag = 0"; 	
		$resultcrm9 = mysqli_query($conn1,$querycrm9);	
		$re9 = mysqli_fetch_assoc($resultcrm9);
		
		$date1=date_create($sdate);
		$date2=date_create($batches9['startdate']);
		$diff=date_diff($date1,$date2);
		$dateF9= $diff->format('%d');
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'><b>
		".$batches9['batch']."	
		</b></td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$batches9['Formatdate']."	
		</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$batches9['starttime']."-".$batches9['endtime']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>Analytics</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:10%;'>".$batches9['batch_type']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re9['registered_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>
		".$re9['attending_count']."</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 0px; background-color: #FFFFFF; text-align:center; font-size: 12px;width:16%;'>".$dateF9."</td>
		</tr>";
		}
		}
		
		
		$html .= "</table>";
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Development Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$today = date("Y-m-d");
		$subject = date("d-M-y",strtotime($today))." - Batch Summary for next 30 Days";
		//echo $html;
		send_smt_mail($subject,$html);
		

?>