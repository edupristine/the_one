<?php

/* Template Name: Migrate students LMS(CRM Based) to ACEIT
 * Description: Migrating and Enrolling the students from crm to the ACEIT, Yesterday CFA Enrolled Students.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 1st June, 2021
 * Created By: Hitesh P
 *
 **/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

$yesterday = date("Y-m-d",strtotime("-1 days"));
$today = date("Y-m-d");
//Orginal Query		
$sql= "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		,sc.cf_1487 AS `not_access`,sc.cf_1467 AS `course_access_flag`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`,ad.bill_country as `country`,sc.cf_1649 as `modules`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE  sc.cf_918 = 'CFA' 
		AND ce.smownerid!='3920' 
		AND ce.smownerid!='4754'
		AND sc.cf_1489 =1
		AND sc.cf_1467 =1
		AND sc.cf_1513 in 
		(
		'CFA Level I',
		'CFA Level I Crash Course',
		'CFA Level I Crash Course LVC',
		'CFA level I LVC',
		'CFA Level I Self study Package',
		'Self-Study Package'
		'CFA L1-Ethics',
		'CFA FT LVC',
		'CFA FT'
		)
		AND DATE(ce.createdtime) = '".$yesterday."'";		

		
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
     //For  ACEIT ID array 
	 $prodid="";
	 $acesql= "SELECT aceit_id from aceit_course_map WHERE crm_subcourse='".$row['subcourse']."' AND is_active=1";		
     $resultac = $conn2->query($acesql);
	 $rowac = $resultac->fetch_assoc();
	 	 
	 $toaccess="";
	 if($row['subcourse']=='Self-Study Package')
		{
			$finalarray= "";
			$sel_modules=explode(" |##| ",$row['modules']);
			$total_count=count($sel_modules);
		
			foreach($sel_modules as $mods)
			{		
			$modql= "SELECT MOODLE_ID from MODULE_MULTISELECT WHERE MODULE_NAME='".$mods."'";		
			$resultacmod = $conn1->query($modql);
			$rowacmod = $resultacmod->fetch_assoc();
			$finalarray .=$rowacmod['MOODLE_ID'].",";
			
			}
			
			if($total_count>=5)
			{
			$finalarray .="38,";
			}
			 
			$toaccess=rtrim($finalarray, ',');
		}else
		{
			$toaccess=$rowac['aceit_id'];
		}
	    $tofinalaccess=explode(",",$toaccess);
		
		
	    foreach ($tofinalaccess as $aceid)
		{
		echo revokeaccesslms($row['first_name'],$row['last_name'],$row['email'],$aceid);	
		}
		
		//Update CRM Course Access Flag
		$updatecaccesflag="UPDATE `vtiger_accountscf` SET cf_1467=0,cf_1469='',cf_1491='".$today."' WHERE accountid=".$row['accid']." AND cf_1467=1";
		//echo $updatecaccesflag."<p>";
		$resultup = $conn1->query($updatecaccesflag);
		
	}
	
function revokeaccesslms($fname,$lname,$email,$prodid)
{
			
		require_once("/home/ubuntu/webapps/the_one/legacySystem/get_userid.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_unenroll.php");
 
        $user = get_moodleuser($email); //Check the user with email address
		//print_r($user);
		if($user == 0 || !isset($user)){
				
		}else{
		     unenroll_moodleuser(strtolower($email), $prodid);
			 $result = "Existing User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
		}
		
		return $result;
		

}	

mysqli_close($conn1);



?>
