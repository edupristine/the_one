<?php
error_reporting(E_ALL & ~E_NOTICE);
/* Template Name: Unenrolling students LMS(CRM Based) to ACEIT
 * Description: Unenrolling the students from crm to the ACEIT, Previous Students.
 * Version: 1.0
 * URL : http://crm.edupristine.com
 * Created On: 8th June, 2021
 * Created By: Hitesh P
 *
 **/

include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/checklogin.php';
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/dbconfig.php';
// parse_str($argv[1], $params);
// $date = $params['date'];
$conn1 = new mysqli($db_server, $db_username, $db_password, $db_name);
$conn2 = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

$yesterday = date("Y-m-d",strtotime("-1 days"));
//Orginal Query
// $sql = "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        // sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		// ,sc.cf_1487 AS `not_access`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`
        // FROM vtiger_account ac
        // LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        // LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        // INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        // WHERE  sc.cf_918 = 'CFA' 
		// AND ce.smownerid='3920' 
		// AND sc.cf_1467 = '1'
		// AND sc.cf_1489 = '1'
		// AND sc.cf_1513 in 
		// (
		// 'CFA Level I',
		// 'CFA Level I Crash Course',
		// 'CFA Level I Crash Course LVC',
		// 'CFA level I LVC',
		// 'CFA Level I Self study Package',
		// 'Self-Study Package'
		// )
		// AND DATE(ce.createdtime) >= '2019-01-01' and DATE(ce.createdtime) <= '2021-05-31'";
		
//DEMO QUERY		
$sql= "SELECT ac.account_no,ac.accountname,ac.phone,ac.email1 as `email`,sc.cf_918 as `course`,
        sc.cf_1513 as `subcourse`,sc.cf_1629 as `location`, ce.createdtime as `created_at`, sc.cf_1495 as `reg_batch`
		,sc.cf_1487 AS `not_access`, sc.cf_1109 AS `first_name`,sc.cf_1111 AS `last_name`,sc.accountid AS `accid`
        FROM vtiger_account ac
        LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
        LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
        INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
        WHERE  sc.cf_918 = 'CFA' 
		AND ce.smownerid='3920' 
		AND sc.cf_1467 = '1'
		AND sc.cf_1489 = '1'
		AND sc.cf_1513 in 
		(
		'CFA Level I',
		'CFA Level I Crash Course',
		'CFA Level I Crash Course LVC',
		'CFA level I LVC',
		'CFA Level I Self study Package',
		'Self-Study Package'
		)
		AND DATE(ce.createdtime) >= '2019-01-01' and DATE(ce.createdtime) <= '2021-05-31' AND ac.email1 LIKE '%hitesh%' LIMIT 1";		

		
$result = $conn1->query($sql);
$dataCount = 1;
while($row = $result->fetch_assoc()) {
     //For  ACEIT ID array 
	 $prodid="";
	 $acesql= "SELECT aceit_id from aceit_course_map WHERE crm_subcourse='".$row['subcourse']."' AND is_active=1";		
     $resultac = $conn2->query($acesql);
	 $rowac = $resultac->fetch_assoc();
	 $rawaceid=explode(",",$rowac['aceit_id']);
	
	    foreach ($rawaceid as $aceid)
		{
		echo revokeaccesslms($row['first_name'],$row['last_name'],$row['email'],$aceid);	
		}
		
		//Update CRM Course Access Flag
		$updatecaccesflag="UPDATE `vtiger_accountscf` SET cf_1467=0 WHERE accountid=".$row['accid']." AND cf_1467=1";
		echo $updatecaccesflag."<p>";
		//$resultup = $conn1->query($updatecaccesflag);
		
	}
	
function revokeaccesslms($fname,$lname,$email,$prodid)
{
		
		require_once("/home/ubuntu/webapps/the_one/legacySystem/get_userid.php");
		require_once("/home/ubuntu/webapps/the_one/legacySystem/moodle_api_unenroll.php");
 
        $user = get_moodleuser($email); //Check the user with email address
		if($user == 0 || !isset($user)){	
			
		}else{
			 unenroll_moodleuser(strtolower($email), $prodid);
			 $result = "Revoke Existing User :---->Student Name:-".$fname.".".$lname."--Student Email:-".$email."--ACEIT Prod Id:".$prodid."<p>"; 	
		}
		
		return $result;
		

}	

mysqli_close($conn1);



?>
