<?php
//error_reporting(0);
/* Template Name: Batch Plan  Cron
 * Description: Batch Plan for the current week and upcoming 3 Months
 * Version: 1.0
 * Created On: 11th Jan, 2023
 * Created By: Hitesh
 **/


include '/home/ubuntu/webapps/the_one/inc/GenericFunctions.php';
include '/home/ubuntu/webapps/the_one/control/core.php'; 	
include '/home/ubuntu/webapps/the_one/control/connection.php';
include '/home/ubuntu/webapps/the_one/legacySystem/mail_asset.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
setlocale(LC_MONETARY, 'en_IN');
$conn2=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$connone=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'ecademe_harry_new');


//$startdate='2019-04-01';
//$enddate='2019-12-30';
$date = new DateTime();
$date->modify("last day of previous month");
$enddate= $date->format("Y-m-d");

$locater_name='Mumbai';

$avail_rooms=0;
if($locater_name=='Mumbai')
{
$avail_rooms=6;	
}

$location_query="SELECT * FROM locations where loc_name = '".$locater_name."'";
$result1oc = mysqli_query($connone,$location_query);

while($r2 = mysqli_fetch_assoc($result1oc)) {
	$result_locations[] = $r2;
}

//Weekdays variables
$start = date('Y-m-d', strtotime('monday this week'));
$end = date('Y-m-d', strtotime('friday this week'));

//Monthly variables
$month_start = date('Y-m-d', strtotime('monday next week'));
//echo "Month Start:-".$month_start;
$month_raw_end = date('Y-m-d', strtotime('friday this week'));
$month_end = date('Y-m-d', strtotime("+3 months", strtotime($month_raw_end)));
//echo "Month End:-".$month_end;


//Saturday variables
$start_sat = date('Y-m-d', strtotime('saturday this week'));
$end_sat = date('Y-m-d', strtotime('saturday this week'));



//Sunday variables
$start_sun = date('Y-m-d', strtotime('sunday this week'));
$end_sun = date('Y-m-d', strtotime('sunday this week'));



$dates_array=array();
$dates_array = dateRange($start,$end);

$dates_sat_array=array();
$dates_sat_array = dateRange($start_sat,$end_sat);

$dates_sun_array=array();
$dates_sun_array = dateRange($start_sun,$end_sun);

//Utilization
$utilize_week_mor=0;
$utilize_week_even=0;

$utilize_sat_mor=0;
$utilize_sat_even=0;

$utilize_sun_mor=0;
$utilize_sun_even=0;


//Available
$available_week_mor=0;
$available_week_even=0;

$available_sat_mor=0;
$available_sat_even=0;

$available_sun_mor=0;
$available_sun_even=0;


//Next Months variables
$batchesweekdays_mor_this_month=0;
$batchesweekdays_even_this_month=0;


$batchessat_mor_this_month=0;
$batchessat_even_this_month=0;

$batchessun_mor_this_month=0;
$batchessun_even_this_month=0;


//Next Months variables
$batchesweekdays_mor_next_month=0;
$batchesweekdays_even_next_month=0;

$batchessat_mor_next_month=0;
$batchessat_even_next_month=0;

$batchessun_mor_next_month=0;
$batchessun_even_next_month=0;


//First Day this month
$this_month_start_date = date('Y-m-d', strtotime('first day of this month'));
// Last Day this month
$this_month_end_date = date('Y-m-d', strtotime('last day of this month'));

//First Day next month
$next_month_start_date = date('Y-m-d', strtotime('first day of next month'));
// Last Day nex month
$next_month_end_date = date('Y-m-d', strtotime('last day of next month'));



//No of Days finder
function numWeekdays( $start_ts, $end_ts, $day, $include_start_end = false ) {

    $day = strtolower( $day );
    $current_ts = $start_ts;
    // loop next $day until timestamp past $end_ts
    while( $current_ts < $end_ts ) {

        if( ( $current_ts = strtotime( 'next '.$day, $current_ts ) ) < $end_ts) {
            $days++;
        }
    }

    // include start/end days
    if ( $include_start_end ) {
        if ( strtolower( date( 'l', $start_ts ) ) == $day ) {
            $days++;
        }
        if ( strtolower( date( 'l', $end_ts ) ) == $day ) {
            $days++;
        }
    }   

    return (int)$days;

}

//$sat = numWeekDays( $start, $end, 'saturday', true );  
//$sun = numWeekDays( $start, $end, 'sunday', true );

$sat = 1;  
$sun = 1;
$finaldays=$sat+$sun;


$d=strtotime($start);
$disstart=date("j \ F ", $d);

$ed=strtotime($end);
$diend=date("j \ F Y", $ed);
/*<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid;border-color:black;'>
		<th  colspan=6 style='text-align:left;'>Date: $disstart / $diend</th>
		<th colspan=4> </th>
		</tr>*/
        $html="";
		$html .= "
		<p>Please find below the Batch Planning Report for all centers</p>
		<table border=1px; style='border:1px solid;border-color:black; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>
		<thead>
		
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid #000000;height:40px;'>
		<th colspan=1 rowspan=2 style='text-align: center;background-color:#00B0F0;color:#000000;'>".$locater_name."</th>
		<th colspan=2 rowspan=2>Running Batches</th>
		<th colspan=2 rowspan=2 style='text-align: center;background-color:#00B050;color:#ffffff;'>Upcoming Batch (Next 3 Months)</th>
		
		</tr>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid;border-color:black;'>
		
		</tr>
		<tr style='text-align: center;background-color:#FFFF00;color:#000000;border:1px solid #000000;'>
		<th >Types</th>
		<th >Morning</th>
		<th >Afternoon</th>
		<th style='text-align: center;background-color:#00B050;color:#ffffff;'>Morning</th>
		<th style='text-align: center;background-color:#00B050;color:#ffffff;'>Afternoon</th>
		</tr>
		</thead>";
				
		$html .= "<tbody>";
		
		
		foreach($result_locations as $locs)
		{
		$locvar="";
		$locvar_wd="";
		if($locs['loc_name']=='Mumbai')
		{
		$locvar= "and  wd.location IN ('Andheri','Mumbai')";
        $locvar_wd= "and  l.loc_name IN ('Andheri','Mumbai')";		
		}
		else
		{
		$locvar= "and  wd.location IN ('".$locs['loc_name']."')";  
		$locvar_wd= "and  l.loc_name IN ('".$locs['loc_name']."')";  
		}										   
		
				
				//Weekly	
				$ot_array=weekdaybatchcodes_weekmorn($start,$end,$locvar,'max');
				$otev_array=weekdaybatchcodes_weekeven($start,$end,$locvar,'max');
				
								
				
                $html .= "<tr style='border:1px solid #000000;text-align: center;'>
				<td ><strong>WeekDays</strong></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>End Date</td></tr>";
				$ij=1;
				foreach($ot_array as $ars)
				{
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');	
				
				$varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchesweekdays_mor_this_month +=$varsmor;
					
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				//Blank values
				for($iar1=count($ot_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>End Date</td></tr>";
				$ijev=1;
				foreach($otev_array as $ars)
				{
					
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');	

                $varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchesweekdays_even_this_month +=$varsmor;				
					
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				//Blank values
				for($iar1=count($otev_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>";
				//MONTHLY
				
				$ot_array_mth=weekdaybatchcodes_weekmorn_month($month_start,$month_end,$locvar_wd,'min');
				$otev_array_mth=weekdaybatchcodes_weekeven_month($month_start,$month_end,$locvar_wd,'min');
				
				
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Start Date</td></tr>";
				$ij=1;
				foreach($ot_array_mth as $ars)
				{
				$date = new DateTime($ars['estimated_start_date']);
				
				
				
				$result_dt = $date->format('d M Y');	
				
				$day = date("D", strtotime($ars['estimated_start_date']));


				if(($day != 'Sun')&&($day != 'Sat'))
				{
				
				$varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchesweekdays_mor_next_month +=$varsmor;
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				}
				//Blank values
				for($iar1=$ij;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>Start Date</td></tr>";
				$ijev=1;
				foreach($otev_array_mth as $ars)
				{
				$date = new DateTime($ars['estimated_start_date']);
				$result_dt = $date->format('d M Y');

                $day = date("D", strtotime($ars['estimated_start_date']));


				if(($day != 'Sun')&&($day != 'Sat'))
				{				
				$varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchesweekdays_even_next_month +=$varsmor;
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				}
				//Blank values
				for($iar1=$ijev;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>
				</tr>";
				
				
				
				
		
			
		   foreach($dates_sat_array as $dts)
			{
				$ot_array=weekdaybatchcodes_morn($dts,$locvar,'max');
				$otev_array=weekdaybatchcodes_even($dts,$locvar,'max');
				
				
				//MONTHLY
				
				$ot_array_mth=weekdaybatchcodes_weekmorn_month($month_start,$month_end,$locvar_wd,'min');
				$otev_array_mth=weekdaybatchcodes_weekeven_month($month_start,$month_end,$locvar_wd,'min');
				
				
                $html .= "<tr style='border:1px solid #000000;text-align: center;'>
				<td ><strong>".date('l', strtotime($dts))."</strong></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>End Date</td></tr>";
				$ij=1;
				foreach($ot_array as $ars)
				{
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');
				
				$varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchessat_mor_this_month +=$varsmor;
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				//Blank values
				for($iar1=count($ot_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>End Date</td></tr>";
				$ijev=1;
				foreach($otev_array as $ars)
				{
					
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');

				$varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchessat_even_this_month +=$varsmor;
				
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				//Blank values
				for($iar1=count($otev_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>";
				
				
				//MONTHLY
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Start Date</td></tr>";
				$ij=1;
				foreach($ot_array_mth as $ars)
				{
					
					
				$day = date("D", strtotime($ars['estimated_start_date']));


				if($day == 'Sat')
				{
				
                $varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchessat_mor_next_month +=$varsmor;				
					
				$date = new DateTime($ars['estimated_start_date']);
				$result_dt = $date->format('d M Y');	
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				
				}
				//Blank values
				for($iar1=$ij;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>Start Date</td></tr>";
				$ijev=1;
				foreach($otev_array_mth as $ars)
				{
				$day = date("D", strtotime($ars['estimated_start_date']));


				if($day == 'Sat')
				{
				$date = new DateTime($ars['estimated_start_date']);
				$result_dt = $date->format('d M Y');	
				
				$varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchessat_even_next_month +=$varsmor;	
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				}
				//Blank values
				for($iar1=$ijev;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>
				</tr>";
				
		    }

		   foreach($dates_sun_array as $dts)
			{
				
				$ot_array=weekdaybatchcodes_morn($dts,$locvar,'max');
				$otev_array=weekdaybatchcodes_even($dts,$locvar,'max');
				
				//MONTHLY
				
				$ot_array_mth=weekdaybatchcodes_weekmorn_month($month_start,$month_end,$locvar_wd,'min');
				$otev_array_mth=weekdaybatchcodes_weekeven_month($month_start,$month_end,$locvar_wd,'min');
				
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;'>
				<td ><strong>".date('l', strtotime($dts))."</strong></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>End Date</td></tr>";
				$ij=1;
				foreach($ot_array as $ars)
				{
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');

                $varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchessun_mor_this_month +=$varsmor;				
				
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				//Blank values
				for($iar1=count($ot_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>End Date</td></tr>";
				$ijev=1;
				foreach($otev_array as $ars)
				{
				$date = new DateTime($ars['step_date']);
				$result_dt = $date->format('d M Y');

				$varsmor=!(check_in_range($this_month_start_date, $this_month_end_date, $ars['step_date']));
				$batchessun_even_this_month +=$varsmor;				
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;'>".$ars['course']."-".$ars['batch_code']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				//Blank values
				for($iar1=count($otev_array)+1;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>";
				//MONTHLY
				$html .= "<td><table style='border:1px solid #000000;text-align: center;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;font-size:12px;width:200px;'>Morning</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>Start Date</td></tr>";
				$ij=1;
				foreach($ot_array_mth as $ars)
				{
					
					
				$day = date("D", strtotime($ars['estimated_start_date']));


				if($day == 'Sun')
				{
					
				$varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchessun_mor_next_month +=$varsmor;
				
				$date = new DateTime($ars['estimated_start_date']);
				$result_dt = $date->format('d M Y');	
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ij."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ij++;
				}
				
				}
				//Blank values
				for($iar1=$ij;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				$html .="</table></td>";
				$html .= "<td><table style='border:1px solid #000000;text-align: center;font-size:12px;width:100%;height:400px;'><tr><td style='border:1px solid #000000;text-align: center;'>Batch Planned</td><td style='border:1px solid #000000;text-align: center;width:200px;'>Afternoon</td><td style='border:1px solid #000000;text-align: center;'>Start Date</td></tr>";
				$ijev=1;
				foreach($otev_array_mth as $ars)
				{
				$day = date("D", strtotime($ars['estimated_start_date']));


				if($day == 'Sun')
				{
				$varsmor=check_in_range($next_month_start_date, $next_month_end_date, $ars['estimated_start_date']);
				$batchessun_even_next_month +=$varsmor;	
					
				$date = new DateTime($ars['estimated_start_date']);
				$result_dt = $date->format('d M Y');	
				
				$html .= "<tr style='border:1px solid #000000;text-align: center;font-size:12px;'><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ijev."</td><td style='border:1px solid #000000;text-align: center;font-size:12px;'>".$ars['course_name']."-".$ars['batch_name']."</td><td style='border:1px solid #000000;text-align: center;'>".$result_dt."</td></tr>";
				$ijev++;
				}
				}
				//Blank values
				for($iar1=$ijev;$iar1<=$locs['rooms'];$iar1++)
				{
				$html .= "<tr style='border:1px solid #000000;text-align: center;'><td style='border:1px solid #000000;text-align: center;'>".$iar1."</td><td style='border:1px solid #000000;text-align: center;'>-</td><td style='border:1px solid #000000;text-align: center;'>-</td></tr>";	
				}
				
				
				
				$html .="</table></td>
				</tr>";
		    }
			
			
			
		}
		$html .= "</tbody>";
		$html .= "</table><p>";
		
		
		$utilize_week_mor=$batchesweekdays_mor_this_month+$batchesweekdays_mor_next_month;
		$utilize_week_even=$batchesweekdays_even_this_month+$batchesweekdays_even_next_month;

		$utilize_sat_mor=$batchessat_mor_this_month+$batchessat_mor_next_month;
		$utilize_sat_even=$batchessat_even_this_month+$batchessat_even_next_month;

		$utilize_sun_mor=$batchessun_mor_this_month+$batchessun_mor_next_month;
		$utilize_sun_even=$batchessun_even_this_month+$batchessun_even_next_month;
		
		$stylescheck="style='background-color:#ffffff;color:#000000;'";
		//Available
		$available_week_mor=$avail_rooms-$utilize_week_mor;
		if($available_week_mor<0)
		{
		$stylescheck="style='background-color:#ff0000;color:#ffffff;'";	
		}
		
		
		$stylescheck1="style='background-color:#ffffff;color:#000000;'";
		$available_week_even=$avail_rooms-$utilize_week_even;
		if($available_week_even<0)
		{
		$stylescheck1="style='background-color:#ff0000;color:#ffffff;'";	
		}

        $stylescheck2="style='background-color:#ffffff;color:#000000;'";
		$available_sat_mor=$avail_rooms-$utilize_sat_mor;
		if($available_sat_mor<0)
		{
		$stylescheck2="style='background-color:#ff0000;color:#ffffff;'";	
		}
		
		$stylescheck3="style='background-color:#ffffff;color:#000000;'";
		$available_sat_even=$avail_rooms-$utilize_sat_even;
		if($available_sat_even<0)
		{
		$stylescheck3="style='background-color:#ff0000;color:#ffffff;'";	
		}

        $stylescheck4="style='background-color:#ffffff;color:#000000;'";
		$available_sun_mor=$avail_rooms-$utilize_sun_mor;
		if($available_sun_mor<0)
		{
		$stylescheck4="style='background-color:#ff0000;color:#ffffff;'";	
		}
		
		$stylescheck5="style='background-color:#ffffff;color:#000000;'";
		$available_sun_even=$avail_rooms-$utilize_sun_even;
		if($available_sun_even<0)
		{
		$stylescheck5="style='background-color:#ff0000;color:#ffffff;'";	
		}

        $html .= "<table border=1px; style='border:1px solid;border-color:black; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>
		<tr style='border:1px solid #000000;text-align: center;'>
		<td colspan=2 style='text-align: center;background-color:#00B0F0;color:#000000;'> <strong>".$locater_name." </strong></td>
		
		<td colspan=2 style='background-color:#FFFF00;'> <strong>Batch Utilise for Next month </strong></td>
		
		<td colspan=2 style='background-color:#FFFF00;'>  <strong><strong>Batches planned for Next Month </strong></td>
		
		<td colspan=2 style='background-color:#00B050;color:#ffffff;'> <strong>Utilize =Running + Planned </strong></td>
		
		<td colspan=2 style='background-color:#FFE699;'> <strong>Available </strong></td>
		
		</tr>
		<tr style='border:1px solid #000000;text-align: center;'>
		<td >Rooms</td>
		<td >Type</td>
		<td>Morning</td>
		<td>Afternoon</td>
		<td>Morning</td>
		<td>Afternoon</td>
		<td>Morning</td>
		<td>Afternoon</td>
		<td>Morning</td>
		<td>Afternoon</td>
		</tr>
		<tr style='border:1px solid #000000;text-align: center;'>
		<td >".$avail_rooms."</td>
		<td>WeekDays</td>
		<td>".$batchesweekdays_mor_this_month."</td>
		<td>".$batchesweekdays_even_this_month."</td>
		<td>".$batchesweekdays_mor_next_month."</td>
		<td>".$batchesweekdays_even_next_month."</td>
		<td>".$utilize_week_mor."</td>
		<td>".$utilize_week_even."</td>
		<td ".$stylescheck.">".$available_week_mor."</td>
		<td ".$stylescheck1.">".$available_week_even."</td>
		</tr>
		<tr style='border:1px solid #000000;text-align: center;'>
		<td >".$avail_rooms."</td>
		<td>Saturday</td>
		<td>".$batchessat_mor_this_month."</td>
		<td>".$batchessat_even_this_month."</td>
		<td>".$batchessat_mor_next_month."</td>
		<td>".$batchessat_even_next_month."</td>
		<td>".$utilize_sat_mor."</td>
		<td>".$utilize_sat_even."</td>
		<td ".$stylescheck2.">".$available_sat_mor."</td>
		<td ".$stylescheck3.">".$available_sat_even."</td>
		</tr>
		<tr style='border:1px solid #000000;text-align: center;'>
		<td >".$avail_rooms."</td>
		<td>Sunday</td>
		<td>".$batchessun_mor_this_month."</td>
		<td>".$batchessun_even_this_month."</td>
		<td>".$batchessun_mor_next_month."</td>
		<td>".$batchessun_even_next_month."</td>
		<td>".$utilize_sun_mor."</td>
		<td>".$utilize_sun_even."</td>
		<td ".$stylescheck4.">".$available_sun_mor."</td>
		<td ".$stylescheck5.">".$available_sun_even."</td>
		</tr>
		
		</table>";
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Development Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$today = date("Y-m-d");
		$subject = "  Asset Utilization Report for $disstart - $diend";
		
		echo $html;
		//send_smt_mail($subject,$html);
		
/**
 * Date range
 *
 * @param $first
 * @param $last
 * @param string $step
 * @param string $format
 * @return array
 */
function dateRange( $first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
    $dates = [];
    $current = strtotime( $first );
    $last = strtotime( $last );

    while( $current <= $last ) {

        $dates[] = date( $format, $current );
        $current = strtotime( $step, $current );
    }

    return $dates;
}

//Weekend Morning
function weekdaybatchcodes_morn($date,$locvar,$type)
{
global $conn1;
	
$conducted="SELECT *,(SELECT ".$type."(ws_date) FROM workshops_dates where  workshop_id=wd.workshop_id) AS `step_date` FROM workshops_dates wd where trim(wd.start_time) in ('07:30 AM','08:00 AM','08:30 AM','09:00 AM','09:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 PM','12:30 PM','01:00 PM','01:30 PM') AND wd.status not in ('Error','No Class','Cancelled') AND wd.ws_date = '".$date."'
				  $locvar ORDER BY wd.batch_code ASC";
				
				$result1 = mysqli_query($conn1,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}

//Weekend Evening
function weekdaybatchcodes_even($date,$locvar,$type)
{
global $conn1;
	
$conducted="SELECT *,(SELECT ".$type."(ws_date) FROM workshops_dates where  workshop_id=wd.workshop_id) AS `step_date` FROM workshops_dates wd where trim(wd.start_time) in ('02:00 PM','02:30 PM','03:00 PM','03:30 PM','04:00 PM','04:30 PM','05:00 PM','05:30 PM','06:00 PM','06:30 PM','07:00 PM','07:30 PM','08:00 PM','08:30 PM','09:00 PM','09:30 PM') AND wd.status not in ('Error','No Class','Cancelled') AND wd.ws_date = '".$date."'
				  $locvar ORDER BY wd.batch_code ASC";
				
				$result1 = mysqli_query($conn1,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}


//WeekDays Morning
function weekdaybatchcodes_weekmorn($fromdate,$todate,$locvar,$type)
{
global $conn1;
	
$conducted="SELECT DISTINCT(wd.batch_code) as `batch_code`,wd.course,(SELECT ".$type."(ws_date) FROM workshops_dates where  workshop_id=wd.workshop_id) AS `step_date` FROM workshops_dates wd where trim(wd.start_time) in ('07:30 AM','08:00 AM','08:30 AM','09:00 AM','09:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 PM','12:30 PM','01:00 PM','01:30 PM') AND wd.status not in ('Error','No Class','Cancelled') AND wd.ws_date >= '".$fromdate."' AND wd.ws_date <= '".$todate."'
				  $locvar ORDER BY wd.batch_code ASC";
				
				$result1 = mysqli_query($conn1,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}

//WeekDays Evening
function weekdaybatchcodes_weekeven($fromdate,$todate,$locvar,$type)
{
global $conn1;
	
$conducted="SELECT DISTINCT(wd.batch_code) as `batch_code`,wd.course,(SELECT ".$type."(ws_date) FROM workshops_dates where  workshop_id=wd.workshop_id) AS `step_date` FROM workshops_dates wd where trim(wd.start_time) in ('02:00 PM','02:30 PM','03:00 PM','03:30 PM','04:00 PM','04:30 PM','05:00 PM','05:30 PM','06:00 PM','06:30 PM','07:00 PM','07:30 PM','08:00 PM','08:30 PM','09:00 PM','09:30 PM') AND wd.status not in ('Error','No Class','Cancelled') AND wd.ws_date >= '".$fromdate."' AND wd.ws_date <= '".$todate."'
				  $locvar ORDER BY wd.batch_code ASC";
				
				$result1 = mysqli_query($conn1,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}



//WeekDays Morning month
function weekdaybatchcodes_weekmorn_month($fromdate,$todate,$locvar_wd,$type)
{
global $connone;
	
$conducted="SELECT * FROM batches b,courses cs, locations l WHERE b.course_id=cs.id AND b.loc_id=l.id AND trim(b.starttime_slot) in ('07:30 AM','08:00 AM','08:30 AM','09:00 AM','09:30 AM','10:00 AM','10:30 AM','11:00 AM','11:30 AM','12:00 PM','12:30 PM','01:00 PM','01:30 PM') AND b.estimated_start_date >= '".$fromdate."' AND b.estimated_start_date <= '".$todate."' $locvar_wd ORDER BY b.estimated_start_date ASC";
				
				$result1 = mysqli_query($connone,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}

//WeekDays Evening month
function weekdaybatchcodes_weekeven_month($fromdate,$todate,$locvar_wd,$type)
{
global $connone;
	
$conducted="SELECT * FROM batches b,courses cs, locations l WHERE b.course_id=cs.id AND b.loc_id=l.id AND trim(b.starttime_slot) in ('02:00 PM','02:30 PM','03:00 PM','03:30 PM','04:00 PM','04:30 PM','05:00 PM','05:30 PM','06:00 PM','06:30 PM','07:00 PM','07:30 PM','08:00 PM','08:30 PM','09:00 PM','09:30 PM') AND b.estimated_start_date >= '".$fromdate."' AND b.estimated_start_date <= '".$todate."'  $locvar_wd ORDER BY b.estimated_start_date ASC";
				
				$result1 = mysqli_query($connone,$conducted);
				while($re1conducted = mysqli_fetch_assoc($result1))
				{
				$blumoon[]=$re1conducted;
				}
				return $blumoon;

}


function check_in_range($start_date, $end_date, $date_from_user)
{
  // Convert to timestamp
  $start_ts = strtotime($start_date);
  $end_ts = strtotime($end_date);
  $user_ts = strtotime($date_from_user);

  // Check that user date is between start & end
  return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}



?>