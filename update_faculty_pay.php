<?php 
include 'inc/GenericFunctions.php';
include 'control/core.php'; 	
include 'control/connection.php';



$query_allstudents = "SELECT
fid,
name,
faculty_in_date as `faculty_start_date`,
base_rate as `actual_base_rate`,
TIMESTAMPDIFF(YEAR,faculty_in_date, now() ) AS `years_served`
FROM fac_facultydetails WHERE fid IN (
28091,
27666,
28854,
29004,
29158,
27841,
28874,
29185,
27301,
26761,
25492,
28869,
3572,
29050,
2880,
10424,
27920,
28513,
27748,
29117,
2240,
29184,
3690,
18572,
2133,
28942,
27844,
3558,
27386,
28462,
25368,
29171,
29174,
28536,
29122,
27870,
29113,
28212,
2940,
2752,
2507,
3194,
29191,
20685,
28645,
1537,
28010,
28611,
28989,
18573,
25333,
27385,
25992,
1767,
28055,
29190,
28845,
29187,
10688,
2356,
2256,
3116,
27337,
10643,
29189,
29005,
25464,
29188,
27516,
25806,
2149,
25892,
25050,
29096,
27884,
29193,
29147,
25678,
29192,
29110,
29196,
29153,
19382,
3394,
29115,
3598,
27925,
29197,
28912,
29199,
19790,
28044,
29200,
29198,
29201,
29203,
13184,
24980,
26633,
28825,
10677,
3446,
2437,
28697,
29051,
26231,
2840,
2560,
28972,
3547,
28559,
29173,
29202,
28191,
29168,
25877,
25675,
25497,
3396,
10462,
25671,
20318,
29205,
29206,
29204
) ORDER BY fid asc";
$result_allstudents = Select($query_allstudents,$harry);


echo "
<table border=1>
<thead>
<tr>
<th>#</th>
<th>Faculty Name</th>
<th >Start In Date</th>
<th>Base Rate</th>
<th>No of years Served</th>
<th>Intial Rate (1100) starting Date</th>
<th>Ist Revision(1100->1380)starting Date</th>
<th>2nd Revision(1380->1800)starting Date</th>
</tr></thead><tbody>";
$i=1;
foreach($result_allstudents['rows'] as $student) 
{

$query_first = "SELECT ws_date FROM workshops_dates WHERE faculty_id=".$student['fid']."  AND faculty_base_rate=1100 ORDER BY id ASC LIMIT 1";
$result_query_first = Select($query_first,$harry);

$query_second = "SELECT ws_date FROM workshops_dates WHERE faculty_id=".$student['fid']."  AND faculty_base_rate=1380 ORDER BY id ASC LIMIT 1";
$result_query_second = Select($query_second,$harry);

$query_third = "SELECT ws_date FROM workshops_dates WHERE faculty_id=".$student['fid']."  AND faculty_base_rate=1800 ORDER BY id ASC LIMIT 1";
$result_query_third = Select($query_third,$harry);
	
echo "<tr>
<td>".$i."</td>
<td>".$student['name']."</td>
<td>".$student['faculty_start_date']."</td>
<td><center>".$student['actual_base_rate']."</center></td>
<td><center>".$student['years_served']."</center></td>
<td><center>".$result_query_first['rows'][0]['ws_date']."</center></td>
<td><center>".$result_query_second['rows'][0]['ws_date']."</center></td>
<td><center>".$result_query_third['rows'][0]['ws_date']."</center></td>
</tr>";

$i++;
}
echo "</tbody></table>";										
?>
