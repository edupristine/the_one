<script>

function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}

final_prods_json = '';
submit_button_clicked = '';
function Saveofferedprice(){
	
var accno= document.getElementById('accountno_1').value;	
var newofferpr=document.getElementById('newofferedprice_1').value;
if(accno=="")
{
	alert("Please Enter account No. ");
	
	
}

final_prods = [];

			/* Setting Array For DB Entry */
			var final_prod = {
				"accountid": (document.getElementById('recs_1').value),
				"accountno": (document.getElementById('accountno_1').value),
				"offeredprice": (document.getElementById('offeredprice_1').value),
				"balance": (document.getElementById('balance_1').value),
				"paid": (document.getElementById('paid_1').value),
				"newofferedprice": (document.getElementById('newofferedprice_1').value),
				"newbalance": (document.getElementById('newbalance_1').value),
				"backdiscount": (document.getElementById('backdiscount_1').value),
				"backcomment": (document.getElementById('backcomment_1').value)
			};
			final_prods.push(final_prod);

	final_prods_json = JSON.stringify(final_prods);
	//alert(final_prods_json);
	/* START - Checking if sale entry is already in progress */
	if(submit_button_clicked=='1')
	{
		//alert('Sale saving is in progress...');
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	/* END - Checking if sale entry is already in progress */

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			//alert(xmlhttp.responseText);
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/update_offeredprice.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('details='+fixEscape(final_prods_json));


}

function RECSelect(val,row)
{
	
	var row_id = row.split("_").pop();
	
	

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById('recs_'+row_id).value = output["accountid"];
			document.getElementById('offeredprice_'+row_id).value = output["offered_price"];
			document.getElementById('paid_'+row_id).value = output["Paid"];
			document.getElementById('balance_'+row_id).value = output["Balance"];
			var str = output["discount"];
            var res = str.split("|##|");
			document.getElementById('discount_'+row_id).value = res;
			document.getElementById('studentname_'+row_id).value = output["studentname"];
			document.getElementById('assignedto_'+row_id).value = output["assigned_user"];
			
			var balcheck= parseInt(output["Balance"]);
			
			if(balcheck<=0)
			{
			document.getElementById("newofferedprice_1").disabled = true;
			document.getElementById("updateoff").disabled = true;
			document.getElementById('backcomment_1').disabled = true;
			}
			else if(balcheck>0)
			{
			document.getElementById("newofferedprice_1").disabled = false;
			document.getElementById("updateoff").disabled = false;
			document.getElementById('backcomment_1').disabled = false;	
			}
				
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_paydetails.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}


function OFRSelect(val,row)
{
	
	var row_id = row.split("_").pop();
	
    var newoffer = parseFloat(document.getElementById('newofferedprice_'+row_id).value);
	var oldoffer = parseFloat(document.getElementById('offeredprice_'+row_id).value);
	var paid = parseFloat(document.getElementById('paid_'+row_id).value);
    var newbal = (newoffer-paid).toFixed(2);
	if(isNaN(newbal))
	{
		alert("Please enter new offered price.");
		newoffer.focus();
	}
	document.getElementById('newbalance_'+row_id).value = newbal;
	document.getElementById('backdiscount_'+row_id).value = (oldoffer-newoffer).toFixed(2);
	
			
}
</script>