<script>
row_num = document.getElementById("rows_div").getElementsByTagName("tr").length;
samplerow = document.getElementById('rows_div').innerHTML;
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function AddREC(){
	row_num++;
	var newstring='_'+row_num
	appendcontent = samplerow.replace(/_1/g,newstring); /* if you want all the "_1"'s in the string to be replaced	*/
	
	$('#rows_div').append(appendcontent);
	document.getElementById('sr_'+row_num).value = row_num;
	
	$('#newregcode_'+row_num+', #newregcode_'+row_num+'_validate').select2({placeholder:"Select New Batch Code"});
	// $("#productname_"+row_num).chosen();
}

function DelREC(row_id){
	var r=confirm("Are you sure you wish to delete this Record?");
	if (r==true) {
		document.getElementById(row_id).innerHTML = '';
	}
	else {
		return;
	}
}
final_prods_json = '';
submit_button_clicked = '';
function SaveBatchcodes(){
	
	for (j=1;j<=row_num;j++)
	{
		if(document.getElementById('row_id_'+j).innerHTML == '')
		{
		}
		else
		{
			sel = document.getElementById('newregcode_'+j).value;
			if(sel == '')
			{
				alert("Please select a New Registered Batch Code for this account.");
				return;
			}	
		}
			
	}
	
	

	

	
	final_prods = [];
	for (var i=1;i<=row_num;i++)
	{
		if(document.getElementById('row_id_'+i).innerHTML == '')
		{
		}
		else
		{
			var row_id = document.getElementById('accountno_'+i).value;
			if(row_id=='')
			{
				alert("Please enter account no. or delete the row.");
				document.getElementById('accountno_'+i).focus();
				return;
			}
			
			/* Setting Array For DB Entry */
			var final_prod = {
				"accountid": (document.getElementById('recs_'+i).value),
				"accountno": (document.getElementById('accountno_'+i).value),
				"oldregcode": (document.getElementById('oldregcode_'+i).value),
				"newregcode": (document.getElementById('newregcode_'+i).value)
			};
			final_prods.push(final_prod);
		}
	}
	final_prods_json = JSON.stringify(final_prods);
	//alert(final_prods_json);
	
	if(submit_button_clicked=='1')
	{
		
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}


	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			//alert(xmlhttp.responseText);
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}

 	xmlhttp.open('POST', 'ajax/update_regbatchcode.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batchcodes='+fixEscape(final_prods_json));


}

function RECSelect(val,row)
{
	
	var row_id = row.split("_").pop();
	
	

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			//alert(xmlhttp.responseText);
			document.getElementById('recs_'+row_id).value = output["accountid"];
			document.getElementById('oldregcode_'+row_id).value = output["reg_code"];;
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_regbatchcode.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('account_no='+fixEscape(val));
}

var KTSelect2 = {};
$( document ).ready(function() {
    $('#newregcode_1, #newregcode_1_validate').select2({placeholder:"Select New Batch Code"});
});
/*
	if(kt_select2A_1=='0')
	{
		alert('Please Select New Registered BatchCode');
		return;
	}*/
</script>