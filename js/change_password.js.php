<script type="text/javascript">
    function Validate() {
        var password = document.getElementById("new_pass").value;
        var confirmPassword = document.getElementById("re_new_pass").value;

		if (password != confirmPassword) {
            alert("Passwords do not match.Please Check!");
			document.getElementById("re_new_pass").value="";
			document.getElementById("re_new_pass").focus();
            return false;
        }
	}
</script>