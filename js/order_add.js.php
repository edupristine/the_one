<script>
row_num = document.getElementById("rows_div").getElementsByTagName("tr").length;
samplerow = document.getElementById('rows_div').innerHTML;
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function AddSKU(){
	row_num++;
	var newstring='_'+row_num
	appendcontent = samplerow.replace(/_1/g,newstring); /* if you want all the "_1"'s in the string to be replaced	*/
	$('#rows_div').append(appendcontent);
	document.getElementById('sr_'+row_num).value = row_num;
	// $("#productname_"+row_num).chosen();
}

function DelSKU(row_id){
	var r=confirm("Are you sure you wish to delete the SKU?");
	if (r==true) {
		document.getElementById(row_id).innerHTML = '';
	}
	else {
		return;
	}
}
final_prods_json = '';
submit_button_clicked = '';
function SaveOrder(){
	
	for (j=1;j<=row_num;j++)
	{
		if(document.getElementById('row_id_'+j).innerHTML == '')
		{
		}
		else
		{
			amt = document.getElementById('amt_'+j).value;
			if(amt == 0)
			{
				alert("You Cannot have a SKU with 0 Amount.");
				return;
			}	
		}
			
	}
	
	
	var ord_num = document.getElementById('ord_num').value;
	var ref_num = document.getElementById('ref_num').value;
	var ord_date = document.getElementById('ord_date').value;
	var exp_rec_date = document.getElementById('exp_rec_date').value;
	
	var ord_amount = document.getElementById('ord_amount').value;
	var kt_select2_1 = document.getElementById('kt_select2_1').value;
	var comments = document.getElementById('comments').value;
	
	if(kt_select2_1=='0')
	{
		alert('Please Select Vendor');
		return;
	}

	if(ord_amount==0)
	{
		alert("Can not save order with ZERO amount. Add some products");
		return;
	}
	
	final_prods = [];
	for (var i=1;i<=row_num;i++)
	{
		if(document.getElementById('row_id_'+i).innerHTML == '')
		{
		}
		else
		{
			
			var sku_id = document.getElementById('skuname_'+i).value;
			if(sku_id=='')
			{
				alert("Select a product or delete the row.");
				document.getElementById('skuname_'+i).focus();
				return;
			}
			//var product_info = [];
			//product_info = JSON.parse(product_info_json);
			/* Setting Array For DB Entry */
			var final_prod = {
				"skuname": (document.getElementById('skuname_'+i).value),
				"qty": (document.getElementById('qty_'+i).value),
				"rate": (document.getElementById('rate_'+i).value),
				"amt": (document.getElementById('amt_'+i).value)
			};
			final_prods.push(final_prod);
		}
	}
	final_prods_json = JSON.stringify(final_prods);
	//alert(final_prods_json);
	/* START - Checking if sale entry is already in progress */
	if(submit_button_clicked=='1')
	{
		//alert('Sale saving is in progress...');
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	/* END - Checking if sale entry is already in progress */

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else if(output.status == 'duplicate')
			{
				alert(output.message);
				submit_button_clicked = '';
				window.open('order_list.php','_blank');
			}
			else if(output.status == 'dateinvalid')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}
	xmlhttp.open('POST', 'ajax/order_add.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('skus='+fixEscape(final_prods_json)+'&ord_num='+fixEscape(ord_num)+'&ref_num='+fixEscape(ref_num)+'&ord_date='+fixEscape(ord_date)+'&exp_rec_date='+fixEscape(exp_rec_date)+'&ord_amount='+fixEscape(ord_amount)+'&kt_select2_1='+fixEscape(kt_select2_1)+'&comments='+fixEscape(comments));
}
function RefreshSKURow(row_id,source)
{
	//alert(document.getElementById('batch-radios_'+row_id));
	var row_id = row_id.split("_").pop();
	
	if(source == 'qty')
	{
		document.getElementById('amt_'+row_id).value = (Number(document.getElementById('rate_'+row_id).value) * Number(document.getElementById('qty_'+row_id).value)).toFixed(2);		
	}
	if(source == 'rate')
	{
		document.getElementById('amt_'+row_id).value = (Number(document.getElementById('rate_'+row_id).value) * Number(document.getElementById('qty_'+row_id).value)).toFixed(2);
	}
	
	if(source == 'amt')
	{
		document.getElementById('rate_'+row_id).value = (Number(document.getElementById('amt_'+row_id).value) / Number(document.getElementById('qty_'+row_id).value));
	}
	
}
function SKUSelect(sku,row)
{
	//row_id = sku.id;
	var row_id = row.split("_").pop();
	document.getElementById('amt_'+row_id).value = 1;
	document.getElementById('rate_'+row_id).value = 1;
	document.getElementById('qty_'+row_id).value = 1;
}
</script>