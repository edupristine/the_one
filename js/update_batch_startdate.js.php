<script>
row_num = document.getElementById("rows_div").getElementsByTagName("tr").length;
samplerow = document.getElementById('rows_div').innerHTML;
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function AddREC(){
	row_num++;
	var newstring='_'+row_num
	appendcontent = samplerow.replace(/_1/g,newstring); /* if you want all the "_1"'s in the string to be replaced	*/
	
	$('#rows_div').append(appendcontent);
	document.getElementById('sr_'+row_num).value = row_num;
	
	$('#newregcode_'+row_num+', #newregcode_'+row_num+'_validate').select2({placeholder:"Select New Batch Code"});
	// $("#productname_"+row_num).chosen();
}

function DelREC(row_id){
	var r=confirm("Are you sure you wish to delete this Record?");
	if (r==true) {
		document.getElementById(row_id).innerHTML = '';
	}
	else {
		return;
	}
}
final_prods_json = '';
submit_button_clicked = '';
function SaveBatchstartdate(){
	
	for (j=1;j<=row_num;j++)
	{
		if(document.getElementById('row_id_'+j).innerHTML == '')
		{
		}
		else
		{
			/*sel = document.getElementById('newstartdate_'+j).value;
			if(sel == '')
			{
				alert("Please enter a new start date for the batch");
				return;
			}*/	
		}
			
	}
	
	

	

	
	final_prods = [];
	for (var i=1;i<=row_num;i++)
	{
		if(document.getElementById('row_id_'+i).innerHTML == '')
		{
		}
		else
		{
			var row_id = document.getElementById('newregcode_'+i).value;
			if(row_id=='')
			{
				alert("Please Select a Batch.");
				document.getElementById('newregcode_'+i).focus();
				return;
			}
			
			/* Setting Array For DB Entry */
			var final_prod = {
				"batch_code": (document.getElementById('recs_'+i).value),
				"newregcode": (document.getElementById('newregcode_'+i).value),
				"oldstartdate": (document.getElementById('oldstartdate_'+i).value),
				"newstartdate": (document.getElementById('newstartdate_'+i).value),
				"status": (document.getElementById('status_'+i).value),
				"comment": (document.getElementById('comment_'+i).value)
				
			};
			final_prods.push(final_prod);
		}
	}
	final_prods_json = JSON.stringify(final_prods);
	
	
	if(submit_button_clicked=='1')
	{
		
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}


	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}

 	xmlhttp.open('POST', 'ajax/update_batch_startdate.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batch_startdate='+fixEscape(final_prods_json));


}


final_prods_json = '';
submit_button_clicked = '';
function Nochangebatch(){
	
   final_prods = [];

	
	var final_prod = {
	"batch_startdate": "Nochange"
    };
	final_prods_json = JSON.stringify(final_prods);
	
	
	if(submit_button_clicked=='1')
	{
		
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}


	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}
 	xmlhttp.open('POST', 'ajax/update_nochange_batch.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batch_startdate='+fixEscape(final_prods_json));


}



final_prods_json = '';
submit_button_clicked = '';
function Tentativebatchplan(){
	
   final_prods = [];

	
	var final_prod = {
	"batch_startdate": "Nochange"
    };
	final_prods_json = JSON.stringify(final_prods);
	
	
	if(submit_button_clicked=='1')
	{
		
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}


	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}
 	xmlhttp.open('POST', 'legacySystem/weekendbatchsummary_one.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batch_startdate='+fixEscape(final_prods_json));


}


final_prods_json = '';
submit_button_clicked = '';
function Existingbatches(){
	
   final_prods = [];
	
	var final_prod = {
	"batch_startdate": "Exiting",
	"month": (document.getElementById('month').value),
	"year": (document.getElementById('year').value)
    };
	
	final_prods.push(final_prod);	
	
	final_prods_json = JSON.stringify(final_prods);
	
	
	if(submit_button_clicked=='1')
	{
		
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}


	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			
			var output = [];
			
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}
 	xmlhttp.open('POST', 'ajax/update_batch_startdate_monthwise.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batch_startdate='+fixEscape(final_prods_json));
    

}

function RECSelect(val,row)
{
	
	var row_id = row.split("_").pop();
	
	

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			output = JSON.parse(xmlhttp.responseText);
			
			document.getElementById('recs_'+row_id).value = output["reg_code"];
			document.getElementById('oldstartdate_'+row_id).value = output["start_date"];;
		}
	}

	xmlhttp.open('POST', 'ajax/fetch_batch_startdate.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('batch_code='+fixEscape(val));
}

var KTSelect2 = {};
$( document ).ready(function() {
    $('#newregcode_1, #newregcode_1_validate').select2({placeholder:"Select New Batch Code"});
});
/*
	if(kt_select2A_1=='0')
	{
		alert('Please Select New Registered BatchCode');
		return;
	}*/
</script>