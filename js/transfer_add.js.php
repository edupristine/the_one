<script>
row_num = document.getElementById("rows_div").getElementsByTagName("tr").length;
samplerow = document.getElementById('rows_div').innerHTML;
function fixEscape(str)
{
    return escape(str).replace( "+", "%2B" );
}
function AddSKU(){
	row_num++;
	var newstring='_'+row_num
	appendcontent = samplerow.replace(/_1/g,newstring); /* if you want all the "_1"'s in the string to be replaced	*/
	$('#rows_div').append(appendcontent);
	document.getElementById('sr_'+row_num).value = row_num;
	// $("#productname_"+row_num).chosen();
}

function DelSKU(row_id){
	var r=confirm("Are you sure you wish to delete the SKU?");
	if (r==true) {
		document.getElementById(row_id).innerHTML = '';
	}
	else {
		return;
	}
}
final_prods_json = '';
submit_button_clicked = '';
function SaveTransfer(){
	
	for (j=1;j<=row_num;j++)
	{
		if(document.getElementById('row_id_'+j).innerHTML == '')
		{
		}
		else
		{
			qty = document.getElementById('qty_'+j).value;
			if(qty == 0)
			{
				alert("You Cannot have a SKU with 0 quantity.");
				return;
			}	
		}
			
	}
	
	
	var transfer_no = document.getElementById('transfer_no').value;
	var transfer_date = document.getElementById('transfer_date').value;
	var transfer_courier = document.getElementById('transfer_courier').value;
	var courier_track_no = document.getElementById('courier_track_no').value;
	
	var transfer_charges = document.getElementById('transfer_charges').value;
	if(transfer_charges == '')
	{
		transfer_charges = 0;
	}
	var kt_select2_1 = document.getElementById('kt_select2_1').value;
	var transfer_status = document.getElementById('transfer_status').value;
	
	if(kt_select2_1=='0')
	{
		alert('Please Select Location');
		return;
	}

	
	final_prods = [];
	for (var i=1;i<=row_num;i++)
	{
		if(document.getElementById('row_id_'+i).innerHTML == '')
		{
		}
		else
		{
			
			var sku_id = document.getElementById('skuname_'+i).value;
			if(sku_id=='')
			{
				alert("Select a product or delete the row.");
				document.getElementById('skuname_'+i).focus();
				return;
			}
			//var product_info = [];
			//product_info = JSON.parse(product_info_json);
			/* Setting Array For DB Entry */
			var final_prod = {
				"skuname": (document.getElementById('skuname_'+i).value),
				"qty": (document.getElementById('qty_'+i).value)
			};
			final_prods.push(final_prod);
		}
	}
	final_prods_json = JSON.stringify(final_prods);
	//alert(final_prods_json);
	/* START - Checking if sale entry is already in progress */
	if(submit_button_clicked=='1')
	{
		//alert('Sale saving is in progress...');
		return;
	}
	else
	{
		submit_button_clicked = '1';
	}
	/* END - Checking if sale entry is already in progress */

	if (window.XMLHttpRequest) {
		xmlhttp = new XMLHttpRequest();
	}
	else {
		xmlhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var output = [];
			//alert(xmlhttp.responseText);
			output = JSON.parse(xmlhttp.responseText);
			if(output.status == 'success')
			{
				alert(output.message);
				location.reload();
			}
			else if(output.status == 'insufficient_qty')
			{
				alert(output.message+" for "+output.sku_name);
				submit_button_clicked = '';
			}
			else if(output.status == 'db_error')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else if(output.status == 'duplicate')
			{
				alert(output.message);
				submit_button_clicked = '';
				window.open('transfer_list.php','_blank');
			}
			else if(output.status == 'dateinvalid')
			{
				alert(output.message);
				submit_button_clicked = '';
			}
			else
			{
				alert(output.message);
				submit_button_clicked = '';
			}
		}
	}

	xmlhttp.open('POST', 'ajax/transfer_add.php', true);
	xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlhttp.send('skus='+fixEscape(final_prods_json)+'&transfer_no='+fixEscape(transfer_no)+'&transfer_date='+fixEscape(transfer_date)+'&transfer_courier='+fixEscape(transfer_courier)+'&courier_track_no='+fixEscape(courier_track_no)+'&transfer_charges='+fixEscape(transfer_charges)+'&kt_select2_1='+fixEscape(kt_select2_1)+'&transfer_status='+fixEscape(transfer_status));


}

function SKUSelect(sku,row)
{
	//row_id = sku.id;
	var row_id = row.split("_").pop();
	document.getElementById('qty_'+row_id).value = 1;
}
</script>