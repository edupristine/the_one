<?php

//echo "My ID:-".$_SESSION['USER_ID'];
$display=0;
$select_faculty = "SELECT user_id FROM users_teams WHERE team_id = 2 ";
$result_fac = Select($select_faculty,$conn);
foreach($result_fac['rows'] as $faculty)
{
if($faculty['user_id']==$_SESSION['USER_ID'])	
{
$display=1;	
}
}

//for marketing
$displayM=0;
$select_facultyM = "SELECT user_id FROM users_teams WHERE team_id = 9 ";
$result_facM = Select($select_facultyM,$conn);
foreach($result_facM['rows'] as $facultyM)
{
if($facultyM['user_id']==$_SESSION['USER_ID'])	
{
$displayM=1;	
}
}


//for sales
$displaySM=0;
$select_facultyM = "SELECT user_id FROM users_teams WHERE team_id = 12 ";
$result_facM = Select($select_facultyM,$conn);
foreach($result_facM['rows'] as $facultyM)
{
if($facultyM['user_id']==$_SESSION['USER_ID'])	
{
$displaySM=1;	
}
}


//for refunds
$displayA=0;
$displaySc=0;
$displayASc=0;
$select_facultyA = "SELECT user_id,team_id FROM users_teams WHERE team_id in (1,7,8,10,11,2) ";
$result_facA = Select($select_facultyA,$conn);
foreach($result_facA['rows'] as $facultyA)
{
if($facultyA['user_id']==$_SESSION['USER_ID'])	
{
$displayA=1;

if(($facultyA['team_id']=="1")||($facultyA['team_id']=="7"))		
{
$displaySc=1;
}

if(($facultyA['team_id']=="11")||($facultyA['team_id']=="7"))		
{
$displayASc=1;
}


}
}

?>
<div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
						<div class="kt-aside__brand-logo">
							<a href="dashboard.php">
								<img alt="Logo" src="./assets/media/logos/logo-light.png" />
							</a>
						</div>
						<div class="kt-aside__brand-tools">
							<button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
											<path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
										</g>
									</svg></span>
								<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
										<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
											<polygon id="Shape" points="0 0 24 0 24 24 0 24" />
											<path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
											<path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
										</g>
									</svg></span>
							</button>

							<!--
			<button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
			-->
						</div>
					</div>
					<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
						<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
						
							<ul class="kt-menu__nav ">
							<?php
														if(($_SESSION['USER_ID']==76)||($_SESSION['USER_ID']==8))
														{
														?>
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Placements</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="location_master.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Location Master</span></a></li>
                                                         <li class="kt-menu__item " aria-haspopup="true"><a href="add_jobs.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Add Jobs</span></a></li>
											             <li class="kt-menu__item " aria-haspopup="true"><a href="job_posting_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Jobs Postings</span></a></li>
														  <li class="kt-menu__item " aria-haspopup="true"><a href="job_posting_scrutiny.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Jobs Applications</span></a></li>
														  <li class="kt-menu__item " aria-haspopup="true"><a href="job_externals.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">External Jobs Visit </span></a></li>
														</ul>
														</div>
														</li>
														
														
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Articles</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
														
                                                         <li class="kt-menu__item " aria-haspopup="true"><a href="add_website_article.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Add Articles</span></a></li>
											             <li class="kt-menu__item " aria-haspopup="true"><a href="article_posting_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Articles Postings</span></a></li>
														</ul>
														</div>
														</li>
														<?php
														}
														?>
							
							
								<?php
								if(($displayM==0))
								{
									if(($_SESSION['USER_ID'] != "41"))
															{
															
																	if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "29"))
															{
																
																if(($_SESSION['USER_ID'] != "44"))
															{
																
															if(($_SESSION['USER_ID'] != "77"))
															{	
																
																if(($_SESSION['USER_ID'] != "45"))
															{
																
																if(($_SESSION['USER_ID'] != "46"))
															{
																if(($_SESSION['USER_ID'] != "76"))
															{
																if(($_SESSION['USER_ID'] != "78"))
															{	
																if(($displaySM==0))
																{
								?>
								<li class="kt-menu__item " aria-haspopup="true"><a href="dashboard.php" class="kt-menu__link "><i class="kt-menu__link-icon flaticon-home"></i><span class="kt-menu__link-text">Dashboard</span></a></li>
								<li class="kt-menu__section ">
									<h4 class="kt-menu__section-text">INVENTORY</h4>
									<i class="kt-menu__section-icon flaticon-more-v2"></i>
								</li>
								<?php
															}
															}
															}
															}
															}}
															}}
								
															}
															
															}
								}
								?>
								
								<?php
								
									if(($_SESSION['U_LOCATION_TYPE'] == "STOCKING UNIT"))
									{
										if($display==0)
														{
															
															if(($displayM==0))
															{
																if(($_SESSION['USER_ID'] != "41"))
															{
																if(($_SESSION['USER_ID'] != "42"))
															{
																	if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "44"))
															{
																if(($_SESSION['USER_ID'] != "77"))
															{
																if(($_SESSION['USER_ID'] != "78"))
															{	
																if(($_SESSION['USER_ID'] != "45"))
															{
																if(($_SESSION['USER_ID'] != "46"))
															{
															if(($_SESSION['USER_ID'] != "29"))
															{
															if($displaySM==0)	
															{
								?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Masters</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Applications</span></span></li>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">SKUs</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="sku_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">SKU Add</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="sku_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">SKU List</span></a></li>
														
														
														<!--<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Profile 1</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
															<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
																<ul class="kt-menu__subnav">
																	<li class="kt-menu__item " aria-haspopup="true"><a href="demo1/custom/apps/user/profile-1/overview.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Overview</span></a></li>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="demo1/custom/apps/user/profile-1/personal-information.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Personal Information</span></a></li>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="demo1/custom/apps/user/profile-1/account-information.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Account Information</span></a></li>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="demo1/custom/apps/user/profile-1/change-password.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Change Password</span></a></li>
																	<li class="kt-menu__item " aria-haspopup="true"><a href="demo1/custom/apps/user/profile-1/email-settings.html" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Email Settings</span></a></li>
																</ul>
															</div>
														</li>-->
														
													</ul>
												</div>
											</li>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Vendors</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="vendor_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Vendor Add</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="vendor_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Vendor List</span></a></li>
														
														
													</ul>
												</div>
											</li>
											<!--<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Centers</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="center_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Centers Add</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="center_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Centers List</span></a></li>
														
														
													</ul>
												</div>
											</li>	

											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Classrooms</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="classroom_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Classroom Add</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="classroom_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Classroom List</span></a></li>
														
														
													</ul>
												</div>
											</li>-->										
											
										</ul>
									</div>
								</li>
															<?php
															}
															}
															}
															}
															}
															}
															}
															}                     }
															}
															}								
														}
									}
									if($_SESSION['USER_ID'] != "71")
								{	
								if($_SESSION['USER_ID'] != "76")
								{	
if(($_SESSION['USER_ID'] != "8"))
															{							
									
									?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Day to Day</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Applications</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="student_validity_checker.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Students Validity Checker</span></a></li>
											
											<?php
								}
								}
												if($_SESSION['U_LOCATION_TYPE'] == "STOCKING UNIT")
												{
													if($display==0)
														{
															if(($displayM==0))
															{
																if(($_SESSION['USER_ID'] != "41"))
															{
																
																if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "29"))
															{
                                                                if(($_SESSION['USER_ID'] != "44"))
															{
																if(($_SESSION['USER_ID'] != "77"))
															{
																if(($_SESSION['USER_ID'] != "78"))
															{
                                                                if(($_SESSION['USER_ID'] != "45"))
															{	
                                                                 if(($_SESSION['USER_ID'] != "46"))
															{
                                                            if($displaySM==0)
															{																
											?>
											<?php
											if(($_SESSION['USER_ID'] == "84")||($_SESSION['USER_ID'] == "85"))	{
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Orders</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="order_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Order Add</span></a></li>

														<li class="kt-menu__item " aria-haspopup="true"><a href="order_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Order List</span></a></li>
																												
													</ul>
												</div>
											</li>
											<?php
											}
											?>
											
															<?php 
															}
															}
															}
															}
															}
															}
															}
															} 
															}
															}
														}
												}
												?>
														<?php
														
														if($display==1)
														{
															if(($displayM==0)||($displaySM==0))
															{
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Workshops</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="workshop_schedule.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Create Schedule</span></a></li>

											
																					
														</ul>
														</div>
														</li>
														
														
														
														
														
														
														<?php
														
														if(($_SESSION['USER_ID']==14)||($_SESSION['USER_ID']==15)||($_SESSION['USER_ID']==79))
														{
														?>
														
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Mailers</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="send_crm_mail.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Bulk Mailer</span></a></li>

											
																					
														</ul>
														</div>
														</li>
							

														<?php
														}
														}
														}
														?>
														
														<?php
														
														if(($_SESSION['USER_ID']==76)||($_SESSION['USER_ID']==8))
														{
														?>
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Mailers</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
														<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
														<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="send_crm_mail.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Bulk Mailer</span></a></li>

											
																					
														</ul>
														</div>
														</li>
														<?php
														}
														?>
														
														<?php
														
														if(($_SESSION['USER_ID'] == "41")||($_SESSION['USER_ID'] == "44")||($_SESSION['USER_ID'] == "77")||($_SESSION['USER_ID'] == "45")||($_SESSION['USER_ID'] == "46")||($_SESSION['USER_ID'] == "78"))
															{
																
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="offline_sitechat_details.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Offline SiteChat Leads</span></a>
																
																</li>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="update_sitechat_details.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Update SiteChat Details</span></a>
														
														</li>
													
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="push_sitechat_leads.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Push SiteChat Leads</span></a>
																	
																	</li>
																	
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="sitechat_details_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">SiteChat Leads List</span></a>
																	
																	</li>			

														<?php
														}
															
														?>
														
														
														
														
														
														<?php
														
														if(($_SESSION['USER_ID'] == "42")||($_SESSION['USER_ID'] == "71")||($_SESSION['USER_ID'] == "39"))
															{
																
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="upsert_lead.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Upsert Leads</span></a>
																
																</li>
																
																<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="upsert_bulk_leads.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Upsert Bulk Leads</span></a>
																
																</li>
																
														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="create_batch.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Create Batch</span></a>
																
																</li>	
																<?php
														
														if(($_SESSION['USER_ID'] == "42")||($_SESSION['USER_ID'] == "71")||($_SESSION['USER_ID'] == "39"))
															{
																
														?>
<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="wp_leads.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Leads Report</span></a>
																
																</li>																	

												
														<?php
															}
														}
															
														?>
														
														<?php
														
														if(($_SESSION['USER_ID'] == "26")||($_SESSION['USER_ID'] == "29")||($_SESSION['USER_ID'] == "87"))
															{
																
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="update_batch_startdate.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Update Batch StartDates</span></a>
																
																</li>
																
																<li class="kt-menu__item " aria-haspopup="true"><a href="update_regbatchcode.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Registered Batch Code</span></a></li>
																

												
														<?php
														}
															
														?>
														
														
														<?php
														
														if(($displaySM == 1))
															{
																if(($_SESSION['USER_ID'] != "8"))
															{
																
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="student_paydetails_mod.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Update Transaction Details Request</span></a>
																
																</li>
																<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="transaction_modreq_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Trans. Mod. Request List</span></a>
																
																</li>

												
														<?php
														}
															}
															
														?>
														
														
														
														<?php
														
														if($display==0)
														{
															if(($displayM==1)||($displaySM==0))
															{
																 if($_SESSION['USER_ID'] != "76")
																 
								{
									
							
														if(($_SESSION['USER_ID'] != "84")||($_SESSION['USER_ID'] != "85"))	{
													
														?>

														<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="push_mark_leads.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Push Mark Leads</span></a>
														
														</li>
							

														<?php
														}
								}
															}
														}
														?>
											
											
											<?php
											if($display==0)
											{
												if(($displayM==0))
												{
													if(($_SESSION['USER_ID'] != "41"))
															{
															
																	if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "44"))
															{
																if(($_SESSION['USER_ID'] != "77"))
															{	
																if(($_SESSION['USER_ID'] != "78"))
															{	
																if(($_SESSION['USER_ID'] != "45"))
															{
																if(($_SESSION['USER_ID'] != "46"))
															{
																if($displaySM==0)
														   {
															    if($_SESSION['USER_ID'] != "76")
								{
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Transfers</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													<?php
														if($_SESSION['U_LOCATION_TYPE'] == "STOCKING UNIT")
														{
														
													?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="transfer_add.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Transfer Add</span></a></li>
														<?php
														}														 
														if($_SESSION['U_LOCATION_TYPE'] == "STOCKING UNIT" || $_SESSION['U_LOCATION_TYPE'] == "CENTER")	{	
														
														?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="transfer_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Transfer List</span></a></li>
														<?php } ?>
														
													</ul>
												</div>
											</li>
															<?php
															}
															}
															}
															}
															}
															} 
															}
															}
											} 
															}
												}
											}
											?>
											
											
											
											
										    <?php
												if($_SESSION['U_LOCATION_TYPE'] == "STOCKING UNIT")
												{
													if($display==0)
														{
															if(($displayM==0))
															{
																if(($_SESSION['USER_ID'] != "41"))
															{
															
																	if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "29"))
															{
																if(($_SESSION['USER_ID'] != "44"))
															{
															    if(($_SESSION['USER_ID'] != "77"))
															{
																if(($_SESSION['USER_ID'] != "78"))
															{																
																if(($_SESSION['USER_ID'] != "45"))
															{
																if(($_SESSION['USER_ID'] != "46"))
															{
															if($displaySM==0)
															{
											?>
											<?php
											if(($_SESSION['USER_ID'] == "84")||($_SESSION['USER_ID'] == "85"))	{
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Couriers</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="send_courier.php?status=Pending" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Send Couriers</span></a></li>
                                                        <li class="kt-menu__item " aria-haspopup="true"><a href="send_courier_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Couriers List</span></a></li>
                                                </ul>
												</div>
											</li>
											
											
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Faculties</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="faculty_sku.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Book Issuance</span></a></li>
                                                        
                                                </ul>
												</div>
											</li>
											
											
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Skus</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="threshold_sku.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Skus Threshold</span></a></li>
                                               </ul>
												</div>
											</li>
											<?php
											}
											?>
											
											
															<?php }} }
															}
															}
															}
															}
															}  }
															}
														}
												}?>
												
											<?php
												if(($_SESSION['U_LOCATION_TYPE'] == "CENTER")||($_SESSION['USER_ID'] == "42"))
												{
													
													 if($_SESSION['USER_ID'] != "76")
								{
									
									if(($_SESSION['USER_ID'] != "8"))
															{
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Issuance</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
														<li class="kt-menu__item " aria-haspopup="true"><a href="issue_books.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Issue Books</span></a></li>
														<!--<li class="kt-menu__item " aria-haspopup="true"><a href="student_pre.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Previous Students</span></a></li>-->
														
														 <li class="kt-menu__item " aria-haspopup="true"><a href="report_students_issuance.php?reporttype=livestudents" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Issuance Report</span></a></li>
														 
														  <li class="kt-menu__item " aria-haspopup="true"><a href="expired_sku.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Expired Skus</span></a></li>
														<!--<li class="kt-menu__item " aria-haspopup="true"><a href="transfer_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Issuance List</span></a></li>-->
													</ul>
												</div>
											</li>
												<?php 
								}
								}
												} 
											if(($_SESSION['USER_ID'] != "42")||($_SESSION['USER_ID'] != "39"))	
											//if(($_SESSION['U_LOCATION_TYPE'] != "STOCKING UNIT" && $_SESSION['U_LOCATION_TYPE'] != "CENTER")||($_SESSION['USER_ID'] != "42"))
												{
													 if($_SESSION['USER_ID'] != "76")
								{
									if(($_SESSION['USER_ID'] != "8"))
															{
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Raise Requirement</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													    <?php
														if(($_SESSION['USER_ID'] == "81")||($_SESSION['USER_ID'] == "70")||($_SESSION['USER_ID'] == "14")||($_SESSION['USER_ID'] == "30")||($_SESSION['USER_ID'] == "15")||($_SESSION['USER_ID'] == "79")||($_SESSION['USER_ID'] == "73")||($_SESSION['USER_ID'] == "31")||($_SESSION['USER_ID'] == "88"))	{
														?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="raise_requirement.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Raise Requirement</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="book_couriers.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Print Courier Labels</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="return_courier_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Returned Couriers list</span></a></li>
														<!--<li class="kt-menu__item " aria-haspopup="true"><a href="prev_student_rr.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Previous Students</span></a></li>-->
														<li class="kt-menu__item " aria-haspopup="true"><a href="update_students.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Change Location</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="student_validity.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Extend Student Validity</span></a></li>
														<?php
														}
														if(($_SESSION['USER_ID'] == "84")||($_SESSION['USER_ID'] == "85"))	{
														?>
														
														<li class="kt-menu__item " aria-haspopup="true"><a href="send_courier_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Couriers List</span></a></li>
														<?php
														}
														?>
														
														<li class="kt-menu__item " aria-haspopup="true"><a href="report_students_issuance.php?reporttype=livestudents" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Issuance Report</span></a></li>
														
														
													   <!-- <li class="kt-menu__item " aria-haspopup="true"><a href="send_schedule_mail.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Email Schedules</span></a></li>-->
														<!--<li class="kt-menu__item " aria-haspopup="true"><a href="selected_batchcourses.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Selected Batches</span></a></li>-->
													<!--<li class="kt-menu__item " aria-haspopup="true"><a href="rec_concern.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Raise Concern</span></a></li>-->
													<li class="kt-menu__item " aria-haspopup="true"><a href="report_sku_location.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">SKU Report</span></a></li>
													
													<?php
														if(($_SESSION['USER_ID'] == "84")||($_SESSION['USER_ID'] == "85"))	{
														?>
                                                    <li class="kt-menu__item " aria-haspopup="true"><a href="send_courier.php?status=Pending" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Send Couriers</span></a></li>
                                                    <?php
														}
														?>    													
													<!--<li class="kt-menu__item " aria-haspopup="true"><a href="transfer_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Issuance List</span></a></li>-->
										
													</ul>
												</div>
											</li>
											
											
											
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Exam Details</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													    <?php
														if(($_SESSION['USER_ID'] == "81")||($_SESSION['USER_ID'] == "70")||($_SESSION['USER_ID'] == "14")||($_SESSION['USER_ID'] == "30")||($_SESSION['USER_ID'] == "15")||($_SESSION['USER_ID'] == "79"))	{
														?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="student_exam_details_raw_og.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Inhouse Courses </span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="exam_details_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Completed Exams</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="exam_details_list_upcoming.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Upcoming Exams</span></a></li>
														<li class="kt-menu__item " aria-haspopup="true"><a href="exam_result_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Submitted Data</span></a></li>
														<?php
														}
														?>
														
														
													</ul>
												</div>
											</li>
											
											
												<?php }}} ?>
												
												
											<?php
											if($displayA=="1")
											{												
											?>
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Refunds</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													    <?php
														if(($displaySc=="1")||($_SESSION['USER_ID'] == "43")||($_SESSION['USER_ID'] == "80"))
														{
											             ?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="student_refunds.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Raise Request</span></a></li>
														<?php
														}
														?>
														
														<li class="kt-menu__item " aria-haspopup="true"><a href="refund_process_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Refund Process List</span></a></li>
														<?php
														if(($displayASc=="1"))
														{
														?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="refund_final_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Refund Final List</span></a></li>
                                                         <?php
														}
														?>
										
													</ul>
												</div>
											</li>
											
											
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Refferal</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													    
														
														<li class="kt-menu__item " aria-haspopup="true"><a href="referral_process_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Referral Process List</span></a></li>
														<?php
														if($displayASc=="1")
														{
														?>
														<li class="kt-menu__item " aria-haspopup="true"><a href="referral_final_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Referral Final List</span></a></li>
                                                         <?php
														}
														?>
										
													</ul>
												</div>
											</li>
											
											
											
											<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">Schedules</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
												<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
													<ul class="kt-menu__subnav">
													    
														
														<li class="kt-menu__item " aria-haspopup="true"><a href="student_scheduler.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Send Mails</span></a></li>
														
										                <li class="kt-menu__item " aria-haspopup="true"><a href="student_scheduler_resend.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Resend Mails</span></a></li>
													</ul>
												</div>
											</li>
											<?php
											}
                                            ?>											
												
												
												
										</ul>
									</div>
								</li>
								<?php
								}
								if($display==0)
								{
									if(($displayM==0))
									{
										if(($_SESSION['USER_ID'] != "41"))
															{
															
																	if(($_SESSION['USER_ID'] != "26"))
															{
																if(($_SESSION['USER_ID'] != "44"))
															{
																if(($_SESSION['USER_ID'] != "77"))
															{
																if(($_SESSION['USER_ID'] != "78"))
															{	
																if(($_SESSION['USER_ID'] != "45"))
															{
																if(($_SESSION['USER_ID'] != "46"))
															{
															if($displaySM==0)
															{
																
																if($_SESSION['USER_ID'] != "71")
								{

                                 if($_SESSION['USER_ID'] != "76")
								{									
								?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Reports</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_sku_location.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">SKU Report</span></a></li>
											<!--<li class="kt-menu__item " aria-haspopup="true"><a href="report_student_course.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Student Report</span></a></li>-->
										</ul>
									</div>
								</li><?php }}}}}
															}}	}					} }
									}
								}
								?>
								<?php
								if(($_SESSION['USER_ID'] == "1" || $_SESSION['USER_ID'] == "18"|| $_SESSION['USER_ID'] == "20"|| $_SESSION['USER_ID'] == "3"|| $_SESSION['USER_ID'] == "36"|| $_SESSION['USER_ID'] == "43"|| $_SESSION['USER_ID'] == "52"|| $_SESSION['USER_ID'] == "71"|| $_SESSION['USER_ID'] == "48" || $_SESSION['USER_ID'] == "50" || $_SESSION['USER_ID'] == "82" || $_SESSION['USER_ID'] == "80"))
								{
									
									if($display==0)
														{
															if(($displayM==0)||($displaySM==0))
															{
																
																
											if($_SESSION['USER_ID'] != "71")
								{					
											?>
								<li class="kt-menu__section ">
									<h4 class="kt-menu__section-text">BATCHES</h4>
									<i class="kt-menu__section-icon flaticon-more-v2"></i>
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Threshold</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="batch_threshold.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Threshold Report</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="batchcode_details.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Data Cleaning</span></a></li>
										</ul>
									</div>
								</li>
								
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Transactions</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_monthly_transaction.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Monthly Report</span></a></li>
										</ul>
									</div>
								</li>
								
								
								
							
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Payment grievances</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="rec_concern.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Grievances Form</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="rec_grev_list.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Grievances List</span></a></li>
										</ul>
									</div>
								</li>
								
							   <?php
								}
							   if(($_SESSION['USER_ID'] == "1" || $_SESSION['USER_ID'] == "3"|| $_SESSION['USER_ID'] == "20"|| $_SESSION['USER_ID'] == "43"|| $_SESSION['USER_ID'] == "36"|| $_SESSION['USER_ID'] == "52"|| $_SESSION['USER_ID'] == "71" || $_SESSION['USER_ID'] == "48" || $_SESSION['USER_ID'] == "50" || $_SESSION['USER_ID'] == "80" || $_SESSION['USER_ID'] == "82"))
							   {
							   ?>
									<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Update Student Details</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="update_offeredprice.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Offered Price</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="update_regbatchcode.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Registered Batch Code</span></a></li>
										     <li class="kt-menu__item " aria-haspopup="true"><a href="update_assignedto.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Assigned to Councelor</span></a></li>
											 <?php
											 if($_SESSION['USER_ID'] == "71"||$_SESSION['USER_ID'] == "1"||$_SESSION['USER_ID'] == "3")
											 {
											 ?>
											 <li class="kt-menu__item " aria-haspopup="true"><a href="upsert_lead.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Upsert Leads</span></a></li>
											 
											 <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="create_batch.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Create Batch</span></a>
											 
											 <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="upsert_bulk_leads.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Upsert Bulk Leads</span></a>
																
											</li>
											 <?php
											 }
											 ?>
										</ul>
									</div>
								</li>
								<?php
								if($_SESSION['USER_ID'] != "71")
								{
								?>
								<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Update Payment Details</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_daily_transaction_nochecked.php?param=0&reporttype=NOT_CHECKED" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Daily Transactions Not Checked</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_daily_transaction.php?param=0&reporttype=ALL" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Daily Transactions</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="summary_daily_transaction.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Transactions Summary</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="summary_daily_transaction_mail.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Consolidated Summary</span></a></li>
										</ul>
									</div>
								</li>


                                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon flaticon-web"></i><span class="kt-menu__link-text">Receivables</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
									<div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
										<ul class="kt-menu__subnav">
											<li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true"><span class="kt-menu__link"><span class="kt-menu__link-text">Utils</span></span></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_balance_payment.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Course Wise</span></a></li>
											<li class="kt-menu__item " aria-haspopup="true"><a href="report_balance_payment_citywise.php" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">City Wise</span></a></li>
	
										</ul>
									</div>
								</li>
								<?php
								}
							   }
								?>
								
								
								</li>
								
								
								<?php }
								}
								}								?>
								
								
								
							</ul>
							
						</div>
					</div>
					
<?php
?>					