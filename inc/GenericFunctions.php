<?php
/* DB Action Functions */
function NumberDispay($number,$decimal,$rsshow = 0)
{
		
}
function checklogin(){
	if( isset($_SESSION['USER_ID']) && !empty($_SESSION['USER_ID']))
	{	
		return true;
	}
	else
	{
		return false;
	}
}
function logout(){
	session_unset();
	session_destroy();
	unset($_SESSION['USER_NAME']);
	unset($_SESSION['U_NAME']);
	unset($_SESSION['USER_ID']);
	unset($_SESSION['U_LOCATION_ID']);
	unset($_SESSION['U_LOCATION_TYPE']);
	unset($_SESSION['U_LOCATION_NAME']);
	unset($_SESSION['WH_ID']);
	unset($_SESSION['COURIER_ID']);
	header('Location: index.php');
}
function getIndianCurrency($number)
{
    $decimal = round($number - ($no = floor($number)), 2) * 100;
    $hundred = null;
    $digits_length = strlen($no);
    $i = 0;
    $str = array();
    $words = array(0 => '', 1 => 'One', 2 => 'Two',
        3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
        7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
        10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
        13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
        16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
        19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
        40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
        70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
    $digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
    while( $i < $digits_length ) {
        $divider = ($i == 2) ? 10 : 100;
        $number = floor($no % $divider);
        $no = floor($no / $divider);
        $i += $divider == 10 ? 1 : 2;
        if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
        } else $str[] = null;
    }
    $Rupees = implode('', array_reverse($str));
	$Rupees = str_replace("Thousands","Thousand",$Rupees);
    $paise = ($decimal) ? ". " . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
    return ($Rupees ? 'Rupees: '.$Rupees : '') . $paise . ' Only';
}
function Insert($query,$conn,$tablename)
{
	$result = $conn->prepare($query);
	$result->execute();
	
	$output['id'] = $conn->lastInsertId();
	AfterInsert($tablename,$output['id']);
	$output['status'] = 'success';
	return $output;
}

function InsertWo($query,$conn,$tablename)
{
	$result = $conn->prepare($query);
	$result->execute();
	
	$output['id'] = $conn->lastInsertId();
	//AfterInsert($tablename,$output['id']);
	$output['status'] = 'success';
	return $output;
}

function InsertCreMod($query,$conn,$tablename)
{
	$result = $conn->prepare($query);
	$result->execute();
	
	$output['id'] = $conn->lastInsertId();
	AfterInsertCreMod($tablename,$output['id']);
	$output['status'] = 'success';
	return $output;
}

function Update($query,$conn)
{
	$result = $conn->prepare($query);
	$result->execute();
	$output['status'] = 'success';
	return $output;
}



function Del($query,$conn)
{
	$result = $conn->prepare($query);
	$result->execute();
	$output['status'] = 'success';
	return $output;
}

function Select($query,$conn)
{
	$result = $conn->prepare($query);
	$result->execute();
	$output['count'] = $result->rowCount();
	$result->setFetchMode(PDO::FETCH_ASSOC);
	$rows = $result->fetchAll();
	$output['rows'] = $rows;
	return $output;
}

function WriteToFile($filename,$var)
{
	$file = fopen($filename,"a");
	echo fwrite($file,$var);
	fclose($file);
}

/* Check OTP Hacking */
function CheckOTPHacking($mobile,$same_num_day_max,$same_num_hour_max,$this_hour_this_ip_max,$this_day_this_ip_max)
{
	global $ClientPublicIP;
	global $conn;
	
	$onedayback= date("Y-m-d H:i:s", time()-86400);
	$onehourback= date("Y-m-d H:i:s", time()-3600);
	$query1="SELECT * FROM `otp_log` WHERE `MOBILE`='".$mobile."' AND `SENT_TIME`>'".$onedayback."'";
	$result1 = Select($query1,$conn);
	$same_num_day = $result1['count'];

	$query2="SELECT * FROM `otp_log` WHERE `SENT_TIME`>'".$onehourback."' AND `CLIENT_IP`='".$ClientPublicIP."'";
	$result2 = Select($query2,$conn);
	$this_hour_this_ip = $result2['count'];

	$query4="SELECT * FROM `otp_log` WHERE `SENT_TIME`>'".$onedayback."' AND `CLIENT_IP`='".$ClientPublicIP."'";
	$result4 = Select($query4,$conn);
	$this_day_this_ip = $result4['count'];
	
	$query3="SELECT * FROM `otp_log` WHERE `MOBILE`='".$mobile."' AND `SENT_TIME`>'".$onehourback."'";
	$result3 = Select($query3,$conn);
	$same_num_hour = $result3['count'];
	
	if($same_num_day>=$same_num_day_max || $same_num_hour>=$same_num_hour_max || $this_hour_this_ip>=$this_hour_this_ip_max || $this_day_this_ip>=$this_day_this_ip_max)
	{
		return "stop";
		exit();
	}
	else
	{
		return "proceed";
		exit();
	}
}

/* OTP Verification */
function VerifyOTP($mobile,$otp)
{
	global $conn1;
	$verified = '0';
	$query="SELECT OTP FROM `otp_log` WHERE `MOBILE`='".$mobile."' AND SENT_TIME > (NOW() - INTERVAL 15 MINUTE) ORDER BY `ID` DESC";
	$result = Select($query,$conn1);
	foreach($result['rows'] as $row)
	{
		if ($otp == $row['OTP'])
		{
			$verified = '1';
		}
	}
	return $verified;
}
	
/* IP Details */
function get_client_ip()
{
	$ipaddress = '';
	/*if (isset($_SERVER['HTTP_CLIENT_IP']))
		$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
	else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_X_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
	else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
		$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
	else if(isset($_SERVER['HTTP_FORWARDED']))
		$ipaddress = $_SERVER['HTTP_FORWARDED'];*/
	if(isset($_SERVER['REMOTE_ADDR']))
		$ipaddress = $_SERVER['REMOTE_ADDR'];
	else
		$ipaddress = 'UNKNOWN';
	return $ipaddress;
}

function SendSMStoPeople(&$numbers,$sender_name,$message)
{
	$user="dinesh.choithani@standingcoin.com:workISworSHI";
	$senderID=$sender_name;
	$msgtxt = $message;
	$status_array=array();
	foreach($numbers as $number)
	{
		$ch = curl_init();
		$receipientno=$number;
		curl_setopt($ch, CURLOPT_URL,  "http://api.mVaayoo.com/mvaayooapi/MessageCompose");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "user=$user&senderID=$senderID&receipientno=$receipientno&msgtxt=$msgtxt");
		$buffer = curl_exec($ch);
		if(empty ($buffer))
		{
			//Sending Failed
			//echo "failed".$buffer.'/n'.$message;
			$status='failed';
		}
		else
		{
			// Sent
			//echo "success".$buffer.'/n'.$message;
			$status='success';
		}
		curl_close($ch);
		$this_attempt=array("number"=>$number,"message"=>$msgtxt,"status"=>$status,"buffer"=>$buffer);
		array_push($status_array,$this_attempt);
	}
	return $status_array;
}

/* Logs Error In DB If A Query Fails (Should Be Called After Every DB Operation In Else Part) */
function DBLogError($errno, $errstr,$errfile,$errline,$errcontext,$currentuserid)
{
	//echo "==".$_SESSION['logerror'];
	global $conn;
	$date_time=date("Y-m-d H:i:s");
	$errmsg=$conn->quote($errstr);
	$errcontext=$conn->quote($errcontext); 
	$query="INSERT INTO `error_db`(`file_name`, `line_no`, `error_message`, `error_description`,`error_type`,`CREATED_AT`) VALUES('".$errfile."','".$errline."',".$errmsg.",".$errcontext.",'DB_ERROR','".$date_time."')";
	  
		try
	{
		// Session variable used to avoid recursive loop when the query of DBLogError function fails
		if( !isset($_SESSION['logerror']) || ( isset($_SESSION['logerror']) && $_SESSION['logerror']==1 ) )
			{
			$result_insert=$conn->prepare($query);
			$result_insert->execute();
			$justinsertedid =$conn->lastInsertId();
			AfterInsert('error_db',$justinsertedid,$currentuserid);
		}
		else
		{	
			$_SESSION['logerror']=1;
		}
	}
	catch(PDOException $ex) 
	{
		$_SESSION['logerror']=0;
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$currentuserid);
	}
}

function AfterInsert($tablename,$rowid)
{
	global $conn;
	$currentuserid = $_SESSION['USER_ID'];
	if(!isset($_SESSION['USER_ID']))
	{
		$currentuserid = 0;
	}
	$date_time=date("Y-m-d H:i:s");
	$update_query=$conn->prepare("UPDATE $tablename SET created_by='$currentuserid',modified_by='$currentuserid',modified_count=0,created_at='$date_time'  WHERE id='$rowid'");
	try
	{
		$update_query->execute();
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$currentuserid);

	}
}

function AfterInsertCreMod($tablename,$rowid)
{
	global $conn;
	$currentuserid = $_SESSION['USER_ID'];
	if(!isset($_SESSION['USER_ID']))
	{
		$currentuserid = 0;
	}
	$date_time=date("Y-m-d H:i:s");
	$update_query=$conn->prepare("UPDATE $tablename SET created_at='$date_time'  WHERE id='$rowid'");
	try
	{
		$update_query->execute();
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$currentuserid);

	}
}

function AfterUpdate($tablename,$rowid)
{
	
	$date_time=date("Y-m-d H:i:s");
	global $conn;
	$currentuserid = $_SESSION['USER_ID'];
	if(!isset($_SESSION['USER_ID']))
	{
		$currentuserid = 0;
	}
	$query	=	"UPDATE `$tablename` SET modified_by='$currentuserid',modified_at='$date_time',modified_count=modified_count+1 WHERE id='$rowid'";
	//echo $query.'</br>';
	try
	{
		$result	=	$conn->prepare($query);
		$result->execute();
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$currentuserid);
	}
}


function AfterUpdateCreMod($tablename,$rowid)
{
	
	$date_time=date("Y-m-d H:i:s");
	global $conn;
	$currentuserid = $_SESSION['USER_ID'];
	if(!isset($_SESSION['USER_ID']))
	{
		$currentuserid = 0;
	}
	$query	=	"UPDATE `$tablename` SET modified_at='$date_time' WHERE id='$rowid'";
	//echo $query.'</br>';
	try
	{
		$result	=	$conn->prepare($query);
		$result->execute();
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$currentuserid);
	}
}

function ImageUpload($fileinputid, $targetfolder, $targetfile)
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$file = $_FILES[$fileinputid]['tmp_name'];
		if (file_exists($file))
		{
			$imagesizedata = getimagesize($file);
			if ($imagesizedata === FALSE)
			{
				//not image
				$msg="Upload an image file and not any other file.";
				$msg = 'invalid';
				LogError('Error in ImageUpload - Invalid File',__FILE__,__LINE__,0);
				return $msg;
				exit();
			}
			else
			{
				//image
				//use $imagesizedata to get extra info
				$target = $targetfolder.$targetfile;
				
				if(move_uploaded_file($_FILES[$fileinputid]['tmp_name'], $target))
				{
					//File Uploaded Successfully
					$msg="Image file uploaded successfully.";
					$msg="success";
					return $msg;
					exit();
				}
				else {
					//File Uploaded Unsuccessful
					$msg="Sorry, there was a problem uploading your file.";
					$msg = 'failure';
					LogError('Error in ImageUpload - Error Uploading',__FILE__,__LINE__,0);						
					return $msg;
					exit();
				}						
			}
		}
		else
		{
			//File not present
			$msg="Kindly upload an image. Empty Not Allowed.";
			$msg = 'empty';
			//LogError('Error in ImageUpload - No File Selected',__FILE__,__LINE__,0);
			return $msg;
			exit();
		}
	}	
}

function AndroidImageUpload1($fileinputid, $targetfolder, $targetfile)
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$file = $_FILES[$fileinputid]['tmp_name'];
		if (file_exists($file))
		{
			$imagesizedata = getimagesize($file);
			if ($imagesizedata === FALSE)
			{
				//not image
				$msg = 'not_image';
				return $msg;
				exit();
			}
			else
			{
				//image
				//use $imagesizedata to get extra info
				$target = $targetfolder.$targetfile;
				if(move_uploaded_file($_FILES[$fileinputid]['tmp_name'], $target))
				{
					//File Uploaded Successfully
					$msg="success";
					return $msg;
					exit();
				}
				else {
					//File Uploaded Unsuccessful
					$msg = 'failure';
					return $msg;
					exit();
				}						
			}
		}
		else
		{
			//File not present
			$msg = 'empty';
			return $msg;
			exit();
		}
	}	
}

function AndroidImageUpload($fileinputid, $targetfolder, $targetfile)
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		//image
		//use $imagesizedata to get extra info
		$target = $targetfolder.$targetfile;
		if(move_uploaded_file($_FILES[$fileinputid]['tmp_name'], $target))
		{
			//File Uploaded Successfully
			$msg="success".$_FILES[$fileinputid]['tmp_name'].'ha';
			return $msg;
			exit();
		}
		else {
			//File Uploaded Unsuccessful
			$msg = 'failure'.$_FILES[$fileinputid]['name'].'ha';
			return $msg;
			exit();
		}
	}	
}
	
// Function for resizing jpg, gif, or png image files
function ak_img_resize($target, $newcopy, $w, $h, $ext) {
    list($w_orig, $h_orig) = getimagesize($target);
    $scale_ratio = $w_orig / $h_orig;
    if (($w / $h) > $scale_ratio) {
           $w = $h * $scale_ratio;
    } else {
           $h = $w / $scale_ratio;
    }
    $img = "";
    $ext = strtolower($ext);
    if ($ext == "gif"){ 
      $img = imagecreatefromgif($target);
    } else if($ext =="png"){ 
      $img = imagecreatefrompng($target);
    } else { 
      $img = imagecreatefromjpeg($target);
    }
    $tci = imagecreatetruecolor($w, $h);
    // imagecopyresampled(dst_img, src_img, dst_x, dst_y, src_x, src_y, dst_w, dst_h, src_w, src_h)
    imagecopyresampled($tci, $img, 0, 0, 0, 0, $w, $h, $w_orig, $h_orig);
    imagejpeg($tci, $newcopy, 80);
}

//Function to create thumbnail of uploaded image 
function make_thumb($src, $dest, $desired_width) {

	/* read the source image */
	$source_image = imagecreatefromjpeg($src); 
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}

function random_password($length) {
		
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#$%^&*";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}

function get_post_value($key){
	if(isset($_POST[$key]))
	{
		return addslashes($_POST[$key]);
	}
	else
	{
		return '';
	}
}

function dateformatconvert($date1) {
	return date('Y-m-d', strtotime($date1));
}

function dateformatui($date1) {
	
	$retdate = date('m/d/Y', strtotime($date1));
	if($retdate!="01/01/1970")
	{
		return $retdate;
	}
	else
	{
		return "";
	}
}

function dateformatandroid($date1) {
	
	$retdate = date('d-M-Y', strtotime($date1));
	if($retdate!="01-Jan-1970")
	{
		return $retdate;
	}
	else
	{
		return "";
	}
}

function datetime_displayformat($date1) {
	$dt = new DateTime($date1);
	$date = $dt->format('d-M-Y');
	$time = $dt->format('h:i A');
	return $date.' | '.$time;
}

function date_displayformat($date1) {
	$dt = new DateTime($date1);
	$date = $dt->format('d-M-Y');
	return $date;
}

function get_get_value($key){
	if(isset($_GET[$key]))
	{
		return addslashes($_GET[$key]);
	}
	else
	{
		return '';
	}
}

function monthtonum($month)
{
	if($month == "January")
	{
		return 1;
	}
	else if ($month == "February")
	{
		return 2;	
	}
	else if ($month == "March")
	{
		return 3;	
	}
	else if ($month == "April")
	{
		return 4;	
	}
	else if ($month == "May")
	{
		return 5;	
	}
	else if ($month == "June")
	{
		return 6;	
	}
	else if ($month == "July")
	{
		return 7;	
	}
	else if ($month == "August")
	{
		return 8;	
	}
	else if ($month == "September")
	{
		return 9;	
	}
	else if ($month == "October")
	{
		return 10;	
	}
	else if ($month == "November")
	{
		return 11;	
	}
	else if ($month == "December")
	{
		return 12;	
	}
}

function numtomonth($num)
{
	if($num == 1)
	{
		return "January";
	}
	else if ($num == 2)
	{
		return "February";	
	}
	else if ($num == 3)
	{
		return "March";	
	}
	else if ($num == 4)
	{
		return "April";	
	}
	else if ($num == 5)
	{
		return "May";	
	}
	else if ($num == 6)
	{
		return "June";	
	}
	else if ($num == 7)
	{
		return "July";	
	}
	else if ($num == 8)
	{
		return "August";	
	}
	else if ($num == 9)
	{
		return "September";	
	}
	else if ($num == 10)
	{
		return "October";	
	}
	else if ($num == 11)
	{
		return "November";	
	}
	else if ($num == 12)
	{
		return "December";	
	}
}

function numtomonthandroid($num)
{
	if($num == 1)
	{
		return "Jan";
	}
	else if ($num == 2)
	{
		return "Feb";	
	}
	else if ($num == 3)
	{
		return "Mar";	
	}
	else if ($num == 4)
	{
		return "Apr";	
	}
	else if ($num == 5)
	{
		return "May";	
	}
	else if ($num == 6)
	{
		return "June";	
	}
	else if ($num == 7)
	{
		return "July";	
	}
	else if ($num == 8)
	{
		return "Aug";	
	}
	else if ($num == 9)
	{
		return "Sep";	
	}
	else if ($num == 10)
	{
		return "Oct";	
	}
	else if ($num == 11)
	{
		return "Nov";	
	}
	else if ($num == 12)
	{
		return "Dec";	
	}
}

function timeformatandroid($date1) {
	
	$retdate = date('d-M-Y H-i-s', strtotime($date1));
	if($retdate!="01-Jan-1970 00-00-00")
	{
		return $retdate;
	}
	else
	{
		return "";
	}
}

function Image_Upload($fileinputid, $targetfolder, $targetfile, $max_size, $allowed_ext)
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$file = $_FILES[$fileinputid]['tmp_name'];
		if (file_exists($file))
		{
			//Check if the uploaded file is an image
			$imagesizedata = getimagesize($file);
			if ($imagesizedata === FALSE)
			{
				//not image
				$msg = 'not_image';
				return $msg;
				exit();
			}
			// Check if file already exists
			if (!file_exists($file)) {
				//Image not selected
				$msg = 'notselected';
				return $msg;
				exit();
			}
			// Check file size
			if ($_FILES[$fileinputid]['size'] > $max_size) {
				//Size exceeds
				$msg = 'sizeexceeds';
				return $msg;
				exit();
			}
			// Allow certain file formats
			$imageFileType = pathinfo($file,PATHINFO_EXTENSION);
			if(in_array($imageFileType, $allowed_ext)) {
				//Size exceeds
				$msg = 'invalidext';
				return $msg;
				exit();
			}
			//Upload File
			$target = $targetfolder.$targetfile;
			if(move_uploaded_file($_FILES[$fileinputid]['tmp_name'], $target))
			{
				//File Uploaded Successfully
				$msg="success";
				return $msg;
				exit();
			}
			else {
				//File Uploaded Unsuccessful
				$msg = 'failure';
				return $msg;
				exit();
			}
		}
		else
		{
			//File not present
			$msg = 'empty';
			return $msg;
			exit();
		}
	}
	else
	{
		//File not send by POST
		$msg = 'notpost';
		return $msg;
		exit();
	}
}

// File Upload
function FileUpload($fileinputid, $targetfolder, $targetfile)
{
	if ($_SERVER['REQUEST_METHOD'] === 'POST')
	{
		$file = $_FILES[$fileinputid]['tmp_name'];
		if (file_exists($file))
		{
			$imagesizedata = getimagesize($file);
			$target = $targetfolder.$targetfile;
			if(move_uploaded_file($_FILES[$fileinputid]['tmp_name'], $target))
			{
				//File Uploaded Successfully
				$msg="success";
				return $msg;
				exit();
			}
			else 
			{
				//File Uploaded Unsuccessful
				$msg = 'failure';
				return $msg;
				exit();
			}
		}
		else
		{
			//File not present
			$msg = 'empty';
			return $msg;
			exit();
		}
	}	
}

function RunSqlFile($conn,$filename){
	// Temporary variable, used to store current query
	$templine = '';
	// Read in entire file
	$lines = file($filename);
	// Loop through each line
	foreach ($lines as $line)
	{
		// Skip it if it's a comment
		if (substr($line, 0, 2) == '--' || $line == '')
			continue;

		// Add this line to the current segment
		$templine .= $line;
		// If it has a semicolon at the end, it's the end of the query
		if (substr(trim($line), -1, 1) == ';')
		{
			// Perform the query
			$result = $conn->prepare($templine);
			$status = $result->execute();
			if(!$status)
			{
				$output['status'] = "failure";
				$output['error'] = "Error on line".$templine;
				return $output;
				exit();
			}
			$templine = '';
		}
	}
	$output['status']="success";
	$output['error']="";
	return $output;
	exit();
}
?>