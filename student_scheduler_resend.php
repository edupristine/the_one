<?php 
include 'inc/GenericFunctions.php';
include 'control/core.php'; 	
include 'control/checklogin.php';
include 'control/connection.php';

include("inc/head.php");
include("inc/sidebar.php");
include("inc/headermenu.php");

include("content/student_scheduler_resend.content.php");
//include("js/sku.js.php");

include("inc/footer.php");
include("inc/foot.php");
?>

<!-- Datatable CSS -->
<link href='DataTables/datatables.min.css' rel='stylesheet' type='text/css'>

<!-- jQuery Library -->
<!--<script src="jquery-3.3.1.min.js"></script>-->

<!-- Datatable JS -->
<script src="DataTables/datatables.min.js"></script>
