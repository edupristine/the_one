<?php
$db_server='52.77.5.117';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'vtigercrm6');

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value


$batch_code = $_POST['batch_code'];

## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (a.account_no like '%".$searchValue."%' or 
	    a.accountname like '%".$searchValue."%') ";
}




## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
    		count(*) as allcount 
    		FROM  vtiger_account a
			LEFT JOIN 
			vtiger_accountscf acf ON acf.accountid = a.accountid 
			LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = a.accountid
			WHERE (acf.cf_1269 LIKE '%".$batch_code."%'
					OR acf.cf_1571 LIKE '%".$batch_code."%'
					OR acf.cf_1575 LIKE '%".$batch_code."%'
					OR acf.cf_1495 = '".$batch_code."'
					OR acf.cf_1497 = '".$batch_code."'  
					OR acf.cf_1499 = '".$batch_code."'
					OR acf.cf_1501 = '".$batch_code."'
					OR acf.cf_1503 = '".$batch_code."'
					OR acf.cf_1505 = '".$batch_code."')
                    AND  a.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920");
				
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
    		count(*) as allcount 
    		FROM  vtiger_account a
			LEFT JOIN 
			vtiger_accountscf acf ON acf.accountid = a.accountid 
			LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = a.accountid
			WHERE (acf.cf_1269 LIKE '%".$batch_code."%'
					OR acf.cf_1571 LIKE '%".$batch_code."%'
					OR acf.cf_1575 LIKE '%".$batch_code."%'
					OR acf.cf_1495 = '".$batch_code."'
					OR acf.cf_1497 = '".$batch_code."'  
					OR acf.cf_1499 = '".$batch_code."'
					OR acf.cf_1501 = '".$batch_code."'
					OR acf.cf_1503 = '".$batch_code."'
					OR acf.cf_1505 = '".$batch_code."')
                    AND  a.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920");
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT 
					a.accountid AS id,
					acf.cf_1269 AS BatchCode,
					acf.cf_1495 as `reg_batch_code`,
					acf.cf_1497 as `at1_batch_code`,
					acf.cf_1499 as `at2_batch_code`,
					acf.cf_1501 as `at3_batch_code`,
					acf.cf_1503 as `at4_batch_code`,
					acf.cf_1505 as `at5_batch_code`,
					acf.cf_1571 as `complementary_code`, 
					acf.cf_1575 as `rescheduling_code`,
					acf.cf_1467 as `access_flag`,
					a.accountname AS `student_name`,
					a.email1 AS email,
					a.phone AS phone,
					a.account_no as organization_no,
					acf.cf_936 AS Amount,
					acf.cf_938 AS Paid,
					acf.cf_940 AS Balance
					FROM vtiger_account a
					LEFT JOIN vtiger_accountscf acf ON acf.accountid = a.accountid 
					LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = a.accountid
					WHERE (acf.cf_1269 LIKE '%".$batch_code."%'
					OR acf.cf_1571 LIKE '%".$batch_code."%'		
					OR acf.cf_1575 LIKE '%".$batch_code."%'
					OR acf.cf_1495 = '".$batch_code."'
					OR acf.cf_1497 = '".$batch_code."'  
					OR acf.cf_1499 = '".$batch_code."'
					OR acf.cf_1501 = '".$batch_code."'
					OR acf.cf_1503 = '".$batch_code."'
					OR acf.cf_1505 = '".$batch_code."') 
					 AND  a.email1 NOT LIKE '%junk%' AND ce.smownerid !=3920 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($con, $empQuery);
$data = array();
$email_array= array();
while ($row = mysqli_fetch_assoc($empRecords)) {
	$email_array[]=$row['email'];
	
  $data[] = array(
	        "id"=>$row['id'],
    		"reg_batch_code"=>$row['reg_batch_code'],
    		"at1_batch_code"=>$row['at1_batch_code'],
			"at2_batch_code"=>$row['at2_batch_code'],
    		"at3_batch_code"=>$row['at3_batch_code'],
    		"at4_batch_code"=>$row['at4_batch_code'],
    		"at5_batch_code"=>$row['at5_batch_code'],
			"complementary_code"=>$row['complementary_code'],
			"student_name"=>$row['student_name'],
			"organization_no"=>$row['organization_no'],
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
    "email_data" => $email_array,
);

echo json_encode($response);
