<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/nopay_mail.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');


try{
	if(isset($_POST['org_no'])  &&  !empty($_POST['org_no']))
	{
		$student_name = get_post_value('student_name');
		$org_no = get_post_value('org_no');
	    $course = get_post_value('course');
		$city = get_post_value('city');
		$Assigned_to = get_post_value('Assigned_to');
		$paid = get_post_value('paid');
		$paiddated = $_POST['paiddated'];
		$mode = get_post_value('mode');
		$trans_id = get_post_value('trans_id');
		$amtpaid = 0;
		$expecteddate = get_post_value('expecteddate');
		$reasons = get_post_value('reasons');
		$comments = get_post_value('comments');
		$trans_column = get_post_value('trans_column');
		$acc_id = get_post_value('acc_id');
		$Assigned = get_post_value('assigned');
		
		
		
        $select_rec="SELECT count(id) as `counts` FROM student_paydetails  WHERE `org_no`='".$org_no."' AND   `trans_column`='".$trans_column."' ";
		$result_rec = Select($select_rec,$conn,"student_paydetails");

		if(($result_rec['rows'][0]['counts'])==0)
		{
	    $insert_paydetails = "INSERT INTO student_paydetails (
		`student_name`,
		`org_no`,
		`course`,
		`city`,
		`assigned_to`,
		`paid`,
		`paiddated`,
		`paymode`,
		`transaction_id`,
		`amountpaid`,
		`expecteddate`,
		`reasons`,
		`comments`,
		`trans_column`
		)values(
		'".$student_name."',
		'".$org_no."',
		'".$course."',
		'".$city."',
		'".$Assigned_to."',
		'".$paid."',
		'".$paiddated."',
		'".$mode."',
		'".$trans_id."',
		'".$amtpaid."',
		'".$expecteddate."',
		'".$reasons."',
		'".$comments."',
		'".$trans_column."'
		)";
		$result_inspaydetails = Insert($insert_paydetails,$conn,"student_paydetails");
		}
		else
		{
		 $update_paydetails = "Update student_paydetails set 
		`amountpaid`='".$amtpaid."',
		`expecteddate`='".$expecteddate."',
		`reasons`='".$reasons."',
		`comments`='".$comments."',
		`trans_column`='".$trans_column."'
		 WHERE `org_no`='".$org_no."'  AND  `trans_column`='".$trans_column."'";
		$result_inspaydetails = Update($update_paydetails,$conn,"student_paydetails");	
		}
		
		
		
		//Intimation Mail Sending to Councellors for the Rejection 
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>This is to inform you that the listed transaction for the organization no. $org_no is been rejected by the accounts team</p>
		<p>Please find below Summary for the same</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$org_no."</td></tr>";
	
	    $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Course:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$course."</td></tr>";
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount Paid:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$paid."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Transaction ID:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$trans_id."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comments."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		Akshata Shenoy</td></tr>";
		
		$html .= "</table>";
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the Accounts Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine Accounts Team</p>";
		
		
		$vusers="SELECT email1,CONCAT(first_name, ' ', last_name) AS 'email_name'  FROM vtiger_users where id=".$Assigned."";
		$result_vusers = mysqli_query($conn1,$vusers);
		$councellors = mysqli_fetch_assoc($result_vusers);
		
		$email=$councellors['email1'];
		$email_name=$councellors['email_name'];
		
		
		$subject = "Transactions Details for Organization no. ".$org_no."  Rejected";
		send_smt_mail($subject,$html,$email,$email_name);
		
	 

		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>