<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['transfer_id'])  &&  !empty($_POST['transfer_id']))
	{
		$transfer_id = get_post_value('transfer_id');
		$courier_track_no = get_post_value('courier_track_no');
		$transfer_courier = get_post_value('transfer_courier');
		$transfer_charges = get_post_value('transfer_charges');
		
		
		if($courier_track_no=="")
		{
			$courier_track_no="";
		}
		
		if($transfer_charges=="")
		{
			$transfer_charges="0";
		}

			
	        $update_courier = "UPDATE transfers set courier_track_no = '".$courier_track_no."', transfer_courier = '".$transfer_courier."', transfer_charges = '".$transfer_charges."' WHERE id = '".$transfer_id."' ";
			$result_up_courier = Update($update_courier,$conn,"transfers");
  
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>