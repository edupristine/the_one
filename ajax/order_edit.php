<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
try{
	if( isset($_POST['skus']) && isset($_POST['ord_num']) && isset($_POST['id']) && isset($_POST['ord_date'])&& isset($_POST['kt_select2_1'])&& 
		!empty($_POST['skus']) && !empty($_POST['ord_num']) && !empty($_POST['id']) && !empty($_POST['ord_date']) && !empty($_POST['kt_select2_1']) )
	{
		$id	= get_post_value('id');
		$ord_num	= get_post_value('ord_num');
		$ord_date	= get_post_value('ord_date');
		$ord_date 	= date("Y-m-d", strtotime($ord_date));		
		$vendor_id		= get_post_value('kt_select2_1');
		$exp_rec_date	= get_post_value('exp_rec_date');
		$exp_rec_date 	= date("Y-m-d", strtotime($exp_rec_date));
		$ref_num	= get_post_value('ref_num');
		$ord_amount	= get_post_value('ord_amount');
		$comments	= get_post_value('comments');
		
		$skus		= get_post_value('skus');
		$skus = stripslashes($skus);
		$skus = json_decode($skus);
		$skus = json_decode(json_encode($skus), true);
		
		$old_order = "SELECT * from orders where id = ".$id;
		$result_old_order = Select($old_order,$conn);
		
		$old_order_skus = "SELECT * from orders_skus where ord_id = ".$id;
		$result_old_order_skus = Select($old_order_skus,$conn);
		
		if($result_old_order['rows'][0]['ord_status'] == "Received")
		{
			$output = array(
			"status"=>'received_order',
			"order_id"=>$id,
			"message"=>'You Cannot Edit Received Order.');
			$output = json_encode($output);
			echo $output;
			exit();
		}
		
		$old_placed_by = $result_old_order['rows'][0]['placed_by'];
		
		$query_delete_skus = "Delete from orders_skus where ord_id = ".$id;
		$result_delete_skus = Del($query_delete_skus,$conn);
		
		$query_delete_order = "Delete from orders where id = ".$id;
		$result_delete_order = Del($query_delete_order,$conn);
		
		$query_insertorder = "INSERT INTO `orders` (`ord_num`,`ord_date`,`pi_num`,`vendor_id`,`placed_by`,`received_by`,
		`ord_status`,`ord_amt`,`exp_rec_date`,`comments`,`loc_id`)
		VALUES('".$ord_num."','".$ord_date."','".$ref_num."','".$vendor_id."','".$old_placed_by."','0',
		'Placed','".$ord_amount."','".$exp_rec_date."','".$comments."',1)";
		
		$result_insertorder = Insert($query_insertorder,$conn,'orders');
		$order_id = $result_insertorder['id'];
		
		foreach($skus as $sku)
		{
			$query_insertsku = "INSERT INTO `orders_skus` (`ord_id`,`sku_id`,qty,`rate`,`amount`)
			VALUES('".$order_id."','".$sku['skuname']."','".$sku['qty']."','".$sku['rate']."','".$sku['amt']."')";
			$result_insertsku = Insert($query_insertsku,$conn,'orders_skus');
			
		}
		
		$output = array(
			"status"=>'success',
			"order_id"=>$order_id,
			"message"=>'Order Updated Successfully');
		$output = json_encode($output);
		echo $output;
		exit();
	}
	else{
		$output = array(
			"status"=>'missing_params',
			"order_id"=>'',
			"message"=>'Order Updation FAILED. Parameters Missing.');
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error',
		"order_id"=>'',
		"message"=>'Order Updation FAILED. Contact Administrator');
	$output = json_encode($output);
	echo $output;
	exit();
}
?>