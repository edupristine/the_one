<?php
ini_set('max_execution_time', 0);//10 min
ini_set('max_input_time', -1);//10 min
ini_set('mysql.connect_timeout', 14400);
ini_set('default_socket_timeout', 14400);
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail_trans_summary.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$conn2=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'edupristine_one');
/*For sending mails to students on validations of the payments*/

$date_range = get_get_value('date_range');
if($date_range != ""){
$drange = explode(" / ",$date_range);	
// $edate = date("Y-m-d",strtotime($drange));
// $sdate = date('Y-m-d', strtotime('-15 days', strtotime($edate)));
$sdate = date('Y-m-d',strtotime($drange[0]));
$edate = date('Y-m-d', strtotime('+1 days', strtotime($drange[1])));

}else{
    $sdate = date("Y-m-d");
    $edate = date('Y-m-d', strtotime('+1 days', strtotime($sdate)));
    $date_range = $sdate." / ".$edate;
}




		$start_date = $sdate;
		$end_date = $edate;


		$start    = new DateTime($sdate);
		$start->modify('first day of this month');
		$end      = new DateTime($edate);
		$end->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		$this_month =  date("Y-m",strtotime("this month"));


        $html="";
		$html .= "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td colspan=10 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
		</tr>
		<tr>
		<td colspan=10 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Hi ,</td>
		</tr>
		<tr>
		<td colspan=10 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		Please find below transaction summary of the payment received.
		</td></tr>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>#</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;width:100px;'>Transaction Date</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Daily Collection as per BI</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Amount Received</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Actual Amount Received</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Wrong Entry/Rejected Cases</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Amount Pending</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Finance Com</td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Cheque </td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 12px;'>Others</td>
		
		</tr>
		";
		$i = 1;
		$totalpaid=0;
		$amt_recv=0;
		$cumulativesum=0;
		$amtrce=0;
		$penamt=0;
		$financecom=0;
		$cheque=0;
		$others=0;
		$wrongentry=0;
		$finaloff=0;
		$blanksvar=0;
		$ac_amt_rec=0;
		foreach ($period as $dt) {
		$monthdisplay =  $dt->format("Y-m");
		$mymonth=$dt->format("m");
		$myyear=$dt->format("Y");

		if($monthdisplay==$this_month)
		{

		$startday    = new DateTime($dt->format("Y-m-d"));
		$startday->modify('first day of this month');
		$endday      = new DateTime($edate);

		$dayinterval = DateInterval::createFromDateString('1 day');
		$dayperiod   = new DatePeriod($startday, $dayinterval, $endday);
		foreach ($dayperiod as $dt) {											
			
		$newdate = $dt->format("Y-m-d");
		$pendingamt=0;

		$querypay="select sum(paid) as `paid` FROM (
			SELECT sum(sc.cf_1303) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1363) = '".$newdate."'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1313) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1365) = '".$newdate."'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1323) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1367) = '".$newdate."'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1333) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1369) = '".$newdate."'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1343) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1371) = '".$newdate."'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1353) as paid  
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1373) = '".$newdate."' 
			AND ce.smownerid != 3920 ) x";
		 

		 
			$resultqp = mysqli_query($conn1,$querypay);
			$qrpay = mysqli_fetch_assoc($resultqp);
			
			
			$querypaymode="select sum(paid) as `paid` FROM (
			SELECT sum(sc.cf_1303) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1363) = '".$newdate."' 
			AND sc.cf_1305 = 'Cheque'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1313) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1365) = '".$newdate."'
			AND sc.cf_1315 = 'Cheque'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1323) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1367) = '".$newdate."'
			AND sc.cf_1325 = 'Cheque'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1333) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1369) = '".$newdate."'
			AND sc.cf_1335 = 'Cheque'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1343) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1371) = '".$newdate."'
			AND sc.cf_1345 = 'Cheque'
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1353) as paid  
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1373) = '".$newdate."'
			AND sc.cf_1355 = 'Cheque'												
			AND ce.smownerid != 3920 ) x";
		 

		 
			$resultqpmode = mysqli_query($conn1,$querypaymode);
			$qrpaymode = mysqli_fetch_assoc($resultqpmode);
			
			
			
			$querypaymodeL="select sum(paid) as `paidL` FROM (
			SELECT sum(sc.cf_1303) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1363) = '".$newdate."' 
			AND (sc.cf_1305 = 'Liqui Loans' OR
			sc.cf_1305 = 'Neev Finance' OR
			sc.cf_1305 = 'ABFL' OR
			sc.cf_1305 = 'Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1313) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1365) = '".$newdate."'
			AND (sc.cf_1315 = 'Liqui Loans' OR
			sc.cf_1315 = 'Neev Finance' OR
			sc.cf_1315 = 'ABFL' OR
			sc.cf_1315 = 'Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1323) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1367) = '".$newdate."'
			AND (sc.cf_1325 = 'Liqui Loans' OR
			sc.cf_1325 = 'Neev Finance' OR
			sc.cf_1325 = 'ABFL' OR
			sc.cf_1325 = 'Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1333) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1369) = '".$newdate."'
			AND (sc.cf_1335 = 'Liqui Loans' OR
			sc.cf_1335 = 'Neev Finance' OR
			sc.cf_1335 = 'ABFL' OR
			sc.cf_1335 = 'Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1343) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1371) = '".$newdate."'
			AND (sc.cf_1345 = 'Liqui Loans'
			OR sc.cf_1345 = 'Neev Finance' OR
			sc.cf_1345 = 'ABFL' OR
			sc.cf_1345 = 'Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1353) as paid  
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1373) = '".$newdate."'
			AND (sc.cf_1355 = 'Liqui Loans' OR
			sc.cf_1355 = 'Neev Finance' OR
			sc.cf_1355 = 'ABFL'  OR
			sc.cf_1355 = 'Avanse')
			AND ce.smownerid != 3920 ) x";
		 

		 
			$resultqpL = mysqli_query($conn1,$querypaymodeL);
			$qrpayL = mysqli_fetch_assoc($resultqpL);
			
			$querypaymodeO="select sum(paid) as `paidO` FROM (
			SELECT sum(sc.cf_1303) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1363) = '".$newdate."' 
			AND (sc.cf_1305 = 'card' OR
			sc.cf_1305 = 'cash' OR
			sc.cf_1305 = 'online' OR
			sc.cf_1305 = 'paytm' OR
			sc.cf_1305 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1313) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1365) = '".$newdate."'
			AND (sc.cf_1315 = 'card' OR
			sc.cf_1315 = 'cash' OR
			sc.cf_1315 = 'online' OR
			sc.cf_1315 = 'paytm' OR
			sc.cf_1315 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1323) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1367) = '".$newdate."'
			AND 
			(sc.cf_1325 = 'card' OR
			sc.cf_1325 = 'cash' OR
			sc.cf_1325 = 'online' OR
			sc.cf_1325 = 'paytm' OR
			sc.cf_1325 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1333) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1369) = '".$newdate."'
			AND 
			(sc.cf_1335 = 'card' OR
			sc.cf_1335 = 'cash' OR
			sc.cf_1335 = 'online' OR
			sc.cf_1335 = 'paytm' OR
			sc.cf_1335 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1343) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1371) = '".$newdate."'
			AND 
			(sc.cf_1345 = 'card' OR
			sc.cf_1345 = 'cash' OR
			sc.cf_1345 = 'online' OR
			sc.cf_1345 = 'paytm' OR
			sc.cf_1345 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1353) as paid  
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1373) = '".$newdate."'
			AND 
			(sc.cf_1355 = 'card' OR
			sc.cf_1355 = 'cash' OR
			sc.cf_1355 = 'online' OR
			sc.cf_1355 = 'paytm' OR
			sc.cf_1355 = 'wire transfer' 
			)
			AND ce.smownerid != 3920 ) x";
		 

		 
			$resultqpO = mysqli_query($conn1,$querypaymodeO);
			$qrpayO = mysqli_fetch_assoc($resultqpO);
			
			
			$querypaymodeOthers="select sum(paid) as `paidOthers` FROM (
			SELECT sum(sc.cf_1303) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1363) = '".$newdate."' 
			AND sc.cf_1305 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
			
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1313) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1365) = '".$newdate."'
			AND sc.cf_1315 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
			
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1323) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1367) = '".$newdate."'
			AND 
			sc.cf_1325 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1333) as paid
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1369) = '".$newdate."'
			AND sc.cf_1335 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1343) as paid 
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1371) = '".$newdate."'
			AND 
			sc.cf_1345 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
			AND ce.smownerid != 3920 
			UNION ALL
			SELECT sum(sc.cf_1353) as paid  
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE
			DATE(sc.cf_1373) = '".$newdate."'
			AND sc.cf_1355 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse') 
			AND ce.smownerid != 3920 ) x";
		 

		 
			$resultqpOthers = mysqli_query($conn1,$querypaymodeOthers);
			$qrpayOthers = mysqli_fetch_assoc($resultqpOthers);
			
			
			
			$select_recx="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' ";
			
			$resultrecx = mysqli_query($conn2,$select_recx);
			$qrpayrex = mysqli_fetch_assoc($resultrecx);
			
			$select_recC="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode='Cheque' ";
			
			$resultrecC = mysqli_query($conn2,$select_recC);
			$qrpayreC = mysqli_fetch_assoc($resultrecC);
			
			$select_recL="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode in ('Liqui Loans','Neev Finance','ABFL','Avanse') ";
			
			$resultrecL = mysqli_query($conn2,$select_recL);
			$qrpayreL = mysqli_fetch_assoc($resultrecL);
			
			$select_recO="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='1' and paymode in ('Cash','Card','Online','Wire Transfer','Paytm','',' ') ";
			
			$resultrecO = mysqli_query($conn2,$select_recO);
			$qrpayreO = mysqli_fetch_assoc($resultrecO);
			
			//$select_wrong="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='2' and reasons in ('Wrong Entry','Reject') ";
			
			//Query Moved Out Due to Akshata's Report Display Issues
			$select_wrong="SELECT sum(paid) as `amount_rec`,paymode FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid` NOT IN ('1') and reasons in ('Wrong Entry','Reject') ";
			$resultwrong = mysqli_query($conn2,$select_wrong);
			$wrong_entries = mysqli_fetch_assoc($resultwrong);
			
			$rowcount1 = mysqli_num_rows($resultwrong);
			
			$select_ac_amtrec="SELECT amount as `ac_amount_rec` FROM datewise_payments_recieved  WHERE   `received_date`='".$newdate."' ";

			$resultamrec = mysqli_query($conn2,$select_ac_amtrec);
			$acamrec = mysqli_fetch_assoc($resultamrec);
			
			$select_wrong_details="SELECT paid as `amount_rec`,paymode FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid`='2' ";
			
			$qrpayrewrongdetails = mysqli_query($conn2,$select_wrong_details);
			
			while($qrpayrewrongdets = mysqli_fetch_assoc($qrpayrewrongdetails)) {
			$qrpayrewrong[] =$qrpayrewrongdets;
			}
			
			$cumulativesum += $qrpay['paid'];
			
        
			if($qrpayrex['amount_rec']!='')
			{
			$amt_recv=$qrpayrex['amount_rec'];		
			$qrpayrex['amount_rec'] =  $qrpayrex['amount_rec'];
			}
			else
			{
			$amt_recv=0;
			$qrpayrex['amount_rec']= "0";
			}	
		
			$finalcheq5= $wrong_entries['amount_rec']; 
			if($finalcheq5<0)
			{
			$finalcheq5= "0";
			}
			else
			{
			 $finalcheq5;
			}
			
			$pendingamt=0;
	        $pendingvalue=$qrpay['paid']-$amt_recv;													
													
		    $pendingamt= $pendingvalue-$qrpayrewrong['amount_rec']; 
			
			if($pendingamt<0)
			{
				$pendingamt= "0";
			}
			else
			{
				$pendingamt;
			}
		
		
		    $wrongdetailssum=0;
			foreach($qrpayrewrongdetails as $paydet)
			{
			if(($paydet['paymode']=='Liqui Loans')||($paydet['paymode']=='Neev Finance')||($paydet['paymode']=='ABFL'))
			{
			$wrongdetailssum += $paydet['amount_rec'];

			}
			}


			$loanvalue=$qrpayL['paidL']-$qrpayreL['amount_rec'];

			if(($loanvalue!=0)&&($wrongdetailssum>0))
			{
			$finalcheq2= $loanvalue-$wrongdetailssum; 
			}
			else
			{
			$finalcheq2= $loanvalue; 	
			}
			if($finalcheq2<0)
			{
			$finalcheq2= "0";
			}
			else
			{
			$finalcheq2;
			}
		
		
			$wrongdetailssum1=0;
			foreach($qrpayrewrongdetails as $paydet)
			{
				if(($paydet['paymode']=='Cheque'))
				{
					$wrongdetailssum1 += $paydet['amount_rec'];
					
				}
			}
			
			$chequevalue=$qrpaymode['paid']-$qrpayreC['amount_rec'];
			
			if(($chequevalue!=0)&&($wrongdetailssum1>0))
			{
			$finalcheq= $chequevalue-$wrongdetailssum1; 
			}
			else
			{
			$finalcheq=$chequevalue; 
			}
			
			if($finalcheq<0)
			{
				$finalcheq = "0";
			}
			else
			{
				$finalcheq;
			}
		
			$wrongdetailssum2=0;
			foreach($qrpayrewrongdetails as $paydet)
			{

			if(($paydet['paymode']=='Cash')||($paydet['paymode']=='Card')||($paydet['paymode']=='Online')||($paydet['paymode']=='Wire Transfer')||($paydet['paymode']=='Paytm')||($paydet['paymode']==' '))
			{
			$wrongdetailssum2 += $paydet['amount_rec'];

			}

			}
			$othersvalue=($qrpayO['paidO']+$qrpayOthers['paidOthers'])-$qrpayreO['amount_rec'];
			if($wrongdetailssum2>0)
			{
			$finalcheq3= $othersvalue-$wrongdetailssum2;													
			}
			else
			{
			$finalcheq3= $othersvalue; 
			}

			if($finalcheq3<0)
			{
			$finalcheq3= "0";
			}
			else
			{
			$finalcheq3;
			}
		
		$date = date_create($newdate);
		$dispdate = date_format($date,"d-M-Y");
		$finalpending=0;
		
		
		if($rowcount1 !="0")
		{
			$finalpending=$pendingamt; 
		}
		else
		{
			$finalpending="0";
		}
		
		$ac_am_rec= $acamrec['ac_amount_rec']; 
		if($ac_am_rec<0)
		{
		$ac_am_rec = "0";
		}
		else
		{
		$ac_am_rec = $ac_am_rec;
		}
		
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>". $i."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:center;'>".$dispdate."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".(int)$qrpay['paid']."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$amt_recv."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$ac_am_rec."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$finalcheq5."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$finalpending."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$finalcheq2."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$finalcheq."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'>".$finalcheq3."</td>
		
		";
		
		$html .="</tr>";
		$amtrce= $amtrce + $amt_recv;
		$penamt= $penamt + $pendingamt;
		$financecom=$financecom+$finalcheq2;
		$cheque=$cheque+$finalcheq;
		$others=$others+$finalcheq3;
		$wrongentry=$wrongentry+$finalcheq5;
		$totalpaid=$totalpaid+$qrpay['paid'];
		$blanksvar=$blanksvar+$finalcheq4;	
		$ac_amt_rec=$ac_amt_rec+$ac_am_rec;	
		$i++;
		}
		}
		else
		{

		$querypay="select sum(paid) as `paid` FROM (
		SELECT sum(sc.cf_1303) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1363) = '".$myyear."'
		AND MONTH(sc.cf_1363) = '".$mymonth."'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1313) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1365) = '".$myyear."'
		AND MONTH(sc.cf_1365) = '".$mymonth."'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1323) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1367) = '".$myyear."'
		AND MONTH(sc.cf_1367) = '".$mymonth."'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1333) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1369) = '".$myyear."'
		AND MONTH(sc.cf_1369) = '".$mymonth."'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1343) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1371) = '".$myyear."'
		AND MONTH(sc.cf_1371) = '".$mymonth."'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1353) as paid  
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1373) = '".$myyear."'
		AND MONTH(sc.cf_1373) = '".$mymonth."'
		AND ce.smownerid != 3920 ) x";



		$resultqp = mysqli_query($conn1,$querypay);
		$qrpay = mysqli_fetch_assoc($resultqp);


		$querypaymode="select sum(paid) as `paid` FROM (
		SELECT sum(sc.cf_1303) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1363) = '".$myyear."'
		AND MONTH(sc.cf_1363) = '".$mymonth."'
		AND sc.cf_1305 = 'Cheque'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1313) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1365) = '".$myyear."'
		AND MONTH(sc.cf_1365) = '".$mymonth."'
		AND sc.cf_1315 = 'Cheque'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1323) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1367) = '".$myyear."'
		AND MONTH(sc.cf_1367) = '".$mymonth."'
		AND sc.cf_1325 = 'Cheque'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1333) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1369) = '".$myyear."'
		AND MONTH(sc.cf_1369) = '".$mymonth."'
		AND sc.cf_1335 = 'Cheque'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1343) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1371) = '".$myyear."'
		AND MONTH(sc.cf_1371) = '".$mymonth."'
		AND sc.cf_1345 = 'Cheque'
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1353) as paid  
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1373) = '".$myyear."'
		AND MONTH(sc.cf_1373) = '".$mymonth."'
		AND sc.cf_1355 = 'Cheque'												
		AND ce.smownerid != 3920 ) x";



		$resultqpmode = mysqli_query($conn1,$querypaymode);
		$qrpaymode = mysqli_fetch_assoc($resultqpmode);



		$querypaymodeL="select sum(paid) as `paidL` FROM (
		SELECT sum(sc.cf_1303) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1363) = '".$myyear."'
		AND MONTH(sc.cf_1363) = '".$mymonth."'
		AND (sc.cf_1305 = 'Liqui Loans' OR
		sc.cf_1305 = 'Neev Finance' OR
		sc.cf_1305 = 'ABFL' OR
		sc.cf_1305 = 'Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1313) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1365) = '".$myyear."'
		AND MONTH(sc.cf_1365) = '".$mymonth."'
		AND (sc.cf_1315 = 'Liqui Loans' OR
		sc.cf_1315 = 'Neev Finance' OR
		sc.cf_1315 = 'ABFL' OR
		sc.cf_1315 = 'Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1323) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1367) = '".$myyear."'
		AND MONTH(sc.cf_1367) = '".$mymonth."'
		AND (sc.cf_1325 = 'Liqui Loans' OR
		sc.cf_1325 = 'Neev Finance' OR
		sc.cf_1325 = 'ABFL' OR
		sc.cf_1325 = 'Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1333) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1369) = '".$myyear."'
		AND MONTH(sc.cf_1369) = '".$mymonth."'
		AND (sc.cf_1335 = 'Liqui Loans' OR
		sc.cf_1335 = 'Neev Finance' OR
		sc.cf_1335 = 'ABFL' OR
		sc.cf_1335 = 'Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1343) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1371) = '".$myyear."'
		AND MONTH(sc.cf_1371) = '".$mymonth."'
		AND (sc.cf_1345 = 'Liqui Loans'
		OR sc.cf_1345 = 'Neev Finance' OR
		sc.cf_1345 = 'ABFL' OR
		sc.cf_1345 = 'Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1353) as paid  
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1373) = '".$myyear."'
		AND MONTH(sc.cf_1373) = '".$mymonth."'
		AND (sc.cf_1355 = 'Liqui Loans' OR
		sc.cf_1355 = 'Neev Finance' OR
		sc.cf_1355 = 'ABFL' OR
		sc.cf_1355 = 'Avanse')
		AND ce.smownerid != 3920 ) x";



		$resultqpL = mysqli_query($conn1,$querypaymodeL);
		$qrpayL = mysqli_fetch_assoc($resultqpL);

		$querypaymodeO="select sum(paid) as `paidO` FROM (
		SELECT sum(sc.cf_1303) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1363) = '".$myyear."'
		AND MONTH(sc.cf_1363) = '".$mymonth."'
		AND (sc.cf_1305 = 'card' OR
		sc.cf_1305 = 'cash' OR
		sc.cf_1305 = 'online' OR
		sc.cf_1305 = 'paytm' OR
		sc.cf_1305 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1313) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1365) = '".$myyear."'
		AND MONTH(sc.cf_1365) = '".$mymonth."'
		AND (sc.cf_1315 = 'card' OR
		sc.cf_1315 = 'cash' OR
		sc.cf_1315 = 'online' OR
		sc.cf_1315 = 'paytm' OR
		sc.cf_1315 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1323) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1367) = '".$myyear."'
		AND MONTH(sc.cf_1367) = '".$mymonth."'
		AND 
		(sc.cf_1325 = 'card' OR
		sc.cf_1325 = 'cash' OR
		sc.cf_1325 = 'online' OR
		sc.cf_1325 = 'paytm' OR
		sc.cf_1325 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1333) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1369) = '".$myyear."'
		AND MONTH(sc.cf_1369) = '".$mymonth."'
		AND 
		(sc.cf_1335 = 'card' OR
		sc.cf_1335 = 'cash' OR
		sc.cf_1335 = 'online' OR
		sc.cf_1335 = 'paytm' OR
		sc.cf_1335 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1343) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1371) = '".$myyear."'
		AND MONTH(sc.cf_1371) = '".$mymonth."'
		AND 
		(sc.cf_1345 = 'card' OR
		sc.cf_1345 = 'cash' OR
		sc.cf_1345 = 'online' OR
		sc.cf_1345 = 'paytm' OR
		sc.cf_1345 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1353) as paid  
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1373) = '".$myyear."'
		AND MONTH(sc.cf_1373) = '".$mymonth."'
		AND 
		(sc.cf_1355 = 'card' OR
		sc.cf_1355 = 'cash' OR
		sc.cf_1355 = 'online' OR
		sc.cf_1355 = 'paytm' OR
		sc.cf_1355 = 'wire transfer' 
		)
		AND ce.smownerid != 3920 ) x";



		$resultqpO = mysqli_query($conn1,$querypaymodeO);
		$qrpayO = mysqli_fetch_assoc($resultqpO);


		$querypaymodeOthers="select sum(paid) as `paidOthers` FROM (
		SELECT sum(sc.cf_1303) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1363) = '".$myyear."'
		AND MONTH(sc.cf_1363) = '".$mymonth."'
		AND sc.cf_1305 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')

		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1313) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1365) = '".$myyear."'
		AND MONTH(sc.cf_1365) = '".$mymonth."'
		AND sc.cf_1315 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')

		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1323) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1367) = '".$myyear."'
		AND MONTH(sc.cf_1367) = '".$mymonth."'
		AND 
		sc.cf_1325 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1333) as paid
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1369) = '".$myyear."'
		AND MONTH(sc.cf_1369) = '".$mymonth."'
		AND sc.cf_1335 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1343) as paid 
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1371) = '".$myyear."'
		AND MONTH(sc.cf_1371) = '".$mymonth."'
		AND 
		sc.cf_1345 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse')
		AND ce.smownerid != 3920 
		UNION ALL
		SELECT sum(sc.cf_1353) as paid  
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE
		YEAR(sc.cf_1373) = '".$myyear."'
		AND MONTH(sc.cf_1373) = '".$mymonth."'
		AND sc.cf_1355 NOT IN  ('Cheque','Cash','Card','Paytm','Online','Wire Transfer','Purchase Order','Tata Capital','Neev Finance','Liqui Loans','ABFL','Avanse') 
		AND ce.smownerid != 3920 ) x";



		$resultqpOthers = mysqli_query($conn1,$querypaymodeOthers);
		$qrpayOthers = mysqli_fetch_assoc($resultqpOthers);



		$select_recx="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='1' ";

		$resultrecx = mysqli_query($conn2,$select_recx);
		$qrpayrex = mysqli_fetch_assoc($resultrecx);



		$select_recC="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='1' and paymode='Cheque' ";

		$resultrecC = mysqli_query($conn2,$select_recC);
		$qrpayreC = mysqli_fetch_assoc($resultrecC);



		$select_recL="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='1' and paymode in ('Liqui Loans','Neev Finance','ABFL','Avanse') ";

		$resultrecL = mysqli_query($conn2,$select_recL);
		$qrpayreL = mysqli_fetch_assoc($resultrecL);



		$select_recO="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='1' and paymode in ('Cash','Card','Online','Wire Transfer','Paytm','',' ') ";

		$resultrecO = mysqli_query($conn2,$select_recO);
		$qrpayreO = mysqli_fetch_assoc($resultrecO);



		//$select_wrong="SELECT sum(paid) as `amount_rec` FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='2' and reasons in ('Wrong Entry','Reject') ";
		
		$select_wrong="SELECT sum(paid) as `amount_rec`,paymode FROM student_paydetails  WHERE   `paiddated`='".$newdate."' AND  `amountpaid` NOT IN ('1') and reasons in ('Wrong Entry','Reject') ";
		$resultwrong = mysqli_query($conn2,$select_wrong);
		$wrong_entries = mysqli_fetch_assoc($resultwrong);
		$rowcount2 = mysqli_num_rows($resultwrong);
		
		$select_ac_amtrec="SELECT sum(amount) as `ac_amount_rec` FROM datewise_payments_recieved  WHERE YEAR(received_date)='".$myyear."' AND MONTH(received_date)='".$mymonth."'  ";

		$resultamrec = mysqli_query($conn2,$select_ac_amtrec);
		$acamrec = mysqli_fetch_assoc($resultamrec);

		$select_wrong_details="SELECT paid as `amount_rec`,paymode FROM student_paydetails  WHERE   YEAR(paiddated)='".$myyear."' AND MONTH(paiddated)='".$mymonth."' AND  `amountpaid`='2' ";

		$qrpayrewrongdetails = mysqli_query($conn2,$select_wrong_details);

		while($qrpayrewrongdets = mysqli_fetch_assoc($qrpayrewrongdetails)) {
		$qrpayrewrong[] =$qrpayrewrongdets;
		}


		$cumulativesum += $qrpay['paid'];
		
			if($qrpayrex['amount_rec']!='')
			{
			$amt_recv=$qrpayrex['amount_rec'];		
			$qrpayrex['amount_rec'] =  $qrpayrex['amount_rec'];
			}
			else
			{
			$amt_recv=0;
			$qrpayrex['amount_rec']= "0";
			}	
		
			$finalcheq5= $wrong_entries['amount_rec']; 
			if($finalcheq5<0)
			{
			$finalcheq5= "0";
			}
			else
			{
			 $finalcheq5;
			}
			
			
			$pendingamt=0;
			
			$pendingvalue=$qrpay['paid']-$amt_recv;													
													
		    $pendingamt= $pendingvalue-$qrpayrewrong['amount_rec']; 
			
			if($pendingamt<0)
			{
				$pendingamt = "0";
			}
			else
			{
				$pendingamt;
			}
		
		
		    $wrongdetailssum=0;
			foreach($qrpayrewrongdetails as $paydet)
			{
			if(($paydet['paymode']=='Liqui Loans')||($paydet['paymode']=='Neev Finance')||($paydet['paymode']=='ABFL')||($paydet['paymode']=='Avanse'))
			{
			$wrongdetailssum += $paydet['amount_rec'];

			}
			}


			$loanvalue=$qrpayL['paidL']-$qrpayreL['amount_rec'];

			if(($loanvalue!=0)&&($wrongdetailssum>0))
			{
			$finalcheq2= $loanvalue-$wrongdetailssum; 
			}
			else
			{
			$finalcheq2= $loanvalue; 	
			}
			if($finalcheq2<0)
			{
			$finalcheq2= "0";
			}
			else
			{
			$finalcheq2;
			}
		
		
			$wrongdetailssum1=0;
			foreach($qrpayrewrongdetails as $paydet)
			{
				if(($paydet['paymode']=='Cheque'))
				{
					$wrongdetailssum1 += $paydet['amount_rec'];
					
				}
			}
			
			$chequevalue=$qrpaymode['paid']-$qrpayreC['amount_rec'];
			
			if(($chequevalue!=0)&&($wrongdetailssum1>0))
			{
			$finalcheq= $chequevalue; 
			}
			else
			{
			$finalcheq=$chequevalue; 
			}
			
			if($finalcheq<0)
			{
				$finalcheq = "0";
			}
			else
			{
				$finalcheq;
			}
		
			$wrongdetailssum2=0;
			foreach($qrpayrewrongdetails as $paydet)
			{

			if(($paydet['paymode']=='Cash')||($paydet['paymode']=='Card')||($paydet['paymode']=='Online')||($paydet['paymode']=='Wire Transfer')||($paydet['paymode']=='Paytm')||($paydet['paymode']==' '))
			{
			$wrongdetailssum2 += $paydet['amount_rec'];

			}

			}
			$othersvalue=($qrpayO['paidO']+$qrpayOthers['paidOthers'])-$qrpayreO['amount_rec'];
			if($wrongdetailssum2>0)
			{
			$finalcheq3= $othersvalue-$wrongdetailssum2;													
			}
			else
			{
			$finalcheq3= $othersvalue; 
			}

			if($finalcheq3<0)
			{
			$finalcheq3= "0";
			}
			else
			{
			$finalcheq3;
			}
			
			$ac_am_rec= $acamrec['ac_amount_rec']; 
			if($ac_am_rec<0)
			{
			$ac_am_rec = "0";
			}
			else
			{
			$ac_am_rec = $ac_am_rec;
			}

		$dateObj   = DateTime::createFromFormat('!m', $mymonth);
		$monthName = $dateObj->format('M');
		$diplayerdate =  $monthName."-".$myyear;
		$finalpending=0;
		if($rowcount2 !="")
		{
			$finalpending=$pendingamt; 
		}
		else
		{
			$finalpending="0";
		}
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:left;'>". $i."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white;text-align:center;'>".$diplayerdate."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".(int)$qrpay['paid']."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".$amt_recv."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color:  #355196; color:white; text-align:right;'>".$ac_am_rec."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".$finalcheq5."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".$finalpending."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".$finalcheq2."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196; color:white; text-align:right;'>".$finalcheq."</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #355196;color:white; text-align:right;'>".$finalcheq3."</td>
		
		";
		
		$html .="</tr>";
		$amtrce= $amtrce + $amt_recv;
		$penamt= $penamt + $pendingamt;
		$financecom=$financecom+$finalcheq2;
		$cheque=$cheque+$finalcheq;
		$others=$others+$finalcheq3;
		$wrongentry=$wrongentry+$finalcheq5;
		$totalpaid=$totalpaid+$qrpay['paid'];
		$blanksvar=$blanksvar+$finalcheq4;
		$ac_amt_rec=$ac_amt_rec+$ac_am_rec;		
		$i++;
		}
		
		}
		
		$html .="<tr><td colspan=2 style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>Total Amount  : </strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$totalpaid."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$amtrce."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$ac_amt_rec."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$wrongentry."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$penamt."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$financecom."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$cheque."</strong></td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:right;'><strong>".$others."</strong></td>
		</tr>";
		
		$html .="<tr><td colspan=10 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Development Team.
		</td>
		</tr>
		<tr>
		<td colspan=10 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		Team EduPristine</td>
		</tr>

		</table>";
		
		$subject = "EduPristine - Transaction summary report";
		//echo $html;
		
		send_smt_mail($subject,$html);
	
		

       /* $output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();*/
		
		header("Location: http://one.edupristine.com/summary_daily_transaction_mail.php");


?>