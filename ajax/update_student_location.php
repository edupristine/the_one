<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if(isset($_POST['student_id'])  &&  !empty($_POST['student_id']))
	{
		$student_id = get_post_value('student_id');
		$new_location = get_post_value('new_location');
		
             
		$select_stucourse="SELECT loc_id FROM `students`  WHERE  `id`='".$student_id."'";
	    $result_stucourse = Select($select_stucourse,$conn,"issuance");
		
		$student_og_location=$result_stucourse['rows'][0]['loc_id'];
		

		$select_skuid="SELECT sku_id FROM `issuance`  WHERE  `student_id`='".$student_id."'";
	    $result_skuid = Select($select_skuid,$conn,"issuance");
		
		foreach($result_skuid['rows'] as $sku)
		{
		     
            $update_stock = "UPDATE skus_locations set sku_qty_req = sku_qty_req - 1 WHERE sku_id = '".$sku['sku_id']."'   and loc_id = ".$student_og_location."";
			$result_stock = Update($update_stock,$conn,"skus_locations");
			//echo $update_stock."<br>"; 
			
			$update_stock2 = "UPDATE skus_locations set sku_qty_req = sku_qty_req + 1 WHERE sku_id = '".$sku['sku_id']."'  and loc_id = ".$new_location."";
			$result_stock2 = Update($update_stock2,$conn,"skus_locations");
            //echo $update_stock2."<br>"; 
			
		}
		

		$update_student_loc = "UPDATE `students` set loc_id = '".$new_location."' WHERE id = '".$student_id."'";
		$result_student_loc = Update($update_student_loc,$conn,"students");
        //echo $update_student_loc."<br>"; 
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>