<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/referrel_mail.php';
$db_server='52.77.5.117';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$convtiger = new mysqli($db_server, $db_username, $db_password, 'vtigercrm6');

try{
	if(isset($_POST['student_id'])  && !empty($_POST['student_id']))
	{
	$student_id = get_post_value('student_id');
	$status = get_post_value('status');
	$comment = get_post_value('comment');
	$id=$student_id;
	
	
	$query_details = "SELECT * FROM `student_referral_requests` WHERE `id`=".$id." ";
	$result2 = mysqli_query($convtiger,$query_details);
	$result_details = mysqli_fetch_assoc($result2);
	
	
	$sel_assigneduser1 = "SELECT user_name as `user`,email1 as `email`
	FROM `vtiger_users` 
	WHERE id='".$result_details['created_by']."' ";

	$result1 = mysqli_query($conn1,$sel_assigneduser1);
	$res_assigneduser1 = mysqli_fetch_assoc($result1);
	
	$email=$res_assigneduser1['user'];
	$uname=$res_assigneduser1['email'];
	
	$query_dupcheck = "SELECT count(`id`) as count FROM `referral_requests_details` WHERE `approver_id` in (SELECT user_id FROM edupristine_one.users_teams  where  team_id in (8,10)) and `referral_req_id`=".$student_id." ";
	$result3 = mysqli_query($convtiger,$query_dupcheck);
	$result_dupcheck = mysqli_fetch_assoc($result3);
	
	
	if($result_dupcheck['count']!=0){
	$query_update = "UPDATE `referral_requests_details` SET approval_status='".$status."',`comments`='".$comment."', created_at=now() , system='one' where `approver_id`='".$_SESSION['USER_ID']."' and `referral_req_id`=".$student_id." ";
	$result4 = mysqli_query($convtiger,$query_update);
	}
    else 
	{
	$query_insert = "INSERT INTO `referral_requests_details` (`referral_req_id`, `approver_id`, `approval_email_sent`, `approval_status`, `comments`,`created_at`,`system`)
	VALUES('".$student_id."','".$_SESSION['USER_ID']."','1','".$status."','".$comment."',now(),'one')";
	$result5 = mysqli_query($convtiger,$query_insert);
	}
    
	
	$query_user = "SELECT `user_name` FROM `users` WHERE `id`='".$_SESSION['USER_ID']."'";
	$result_user = Select($query_user,$conn);
	
	
    if($status=="0")
	{
      
		//Reject  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the referral request raised which is Rejected by Accounts</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:40%;'><b>
        Refered From Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:60%;'><b>".$result_details['student_name']."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:40%;'>Refered From Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:60%;'>
		".$result_details['account_no']."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:40%;'>Refered To Student Name:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:60%;'>
		".$result_details['refered_student_name']."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:40%;'>Refered To Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:60%;'>
		".$result_details['refered_account_no']."</td></tr>";
	
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:40%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:60%;'>
		".$comment."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:40%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:60%;'>
		".$result_user['rows'][0]['user_name']."</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Referrel request Rejected";
		
		send_smt_mail_reject($subject,$html,$email,$uname);
	}		

	
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>