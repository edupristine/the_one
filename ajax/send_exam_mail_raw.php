<?php
/*if($_SESSION['USER_ID']!=1)
{
	echo "Not Authorized";
	exit;
}*/

//
/* Template Name: Mail Sender
 * Description: Exam Info Link Sender
 * Version: 1.0
 * Created On: 08 Feb, 2022
 * Created By: Hitesh
 **/

 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php'; 
setlocale(LC_MONETARY, 'en_IN'); 
function send_smt_mail($subject,$email_msg,$email,$email_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('feedback@edupristine.com', 'EduPristine Feedback');
		$mail->addReplyTo('care@edupristine.com', 'EduPristine Care');
		$mail->addAddress($email,$email_name);
	   
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
		$a_date = date('d-m-Y');
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$l_date = $date->format('d-m-Y');

		$query_mail = "SELECT * FROM `exam_survey_mail` where mail_sent=0 ";
		$result_mail = Select($query_mail,$conn);
		$i=1;
	    foreach($result_mail['rows'] as $mail)
	    {
			
		$unique_stu_id = random_strings(12);
		$url_link = "http://one.edupristine.com/student_onl_details.php?id=".base64_encode($unique_stu_id)."";	
				
		$html="";
		$html .= "<center><table style='border: 0px solid #327bbe;border-collapse: collapse; width: 80%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 24px;'><b><center><img src='http://one.edupristine.com/ajax/logo.png' height='60px'/></center></b></td>
		</tr>
		<tr style='height:30px;'>
		<td colspan=2>
		<p>
		</td>
		</tr>
	    <tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ! ".$mail['first_name']." ".$mail['last_name']."</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		EduPristine wishes you good health and a successful career. 
		<p>
		You are receiving this survey invite due to the training you are undergoing with us. Your feedback is very important to us and will allow us to improve our programs and services. 
		</p>
		<p>
		Please keep us updated about your examination and preparations so that we can conduct extra doubt solving sessions or mock test which will help you prepare for your ".$mail['course']." examination better. <font style='background-color:#FFFF00;'><a href='".$mail['link']."'>".$mail['link']."</a></font></p>
				
		<p>
		Please provide us the information related to your past and upcoming exams by using the below link.<font style='background-color:#FFFF00;'></font>
		</p>
		
		<p>
		The link will be active from ".$a_date." to ".$l_date.".
		</p>
		
	
		
		
		<p>
		Link: <font style='background-color:#FFFF00;'><a href='".$url_link."' target=_blank>".$url_link."</a></font>
		</p>
		
		
		<p>
		You can also copy-paste the URL below into your internet browser:  LINK- <a href='".$url_link."' target=_blank>".$url_link."</a>
		</p>
		</td></tr>";

		

		$html .="<tr><td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		We value your time and efforts in providing us your honest feedback, hence we have an Amazon e-voucher worth Rs __ as a token of appreciation for you. </td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		Thank you in advance for your valuable time and patience! 
		<p>
		EduPristine 
		</p>
		</td>
		</tr>
		<tr>
		<td colspan=2>
		<p style='text-align:justify;'>
		* All questions in the survey must be completed in order to qualify for Amazon e-vouchers. Survey submitted without answers to the question will be deleted from the final response set. Amazon e-vouchers shall be digitally circulated and EduPristine / Neev Knowledge Management Pvt. Ltd. (“Company”) shall not be responsible for any misuse of the vouchers or in case the vouchers get utilized by individuals other than the responders to the survey. The users of the above-mentioned incentives shall be bound by the terms of use of the respective service provider.  
		</p>
		</td>
		</tr>
		

		
	

		
		
		
        <tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 12px;'><i>2022 © EduPristine. All rights reserved.</i></td>
		</tr>
		</table></center>";
		
		$subject = "".$mail['first_name'].", EduPristine needs your feedback";
		//echo $html."<br>";
		$email=$mail['email'];
		$email_name=$mail['first_name']." ".$mail['last_name'];
		///////////////////////send_smt_mail($subject,$html,$email,$email_name);
		
		$update_mail = "Update`exam_survey_mail` set mail_sent=1,link='".$url_link."',unique_id='".$unique_stu_id."',mail_sent_on=now(),attempts=1 where id='".$mail['id']."'";
	    echo $update_mail;
	////////////////////  //  $update_mail = Update($update_mail,$conn);

		
		echo $i.") ". $email." - Mail Sent\n";
		$i++;
        }
		
		
// This function will return a random 
// string of specified length 
function random_strings($length_of_string) 
{ 

// String of all alphanumeric character 
$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 

// Shufle the $str_result and returns substring 
// of specified length 
return substr(str_shuffle($str_result),  
0, $length_of_string); 
} 

function roundoff($n)  
{  
   $a = (int)($n / 100) * 100;  
   $b = ($a + 100);  
   return ($n - $a > $b - $n) ? $b : $a;  
} 
?>

