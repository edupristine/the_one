<?php
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.interakt.ai/v1/public/track/users/',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
	"userId": "'.$da['ac_id'].'", 
    "countryCode": "+91",
    "phoneNumber": "'.$da['phone'].'",
	"traits": {
		"name": "'.$da['student_name'].'",
		"email": "'.$da['primary_email'].'"
	}
}',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic ZWJQRlBqdVJTT1BINGE5c1ItaXBhM1VzZDUyTWJ6WEpaajd0MWp3MnZjczo=',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$user_response = $response;
