<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['faculty']) && !empty($_POST['faculty']))
	{
	$location_id = $_SESSION['U_LOCATION_ID'];	
?>
	<table class="table table-striped- table-bordered table-hover table-checkable" id="">
		<thead>
			<tr>
				<th style="width:2%;">#</th>
				<th style="width:25%;">SKU Name</th>
				<th style="width:10%;"><center>Allocated On</center></th>
				<th style="width:10%;"><center>Returned On</center></th>
				<th style="width:10%;"><center>Transfered On</center></th>
				<th style="width:10%;"><center>Transfered From</center></th>
				<th style="width:10%;"><center>SKU Inv Qty</center></th>
				<th style="width:15%;">Allocate / Return</th>
			</tr>
		</thead>
		
		
		
		<tbody id="rows_div">
		<?php 
			$query_skus = "SELECT sl.id,s.sku_name,l.loc_name,(sl.sku_pre_qty+sl.sku_qty_avl) as sku_available,sl.sku_qty_avl as sku_og,sl.sku_pre_qty as sku_pre_og,sl.sku_id,sl.loc_id FROM `skus_locations` sl,`skus` s, `locations` l WHERE sl.sku_id=s.id and loc_id in ('".$location_id."')
and sl.loc_id=l.id  order by l.id,s.sku_name";
			$result_skus = Select($query_skus,$conn);
			$i=0;
			foreach($result_skus['rows'] as $sku)
			{
			$allocater_check = "SELECT DATE_FORMAT(allocated_on, '%d-%m-%Y') as allocated_on,DATE_FORMAT(returned_on, '%d-%m-%Y') as returned_on,status,DATE_FORMAT(transfered_on, '%d-%m-%Y') as transfered_on,transfered_from from faculties_skus WHERE `faculty_id`='".$_POST['faculty']."' AND `sku_id`='".$sku['id']."'";
			
			$result_check = Select($allocater_check,$conn);
			$result_allot_on = $result_check['rows'][0]['allocated_on'];	
			$result_return_on = $result_check['rows'][0]['returned_on'];
			$result_transfer_on = $result_check['rows'][0]['transfered_on'];
			$result_transfer_from = $result_check['rows'][0]['transfered_from'];
			
			$faculties_get_val = "SELECT * FROM faculties where `id` = '".$result_transfer_from."'";
			$result_faculties = Select($faculties_get_val,$conn);
			$transfered_from_faculty_name = $result_faculties['rows'][0]['faculty_name'];
			
            $result_fbstatus = $result_check['rows'][0]['status'];

            $dropval="<option value='0' selected>Select</option>
				<option value='1'>Allocate</option>
				<option value='2'>Return</option>
				<option value='3'>Transfer</option>
				";
			$endsable="";	
			$dispcolor="#000000";	
                if($result_fbstatus=='0')
				{
				$dropval= "<option value='0' selected>Select</option>
				<option value='1'>Allocate</option>
				<option value='2'>Return</option>
				<option value='3'>Transfer</option>
				";
				$endsable="";	
				}
				else if($result_fbstatus=='1')
				{
				$dropval= "<option value='0' >Select</option>
				<option value='1' selected>Allocate</option>
				<option value='2'>Return</option>
				<option value='3'>Transfer</option>
				";	
				$endsable="";
                $dispcolor="red";				
				}
				else if($result_fbstatus=='2')
				{
				$dropval= "<option value='0' >Select</option>
				<option value='1' >Allocate</option>
				<option value='2' selected>Return</option>
				<option value='3'>Transfer</option>
				";
                $endsable="disabled";
                $dispcolor="green";					
				}
				else if($result_fbstatus=='3')
				{
				$dropval= "<option value='0' >Select</option>
				<option value='1' >Allocate</option>
				<option value='2' >Return</option>
				<option value='3' selected>Transfer</option>
				";
                $endsable="disabled";
                $dispcolor="green";					
				}

			
				
				$i++;?>
			<tr id="row_id_<?php echo $i; ?>">
				<td class="text-left"><input class="form-control" name="skl_<?php echo $sku['id']; ?>" id="skl_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['id']; ?>"/><?php echo $i; ?></td>
				<td class="text-left" style="font-color:<?php $dispcolor;?>;"><input class="form-control" name="sku_<?php echo $sku['id']; ?>" id="sku_<?php echo $sku['id']; ?>"  type="hidden" value="<?php echo $sku['sku_id']; ?>"/><?php echo $sku['sku_name']; ?></td>
				<td><?php echo $result_allot_on;?></td>
				<td><?php echo $result_return_on;?></td>
				<td><?php echo $result_transfer_on;?></td>
				<td><?php echo $transfered_from_faculty_name;?></td>
				<td class="text-center"><input class="form-control" name="qty_pre_<?php echo $sku['id']; ?>" id="qty_pre_<?php echo $sku['id']; ?>"  type="text" value="<?php echo $sku['sku_og']; ?>" hidden /><input class="form-control" name="qty_pre_qty_<?php echo $sku['id']; ?>" id="qty_pre_qty_<?php echo $sku['id']; ?>"  type="text" value="<?php echo $sku['sku_pre_og']; ?>" hidden /><?php echo $sku['sku_available']; ?></td>
				<td >
				<select class="form-control" id="allocateret_<?php echo $sku['id']; ?>" name="allocateret_<?php echo $sku['id']; ?>" <?php echo $endsable;?>    onchange="display_transfer(this.value,<?php echo $sku['id']; ?>);">
				<?php
				echo $dropval;
				?>
				</select>
				<br>
				<div id="display_me_<?php echo $sku['id']; ?>" name="display_me_<?php echo $sku['id']; ?>" style="display:none;">
				<label>Transfering From <?php echo $result_transfer_from;?></label>
				<select id="transfered_from_<?php echo $sku['id']; ?>" name="transfered_from_<?php echo $sku['id']; ?>" class="form-control" >  
				<option value="0">Select</option>
				<?php 
				$query_faculties = "SELECT * FROM faculties order by faculty_name";
				$result_faculties = Select($query_faculties,$conn);
				$i=0;
				foreach($result_faculties['rows'] as $faculty)
				{
				if($faculty['id']==$result_transfer_from)	
				{
				?>
				<option value="<?php echo $faculty['id'];?>" selected>
				<?php echo $faculty['faculty_name'];?></option>

				<?php
				}
				else
				{
				?>
				<option value="<?php echo $faculty['id'];?>">
				<?php echo $faculty['faculty_name'];?></option>	
				<?php	
				}
				}
				?>
				</select>
				</div>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>

<?php
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	print_r($ex);
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>
