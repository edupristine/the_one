<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';/*liveuser*/
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
$con_pay=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'payment_generation');


try{
	if(isset($_POST['org_no'])  &&  !empty($_POST['org_no']))
	{
		$student_name = get_post_value('student_name');
		$org_no = get_post_value('org_no');
	    $course = get_post_value('course');
		$city = get_post_value('city');
		$Assigned_to = get_post_value('Assigned_to');
		$paid = get_post_value('paid');
		$paiddated = $_POST['paiddated'];
		$mode = get_post_value('mode');
		$trans_id = get_post_value('trans_id');
		$amtpaid = get_post_value('amtpaid');
		$expecteddate = get_post_value('expecteddate');
		$reasons = get_post_value('reasons');
		$comments = get_post_value('comments');
		$trans_column = get_post_value('trans_column');
		$acc_id = get_post_value('acc_id');
		
		
        $select_rec="SELECT count(id) as `counts` FROM student_paydetails  WHERE `org_no`='".$org_no."'  AND  `trans_column`='".$trans_column."' ";
		$result_rec = Select($select_rec,$conn,"student_paydetails");

		if(($result_rec['rows'][0]['counts'])==0)
		{
	    $insert_paydetails = "INSERT INTO student_paydetails (
		`student_name`,
		`org_no`,
		`course`,
		`city`,
		`assigned_to`,
		`paid`,
		`paiddated`,
		`paymode`,
		`transaction_id`,
		`amountpaid`,
		`expecteddate`,
		`reasons`,
		`comments`,
		`trans_column`
		)values(
		'".$student_name."',
		'".$org_no."',
		'".$course."',
		'".$city."',
		'".$Assigned_to."',
		'".$paid."',
		'".$paiddated."',
		'".$mode."',
		'".$trans_id."',
		'".$amtpaid."',
		'".$expecteddate."',
		'".$reasons."',
		'".$comments."',
		'".$trans_column."'
		)";
		$result_inspaydetails = Insert($insert_paydetails,$conn,"student_paydetails");
		}
		else
		{
		 $update_paydetails = "Update student_paydetails set 
		`amountpaid`='".$amtpaid."',
		`expecteddate`='".$expecteddate."',
		`reasons`='".$reasons."',
		`comments`='".$comments."',
		`trans_column`='".$trans_column."'
		 WHERE `org_no`='".$org_no."' AND  `trans_column`='".$trans_column."'";
		$result_inspaydetails = Update($update_paydetails,$conn,"student_paydetails");	
		}
		
		//To Validate payment directly to CRM on Payment Received Option selected YES 
		$today = date("Y-m-d");
		$crmquery="";
       $trans_column_main='';		
		if($amtpaid=="1")
		{
			if($trans_column=='cf_1307')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1309 = 1, scf.cf_1311 = '".$today."' WHERE a.accountid = ".$acc_id." ";
                $trans_column_main='cf_1303';	
			}
			elseif($trans_column=='cf_1317')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1319 = 1, scf.cf_1321 = '".$today."' WHERE a.accountid = ".$acc_id." ";
				$trans_column_main='cf_1313';
			}
			elseif($trans_column=='cf_1327')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1329 = 1, scf.cf_1331 = '".$today."' WHERE a.accountid = ".$acc_id." ";
				$trans_column_main='cf_1323';
			}
			elseif($trans_column=='cf_1337')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1339 = 1, scf.cf_1341 = '".$today."' WHERE a.accountid = ".$acc_id." ";
				$trans_column_main='cf_1333';
			}
			elseif($trans_column=='cf_1347')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1349 = 1, scf.cf_1351 = '".$today."' WHERE a.accountid = ".$acc_id." ";
				$trans_column_main='cf_1343';
			}
			elseif($trans_column=='cf_1357')
			{
				$crmquery = "UPDATE vtiger_account a LEFT JOIN vtiger_accountscf scf ON scf.accountid = a.accountid 
				SET scf.cf_1359 = 1, scf.cf_1361 = '".$today."' WHERE a.accountid = ".$acc_id." ";
				$trans_column_main='cf_1353';
			}
			
			//echo $crmquery."<br>";
			$resultqp = mysqli_query($conn1,$crmquery);
			
			$sel_regcode = "SELECT sc.accountid as `accountid`,ac.account_no as `org_no`,sc.cf_1495,ROUND(sc.cf_936,0) AS `offered_price`,ROUND(sc.cf_938,0) AS `Paid`,ROUND(sc.cf_940,0) AS `Balance`,ROUND(sc.cf_1291,0) as `discount`,ac.accountname AS `studentname`, sc.cf_1621 AS `MRP`, sc.cf_1109 AS `first_name`, sc.cf_1111 AS `last_name`
			,ac.phone AS phone,ac.email1 AS email,sc.cf_1601 AS batch_date,sc.cf_1629 AS batch_location,sc.cf_918 AS course,DATE_FORMAT(ce.createdtime, '%d-%m-%Y') AS createdat,sc.cf_1513 AS subcourse,
			ROUND(sc.cf_1303,0) AS t_amt_1,sc.cf_1363 AS t_dat_1,sc.cf_1305 AS t_mod_1,sc.cf_1307 AS t_tra_1,sc.cf_1309 AS t_valid_1, sc.cf_1449 AS `t_rc_1`, sc.cf_1311 AS `t_valdt_1`,
			ROUND(sc.cf_1313,0) AS t_amt_2,sc.cf_1365 AS t_dat_2,sc.cf_1315 AS t_mod_2,sc.cf_1317 AS t_tra_2,sc.cf_1319 AS t_valid_2, sc.cf_1451 AS `t_rc_2`, sc.cf_1321 AS `t_valdt_2`,
			ROUND(sc.cf_1323,0) AS t_amt_3,sc.cf_1367 AS t_dat_3,sc.cf_1325 AS t_mod_3,sc.cf_1327 AS t_tra_3,sc.cf_1329 AS t_valid_3, sc.cf_1453 AS `t_rc_3`, sc.cf_1331 AS `t_valdt_3`,
			ROUND(sc.cf_1333,0) AS t_amt_4,sc.cf_1369 AS t_dat_4,sc.cf_1335 AS t_mod_4,sc.cf_1337 AS t_tra_4,sc.cf_1339 AS t_valid_4, sc.cf_1455 AS `t_rc_4`, sc.cf_1341 AS `t_valdt_4`,
			ROUND(sc.cf_1343,0) AS t_amt_5,sc.cf_1371 AS t_dat_5,sc.cf_1345 AS t_mod_5,sc.cf_1347 AS t_tra_5,sc.cf_1349 AS t_valid_5, sc.cf_1457 AS `t_rc_5`, sc.cf_1351 AS `t_valdt_5`,
			ROUND(sc.cf_1353,0) AS t_amt_6,sc.cf_1373 AS t_dat_6,sc.cf_1355 AS t_mod_6,sc.cf_1357 AS t_tra_6,sc.cf_1359 AS t_valid_6, sc.cf_1459 AS `t_rc_6`, sc.cf_1361 AS `t_valdt_6`,ce.smownerid as `assigned_user_id`
			FROM vtiger_account ac
			LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
			INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
			WHERE ce.smownerid!='3920'  
			and  ac.accountid='".$acc_id."'";

			$result = mysqli_query($conn1,$sel_regcode);
			$row = mysqli_fetch_assoc($result);
			
			
			$accountid=	$row['accountid'];
			$account_no=$row['org_no'];
			$studentname=$row['studentname'];
			$first_name = $_POST['first_name'];
			$last_name = $_POST['last_name'];
			$email=$row['email'];
			$phone=$row['phone'];
			$course=$row['course'];
			$subcourse=$row['variant'];
			$assigned_user_id=$row['assigned_user_id'];
			$new_org_date=$row['org_createdtime'];
			$offered_price=$row['offered_price'];
			//$paid=$row['Paid'];
			
			$t_amt_1 = $row['t_amt_1'];
			$t_amt_2 = $row['t_amt_2'];
			$t_amt_3 = $row['t_amt_3'];
			$t_amt_4 = $row['t_amt_4'];
			$t_amt_5 = $row['t_amt_5'];
			$t_amt_6 = $row['t_amt_6'];
			$t_valid_1 = $row['t_valid_1'];
			$t_valid_2 = $row['t_valid_2'];
			$t_valid_3 = $row['t_valid_3'];
			$t_valid_4 = $row['t_valid_4'];
			$t_valid_5 = $row['t_valid_5'];
			$t_valid_6 = $row['t_valid_6'];
			
			$validated=0;
			if(($t_amt_1!='')&&($t_valid_1=='1'))
			{
			$validated = $validated + $t_amt_1;
			}
			if(($t_amt_2!='')&&($t_valid_2=='1'))
			{
			$validated = $validated + $t_amt_2;
			}
			if(($t_amt_3!='')&&($t_valid_3=='1'))
			{
			$validated = $validated + $t_amt_3;
			}
			if(($t_amt_4!='')&&($t_valid_4=='1'))
			{
			$validated = $validated + $t_amt_4;
			}
			if(($t_amt_5!='')&&($t_valid_5=='1'))
			{
			$validated = $validated + $t_amt_5;
			}
			if(($t_amt_6!='')&&($t_valid_6=='1'))
			{
			$validated = $validated + $t_amt_6;
			}

			$unvalidated=0;
			if(($t_amt_1!='')&&(($t_valid_1=='')||($t_valid_1=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_1;
			}
			if(($t_amt_2!='')&&(($t_valid_2=='')||($t_valid_2=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_2;
			}
			if(($t_amt_3!='')&&(($t_valid_3=='')||($t_valid_3=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_3;
			}
			if(($t_amt_4!='')&&(($t_valid_4=='')||($t_valid_4=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_4;
			}
			if(($t_amt_5!='')&&(($t_valid_5=='')||($t_valid_5=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_5;
			}
			if(($t_amt_6!='')&&(($t_valid_6=='')||($t_valid_6=='0')))
			{
			$unvalidated = $unvalidated + $t_amt_6;
			}
					
					
			
			$full_paid=$t_amt_1 + $t_amt_2 + $t_amt_3 + $t_amt_4 + $t_amt_5 + $t_amt_6;
		
		   
			
			
		/*	$sqlchk = "SELECT id FROM payment_records where acc_id='".$acc_id."' and transaction_column='".$trans_column_main."'";
			$result = mysqli_query($con_pay,$sqlchk);
			$rowcount=mysqli_num_rows($result);
			if($rowcount==0)
			{
				//$insert_paytrans="INSERT INTO `payment_records`(`acc_id`, `organization_no`, `name`, `email`, `phone`,`course`,`subcourse`,`assigned_to`,`assignee_id` ,`organization_created_on`,`offered_price`,`transaction_column`,`transaction_amount`,`transaction_mode`,`transaction_date`,`new_balance`,`transaction_id`,`validated`,`validated_date`,`receipt`,`transaction_type`,`org_created`,`first_name`,`last_name`,`rec_val_up`) VALUES ('".$acc_id."','".$account_no."','".$studentname."','".$email."','".$phone."','".$course."','".$subcourse."','','".$assigned_user_id."','".$new_org_date."','".$offered_price."','".$trans_column_main."','".$paid."','".$mode."','".$paiddated."','','".$trans_id."','1','".$today."','','CRM Payment',1,'".$first_name."','".$last_name."',1)";

				//echo $insert_paytrans."<br>";
				//$result = mysqli_query($con_pay,$insert_paytrans);
			
				//$sqlchk2 = "SELECT id  FROM payment_selfbalance where acc_id='".$acc_id."'";
				//echo $sqlchk2;
				/*	$result_query = mysqli_query($con_pay,$sqlchk2);
					$resultx = mysqli_fetch_assoc($result_query);*/
					//print_r($resultx);
					
					//echo "rowcounterssx";print_r($resultx['id']); echo "<br>";
					/*
					
					if($resultx['id']=='')
					{
					$available_balance=0;
					$available_balance=$offered_price-($validated + $unvalidated);	
					$insert_paytrans="INSERT INTO `payment_selfbalance`(`acc_id`, `organization_no`,`offered_price`,`paid`,`validated_transaction_amount`,`unvalidated_transaction_amount`,`available_balance`) VALUES ('".$acc_id."','".$account_no."','".$offered_price."','".$full_paid."','".$validated."','".$unvalidated."','".$available_balance."')";

					$rowsc=$insert_paytrans;*/
					//echo $insert_paytrans;
				/*	$result2 = mysqli_query($con_pay,$insert_paytrans);
				
						
					}else if($resultx['id']!='')
					{
					$query = "SELECT available_balance as `available_balance`,unvalidated_transaction_amount as `unvalidated_transaction_amount`,validated_transaction_amount as `validated_transaction_amount`,offered_price as `offered_price` FROM `payment_selfbalance` WHERE acc_id ='".$accountid."'";	

					$result = mysqli_query($con_pay,$query);			
					$rx = mysqli_fetch_assoc($result);
					$balance = $rx['available_balance'];
					$unvalidated_transaction_amount = $rx['unvalidated_transaction_amount'];
					$validated_transaction_amount = $rx['validated_transaction_amount'];
					$offered_price = $rx['offered_price'];



					$new_validated_transaction_amount = $validated_transaction_amount + $paid;	
					$new_unvalidated_transaction_amount = $unvalidated_transaction_amount - $paid;	
					$new_balance = $offered_price-($new_unvalidated_transaction_amount + $new_validated_transaction_amount);
					$update_paytrans2 = "UPDATE `payment_selfbalance` SET `paid`='".$full_paid."',`validated_transaction_amount`='".$new_validated_transaction_amount."' , `unvalidated_transaction_amount`='".$new_unvalidated_transaction_amount."', `available_balance`='".$new_balance."' WHERE acc_id='".$accountid."'";
*/
					//echo $update_paytrans2."<br>";
					/*$result2 = mysqli_query($con_pay,$update_paytrans2);
					}
				
				
			}else
			{*/
				
				/*$sqlchkup = "SELECT id,transaction_column FROM payment_records where acc_id='".$acc_id."'  and unique_pay_id!=''   AND pay_link_expired=0 order by id asc";
				$resultchup = mysqli_query($con_pay,$sqlchkup);
				while($resultxup = mysqli_fetch_assoc($resultchup))
				{
				$trans_last_col=$resultxup['transaction_column'];
				$val_ex=explode("_",$trans_last_col);
				$val_nb=$val_ex[1]+10;
				$new_trans_val="cf_".$val_nb;

				$sqlchkup2 = "UPDATE payment_records SET transaction_column='".$new_trans_val."' where id='".$resultxup['id']."' ";
				//echo $sqlchkup2."<br>";
				$resultchupXX = mysqli_query($con_pay,$sqlchkup2);
				}*/
				
				//$insert_paytrans="INSERT INTO `payment_records`(`acc_id`, `organization_no`, `name`, `email`, `phone`,`course`,`subcourse`,`assigned_to`,`assignee_id` ,`organization_created_on`,`offered_price`,`transaction_column`,`transaction_amount`,`transaction_mode`,`transaction_date`,`new_balance`,`transaction_id`,`validated`,`validated_date`,`receipt`,`transaction_type`,`org_created`,`first_name`,`last_name`,`rec_val_up`) VALUES ('".$acc_id."','".$account_no."','".$studentname."','".$email."','".$phone."','".$course."','".$subcourse."','','".$assigned_user_id."','".$new_org_date."','".$offered_price."','".$trans_column_main."','".$paid."','".$mode."','".$paiddated."','','".$trans_id."','1','".$today."','','CRM Payment',1,'".$first_name."','".$last_name."',1)";

				//echo $insert_paytrans."<br>";
				//$result = mysqli_query($con_pay,$insert_paytrans);
			
				$sqlchk2 = "SELECT id  FROM payment_selfbalance where acc_id='".$acc_id."'";
				//echo $sqlchk2;
					$result_query = mysqli_query($con_pay,$sqlchk2);
					$resultx = mysqli_fetch_assoc($result_query);
					
					if($resultx['id']=='')
					{
					$available_balance=0;
					$available_balance=$offered_price-($validated + $unvalidated);	
					$insert_paytrans="INSERT INTO `payment_selfbalance`(`acc_id`, `organization_no`,`offered_price`,`paid`,`validated_transaction_amount`,`unvalidated_transaction_amount`,`available_balance`) VALUES ('".$acc_id."','".$account_no."','".$offered_price."','".$full_paid."','".$validated."','".$unvalidated."','".$available_balance."')";

					$rowsc=$insert_paytrans;
				
					$result2 = mysqli_query($con_pay,$insert_paytrans);
				
						
					}else if($resultx['id']!='')
					{
					$query = "SELECT available_balance as `available_balance`,unvalidated_transaction_amount as `unvalidated_transaction_amount`,validated_transaction_amount as `validated_transaction_amount`,offered_price as `offered_price` FROM `payment_selfbalance` WHERE acc_id ='".$accountid."'";	

					$result = mysqli_query($con_pay,$query);			
					$rx = mysqli_fetch_assoc($result);
					$balance = $rx['available_balance'];
					$unvalidated_transaction_amount = $rx['unvalidated_transaction_amount'];
					$validated_transaction_amount = $rx['validated_transaction_amount'];
					$offered_price = $rx['offered_price'];



					$new_validated_transaction_amount = $validated_transaction_amount + $paid;	
					$new_unvalidated_transaction_amount = $unvalidated_transaction_amount - $paid;	
					$new_balance = $offered_price-($new_unvalidated_transaction_amount + $new_validated_transaction_amount);
					$update_paytrans2 = "UPDATE `payment_selfbalance` SET `paid`='".$full_paid."',`validated_transaction_amount`='".$new_validated_transaction_amount."' , `unvalidated_transaction_amount`='".$new_unvalidated_transaction_amount."', `available_balance`='".$new_balance."' WHERE acc_id='".$accountid."'";

					//echo $update_paytrans2."<br>";
					
					
					$result2 = mysqli_query($con_pay,$update_paytrans2);
					}
				
			//}
			
			
			
			
			
		}
		

		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>