<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';/*p3t3r5bu4g*/
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
/*For sending mails to students on resolutions*/
try{
	if( isset($_POST['recordid'])  &&  !empty($_POST['recordid']))
	{
		$record_id  = get_post_value('recordid');
		$resolution  = get_post_value('resolution');
		
		$sel_ticket = "Select ticket_no,student_name,email,account_no,concerns from  recv_grievances  where id=".$record_id;
	   
	    $result_ticket = Select($sel_ticket,$conn,"recv_grievances");
		
		$ticketno = $result_ticket['rows'][0]['ticket_no'];
		$student_name = $result_ticket['rows'][0]['student_name'];
		$email = $result_ticket['rows'][0]['email'];
		$concern = $result_ticket['rows'][0]['concerns'];
		$account_no = $result_ticket['rows'][0]['account_no'];
		
		$sql = "SELECT ac.account_no,ac.accountname,ac.email1 as `email`, ac.phone,sc.cf_918 as `course`,
		sc.cf_1495 as `reg_batch`, sc.cf_936 as `offered_price`, sc.cf_938 as `paid`, sc.cf_1497 as `at1`,sc.cf_1499 as `at2`,
		sc.cf_1501 as `at3`, sc.cf_1503 as `at4`, sc.cf_1505 as `at5`, ce.smownerid as `salesperson`, sc.cf_1629 as `center`,sc.cf_940 AS `Balance`,sc.cf_1621 as `MRP`,sc.cf_1291 as `discount`
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		LEFT JOIN `vtiger_accountbillads` ad ON ad.`accountaddressid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ce.smownerid != 3920 AND ac.account_no='".$account_no."'";
		$result1 = mysqli_query($conn1,$sql);
		$students = array();
		while($res = mysqli_fetch_assoc($result1)) {
		$students[] = $res;
		}
		
		$sel_assigneduser1 = "SELECT us.user_name as `assigned_user`,us.email1 AS `assigned_umail`,us.reports_to_id AS `reporting`
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		LEFT JOIN `vtiger_users` us ON us.id =ce.smownerid
		WHERE ce.smownerid!='3920'
		and  ac.account_no='".$account_no."'";
		
		

		$result1 = mysqli_query($conn1,$sel_assigneduser1);
		$res_assigneduser1 = mysqli_fetch_assoc($result1);
		
		 $assigned_user1= $res_assigneduser1['assigned_user'];	
         $assigned_umail1= $res_assigneduser1['assigned_umail'];
		 $assigned_reporting1= $res_assigneduser1['reporting'];
		
		$qry = "SELECT CONCAT(vu.first_name,' ',vu.last_name) as `name`,vu.email1 as `email`FROM vtiger_users vu WHERE  id=".$assigned_reporting1;
		$result = mysqli_query($conn1,$qry);
		$re = mysqli_fetch_assoc($result);
         
		 $rename= $re['name'];	
         $remail= $re['email']; 
				
		
		$html="";
		$html .= "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Hi ".$assigned_user1.",</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		One of your students has raised a query regarding the collection of balance amount.
		<p>
		Please find the details below:</p></td></tr>";
		foreach($students as $student){
		$dis="";	
		$disc = explode('|##|',$student['discount']);
       
        foreach($disc as $dsc)
		{
        $dis .= $dsc."<br>";
		}
        $findsc = $dis;		
        
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Student ID:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['account_no']."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Student Name:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['accountname']."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>City:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$student['center']."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>MRP:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['MRP']."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Discount Given:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>".$findsc."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Offered Price:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['offered_price']."</td>
		</tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Paid Amount:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['paid']."</td>
		</tr>";
		$html .= "<tr>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Balance as per CRM:</td>
		<td style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'>Rs. ".$student['Balance']."</td>
		</tr>";
		}
		
		$html .="<tr><td colspan=2 style='border: 1px solid #327bbe; padding: 5px; background-color: #ffffff; text-align:left;'><p><strong>Student comments </strong>:
		<p>
		".$concern."
		<p></td></tr>";
		$html .="<tr><td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		It would be great if you can help us with the background of your conversation with this student so that we can resolve concern as soon as possible.
		</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		Team EduPristine</td>
		</tr>

		</table>";
		
		$subject = "EduPristine - Payment Related Concerns";
		//echo $html;
		//$student_name="Hitesh Patil";
		//$email="hitesh.patil@edupristine.com";
		//send_smt_mail($subject,$html,$email,$student_name,"","",1);
		send_smt_mail($subject,$html,$assigned_umail1,$assigned_user1,$remail,$rename,0);
		
		$sel_ticket = "Update  recv_grievances set notified_counsellor=1  where id=".$record_id;
		
		$result_update = Update($sel_ticket,$conn,'recv_grievances');
		
        $output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>