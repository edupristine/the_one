<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['member_id'])  &&  !empty($_POST['member_id']))
	{
		$member_id  = get_post_value('member_id');
					
		$update_userteams = "Update users_teams set end_date=now(),del_flag=1 where id=".$member_id;
	    //echo $update_userteams."<br>";
	    $result_users_teams = Update($update_userteams,$conn,"users_teams");
		
  
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>