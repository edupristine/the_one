<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/refund_mail.php';

try{
	if(isset($_POST['student_id'])  && !empty($_POST['student_id']))
	{
		
		$student_id = get_post_value('student_id');
		
		$query_details = "SELECT `account_no`,`student_name`,`course_enrolled` FROM `student_refund_requests` WHERE `id`=".$student_id." ";
		$result_details = Select($query_details,$conn);
		$student_name=$result_details['rows'][0]['student_name'];
		$account_no=$result_details['rows'][0]['account_no'];
		$course_enrolled=$result_details['rows'][0]['course_enrolled'];
		
		$ref_trans = get_post_value('ref_trans');
		$ref_date = get_post_value('ref_date');
		$ref_amt = get_post_value('ref_amt');
		$comment = get_post_value('comment');
		

		$query_update = "UPDATE `student_refund_requests` SET actual_refund_date='".$ref_date."',actual_refund_amount='".$ref_amt."',actual_refund_transaction_id='".$ref_trans."',actual_refund_processed_by='".$_SESSION['USER_ID']."' where `id`=".$student_id."";
		$result_update = Update($query_update,$conn,'student_refund_requests');
	
    
	    //Transaction ID Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the Final Refund transaction Details</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #1F76D3; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #1F76D3; text-align:left; font-size: 14px;width:70%;'><b>".$student_name."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$account_no."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$ref_amt."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Transaction ID:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$ref_trans."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Transaction Date:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$ref_date."</td></tr>";
		
	    $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Transaction (If Any):</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comment."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Processed By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		Accounts LEVEL 2</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the Accounts Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine Accounts Team</p>";
		
		
		$subject = "Refund/S Processed $account_no / $student_name";
		send_smt_mail4($subject,$html);
	
        $output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>