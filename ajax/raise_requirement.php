<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['final_cbs_json']) && isset($_POST['student_id'])  && 
		!empty($_POST['final_cbs_json']) && !empty($_POST['student_id']))
	{
		$student_raiselocation="";
		$student_id = get_post_value('student_id');
		$student_location = get_post_value('student_loc');
		$student_raiselocation = get_post_value('raise_loc');
		
		$final_cbs_json = get_post_value('final_cbs_json');
		$cbs = stripslashes($final_cbs_json);
		$cbs = json_decode($cbs);
		$cbs = json_decode(json_encode($cbs), true);
		
		if($student_raiselocation==8)
		{   $student_location="";
			$update_student_loc = "UPDATE `students` set loc_id = 8 WHERE id = '".$student_id."'";
			$result_student_loc = Update($update_student_loc,$conn,"students");
			$student_location=8;
		}
		
		

		$insert_cour="INSERT INTO `student_couriers_request`(`student_id`, `sending_center`, `courier_status`, `courier_intiated_by`, `total_records` ) VALUES ('".$student_id."','1','Requested','".$_SESSION['USER_ID']."','".count($cbs)."')";
		$result_cour = Insert($insert_cour,$conn,"student_couriers_request");
        $insertId = $result_cour['id'];
		
		foreach($cbs as $cb)
		{
		     
			$cbnamearray = explode("_",$cb['cb_name']);
			
		    $select_req="SELECT count(id) FROM issuance  WHERE `sku_id`='".$cbnamearray[2]."' AND  `student_id`='".$cbnamearray[1]."'";
			$result_sel = Select($select_req,$conn,"issuance");
			
		
	        $insert_req="INSERT INTO `issuance`(`sku_id`, `student_id`,`courier_req_id`) VALUES ('".$cbnamearray[2]."','".$cbnamearray[1]."','".$insertId."')";
		    $result_req = Insert($insert_req,$conn,"issuance");
			
			$update_stock = "UPDATE skus_locations set sku_qty_req = sku_qty_req + 1 WHERE sku_id = '".$cbnamearray[2]."'   and loc_id = 1";
		    $result_stock = Update($update_stock,$conn,"skus_locations");

			
		}
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>