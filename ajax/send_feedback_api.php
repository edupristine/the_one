<?php

if (! function_exists ( 'curl_version' )) {
    exit ( "Enable cURL in PHP" );
}
else
{
	echo "Curl Present";
}

exit;

// User data to send using HTTP POST method in curl
//student_name=Participant+11&email=test%40edupristine.com&phone=0000000000&f1_knowledge=5&f2_key_concepts=5&f3_solving_skills=5&f4_coverage=5&f5_content_quality=5&f6_infra_rating=5&f7_cs_rating=5&f8_fac_reporting_time=2&f9_class_end_time=2&f10_comments=TEST&student_availability=Yes&oper=add&id=_empty


$data = array('student_name'=>'Participant11','email'=>'test@edupristine.com', 'phone' => '0000000000', 'f1_knowledge' => '5', 'f2_key_concepts' => '3', 'f3_solving_skills' => '1', 'f4_coverage' => '2', 'f5_content_quality' => '1', 'f6_infra_rating' => '2', 'f7_cs_rating' => '1', 'f8_fac_reporting_time' => '1', 'f9_class_end_time' => '1', 'f10_comments' => 'TEST', 'student_availability' => 'Yes', 'oper' => 'add', 'id' => '_empty');

// Data should be passed as json format
$data_json = json_encode($data);

// API URL to send data
$url ='http://harry.edupristine.org/api/v1/dateinfo/addFeedback?workshop_id=4616&workshop_date=2022-04-26&batch_code=1388MUMCFA1-FT&date_id=64124&faculty_id=29096&user=admin';

// curl initiate
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $url);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
curl_setopt($ch, CURLOPT_FAILONERROR, true);
// SET Method as a POST
curl_setopt($ch, CURLOPT_POST, 1);

// Pass user data in POST command
curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Execute curl and assign returned data
$response  = curl_exec($ch);
if (curl_errno($ch)) {
    $error_msg = curl_error($ch);
}
// Close curl
curl_close($ch);

// See response if data is posted successfully or any error
print_r ($response);

?>