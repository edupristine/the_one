<?php
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.interakt.ai/v1/public/message/',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "countryCode": "+91",
    "phoneNumber": "'.$da['mobile_no'].'",
    "type": "Template",
    "template": {
        "name": "thank_you_referre",
        "languageCode": "en",
        "bodyValues": [
            "'.$da['student_name'].'."
        ]
    }
}',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic ZWJQRlBqdVJTT1BINGE5c1ItaXBhM1VzZDUyTWJ6WEpaajd0MWp3MnZjczo=',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$final_response=$response;