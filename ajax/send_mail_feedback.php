<?php
/*if($_SESSION['USER_ID']!=1)
{
	echo "Not Authorized";
	exit;
}*/

//
/* Template Name: Mail Sender
 * Description: Link Sender
 * Version: 1.0
 * Created On: 15Th November, 2019
 * Created By: Hitesh
 **/
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php'; 
setlocale(LC_MONETARY, 'en_IN'); 
function send_smt_mail($subject,$email_msg,$email,$email_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('no-reply@edupristine.in', 'CMA Team - Edupristine');
		$mail->addAddress($email,$email_name);
		//$mail->AddAttachment("/home/ubuntu/webapps/the_one/ajax/CMA_Subject.pdf");
		//$mail->AddAttachment("/home/ubuntu/webapps/the_one/ajax/EDUPRISTINE_USCMA_BROUCHER.pdf");
		//$mail->AddAttachment("/home/ubuntu/webapps/the_one/ajax/CMA_Handbook.pdf");
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
	    $query_mail = "SELECT * FROM `camptest` where  sent=0 order by id desc limit 100";
	    // // $query_mail = "SELECT * FROM `camptest` where  sent=0 and id in (7923,7925)";
		$result_mail = Select($query_mail,$conn);
		$i=1;
	    foreach($result_mail['rows'] as $mail)
	    {
				
		$html="";
		$html .= "<html>
<table style='border: 0px solid #327bbe;border-collapse: collapse; width: 100%; font-family:sans-serif,Arial, Helvetica; font-size: 20px;' >
<tr>
<td colspan=2 >
<center><img src='http://one.edupristine.com/ajax/mail-sign.png' alt='EduPristine' /></center><p>
<br>
<strong>Dear Student,
<p>
Thank you for becoming part of EduPristine Family, We’d love to hear what you think of our services related to CMA . Your feedback will help us determine what features to add and how we can make the services better for you.
<p>
If you have 3 minutes, please fill out our survey by clicking on the link below<br>
<a href='https://forms.gle/imtgczNRFpNvDjS27' target=_blank>https://forms.gle/imtgczNRFpNvDjS27</a>
<p>
Thanks again!
<p>
</center>

<p>
<strong>Thanks & Regards, <p>
Sakina Dahodwala<br>
</font>
 <font color='#ADADAD'>
Senior Counselor <p>
-----------------------------------------------------<br>
EduPristine- Neev Knowledge Management Pvt. Ltd.  <br>
702, Raaj Chambers, Near Andheri East-West subway, Andheri (E), Mumbai-400 069. <br>
M - +91 7208010690 | E - sakina.dahodwala@edupristine.com | W - www.edupristine.com </font></strong><br>
<table style='border:0px;'>
<tr>
<td>
<a href='http://www.facebook.com/edupristine/' target=_blank><img src='http://one.edupristine.com/ajax/facebook.png' height='32px' alt='Facebook' /></a>
</td>
<td>
<a href='http://www.linkedin.com/edupristine/' target=_blank><img src='http://one.edupristine.com/ajax/linkedin.png' height='32px' alt='Linkedin' /></a>
</td>
<td>
<a href='http://www.twitter.com/edupristine/' target=_blank><img src='http://one.edupristine.com/ajax/twitter.png' height='32px' alt='Twitter' /></a>
</td>
<td>
<a href='http://google.com/search?q=edupristine' target=_blank><img src='http://one.edupristine.com/ajax/google.png' height='32px' alt='Google' /></a>
</td>
<td>
<a href='http://www.youtube.com/edupristine/' target=_blank><img src='http://one.edupristine.com/ajax/youtube.png' height='32px' alt='Youtube' /></a>
</td>
</tr>
</table>
</p>
<a href='https://wa.me/+917208010690/?text=Hi%20Sakina%20Maam%20,want%20to%20unsubscribe%20from%20CMA%20Mails' target=_blank><img src='http://one.edupristine.com/ajax/unsubscribe.png' height='50px' alt='EduPristine' /><font size=2px>Unsubscribe from our mails.</font></a></strong>
</td>
</tr>
</table>
</html>";
		
		$subject = "EduPristine Feedback : We want to hear from you.";
        //$subject="EDUPRISTINE (BECKER) : Live Online Classes starting soon, registration is ON, batch start from 25th August 2021";
	echo $html;
		
		// // // $email="hiteshpatil86@gmail.com";
		// // // $email_name="Hitesh Patil";
		
		$email=$mail['email'];
		$email_name=$mail['name'];
		send_smt_mail($subject,$html,$email,$email_name);
		
		$update_mail = "Update`camptest` set sent=1 where id='".$mail['id']."'";
		$update_mail = Update($update_mail,$conn);
		
		echo $i.") ". $email." - Mail Sent\n";
		$i++;
        }
?>

