<?php
	include '../inc/GenericFunctions.php';
	include '../control/core.php';
	include '../control/checklogin.php';
	include '../control/connection.php';
	try
	{
		$transfer_id = get_get_value('id');
		if($transfer_id == "")
		{
			header('location: ../transfer_list.php?tfrrecparammiss');
			exit();
		}
		
		$query_tfr = "select * from transfers where id = ".$transfer_id;
		$result_tfr = Select($query_tfr,$conn);
		$to_loc_id = $result_tfr['rows'][0]['to_loc_id'];
		$query_tfr_skus = "select * from transfers_skus where trans_id = ".$transfer_id;
		$result_tfr_skus = Select($query_tfr_skus,$conn);
		
		foreach($result_tfr_skus['rows'] as $tfr_sku)
		{
			$select = "SELECT sku_qty_avl,sku_qty_tot_in from skus_locations where loc_id = '".$to_loc_id."' AND sku_id = ".$tfr_sku['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_in'];
			$newqty = $curqty + $tfr_sku['trans_qty'];
			$newtotinqty = $totcurqty + $tfr_sku['trans_qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_in = '".$newtotinqty."' WHERE 
			loc_id = '".$to_loc_id."' AND sku_id = ".$tfr_sku['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
			
			$select = "SELECT sku_qty_avl,sku_qty_tot_out from skus_locations where loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$tfr_sku['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_out'];
			$newqty = $curqty - $tfr_sku['trans_qty'];
			$newtotoutqty = $totcurqty + $tfr_sku['trans_qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_out = '".$newtotoutqty."' WHERE 
			loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$tfr_sku['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
		}
		
		$update_order = "UPDATE transfers SET transfer_status = 'COMPLETED',transfer_completed_by = '".$_SESSION['USER_ID']."' where id = ".$transfer_id;
		$result_upd_ord = Update($update_order,$conn);
		
		header('location: ../transfer_list.php?tfrstatusupd');
		exit();
		
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
		//print_r($ex);
		header('location: ../transfer_list.php?tfrupdfailed');
		exit();
		
	}
?>