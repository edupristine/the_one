<?php
/*if($_SESSION['USER_ID']!=1)
{
	echo "Not Authorized";
	exit;
}*/

//
/* Template Name: Mail Sender
 * Description: Link Sender
 * Version: 1.0
 * Created On: 15Th November, 2019
 * Created By: Hitesh
 **/
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php'; 
setlocale(LC_MONETARY, 'en_IN'); 
function send_smt_mail($subject,$email_msg,$email,$email_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('feedback@edupristine.com', 'EduPristine Feedback');
		$mail->addReplyTo('care@edupristine.com', 'EduPristine Care');
		$mail->addAddress($email,$email_name);
	   
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
		$query_mail = "SELECT * FROM `survey_mail` where mail_sent=0 ";
		$result_mail = Select($query_mail,$conn);
		$i=1;
	    foreach($result_mail['rows'] as $mail)
	    {
				
		$html="";
		$html .= "<center><table style='border: 0px solid #327bbe;border-collapse: collapse; width: 80%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 24px;'><b><center><img src='http://one.edupristine.com/ajax/logo.png' height='60px'/></center></b></td>
		</tr>
		<tr style='height:30px;'>
		<td colspan=2>
		<p>
		</td>
		</tr>
	    <tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ".$mail['first_name']." !</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		EduPristine wishes you good health and a successful career. 
		<p>
		Being a part of EduPristine, your opinion is very important to us and will allow us to improve our programs and services. 
		</p>
		<p>
		We want to measure how happy and satisfied our students are through the EduPristine Student Satisfaction Survey. The survey link will be active from December 19 2019, to December 30, 2019: <font style='background-color:#FFFF00;'><a href='".$mail['link']."'>".$mail['link']."</a></font></p>
		
		<p>
		You can also copy-paste the URL below into your internet browser:<font style='background-color:#FFFF00;'><a href='".$mail['link']."'>".$mail['link']."</a></font>
		</p>
		
		
		</td></tr>";

		

		$html .="<tr><td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		We value your time and efforts in providing us your honest feedback, hence we have an Amazon e-voucher worth Rs 200 as a token of appreciation for you. </td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		Kind Regards,
		<br>
		EduPristine 
		<p>
		</td>
		</tr>
		<tr>
		<td colspan=2>
		<p style='text-align:justify;'>
		* All questions in the survey must be completed in order to qualify for Amazon e-vouchers. Survey submitted without answers to the question will be deleted from the final response set. Amazon e-vouchers shall be digitally circulated and EduPristine / Neev Knowledge Management Pvt. Ltd. (“Company”) shall not be responsible for any misuse of the vouchers or in case the vouchers get utilized by individuals other than the responders to the survey. The users of the above-mentioned incentives shall be bound by the terms of use of the respective service provider.  
		</p>
		</td>
		</tr>
		
		<tr>
		<td colspan=2 style='text-align:justify;'>
		<p>
		Vouchers/gift cards will be distributed to respondents within 60 days after the end of the survey period, which occurs on Monday, December 30, 2019.
        </p>
		</td>
		</tr>
		
	
		
		<tr>
		<td colspan=2 style='text-align:justify;'>
		<p>
	    Note: Individuals who have participated in the Student Satisfaction survey earlier will receive their e-vouchers as per the above-mentioned schedule.  
        </p>
		</td>
		</tr>
		
		
		
        <tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 12px;'><i>2019 © EduPristine. All rights reserved.</i></td>
		</tr>
		</table></center>";
		
		$subject = "".$mail['first_name'].", EduPristine needs your feedback";
		//echo $html;
		$email=$mail['email'];
		$email_name=$mail['first_name']." ".$mail['last_name'];
		send_smt_mail($subject,$html,$email,$email_name);
		
		$update_mail = "Update`survey_mail` set mail_sent=1 where unique_id='".$mail['unique_id']."'";
		$update_mail = Update($update_mail,$conn);
		
		echo $i.") ". $email." - Mail Sent\n";
		$i++;
        }
?>

