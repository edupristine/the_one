<?php
$db_server='13.126.164.124';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'wp_tetris');
## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

## Date search value
$searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
$searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);
$job_typechk = mysqli_real_escape_string($con,$_POST['job_type']);
$locationchk = mysqli_real_escape_string($con,$_POST['location']);
$verticalchk = mysqli_real_escape_string($con,$_POST['vertical']);
## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (jp.job_title_1 like '%".$searchValue."%' or 
	    jp.job_title_2 like '%".$searchValue."%' or 
        jp.company_name like'%".$searchValue."%'  or 
        jp.qualification like'%".$searchValue."%'  or 
        jp.salary like'%".$searchValue."%'  or 
        jp.experience like'%".$searchValue."%' ) ";
}

// Date filter
if($searchByFromdate != '' && $searchByTodate != ''){
	$rawfrom=explode("/",$searchByFromdate);
	$newfrom=$rawfrom[2]."-".$rawfrom[0]."-".$rawfrom[1];
	
	$rawto=explode("/",$searchByTodate);
	$newto=$rawto[2]."-".$rawto[0]."-".$rawto[1];
	
    $searchQuery .= " and (sa.visited_on between '".$newfrom."' and '".$newto."' ) ";
}

// Job Type
if($job_typechk != ''){

    $searchQuery .= " and (jp.job_type = ".$job_typechk.") ";
}

//Location
if($locationchk != ''){

    $searchQuery .= " and (jp.job_locations like '%".$locationchk."%') ";
}

//Vertical
if($verticalchk != ''){

    $searchQuery .= " and (jp.vertical like '%".$verticalchk."%') ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM students_visit_logs sa , job_postings jp where sa.job_id = jp.id and jp.job_type=2 and sa.email!=''");		
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM students_visit_logs sa , job_postings jp where sa.job_id = jp.id and jp.job_type=2 and sa.email!='' ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT *,sa.student_name as `name`,sa.email as `email`,sa.phone as `phone` , sa.course as `course`, jp.job_locations as `job_locations`, sa.org_no as `org_no`,sa.visited_on as `visited_on`,jp.job_title_1 as `job_profile`,sa.id as `id`,jp.company_name as `company_name` FROM students_visit_logs sa , job_postings jp where sa.job_id = jp.id  and jp.job_type=2 and sa.email!='' ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;




$empRecords = mysqli_query($con, $empQuery);
$data = array();
$is_active="";
while ($row = mysqli_fetch_assoc($empRecords)) {
	
	$final_locations="";
	$qry_loc = "SELECT location_name FROM job_locations where id in (".$row['job_locations'].") order by id desc";
	$res = mysqli_query($con,$qry_loc);
	while($rl = mysqli_fetch_assoc($res)) {
	$final_locations .= $rl['location_name'].", ";
	}
	
	
    $data[] = array(
	        
    		"name"=>$row['name'],
    		"email"=>$row['email'],
			"phone"=>$row['phone'],
			"course"=>$row['course'],
			"job_locations"=>rtrim($final_locations,", "),
			"company_name"=>$row['company_name'],
    		"org_no"=>$row['org_no'],
    		"visited_on"=>$row['visited_on'],
			"job_profile"=>$row['job_title_1'],
			"id"=>$row['id'],
			 
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
   // "query1" => $empQuery,
);

echo json_encode($response);
