<?php
/*if($_SESSION['USER_ID']!=1)
{
	echo "Not Authorized";
	exit;
}*/

//
/* Template Name: Mail Sender
 * Description: Amazon Voucher Sender
 * Version: 1.0
 * Created On: 15 June, 2022
 * Created By: Hitesh
 **/

 
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php'; 
setlocale(LC_MONETARY, 'en_IN'); 
function send_smt_mail($subject,$email_msg,$email,$email_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('feedback@EduPristine.com', 'EduPristine Feedback');
		$mail->addAddress($email,$email_name);
		$mail->addBCC('hitesh.patil@EduPristine.com', 'Hitesh Patil');

		
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}
		$a_date = date('d-m-Y');
		$date = new DateTime($a_date);
		$date->modify('last day of this month');
		$l_date = $date->format('d-m-Y');
		$l_date = "31-03-2022";

		$query_mail = "SELECT * FROM `exam_cfa_clearance` order by id desc ";
		$result_mail = Select($query_mail,$conn);
		$i=1;
	    foreach($result_mail['rows'] as $mail)
	    {
			

		
		$name="";
		$name=trim($mail['full_name']);		
		$html="";
		$html .= "<center><table style='border: 0px solid #FFFFFF;border-collapse: collapse; width: 80%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/logo.png'  height='60px;'/></b></td>
		
		</tr>
		
		<tr>
		<td  colspan=2 style='border: 0px solid #327bbe; color:white; padding: 8px 0px; text-align:center; font-size: 24px;'><b><img src='http://one.EduPristine.com/ajax/hurry.png'  height='300px' /></b></td>
		
		</tr>
		
		<tr style='height:30px;'>
		<td colspan=2>
		<p>
		</td>
		</tr>
	    <tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 17px;'>Hey ".trim($name).",</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 17px;'>
		
        <p>If you have not cleared the exam; we want to assist you to pass and if you have cleared the we need your testimonial, last date of form filling is <strong>25th July 2022</strong>.</p>
		
		<center><p><strong><a href='https://forms.office.com/r/ULPUXRmhrx' target=_blank><img src='http://one.EduPristine.com/ajax/newl4abc.PNG.jpg'  /></a></strong></p></center>
		
		 <p><strong>Note: If you have already filled this form. Kindly ignore the mail.</strong>.</p>
		
		<p>Regards,<br>
		Team EduPristine
		<br>
		EduPristine
		</p>
		
		</td></tr>";

		

		$html .="
				
        <tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 12px;'><i>2022 © EduPristine. All rights reserved.</i></td>
		</tr>
		</table></center>";
		
		$subject = "".$mail['first_name'].", Hurry Up ! CFA Exam Clearance Survey";
        echo $html."<br>";
		$email=$mail['email'];
		$email_name=$mail['first_name']." ".$mail['last_name'];
		
   

		
		send_smt_mail($subject,$html,$email,$email_name);
		
		$update_mail = "Update`exam_cfa_clearance` set mail_sent=1,mail_sent_on=now() where id='".$mail['id']."'";
	    echo $update_mail;
	    $update_mail = Update($update_mail,$conn);

		
		echo $i.") ". $email." - Mail Sent\n";
		$i++;
        }
		
		
// This function will return a random 
// string of specified length 
function random_strings($length_of_string) 
{ 

// String of all alphanumeric character 
$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 

// Shufle the $str_result and returns substring 
// of specified length 
return substr(str_shuffle($str_result),  
0, $length_of_string); 
} 

function roundoff($n)  
{  
   $a = (int)($n / 100) * 100;  
   $b = ($a + 100);  
   return ($n - $a > $b - $n) ? $b : $a;  
} 
?>

