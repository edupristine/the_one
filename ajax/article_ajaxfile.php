<?php
$db_server='13.126.164.124';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'wp_tetris');
## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

## Date search value
$searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
$searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);
$publish_type = mysqli_real_escape_string($con,$_POST['publish_type']);
## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (arct_url like '%".$searchValue."%' or 
	    synopsis like '%".$searchValue."%') ";
}

// Date filter
if($searchByFromdate != '' && $searchByTodate != ''){
	$rawfrom=explode("/",$searchByFromdate);
	$newfrom=$rawfrom[2]."-".$rawfrom[0]."-".$rawfrom[1];
	
	$rawto=explode("/",$searchByTodate);
	$newto=$rawto[2]."-".$rawto[0]."-".$rawto[1];
	
    $searchQuery .= " and (published_on between '".$newfrom."' and '".$newto."' ) ";
}

// Publish Type
if($publish_type != ''){

    $searchQuery .= " and (is_published = ".$publish_type.") ";
}



## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM articles order by id desc");
				
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM articles where id!=0 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT * FROM articles where id!=0  ".$searchQuery." order by id desc, ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;				
$empRecords = mysqli_query($con, $empQuery);
$data = array();
$is_published="";
while ($row = mysqli_fetch_assoc($empRecords)) {
	if($row['is_published']=='1')
    {
	//$is_active="<a href='#' onclick='deactivate_me(".$row['id'].")' ><i class='la la-edit'></i></a><font color='green'>Active</font>";
	$is_published="<font color='green'>Published</font>";
	} else 
	{
		$is_published="<font color='red'>UnPublished</font>";
	}
	
	
	
    $data[] = array(
	        
    		"arct_url"=>$row['arct_url'],
    		"synopsis"=>$row['synopsis'],
			"published_on"=>$row['published_on'],
			"created_at"=>$row['created_at'],
			"is_published"=>$is_published,
			"id"=>$row['id'],
			 
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
    "query1" => $empQuery,
);

echo json_encode($response);
