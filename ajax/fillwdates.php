<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['workshop_id'])  &&  !empty($_POST['workshop_id']))
	{
		$workshop_id  = get_post_value('workshop_id');
					
		$select_wdates = "SELECT ws_date FROM workshops_dates WHERE STATUS IN ('Conducted','Upcoming','Confirmed') AND workshop_id=".$workshop_id."  AND ws_date >=now() order by ws_date";
	    //echo $select_wdates."<br>";
	    $result_wdates = Select($select_wdates,$harry,"workshops_dates");
		$opt="";
		$opt .= "<select class='form-control kt-selectpicker' id='w_date' name='w_date' name='param[]' multiple>";
		$opt .= "<option value='0'>Workshop Date</option>";
		foreach($result_wdates['rows'] as $dates )
		{
		$opt .='<option value="'.$dates['ws_date'].'">'.$dates['ws_date'].'</option>';
		}
		$opt .= "</select>";
		echo $opt;
		exit();
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>