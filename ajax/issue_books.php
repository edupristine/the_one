<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['final_cbs_json']) && isset($_POST['student_id'])  && 
		!empty($_POST['final_cbs_json']) && !empty($_POST['student_id']))
	{
		$student_id = get_post_value('student_id');
		
		$final_cbs_json = get_post_value('final_cbs_json');
		$cbs = stripslashes($final_cbs_json);
		$cbs = json_decode($cbs);
		$cbs = json_decode(json_encode($cbs), true);
		
		foreach($cbs as $cb)
		{
			$cbnamearray = explode("_",$cb['cb_name']);
			$issuance_id = $cbnamearray[2];
			
			$select_sku = "SELECT (sku_qty_avl+sku_pre_qty) as sku_qty from skus_locations where sku_id = (select sku_id from issuance where id = ".$issuance_id.") and loc_id = ".$_SESSION['U_LOCATION_ID'];
			$result_sku = Select($select_sku,$conn);
			
			if($result_sku['rows'][0]['sku_qty'] < 1)
			{
				$output = array(
					"status"=>'inventoryunavailable'
					);
				$output = json_encode($output);
				echo $output;
				exit;
			}
		}
		
		foreach($cbs as $cb)
		{
			$cbnamearray = explode("_",$cb['cb_name']);
			$issuance_id = $cbnamearray[2];
			
			$update_issue = "UPDATE issuance SET issued_status = 1, issued_by = ".$_SESSION['USER_ID'].", issue_date = '".date('Y-m-d')."' WHERE id = ".$issuance_id;
			$result_update = Update($update_issue,$conn,"issuance");
			
			$update_stock = "UPDATE skus_locations set sku_qty_avl = sku_qty_avl - 1,sku_qty_req = sku_qty_req - 1,sku_qty_tot_out = sku_qty_tot_out + 1 WHERE sku_id = (select sku_id from issuance where id = ".$issuance_id.") and loc_id = ".$_SESSION['U_LOCATION_ID'];
			$result_update = Update($update_stock,$conn,"skus_locations"); 
		}
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>