<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if(isset($_POST['recordid'])  &&  !empty($_POST['recordid']))
	{
		$student_id = get_post_value('student_id');
		$recordid = get_post_value('recordid');
		$return_date = get_post_value('return_date');
		$return_comment = get_post_value('return_comment');
		
		if($return_date=="")
		{
			$return_date=date("Y-m-d");
		}
				
		$select_stucourse="SELECT loc_id FROM `students`  WHERE  `id`='".$student_id."'";
		$result_stucourse = Select($select_stucourse,$conn,"issuance");
        $student_og_location=$result_stucourse['rows'][0]['loc_id'];
		
		$select_sendloc="SELECT sending_center FROM `student_couriers_request`  WHERE  `id` in (select courier_req_id from student_couriers_records where id = '".$recordid."')";
		$result_sendloc = Select($select_sendloc,$conn,"issuance");
        $student_sending_location=$result_sendloc['rows'][0]['sending_center'];
		

		$select_skuid="SELECT sku_id FROM `issuance`  WHERE  `student_id`='".$student_id."' and courier_record_id='".$recordid."'";
	    $result_skuid = Select($select_skuid,$conn,"issuance");
		
		foreach($result_skuid['rows'] as $sku)
		{
		    //For Center Stock
			$update_cen_stock = "UPDATE skus_locations set sku_qty_avl = sku_qty_avl + 1 ,sku_qty_tot_out = sku_qty_tot_out - 1 WHERE sku_id = '".$sku['sku_id']."'   and loc_id = 1";
		    $result_cen_stock = Update($update_cen_stock,$conn,"skus_locations");
			//echo $update_cen_stock."<br>";
			
            //Issuance Table
			// $update_iss_sku = "UPDATE issuance set courier_record_id = NULL ,issued_status = 0, issued_by = 0, issue_date = NULL   WHERE sku_id = '".$sku['sku_id']."'   and student_id ='".$student_id."' and  courier_record_id='".$recordid."'";
		    // $result_iss_sku = Update($update_iss_sku,$conn,"skus_locations");
			//echo $update_iss_sku."<br>";
			
			// $update_stu_loc = "UPDATE skus_locations set sku_qty_req = sku_qty_req + 1  WHERE sku_id = '".$sku['sku_id']."'   and loc_id = ".$student_og_location."";
		    // $result_stu_loc = Update($update_stu_loc,$conn,"skus_locations");
			// //echo $update_stu_loc."<br>";
        
		}
		

		$Del_cour_rec = "UPDATE `student_couriers_records` SET del_flag=1,returned_on='".$return_date."',return_comment='".$return_comment."'  WHERE id = '".$recordid."'";
		$result_del_rec = Update($Del_cour_rec,$conn,"student_couriers_records");
        //echo $Del_cour_rec."<br>"; 
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	print_r($ex);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>