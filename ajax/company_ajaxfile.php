<?php
$db_server='13.126.164.124';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'wp_tetris');
## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

## Date search value
$searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
$searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);
$job_typechk = mysqli_real_escape_string($con,$_POST['job_type']);
$locationchk = mysqli_real_escape_string($con,$_POST['location']);
$verticalchk = mysqli_real_escape_string($con,$_POST['vertical']);
## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (job_title_1 like '%".$searchValue."%' or 
	    job_title_2 like '%".$searchValue."%' or 
        company_name like'%".$searchValue."%'  or 
        qualification like'%".$searchValue."%'  or 
        salary like'%".$searchValue."%'  or 
        experience like'%".$searchValue."%' ) ";
}

// Date filter
if($searchByFromdate != '' && $searchByTodate != ''){
	$rawfrom=explode("/",$searchByFromdate);
	$newfrom=$rawfrom[2]."-".$rawfrom[0]."-".$rawfrom[1];
	
	$rawto=explode("/",$searchByTodate);
	$newto=$rawto[2]."-".$rawto[0]."-".$rawto[1];
	
    $searchQuery .= " and (created_at between '".$newfrom."' and '".$newto."' ) ";
}

// Job Type
if($job_typechk != ''){

    $searchQuery .= " and (job_type = ".$job_typechk.") ";
}

//Location
if($locationchk != ''){

    $searchQuery .= " and (job_locations like '%".$locationchk."%') ";
}

//Vertical
if($verticalchk != ''){

    $searchQuery .= " and (vertical like '%".$verticalchk."%') ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM job_postings order by id desc");
				
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM job_postings where id!=0 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT * FROM job_postings where id!=0  ".$searchQuery." order by id desc, ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;				
$empRecords = mysqli_query($con, $empQuery);
$data = array();
$is_active="";
while ($row = mysqli_fetch_assoc($empRecords)) {
	if($row['is_active']=='1')
    {
	//$is_active="<a href='#' onclick='deactivate_me(".$row['id'].")' ><i class='la la-edit'></i></a><font color='green'>Active</font>";
	$is_active="<font color='green'>Active</font>";
	} else 
	{
		$is_active="<font color='red'>Not Active</font>";
	}
	$final_locations="";
	$qry_loc = "SELECT location_name FROM job_locations where id in (".$row['job_locations'].") order by id desc";
	$res = mysqli_query($con,$qry_loc);
	while($rl = mysqli_fetch_assoc($res)) {
	$final_locations .= $rl['location_name'].", ";
	}
	
	## Total number of students applied for the posting
	$sel_appl = mysqli_query($con,"SELECT
	count(*) as applied FROM student_applications where job_id=".$row['id']);
	$applied_records = mysqli_fetch_assoc($sel_appl);
	$totalapplied = $applied_records['applied'];
	
	
    $data[] = array(
	        
    		"course"=>$row['course'],
    		"job_title_1"=>$row['job_title_1'],
			"job_title_2"=>$row['job_title_2'],
			"company_name"=>$row['company_name'],
			"job_locations"=>rtrim($final_locations,", "),
    		"qualification"=>$row['qualification'],
    		"salary"=>$row['salary'],
			"start_date"=>$row['start_date'],
			"end_date"=>$row['end_date'],
			"created_at"=>$row['created_at'],
			"is_active"=>$is_active,
			"id"=>$row['id'],
			"totalapplied"=>$totalapplied, 
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
    //"query1" => $empQuery,
);

echo json_encode($response);
