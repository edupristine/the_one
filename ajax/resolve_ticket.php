<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail.php';
/*For sending mails to students on resolutions*/
try{
	if( isset($_POST['recordid'])  &&  !empty($_POST['recordid']))
	{
		$record_id  = get_post_value('recordid');
		$resolution  = get_post_value('resolution');
		
		$sel_ticket = "Select ticket_no,student_name,email from  recv_grievances  where id=".$record_id;
	   
	    $result_ticket = Select($sel_ticket,$conn,"recv_grievances");
		
		$ticketno = $result_ticket['rows'][0]['ticket_no'];
		$student_name = $result_ticket['rows'][0]['student_name'];
		$email = $result_ticket['rows'][0]['email'];
					
		$update_resol = "Update recv_grievances set resolved_at=now(),status='Closed',resolution='".$resolution."' where id=".$record_id;
	   
	    $result_resol = Update($update_resol,$conn,"recv_grievances");
		
		$html = "<table style='border: 1px solid #327bbe;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'><tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #00AAFF; text-align:center; font-size: 24px;'><b>Edupristine</b></td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>Dear ".$student_name.",</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		This is to inform you that request raised by you under ticket no: <strong>".$ticketno."</strong> has been resolved.
		<p>
		Resolution Provided as Below:
		<p>
		".$resolution."
		<p>
		</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		Team EduPristine</td>
		</tr>

		</table>";
		
		$subject = "EduPristine - Payment Assistance";
		send_smt_mail($subject,$html,$email,$student_name,"","",1);
		
        $output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>