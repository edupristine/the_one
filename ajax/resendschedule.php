<?php
$db_server='52.77.5.117';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');



## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

## Date search value
$searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
$searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);
$statuschk = mysqli_real_escape_string($con,$_POST['status']);

## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and ( 
	    batch_code like '%".$searchValue."%' or student_email like'%".$searchValue."%') ";
}

// Date filter
if($searchByFromdate != '' && $searchByTodate != ''){
	$rawfrom=explode("/",$searchByFromdate);
	$newfrom=$rawfrom[2]."-".$rawfrom[0]."-".$rawfrom[1];
	
	$rawto=explode("/",$searchByTodate);
	$newto=$rawto[2]."-".$rawto[0]."-".$rawto[1];
	
    $searchQuery .= " and (workshop_date between '".$newfrom."' and '".$newto."') ";
}

// Status
if($statuschk != ''){

    $searchQuery .= " and (mailtype like '%".$statuschk."%') ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM batchcode_schedules ");
				
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM batchcode_schedules where id!=0 ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT * FROM batchcode_schedules where id!=0 ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
$empRecords = mysqli_query($con, $empQuery);
$data = array();

while ($row = mysqli_fetch_assoc($empRecords)) {
    $data[] = array(
	        "id"=>$row['id'],
    		"batch_code"=>$row['batch_code'],
			"student_email"=>$row['student_email'],
    		"workshop_date"=>$row['workshop_date'],
			"workshop_id"=>$row['workshop_id'],
    		"mailtype"=>$row['mailtype'],
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
    //"query1" => $empQuery,
);

echo json_encode($response);
