<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['record_id'])  &&  !empty($_POST['record_id']))
	{
		$record_id = get_post_value('record_id');
		$courier_track_no = get_post_value('courier_track_no');
		$courier_provider = get_post_value('courier_provider');
		$courier_charges = get_post_value('courier_charges');
		
		
		if($courier_track_no=="")
		{
			$courier_track_no="";
		}
		
		if($courier_charges=="")
		{
			$courier_charges="0";
		}

			
	        $update_courier = "UPDATE student_couriers_records set courier_track_no = '".$courier_track_no."', courier_provider = '".$courier_provider."', courier_charges = '".$courier_charges."' WHERE id = '".$record_id."' ";
			$result_up_courier = Update($update_courier,$conn,"student_couriers_records");
  
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	print_r($ex);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>