<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['final_cbs_json'])  &&  !empty($_POST['final_cbs_json']))
	{
	
		$student_id = get_post_value('student_id');
		$prev_student_id = get_post_value('prevst_id');
		$student_location = get_post_value('student_loc');
		$courier_prov = get_post_value('courier_prov');
		$courier_track_no = get_post_value('courier_track_no');
		$courier_charges = get_post_value('courier_charges');
		$courier_id = get_post_value('courierid');
	    
		
	    
		if($courier_charges=="")
		{
			$courier_charges=0;
		}
		
		
		$skuscheck		= get_post_value('final_cbs_json');
		$skuscheck = stripslashes($skuscheck);
		$skuscheck = json_decode($skuscheck);
		$skuscheck = json_decode(json_encode($skuscheck), true);
		
		
		foreach($skuscheck as $skuchk)
		{
			$cbnamearray = explode("_",$skuchk['cb_name']);
			
			
			$select = "SELECT sku_qty_avl, sku_pre_qty from skus_locations where sku_id = '".$cbnamearray[2]."'   and loc_id = '".$_SESSION['U_LOCATION_ID']."'";
			
			$result_sku_cur = Select($select,$conn);
		
            $curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$preqty = $result_sku_cur['rows'][0]['sku_pre_qty'];
			$avl_qty=$curqty+$preqty;

			
			$select_skname = "SELECT sku_name from skus where id = '".$cbnamearray[2]."'";
			$result_sku_name = Select($select_skname,$conn);
			$skuname = $result_sku_name['rows'][0]['sku_name'];
            $final=$avl_qty-1;

			if($final<=0)
			{
				$output = array(
				"status"=>'insufficient_qty',
				"sku_name"=>"'".$skuname."'",
				"message"=>'No Sufficient Qty Available');
				$output = json_encode($output);
				echo $output;
				exit();
				
			}
			
			
		}
		
		
		$final_cbs_json = get_post_value('final_cbs_json');
		$cbs = stripslashes($final_cbs_json);
		$cbs = json_decode($cbs);
		$cbs = json_decode(json_encode($cbs), true);
		
		$cbs_count = count($cbs);
       
	    if($student_id!=0)
		{
		$select_req="SELECT count(id) as counts FROM issuance  WHERE  courier_record_id is Null and `student_id`='".$student_id."'";
		$result_sel = Select($select_req,$conn,"issuance");
        }
		if($prev_student_id!=0)
		{
		$select_req="SELECT count(id) as counts FROM prev_student_rr  WHERE  courier_record_id is Null and `id`='".$prev_student_id."'";
		$result_sel = Select($select_req,$conn,"prev_student_rr");	
		
		
		}
       

        $courier_status="";
		if(($result_sel['rows'][0]['counts'])!=$cbs_count)
		{
		$courier_status= 'Partially Sent';	
		}	
		else
		{
			$courier_status= 'Sent';
		}
		
		 if($student_id!=0)
		{
		//update Couriers_request
		$update_cour="UPDATE `student_couriers_request` set courier_status='".$courier_status."'  WHERE student_id = '".$student_id."' and id= '".$courier_id."' ";
	    $result_cour = Update($update_cour,$conn,"student_couriers_request");
		}
		if($prev_student_id!=0)
		{
		//update Couriers_request
		$update_cour="UPDATE `student_couriers_request` set courier_status='".$courier_status."'  WHERE prev_student_id = '".$prev_student_id."' and id= '".$courier_id."' ";
	    $result_cour = Update($update_cour,$conn,"student_couriers_request");	
		
		}
		//echo $update_cour."<br>";
		
		
		$courier_record_id="";
		//Insert into Courier Records 
		$insert_cour_rec="INSERT INTO `student_couriers_records`(`courier_req_id`,`courier_provider`, `courier_track_no`, `courier_charges`,`courier_sent_by` ) VALUES ('".$courier_id."','".$courier_prov."','".$courier_track_no."','".$courier_charges."','".$_SESSION['USER_ID']."')";
		$result_cour_rec = Insert($insert_cour_rec,$conn,"student_couriers_records");
		$courier_record_id = $result_cour_rec['id'];
		//echo $insert_cour_rec."<br>";
		foreach($cbs as $cb)
		{
		     
			$cbnamearray = explode("_",$cb['cb_name']);
			
			//For Center Stock
			$update_cen_stock = "UPDATE skus_locations set sku_qty_avl = sku_qty_avl - 1 ,sku_qty_tot_out = sku_qty_tot_out + 1 WHERE sku_id = '".$cbnamearray[2]."'   and loc_id = '".$_SESSION['U_LOCATION_ID']."'";
		    $result_cen_stock = Update($update_cen_stock,$conn,"skus_locations");
			//echo $update_cen_stock."<br>";
			
			
			//For Others Stock
			$update_oth_stock = "UPDATE skus_locations set sku_qty_req = sku_qty_req - 1  WHERE sku_id = '".$cbnamearray[2]."'   and loc_id = 8";
		    $result_oth_stock = Update($update_oth_stock,$conn,"skus_locations");
            //echo $update_oth_stock."<br>";
			
			
			if($student_id!=0)
		    {			
			//Issuance Table
			$update_iss_sku = "UPDATE issuance set courier_record_id = '".$courier_record_id."' ,issued_status = 1, issued_by = ".$_SESSION['USER_ID'].", issue_date = '".date('Y-m-d')."'   WHERE sku_id = '".$cbnamearray[2]."'   and student_id ='".$student_id."'";
		    $result_iss_sku = Update($update_iss_sku,$conn,"skus_locations");
			//echo $insert_cour_rec."<br>";
			}
			if($prev_student_id!=0)
		    {
			//Prev_student_rr Table
			$update_iss_sku = "UPDATE prev_student_rr set courier_record_id = '".$courier_record_id."' ,issued_status = 1, issued_by = ".$_SESSION['USER_ID'].", issue_date = '".date('Y-m-d')."'   WHERE sku_id = '".$cbnamearray[2]."'   and id ='".$prev_student_id."'";
		    $result_iss_sku = Update($update_iss_sku,$conn,"prev_student_rr");
			
				
			}
			

		}
		
		
		

		
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
		
		
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	print_r($ex);
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>