<?php
error_reporting(0);
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['student_id'])  &&  !empty($_POST['student_id']))
	{
		
		$student_id = get_post_value('student_id');
		$final_cbs_json = get_post_value('final_cbs_json');
		$cbs = stripslashes($final_cbs_json);
		$cbs = json_decode($cbs);
		$cbs = json_decode(json_encode($cbs), true);
		
		
		$final_upcodes_json = get_post_value('final_upcodes_json');
		$cbs_upcode = stripslashes($final_upcodes_json);
		$cbs_upcode = json_decode($cbs_upcode);
		$cbs_upcode = json_decode(json_encode($cbs_upcode), true);
		
		//For new Insert
		
		foreach($cbs as $cb)
		{
			$cbnamearray = explode("_",$cb['cb_name']);			
	        $insert_req="INSERT INTO `students_course_modules`(`student_id`,`course_module_id`,`batch_attended`) VALUES ('".$cbnamearray[1]."','".$cbnamearray[2]."','".$cbnamearray[3]."')";
			$result_req = Insert($insert_req,$conn,"students_course_modules");
			//echo $insert_req."<br>";
		}
		
		foreach($cbs_upcode as $cb)
		{
			$cbnamearray = explode("_",$cb['cbsc_name']);			
	        $update_req="UPDATE `students_course_modules` SET `batch_attended`='".$cbnamearray[3]."' where  `course_module_id`='".$cbnamearray[2]."' and `student_id`='".$cbnamearray[1]."'";
			$result_req = Update($update_req,$conn,"students_course_modules");
			//echo $update_req."<br>";
		}
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>