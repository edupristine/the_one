<?php
/*if($_SESSION['USER_ID']!=1)
{
	echo "Not Authorized";
	exit;
}*/

//
/* Template Name: Lecture Feedback Form
 * Description: Lecture Feedback Form Link Sender
 * Version: 1.0
 * Created On: 20 April, 2022
 * Created By: Hitesh
 **/

 
include '../inc/GenericFunctions.php';
include '../control/core.php';
setlocale(LC_MONETARY, 'en_IN'); 

$db_server='52.77.5.117';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$convtiger = new mysqli($db_server, $db_username, $db_password, 'vtigercrm6');
$conharry = new mysqli($db_server, $db_username, $db_password, 'ecademe_harry_new');
$connone = new mysqli($db_server, $db_username, $db_password, 'edupristine_one');

//Fetching Today's Classrooms from Harry
		$classQuery = "SELECT
		                w.id as `workshop_id`,
						wd.id as `workshop_date_id`, 
						wd.batch_code as `batch_code`,
						wd.ws_date,
						wd.start_time `start_time`, 
						wd.end_time as `end_time`,
						wd.actual_start_time as `actual_start_time`,
						wd.actual_end_time as `actual_end_time`,
						fc.fid as `faculty_id`
						FROM workshops_dates wd
						LEFT JOIN workshops w ON w.id = wd.workshop_id
						LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
						LEFT JOIN fac_facultydetails fc1 ON fc1.fid = wd.new_faculty_id
						WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'  and wd.`status`!= 'Error'  AND wd.ws_date=CURDATE()";
					
		//echo $classQuery."<br>";
		$classRecords = mysqli_query($conharry, $classQuery);
		while ($row = mysqli_fetch_assoc($classRecords)) {
		$data_array[]=$row;
		}
		
		//print_r($data_array);
	
// Fetching Data from CRM for students whom are allowed to access data if access flag is removed all students are displayed according to harry	
		foreach($data_array as $workshops_det)
		{
			
		$batch_code="";	
        $batch_code=$workshops_det['batch_code'];
		$crmQuery = "SELECT 
						a.account_no as organization_no,
						a.email1 AS primary_email,
						a.email2 AS secondary_email,
						a.phone AS phone,
						a.accountid AS ac_id,
						acf.cf_1467 as `access_flag`,
						a.accountname AS `student_name`,
						acf.cf_918 AS `Course`,
						acf.cf_1513 AS `SubCoures`,
						ce.createdtime AS `created_time`,
						ce.smownerid AS `created_by`
						FROM vtiger_account a
						LEFT JOIN vtiger_accountscf acf ON acf.accountid = a.accountid 
						LEFT JOIN vtigercrm6.vtiger_crmentity ce ON ce.crmid = a.accountid
						LEFT JOIN vtigercrm6.vtiger_accountbillads va ON va.accountaddressid=a.accountid
						WHERE (acf.cf_1269 LIKE '%".$batch_code."%'
						OR acf.cf_1571 LIKE '%".$batch_code."%'		
						OR acf.cf_1575 LIKE '%".$batch_code."%'
						OR acf.cf_1495 = '".$batch_code."'
						OR acf.cf_1497 = '".$batch_code."'  
						OR acf.cf_1499 = '".$batch_code."'
						OR acf.cf_1501 = '".$batch_code."'
						OR acf.cf_1503 = '".$batch_code."'
						OR acf.cf_1505 = '".$batch_code."') 
						AND  a.email1 NOT LIKE '%junk%' AND acf.cf_1489 =0 AND ce.smownerid !=3920 ";
						//echo $crmQuery."<br>";
						$crmRecords = mysqli_query($convtiger, $crmQuery);
						while ($row = mysqli_fetch_assoc($crmRecords)) {
						$crmdata_array[]=array_merge($row,$workshops_det);
						}


        }
       // print_r($crmdata_array);
	   
		// echo "<html><table>";
		// $ax=1;
		// foreach($crmdata_array as $da)
		// {
		// echo "<tr>";	
		// echo "<td>".$ax."</td>";
		// echo "<td>".$da['id']."</td>";
		// echo "<td>".$da['batch_code']."</td>";
		// echo "<td>".$da['ws_date']."</td>";
		// echo "<td>".$da['start_time']."</td>";
		// echo "<td>".$da['end_time']."</td>";
		// echo "<td>".$da['actual_start_time']."</td>";
		// echo "<td>".$da['actual_end_time']."</td>";
		// echo "<td>".$da['organization_no']."</td>";
		// echo "<td>".$da['primary_email']."</td>";
		// echo "<td>".$da['phone']."</td>";
		// echo "<td>".$da['ac_id']."</td>";
		// echo "<td>".$da['faculty_id']."</td>";
		// echo "<td>".$da['access_flag']."</td>";
		// echo "<td>".$da['student_name']."</td>";
		// echo "<td>".$da['Course']."</td>";
		// echo "</tr>";
		
		// $ax++;
		// }
		// echo "</table></html>";
		// exit;
		
		
		$i=1;
		foreach($crmdata_array as $da)
	    {
			
		$query = "SELECT * FROM lectures_feedback WHERE acc_id = '".$da['ac_id']."' AND workshop_id='".$da['id']."' ";
		$result = mysqli_query($connone, $query);
		if ($result) {
		if (mysqli_num_rows($result) > 0) {
		echo 'found!'."<br>";
		} else {	
			
		$unique_stu_id = random_strings(12);
		$url_link = "http://one.edupristine.com/student_lecture_feedback.php?id=".base64_encode($unique_stu_id)."";	
				
		$html="";
		$html .= "<center><table style='border: 0px solid #FFFFFF;border-collapse: collapse; width: 80%; font-family: Arial, Helvetica, sans-serif; font-size: 12px;'>
		
		<tr style='height:30px;'>
		<td colspan=2>
		<p>
		</td>
		</tr>
	    <tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 17px;'>Dear ".trim($da['student_name']).",</td>
		</tr>
		<tr>
		<td colspan=2 style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 17px;'>
		
        <p>Hope you have attended the lecture for ".$da['batch_code'].". of ".$da['Course']."</p>
		
		<p>In regards to the same, we would request you to help us with the overall feedback of the workshop.</p>

		<p>We have enclosed an feedback Form here:<a href='".$url_link."' target=_blank>Workshop Feedback Form</a></p>
		
		<p>Regards,<br>
		Team EduPristine</p>
		
		</td></tr>";

		

		$html .="
		
		<tr>
		<td colspan=2>
		<p style='text-align:justify;'>
		* Please provide the exact feedback for the questions to help us to enhance our sevices towards you.  
		</p>
		</td>
		</tr>
		
	
		
        <tr>
		<td colspan=2 style='border: 1px solid #327bbe; color:white; padding: 8px 0px; background-color: #394970; text-align:center; font-size: 12px;'><i>2022 © EduPristine. All rights reserved.</i></td>
		</tr>
		</table></center>";
		
		
		
		$insert_rec = "INSERT INTO `lectures_feedback` (
		`organization_no`,
		`acc_id`,
		`student_name`,
		`email_id`,
		`contact_no`,
		`batch_code`,
		`workshop_id`,
	    `date_id`,
		`workshop_date`,
		`faculty_id`,
		`user`,
		`link`,
		`unique_id`,
		`created_at`
		) VALUES ('".$da['organization_no']."','".$da['ac_id']."','".$da['student_name']."','".$da['primary_email']."','".$da['phone']."','".$da['batch_code']."','".$da['workshop_id']."','".$da['workshop_date_id']."','".$da['ws_date']."','".$da['faculty_id']."','admin','".$url_link."','".$unique_stu_id."',now())";
		echo $insert_rec."<br>";
		
		
	    $insert_mail = mysqli_query($connone, $insert_rec);
		
		$subject = "".$mail['first_name'].",Provide Feedback for workshop ".$da['batch_code']." held on ".$da['ws_date'];
        echo $html."<br>";
		$email=$da['primary_email'];
		$email_name=$da['student_name'];
		
        ///////////////////////////send_smt_mail($subject,$html,$email,$email_name);

		
		echo $i.") ". $email." - Mail Sent\n";
		$i++;
		}
        }
		}
		
		
// This function will return a random 
// string of specified length 
function random_strings($length_of_string) 
{ 

// String of all alphanumeric character 
$str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'; 

// Shufle the $str_result and returns substring 
// of specified length 
return substr(str_shuffle($str_result),  
0, $length_of_string); 
} 

function roundoff($n)  
{  
   $a = (int)($n / 100) * 100;  
   $b = ($a + 100);  
   return ($n - $a > $b - $n) ? $b : $a;  
}


function send_smt_mail($subject,$email_msg,$email,$email_name){
	include_once(dirname(__FILE__) . '/../PHPMailer/src/Exception.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/PHPMailer.php');
	include_once(dirname(__FILE__) . '/../PHPMailer/src/SMTP.php');

	$mail = new PHPMailer\PHPMailer\PHPMailer();
	try {
		//Server settings
		$mail->SMTPDebug = 0;                                 // Enable verbose debug output
		$mail->isSMTP();                                    // Set mailer to use SMTP
		$mail->Host = 'in-v3.mailjet.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = '5c0f024f36586790335e4ca623c6f970';                 // SMTP username
		$mail->Password = '01b9256230fed13e86c7b9c77b219a1c'; // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to
		$today = date('d-m-Y');
        //Recipients
		
		$mail->setFrom('feedback@EduPristine.com', 'EduPristine Feedback');
		$mail->addAddress($email,$email_name);
		$mail->addBCC('hitesh.patil@EduPristine.com', 'Hitesh Patil');
		//Content
		$mail->isHTML(true);			             // Set email format to HTML
		$mail->CharSet = "text/html; charset=UTF-8;";
		$mail->Subject = $subject;
		$mail->Body   = $email_msg;
		$mail->AltBody = $email_msg;
		$mail->send();
		//exit;

	} catch (Exception $e) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
	}
}

 
?>

