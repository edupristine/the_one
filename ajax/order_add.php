<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
try{
	if( isset($_POST['skus']) && isset($_POST['ord_num']) && isset($_POST['ord_date'])&& isset($_POST['kt_select2_1'])&& 
		!empty($_POST['skus']) && !empty($_POST['ord_num']) && !empty($_POST['ord_date']) && !empty($_POST['kt_select2_1']) )
	{
		
		$ord_num	= get_post_value('ord_num');
		$ord_date	= get_post_value('ord_date');
		$ord_date 	= date("Y-m-d", strtotime($ord_date));		
		$vendor_id		= get_post_value('kt_select2_1');
		$exp_rec_date	= get_post_value('exp_rec_date');
		$exp_rec_date 	= date("Y-m-d", strtotime($exp_rec_date));
		$ref_num	= get_post_value('ref_num');
		$ord_amount	= get_post_value('ord_amount');
		$comments	= get_post_value('comments');
		
		$skus		= get_post_value('skus');
		$skus = stripslashes($skus);
		$skus = json_decode($skus);
		$skus = json_decode(json_encode($skus), true);
		
		
		$query_insertorder = "INSERT INTO `orders` (`ord_num`,`ord_date`,`pi_num`,`vendor_id`,`placed_by`,`received_by`,
		`ord_status`,`ord_amt`,`exp_rec_date`,`comments`,`loc_id`)
		VALUES('".$ord_num."','".$ord_date."','".$ref_num."','".$vendor_id."','".$_SESSION['USER_ID']."','0',
		'Placed','".$ord_amount."','".$exp_rec_date."','".$comments."',1)";
		
		$result_insertorder = Insert($query_insertorder,$conn,'orders');
		$order_id = $result_insertorder['id'];
		
		foreach($skus as $sku)
		{
			$query_insertsku = "INSERT INTO `orders_skus` (`ord_id`,`sku_id`,qty,`rate`,`amount`)
			VALUES('".$order_id."','".$sku['skuname']."','".$sku['qty']."','".$sku['rate']."','".$sku['amt']."')";
			$result_insertsku = Insert($query_insertsku,$conn,'orders_skus');
			
		}
		
		
		$query_ord = "select * from orders where id = ".$order_id;
		$result_ord = Select($query_ord,$conn);
		
		$location_id=$result_ord['rows'][0]['loc_id'];
		
		$query_ord_skus = "select * from orders_skus where ord_id = ".$order_id;
		$result_ord_skus = Select($query_ord_skus,$conn);
		
		foreach($result_ord_skus['rows'] as $ord_sku)
		{
			$select = "SELECT sku_qty_avl,sku_qty_tot_in from skus_locations where loc_id = '".$location_id."' AND sku_id = ".$ord_sku['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_in'];
			$newqty = $curqty + $ord_sku['qty'];
			$newtotinqty = $totcurqty + $ord_sku['qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_in = '".$newtotinqty."' WHERE 
			loc_id = '".$location_id."' AND sku_id = ".$ord_sku['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
		}
		
		$update_order = "UPDATE orders SET ord_status = 'Received',received_by = '".$_SESSION['USER_ID']."' where id = ".$order_id;
		$result_upd_ord = Update($update_order,$conn);
		
		
		
		$output = array(
			"status"=>'success',
			"order_id"=>$order_id,
			"message"=>'Order Created Successfully');
		$output = json_encode($output);
		echo $output;
		exit();
	}
	else{
		$output = array(
			"status"=>'missing_params',
			"order_id"=>'',
			"message"=>'Order creation FAILED. Contact Administrator');
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error',
		"order_id"=>'',
		"message"=>'Order creation FAILED. Contact Administrator');
	$output = json_encode($output);
	echo $output;
	exit();
}
?>