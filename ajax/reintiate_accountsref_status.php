<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/refund_mail.php';

try{
	if(isset($_POST['student_id'])  && !empty($_POST['student_id']))
	{
	$student_id = get_post_value('student_id');
	$resval = get_post_value('resval');
	$status=$resval;
	$comment = get_post_value('comment');
	$id=$student_id;
	
	
	$query_details = "SELECT `account_no`,`amount_to_refund`,`course_enrolled` FROM `student_refund_requests` WHERE `id`=".$id." ";
	$result_details = Select($query_details,$conn);
	$amtref=$result_details['rows'][0]['amount_to_refund'];
	$account_no=$result_details['rows'][0]['account_no'];
	$course_enrolled=$result_details['rows'][0]['course_enrolled'];
		
	$query_dupcheck = "SELECT `id` FROM `refund_details` WHERE `approver_id` in (SELECT user_id FROM users_teams  where  team_id in (11)) and `refund_req_id`=".$student_id." ";
	$result_dupcheck = Select($query_dupcheck,$conn);
	if($result_dupcheck['count']!=0){
	$query_update = "UPDATE `refund_details` SET approval_status='".$status."' ,`comments`='".$comment."' where `approver_id`='".$_SESSION['USER_ID']."' and `refund_req_id`=".$student_id."";
	$result_update = Update($query_update,$conn,'refund_details');
	}
    else 
	{
	$query_insert = "INSERT INTO `refund_details` (`refund_req_id`, `approver_id`, `approval_email_sent`, `approval_status`, `comments`)
	VALUES('".$student_id."','".$_SESSION['USER_ID']."','1','".$status."','".$comment."')";
	$result_insert = Insert($query_insert,$conn,'refund_details');	
	}
    
	
	$query_user = "SELECT `user_name` FROM `users` WHERE `id`='".$_SESSION['USER_ID']."'";
	$result_user = Select($query_user,$conn);
	
	
	$query_appuser = "SELECT `account_no`,`student_name`,`amount_to_refund`,`counselor_name` FROM `student_refund_requests` WHERE `id`='".$student_id."'";
	$result_appuser = Select($query_appuser,$conn);
	
	$ip_address = get_client_ip();

	$query_insert = "INSERT INTO `refund_logs` (`ip`, `creating_user`, `status`, `reason_of_update`, `amount`,`org_no`,`course`,`log_type`)

	VALUES('".$ip_address."','".$_SESSION['USER_ID']."','".$status."','".$comment."','".$amtref."','".$account_no."','".$course_enrolled."','ACCOUNT_REINTIATE')";

	$result_insert = Insert($query_insert,$conn,'refund_logs');
	
	
    if($status=="0")
	{
      
		//ReinTitation  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the Refund Renitiated request raised  by Accounts to rejection</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #FF0000; text-align:left; font-size: 14px;width:70%;'><b>".$result_appuser['rows'][0]['student_name']."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_appuser['rows'][0]['account_no']."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_appuser['rows'][0]['amount_to_refund']."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comment."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_user['rows'][0]['user_name']."</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Rejected";
		send_smt_mail3($subject,$html);
	}

	
    if($status=="1")
	{
      
		//ReinTitation  Mail Sending for Intimation to Support
		$html="";
		$date = date('Y-m-d');
		$html .= "
		<p>Please find below details for the Refund Renitiated request raised  by Accounts to Reapproval</p>
		<table style='border: 1px solid #F0F0F0;border-collapse: collapse; width: 100%; font-family: Arial, Helvetica, sans-serif; font-size: 14px;'>
		<tr>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #54AD00; text-align:right; font-size: 14px;width:30%;'><b>
        Student Name:</b></td>
		<td style='border: 1px solid #327bbe; color:white; padding: 8px 8px; background-color: #54AD00; text-align:left; font-size: 14px;width:70%;'><b>".$result_appuser['rows'][0]['student_name']."</b></td>
		</tr>";
	
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Organization Number:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_appuser['rows'][0]['account_no']."</td></tr>";
	
		
        $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Amount to be Refunded:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_appuser['rows'][0]['amount_to_refund']."</td></tr>";
		
	     $html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Reason for Rejection:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$comment."</td></tr>";
		
		$html .= "<tr>
		<td style='border: 1px solid #000000; color:#000000; padding: 8px 8px; background-color: #ffffff; text-align:right; font-size: 14px;width:30%;'>Rejected By:</td>
		<td style='border: 1px solid #327bbe; color:#000000; padding: 8px 8px; background-color: #FFFFFF; text-align:left; font-size: 14px;width:70%;'>
		".$result_user['rows'][0]['user_name']."</td></tr>";
		
		
		
		$html .= "
	
        </table>";
		
		
		
		
		$html .="<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;'>
		In case of any discrepancy or doubt please contact the IT Team.</p>
		
		<p style='color:black; padding: 8px 5px; background-color: #ffffff; text-align:left; font-size: 14px;' >
		EduPristine IT Team</p>";
		
		
		$subject = "Refund request Reapproved";
		send_smt_mail3($subject,$html);
	}
	
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>