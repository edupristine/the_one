<?php
	include '../inc/GenericFunctions.php';
	include '../control/core.php';
	include '../control/checklogin.php';
	include '../control/connection.php';
	try
	{
		$order_id = get_get_value('id');
		if($order_id == "")
		{
			header('location: ../order_list.php?ordrecparammiss');
			exit();
		}
		
		$query_ord = "select * from orders where id = ".$order_id;
		$result_ord = Select($query_ord,$conn);
		
		$location_id=$result_ord['rows'][0]['loc_id'];
		
		$query_ord_skus = "select * from orders_skus where ord_id = ".$order_id;
		$result_ord_skus = Select($query_ord_skus,$conn);
		
		foreach($result_ord_skus['rows'] as $ord_sku)
		{
			$select = "SELECT sku_qty_avl,sku_qty_tot_in from skus_locations where loc_id = '".$location_id."' AND sku_id = ".$ord_sku['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_in'];
			$newqty = $curqty + $ord_sku['qty'];
			$newtotinqty = $totcurqty + $ord_sku['qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_in = '".$newtotinqty."' WHERE 
			loc_id = '".$location_id."' AND sku_id = ".$ord_sku['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
		}
		
		$update_order = "UPDATE orders SET ord_status = 'Received',received_by = '".$_SESSION['USER_ID']."' where id = ".$order_id;
		$result_upd_ord = Update($update_order,$conn);
		
		header('location: ../order_list.php?ordstatusupd');
		exit();
		
	}
	catch(PDOException $ex)
	{
		DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
		//print_r($ex);
		header('location: ../order_list.php?ordupdfailed');
		exit();
		
	}
?>