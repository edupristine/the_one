<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
try{
		if( isset($_POST['skus']) && isset($_POST['transfer_no']) && isset($_POST['id']) && isset($_POST['transfer_date'])&& isset($_POST['kt_select2_1'])&& 
		!empty($_POST['skus']) && !empty($_POST['transfer_no']) && !empty($_POST['id']) && !empty($_POST['transfer_date']) && !empty($_POST['kt_select2_1']) )
		{    
		$id	= get_post_value('id');
        $transfer_no	= get_post_value('transfer_no');
		$transfer_date	= get_post_value('transfer_date');
		$transfer_date 	= date("Y-m-d", strtotime($transfer_date));		
		$to_loc_id		= get_post_value('kt_select2_1');
		$courier_track_no	= get_post_value('courier_track_no');
		$transfer_charges	= get_post_value('transfer_charges');
		$transfer_status	= get_post_value('transfer_status');
		$transfer_courier	= get_post_value('transfer_courier');
		
		$skus		= get_post_value('skus');
		$skus = stripslashes($skus);
		$skus = json_decode($skus);
		$skus = json_decode(json_encode($skus), true);
		
		$old_transfer = "SELECT * from transfers where id = ".$id;
		$result_old_transfer = Select($old_transfer,$conn);
		
		$old_transfer_skus = "SELECT * from transfers_skus where trans_id = ".$id;
		$result_old_transfer_skus = Select($old_transfer_skus,$conn);
		
		if($result_old_transfer['rows'][0]['transfer_status'] == "TRANSFER COMPLETED")
		{
			$output = array(
			"status"=>'completed_transfer',
			"order_id"=>$id,
			"message"=>'You Cannot Edit Completed Transfer.');
			$output = json_encode($output);
			echo $output;
			exit();
		}
		
		$skuscheck		= get_post_value('skus');
		$skuscheck = stripslashes($skuscheck);
		$skuscheck = json_decode($skuscheck);
		$skuscheck = json_decode(json_encode($skuscheck), true);
		
		
		foreach($skuscheck as $skuchk)
		{
						
			$select_transqty = "SELECT trans_qty from transfers_skus where trans_id = '".$id."' AND sku_id = '".$skuchk['skuname']."'";
			$result_sku_trans = Select($select_transqty,$conn);
			$transqty = $result_sku_trans['rows'][0]['trans_qty'];

			
			$select = "SELECT sku_qty_avl, sku_pre_qty from skus_locations where loc_id = '".$_SESSION['WH_ID']."' AND sku_id = ".$skuchk['skuname'];
			$result_sku_cur = Select($select,$conn);
		

            $curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$preqty = $result_sku_cur['rows'][0]['sku_pre_qty'];
			$avl_qty=$curqty+$preqty;


             $newqtychk = $skuchk['qty']-$transqty;
			
			$select_skname = "SELECT sku_name from skus where id = ".$skuchk['skuname'];
			$result_sku_name = Select($select_skname,$conn);
			$skuname = $result_sku_name['rows'][0]['sku_name'];
            $final=$avl_qty-$newqtychk;

			if($final<0)
			{
				$output = array(
				"status"=>'insufficient_qty',
				"sku_name"=>"'".$skuname."'",
				"message"=>'Transfer Update Failed. No Sufficient Qty Available');
				$output = json_encode($output);
				echo $output;
				exit();
				
			}
			
			
		}
		
		foreach($result_old_transfer_skus['rows'] as $old_skus)
		{
			$select = "SELECT sku_qty_avl,sku_qty_tot_in from skus_locations where loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$old_skus['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_in'];
			$newqty = $curqty - $old_skus['trans_qty'];
			$newtotinqty = $totcurqty - $old_skus['trans_qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_in = '".$newtotinqty."' WHERE 
			loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$old_skus['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
			
			$select = "SELECT sku_qty_avl,sku_qty_tot_out from skus_locations where loc_id = '".$_SESSION['WH_ID']."' AND sku_id = ".$old_skus['sku_id'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_out'];
			$newqty = $curqty + $old_skus['trans_qty'];
			$newtotoutqty = $totcurqty - $old_skus['trans_qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_out = '".$newtotoutqty."' WHERE 
			loc_id = '".$_SESSION['WH_ID']."' AND sku_id = ".$old_skus['sku_id'];
			$result_upd = Update($update_sku_qty,$conn);
		}
		
		$old_transfer_intiated_by = $result_old_transfer['rows'][0]['transfer_intiated_by'];
		
		$query_delete_skus = "Delete from transfers_skus where trans_id = ".$id;
		$result_delete_skus = Del($query_delete_skus,$conn);
		
		$query_delete_transfer = "Delete from transfers where id = ".$id;
		$result_delete_transfer = Del($query_delete_transfer,$conn);
		

		$query_inserttransfer = "INSERT INTO `transfers` (`transfer_no`,`transfer_date`,`transfer_status`,`to_loc_id`,`transfer_intiated_by`,`transfer_completed_by`,`transfer_courier`,`courier_track_no`,`transfer_charges`,`from_loc_id`)
		VALUES('".$transfer_no."','".$transfer_date."','".$transfer_status."','".$to_loc_id."','".$old_transfer_intiated_by."','0',
		'".$transfer_courier."','".$courier_track_no."','".$transfer_charges."','1')";
		
		$result_inserttransfer = Insert($query_inserttransfer,$conn,'transfers');
		$transfer_id = $result_inserttransfer['id'];
		
		foreach($skus as $sku)
		{
			$query_insertsku = "INSERT INTO `transfers_skus` (`trans_id`,`sku_id`,`trans_qty`)
			VALUES('".$transfer_id."','".$sku['skuname']."','".$sku['qty']."')";
			$result_insertsku = Insert($query_insertsku,$conn,'transfers_skus');
			
			$select = "SELECT sku_qty_avl,sku_qty_tot_in from skus_locations where loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$sku['skuname'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_in'];
			$newqty = $curqty + $sku['qty'];
			$newtotinqty = $totcurqty + $sku['qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_in = '".$newtotinqty."' WHERE 
			loc_id = '".$_SESSION['COURIER_ID']."' AND sku_id = ".$sku['skuname'];
			$result_upd = Update($update_sku_qty,$conn);
			
			$select = "SELECT sku_qty_avl,sku_qty_tot_out from skus_locations where loc_id = '".$_SESSION['WH_ID']."' AND sku_id = ".$sku['skuname'];
			$result_sku_cur = Select($select,$conn);
			
			$curqty = $result_sku_cur['rows'][0]['sku_qty_avl'];
			$totcurqty = $result_sku_cur['rows'][0]['sku_qty_tot_out'];
			$newqty = $curqty - $sku['qty'];
			$newtotoutqty = $totcurqty + $sku['qty'];
			
			$update_sku_qty = "UPDATE skus_locations SET sku_qty_avl = '".$newqty."', sku_qty_tot_out = '".$newtotoutqty."' WHERE 
			loc_id = '".$_SESSION['WH_ID']."' AND sku_id = ".$sku['skuname'];
			$result_upd = Update($update_sku_qty,$conn);
			
		}
		
		$output = array(
			"status"=>'success',
			"transfer_id"=>$transfer_id,
			"message"=>'Transfers Updated Successfully');
		$output = json_encode($output);
		echo $output;
		exit();
	}
	else{
		$output = array(
			"status"=>'missing_params',
			"transfer_id"=>'',
			"message"=>'Sku Transfers Updation FAILED. Parameters Missing.');
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	print_r($ex);
	$output = array(
		"status"=>'db_error',
		"transfer_id"=>'',
		"message"=>'Transfers Updation FAILED. Contact Administrator');
	$output = json_encode($output);
	echo $output;
	exit();
}
?>