<?php
$db_server='52.77.5.117';
$db_username='liveuser';/*liveuser*/
$db_password='p3t3r5bu4g';/*p3t3r5bu4g*/
$con = new mysqli($db_server, $db_username, $db_password, 'ecademe_harry_new');

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = mysqli_real_escape_string($con,$_POST['search']['value']); // Search value

## Date search value
$searchByFromdate = mysqli_real_escape_string($con,$_POST['searchByFromdate']);
$searchByTodate = mysqli_real_escape_string($con,$_POST['searchByTodate']);
$statuschk = mysqli_real_escape_string($con,$_POST['status']);

## Search 
$searchQuery = " ";
if($searchValue != ''){
	$searchQuery = " and (wd.batch_code like '%".$searchValue."%' or 
	    fc.name like '%".$searchValue."%' or 
        wd.location like'%".$searchValue."%' ) ";
}

// Date filter
if($searchByFromdate != '' && $searchByTodate != ''){
	$rawfrom=explode("/",$searchByFromdate);
	$newfrom=$rawfrom[2]."-".$rawfrom[0]."-".$rawfrom[1];
	
	$rawto=explode("/",$searchByTodate);
	$newto=$rawto[2]."-".$rawto[0]."-".$rawto[1];
	
    $searchQuery .= " and (wd.ws_date between '".$newfrom."' and '".$newto."' ) ";
}

// Status
if($statuschk != ''){

    $searchQuery .= " and (wd.`status` like '%".$statuschk."%') ";
}

## Total number of records without filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM workshops_dates wd
				LEFT JOIN workshops w ON w.id = wd.workshop_id
				LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
        		WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'");
				
$records = mysqli_fetch_assoc($sel);
$totalRecords = $records['allcount'];

## Total number of records with filtering
$sel = mysqli_query($con,"SELECT
        		count(*) as allcount FROM workshops_dates wd
				LEFT JOIN workshops w ON w.id = wd.workshop_id
				LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
        		WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled' ".$searchQuery);
$records = mysqli_fetch_assoc($sel);
$totalRecordwithFilter = $records['allcount'];

## Fetch records
$empQuery = "SELECT
					wd.id, 
					wd.batch_code as `batch_code`, 
					wd.venue_details as `venue_details`,
					wd.location as `location`, 
					wd.start_time `start_time`, 
					wd.end_time as `end_time`,
					wd.actual_start_time as `actual_start_time`,
					wd.actual_end_time as `actual_end_time`,
					wd.classroom_rent as `classroom_rent`,
					wd.slot as slot,
					wd.student_count_by_admin as student_count_by_admin,
					wd.topic as `topic`, 
					wd.fac_average_rating as `faculty_rating`,
					wd.content_quality_rating,
					wd.infra_rating, 
					wd.cs_rating,
					wd.newFaculty,
					wd.ws_comment as `ws_comment`, 
					wd.`status` as `status`,
					wd.mail_sent_flag as `mail_sent_flag`,
					wd.sms_sent_flag as `sms_sent_flag`,
					wd.course as `course`,
					wd.ws_date as `ws_date`,
					wd.content_link,
					wd.venue_coordinator_name,
					wd.venue_coordinator_email,
					wd.venue_coordinator_contact,
					wd.support_team_email,
					wd.sales_manager_email,
					wd.extra_instructions_faculty,
					wd.cancel_point,
					wd.changes_point, 
					wd.repeat_session,
					wd.re_entry_point,
					wd.tentitve_remarks,
					wd.feedback_status as `feedback_status`,
					wd.lvc_url as `lvc_url`,
					w.id as `workshop_id`,
					wd.confirm_schedule_sent as `conf_sch`,
					wd.cancelled_schedule_sent as `can_sch`,
					DAYNAME(wd.ws_date) as `day`,
					w.start_date as `start_date`,
					wd.faculty_incharge as `faculty_incharge`,
					wd.faculty_incharge_email as `faculty_incharge_email`, 
					fc.fid,
					fc.name as  `name`, 
					fc.email as `email`,
					fc.phone as `phone`,
					(select count(*) from workshops_feedback wf
					where wf.batch_code = wd.batch_code and wf.ws_feedback_date = wd.ws_date group by wf.batch_code, wf.ws_feedback_date) as `no_of_student`,
					w.course_version AS `course_version`
					FROM workshops_dates wd
					LEFT JOIN workshops w ON w.id = wd.workshop_id
					LEFT JOIN fac_facultydetails fc ON fc.fid = wd.faculty_id
        			WHERE wd.delete_flag = 0 and w.`status` != 'Cancelled'  and wd.`status`!= 'Error' ".$searchQuery." order by ".$columnName." ".$columnSortOrder." limit ".$row.",".$rowperpage;
					
$empRecords = mysqli_query($con, $empQuery);
$data = array();
$schd_sent="";
while ($row = mysqli_fetch_assoc($empRecords)) {
	if($row['conf_sch']=='1')
    {
	$schd_sent="<center><i class='fa fa-check' aria-hidden='true'></i><center>";
	} else if($row['can_sch']=='1')
    {
	$schd_sent="<center><i class='fa fa-check' aria-hidden='true'></i></center>";
	}else 
	{
		$schd_sent="";
	}
    $data[] = array(
	        "id"=>$row['id'],
    		"batch_code"=>$row['batch_code'],
    		"ws_date"=>$row['ws_date'],
			"day"=>$row['day'],
    		"location"=>$row['location'],
    		"status"=>$row['status'],
    		"name"=>$row['name'],
			"course"=>$row['course'],
			"start_time"=>$row['start_time'],
			"end_time"=>$row['end_time'],
			"start_date"=>$row['start_date'],
			"workshop_id"=>$row['workshop_id'],
			"faculty_id"=>$row['fid'],
			"schedule_sent"=>$schd_sent,
    	);
}

## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data,
//	"query1" => $empQuery,
);

echo json_encode($response);
