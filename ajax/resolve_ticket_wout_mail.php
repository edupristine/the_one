<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
include '../legacySystem/mail.php';
/*For sending mails to students on resolutions*/
try{
	if( isset($_POST['recordid'])  &&  !empty($_POST['recordid']))
	{
		$record_id  = get_post_value('recordid');
		$resolution  = "Duplicate or Directly Closed Ticket";
		
		$sel_ticket = "Select ticket_no,student_name,email from  recv_grievances  where id=".$record_id;
	   
	    $result_ticket = Select($sel_ticket,$conn,"recv_grievances");
		
		$ticketno = $result_ticket['rows'][0]['ticket_no'];
		$student_name = $result_ticket['rows'][0]['student_name'];
		$email = $result_ticket['rows'][0]['email'];
					
		$update_resol = "Update recv_grievances set resolved_at=now(),status='Closed',resolution='".$resolution."' where id=".$record_id;
	   
	    $result_resol = Update($update_resol,$conn,"recv_grievances");
				
        $output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>