<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';

try{
	if( isset($_POST['final_cbs_json']) && isset($_POST['student_id'])  && 
		!empty($_POST['final_cbs_json']) && !empty($_POST['student_id']))
	{
		$student_raiselocation="";
		$student_id = get_post_value('student_id');
		$student_location = get_post_value('student_loc');
		$student_raiselocation = get_post_value('raise_loc');
		
		$final_cbs_json = get_post_value('final_cbs_json');
		$cbs = stripslashes($final_cbs_json);
		$cbs = json_decode($cbs);
		$cbs = json_decode(json_encode($cbs), true);
		
		
		$total_sel=0;
		$sel_skus=0;
		$sel_skus_count=0;
		foreach($cbs as $cb)
		{
			$cbnamearray = explode("_",$cb['cb_name']);
			//print_r($cbnamearray);			
			
			$select_req="SELECT count(id) as `sel_count` FROM issuance  WHERE `sku_id`='".$cbnamearray[3]."' AND  `student_id`='".$cbnamearray[2]."' AND courier_req_id='".$cbnamearray[4]."'";
		    //echo $select_req;
			$result_sku_sel = Select($select_req,$conn,"issuance");
			$total_sel = $result_sku_sel['rows'][0]['sel_count'];	

			//echo $total_sel."<br>";		
            if($total_sel>0)
			{	
            $del_req="DELETE FROM issuance  WHERE `sku_id`='".$cbnamearray[3]."' AND  `student_id`='".$cbnamearray[2]."' AND courier_req_id='".$cbnamearray[4]."'";
			//echo $del_req."<br>";
			$del_sku_sel = Select($del_req,$conn,"issuance");		
            

            $select_total_req="SELECT total_records as `tot_rec` FROM student_couriers_request  WHERE   `student_id`='".$cbnamearray[2]."' AND id='".$cbnamearray[4]."'";
		    //echo $select_total_req."<br>";
			$result_total_sel = Select($select_total_req,$conn,"student_couriers_request");
			$total_rec = $result_total_sel['rows'][0]['tot_rec'];
			//echo $total_rec."<br>";
			if($total_rec>1)
			{
			$final_val=$total_rec - 1;	
			$up_req="UPDATE student_couriers_request SET total_records = '".$final_val."' WHERE  `student_id`='".$cbnamearray[2]."' AND id='".$cbnamearray[4]."'";
			//echo $up_req."<br>";
			$upsel = Select($up_req,$conn,"student_couriers_request");		
			}
			else
			{
			$delcq_req="DELETE FROM student_couriers_request  WHERE  `student_id`='".$cbnamearray[2]."' AND id='".$cbnamearray[4]."'";
			//echo $delcq_req."<br>";
			$upcqsel = Select($delcq_req,$conn,"student_couriers_request");		
				
			}
			
			}
			
			
			$update_stock = "UPDATE skus_locations set sku_qty_req = sku_qty_req - 1 WHERE sku_id = '".$cbnamearray[3]."'   and loc_id = 1";
		    $result_stock = Select($update_stock,$conn,"skus_locations");
				
			
		}

			
		
	
		

		
		
		$output = array(
		"status"=>'success'
		);
		$output = json_encode($output);
		echo $output;
		exit();
	}
}
catch(PDOException $ex){
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	
	$output = array(
		"status"=>'db_error'
		);
	$output = json_encode($output);
	echo $output;
	exit();
}
?>