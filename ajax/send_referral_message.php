<?php
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => 'https://api.interakt.ai/v1/public/message/',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{
    "countryCode": "+91",
    "phoneNumber": "'.$da['phone'].'",
    "type": "Template",
    "template": {
        "name": "new_year_referral_revised",
        "languageCode": "en",
        "headerValues": [
            "https://interaktprodstorage.blob.core.windows.net/mediaprodstoragecontainer/b722fb93-d29f-4d03-939e-2128965ea222/message_template_media/n6x1AEvhCLQZ/SM%20referral%20banner%20%281%29.jpg?se=2028-01-06T05%3A04%3A31Z&sp=rt&sv=2019-12-12&sr=b&sig=kEwX2ZBK%2Brnj9w0dPPIvFyG9tvCJg%2BxnNEpYtf7QEU0%3D"
        ],
        "bodyValues": [
            "'.$da['student_name'].'."
        ]
    }
}',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Basic ZWJQRlBqdVJTT1BINGE5c1ItaXBhM1VzZDUyTWJ6WEpaajd0MWp3MnZjczo=',
    'Content-Type: application/json'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
$final_response=$response;