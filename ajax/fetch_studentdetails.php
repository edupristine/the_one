<?php
include '../inc/GenericFunctions.php';
include '../control/core.php';
include '../control/checklogin.php';
include '../control/connection.php';
$mysql_host='52.77.5.117';
$mysql_user='liveuser';
$mysql_pass='p3t3r5bu4g';
$conn1=mysqli_connect($mysql_host,$mysql_user,$mysql_pass,'vtigercrm6');
try{
	if( isset($_POST['account_no'])  &&  !empty($_POST['account_no']))
	{
		$account_no  = get_post_value('account_no');
		$output="";			
		$sel_regcode = "SELECT sc.accountid as `accountid`,ac.account_no,sc.cf_1495,ROUND(sc.cf_936,0) AS `offered_price`,ROUND(sc.cf_938,0) AS `Paid`,ROUND(sc.cf_940,0) AS `Balance`,ROUND(sc.cf_1291,0) as `discount`,ac.accountname AS `studentname`
		,ac.phone AS phone,ac.email1 AS email,sc.cf_1601 AS batch_date,sc.cf_1629 AS batch_location,sc.cf_918 AS course,DATE_FORMAT(ce.createdtime, '%d-%m-%Y') AS createdat,sc.cf_1513 AS subcourse,sc.cf_1649 AS modules,
		ROUND(sc.cf_1303,0) AS t_amt_1,sc.cf_1363 AS t_dat_1,sc.cf_1305 AS t_mod_1,sc.cf_1307 AS t_tra_1,sc.cf_1309 AS t_valid_1,
		ROUND(sc.cf_1313,0) AS t_amt_2,sc.cf_1365 AS t_dat_2,sc.cf_1315 AS t_mod_2,sc.cf_1317 AS t_tra_2,sc.cf_1319 AS t_valid_2,
		ROUND(sc.cf_1323,0) AS t_amt_3,sc.cf_1367 AS t_dat_3,sc.cf_1325 AS t_mod_3,sc.cf_1327 AS t_tra_3,sc.cf_1329 AS t_valid_3,
		ROUND(sc.cf_1333,0) AS t_amt_4,sc.cf_1369 AS t_dat_4,sc.cf_1335 AS t_mod_4,sc.cf_1337 AS t_tra_4,sc.cf_1339 AS t_valid_4,
		ROUND(sc.cf_1343,0) AS t_amt_5,sc.cf_1371 AS t_dat_5,sc.cf_1345 AS t_mod_5,sc.cf_1347 AS t_tra_5,sc.cf_1349 AS t_valid_5,
		ROUND(sc.cf_1353,0) AS t_amt_6,sc.cf_1373 AS t_dat_6,sc.cf_1355 AS t_mod_6,sc.cf_1357 AS t_tra_6,sc.cf_1359 AS t_valid_6,
		sc.cf_1109 AS first_name,sc.cf_1111 AS last_name
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		WHERE ce.smownerid!='3920'  
		and  ac.account_no='".$account_no."'";
	      
		$result = mysqli_query($conn1,$sel_regcode);
		$pay_details = mysqli_fetch_assoc($result);
		
	
		
		$sel_assigneduser1 = "SELECT us.user_name as `assigned_user`
		FROM vtiger_account ac
		LEFT JOIN `vtiger_accountscf` sc ON sc.`accountid` = ac.accountid
		INNER JOIN vtiger_crmentity ce ON ce.crmid = ac.accountid
		LEFT JOIN `vtiger_users` us ON us.id =ce.smownerid
		WHERE ce.smownerid!='3920'
		and  ac.account_no='".$account_no."'";

		$result1 = mysqli_query($conn1,$sel_assigneduser1);
		$res_assigneduser1 = mysqli_fetch_assoc($result1);
        $finalarr=array_merge($pay_details,$res_assigneduser1);
		$output = json_encode($finalarr);
        echo $output;
		
		mysqli_close($conn1);
		
	}
}
catch(PDOException $ex){
	print_r($ex);
	DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString(),$_SESSION['USER_ID']);
	$output = array(
		"status"=>'db_error'
		);
	
	$output = json_encode($output);
	echo $output;
	exit();
}
?>