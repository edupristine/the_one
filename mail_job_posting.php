<?php 
include 'inc/GenericFunctions.php';
include 'control/core.php'; 	
include 'control/checklogin.php';
include 'control/connection.php';

include("inc/head.php");
include("inc/sidebar.php");
include("inc/headermenu.php");

include("content/mail_job_posting.content.php");
//include("js/sku.js.php");

include("inc/footer.php");
include("inc/foot.php");
?>
<script src="./assets/js/demo1/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<link href="./assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="./assets/js/jqwidgets/jqwidgets/styles/jqx.base.css" type="text/css" />
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/scripts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxcore.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxlistbox.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxdropdownbutton.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxcolorpicker.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxwindow.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxeditor.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxtooltip.js"></script>
<script type="text/javascript" src="./assets/js/jqwidgets/jqwidgets/jqxcheckbox.js"></script>
<!-- Datatable CSS
<link href='DataTables/datatables.min.css' rel='stylesheet' type='text/css'> -->

<!-- jQuery Library -->
<!--<script src="jquery-3.3.1.min.js"></script>-->

<!-- Datatable JS 
<script src="DataTables/datatables.min.js"></script>-->

