<?php
	require $env;
	//echo "ABCC";
	try
	{
		//connect as appropriate as above
		$conn = new PDO("mysql:host=$mysql_host;dbname=$mydb_main",$mysql_user,$mysql_pass);
		$harry = new PDO("mysql:host=52.77.5.117;dbname=ecademe_harry_new","liveuser","p3t3r5bu4g");
		//$conn = new PDO("mysql:host=localhost;dbname=edupristine_one","root","ONEUSER1234");
		$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $ex)
	{
		$file	=	fopen('logs/connectionerror.log.txt','a');
		fwrite($file,'------------------------------------'.PHP_EOL);
		fwrite($file,'----------Connection Error----------'.PHP_EOL);
		fwrite($file,'Database:'.$mydb_main.PHP_EOL);
		fwrite($file,'Error Message:'.$ex->getMessage().PHP_EOL);
		fwrite($file,'Error File:'.$ex->getFile().PHP_EOL);
		fwrite($file,'Error Line:'.$ex->getLine().PHP_EOL);
		fwrite($file,'Error Code:'.$ex->getCode().PHP_EOL);
		fwrite($file,'Error Description:'.$ex->getTraceAsString().PHP_EOL);
		fwrite($file,'Date and Time:'.date("Y-m-d H:i:s").PHP_EOL);
		fclose($file);
		//DBLogError($ex->getCode(), $ex->getMessage(),$ex->getFile(),$ex->getLine(),$ex->getTraceAsString());
	}
?>